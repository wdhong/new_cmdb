// 用户信息缓存
export default {
    state: {
        //用户名
        userName: '',
        //问题及建议路径
        linkQuestions: '',
        //cmdb单独的路由
        toPage: '',
        //一级部门
        department1: '',
        //二级部门
        department2: '',
        //时间
        monthlyTime: '',
        //部门名称-bills
        departmentName:'',
        //部门ID-bills
        departmentID:'',

    },
    mutations: {
        //设置用户信息
        setUser(state, data) {
            let allKeys = Object.keys(state);
            for (let key in data) {
                if (allKeys.indexOf(key) > -1) {
                    state[key] = data[key];
                }
            }
        },
    },

    actions: {
        // setComponent(context, data = []) {
        //   context.commit('setComponenet', data);
        // }
    }


}