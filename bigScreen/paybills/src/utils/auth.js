/** 权限配置 **/
import store from '@/store'
import { getUrlKey } from '@/utils/filter.js'

//保存用户信息
function setStore() {
    store.commit('setUser', {
        userName: getUrlKey('userName') || getUrlKey('username'),
        linkQuestions: getUrlKey('linkQuestions'),
        department1: getUrlKey('department1'),
        department2: getUrlKey('department2') === 'null' ? null : getUrlKey('department2'),
        toPage: getUrlKey('toPage')
    });
}

//初始化
function init() {
    setStore()
}
init()