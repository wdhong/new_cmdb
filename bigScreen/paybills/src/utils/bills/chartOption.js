/** echarts图表option配置**/
export default {
    // 颜色系列
    'color-option': {
        // 渐变由深至浅[蓝、浅蓝、绿、浅绿、黄、橙、红、紫、青]
        linearColor: {
            blue: [{
                offset: 0,
                color: '#0D7EF4' // 0% 处的颜色
            },
            {
                offset: 1,
                color: '#0D7EF4' // 100% 处的颜色
            }
            ],
            lineGreen: [{
                offset: 0,
                color: '#20B6A8' // 0% 处的颜色
            },
            {
                offset: 1,
                color: '#20B6A8' // 100% 处的颜色
            }
            ],
            pieBlue: [{
                offset: 0,
                color: '#0099FF' // 0% 处的颜色
            },
            {
                offset: 1,
                color: '#0099FF' // 100% 处的颜色
            }
            ],
            piePurple: [{
                offset: 0,
                color: '#9723F3' // 0% 处的颜色
            },
            {
                offset: 1,
                color: '#9723F3' // 100% 处的颜色
            }
            ],
            pieGreen: [{
                offset: 0,
                color: '#08CE86' // 0% 处的颜色
            },
            {
                offset: 1,
                color: '#08CE86' // 100% 处的颜色
            }
            ],
            pieYellow: [{
                offset: 0,
                color: '#FFCC3F' // 0% 处的颜色
            },
            {
                offset: 1,
                color: '#FFCC3F' // 100% 处的颜色
            }
            ],
            pieBlueDark: [{
                offset: 0,
                color: '#391FCE' // 0% 处的颜色
            },
            {
                offset: 1,
                color: '#391FCE' // 100% 处的颜色
            }
            ],
            barBlue: [{
                offset: 0,
                color: '#1657F0' // 0% 处的颜色
            },
            {
                offset: 1,
                color: '#3CBAE3' // 100% 处的颜色
            }
            ],
            gaugePurple: [
                [0.5, new echarts.graphic.LinearGradient(0, 0, 1, 0, [{ // 右/下/左/上
                    offset: 0,
                    color: '#0FD3EF'
                },
                {
                    offset: 1,
                    color: '#6323DB'
                }
                ])],
                [1, '#17154F']
            ],
            gaugeRed: [
                [0.5, new echarts.graphic.LinearGradient(0, 0, 1, 0, [{ // 右/下/左/上
                    offset: 0,
                    color: '#10A2EB'
                },
                {
                    offset: 1,
                    color: '#B723DB'
                }
                ])],
                [1, '#17154F']
            ],
        },
    },

    // 饼图
    'pie-option': {
        textStyle: {
            color: '#B3B1CF',
            fontSize: 14
        },
        title: {
            show: false,
            text: '标题',
            subtext: '子标题',
            textStyle: {
                fontSize: 14,
                color: '#fff'
            },
            subtextStyle: {
                fontSize: 12,
                color: '#fff'
            },
            textAlign: 'center',
            x: '50%',
            y: '50%'
        },
        tooltip: {
            trigger: 'item',
            confine: true,
            textStyle: {
                fontSize: 12,
            },
            // formatter: function (param) {
            //   return param
            // },
        },
        //图形中间文字
        graphic: [
            {
                type: "text",
                id: 'text1',
                left: '23%',
                top: '37%',
                style: {
                    text: "",
                    textAlign: "",
                    fill: "#3FFFFE",
                    fontSize: '130%',
                },
            },
            {
                type: "text",
                id: 'text2',
                left: '17%',
                top: '75%',
                style: {
                    text: "云主机资源",
                    textAlign: "center",
                    fill: "#FFFFFF",
                    fontSize: '90%'
                }
            }, {
                type: "text",
                id: 'text3',
                left: '24%',
                top: '40%',
                style: {
                    text: "\n元",
                    textAlign: "center",
                    fill: "#3FFFFE",
                    fontSize: '100%'
                },
            },
        ],
        legend: {
            selectedMode:false,//  禁止点击
            show: true,
            itemWidth: 18,
            itemHeight: 20,
            orient: 'vertical',
            formatter: function (param) {
                return param
            },
            textStyle: {
                color: '#ADACC8',
                fontSize: 10,
            },
            data: ['名称']

        },
        grid: {

        },
        series: [
            {
                name: '名称',
                type: 'pie',
                radius: ['48%', '55%'], // 圆环半径
                center: ['25%', '40%'], // 圆心位置
                startAngle: 90,
                label: {
                    show: true,
                    formatter: '{b}\n{d}%',
                    rich: {

                    },
                    color: '#fff',
                    fontSize: 14,
                    align: 'center'
                },
                labelLine: {
                    show: true,
                },
                itemStyle: {
                    borderWidth: 3, //设置border的宽度有多大
                    borderColor: '#2E2979',
                    color: {
                        type: 'linear',
                        x: 0,
                        y: 1, // 由下至上
                        x2: 0,
                        y2: 0,
                        colorStops: [{
                            offset: 0,
                            color: '#4D4D67' // 0% 处的颜色
                        },
                        {
                            offset: 1,
                            color: '#4D4D67' // 100% 处的颜色
                        }
                        ],
                        global: false // 缺省为 false
                    }
                },
                selectedOffset: 5,
                data: [{
                    value: 0, // 占比0%
                    name: '暂无',
                    label: {
                        color: '#fff'
                    },
                    itemStyle: {
                        color: '#fff'
                    }
                }]
            },
        ]
    },
    // 线图
    'line-option': {
        color: ['#0D7EF4', '#20B6A8'],
        textStyle: {
            color: '#817DB0',
            fontSize: 14
        },
        title: {
            // show: false,
            text: '',
            textStyle: {
                fontSize: 14,
                color: '#817DB0'
            },
            subtext: '',
            subtextStyle: {
                fontSize: 14,
                color: '#817DB0'
            }
        },
        tooltip: {
            trigger: 'axis',
            confine: true,
            // padding: [5, 10],
            axisPointer: {
                lineStyle: {
                    color: 'rgba(255,255,255,0.36)'
                }
            },
            backgroundColor: '#0D7EF4',
            textStyle: {
                fontSize: 14,
            },
            formatter: function (param) {
                return param
            },
        },
        legend: {
            // show: true,
            // top: 0,
            // right: 10,
            // icon: 'circle',
            selectedMode: false,//禁止legend选中图表
            itemWidth: 14,
            itemHeight: 5,
            textStyle: {
                color: '#fff',
                fontSize: '14px'
            },
            left: 'auto',
            // data: ['名称']
        },
        grid: {
            left: 10,
            right: 50,
            top: 40,
            bottom: 30,
            containLabel: true
        },
        xAxis: {
            type: 'category',
            boundaryGap: false,
            axisLabel: {
                // interval: 0
                // rotate: 30
            },
            axisTick: {
                show: false
            },
            axisLine: {
                lineStyle: {
                    color: '#2A3E7A'
                }
            },
            data: ['暂无数据']
            // data: ['2019-02-14', '2019-02-15', '2019-02-16', '2019-02-17', '2019-02-18', '2019-02-19', '2019-02-20']
        },
        yAxis: {
            type: 'value',
            name: '',
            // min: 'dataMin',
            max: 'dataMax',
            nameTextStyle: {},
            axisLine: {
                // show: false
                lineStyle: {
                    color: '#2A3E7A'
                }
            },
            nameGap: 5,
            nameTextStyle: {
                fontSize: 12,
                padding: [0, 0, 0, -20]
            },
            axisTick: {
                show: false
            },
            splitLine: {
                lineStyle: {
                    // type: 'dashed',
                    color: 'rgba(255,255,255,0.16)'
                }
            }
        },
        series: [{
            name: '名称',
            type: 'line',
            // smooth: true,
            showSymbol: false,
            itemStyle: {
                normal: {
                    borderWidth: 4,
                },
                emphasis: {
                    color: '#0D7EF4',
                    borderColor: '#211E67'
                }
            },
            lineStyle: {
                width: 3,
                color: {
                    type: 'linear',
                    x: 0,
                    y: 0,
                    x2: 1,
                    y2: 0,
                    colorStops: [{
                        offset: 0,
                        color: '#186FFF' // 0% 处的颜色
                    },
                    {
                        offset: 1,
                        color: '#3EAAF6' // 100% 处的颜色
                    }
                    ],
                    global: false // 缺省为 false
                }

            },
            areaStyle: {
                color: {
                    type: 'linear',
                    x: 0,
                    y: 0,
                    x2: 0,
                    y2: 1,
                    colorStops: [{
                        offset: 0,
                        color: '#1350AA' // 0% 处的颜色
                    },
                    {
                        offset: 1,
                        color: '#19126F' // 100% 处的颜色
                    }
                    ],
                    global: false // 缺省为 false
                }
            },
            data: [0]
        }]
    }

}