#!/bin/bash

export ENV_CONFIG_IP=10.12.70.39
export ENV_CONFIG_PORT=18888
export ENV_TYPE=prod


nohup java -Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,address=9899,server=y,suspend=n -jar ./alert-schedule-service-0.0.1-SNAPSHOT.jar >/dev/null 2>&1 &
