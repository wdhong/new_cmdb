#!/usr/bin/env bash

cd ../../
path=`pwd`
echo ${path}

echo "java project is building  start..."
mvn clean install
echo "java project is building end..."

echo "cp alert.war ..."
cp ./mirror-alert/alert-service-war/target/alert.war bin/all-war

echo "cp common.war ..."
cp ./mirror-common/common-service-war/target/common.war bin/all-war

echo "cp composite.war ..."
cp ./mirror-composite/composite-service-war/target/composite.war bin/all-war

echo "cp proxy.war ..."
cp ./mirror-proxy/index-proxy-war/target/proxy.war bin/all-war

echo "cp config.jar ..."
cp ./mirror-public/config-server/target/config-server-0.0.1-SNAPSHOT.jar bin/config-server-0.0.1-SNAPSHOT.jar

echo "cp eureka.war ..."
cp ./mirror-public/eureka-server-war/target/ROOT.war bin/all-war

echo "cp template.war ..."
cp ./mirror-template/template-service-war/target/template.war bin/all-war

echo "cp theme.war ..."
cp ./mirror-theme/theme-service-war/target/theme.war bin/all-war