#!/bin/bash

export ENV_CONFIG_IP=10.12.70.39
export ENV_CONFIG_PORT=18888
export ENV_TYPE=prod

#nohup java -jar ./collect-service-0.0.1-SNAPSHOT.jar &

nohup java -Xms128M -Xmx256M -jar ./collect-service-0.0.1-SNAPSHOT.jar >/dev/null 2>&1 &
