INSERT INTO `resource_schema` VALUES ('template', 'f', '2017-07-17 22:10:52.000000');
INSERT INTO `resource_schema` VALUES ('task', 'f', '2017-07-17 22:10:52.000000');
INSERT INTO `resource_schema` VALUES ('report', 'f', '2017-07-17 22:10:52.000000');


INSERT INTO `resource_schema_actions` VALUES ('template', 'template:create');
INSERT INTO `resource_schema_actions` VALUES ('template', 'template:delete');
INSERT INTO `resource_schema_actions` VALUES ('template', 'template:update');
INSERT INTO `resource_schema_actions` VALUES ('template', 'template:view');
INSERT INTO `resource_schema_actions` VALUES ('template', 'template:copy');


INSERT INTO `resource_schema_actions` VALUES ('task', 'task:create');
INSERT INTO `resource_schema_actions` VALUES ('task', 'task:delete');
INSERT INTO `resource_schema_actions` VALUES ('task', 'task:update');
INSERT INTO `resource_schema_actions` VALUES ('task', 'task:view');
INSERT INTO `resource_schema_actions` VALUES ('task', 'task:addSchedule');
INSERT INTO `resource_schema_actions` VALUES ('task', 'task:stopSchedule');

INSERT INTO `resource_schema_actions` VALUES ('report', 'report:view');
