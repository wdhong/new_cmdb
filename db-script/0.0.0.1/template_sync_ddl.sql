CREATE TABLE `monitor_template_data_sync` (

`sync_id` int(32) NOT NULL AUTO_INCREMENT comment '同步',

`sync_data_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '同步数据类型',

`data_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '同步数据ID',

`operate_type`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,

PRIMARY KEY (`sync_id`)

)
ENGINE=InnoDB

DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
;
