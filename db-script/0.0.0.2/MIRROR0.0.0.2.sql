CREATE TABLE `alert_alerts` (
`alert_id` varchar(50) NOT NULL,
`r_alert_id` varchar(50) NULL,
`event_id` varchar(50) NOT NULL,
`device_id` varchar(50) NULL,
`biz_sys` varchar(50) NOT NULL COMMENT '业务系统',
`moni_index` varchar(4000) NOT NULL COMMENT '监控指标/内容，关联触发器name',
`moni_object` varchar(2000) NULL COMMENT '监控对象',
`cur_moni_value` varchar(2000) NULL COMMENT '当前监控值',
`cur_moni_time` datetime NULL COMMENT '当前监控时间',
`alert_level` varchar(20) NOT NULL COMMENT '告警级别\r\n1-提示\r\n2-低\r\n3-中\r\n4-高\r\n5-严重\r\n',
`item_id` varchar(50) NOT NULL,
`alert_end_time` datetime NULL COMMENT '告警结束时间',
`remark` varchar(4000) NULL COMMENT '备注',
`order_status` varchar(50) NULL COMMENT '1-未派单\r\n2-处理中\r\n3-已完成\r\n',
`source` varchar(100) NULL COMMENT '告警来源\r\nMIRROR\r\nZABBIX\r\n',
`source_room` varchar(100) NULL COMMENT '机房/资源池',
`object_type` varchar(50) NULL COMMENT '告警类型\r\n1-系统\r\n2-业务',
`object_id` varchar(50) NULL COMMENT '对象ID，如果是设备告警则是设备ID，如果是业务则是业务系统code',
`region` varchar(50) NULL COMMENT '域/资源池code',
`device_ip` varchar(100) NULL,
PRIMARY KEY (`alert_id`) 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `alert_alerts_his` (
`alert_id` varchar(50) NOT NULL,
`r_alert_id` varchar(50) NULL,
`event_id` varchar(50) NOT NULL,
`device_id` varchar(50) NULL,
`biz_sys` varchar(50) NOT NULL COMMENT '业务系统',
`moni_index` varchar(4000) NOT NULL COMMENT '监控指标/内容，关联触发器name',
`moni_object` varchar(2000) NULL COMMENT '监控对象',
`cur_moni_value` varchar(2000) NULL COMMENT '当前监控值',
`cur_moni_time` datetime NULL COMMENT '当前监控时间',
`alert_level` varchar(20) NOT NULL COMMENT '告警级别\r\n1-提示\r\n2-低\r\n3-中\r\n4-高\r\n5-严重\r\n',
`item_id` varchar(50) NOT NULL,
`alert_end_time` datetime NULL COMMENT '告警结束时间',
`remark` varchar(4000) NULL COMMENT '备注',
`order_status` varchar(50) NULL COMMENT '1-未派单\r\n2-处理中\r\n3-已完成\r\n',
`clear_time` datetime NULL COMMENT '清除时间',
`source` varchar(100) NULL COMMENT '告警来源\r\nMIRROR',
`source_room` varchar(100) NULL COMMENT '机房',
`object_type` varchar(50) NULL,
`object_id` varchar(50) NULL,
`region` varchar(50) NULL,
`device_ip` varchar(100) NULL,
PRIMARY KEY (`alert_id`) 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci;CREATE TABLE `alert_manual_statistic` (`room_id`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '机房id' ,`source_alert_id`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '原始告警id' ,`auto_count`  int(11) NOT NULL DEFAULT 0 COMMENT '自动清除次数' ,`manual_count`  int(11) NOT NULL DEFAULT 0 COMMENT '手工清除次数' )ENGINE=InnoDBDEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci;
alter table alert_alerts add order_type varchar(20) comment '工单类型\r\n1-告警\r\n2-故障';alter table alert_alerts add order_id varchar(100) comment '工单ID';alter table alert_alerts add biz_sys_desc varchar(200) comment '业务系统描述';
