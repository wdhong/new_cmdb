INSERT INTO `monitor_biz_theme_dim` (`id`, `dim_name`, `dim_type`) VALUES (10, '时间', '2');

-- {{ jinsu add mirror0.0.0.3-2
INSERT INTO `code_dict` (`code_type`, `key`, `value`, `code_desc`, `valid_flag`, `order_id`) VALUES ('theme_access_url', 'in', 'http://10.12.70.37:8129/v1/theme/createThemeData', '内部主题数据接入地址', '1', 1);
INSERT INTO `code_dict` (`code_type`, `key`, `value`, `code_desc`, `valid_flag`, `order_id`) VALUES ('theme_access_url', 'out', 'www.baidu.com', '外部主题数据接入地址', '1', 2);

update monitor_template set status='1';
-- }}