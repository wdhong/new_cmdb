CREATE TABLE `monitor_biz_theme` (
`theme_id` varchar(50) NOT NULL COMMENT '主题ID',
`theme_code` varchar(255) NOT NULL COMMENT '主题编码',
`object_type` varchar(50) NOT NULL COMMENT '关联对象类型\r\n1-设备ID\r\n2-业务系统',
`object_id` varchar(50) NULL COMMENT 'object_id',
`index_name` varchar(255) NOT NULL COMMENT 'es索引名',
`value_type` varchar(10) NOT NULL COMMENT '数据类型0：数字1：字符串',
`up_type` varchar(10) NOT NULL COMMENT '上报类型0：接口接入1：日志接入',
`up_status` varchar(10) NULL COMMENT '上报状态0：成功1：失败',
`last_up_value` varchar(10) NULL COMMENT '最近上报值',
`last_up_time` datetime NULL COMMENT '最近上报时间',
`up_switch` varchar(10) NULL COMMENT '上报开关0：开启1：关闭',
`status` varchar(10) NULL COMMENT '状态0：正式1：临时',
`create_time` datetime NULL COMMENT '创建时间',
`last_fail_time` datetime NULL COMMENT '最近失败上报时间',
`interval` int(255) NULL,
`dim_ids` varchar(255) NULL COMMENT '维度id集合',
`theme_name` varchar(255) NULL,
PRIMARY KEY (`theme_id`) 
);

CREATE TABLE `monitor_biz_theme_dim` (`id` int(50) NOT NULL AUTO_INCREMENT,`dim_name` varchar(255) NULL COMMENT '维度名称',`dim_type` varchar(10) NULL COMMENT '维度类型',PRIMARY KEY (`id`) );
alter table monitor_biz_theme_dim auto_increment = 11; SET GLOBAL event_scheduler = 1;-- {{ jinsu add mirror0.0.0.3-2CREATE EVENT IF NOT EXISTS theme_delete_event       ON SCHEDULE EVERY 1 DAY STARTS DATE_ADD(DATE_ADD(CURDATE(), INTERVAL 1 DAY), INTERVAL 1 HOUR)       ON COMPLETION PRESERVE ENABLE       DO delete from monitor_biz_theme where status = '1';	alter table monitor_items add calc_type varchar(20);alter table monitor_items add biz_index varchar(200);alter table monitor_items add biz_calc_obj varchar(4000);alter table monitor_items add biz_calc_exp varchar(200);alter table monitor_items add biz_theme_id varchar(50);alter table monitor_items add biz_is_zero varchar(50);alter table monitor_items add cron varchar(20);
alter table monitor_template add mon_type varchar(20);alter table monitor_template add status varchar(20);alter table monitor_template_data_sync add operate_type varchar(10);CREATE TABLE IF NOT EXISTS `code_dict` (  `id` int(32) NOT NULL AUTO_INCREMENT COMMENT '主键ID',  `code_type` varchar(50) NOT NULL COMMENT '编码类型',  `key` varchar(50) NOT NULL COMMENT '编码key',  `value` varchar(100) NOT NULL COMMENT '编码值',  `code_desc` varchar(255) DEFAULT NULL COMMENT '描述',  `valid_flag` varchar(1) NOT NULL COMMENT '是否有效or可见，1=是；0=否',  `order_id` int(11) DEFAULT NULL COMMENT '排序（从1开始的正整数，NULL表示不排序，各level互不影响）',  `parent_id` int(32) DEFAULT NULL COMMENT '上级ID',  PRIMARY KEY (`id`)) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='编码字典';-- end }}