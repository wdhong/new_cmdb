ALTER TABLE cmdb_instance ADD INDEX IDX_INSTANCE_01 (moduleId);
ALTER TABLE cmdb_instance_form_value ADD UNIQUE INDEX IDX_FORM_VALUE_01 (formCode,instanceId);
ALTER TABLE cmdb_instance_form_value ADD UNIQUE INDEX IDX_FORM_VALUE_02 (formId,instanceId);
ALTER TABLE cmdb_form_options ADD UNIQUE INDEX IDX_FORM_OPTIONS_01 (formId,value);