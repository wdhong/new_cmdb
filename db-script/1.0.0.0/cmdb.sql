/*
Navicat MySQL Data Transfer

Source Server         : 10.1.5.119
Source Server Version : 50718
Source Host           : 10.1.5.119:3306
Source Database       : cmdb

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2019-03-19 18:06:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_collect
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_collect`;
CREATE TABLE `cmdb_collect` (
  `id` varchar(40) NOT NULL COMMENT 'ID',
  `moduleId` varchar(40) NOT NULL COMMENT '模型ID',
  `formId` varchar(40) NOT NULL COMMENT '模型表单ID',
  `frequency` varchar(10) DEFAULT NULL COMMENT '执行频率 高/中/低',
  `needAlarm` varchar(2) DEFAULT NULL COMMENT '需要告警 是/否',
  `alermLevel` varchar(50) DEFAULT NULL COMMENT '告警等级',
  `needNotice` varchar(2) DEFAULT NULL COMMENT '需要通知 是/否',
  `visitType` varchar(10) DEFAULT NULL COMMENT '访问方式 SSH/登录密码/SNMP',
  `visitRequest` varchar(500) DEFAULT NULL COMMENT '访问请求信息, SSH用户/登录密码/共同体 信息',
  `disabled` varchar(2) DEFAULT '' COMMENT '是否禁用 是/否',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for cmdb_collect_change_log
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_collect_change_log`;
CREATE TABLE `cmdb_collect_change_log` (
  `id` varchar(40) NOT NULL COMMENT 'ID',
  `instanceId` varchar(40) DEFAULT NULL COMMENT '实例ID',
  `formId` varchar(40) DEFAULT NULL COMMENT '字段ID',
  `batchId` varchar(40) DEFAULT NULL COMMENT '操作号, 同一批操作的模型字段号，该值相同',
  `oldValue` varchar(2000) DEFAULT NULL,
  `currValue` varchar(2000) DEFAULT NULL,
  `operaterType` varchar(40) DEFAULT NULL COMMENT '操作方式',
  `operator` varchar(40) DEFAULT NULL COMMENT '操作人',
  `operatorTime` timestamp NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`),
  KEY `IDX_CHANGE_LOG_FORMID` (`formId`),
  KEY `IDX_CHANGE_LOG_BATCHID` (`instanceId`,`batchId`),
  KEY `IDX_CHANGE_LOG_3` (`instanceId`,`formId`,`batchId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for cmdb_collect_employee_manager
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_collect_employee_manager`;
CREATE TABLE `cmdb_collect_employee_manager` (
  `id` varchar(40) NOT NULL COMMENT 'ID',
  `collectId` varchar(40) NOT NULL COMMENT '采集ID',
  `opertype` varchar(40) DEFAULT NULL COMMENT '操作类型 通知/审批',
  `employeeName` varchar(40) DEFAULT NULL COMMENT '人员名称',
  `employeeAddr` varchar(50) DEFAULT NULL COMMENT '通知地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for cmdb_collect_original_record
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_collect_original_record`;
CREATE TABLE `cmdb_collect_original_record` (
  `id` varchar(40) NOT NULL COMMENT 'ID',
  `collectId` varchar(40) NOT NULL COMMENT '采集配置ID',
  `instanceId` varchar(40) NOT NULL COMMENT '实例ID',
  `taskId` varchar(40) NOT NULL COMMENT '采集任务ID',
  `operateType` varchar(40) DEFAULT NULL COMMENT '操作方式 自动采集/手工采集/导入',
  `operator` varchar(40) DEFAULT NULL COMMENT '操作人',
  `collectTime` timestamp DEFAULT NULL COMMENT '操作时间',
  `collectStatus` varchar(10) DEFAULT NULL COMMENT '操作状态 成功/失败',
  `collectResult` text COMMENT '操作结果',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `cmdb_dict` (
  `id` varchar(40) DEFAULT NULL COMMENT 'ID',
  `code` varchar(40) NOT NULL COMMENT '字典编码, code + name 应该保持唯一',
  `name` varchar(40) NOT NULL COMMENT '字典名称',
  `value` varchar(40) DEFAULT NULL COMMENT '字典值',
  `displayOrder` int(11) DEFAULT NULL COMMENT '字典排序',
  PRIMARY KEY (`code`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cmdb_dict
-- ----------------------------
INSERT INTO `cmdb_dict` VALUES ('9', 'alerm_level', '中', '3', '3');
INSERT INTO `cmdb_dict` VALUES ('7', 'alerm_level', '低', '2', '1');
INSERT INTO `cmdb_dict` VALUES ('8', 'alerm_level', '提示', '1', '2');
INSERT INTO `cmdb_dict` VALUES ('10', 'alerm_level', '高', '4', '4');
INSERT INTO `cmdb_dict` VALUES ('11', 'alert_level', '严重', '5', '5');
INSERT INTO `cmdb_dict` VALUES ('1', 'collect_notice_employee', '系统管理员', 'admin@aspirecn.com', '1');
INSERT INTO `cmdb_dict` VALUES ('3', 'frequency', '中', '中', '2');
INSERT INTO `cmdb_dict` VALUES ('4', 'frequency', '低', '低', '3');
INSERT INTO `cmdb_dict` VALUES ('2', 'frequency', '高', '高', '1');
INSERT INTO `cmdb_dict` VALUES ('6', 'whether', '否', '否', '2');
INSERT INTO `cmdb_dict` VALUES ('5', 'whether', '是', '是', '1');
INSERT INTO `cmdb`.`cmdb_dict` (`id`, `code`, `name`, `value`, `displayOrder`) VALUES ('12', 'collect_opertype', '修改', '修改', '1');

DROP TABLE IF EXISTS `cmdb_column_query_condition`;
CREATE TABLE `cmdb_column_query_condition` (
  `id` varchar(40) NOT NULL,
  `queryName` varchar(40) NOT NULL COMMENT '查询名称',
  `user` varchar(40) NOT NULL COMMENT '登录用户ID',
  `menuType` varchar(40) NOT NULL COMMENT '菜单类型:Maintain:维护圈 Repertry:仓库',
  `moduleId` varchar(40) NOT NULL COMMENT '模型ID',
  `queryInfo` text COMMENT '选择字段信息',
  `insertTime` timestamp NULL DEFAULT NULL,
  `updateTime` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='cmdb高级查询条件表';