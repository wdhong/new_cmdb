 

ALTER TABLE `alert_alerts`
ADD COLUMN `idc_type`  varchar(100) NULL COMMENT '所属位置-资源池' AFTER `source`;

ALTER TABLE `alert_alerts_his`
ADD COLUMN `idc_type`  varchar(100) NULL COMMENT '所属位置-资源池' AFTER `source`;

ALTER TABLE alert_alerts ADD COLUMN `operate_status` TINYINT(1) DEFAULT 0 COMMENT '警报操作状态:0-待确认,1-已确认,2-待解决,3-已处理' AFTER `order_status`;
ALTER TABLE alert_operation_record MODIFY `operation_type` TINYINT(1) NOT NULL COMMENT '操作类型 0-转派, 1-确认,2-派发工单, 3-清除, 4-通知, 5-过滤, 6-工程, 7-维护模式';

ALTER TABLE alert_alerts ADD COLUMN `device_class` VARCHAR(50) DEFAULT NULL COMMENT '设备类型' AFTER `device_id`;
ALTER TABLE alert_alerts_his ADD COLUMN `device_class` VARCHAR(50) DEFAULT NULL COMMENT '设备类型' AFTER `device_id`;

-- ----------------------------
-- Table structure for alert_config
-- ----------------------------
DROP TABLE IF EXISTS `alert_config`;
CREATE TABLE `alert_config` (
  `ID` varchar(255) NOT NULL,
  `TITLE` varchar(64) DEFAULT NULL,
  `DESCRIPTION` text,
  `CREATOR` varchar(64) DEFAULT NULL,
  `CRATE_TIME` datetime DEFAULT NULL,
  `IS_START` char(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for alert_config_detail
-- ----------------------------
DROP TABLE IF EXISTS `alert_config_detail`;
CREATE TABLE `alert_config_detail` (
  `ID` varchar(32) NOT NULL,
  `ALERT_CONFIG_ID` varchar(255) DEFAULT NULL,
  `CONFIG_TYPE` char(1) DEFAULT NULL,
  `TARGET_ID` text,
  `MONIT_TYPE` varchar(64) DEFAULT NULL,
  `ALERT_LEVEL` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ALERT_CONFIG_ID_f` (`ALERT_CONFIG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `alert_operation_record`;
CREATE TABLE `alert_operation_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `alert_id` varchar(50) NOT NULL COMMENT '告警id',
  `user_id` varchar(32) DEFAULT NULL COMMENT '操作人',
  `user_name` varchar(32) DEFAULT NULL COMMENT '操作人名称',
  `operation_type` tinyint(1) NOT NULL COMMENT '操作类型 0-转派, 1-确认,2-派发工单, 3-清除, 4-通知, 5-过滤, 6-工程, 7-维护模式',
  `operation_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  `operation_status` tinyint(1) DEFAULT '1' COMMENT '操作状态 0-失败 1-成功,',
  `content` varchar(256) DEFAULT NULL COMMENT '操作内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8;

 
DROP TABLE IF EXISTS `alert_report_operate_record`;

CREATE TABLE `alert_report_operate_record` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ALERT_ID` varchar(64) DEFAULT NULL COMMENT '警报ID',
  `USER_ID` varchar(32) DEFAULT NULL COMMENT '发件人ID',
  `USER_NAME` varchar(32) DEFAULT NULL COMMENT '发件人姓名',
  `REPORT_TYPE` tinyint(1) DEFAULT '0' COMMENT '通知类型, 0:短信,1:邮件',
  `USER_EMAIL` varchar(32) DEFAULT NULL COMMENT '发件人邮箱',
  `DESTINATION` varchar(64) DEFAULT NULL COMMENT '短信/邮件 地址',
  `MESSAGE` text COMMENT '短信/邮件 内容',
  `STATUS` tinyint(1) DEFAULT '1' COMMENT '发送状态,0:失败,1:成功',
  `CREATE_TIME` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COMMENT='告警通知操作记录表';


DROP TABLE IF EXISTS `alert_transfer`;

CREATE TABLE `alert_transfer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `alert_id` varchar(50) NOT NULL COMMENT '告警id',
  `user_id` varchar(32) DEFAULT NULL COMMENT '操作人id',
  `user_name` varchar(32) DEFAULT NULL COMMENT '操作人名称',
  `confirm_user_id` varchar(32) DEFAULT NULL COMMENT '待确认人id',
  `confirm_user_name` varchar(32) DEFAULT NULL COMMENT '待确认人name',
  `operation_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1182 DEFAULT CHARSET=utf8;

 
