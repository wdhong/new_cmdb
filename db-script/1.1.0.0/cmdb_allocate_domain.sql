﻿/*
Navicat MySQL Data Transfer

Source Server         : 10.1.5.119
Source Server Version : 50718
Source Host           : 10.1.5.119:3306
Source Database       : cmdb

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2019-04-16 10:30:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_allocate_domain
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_allocate_domain`;
CREATE TABLE `cmdb_allocate_domain` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `system_id` varchar(50) NOT NULL COMMENT '资源池id',
  `hostnet` tinyint(1) NOT NULL COMMENT '承载网络  0-承载网, 1-公网',
  `domain` varchar(50) NOT NULL COMMENT '域名',
  `user_name` varchar(32) DEFAULT NULL COMMENT '联系人姓名',
  `telephone` varchar(32) DEFAULT NULL COMMENT '电话',
  `description` varchar(256) DEFAULT NULL COMMENT '描述说明',
  `flag` int(5) DEFAULT '0' COMMENT '删除标志 0-未删除 1-已删除',
  PRIMARY KEY (`id`) 
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='cmdb域名分配表';
