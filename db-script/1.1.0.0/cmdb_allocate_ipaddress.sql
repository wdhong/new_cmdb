﻿/*
Navicat MySQL Data Transfer

Source Server         : 10.1.5.119
Source Server Version : 50718
Source Host           : 10.1.5.119:3306
Source Database       : cmdb

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2019-04-16 17:32:15
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_allocate_ipaddress
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_allocate_ipaddress`;
CREATE TABLE `cmdb_allocate_ipaddress` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `vpn_id` bigint(50) NOT NULL COMMENT '域名id',
  `network_id` bigint(50) NOT NULL COMMENT '网段',
  `biz_system` varchar(50) DEFAULT NULL COMMENT '业务系统',
  `allocate_ip` varchar(50) NOT NULL COMMENT '分配ip',
  `clouds_ip` varchar(50) DEFAULT NULL COMMENT '私有云ip',
  `start_time` varchar(50) DEFAULT NULL COMMENT '生效开始时间',
  `end_time` varchar(50) DEFAULT NULL COMMENT '生效结束时间',
  `user_name` varchar(32) DEFAULT NULL COMMENT '联系人姓名',
  `telephone` varchar(32) DEFAULT NULL COMMENT '电话',
  `flag` int(10) DEFAULT NULL COMMENT '删除标志 0-未删除 1-已删除',
  PRIMARY KEY (`id`),
  KEY `t_ipaddress_fk_1` (`network_id`),
  CONSTRAINT `cmdb_allocate_ipaddress_ibfk_1` FOREIGN KEY (`network_id`) REFERENCES `cmdb_allocate_netsegment` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COMMENT='cmdb-ip分配配置表';
