﻿/*
Navicat MySQL Data Transfer

Source Server         : 10.1.5.119
Source Server Version : 50718
Source Host           : 10.1.5.119:3306
Source Database       : cmdb

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2019-04-16 10:31:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_allocate_netsegment
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_allocate_netsegment`;
CREATE TABLE `cmdb_allocate_netsegment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hostnet` tinyint(1) NOT NULL COMMENT '承载网络  0-承载网, 1-公网',
  `domain_id` bigint(50) NOT NULL COMMENT '域名',
  `network_segment` varchar(50) NOT NULL COMMENT '网段',
  `flag` int(5) DEFAULT '0' COMMENT '删除标志 0-未删除 1-已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='cmdb域名ip关联表';
