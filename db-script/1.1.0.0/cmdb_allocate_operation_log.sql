/*
Navicat MySQL Data Transfer

Source Server         : 10.1.5.119
Source Server Version : 50718
Source Host           : 10.1.5.119:3306
Source Database       : cmdb

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2019-04-16 10:31:48
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_allocate_operation_log
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_allocate_operation_log`;
CREATE TABLE `cmdb_allocate_operation_log` (
  `id` varchar(50) NOT NULL COMMENT 'id',
  `type` bigint(20) NOT NULL COMMENT '操作类型：0-添加 1-删除 ',
  `allocate_ip_id` text COMMENT '分配ip的id',
  `operator` varchar(25) DEFAULT NULL COMMENT '操作者',
  `operation_time` datetime DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='cmdb ip分配配置管理操作记录表';
