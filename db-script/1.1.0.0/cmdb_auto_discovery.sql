/*
Navicat MySQL Data Transfer

Source Server         : 10.1.5.119
Source Server Version : 50718
Source Host           : 10.1.5.119:3306
Source Database       : cmdb

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2019-04-16 10:53:57
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_auto_discovery_log
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_auto_discovery_log`;
CREATE TABLE `cmdb_auto_discovery_log` (
  `id` varchar(40) NOT NULL COMMENT 'ID',
  `ruleId` varchar(40) DEFAULT NULL COMMENT '规则ID',
  `instanceName` varchar(40) DEFAULT NULL COMMENT '发现实例名称',
  `status` varchar(200) DEFAULT NULL COMMENT '处理状态',
  `remark` text COMMENT '备注',
  `createTime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `updateTime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新的时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for cmdb_auto_discovery_rule
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_auto_discovery_rule`;
CREATE TABLE `cmdb_auto_discovery_rule` (
  `id` varchar(40) NOT NULL COMMENT 'ID',
  `ruleName` varchar(200) NOT NULL COMMENT '规则名称',
  `moduleId` varchar(40) NOT NULL COMMENT '模型ID',
  `room` varchar(40) DEFAULT NULL COMMENT '所属机房',
  `discoveryType` varchar(40) NOT NULL COMMENT '发现类型',
  `discoveryParam` varchar(200) DEFAULT NULL COMMENT '发现类型对应的参数',
  `startScanIp` varchar(40) NOT NULL COMMENT '开始扫描网段',
  `endScanIp` varchar(40) NOT NULL COMMENT '结束扫描网段',
  `collectCycle` int(11) NOT NULL COMMENT '采集周期',
  `cycleUnit` varchar(10) NOT NULL COMMENT '采集周期单位',
  `enable` varchar(4) NOT NULL COMMENT '是否启用',
  `createTime` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
