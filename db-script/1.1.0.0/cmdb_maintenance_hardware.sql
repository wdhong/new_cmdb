/*
SQLyog Ultimate v11.24 (32 bit)
MySQL - 5.7.18 : Database - cmdb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cmdb` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `cmdb`;

/*Table structure for table `cmdb_maintenance_hardware` */

DROP TABLE IF EXISTS `cmdb_maintenance_hardware`;

CREATE TABLE `cmdb_maintenance_hardware` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `contract_id` varchar(64) DEFAULT NULL COMMENT '合同编号',
  `mainten_factory` varchar(64) DEFAULT NULL COMMENT '维保厂家',
  `province` varchar(64) DEFAULT NULL COMMENT '省份',
  `city` varchar(64) DEFAULT NULL COMMENT '市/区',
  `resource_pool` varchar(64) DEFAULT NULL COMMENT '资源池',
  `brand` varchar(32) DEFAULT NULL COMMENT '品牌',
  `device_type` varchar(20) DEFAULT NULL COMMENT '设备类型',
  `device_model` varchar(64) DEFAULT NULL COMMENT '设备型号',
  `device_serial_number` varchar(64) DEFAULT NULL COMMENT '设备序列号',
  `assets_number` varchar(32) DEFAULT NULL COMMENT '资产编号',
  `network_date` date DEFAULT NULL COMMENT '入网时间',
  `date_warranty01` date DEFAULT NULL COMMENT '出保日期01',
  `date_warranty02` date DEFAULT NULL COMMENT '出保日期02',
  `date_warranty03` date DEFAULT NULL COMMENT '出保日期03',
  `system_name` varchar(64) DEFAULT NULL COMMENT '承载业务',
  `service_time` varchar(64) DEFAULT NULL COMMENT '服务时间',
  `response_time` varchar(64) DEFAULT NULL COMMENT '响应时间',
  `person_time` varchar(64) DEFAULT NULL COMMENT '人员到场时间',
  `spare_time` varchar(64) DEFAULT NULL COMMENT '备件到场时间',
  `phone_patch` varchar(10) DEFAULT NULL COMMENT '电话支持及补丁下载 0-否 1-是',
  `spare_replace` varchar(10) DEFAULT NULL COMMENT '备件现场更换 0-否 1-是',
  `trouble_optimize` varchar(10) DEFAULT NULL COMMENT '故障处理及优化 0-否 1-是',
  `duty_service` varchar(10) DEFAULT NULL COMMENT '驻场及值班服务 0-否 1-是',
  `machine_room` varchar(64) DEFAULT NULL COMMENT '设备物理位置(机房）',
  `cabinet` varchar(20) DEFAULT NULL COMMENT '机柜',
  `card_serial` varchar(20) DEFAULT NULL COMMENT '板卡序列号',
  `pool_manage` varchar(20) DEFAULT NULL COMMENT '是否资源池管理设备 0-否 1-是',
  `thirdpart_service` varchar(64) DEFAULT NULL COMMENT '第三方服务的选择原则',
  `mainten_service_time` varchar(64) DEFAULT NULL COMMENT '拟购买的维保服务时限',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
