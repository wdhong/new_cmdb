/*
Navicat MySQL Data Transfer

Source Server         : 10.1.5.119
Source Server Version : 50718
Source Host           : 10.1.5.119:3306
Source Database       : test_mirror

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2019-04-11 09:45:18
*/


-- ----------------------------
-- Table structure for alert_filter
-- ----------------------------
DROP TABLE IF EXISTS `alert_filter`;
CREATE TABLE `alert_filter` (
  `id` tinyint(12) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8 NOT NULL,
  `creater` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `editer` varchar(64) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `note` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*
Navicat MySQL Data Transfer

Source Server         : 10.1.5.119
Source Server Version : 50718
Source Host           : 10.1.5.119:3306
Source Database       : test_mirror

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2019-04-11 13:56:02
*/


-- ----------------------------
-- Table structure for alert_filter_scene
-- ----------------------------
DROP TABLE IF EXISTS `alert_filter_scene`;
CREATE TABLE `alert_filter_scene` (
  `id` tinyint(12) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8 NOT NULL,
  `filter_id` tinyint(12) DEFAULT NULL,
  `condition` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `operate_user` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `creater` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `editer` varchar(64) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `note` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS cyc_report_resource;
CREATE TABLE cyc_report_resource
(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_type` tinyint(1) NOT NULL COMMENT '数据文件类型',
  `file_path` varchar(200) NOT NULL COMMENT '数据文件的全路径',
  `file_name` varchar(100) NOT NULL COMMENT '数据文件名',
  `start_time` datetime DEFAULT NULL COMMENT '数据文件名中的数据开始时间',
  `session_id` varchar(50) DEFAULT NULL COMMENT '数据文件名中的sessionId',
  `request_id` int(11) NOT NULL COMMENT '请求消息的流水号',
  `read_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '数据文件读取状态, 0:未读, 1:已读',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY(`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
create index idx_cyc_rpt_res_start_time on cyc_report_resource(start_time);

ALTER TABLE `alert_alerts`
ADD COLUMN `action_id`  bigint(20) NULL AFTER `event_id`;
ALTER TABLE `alert_alerts_his`
ADD COLUMN `action_id`  bigint(20) NULL AFTER `event_id`;
ALTER TABLE `alert_alerts_his`
ADD INDEX `index_group` (`device_ip`, item_id, `alert_level`) USING BTREE ;
ALTER TABLE `alert_alerts`
ADD INDEX `index_group` (`device_ip`, item_id, `alert_level`) USING BTREE ;
ALTER TABLE `alert_operation_record`
ADD INDEX `index_alert_id` (`alert_id`) USING BTREE ;