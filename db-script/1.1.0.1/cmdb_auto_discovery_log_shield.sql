/*
Navicat MySQL Data Transfer

Source Server         : 10.1.5.119
Source Server Version : 50718
Source Host           : 10.1.5.119:3306
Source Database       : cmdb

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2019-04-24 16:34:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_auto_discovery_log_shield
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_auto_discovery_log_shield`;
CREATE TABLE `cmdb_auto_discovery_log_shield` (
  `id` varchar(40) NOT NULL COMMENT 'ID',
  `ruleId` varchar(40) NOT NULL COMMENT '规则ID',
  `instanceName` varchar(40) NOT NULL COMMENT '屏蔽实例名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `cmdb`.`cmdb_auto_discovery_log`
 CHANGE `ruleId` `ruleId` VARCHAR (40) CHARSET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '规则ID',
  CHANGE `instanceName` `instanceName` VARCHAR (40) CHARSET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '发现实例名称',
  CHANGE `updateTime` `updateTime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL COMMENT '更新的时间',
    CHANGE `status` `status` VARCHAR (200) CHARSET utf8 COLLATE utf8_general_ci DEFAULT '待处理' NOT NULL COMMENT '处理状态（待处理 已屏蔽 已维护 已绑定）';
