INSERT INTO `code_dict` (`code_type`, `key`, `value`, `code_desc`, `valid_flag`, `order_id`, `parent_id`) VALUES ('dept_type', '1', '正式部门', '部门类型-正式部门', '1', 1, NULL);
INSERT INTO `code_dict` (`code_type`, `key`, `value`, `code_desc`, `valid_flag`, `order_id`, `parent_id`) VALUES ('dept_type', '0', '临时部门', '部门类型-正式部门', '1', 2, NULL);

INSERT INTO `code_dict` (`code_type`, `key`, `value`, `code_desc`, `valid_flag`, `order_id`, `parent_id`) VALUES ('sex', '1', '男', '性别-男', '1', 1, NULL);
INSERT INTO `code_dict` (`code_type`, `key`, `value`, `code_desc`, `valid_flag`, `order_id`, `parent_id`) VALUES ('sex', '0', '女', '性别-女', '1', 2, NULL);

INSERT INTO `code_dict` (`code_type`, `key`, `value`, `code_desc`, `valid_flag`, `order_id`, `parent_id`) VALUES ('user_type', '1', '正式用户', '用户类型-正式用户', '1', 1, NULL);
INSERT INTO `code_dict` (`code_type`, `key`, `value`, `code_desc`, `valid_flag`, `order_id`, `parent_id`) VALUES ('user_type', '2', '临时用户', '用户类型-临时用户', '1', 2, NULL);


INSERT INTO `code_dict` (`code_type`, `key`, `value`, `code_desc`, `valid_flag`, `order_id`, `parent_id`) VALUES ('ldap_status', '0', '禁用', '用户类型-正式用户', '1', 1, NULL);
INSERT INTO `code_dict` (`code_type`, `key`, `value`, `code_desc`, `valid_flag`, `order_id`, `parent_id`) VALUES ('ldap_status', '1', '启用', '用户类型-临时用户', '1', 2, NULL);

