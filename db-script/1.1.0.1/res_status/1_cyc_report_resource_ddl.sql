DROP TABLE IF EXISTS `cyc_report_resource`;

CREATE TABLE `cyc_report_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `file_type` tinyint(1) NOT NULL COMMENT '数据文件类型',
  `ftp_path` varchar(200) NOT NULL COMMENT '数据文件的全路径',
  `file_path` varchar(200) NOT NULL COMMENT '数据文件的全路径',
  `file_name` varchar(100) NOT NULL COMMENT '数据文件名',
  `start_time` datetime DEFAULT NULL COMMENT '数据文件名中的数据开始时间',
  `session_id` varchar(50) DEFAULT NULL COMMENT '数据文件名中的sessionId',
  `request_id` int(11) NOT NULL COMMENT '请求消息的流水号',
  `read_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '数据文件读取状态, 0:未读, 1:已读',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `idx_cyc_rpt_res_start_time` (`start_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='上报东软指标文件表';