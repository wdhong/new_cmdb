/*Table structure for table `cm_backup_info` */

DROP TABLE IF EXISTS `cm_backup_info`;

CREATE TABLE `cm_backup_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `rc_id` varchar(64) NOT NULL COMMENT '所属资源池系统标识',
  `eqp_id` varchar(64) NOT NULL COMMENT '介质库标识',
  `eqp_name` varchar(64) NOT NULL COMMENT '介质库名称',
  `instance_id` varchar(30) DEFAULT NULL COMMENT '资源实例编码',
  `mediumlibrary_type` varchar(30) DEFAULT NULL COMMENT '介质库类型',
  `mediumlibrary_num` int(11) DEFAULT NULL COMMENT '介质库数目',
  `driver_ids` varchar(64) DEFAULT NULL COMMENT '驱动器标识列表',
  `driver_type` varchar(30) DEFAULT NULL COMMENT '驱动器类型',
  `driver_num` int(11) DEFAULT NULL COMMENT '驱动器数目',
  `mediumlibrary_capcity` int(11) DEFAULT NULL COMMENT '介质库配置容量',
  `datachannel_id` varchar(64) DEFAULT NULL COMMENT '数据通道标识列表',
  `datachannel_type` varchar(64) DEFAULT NULL COMMENT '数据通道类型',
  `datachannel_num` int(11) DEFAULT NULL COMMENT '数据通道数目',
  `medium_mcver` varchar(64) DEFAULT NULL COMMENT '介质库微码版本',
  `driver_mcver` varchar(64) DEFAULT NULL COMMENT '驱动器微码版本',
  `vendor_name` varchar(64) DEFAULT NULL COMMENT '设备厂商',
  `cur_status` tinyint(1) DEFAULT NULL COMMENT '当前状态, 0:正常运行 1:还是闲置 2:报废 3:坏',
  `eqp_serialnum` varchar(64) DEFAULT NULL COMMENT '设备序列号',
  `related_eqp_id` varchar(64) DEFAULT NULL COMMENT '依附主机ID',
  `backup_eqp_id` varchar(64) DEFAULT NULL COMMENT '备份服务器ID',
  `app_id` varchar(50) DEFAULT NULL COMMENT '所属业务系统',
  `asset_origin_type` tinyint(1) DEFAULT NULL COMMENT '资产来源分类, 1：自产 2：外购 3：借入 4：订单 5：第三方监控源 6：可自义',
  `asset_state` tinyint(1) DEFAULT NULL COMMENT '资产状态, 1：已使用 2：未使用 3：不可用 4：丢失 5：待确认 6：已删除',
  `asset_sla_type` tinyint(1) DEFAULT NULL COMMENT '资产SLA类型, 1：提供服务 2：平台自服务',
  PRIMARY KEY (`uu_id`),
  KEY `idx_cm_backup_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='备份-指标记录表';

/*Table structure for table `cm_bs_info` */

DROP TABLE IF EXISTS `cm_bs_info`;

CREATE TABLE `cm_bs_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `rc_id` varchar(64) NOT NULL COMMENT '所属资源池系统标识',
  `eqp_id` varchar(64) NOT NULL COMMENT '介质库标识',
  `eqp_name` varchar(64) NOT NULL COMMENT '介质库名称',
  `instance_id` varchar(30) DEFAULT NULL COMMENT '资源实例编码',
  `tier` tinyint(2) DEFAULT NULL COMMENT '存储性能级别,0为SSD盘， 1为FC盘，2为SATA盘，0级别最高、1级次之、以此类推',
  `vol_size` int(11) DEFAULT NULL COMMENT '卷大小',
  `lv_size` int(11) DEFAULT NULL COMMENT '卷条带深度',
  `lv_state` tinyint(1) DEFAULT NULL COMMENT '卷状态 1:active  2:inactive',
  `app_id` varchar(50) DEFAULT NULL COMMENT '所属业务系统',
  `vol_status` tinyint(1) DEFAULT NULL COMMENT '存储的状态 1-已创建 2-已挂载 3-创建失败 4-挂载失败 5-创建中 6-占用',
  `parent_id` varchar(128) DEFAULT NULL COMMENT '挂载资源ID',
  `sa_id` varchar(30) DEFAULT NULL COMMENT '所属块存储设备',
  PRIMARY KEY (`uu_id`),
  KEY `idx_cm_bs_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='卷信息-指标记录表';

/*Table structure for table `cm_fs_info` */

DROP TABLE IF EXISTS `cm_fs_info`;

CREATE TABLE `cm_fs_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `rc_id` varchar(64) NOT NULL COMMENT '所属资源池系统标识',
  `eqp_id` varchar(64) NOT NULL COMMENT '主机资源唯一标识',
  `eqp_name` varchar(64) NOT NULL COMMENT '主机名称',
  `fs_size` int(11) DEFAULT NULL COMMENT '存储大小',
  `node_num` int(11) DEFAULT NULL COMMENT '节点数量',
  `fs_state` tinyint(1) DEFAULT NULL COMMENT '资源存储状态： 0-可用 1-不可用',
  `isassigned` tinyint(1) DEFAULT NULL COMMENT '是否已分配, 存储分配状态（只要有业务系统申请，则表示已分配）：0-未分配 1-已分配',
  `app_ids` varchar(128) DEFAULT NULL COMMENT '所属业务系统列表',
  `related_eqp_ids` text COMMENT '对应物理机ID列表',
  `asset_state` tinyint(1) DEFAULT NULL COMMENT '资产状态 1：已使用 2：未使用 3：不可用 4：丢失 5：待确认 6：已删除',
  `asset_sla_type` tinyint(1) DEFAULT NULL COMMENT '资产SLA类型: 1：提供服务 2：平台自服务',
  `asset_origin_type` tinyint(1) DEFAULT NULL COMMENT '资产来源分类: 1：自产 2：外购 3：借入 4：订单 5：第三方监控源 6：可自义',
  `vendor_name` varchar(64) DEFAULT NULL COMMENT '厂家名称',
  `dfs_root` varchar(64) DEFAULT NULL COMMENT '分布式文件系统根用户',
  PRIMARY KEY (`uu_id`),
  KEY `idx_cm_fs_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分布式文件系统-指标记录表';

/*Table structure for table `cm_raid_info` */

DROP TABLE IF EXISTS `cm_raid_info`;

CREATE TABLE `cm_raid_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `rc_id` varchar(64) NOT NULL COMMENT '所属资源池系统标识',
  `eqp_id` varchar(64) NOT NULL COMMENT '主机资源唯一标识',
  `eqp_name` varchar(64) NOT NULL COMMENT '主机名称',
  `vendor_name` varchar(30) DEFAULT NULL COMMENT '厂家名称',
  `sa_type` varchar(30) DEFAULT NULL COMMENT '存储阵列类型',
  `sa_microcode_ver` varchar(30) DEFAULT NULL COMMENT '存储微码版本',
  `sa_capacity` int(11) DEFAULT NULL COMMENT '存储配置容量',
  `sa_ip` varchar(50) DEFAULT NULL COMMENT '阵列IP地址',
  `related_eqp_id` varchar(64) DEFAULT NULL COMMENT '依附的设备ID列表',
  `cache_capacity` float DEFAULT NULL COMMENT '存储CACHE容量',
  `disk_ids` varchar(30) DEFAULT NULL COMMENT '磁盘标识列表',
  `disk_specification` varchar(30) DEFAULT NULL COMMENT '磁盘的规格',
  `hba_ids` varchar(30) DEFAULT NULL COMMENT '主机通道卡标识列表',
  `hba_types` varchar(30) DEFAULT NULL COMMENT '主机通道卡类型',
  `hba_num` int(11) DEFAULT NULL COMMENT '主机通道卡数目',
  `disk_adaptor_id` varchar(30) DEFAULT NULL COMMENT '磁盘适配卡标识列表',
  `disk_adaptor_type` varchar(30) DEFAULT NULL COMMENT '磁盘适配卡类型',
  `eqp_serialnum` varchar(30) DEFAULT NULL COMMENT '设备序列号',
  `asset_state` tinyint(1) DEFAULT NULL COMMENT '资产状态 1：已使用 2：未使用 3：不可用 4：丢失 5：待确认 6：已删除',
  `asset_sla_type` tinyint(1) DEFAULT NULL COMMENT '资产SLA类型: 1：提供服务 2：平台自服务',
  PRIMARY KEY (`uu_id`),
  KEY `idx_cm_raid_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='块存储-指标记录表';

/*Table structure for table `cm_resourcepool_info` */

DROP TABLE IF EXISTS `cm_resourcepool_info`;

CREATE TABLE `cm_resourcepool_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `rc_id` varchar(50) NOT NULL COMMENT '资源池的唯一标识',
  `rc_name` varchar(64) NOT NULL COMMENT '描述资源池名称',
  `rc_desc` varchar(128) NOT NULL COMMENT '资源池的描述信息',
  `rc_status` tinyint(1) DEFAULT NULL COMMENT '"描述资源池状态，包括: 0:正常 1:暂停服务 2:终止服务 3:响应错误"',
  `run_time` time DEFAULT NULL COMMENT '建设时间,指资源池系统投入生产运行时间',
  `vendor_name` varchar(50) DEFAULT NULL COMMENT '系统集成商',
  `total_bandwidth` float DEFAULT NULL COMMENT '总带宽: 资源池可提供总带宽（针对公网IP），单位Mbps',
  PRIMARY KEY (`uu_id`),
  KEY `idx_cm_resourcepool_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资源池系统-指标记录表';

/*Table structure for table `cm_srv_info` */

DROP TABLE IF EXISTS `cm_srv_info`;

CREATE TABLE `cm_srv_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `rc_id` varchar(64) NOT NULL COMMENT '所属资源池系统标识',
  `eqp_id` varchar(64) NOT NULL COMMENT '主机资源唯一标识',
  `eqp_name` varchar(64) NOT NULL COMMENT '主机名称',
  `eqp_ip` varchar(50) NOT NULL COMMENT '主机IP地址',
  `eqp_netmask` varchar(50) DEFAULT NULL COMMENT '网络掩码',
  `instance_id` varchar(64) DEFAULT NULL COMMENT '资源实例编码',
  `server_type` varchar(64) DEFAULT NULL COMMENT '主机型号',
  `cpu_num` smallint(6) DEFAULT NULL COMMENT '主机CPU个数',
  `cpu_type` varchar(128) DEFAULT NULL COMMENT '主机CPU类型',
  `cpu_frequency` float DEFAULT NULL COMMENT '主机CPU主频',
  `nuc_num_per_cpu` tinyint(1) DEFAULT NULL COMMENT '主机单CPU核数',
  `memory_size` int(11) DEFAULT NULL COMMENT '主机内存总容量大小',
  `memory_num` tinyint(2) DEFAULT NULL COMMENT '主机内存条数量',
  `os_type` varchar(64) DEFAULT NULL COMMENT '操作系统标识',
  `disk_size` int(11) DEFAULT NULL COMMENT '操作系统磁盘空间',
  `datadisk_size` int(11) DEFAULT NULL COMMENT '数据盘磁盘空间',
  `disk_num` tinyint(2) DEFAULT NULL COMMENT '内置硬盘的个数',
  `if_num` tinyint(2) DEFAULT NULL COMMENT '系统网络接口总数',
  `mainbord_num` tinyint(2) DEFAULT NULL COMMENT '主板数量',
  `powermodule_num` tinyint(2) DEFAULT NULL COMMENT '电源模块数量',
  `fan_num` tinyint(2) DEFAULT NULL COMMENT '风扇数量',
  `vendor_name` varchar(20) DEFAULT NULL COMMENT '设备厂家',
  `run_time` datetime DEFAULT NULL COMMENT '投入生产运行的时间',
  `cur_status` tinyint(1) DEFAULT NULL COMMENT '当前状态， "X86物理机状态，0：不可用 1：已加电 2：运行 3：关机 4：休眠 5：处理中"',
  `eqp_serialnum` varchar(64) DEFAULT NULL COMMENT '主机设备的序列号',
  `app_id` varchar(50) DEFAULT NULL COMMENT '所属业务系统',
  `zone_id` varchar(50) DEFAULT NULL COMMENT '所属分区',
  `ethada_num` tinyint(2) DEFAULT NULL COMMENT '网卡个数',
  `ethada_type` varchar(64) DEFAULT NULL COMMENT '网卡规格',
  `hba_num` tinyint(2) DEFAULT NULL COMMENT 'HBA卡个数',
  `hba_type` varchar(20) DEFAULT NULL COMMENT 'HBA卡规格',
  `used_flag` tinyint(1) DEFAULT NULL COMMENT '应用标志, 1：X86物理机，单机提供计算资源。 2、虚拟化，虚拟机的宿主机。3、用于存储集群。',
  `hv_info` varchar(30) DEFAULT NULL COMMENT '虚拟化软件信息',
  `hvpool_id` varchar(30) DEFAULT NULL COMMENT '虚拟化集群标识',
  `switch_if_relations` varchar(64) DEFAULT NULL COMMENT '连接到的交换机及端口标识',
  `cabinet_id` varchar(30) DEFAULT NULL COMMENT '所属整机柜名称',
  `asset_origin_type` tinyint(1) DEFAULT NULL COMMENT '资产来源分类, 1：自产 2：外购 3：借入 4：订单 5：第三方监控源 6：可自义',
  `asset_state` tinyint(1) DEFAULT NULL COMMENT '资产状态, 1：已使用 2：未使用 3：不可用 4：丢失 5：待确认 6：已删除',
  `asset_sla_type` tinyint(1) DEFAULT NULL COMMENT '资产SLA类型, 1：提供服务 2：平台自服务',
  PRIMARY KEY (`uu_id`),
  KEY `idx_cm_srv_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='X86物理机-指标记录表';

/*Table structure for table `cm_switch_if_info` */

DROP TABLE IF EXISTS `cm_switch_if_info`;

CREATE TABLE `cm_switch_if_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `rc_id` varchar(64) NOT NULL COMMENT '所属资源池系统标识',
  `eqp_id` varchar(64) NOT NULL COMMENT '介质库标识',
  `eqp_name` varchar(64) NOT NULL COMMENT '介质库名称',
  `parent_id` varchar(64) DEFAULT NULL COMMENT '所属交换机',
  `if_description` varchar(30) DEFAULT NULL COMMENT '端口描述',
  `if_status` varchar(10) DEFAULT NULL COMMENT '端口状态',
  `if_type` varchar(20) DEFAULT NULL COMMENT '端口类型',
  `if_set_mac_addr` varchar(50) DEFAULT NULL COMMENT '端口已配置要绑定的MAC地址',
  `if_real_mac_addr` varchar(50) DEFAULT NULL COMMENT '实际连接到的设备的MAC地址',
  `if_connect_eqp_ip` varchar(50) DEFAULT NULL COMMENT '端口所连接设备的IP地址',
  `vlan_id` varchar(50) DEFAULT NULL COMMENT 'VLAN',
  `if_speed` float DEFAULT NULL COMMENT '端口速率',
  `dest_if_id` varchar(50) DEFAULT NULL COMMENT '端口所连对端端口唯一标识',
  PRIMARY KEY (`uu_id`),
  KEY `idx_cm_switch_if_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='交换机端口信息-指标记录表';

/*Table structure for table `cm_switch_info` */

DROP TABLE IF EXISTS `cm_switch_info`;

CREATE TABLE `cm_switch_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `rc_id` varchar(64) NOT NULL COMMENT '所属资源池系统标识',
  `eqp_id` varchar(64) NOT NULL COMMENT '介质库标识',
  `eqp_name` varchar(64) NOT NULL COMMENT '介质库名称',
  `switch_type` varchar(64) DEFAULT NULL COMMENT '交换机型号',
  `sw_version` varchar(30) DEFAULT NULL COMMENT '设备软件版本',
  `vendor_name` varchar(30) DEFAULT NULL COMMENT '网元设备厂家',
  `switch_ip` varchar(50) DEFAULT NULL COMMENT '网元IP地址',
  `cur_status` tinyint(1) DEFAULT NULL COMMENT '当前状态, 0：正常运行 1：还是闲置 2：报废 3：坏',
  `switch_serialnum` varchar(50) DEFAULT NULL COMMENT '设备序列号',
  `asset_origin_type` tinyint(1) DEFAULT NULL COMMENT '资产来源分类, "1：自产 2：外购 3：借入 4：订单 5：第三方监控源 6：可自义',
  `asset_state` tinyint(1) DEFAULT NULL COMMENT '资产状态, 1：已使用 2：未使用 3：不可用 4：丢失 5：待确认 6：已删除',
  `asset_sla_type` tinyint(1) DEFAULT NULL COMMENT '资产SLA类型, 1：提供服务 2：平台自服务',
  `switch_mold` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`uu_id`),
  KEY `idx_cm_switch_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='交换机-指标记录表';

/*Table structure for table `cm_vm_info` */

DROP TABLE IF EXISTS `cm_vm_info`;

CREATE TABLE `cm_vm_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `rc_id` varchar(64) NOT NULL COMMENT '所属资源池系统标识',
  `eqp_name` varchar(64) NOT NULL COMMENT '虚拟机名称',
  `instance_id` varchar(64) NOT NULL COMMENT '资源实例编码',
  `vm_ip` varchar(50) DEFAULT NULL COMMENT '虚拟机IP地址',
  `vm_netmask` varchar(50) DEFAULT NULL COMMENT '网络掩码',
  `vm_mac` varchar(50) DEFAULT NULL COMMENT '虚拟网卡MAC地址',
  `vm-vlan` varchar(50) DEFAULT NULL COMMENT 'VLAN信息',
  `cluster_ip` varchar(50) DEFAULT NULL COMMENT '绑定的cluster浮动IP地址',
  `cpu_num` smallint(6) DEFAULT NULL COMMENT '虚拟CPU数量',
  `cpu_frequency` decimal(5,3) DEFAULT NULL COMMENT 'CPU主频',
  `memory_size` int(11) DEFAULT NULL COMMENT 'CPU主频',
  `os_type` tinyint(2) DEFAULT NULL COMMENT '虚拟磁盘位置',
  `disk_size` int(11) DEFAULT NULL COMMENT '虚拟磁盘系统盘容量',
  `datadisk_size` int(11) DEFAULT NULL COMMENT '虚拟磁盘数据盘容量',
  `run_time` datetime DEFAULT NULL COMMENT '投入生产运行的时间',
  `cur_status` tinyint(1) DEFAULT NULL COMMENT '当前状态',
  `app_id` varchar(50) DEFAULT NULL COMMENT '所属业务系统',
  `zone_id` varchar(50) DEFAULT NULL COMMENT '所属分区',
  `v_ethada_num` smallint(6) DEFAULT NULL COMMENT '虚拟网卡个数',
  `v_fchba_num` smallint(6) DEFAULT NULL COMMENT '虚拟FC-HBA卡的个数',
  `vm_os` varchar(50) DEFAULT NULL COMMENT '操作系统标识',
  `parent_id` varchar(50) DEFAULT NULL COMMENT '所属物理机ID',
  `switch_if_relations` varchar(50) DEFAULT NULL COMMENT '连接到的交换机及端口标识',
  `max_netband` decimal(5,2) DEFAULT NULL COMMENT '网络带宽上限',
  PRIMARY KEY (`uu_id`),
  KEY `idx_cm_vm_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='虚拟机-指标记录表';

/*Table structure for table `pm_backup_info` */

DROP TABLE IF EXISTS `pm_backup_info`;

CREATE TABLE `pm_backup_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `eqp_id` varchar(64) NOT NULL COMMENT '主机资源唯一标识',
  `start_time` datetime DEFAULT NULL COMMENT '数据开始时间',
  `stop_time` datetime DEFAULT NULL COMMENT '数据结束时间',
  `backup_cpu_per` float DEFAULT NULL COMMENT '备份CPU占用率',
  `backup_mem_per` float DEFAULT NULL COMMENT '备份内存占用率',
  `ml_remain` float DEFAULT NULL COMMENT '介质（池）的剩余空间',
  `ml_total` float DEFAULT NULL COMMENT '介质（池）的总空间',
  `ml_remain_per` float DEFAULT NULL COMMENT '介质（池）的剩余空间百分比',
  `backup_userquanti` float DEFAULT NULL COMMENT '用户备份数据量',
  `backup_quantity` float DEFAULT NULL COMMENT '备份作业数据量',
  `backup_time` int(11) DEFAULT NULL COMMENT '备份作业时长,分钟',
  `backup_datatype` tinyint(4) DEFAULT NULL COMMENT '备份数据类型',
  `med_cpu_per` float DEFAULT NULL COMMENT '介质服务CPU占用率',
  `med_mem_per` float DEFAULT NULL COMMENT '介质服务内存占用率',
  PRIMARY KEY (`uu_id`),
  KEY `idx_pm_backup_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='备份-指标记录表';

/*Table structure for table `pm_bs_info` */

DROP TABLE IF EXISTS `pm_bs_info`;

CREATE TABLE `pm_bs_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `start_time` datetime DEFAULT NULL COMMENT '数据开始时间',
  `stop_time` datetime DEFAULT NULL COMMENT '数据结束时间',
  `instance_id` varchar(64) DEFAULT NULL COMMENT '设备唯一标识',
  `lv_work_state` int(11) DEFAULT NULL COMMENT '每秒读字次',
  `lv_write_rate` int(11) DEFAULT NULL COMMENT '每秒写字次',
  `lv_read_bytes` int(11) DEFAULT NULL COMMENT '卷读字节数',
  `lv_write_bytes` int(11) DEFAULT NULL COMMENT '卷写字节数',
  `lv_avg_read_time` int(11) DEFAULT NULL COMMENT '卷平均读时间',
  `lv_avg_write_time` int(11) DEFAULT NULL COMMENT '卷平均写时间',
  `lv_read_fail_times` int(11) DEFAULT NULL COMMENT '卷读失败数',
  `lv_write_fail_times` int(11) DEFAULT NULL COMMENT '卷写失败数',
  PRIMARY KEY (`uu_id`),
  KEY `idx_pm_bs_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='储存阵列(卷)-指标记录表';

/*Table structure for table `pm_dfs_info` */

DROP TABLE IF EXISTS `pm_dfs_info`;

CREATE TABLE `pm_dfs_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `fs_id` varchar(64) NOT NULL COMMENT '文件系统标识',
  `fs_name` varchar(64) NOT NULL COMMENT '文件系统名称',
  `fs_used_per` float DEFAULT NULL COMMENT '文件系统使用比率',
  `fs_sp_free` float DEFAULT NULL COMMENT '可用空间',
  `fs_sp_used` float DEFAULT NULL COMMENT '已用空间',
  PRIMARY KEY (`uu_id`),
  KEY `idx_pm_dfs_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `pm_disk_info` */

DROP TABLE IF EXISTS `pm_disk_info`;

CREATE TABLE `pm_disk_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `eqp_id` varchar(64) DEFAULT NULL COMMENT '主机资源唯一标识',
  `disk_id` varchar(64) NOT NULL COMMENT '磁盘标识',
  `disk_name` varchar(64) NOT NULL COMMENT '磁盘名称',
  `disk_type` tinyint(1) DEFAULT NULL COMMENT '磁盘外挂标识, 1：本地磁盘；2：外挂磁盘；',
  `disk_busy_per` float DEFAULT NULL COMMENT '磁盘忙的百分比',
  `io_wait` float DEFAULT NULL COMMENT 'I/O操作等待时间',
  `io_queue` float DEFAULT NULL COMMENT '平均I/O队列长度',
  `disk_read` float DEFAULT NULL COMMENT '每秒磁盘读请求',
  `disk_write` float DEFAULT NULL COMMENT '每秒磁盘写请求',
  PRIMARY KEY (`uu_id`),
  KEY `idx_pm_disk_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='宿主机、整机柜-指标记录表';

/*Table structure for table `pm_firewall_info` */

DROP TABLE IF EXISTS `pm_firewall_info`;

CREATE TABLE `pm_firewall_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `eqp_id` varchar(64) NOT NULL COMMENT '防火墙唯一标识',
  `start_time` datetime DEFAULT NULL COMMENT '数据开始时间',
  `stop_time` datetime DEFAULT NULL COMMENT '数据结束时间',
  `cpu_used_per` float DEFAULT NULL COMMENT 'CPU利用率',
  `mem_used_per` float DEFAULT NULL COMMENT '内存利用率',
  `open_time` datetime DEFAULT NULL COMMENT '设备开启时间',
  `run_time` float DEFAULT NULL COMMENT '设备运行时间',
  `concurrent_connect_num` int(11) DEFAULT NULL COMMENT '并发链接数',
  `new_connect_num` int(11) DEFAULT NULL COMMENT '新建链接数',
  `throughput` float DEFAULT NULL COMMENT '吞吐量',
  PRIMARY KEY (`uu_id`),
  KEY `idx_pm_firewall_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='防火墙-指标记录表';

/*Table structure for table `pm_fs_app_info` */

DROP TABLE IF EXISTS `pm_fs_app_info`;

CREATE TABLE `pm_fs_app_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `eqp_id` varchar(64) NOT NULL COMMENT '防火墙唯一标识',
  `start_time` datetime DEFAULT NULL COMMENT '数据开始时间',
  `stop_time` datetime DEFAULT NULL COMMENT '数据结束时间',
  `app_id` varchar(50) DEFAULT NULL COMMENT '所属业务系统',
  `used_space` int(11) DEFAULT NULL COMMENT '已用容量',
  `free_space` int(11) DEFAULT NULL COMMENT '可用容量',
  `used_space_per` float DEFAULT NULL COMMENT '文件系统使用比率',
  `used_file` int(11) DEFAULT NULL COMMENT '已用文件数',
  `total_file` int(11) DEFAULT NULL COMMENT '总文件数',
  PRIMARY KEY (`uu_id`),
  KEY `idx_pm_fs_app_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='应用级-指标记录表';

/*Table structure for table `pm_fs_node_info` */

DROP TABLE IF EXISTS `pm_fs_node_info`;

CREATE TABLE `pm_fs_node_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `eqp_id` varchar(64) DEFAULT NULL COMMENT '防火墙唯一标识',
  `start_time` datetime DEFAULT NULL COMMENT '数据开始时间',
  `stop_time` datetime DEFAULT NULL COMMENT '数据结束时间',
  `used_cpu_per` float DEFAULT NULL COMMENT 'CPU占用率',
  `used_mem_per` float DEFAULT NULL COMMENT '内存占用率',
  `read_throughput` float DEFAULT NULL COMMENT '读带宽',
  `write_throughput` float DEFAULT NULL COMMENT '写带宽',
  `read_iops` int(11) DEFAULT NULL COMMENT '读IOPS',
  `write_iops` int(11) DEFAULT NULL COMMENT '写IOPS',
  `srv_eqp_id` varchar(64) NOT NULL COMMENT '节点标识',
  `used_space` int(11) DEFAULT NULL COMMENT '已用空间',
  `free_space` int(11) DEFAULT NULL COMMENT '可用空间',
  `used_space_per` float DEFAULT NULL COMMENT '文件系统使用比率',
  PRIMARY KEY (`uu_id`),
  KEY `idx_pm_fs_node_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='节点级-指标记录表';

/*Table structure for table `pm_fs_sys_info` */

DROP TABLE IF EXISTS `pm_fs_sys_info`;

CREATE TABLE `pm_fs_sys_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `eqp_id` varchar(64) DEFAULT NULL COMMENT '防火墙唯一标识',
  `start_time` datetime DEFAULT NULL COMMENT '数据开始时间',
  `stop_time` datetime DEFAULT NULL COMMENT '数据结束时间',
  `used_space` int(11) DEFAULT NULL COMMENT '已用空间',
  `free_space` int(11) DEFAULT NULL COMMENT '可用空间',
  `used_space_per` float DEFAULT NULL COMMENT '文件系统使用比率',
  `used_cpu_per` float DEFAULT NULL COMMENT 'CPU占用率',
  `used_mem_per` float DEFAULT NULL COMMENT '内存占用率',
  `read_throughput` float DEFAULT NULL COMMENT '读带宽',
  `write_throughput` float DEFAULT NULL COMMENT '写带宽',
  `read_iops` int(11) DEFAULT NULL COMMENT '读IOPS',
  `write_iops` int(11) DEFAULT NULL COMMENT '写IOPS',
  `used_file` int(11) DEFAULT NULL COMMENT '已用文件数',
  `total_file` int(11) DEFAULT NULL COMMENT '总文件数',
  PRIMARY KEY (`uu_id`),
  KEY `idx_pm_fs_sys_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='节点级-指标记录表';

/*Table structure for table `pm_ids_info` */

DROP TABLE IF EXISTS `pm_ids_info`;

CREATE TABLE `pm_ids_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `eqp_id` varchar(64) DEFAULT NULL COMMENT '防火墙唯一标识',
  `start_time` datetime DEFAULT NULL COMMENT '数据开始时间',
  `stop_time` datetime DEFAULT NULL COMMENT '数据结束时间',
  `throughput` int(11) DEFAULT NULL COMMENT '吞吐量',
  `connect_num` int(11) DEFAULT NULL COMMENT '并发链接数',
  `new_connec_tnum` int(11) DEFAULT NULL COMMENT '新建链接数',
  `process_cpu_per` float DEFAULT NULL COMMENT 'CPU占用率',
  `if_in_ucast_pkts` float DEFAULT NULL COMMENT '端口入方向单播包数',
  PRIMARY KEY (`uu_id`),
  KEY `idx_pm_ids_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='入侵检测设备-指标记录表';

/*Table structure for table `pm_loadbalance_info` */

DROP TABLE IF EXISTS `pm_loadbalance_info`;

CREATE TABLE `pm_loadbalance_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `eqp_id` varchar(64) DEFAULT NULL COMMENT '防火墙唯一标识',
  `start_time` datetime DEFAULT NULL COMMENT '数据开始时间',
  `stop_time` datetime DEFAULT NULL COMMENT '数据结束时间',
  `throughput` int(11) DEFAULT NULL COMMENT '吞吐量',
  `connect_num` int(11) DEFAULT NULL COMMENT '并发链接数',
  `new_connect_num` int(11) DEFAULT NULL COMMENT '新建链接数',
  `process_cpu_per` float DEFAULT NULL COMMENT 'CPU占用率',
  `if_out_discards` float DEFAULT NULL COMMENT '出丢包数',
  `if_in_discards` float DEFAULT NULL COMMENT '入丢包数',
  `if_out_ucast_pkts` float DEFAULT NULL COMMENT '出单播包数',
  `if_in_ucast_pkts` float DEFAULT NULL COMMENT '入单播包数',
  `if_out_broadcast_pkts` float DEFAULT NULL COMMENT '出广播包数',
  `if_in_broadcast_pkts` float DEFAULT NULL COMMENT '入广播包数',
  `if_out_multicast_pkts` float DEFAULT NULL COMMENT '出组播包数',
  `if_in_multicast_pkts` float DEFAULT NULL COMMENT '入组播包数',
  PRIMARY KEY (`uu_id`),
  KEY `idx_pm_loadbalance_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='节点级-指标记录表';

/*Table structure for table `pm_ls_parc_info` */

DROP TABLE IF EXISTS `pm_ls_parc_info`;

CREATE TABLE `pm_ls_parc_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `start_time` datetime DEFAULT NULL COMMENT '数据开始时间',
  `stop_time` datetime DEFAULT NULL COMMENT '数据结束时间',
  `soft_id` varchar(32) DEFAULT NULL,
  `total_jobs` int(11) DEFAULT NULL,
  `complete_jobs` int(11) DEFAULT NULL,
  `fail_jobs` int(11) DEFAULT NULL,
  `run_jobs` int(11) DEFAULT NULL,
  `total_map` int(11) DEFAULT NULL,
  `total_reduce` int(11) DEFAULT NULL,
  `complete_map` int(11) DEFAULT NULL,
  `complete_reduce` int(11) DEFAULT NULL,
  `run_map` int(11) DEFAULT NULL,
  `run_reduce` int(11) DEFAULT NULL,
  `avg_time_map` int(11) DEFAULT NULL,
  `avg_time_reduce` int(11) DEFAULT NULL,
  `min_time_map` int(11) DEFAULT NULL,
  `min_time_reduce` int(11) DEFAULT NULL,
  `max_time_map` int(11) DEFAULT NULL,
  `max_time_reduce` int(11) DEFAULT NULL,
  `fail_map` int(11) DEFAULT NULL,
  `fail_reduce` int(11) DEFAULT NULL,
  `suc_map` int(11) DEFAULT NULL,
  `sum_reduce` int(11) DEFAULT NULL,
  PRIMARY KEY (`uu_id`),
  KEY `idx_pm_ls_parc_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `pm_ls_stru_info` */

DROP TABLE IF EXISTS `pm_ls_stru_info`;

CREATE TABLE `pm_ls_stru_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `start_time` datetime DEFAULT NULL COMMENT '数据开始时间',
  `stop_time` datetime DEFAULT NULL COMMENT '数据结束时间',
  `soft_id` varchar(32) DEFAULT NULL,
  `total_table_space_size` int(11) DEFAULT NULL,
  `used_table_space_size` int(11) DEFAULT NULL,
  `used_table_per` float DEFAULT NULL,
  `cur_connections` int(11) DEFAULT NULL,
  `total_connects` int(11) DEFAULT NULL,
  `aborted_connects` int(11) DEFAULT NULL,
  `sql_num` int(11) DEFAULT NULL,
  PRIMARY KEY (`uu_id`),
  KEY `idx_pm_ls_stru_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `pm_ls_unstru_node_info` */

DROP TABLE IF EXISTS `pm_ls_unstru_node_info`;

CREATE TABLE `pm_ls_unstru_node_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `start_time` datetime DEFAULT NULL COMMENT '数据开始时间',
  `stop_time` datetime DEFAULT NULL COMMENT '数据结束时间',
  `srv_eqp_id` varchar(64) DEFAULT NULL,
  `soft_id` varchar(32) DEFAULT NULL,
  `total_capacity` int(11) DEFAULT NULL,
  `remaining_capacity` int(11) DEFAULT NULL,
  `used_capacity` int(11) DEFAULT NULL,
  `used_capacity_per` float DEFAULT NULL,
  `up_throughput` float DEFAULT NULL,
  `down_throughput` float DEFAULT NULL,
  `used_cpu_per` float DEFAULT NULL,
  `used_mem_per` float DEFAULT NULL,
  `used_mem_cap` int(11) DEFAULT NULL,
  `io_read` int(11) DEFAULT NULL,
  `io_write` int(11) DEFAULT NULL,
  PRIMARY KEY (`uu_id`),
  KEY `idx_pm_ls_unstru_node_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `pm_ls_unstru_sys_info` */

DROP TABLE IF EXISTS `pm_ls_unstru_sys_info`;

CREATE TABLE `pm_ls_unstru_sys_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `start_time` datetime DEFAULT NULL COMMENT '数据开始时间',
  `stop_time` datetime DEFAULT NULL COMMENT '数据结束时间',
  `soft_id` varchar(32) DEFAULT NULL,
  `total_capacity` int(11) DEFAULT NULL,
  `remaining_capacity` int(11) DEFAULT NULL,
  `used_capacity` int(11) DEFAULT NULL,
  `used_capacity_per` float DEFAULT NULL,
  `up_throughput` float DEFAULT NULL,
  `down_throughput` float DEFAULT NULL,
  `used_cpu_per` float DEFAULT NULL,
  `used_mem_per` float DEFAULT NULL,
  `used_mem_cap` int(11) DEFAULT NULL,
  `io_read` int(11) DEFAULT NULL,
  `io_write` int(11) DEFAULT NULL,
  PRIMARY KEY (`uu_id`),
  KEY `idx_pm_ls_unstru_sys_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `pm_process_info` */

DROP TABLE IF EXISTS `pm_process_info`;

CREATE TABLE `pm_process_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `process_name` varchar(32) DEFAULT NULL COMMENT '进程名',
  `process_status` varchar(20) DEFAULT NULL COMMENT '进程状态',
  `process_user` varchar(32) DEFAULT NULL COMMENT '进程用户',
  `pro_cpu_time` float DEFAULT NULL COMMENT '占用CPU时间',
  `pro_commond` varchar(128) DEFAULT NULL COMMENT '进程指令行',
  `pro_start_time` datetime DEFAULT NULL COMMENT '进程开始时间',
  `pro_mem` float DEFAULT NULL COMMENT '进程的规模',
  PRIMARY KEY (`uu_id`),
  KEY `idx_pm_process_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统-指标记录表';

/*Table structure for table `pm_raid_info` */

DROP TABLE IF EXISTS `pm_raid_info`;

CREATE TABLE `pm_raid_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `start_time` datetime DEFAULT NULL COMMENT '数据开始时间',
  `stop_time` datetime DEFAULT NULL COMMENT '数据结束时间',
  `eqp_id` varchar(64) NOT NULL COMMENT '设备唯一标识',
  `cach_read_hr` float DEFAULT NULL COMMENT 'CACHE读命中率',
  `cach_writed_hr` float DEFAULT NULL COMMENT 'CACHE写命中率',
  `hst_disk_read_rate` int(11) DEFAULT NULL COMMENT '读速率',
  `hst_disk_write_rate` int(11) DEFAULT NULL COMMENT '写速率',
  `hst_disk_read_bytes` int(11) DEFAULT NULL COMMENT '读吞吐',
  `hst_disk_write_bytes` int(11) DEFAULT NULL COMMENT '写吞吐',
  `hst_disk_read_avg_time` int(11) DEFAULT NULL COMMENT '读平均时间',
  `hst_disk_write_avg_time` int(11) DEFAULT NULL COMMENT '写平均时间',
  PRIMARY KEY (`uu_id`),
  KEY `idx_pm_raid_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='阵列-指标记录表';

/*Table structure for table `pm_router_if_info` */

DROP TABLE IF EXISTS `pm_router_if_info`;

CREATE TABLE `pm_router_if_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `eqp_id` varchar(64) NOT NULL COMMENT '主机资源唯一标识',
  `start_time` datetime DEFAULT NULL COMMENT '数据开始时间',
  `stop_time` datetime DEFAULT NULL COMMENT '数据结束时间',
  `parent_id` varchar(50) DEFAULT NULL COMMENT '所属物理机ID',
  `if_speed` float DEFAULT NULL COMMENT '接口带宽',
  `if_in_octets` float DEFAULT NULL COMMENT '入字节数',
  `if_in_ucast_pkts` float DEFAULT NULL COMMENT '入单播包数',
  `if_in_discards` float DEFAULT NULL COMMENT '入丢包数',
  `if_in_errors` float DEFAULT NULL COMMENT '入错包数',
  `if_out_octets` float DEFAULT NULL COMMENT '出字节数',
  `if_out_ucast_pkts` float DEFAULT NULL COMMENT '出单播包数',
  `if_out_discards` float DEFAULT NULL COMMENT '出丢包数',
  `if_out_errors` float DEFAULT NULL COMMENT '出错报数',
  `if_in_multicast_pkts` float DEFAULT NULL COMMENT '入组播包数',
  `if_in_broadcast_pkts` float DEFAULT NULL COMMENT '入广播包数',
  `if_out_multicast_pkts` float DEFAULT NULL COMMENT '出组播包数',
  `if_out_broadcast_pkts` float DEFAULT NULL COMMENT '出广播包数',
  PRIMARY KEY (`uu_id`),
  KEY `idx_pm_router_if_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='端口-指标记录表';

/*Table structure for table `pm_router_info` */

DROP TABLE IF EXISTS `pm_router_info`;

CREATE TABLE `pm_router_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `start_time` datetime DEFAULT NULL COMMENT '数据开始时间',
  `stop_time` datetime DEFAULT NULL COMMENT '数据结束时间',
  `eqp_id` varchar(64) DEFAULT NULL COMMENT '主机资源唯一标识',
  `cpu_used_per` float DEFAULT NULL COMMENT 'CPU 使用率',
  `mem_used_per` float DEFAULT NULL COMMENT '内存 使用率',
  `cache_used_per` float DEFAULT NULL COMMENT '缓存 使用率',
  PRIMARY KEY (`uu_id`),
  KEY `idx_pm_router_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `pm_srv_info` */

DROP TABLE IF EXISTS `pm_srv_info`;

CREATE TABLE `pm_srv_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `start_time` datetime DEFAULT NULL COMMENT '数据开始时间',
  `stop_time` datetime DEFAULT NULL COMMENT '数据结束时间',
  `eqp_id` varchar(64) DEFAULT NULL COMMENT '主机资源唯一标识',
  `cpu_sys_time` float DEFAULT NULL COMMENT 'CPU时间：系统百分比',
  `cpu_user_time` float DEFAULT NULL COMMENT 'CPU时间：用户百分比',
  `cpu_idle_time` float DEFAULT NULL COMMENT 'CPU时间：等待百分比',
  `processor_utilization` float DEFAULT NULL COMMENT 'CPU利用率',
  `mem_used_per` float DEFAULT NULL COMMENT '内存的使用率',
  `mem_free` int(11) DEFAULT NULL COMMENT '可用内存量',
  `disk_used_per` float DEFAULT NULL COMMENT '磁盘空间占用百分比',
  `net_in_speed` float DEFAULT NULL COMMENT '网络input速率',
  `net_out_speed` float DEFAULT NULL COMMENT '网络output速率',
  `sys_mem_used_per` float DEFAULT NULL COMMENT '系统内存使用率',
  `user_mem_used_per` float DEFAULT NULL COMMENT '用户内存使用率',
  PRIMARY KEY (`uu_id`),
  KEY `idx_pm_srv_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='宿主机-指标记录表';

/*Table structure for table `pm_switch_if_info` */

DROP TABLE IF EXISTS `pm_switch_if_info`;

CREATE TABLE `pm_switch_if_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `start_time` datetime DEFAULT NULL COMMENT '数据开始时间',
  `stop_time` datetime DEFAULT NULL COMMENT '数据结束时间',
  `eqp_id` varchar(64) DEFAULT NULL COMMENT '主机资源唯一标识',
  `parent_id` varchar(50) DEFAULT NULL COMMENT '所属物理机ID',
  `if_speed` float DEFAULT NULL COMMENT '接口带宽',
  `if_in_octets` float DEFAULT NULL COMMENT '入字节数',
  `if_in_ucast_pkts` float DEFAULT NULL COMMENT '入单播包数',
  `if_in_discards` float DEFAULT NULL COMMENT '入丢包数',
  `if_in_errors` float DEFAULT NULL COMMENT '入错包数',
  `if_out_octets` float DEFAULT NULL COMMENT '出字节数',
  `if_out_ucast_pkts` float DEFAULT NULL COMMENT '出单播包数',
  `if_out_discards` float DEFAULT NULL COMMENT '出丢包数',
  `if_out_errors` float DEFAULT NULL COMMENT '出错报数',
  `if_in_multicast_pkts` float DEFAULT NULL COMMENT '入组播包数',
  `if_in_broadcast_pkts` float DEFAULT NULL COMMENT '入广播包数',
  `if_out_multicast_pkts` float DEFAULT NULL COMMENT '出组播包数',
  `if_out_broadcast_pkts` float DEFAULT NULL COMMENT '出广播包数',
  PRIMARY KEY (`uu_id`),
  KEY `idx_pm_switch_if_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='端口-指标记录表';

/*Table structure for table `pm_switch_info` */

DROP TABLE IF EXISTS `pm_switch_info`;

CREATE TABLE `pm_switch_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `start_time` datetime DEFAULT NULL COMMENT '数据开始时间',
  `stop_time` datetime DEFAULT NULL COMMENT '数据结束时间',
  `eqp_id` varchar(64) DEFAULT NULL COMMENT '主机资源唯一标识',
  `cpu_used_per` float DEFAULT NULL COMMENT '设备CPU利用率',
  `mem_used_per` float DEFAULT NULL COMMENT '设备内存利用率',
  `cache_used_per` float DEFAULT NULL COMMENT '设备缓存利用率',
  PRIMARY KEY (`uu_id`),
  KEY `idx_pm_switch_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='交换机-指标记录表';

/*Table structure for table `pm_vm_info` */

DROP TABLE IF EXISTS `pm_vm_info`;

CREATE TABLE `pm_vm_info` (
  `uu_id` varchar(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` varchar(50) DEFAULT NULL COMMENT '父节点主键ID',
  `start_time` datetime DEFAULT NULL COMMENT '数据开始时间',
  `stop_time` datetime DEFAULT NULL COMMENT '数据结束时间',
  `instance_id` varchar(64) DEFAULT NULL COMMENT '资源实例编码',
  `cpu_sys_time` float DEFAULT NULL COMMENT 'CPU时间：系统百分比',
  `cpu_user_time` float DEFAULT NULL COMMENT 'CPU时间：用户百分比',
  `cpu_idle_time` float DEFAULT NULL COMMENT 'CPU时间：等待百分比',
  `processor_utilization` float DEFAULT NULL COMMENT 'CPU利用率',
  `mem_used_per` float DEFAULT NULL COMMENT '内存的使用率',
  `sys_mem_used_per` float DEFAULT NULL COMMENT '系统内存使用率',
  `user_mem_used_per` float DEFAULT NULL COMMENT '用户内存使用率',
  `mem_free` int(11) DEFAULT NULL COMMENT '可用内存量',
  `disk_used_per` float DEFAULT NULL COMMENT '磁盘空间占用百分比',
  `net_used_per` float DEFAULT NULL COMMENT '虚拟机网络平均带宽',
  PRIMARY KEY (`uu_id`),
  KEY `idx_pm_vm_info_p_uu_id` (`p_uu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='虚拟机-指标记录表';
