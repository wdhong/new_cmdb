DROP TABLE IF EXISTS `metrics_table_struct`;

CREATE TABLE `metrics_table_struct` (
  `table_name` varchar(64) NOT NULL COMMENT '表名',
  `column_name` varchar(50) NOT NULL COMMENT '列名',
  `column_type` tinyint(1) DEFAULT NULL COMMENT '数据类型: 0-varchar, 1-int, 2-smallint, 3-tinyint, 4-decimal, 5-datetime 6-time, 7-float, 8-double, 9-text, 10-long',
  PRIMARY KEY (`table_name`,`column_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='指标录入表结构数据类型表';