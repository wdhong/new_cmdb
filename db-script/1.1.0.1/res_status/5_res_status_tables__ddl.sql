

DROP TABLE IF EXISTS cm_router_info;
CREATE TABLE cm_router_info(
`uu_id` VARCHAR(50) NOT NULL COMMENT '主键ID',
`p_uu_id` VARCHAR(50) DEFAULT NULL COMMENT '父节点主键ID',
`rc_id` VARCHAR(64) NOT NULL COMMENT '所属资源池系统标识',
`eqp_id` VARCHAR(64) NOT NULL COMMENT '主机资源唯一标识',
`eqp_name` VARCHAR(64) NOT NULL COMMENT '主机名称',
`router_type` VARCHAR(64) DEFAULT NULL COMMENT '设备型号',
`sw_version` VARCHAR(64) DEFAULT NULL COMMENT '设备软件版本',
`vendor_name` VARCHAR(50) DEFAULT NULL COMMENT '网元设备产商',
`router_ip` VARCHAR(50) DEFAULT NULL COMMENT '网元设备IP',
`cur_status` TINYINT(1) DEFAULT NULL COMMENT '当前状态， 0：正常运行 1：还是闲置 2：报废 3：坏',
`router_serialnum` VARCHAR(64) DEFAULT NULL COMMENT '设备序列号',
`asset_origin_type` TINYINT(1) DEFAULT NULL COMMENT '资产来源分类: 1：自产 2：外购 3：借入 4：订单 5：第三方监控源 6：可自义',
`asset_state` TINYINT(1) DEFAULT NULL COMMENT '资产状态: 1：已使用 2：未使用 3：不可用 4：丢失 5：待确认 6：已删除',
`asset_sla_type` TINYINT(1) DEFAULT NULL COMMENT '资产SLA类型: 1：提供服务 2：平台自服务',
PRIMARY KEY (`uu_id`),
KEY `idx_cm_router_info_p_uu_id` (`p_uu_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='路由器-指标记录表';



DROP TABLE IF EXISTS cm_publicip_info;
CREATE TABLE cm_publicip_info(
`uu_id` VARCHAR(50) NOT NULL COMMENT '主键ID',
`p_uu_id` VARCHAR(50) DEFAULT NULL COMMENT '父节点主键ID',
`rc_id` VARCHAR(64) NOT NULL COMMENT '所属资源池系统标识',
`ip` VARCHAR(50) DEFAULT NULL COMMENT '设备IP地址',
`ip_type` VARCHAR(10) DEFAULT NULL COMMENT '设备IP类型',
`related_eqp_id` VARCHAR(64) DEFAULT NULL COMMENT '资源对象标识',
`bandwidth` FLOAT DEFAULT NULL COMMENT '总带宽: 资源池可提供总带宽（针对公网IP），单位Mbps',
PRIMARY KEY (`uu_id`),
KEY `idx_cm_publicip_info_p_uu_id` (`p_uu_id`)
)ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='公网IP资源-指标记录表';




DROP TABLE IF EXISTS cm_router_if_info;
CREATE TABLE cm_router_if_info(
`uu_id` VARCHAR(50) NOT NULL COMMENT '主键ID',
`p_uu_id` VARCHAR(50) DEFAULT NULL COMMENT '父节点主键ID',
`rc_id` VARCHAR(64) NOT NULL COMMENT '所属资源池系统标识',
`eqp_id` VARCHAR(64) NOT NULL COMMENT '主机资源唯一标识',
`eqp_name` VARCHAR(64) NOT NULL COMMENT '主机名称',
`parent_id` VARCHAR(64) DEFAULT NULL COMMENT '所属交换机',
`if_description` VARCHAR(30) DEFAULT NULL COMMENT '端口描述',
`if_status` VARCHAR(10) DEFAULT NULL COMMENT '端口状态',
`if_type` VARCHAR(20) DEFAULT NULL COMMENT '端口类型',
`if_set_local_ip` VARCHAR(50) DEFAULT NULL COMMENT '端口本端配置的IP地址',
`ip_sub_netmask` VARCHAR(50) DEFAULT NULL COMMENT '端口IP网络掩码',
`if_connect_eqp_ip` VARCHAR(50) DEFAULT NULL COMMENT  '端口所连接设备的IP地址',
`if_phy_address` VARCHAR(50) DEFAULT NULL COMMENT '端口物理地址',
`if_speed` FLOAT DEFAULT NULL COMMENT '接口带宽',
`dest_if_id` VARCHAR(50) DEFAULT NULL COMMENT '端口所连对端端口唯一标识',
PRIMARY KEY (`uu_id`),
KEY `idx_cm_router_if_info_p_uu_id` (`p_uu_id`)
)ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='路由器-指标记录表';




DROP TABLE IF EXISTS cm_ids_info;
CREATE TABLE cm_ids_info(
  `uu_id` VARCHAR(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` VARCHAR(50) DEFAULT NULL COMMENT '父节点主键ID',
  `rc_id` VARCHAR(64) NOT NULL COMMENT '所属资源池系统标识',
  `eqp_id` VARCHAR(64) NOT NULL COMMENT '介质库标识',
  `eqp_name` VARCHAR(64) NOT NULL COMMENT '介质库名称',
  `instance_id` VARCHAR(64) NOT NULL COMMENT '资源实例编码',
  `sw_version` VARCHAR(64) DEFAULT NULL COMMENT '设备软件版本',
  `vendor_name` VARCHAR(30) DEFAULT NULL COMMENT '网元设备厂家',
  `lb_ip` VARCHAR(50) DEFAULT NULL COMMENT '网元IP地址',
  `lb_serialnum` VARCHAR(50) DEFAULT NULL COMMENT '设备序列号',
  `throughput` FLOAT DEFAULT NULL COMMENT '吞吐量',
  `connect_num` INT(11) DEFAULT NULL COMMENT '并发链接数',
  `asset_origin_type` TINYINT(1) DEFAULT NULL COMMENT '资产来源分类, 1：自产 2：外购 3：借入 4：订单 5：第三方监控源 6：可自义',
  `asset_state` TINYINT(1) DEFAULT NULL COMMENT '资产状态 1：已使用 2：未使用 3：不可用 4：丢失 5：待确认 6：已删除',
  `asset_sla_type` TINYINT(1) DEFAULT NULL COMMENT '资产SLA类型: 1：提供服务 2：平台自服务',
  PRIMARY KEY (`uu_id`),
  KEY `idx_cm_ids_info_p_uu_id` (`p_uu_id`)
)ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='负载均衡器-指标记录表';




DROP TABLE IF EXISTS cm_ls_info;
CREATE TABLE cm_ls_info(
  `uu_id` VARCHAR(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` VARCHAR(50) DEFAULT NULL COMMENT '父节点主键ID',
  `rc_id` VARCHAR(64) NOT NULL COMMENT '所属资源池系统标识',
  `soft_id` VARCHAR(32) DEFAULT NULL,
  `soft_name` VARCHAR(64) DEFAULT NULL,
  `soft_version` VARCHAR(32) DEFAULT NULL,
  `node_num` INT(11) DEFAULT NULL COMMENT '节点数量',
  `lds_type` TINYINT DEFAULT NULL,
  `ls_size` INT DEFAULT NULL,
  `app_id` VARCHAR(50) DEFAULT NULL COMMENT '所属业务系统',
  `related_eqp_ids` TEXT COMMENT '对应物理机ID列表',
  `asset_state` TINYINT(1) DEFAULT NULL COMMENT '资产状态 1：已使用 2：未使用 3：不可用 4：丢失 5：待确认 6：已删除',
  `asset_sla_type` TINYINT(1) DEFAULT NULL COMMENT '资产SLA类型: 1：提供服务 2：平台自服务',
  `vendor_name` VARCHAR(64) DEFAULT NULL COMMENT '厂家名称',
  PRIMARY KEY (`uu_id`),
  KEY `idx_cm_ls_info_p_uu_id` (`p_uu_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS cm_firewall_info;
CREATE TABLE cm_firewall_info(
  `uu_id` VARCHAR(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` VARCHAR(50) DEFAULT NULL COMMENT '父节点主键ID',
  `rc_id` VARCHAR(64) NOT NULL COMMENT '所属资源池系统标识',
  `eqp_id` VARCHAR(64) NOT NULL COMMENT '主机资源唯一标识',
  `eqp_name` VARCHAR(64) NOT NULL COMMENT '主机名称',
  `fw_type` VARCHAR(30) DEFAULT NULL COMMENT '型号',
  `instance_id` VARCHAR(30) DEFAULT NULL COMMENT '资源实例编码',
  `sw_version` VARCHAR(64) DEFAULT NULL COMMENT '设备软件版本',
  `vendor_name` VARCHAR(30) DEFAULT NULL COMMENT '网元设备厂家',
  `fw_ip` VARCHAR(50) DEFAULT NULL COMMENT '防火墙IP地址',
  `cur_status` TINYINT DEFAULT NULL COMMENT '当前状态',
  `fw_serialnum` VARCHAR(50) DEFAULT NULL COMMENT '设备序列号',
  `throughput` FLOAT DEFAULT NULL COMMENT '吞吐量',
  `app_ids` VARCHAR(128) DEFAULT NULL COMMENT '所属业务系统列表',
  `connect_num` INT(11) DEFAULT NULL COMMENT '并发链接数',
  `new_connect_num` INT(11) DEFAULT NULL COMMENT '新建链接数',
  `asset_origin_type` TINYINT(1) DEFAULT NULL COMMENT '资产来源分类, 1：自产 2：外购 3：借入 4：订单 5：第三方监控源 6：可自义',
  `asset_state` TINYINT(1) DEFAULT NULL COMMENT '资产状态, 1：已使用 2：未使用 3：不可用 4：丢失 5：待确认 6：已删除',
  `asset_sla_type` TINYINT(1) DEFAULT NULL COMMENT '资产SLA类型, 1：提供服务 2：平台自服务',
  `port_num` INT DEFAULT NULL COMMENT '端口数量',
  `firewall_policy` VARCHAR(64) DEFAULT NULL COMMENT '防火墙策略',
  PRIMARY KEY (`uu_id`),
  KEY `idx_cm_firewall_info_p_uu_id` (`p_uu_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='防火墙-指标记录表';



DROP TABLE IF EXISTS cm_v_firewall_info;
CREATE TABLE cm_v_firewall_info(
 `uu_id` VARCHAR(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` VARCHAR(50) DEFAULT NULL COMMENT '父节点主键ID',
  `rc_id` VARCHAR(50) NOT NULL COMMENT '资源池的唯一标识',
  `eqp_name` VARCHAR(64) NOT NULL COMMENT '主机名称',
  `parent_id` VARCHAR(64) DEFAULT NULL COMMENT '所属交换机',
  `instance_id` VARCHAR(64) NOT NULL COMMENT '资源实例编码',
  `cur_status` TINYINT(1) DEFAULT NULL COMMENT '当前状态',
  `throughput` FLOAT DEFAULT NULL COMMENT '吞吐量',
  `app_ids` VARCHAR(128) DEFAULT NULL COMMENT '所属业务系统列表',
  `connect_num` INT(11) DEFAULT NULL COMMENT '并发链接数',
  `new_connect_num` INT(11) DEFAULT NULL COMMENT '新建链接数',
  `firewall_policy` VARCHAR(64) DEFAULT NULL COMMENT '防火墙策略',
  PRIMARY KEY (`uu_id`),
  KEY `idx_cm_v_firewall_info_p_uu_id` (`p_uu_id`)
)ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='虚拟防火墙-指标记录表';



DROP TABLE IF EXISTS cm_loadbalance_info;
CREATE TABLE cm_loadbalance_info(
`uu_id` VARCHAR(50) NOT NULL COMMENT '主键ID',
`p_uu_id` VARCHAR(50) DEFAULT NULL COMMENT '父节点主键ID',
`rc_id` VARCHAR(64) NOT NULL COMMENT '所属资源池系统标识',
`eqp_id` VARCHAR(64) NOT NULL COMMENT '介质库标识',
`eqp_name` VARCHAR(64) NOT NULL COMMENT '介质库名称',
`sw_version` VARCHAR(64) DEFAULT NULL COMMENT '设备软件版本',
`instance_id` VARCHAR(64) NOT NULL COMMENT '资源实例编码',
`vendor_name` VARCHAR(64) DEFAULT NULL COMMENT '设备厂商',
`lb_ip` VARCHAR(50) DEFAULT NULL COMMENT '网元IP地址',
`lb_serialnum` VARCHAR(50) DEFAULT NULL COMMENT '设备序列号',
`throughput` FLOAT DEFAULT NULL COMMENT '吞吐量',
`connect_num` INT(11) DEFAULT NULL COMMENT '并发链接数',
  `new_connect_num` INT(11) DEFAULT NULL COMMENT '新建链接数',
  `asset_origin_type` TINYINT(1) DEFAULT NULL COMMENT '资产来源分类, 1：自产 2：外购 3：借入 4：订单 5：第三方监控源 6：可自义',
  `asset_state` TINYINT(1) DEFAULT NULL COMMENT '资产状态, 1：已使用 2：未使用 3：不可用 4：丢失 5：待确认 6：已删除',
  `asset_sla_type` TINYINT(1) DEFAULT NULL COMMENT '资产SLA类型, 1：提供服务 2：平台自服务',
PRIMARY KEY (`uu_id`),
KEY `idx_cm_loadbalance_info_p_uu_id` (`p_uu_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='cm负载均衡-指标记录表';




DROP TABLE IF EXISTS cm_fs_share_info;
CREATE TABLE cm_fs_share_info(
 `uu_id` VARCHAR(50) NOT NULL COMMENT '主键ID',
  `p_uu_id` VARCHAR(50) DEFAULT NULL COMMENT '父节点主键ID',
  `rc_id` VARCHAR(64) NOT NULL COMMENT '所属资源池系统标识',
  `eqp_id` VARCHAR(64) NOT NULL COMMENT '介质库标识',
  `eqp_name` VARCHAR(64) NOT NULL COMMENT '介质库名称',
  `instance_id` VARCHAR(30) DEFAULT NULL COMMENT '资源实例编码',
  `path` VARCHAR(64) DEFAULT NULL COMMENT '目录路径',
  `share_name` VARCHAR(64) DEFAULT NULL COMMENT '目录共享名',
  `share_type` TINYINT(1) DEFAULT NULL COMMENT '共享方式: 该共享目录的共享方式 1：NFS 2：CIFS 3：POSIX',
  `mount_allowed_ips` VARCHAR(50) DEFAULT NULL COMMENT '允许挂载的IP地址段',
  `app_id` VARCHAR(50) DEFAULT NULL COMMENT '所属业务系统',
  `redundant_strategy` TINYINT(1) DEFAULT NULL COMMENT '冗余策略 "分为两种 0：校验冗余 1：副本冗余',
  `redundant_level` VARCHAR(20) DEFAULT NULL COMMENT '冗余级别',
  PRIMARY KEY (`uu_id`),
  KEY `idx_cm_fs_share_info_p_uu_id` (`p_uu_id`)
)ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='分布式文件系统-指标记录表';






