
--  -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
--  用户表
--  -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

CREATE TABLE If Not Exists `user`  (
  `uuid` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `name` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `user_type` tinyint(4) NULL DEFAULT NULL COMMENT '用户类型',
  `dept_Id` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属部门',
  `no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '人员编号',
  `sex` tinyint(4) NULL DEFAULT NULL COMMENT '性别',
  `mail` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `address` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地址',
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电话号码',
  `mobile` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '移动号码',
  `ldapId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '账号id',
  `namespace` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '空间',
  PRIMARY KEY (`uuid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

--  -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
--  部门表
--  -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
CREATE TABLE If Not Exists `department`  (
  `uuid` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
  `parent_id` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父节点id',
  `name` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  `no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门编号',
  `dept_type` tinyint(4) NULL DEFAULT NULL COMMENT '部门类型',
  `descr` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门描述',
  `level` tinyint(4) NULL DEFAULT NULL COMMENT '部门层级',
  `namespace` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '空间',
  PRIMARY KEY (`uuid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

--  -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
--  资源类型
--  -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
alter table resource_schema rename resource_schema_bak; 
CREATE TABLE If Not Exists `resource_schema`  (
  `resource` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资源类型',
  `general` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否全局',
  `created_at` datetime(6) NOT NULL COMMENT '创建时间',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `parent_resource` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父节点resource',
  PRIMARY KEY (`resource`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

--  -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
--  资源操作
--  -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
alter table resource_schema_actions rename resource_schema_actions_bak; 
CREATE TABLE If Not Exists `resource_schema_actions`  (
  `resource` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资源类型',
  `action` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资源操作',
  `action_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作名称',
  `action_type` tinyint(1) NULL DEFAULT NULL COMMENT '操作类型',
  INDEX `index_actions_resource`(`resource`) USING BTREE,
  CONSTRAINT `resource_schema_actions_ibfk_1` FOREIGN KEY (`resource`) REFERENCES `resource_schema` (`resource`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

--  -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
--  资源约束
--  -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
alter table resource_schema_constraints rename resource_schema_constraints_bak; 
CREATE TABLE If Not Exists `resource_schema_constraints`  (
  `resource` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资源类型',
  `const_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资源约束名',
  `const_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资源约束值',
  INDEX `index_constraints_resource`(`resource`) USING BTREE,
  CONSTRAINT `resource_schema_constraints_ibfk_1` FOREIGN KEY (`resource`) REFERENCES `resource_schema` (`resource`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


--  -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
--  角色
--  -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
alter table role rename role_bak; 
CREATE TABLE If Not Exists `role`  (
  `uuid` char(36) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '角色UUID主键',
  `name` char(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '角色名称',
  `role_type` tinyint(1) NOT NULL COMMENT '角色类型',
  `namespace` char(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '空间名',
  `admin_role` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为管理员角色',
  `descr` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '角色描述',
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`uuid`) USING BTREE,
  INDEX `index_role_name`(`name`) USING BTREE,
  INDEX `index_role_namespace`(`namespace`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

--  -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
--  角色关联
--  -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
alter table role_parents rename role_parents_bak; 
CREATE TABLE If Not Exists `role_parents`  (
  `role_uuid` char(36) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '角色UUID主键',
  `parent_uuid` char(36) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '父角色UUID主键',
  `assigned_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '分配时间',
  INDEX `index_parents_role_uuid`(`role_uuid`) USING BTREE,
  INDEX `index_parents_parent_uuid`(`parent_uuid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

--  -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
--  角色用户
--  -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
CREATE TABLE If Not Exists `roles_user`  (
  `role_uuid` char(36) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '角色UUID主键',
  `namespace` char(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '空间名',
  `username` char(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '用户名',
  `assigned_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '分配时间',
  INDEX `index_users_role_uuid`(`role_uuid`) USING BTREE,
  INDEX `index_users_username`(`username`) USING BTREE,
  INDEX `index_users_namespace`(`namespace`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

--  -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
--  角色权限
--  -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
alter table permissions rename permissions_bak; 

CREATE TABLE If Not Exists `permissions`  (
  `uuid` char(36) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '权限UUID主键',
  `role_uuid` char(36) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '角色UUID主键',
  `actions` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '资源操作',
  `resources` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '资源名',
  `constraints` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '资源约束',
  PRIMARY KEY (`uuid`) USING BTREE,
  INDEX `index_permissions_role_uuid`(`role_uuid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

--  2019.3.14，by 曾祥华
--  添加人员头像
ALTER TABLE `user` 
add COLUMN `picture` longtext NULL COMMENT '头像' AFTER `namespace`;


-- 增加人员字段
ALTER TABLE `user` 
ADD COLUMN `code` varchar(50) NULL COMMENT '人员代码' AFTER `namespace`,
ADD COLUMN `fax` varchar(50) NULL COMMENT '传真' AFTER `code`,
ADD COLUMN `post` varchar(50) NULL COMMENT '职责' AFTER `fax`,
ADD COLUMN `relation_person` char(36) NULL COMMENT '关联人员id' AFTER `post`,
ADD COLUMN `descr` varchar(255) NULL COMMENT '描述' AFTER `relation_person`;


ALTER TABLE `permissions` 
MODIFY COLUMN `actions` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '资源操作' AFTER `role_uuid`,
MODIFY COLUMN `resources` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '资源名' AFTER `actions`;

ALTER TABLE `user` 
ADD COLUMN `ldap_status` tinyint(1) NULL COMMENT '账号状态' AFTER `descr`,
ADD COLUMN `ldap_password_updatetime` datetime(0) NULL COMMENT '密码更新时间' AFTER `ldap_status`;

ALTER TABLE `user` 
ADD COLUMN `ldap_lock_times` int(11) ZEROFILL NULL DEFAULT NULL COMMENT '锁定次数' AFTER `ldap_password_updatetime`;