/*
Navicat MySQL Data Transfer

Source Server         : 10.1.5.119
Source Server Version : 50718
Source Host           : 10.1.5.119:3306
Source Database       : cmdb

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2019-04-25 12:50:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_cfg_dict_log
-- ----------------------------
DROP TABLE IF EXISTS `t_cfg_dict_log`;
CREATE TABLE `t_cfg_dict_log` (
  `dict_id` int(11) DEFAULT NULL,
  `dict_code` varchar(200) NOT NULL COMMENT '名称',
  `col_name` varchar(50) DEFAULT NULL COMMENT '类型',
  `dict_note` varchar(512) DEFAULT NULL COMMENT '值',
  `up_dict` int(11) DEFAULT NULL COMMENT '父ID',
  `description` varchar(512) DEFAULT NULL COMMENT '描述',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `delete_flag` int(1) DEFAULT '0',
  `action` varchar(200) NOT NULL COMMENT '操作'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典操作日志表';

-- ----------------------------
-- Records of t_cfg_dict_log
-- ----------------------------
