/*
Navicat MySQL Data Transfer

Source Server         : 10.1.5.119
Source Server Version : 50718
Source Host           : 10.1.5.119:3306
Source Database       : test_mirror

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2019-05-05 10:17:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_alerts
-- ----------------------------
DROP TABLE IF EXISTS `alert_alerts_test`;
CREATE TABLE `alert_alerts_test` (
  `alert_id` bigint(11) NOT NULL AUTO_INCREMENT,
  `r_alert_id` varchar(300) DEFAULT NULL,
  `event_id` varchar(80) DEFAULT NULL,
  `action_id` bigint(20) DEFAULT NULL,
  `device_id` varchar(50) DEFAULT NULL,
  `device_class` varchar(50) DEFAULT NULL COMMENT '设备类型',
  `biz_sys` varchar(50) DEFAULT NULL COMMENT '业务系统',
  `moni_index` varchar(4000) DEFAULT NULL COMMENT '监控指标/内容，关联触发器name',
  `moni_object` varchar(2000) DEFAULT NULL COMMENT '监控对象',
  `cur_moni_value` varchar(2000) DEFAULT NULL COMMENT '当前监控值',
  `cur_moni_time` datetime DEFAULT NULL COMMENT '当前监控时间',
  `alert_level` varchar(20) NOT NULL COMMENT '告警级别\r\n1-提示\r\n2-低\r\n3-中\r\n4-高\r\n5-严重\r\n',
  `item_id` varchar(50) DEFAULT NULL,
  `alert_end_time` datetime DEFAULT NULL COMMENT '告警结束时间',
  `remark` varchar(4000) DEFAULT NULL COMMENT '备注',
  `order_status` varchar(50) DEFAULT NULL COMMENT '1-未派单\r\n2-处理中\r\n3-已完成\r\n',
  `operate_status` tinyint(1) DEFAULT '0' COMMENT '警报操作状态:0-待确认,1-已确认,2-待解决,3-已处理',
  `source` varchar(100) DEFAULT NULL COMMENT '告警来源\r\nMIRROR\r\nZABBIX\r\n',
  `idc_type` varchar(100) DEFAULT NULL COMMENT '所属位置-资源池',
  `source_room` varchar(100) DEFAULT NULL COMMENT '机房/资源池',
  `object_type` varchar(50) DEFAULT NULL COMMENT '告警类型\r\n1-系统\r\n2-业务',
  `object_id` varchar(50) DEFAULT NULL COMMENT '对象ID，如果是设备告警则是设备ID，如果是业务则是业务系统code',
  `region` varchar(50) DEFAULT NULL COMMENT '域/资源池code',
  `device_ip` varchar(100) DEFAULT NULL,
  `order_id` varchar(100) DEFAULT NULL,
  `order_type` varchar(100) DEFAULT NULL,
  `biz_sys_desc` varchar(500) DEFAULT NULL,
  `alert_start_time` datetime DEFAULT NULL COMMENT '告警开始时间',
  PRIMARY KEY (`alert_id`),
  KEY `index_alerts_device_class` (`device_class`),
  KEY `index_alerts_operate_status` (`operate_status`),
  KEY `index_alerts_alert_level` (`alert_level`),
  KEY `index_alerts_alert_start_time` (`alert_start_time`),
  KEY `index_alerts_object_type` (`object_type`),
  KEY `index_alerts_biz_sys` (`biz_sys`),
  KEY `index_alerts_idc_type` (`idc_type`),
  KEY `index_alerts_source_room` (`source_room`),
  KEY `index_alerts_source` (`source`),
  key `index_group` (`device_ip`, item_id, `alert_level`) USING BTREE,
  key `index_r_alert_id` (`r_alert_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=10000000;

-- ----------------------------
-- Table structure for alert_alerts_his
-- ----------------------------
DROP TABLE IF EXISTS `alert_alerts_his_test`;
CREATE TABLE `alert_alerts_his_test` (
  `alert_id` bigint(11) NOT NULL AUTO_INCREMENT,
  `r_alert_id` varchar(300) DEFAULT NULL,
  `event_id` varchar(50) NULL,
  `action_id` bigint(20) DEFAULT NULL,
  `device_id` varchar(50) DEFAULT NULL,
  `device_class` varchar(50) DEFAULT NULL COMMENT '设备类型',
  `biz_sys` varchar(50) DEFAULT NULL COMMENT '业务系统',
  `moni_index` varchar(4000) NOT NULL COMMENT '监控指标/内容，关联触发器name',
  `moni_object` varchar(2000) DEFAULT NULL COMMENT '监控对象',
  `cur_moni_value` varchar(2000) DEFAULT NULL COMMENT '当前监控值',
  `cur_moni_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '当前监控时间',
  `alert_level` varchar(20) NOT NULL COMMENT '告警级别\r\n1-提示\r\n2-低\r\n3-中\r\n4-高\r\n5-严重\r\n',
  `item_id` varchar(50) DEFAULT NULL,
  `alert_end_time` datetime DEFAULT NULL COMMENT '告警结束时间',
  `remark` varchar(4000) DEFAULT NULL COMMENT '备注',
  `order_status` varchar(50) DEFAULT NULL COMMENT '1-未派单\r\n2-处理中\r\n3-已完成\r\n',
  `clear_time` datetime DEFAULT NULL COMMENT '清除时间',
  `source` varchar(100) DEFAULT NULL COMMENT '告警来源\r\nMIRROR',
  `idc_type` varchar(100) DEFAULT NULL COMMENT '所属位置-资源池',
  `source_room` varchar(100) DEFAULT NULL COMMENT '机房',
  `object_type` varchar(50) DEFAULT NULL,
  `object_id` varchar(50) DEFAULT NULL,
  `region` varchar(50) DEFAULT NULL,
  `device_ip` varchar(100) DEFAULT NULL,
  `order_type` varchar(20) DEFAULT NULL COMMENT '工单类型\r\n1-告警\r\n2-故障',
  `order_id` varchar(100) DEFAULT NULL COMMENT '工单ID',
  `biz_sys_desc` varchar(200) DEFAULT NULL COMMENT '业务系统名称（中文）',
  `alert_start_time` datetime DEFAULT NULL COMMENT '告警开始时间',
  PRIMARY KEY (`alert_id`, `cur_moni_time`),
  KEY `index_his_alerts_create_time` (`alert_start_time`),
  KEY `index_his_alert_level` (`alert_level`),
  KEY `index_his_alert_start_time` (`alert_start_time`),
  KEY `index_his_alert_object_type` (`object_type`),
  KEY `index_his_device_class` (`device_class`),
  KEY `index_his_biz_sys` (`biz_sys`),
  KEY `index_his_idc_type` (`idc_type`),
  KEY `index_his_source_room` (`source_room`),
  KEY `index_his_source` (`source`),
  KEY `index_alert_id` (`alert_id`) USING BTREE,
  key `index_group` (`device_ip`, item_id, `alert_level`) USING BTREE,
  key `index_r_alert_id` (`r_alert_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=10000000;
/**PARTITION BY RANGE(TO_DAYS(cur_moni_time))
(PARTITION `2019-04` VALUES LESS THAN (TO_DAYS('2019-05-01')) ENGINE = InnoDB,
PARTITION `2019-05` VALUES LESS THAN (TO_DAYS('2019-06-01')) ENGINE = InnoDB,
PARTITION `2019-06` VALUES LESS THAN (TO_DAYS('2019-07-01')) ENGINE = InnoDB,
PARTITION `2019-07` VALUES LESS THAN (TO_DAYS('2019-08-01')) ENGINE = InnoDB,
PARTITION `2019-08` VALUES LESS THAN (TO_DAYS('2019-09-01')) ENGINE = InnoDB,
PARTITION `2019-09` VALUES LESS THAN (TO_DAYS('2019-10-01')) ENGINE = InnoDB,
PARTITION `2019-10` VALUES LESS THAN (TO_DAYS('2019-11-01')) ENGINE = InnoDB,
PARTITION `2019-11` VALUES LESS THAN (TO_DAYS('2019-12-01')) ENGINE = InnoDB,
PARTITION `2019-12` VALUES LESS THAN (TO_DAYS('2020-01-01')) ENGINE = InnoDB,
PARTITION `2020-01` VALUES LESS THAN (TO_DAYS('2020-02-01')) ENGINE = InnoDB,
PARTITION `2020-02` VALUES LESS THAN (TO_DAYS('2020-03-01')) ENGINE = InnoDB,
PARTITION `2020-03` VALUES LESS THAN (TO_DAYS('2020-04-01')) ENGINE = InnoDB,
PARTITION `2020-04` VALUES LESS THAN (TO_DAYS('2020-05-01')) ENGINE = InnoDB,
PARTITION `2020-05` VALUES LESS THAN (TO_DAYS('2020-06-01')) ENGINE = InnoDB)*/;

INSERT into alert_alerts_test (r_alert_id, event_id, action_id, device_id, device_class, biz_sys, moni_index, moni_object, cur_moni_value, cur_moni_time, alert_level, item_id, alert_end_time, remark, order_status, operate_status, source, idc_type, source_room, object_type, object_id, region, device_ip, order_id, order_type, biz_sys_desc, alert_start_time) SELECT alert_id, event_id, action_id, device_id, device_class, biz_sys, moni_index, moni_object, cur_moni_value, cur_moni_time, alert_level, item_id, alert_end_time, remark, order_status, operate_status, source, idc_type, source_room, object_type, object_id, region, device_ip, order_id, order_type, biz_sys_desc, alert_start_time from alert_alerts ORDER BY cur_moni_time asc;

INSERT into alert_alerts_his_test (r_alert_id, event_id, action_id, device_id, device_class, biz_sys, moni_index, moni_object, cur_moni_value, cur_moni_time, alert_level, item_id, alert_end_time, remark, order_status, clear_time, source, idc_type, source_room, object_type, object_id, region, device_ip, order_id, order_type, biz_sys_desc, alert_start_time) SELECT alert_id, event_id, action_id, device_id, device_class, biz_sys, moni_index, moni_object, cur_moni_value, cur_moni_time, alert_level, item_id, alert_end_time, remark, order_status, clear_time, source, idc_type, source_room, object_type, object_id, region, device_ip, order_id, order_type, biz_sys_desc, alert_start_time from alert_alerts_his ORDER BY cur_moni_time asc;

RENAME TABLE alert_alerts_his to alert_alerts_his_20190505;
RENAME TABLE alert_alerts to alert_alerts_20190505;
RENAME TABLE alert_alerts_his_test to alert_alerts_his;
RENAME TABLE alert_alerts_test to alert_alerts;


-- ----------------------------
-- Procedure structure for PRO_MGR_ALERT_ALERTS_HIS_ZONE
-- ----------------------------
DROP PROCEDURE IF EXISTS `PRO_MGR_ALERT_ALERTS_HIS_ZONE`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `PRO_MGR_ALERT_ALERTS_HIS_ZONE`()
BEGIN

DECLARE first_zone_name VARCHAR(20);
DECLARE total_zone int;
DECLARE next_time VARCHAR(20);
DECLARE next_time_par VARCHAR(20);

SELECT PARTITION_NAME into first_zone_name FROM INFORMATION_SCHEMA.PARTITIONS WHERE TABLE_NAME = 'alert_alerts_his' limit 1;
SELECT count(*) into total_zone FROM INFORMATION_SCHEMA.PARTITIONS WHERE TABLE_NAME = 'alert_alerts_his';
SELECT date_add(curdate() - DAY (curdate()) + 1,INTERVAL 2 MONTH) into next_time;
SELECT date_format(date_add(now(), interval 1 MONTH),'%Y-%m') into next_time_par;

set @V_ADD_SQL = CONCAT('ALTER TABLE alert_alerts_his  add partition (partition `',next_time_par,'` values less than (\'',next_time,'\'))');

PREPARE stmt from @V_ADD_SQL;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

IF total_zone > 25
THEN
SET @V_DELETE_SQL = CONCAT('ALTER TABLE alert_alerts_his DROP PARTITION ', first_zone_name);
PREPARE stmt from @V_DELETE_SQL;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

END IF;

 
END
;;
DELIMITER ;

-- ----------------------------
-- Event structure for EVENT_ALERT_HIS_CREATE
-- ----------------------------
DROP EVENT IF EXISTS `EVENT_ALERT_HIS_CREATE`;
DELIMITER ;;
CREATE EVENT `EVENT_ALERT_HIS_CREATE`
ON SCHEDULE EVERY 1 MONTH STARTS '2019-05-15 01:00:00'
ON COMPLETION NOT PRESERVE
ENABLE
DO
call PRO_MGR_ALERT_ALERTS_HIS_ZONE();;
DELIMITER;;

ALTER TABLE `alert_report_operate_record` ADD INDEX `index_alert_id` (`ALERT_ID`) USING BTREE;
