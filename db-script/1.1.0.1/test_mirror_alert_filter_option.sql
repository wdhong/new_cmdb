
DROP TABLE IF EXISTS `alert_filter_scene`;
CREATE TABLE `alert_filter_scene` (
  `id` tinyint(12) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8 NOT NULL COMMENT '场景名称',
  `filter_id` tinyint(12) DEFAULT NULL COMMENT '过滤器id',
  `option_condition` varchar(4000) CHARACTER SET utf8 NOT NULL COMMENT '过滤条件',
  `operate_user` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '维护用户',
  `creater` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `editer` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `note` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='告警过滤场景表';
/*
Navicat MySQL Data Transfer

Source Server         : 10.1.5.119
Source Server Version : 50718
Source Host           : 10.1.5.119:3306
Source Database       : test_mirror

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2019-04-23 15:17:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_filter_option
-- ----------------------------
DROP TABLE IF EXISTS `alert_filter_option`;
CREATE TABLE `alert_filter_option` (
  `id` tinyint(12) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8 NOT NULL COMMENT '筛选条件名称',
  `type` varchar(64) CHARACTER SET utf8 NOT NULL COMMENT '展示类型select,string,datetime',
  `code` varchar(64) CHARACTER SET utf8 NOT NULL,
  `operate` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '操作：大于、小于、等于、大于等于、小于等于...分号分割',
  `source` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '数据来源',
  `content` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '配置数据',
  `method` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1:使用，0：停用',
  `jdbc_type` varchar(64) CHARACTER SET utf8 DEFAULT '' COMMENT '数据类型:string,number',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alert_filter_option
-- ----------------------------
INSERT INTO `alert_filter_option` VALUES ('1', '设备ip', 'string', 'device_ip', '模糊匹配,等于,不等于', null, null, null, '1', 'string');
INSERT INTO `alert_filter_option` VALUES ('2', '告警级别', 'select', 'alert_level', '等于,不等于,包含,不包含', null, '[{\"name\": \"提示\", \"value\": \"1\"}, {\"name\": \"低\", \"value\": \"2\"}, {\"name\": \"中\", \"value\": \"3\"}, {\"name\": \"高\", \"value\": \"4\"}, {\"name\": \"严重\", \"value\": \"5\"}]', null, '1', 'string');
INSERT INTO `alert_filter_option` VALUES ('3', '监控对象', 'select', 'moni_object', '等于,不等于,包含,不包含', 'alerts/statistic/monit-obj-list', null, 'GET', '1', 'string');
INSERT INTO `alert_filter_option` VALUES ('4', '资源池', 'select', 'idc_type', '等于,不等于,包含,不包含', 'cmdb/configDict/getDictsByType?type=idcType', null, 'GET', '1', 'string');
INSERT INTO `alert_filter_option` VALUES ('5', '业务系统', 'select', 'biz_sys', '等于,不等于,包含,不包含', 'cmdb/getBizSysList', '', 'GET', '1', 'string');

INSERT INTO `alert_filter_option` VALUES ('6', '告警来源', 'select', 'source', '等于,不等于,包含,不包含', null, '[{\"value\":\"东软\", \"name\":\"东软\"}, {\"value\":\"ZABBIX\", \"name\":\"ZABBIX\"}, {\"value\":\"prometheus\", \"name\":\"prometheus\"}, {\"value\":\"内部告警\", \"name\":\"内部告警\"}]', null, '1', 'string');
INSERT INTO `alert_filter_option` VALUES ('7', '设备类型', 'select', 'device_class', '等于,不等于,包含,不包含', 'cmdb/configDict/getDictsByType?type=device_type', null, 'GET', '1', 'string');
INSERT INTO `alert_filter_option` VALUES ('8', '机房', 'select', 'source_room', '等于,不等于,包含,不包含', 'cmdb/configDict/getDictsByType?type=roomId', null, 'GET', '1', 'string');
INSERT INTO `alert_filter_option` VALUES ('9', '告警内容', 'string', 'moni_index', '模糊匹配,等于,不等于', null, null, null, '1', 'string');


