/*
Navicat MySQL Data Transfer

Source Server         : 10.12.8.191
Source Server Version : 50716
Source Host           : 10.12.8.191:3306
Source Database       : zabbixdb

Target Server Type    : MYSQL
Target Server Version : 50716
File Encoding         : 65001

Date: 2019-05-06 09:44:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_proxy_idc
-- ----------------------------
DROP TABLE IF EXISTS `alert_proxy_idc`;
CREATE TABLE `alert_proxy_idc` (
  `id` bigint(11) NOT NULL,
  `proxy_name` varchar(64) DEFAULT NULL,
  `idc` varchar(64) DEFAULT NULL,
  `remark` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alert_proxy_idc
-- ----------------------------
INSERT INTO `alert_proxy_idc` VALUES ('1', 'xxg_proxy245', 'XXGZYC', '信息港资源池');
INSERT INTO `alert_proxy_idc` VALUES ('2', 'xxg_proxy76', 'XXGZYC', '信息港资源池');
INSERT INTO `alert_proxy_idc` VALUES ('3', 'xxg_proxy77', 'XXGZYC', '信息港资源池');
INSERT INTO `alert_proxy_idc` VALUES ('4', 'xxg_proxy78', 'XXGZYC', '信息港资源池');
INSERT INTO `alert_proxy_idc` VALUES ('5', 'xxg_proxy79', 'XXGZYC', '信息港资源池');
INSERT INTO `alert_proxy_idc` VALUES ('6', 'xxg_proxy80', 'XXGZYC', '信息港资源池');
INSERT INTO `alert_proxy_idc` VALUES ('7', 'ccjk5_proxy', 'YZYFCH', '业支域非池化');
INSERT INTO `alert_proxy_idc` VALUES ('8', 'hrb_proxy80', 'HEBZYC', '哈尔滨资源池');
INSERT INTO `alert_proxy_idc` VALUES ('9', 'hrb_proxy81', 'HEBZYC', '哈尔滨资源池');
INSERT INTO `alert_proxy_idc` VALUES ('10', 'Hohhot_ZAproxy1', 'HHHTZYC', '呼和浩特资源池');
