/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.38
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2020-02-27 14:02:18
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_month_department_sync
-- ----------------------------
DROP TABLE IF EXISTS `alert_month_department_sync`;
CREATE TABLE `alert_month_department_sync` (
  `idcType` varchar(64) DEFAULT NULL,
  `cpu_max` varchar(12) DEFAULT NULL COMMENT '衍生ID',
  `cpu_avg` varchar(12) DEFAULT NULL COMMENT '衍生告警ID',
  `memory_avg` varchar(12) DEFAULT NULL,
  `memory_max` varchar(12) DEFAULT NULL,
  `deviceType` varchar(12) DEFAULT NULL,
  `cpu_eighty_ratio` varchar(12) DEFAULT NULL,
  `cpu_fourty_ratio` varchar(12) DEFAULT NULL COMMENT '设备类型',
  `cpu_fifteen_ratio` varchar(12) DEFAULT NULL COMMENT '业务系统',
  `cpu_eighty_more_ratio` varchar(12) DEFAULT NULL COMMENT '监控指标/内容，关联触发器name',
  `memory_eighty_ratio` varchar(12) DEFAULT NULL,
  `memory_fourty_ratio` varchar(12) DEFAULT NULL COMMENT '设备类型',
  `memory_fifteen_ratio` varchar(12) DEFAULT NULL COMMENT '业务系统',
  `memory_eighty_more_ratio` varchar(12) DEFAULT NULL COMMENT '监控指标/内容，关联触发器name',
  `bizSystem` varchar(255) DEFAULT NULL,
  `month` varchar(12) DEFAULT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `index_r_alert_id` (`memory_avg`) USING BTREE,
  KEY `index_isolate_id` (`cpu_max`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='衍生告警日志表';
