/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.38
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2020-02-27 14:02:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_month_idctype_sync
-- ----------------------------
DROP TABLE IF EXISTS `alert_month_idctype_sync`;
CREATE TABLE `alert_month_idctype_sync` (
  `idcType` varchar(64) DEFAULT NULL COMMENT '资源池',
  `cpu_max` varchar(12) DEFAULT NULL COMMENT '月度CPU资源峰值利用率',
  `cpu_avg` varchar(12) DEFAULT NULL COMMENT '月度CPU资源均值利用率',
  `memory_avg` varchar(12) DEFAULT NULL COMMENT '月度内存均值利用率',
  `memory_max` varchar(12) DEFAULT NULL COMMENT '月度内存峰值利用率',
  `deviceType` varchar(12) DEFAULT NULL COMMENT '设备类型：物理计算资源（X86服务器）；虚拟计算资源（云主机）',
  `cpu_eighty_ratio` varchar(12) DEFAULT NULL COMMENT '80%>CPU峰值>=40%占比',
  `cpu_fourty_ratio` varchar(12) DEFAULT NULL COMMENT '40%>CPU峰值>=15%占比',
  `cpu_fifteen_ratio` varchar(12) DEFAULT NULL COMMENT '15%>CPU峰值占比',
  `cpu_eighty_more_ratio` varchar(12) DEFAULT NULL COMMENT 'CPU峰值>=80%占比',
  `memory_eighty_ratio` varchar(12) DEFAULT NULL COMMENT '80%>内存峰值>=40%占比',
  `memory_fourty_ratio` varchar(12) DEFAULT NULL COMMENT '40%>内存峰值>=15%占比',
  `memory_fifteen_ratio` varchar(12) DEFAULT NULL COMMENT '15%>内存峰值占比',
  `memory_eighty_more_ratio` varchar(12) DEFAULT NULL COMMENT '内存峰值>=80%占比',
  `month` varchar(12) DEFAULT NULL COMMENT '月份',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `index_r_alert_id` (`memory_avg`) USING BTREE,
  KEY `index_isolate_id` (`cpu_max`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='物理计算资源/虚拟计算资源 表';
