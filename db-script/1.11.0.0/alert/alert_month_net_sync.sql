/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.38
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2020-02-27 14:02:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_month_net_sync
-- ----------------------------
DROP TABLE IF EXISTS `alert_month_net_sync`;
CREATE TABLE `alert_month_net_sync` (
  `idcType` varchar(64) DEFAULT NULL COMMENT '资源池',
  `deviceType` varchar(12) DEFAULT NULL COMMENT '资源类型：CMNET;IP承载网',
  `outHighSpeed` varchar(12) DEFAULT NULL COMMENT '出口设计带宽',
  `inHighSpeed` varchar(12) DEFAULT NULL COMMENT '入口设计带宽',
  `ifOutOctetsAvg` varchar(12) DEFAULT NULL COMMENT '出口带宽本月均值利用率',
  `ifOutOctetsMax` varchar(12) DEFAULT NULL COMMENT '出口带宽本月峰值利用率',
  `ifInOctetsAvg` varchar(12) DEFAULT NULL COMMENT '入口带宽本月均值利用率',
  `ifInOctetsMax` varchar(12) DEFAULT NULL COMMENT '入口带宽本月峰值利用率',
  `outTotal` varchar(12) DEFAULT NULL COMMENT '出口流量总量（G）',
  `inTotal` varchar(12) DEFAULT NULL COMMENT '入口流量总量（G）',
  `month` varchar(12) DEFAULT NULL COMMENT '月份',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `index_r_alert_id` (`inHighSpeed`) USING BTREE,
  KEY `index_isolate_id` (`deviceType`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='网络带宽资源表';
