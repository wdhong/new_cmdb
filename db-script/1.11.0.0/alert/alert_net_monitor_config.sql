/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.38
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2020-02-25 10:52:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_net_monitor_config
-- ----------------------------
DROP TABLE IF EXISTS `alert_net_monitor_config`;
CREATE TABLE `alert_net_monitor_config` (
  `ip` varchar(64) DEFAULT NULL,
  `idcType` varchar(12) DEFAULT NULL COMMENT '衍生ID',
  `port` varchar(128) DEFAULT NULL COMMENT '衍生告警ID',
  `device_type` varchar(64) DEFAULT NULL,
  `device_name` varchar(64) DEFAULT NULL,
  `device_type_name` varchar(64) DEFAULT NULL,
  KEY `index_r_alert_id` (`device_type`) USING BTREE,
  KEY `index_isolate_id` (`idcType`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='衍生告警日志表';

-- ----------------------------
-- Records of alert_net_monitor_config
-- ----------------------------
INSERT INTO `alert_net_monitor_config` VALUES ('172.16.154.176', '信息港资源池', 'ae2', 'IP承载网路由器', 'BFJD-ISC-CE01-MX960', 'CE路由器Juniper MX960-1');
INSERT INTO `alert_net_monitor_config` VALUES ('172.16.154.177', '信息港资源池', 'ae2', 'IP承载网路由器', 'BFJD-ISC-CE02-MX960', 'CE路由器Juniper MX960-2');
INSERT INTO `alert_net_monitor_config` VALUES ('192.168.108.1', '哈尔滨资源池', 'Eth-Trunk5', 'CMNET路由器', 'HRB-PCRP1-CMNET-NE40E-1', 'Cmnet接入路由器NE40E-1');
INSERT INTO `alert_net_monitor_config` VALUES ('192.168.108.2', '哈尔滨资源池', 'Eth-Trunk5', 'CMNET路由器', 'HRB-PCRP1-CMNET-NE40E-2', 'Cmnet接入路由器NE40E-2');
INSERT INTO `alert_net_monitor_config` VALUES ('192.168.108.3', '哈尔滨资源池', 'Eth-Trunk3', 'IP承载网路由器', 'HRB-PCRP1-CE-NE40E-1', 'IP承载网路由器NE40E-1');
INSERT INTO `alert_net_monitor_config` VALUES ('192.168.108.4', '哈尔滨资源池', 'Eth-Trunk3', 'IP承载网路由器', 'HRB-PCRP1-CE-NE40E-2', 'IP承载网路由器NE40E-2');
INSERT INTO `alert_net_monitor_config` VALUES ('192.168.254.181', '信息港资源池', 'Eth-Trunk13', 'CMNET路由器', 'BFJD-ISC2-CMNET-NE40E-1', 'Cmnet接入路由器NE40E-X16-1');
INSERT INTO `alert_net_monitor_config` VALUES ('192.168.254.182', '信息港资源池', 'Eth-Trunk13', 'CMNET路由器', 'BFJD-ISC2-CMNET-NE40E-2', 'Cmnet接入路由器NE40E-X16-2');
INSERT INTO `alert_net_monitor_config` VALUES ('192.168.8.1', '呼和浩特资源池', 'Eth-Trunk5', 'CMNET路由器', 'HHHT-PCRP1-CMNET-NE40E-1', 'CMNET接入路由器NE40E-X16A-1');
INSERT INTO `alert_net_monitor_config` VALUES ('192.168.8.2', '呼和浩特资源池', 'Eth-Trunk5', 'CMNET路由器', 'HHHT-PCRP1-CMNET-NE40E-2', 'CMNET接入路由器NE40E-X16A-2');
INSERT INTO `alert_net_monitor_config` VALUES ('192.168.8.3', '呼和浩特资源池', 'Eth-Trunk3,Eth-Trunk5,Eth-Trunk11', 'IP承载网路由器', 'HHHT-PCRP1-CE-NE40E-1', 'CE路由器NE40E-X8A-1');
INSERT INTO `alert_net_monitor_config` VALUES ('192.168.8.4', '呼和浩特资源池', 'Eth-Trunk3,Eth-Trunk5,Eth-Trunk11', 'IP承载网路由器', 'HHHT-PCRP1-CE-NE40E-2', 'CE路由器NE40E-X8A-2');
