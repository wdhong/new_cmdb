/*
 Navicat Premium Data Transfer

 Source Server         : 10.12.70.40
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : 10.12.70.40:3306
 Source Schema         : mirror

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 28/02/2020 15:57:01
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for mon_report_alert_device
-- ----------------------------
DROP TABLE IF EXISTS `mon_report_alert_device`;
CREATE TABLE `mon_report_alert_device`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `idc_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资源池',
  `alert_level` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '告警等级',
  `alert_index` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '告警指标',
  `device_model` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备型号',
  `device_mfrs` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备厂商',
  `pod` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'pod池',
  `s_count` bigint(20) NULL DEFAULT NULL COMMENT '告警数量',
  `rank` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '排名',
  `mon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '月份',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 121 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '运营月报-告警设备top10' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
