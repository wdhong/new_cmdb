/*
 Navicat Premium Data Transfer

 Source Server         : 10.12.70.40
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : 10.12.70.40:3306
 Source Schema         : mirror

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 28/02/2020 15:57:23
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for mon_report_alert_idc
-- ----------------------------
DROP TABLE IF EXISTS `mon_report_alert_idc`;
CREATE TABLE `mon_report_alert_idc`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `idc_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资源池',
  `alert_level` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '告警等级',
  `server_count` bigint(255) NULL DEFAULT NULL COMMENT '服务器告警数量',
  `firewall_count` bigint(255) NULL DEFAULT NULL COMMENT '防火墙告警数量',
  `router_count` bigint(255) NULL DEFAULT NULL COMMENT '路由器告警数量',
  `exchange_count` bigint(255) NULL DEFAULT NULL COMMENT '交换价告警数量',
  `load_balance_count` bigint(255) NULL DEFAULT NULL COMMENT '负载均衡告警数量',
  `cloud_storage_count` bigint(255) NULL DEFAULT NULL COMMENT '云存储告警数量',
  `sdn_controller_count` bigint(255) NULL DEFAULT NULL COMMENT 'sdn控制器告警数量',
  `disk_arrary_count` bigint(255) NULL DEFAULT NULL COMMENT '磁盘阵列告警数量',
  `tape_library_count` bigint(255) NULL DEFAULT NULL COMMENT '磁带库告警数量',
  `mon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '月份',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '运营月报-各资源池告警分布' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
