/*
Navicat MySQL Data Transfer

Source Server         : bpm
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : cmdb

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2020-02-28 16:28:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_component_info
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_component_info`;
CREATE TABLE `cmdb_component_info` (
  `id` varchar(50) NOT NULL,
  `maintenance_id` varchar(50) NOT NULL,
  `specific_model` varchar(50) DEFAULT NULL,
  `config_desc` varchar(200) DEFAULT NULL,
  `serial_number` varchar(50) DEFAULT NULL,
  `component_name` varchar(50) DEFAULT NULL,
  `unit` varchar(20) DEFAULT NULL,
  `quantity` varchar(20) DEFAULT NULL,
  `unit_not_tax` decimal(20,5) DEFAULT NULL,
  `money_not_tax` decimal(20,5) DEFAULT NULL,
  `add_tax` varchar(20) DEFAULT NULL,
  `add_amount` decimal(20,5) DEFAULT NULL,
  `money_with_tax` decimal(20,5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
