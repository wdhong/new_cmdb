/*
Navicat MySQL Data Transfer

Source Server         : bpm
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : cmdb

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2020-02-28 16:21:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_file_manage
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_file_manage`;
CREATE TABLE `cmdb_file_manage` (
  `id` varchar(50) NOT NULL,
  `file_name` varchar(50) DEFAULT NULL,
  `file_name_alias` varchar(50) DEFAULT NULL,
  `file_type` varchar(50) DEFAULT NULL,
  `file_object` varchar(50) DEFAULT NULL,
  `file_data` mediumblob,
  `is_delete` varchar(10) DEFAULT '0' COMMENT '0表示未删除，1表示删除',
  PRIMARY KEY (`id`),
  KEY `index_file_type_and_object` (`file_type`,`file_object`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
