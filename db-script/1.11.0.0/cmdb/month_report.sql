/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 5.7.26-log 
*********************************************************************
*/

ALTER TABLE `cmdb`.`cmdb_instance`
  ADD COLUMN `node_type` VARCHAR (40) NULL COMMENT '节点类型' AFTER `device_model_Edition`,
  ADD INDEX `IDX_CMDB_INSTANCE_04` (`idcType`, `node_type`),
  ADD INDEX `IDX_CMDB_INSTANCE_05` (`bizSystem`, `node_type`);
/*更新计算节点*/
UPDATE cmdb_instance SET node_type = '计算节点' WHERE department2 != '基础平台部' OR bizSystem='未分配';
/*更新宿主机*/
UPDATE cmdb_instance SET node_type = '宿主机' WHERE department2 = '基础平台部' AND bizSystem in ('VMWARE','Vmware虚拟化宿主机','Vmware虚拟化管理节点VROPS','虚拟化宿主机');
/*更新存储节点*/
UPDATE cmdb_instance SET node_type = '存储节点' WHERE device_type = 'X86服务器' AND device_class_3 NOT IN ( 'X86服务器', '网管服务器');
/*更新管理节点*/
UPDATE cmdb_instance SET node_type = '管理节点' WHERE department2 = '基础平台部' AND bizSystem NOT IN('未分配','VMWARE','Vmware虚拟化宿主机','Vmware虚拟化管理节点VROPS') AND device_class_3 ='X86服务器';


ALTER TABLE `cmdb`.`cmdb_biz_system`
  ADD COLUMN `create_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NULL COMMENT '录入时间' AFTER `isdel`;




DROP TABLE IF EXISTS `cmdb`.`cmdb_month_report_phy`;
CREATE TABLE `cmdb`.`cmdb_month_report_phy` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',,
  `idcType` varchar(40) NOT NULL COMMENT '资源池名称',
  `m_phy_all_count` varchar(40) DEFAULT NULL COMMENT '已交维所有物理服务器数量（台）',
  `m_phy_comp_count` varchar(40) DEFAULT NULL COMMENT '已交维建设物理计算资源节点数（台）-不含管理节点',
  `m_phy_gup_comp_count` varchar(40) DEFAULT NULL COMMENT '已交维GPU计算资源节点数（台）-不含管理节点',
  `m_phy_host_comp_count` varchar(40) DEFAULT NULL COMMENT '已交维虚拟计算资源宿主机数（台）-不含管理节点',
  `m_phy_storage_count` varchar(40) DEFAULT NULL COMMENT '已交维存储服务器节点数（台）',
  `m_phy_manage_count` varchar(40) DEFAULT NULL COMMENT '已交维管理节点（台）',
  `unm_phy_all_count` varchar(40) DEFAULT NULL COMMENT '未交维所有物理服务器数量（台）',
  `unm_phy_comp_count` varchar(40) DEFAULT NULL COMMENT '未交维建设物理计算资源节点数（台）-不含管理节点',
  `unm_phy_gup_comp_count` varchar(40) DEFAULT NULL COMMENT '未交维GPU计算资源节点数（台）-不含管理节点',
  `unm_phy_host_comp_count` varchar(40) DEFAULT NULL COMMENT '未交维虚拟计算资源宿主机数（台）-不含管理节点',
  `unm_phy_storage_count` varchar(40) DEFAULT NULL COMMENT '未交维存储服务器节点数（台）-不含管理节点',
  `unm_phy_manage_count` varchar(40) DEFAULT NULL COMMENT '未交维管理节点（台）',
  `month` varchar(40) NOT NULL COMMENT '统计月份',
  PRIMARY KEY (`idcType`,`month`),
  UNIQUE KEY `CMDB_MR_PHY_IDX_01` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='运营月报-资源概况-资源概况';



DROP TABLE IF EXISTS `cmdb`.`cmdb_month_report_comp`;
CREATE TABLE `cmdb`.`cmdb_month_report_comp` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',,
  `idcType` varchar(40) NOT NULL COMMENT '资源池名称',
  `m_phy_comp_count` varchar(40) DEFAULT NULL COMMENT '截至当月历史累计已建物理计算资源设备总量（台）（资源池已交维）',
  `m_dist_count` varchar(40) DEFAULT NULL COMMENT '截至当月历史累计已分配设备总量（台）（资源池已交维设备分配）',
  `bpm_plan_apply_count` varchar(40) DEFAULT NULL COMMENT '本月各系统计划申请资源合计（台）（工单申请）',
  `bpm_plan_apply_reply_count` varchar(40) DEFAULT NULL COMMENT '本月各各系统计划申请资源批复合计（台）（工单申请中批复）',
  `bpm_apply_dist_count` varchar(40) DEFAULT NULL COMMENT '本月各系统申请并分配设备总量（台）',
  `bpm_apply_undist_count` varchar(40) DEFAULT NULL COMMENT '本月申请未分配设备总量（台）',
  `bpm_lm_apply_dist_count` varchar(40) DEFAULT NULL COMMENT '往月申请本月分配设备总量（台）',
  `bpm_lm_apply_undist_count` varchar(40) DEFAULT NULL COMMENT '往月申请本月仍未分配设备总量（台）',
  `unm_comp_count` varchar(40) DEFAULT NULL COMMENT '在建设备总量（台）（未交维部分）',
  `unm_comp_predist_count` varchar(40) DEFAULT NULL COMMENT '在建已预分配总量（台）（未交维部分）',
  `surplus_count` varchar(40) DEFAULT NULL COMMENT '已建剩余设备总量（台）',
  `distribute_rate` varchar(40) DEFAULT NULL COMMENT '分配率',
  `month` varchar(40) NOT NULL COMMENT '统计月份',
  PRIMARY KEY (`idcType`,`month`),
  UNIQUE KEY `CMDB_MR_COMP_IDX_01` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='运营月报-资源概况-物理计算资源';


DROP TABLE IF EXISTS `cmdb`.`cmdb_month_report_vm`;
CREATE TABLE `cmdb`.`cmdb_month_report_vm` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',,
  `idcType` varchar(40) NOT NULL COMMENT '资源池名称',
  `m_vm_dist_count` varchar(40) DEFAULT NULL COMMENT '截至当月历史累计已分配虚拟计算资源设备总量（台）（资源池已交维设备分配）',
  `vm_host_count` varchar(40) DEFAULT NULL COMMENT '宿主机总量（台）',
  `vm_host_cpu_core_number` varchar(40) DEFAULT NULL COMMENT '宿主机CPU总核数（个）',
  `vm_host_memory_size` varchar(40) DEFAULT NULL COMMENT '宿主机总内存（G）',
  `vm_vcpu_core_number` varchar(40) DEFAULT NULL COMMENT '虚拟VCPU总核数（个）',
  `vm_memory_size` varchar(40) DEFAULT NULL COMMENT '虚拟机总内存（G）',
  `vm_host_w_dist_memory_size` varchar(40) DEFAULT NULL COMMENT '宿主机待分配内存（G）',
  `vm_rate` varchar(40) DEFAULT NULL COMMENT '虚拟化比（%）(虚拟机数量/物理机数量)',
  `bpm_plan_apply_count` varchar(40) DEFAULT NULL COMMENT '本月各系统计划申请资源合计（台）（工单申请）',
  `bpm_plan_apply_reply_count` varchar(40) DEFAULT NULL COMMENT '本月各各系统计划申请资源批复合计（台）（工单申请中批复）',
  `bpm_apply_dist_count` varchar(40) DEFAULT NULL COMMENT '本月各系统申请并分配设备总量（台）',
  `bpm_apply_undist_count` varchar(40) DEFAULT NULL COMMENT '本月申请未分配设备总量（台）',
  `bpm_lm_apply_dist_count` varchar(40) DEFAULT NULL COMMENT '往月申请本月分配设备总量（台）',
  `bpm_lm_apply_undist_count` varchar(40) DEFAULT NULL COMMENT '往月申请本月仍未分配设备总量（台）',
  `month` varchar(40) NOT NULL COMMENT '统计月份',
  PRIMARY KEY (`idcType`,`month`),
  UNIQUE KEY `CMDB_MR_VM_IDX_01` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='运营月报-资源概况-虚拟计算资源';

DROP TABLE IF EXISTS `cmdb`.`cmdb_month_report_network`;
CREATE TABLE `cmdb`.`cmdb_month_report_network` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',,
  `idcType` varchar(40) NOT NULL COMMENT '资源池名称',
  `m_network_count` varchar(40) DEFAULT NULL COMMENT '已建设设备总量（台）--已交维',
  `unm_network_count` varchar(40) DEFAULT NULL COMMENT '在建设备总量（台）--未交维',
  `month` varchar(40) NOT NULL COMMENT '统计月份',
  PRIMARY KEY (`idcType`,`month`),
  UNIQUE KEY `CMDB_MR_NETWORK_IDX_01` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='运营月报-资源概况-网络设备总量';

DROP TABLE IF EXISTS `cmdb`.`cmdb_month_report_recycle`;
CREATE TABLE `cmdb`.`cmdb_month_report_recycle` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',,
  `idcType` varchar(40) NOT NULL COMMENT '资源池名称',
  `bizSystem` varchar(40) NOT NULL COMMENT '业务系统名称',
  `department1` varchar(40) DEFAULT NULL COMMENT '归属部门一级',
  `department2` varchar(40) DEFAULT NULL COMMENT '归属部门二级',
  `roomId` varchar(40) DEFAULT NULL COMMENT '所属资源池物理地址（机房）',
  `pod_name` varchar(40) DEFAULT NULL COMMENT 'POD名称',
  `phy_count` varchar(40) DEFAULT NULL COMMENT '物理计算资源（台）',
  `gpu_count` varchar(40) DEFAULT NULL COMMENT 'GPU服务器（台）',
  `vm_count` varchar(40) DEFAULT NULL COMMENT '虚拟计算资源（台）',
  `vm_cpu_count` varchar(40) DEFAULT NULL COMMENT '虚拟计算资源CPU数量（台）',
  `vm_memory_count` varchar(40) DEFAULT NULL COMMENT '虚拟计算资源内存数量（G）',
  `vm_storage_count` varchar(40) DEFAULT NULL COMMENT '虚拟计算资源存储数量（T）',
  `plan_recycle` varchar(40) DEFAULT NULL COMMENT '计划资源回收/资源清理',
  `recycle_time` varchar(40) DEFAULT NULL COMMENT '资源回收0r清理时间',
  `month` varchar(40) NOT NULL COMMENT '统计月份',
  PRIMARY KEY (`idcType`,`bizSystem`,`month`),
  UNIQUE KEY `CMDB_MR_RECYCLE_IDX_01` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='运营月报-资源清理和回收-低效无效资源清理';



DROP TABLE IF EXISTS `cmdb`.`cmdb_month_report_depinfo`;
CREATE TABLE `cmdb`.`cmdb_month_report_depinfo` (
   `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `bizSystem` varchar(40) NOT NULL COMMENT '业务系统名称',
  `name` varchar(40) DEFAULT NULL COMMENT '联系人',
  `phone` varchar(40) DEFAULT NULL COMMENT '联系电话',
  `department2` varchar(40) DEFAULT NULL COMMENT '归属部门（二级）',
  `department1` varchar(40) DEFAULT NULL COMMENT '所属租户（一级）',
  `idcType` varchar(40) NOT NULL COMMENT '所属资源池名称',
  `bpm_phy_plan_apply_count` varchar(40) DEFAULT NULL COMMENT '物理计算资源-单月计划申请总量批复（台）',
  `phy_dist_count` varchar(40) DEFAULT NULL COMMENT '物理计算资源-物理机已分配设备总量（台）',
  `bpm_phy_delivery_cycle` varchar(40) DEFAULT NULL COMMENT '物理计算资源-交付周期（天）',
  `bpm_phy_delivery_percent` varchar(40) DEFAULT NULL COMMENT '物理计算资源-交付比例（已交付批复）（%）',
  `bpm_gpu_plan_apply_count` varchar(40) DEFAULT NULL COMMENT 'GPU计算资源-单月计划申请总量批复（台）',
  `gpu_dist_count` varchar(40) DEFAULT NULL COMMENT 'GPU计算资源-物理机已分配设备总量（台）',
  `bpm_gpu_delivery_cycle` varchar(40) DEFAULT NULL COMMENT 'GPU计算资源-交付周期（天）',
  `bpm_gpu_delivery_percent` varchar(40) DEFAULT NULL COMMENT 'GPU计算资源-交付比例（已交付批复）（%）',
  `bpm_vm_plan_apply_count` varchar(40) DEFAULT NULL COMMENT '虚拟计算资源-计划申请总量（台）',
  `vm_dist_count` varchar(40) DEFAULT NULL COMMENT '虚拟计算资源-已分配内存总量（G）',
  `vm_vcpu_core_number` varchar(40) DEFAULT NULL COMMENT '虚拟计算资源-虚拟核数VCPU总量（个）',
  `vm_dist_memory_count` varchar(40) DEFAULT NULL COMMENT '虚拟计算资源-已分配内存总量（G）',
  `bpm_vm_delivery_cycle` varchar(40) DEFAULT NULL COMMENT '虚拟计算资源-交付周期（天）',
  `bpm_vm_delivery_percent` varchar(40) DEFAULT NULL COMMENT '虚拟计算资源-交付比例（%）',
  `bpm_fcsan_plan_apply_count` varchar(40) DEFAULT NULL COMMENT 'FSCAN-计划申请总量批复（台）',
  `fcsan_dist_count` varchar(40) DEFAULT NULL COMMENT 'FSCAN-已分配设备总量（台）',
  `bpm_fcsan_delivery_cycle` varchar(40) DEFAULT NULL COMMENT 'FSCAN-交付周期（天）',
  `bpm_fcsan_delivery_percent` varchar(40) DEFAULT NULL COMMENT 'FSCAN-交付比例（已交付批复）（%）',
  `bpm_ipsan_plan_apply_count` varchar(40) DEFAULT NULL COMMENT 'IPSAN-计划申请总量批复（台）',
  `ipsan_dist_count` varchar(40) DEFAULT NULL COMMENT 'IPSAN-已分配设备总量（台）',
  `bpm_ipsan_delivery_cycle` varchar(40) DEFAULT NULL COMMENT 'IPCAN-交付周期（天）',
  `bpm_ipsan_delivery_percent` varchar(40) DEFAULT NULL COMMENT 'IPCAN-交付比例（已交付批复）（%）',
  `bpm_block_plan_apply_count` varchar(40) DEFAULT NULL COMMENT '块存储-计划申请总量批复（台）',
  `block_dist_count` varchar(40) DEFAULT NULL COMMENT '块存储-已分配设备总量（台）',
  `bpm_block_delivery_cycle` varchar(40) DEFAULT NULL COMMENT '块存储-交付周期（天）',
  `bpm_block_delivery_percent` varchar(40) DEFAULT NULL COMMENT '块存储-交付比例（已交付批复）（%）',
  `bpm_file_plan_apply_count` varchar(40) DEFAULT NULL COMMENT '文件存储-计划申请总量批复（台）',
  `file_dist_count` varchar(40) DEFAULT NULL COMMENT '文件存储-已分配设备总量（台）',
  `bpm_file_delivery_cycle` varchar(40) DEFAULT NULL COMMENT '文件存储-交付周期（天）',
  `bpm_file_delivery_percent` varchar(40) DEFAULT NULL COMMENT '文件存储-交付比例（已交付批复）（%）',
  `bpm_obj_plan_apply_count` varchar(40) DEFAULT NULL COMMENT '对象存储-计划申请总量批复（台）',
  `obj_dist_count` varchar(40) DEFAULT NULL COMMENT '对象存储-已分配设备总量（台）',
  `bpm_obj_delivery_cycle` varchar(40) DEFAULT NULL COMMENT '对象存储-交付周期（天）',
  `bpm_obj_delivery_percent` varchar(40) DEFAULT NULL COMMENT '对象存储-交付比例（已交付批复）（%）',
  `bpm_backup_plan_apply_count` varchar(40) DEFAULT NULL COMMENT '备份存储-计划申请总量批复（台）',
  `backup_dist_count` varchar(40) DEFAULT NULL COMMENT '备份存储-已分配设备总量（台）',
  `bpm_backup_delivery_cycle` varchar(40) DEFAULT NULL COMMENT '备份存储-交付周期（天）',
  `bpm_backup_delivery_percent` varchar(40) DEFAULT NULL COMMENT '备份存储-交付比例（已交付批复）（%）',
  `alert_phy_cpu_max` varchar(40) DEFAULT NULL COMMENT '物理机-月度CPU资源峰值利用率',
  `alert_phy_cpu_avg` varchar(40) DEFAULT NULL COMMENT '物理机-月度CPU资源均值利用率',
  `alert_phy_memory_avg` varchar(40) DEFAULT NULL COMMENT '物理机-月度内存均值利用率',
  `alert_phy_memory_max` varchar(40) DEFAULT NULL COMMENT '物理机-月度内存峰值利用率',
  `alert_phy_cpu_eighty_ratio` varchar(40) DEFAULT NULL COMMENT '物理机-80%>CPU峰值>=40%占比',
  `alert_phy_cpu_fourty_ratio` varchar(40) DEFAULT NULL COMMENT '物理机-40%>CPU峰值>=15%占比',
  `alert_phy_cpu_fifteen_ratio` varchar(40) DEFAULT NULL COMMENT '物理机-40%>CPU峰值>=15%占比',
  `alert_phy_cpu_eighty_more_ratio` varchar(40) DEFAULT NULL COMMENT '物理机-CPU峰值>=80%机占比',
  `alert_phy_memory_eighty_ratio` varchar(40) DEFAULT NULL COMMENT '物理机-80%>内存峰值>=40%占比',
  `alert_phy_memory_fourty_ratio` varchar(40) DEFAULT NULL COMMENT '物理机-40%>内存峰值>=15%占比',
  `alert_phy_memory_fifteen_ratio` varchar(40) DEFAULT NULL COMMENT '物理机-15%>内存峰值占比',
  `alert_phy_memory_eighty_more_ratio` varchar(40) DEFAULT NULL COMMENT '物理机-内存峰值>=80%占比',
  `alert_vm_cpu_max` varchar(40) DEFAULT NULL COMMENT '云主机-月度CPU资源峰值利用率',
  `alert_vm_cpu_avg` varchar(40) DEFAULT NULL COMMENT '云主机-月度CPU资源均值利用率',
  `alert_vm_memory_avg` varchar(40) DEFAULT NULL COMMENT '云主机-月度内存均值利用率',
  `alert_vm_memory_max` varchar(40) DEFAULT NULL COMMENT '云主机-月度内存峰值利用率',
  `alert_vm_cpu_eighty_ratio` varchar(40) DEFAULT NULL COMMENT '云主机-80%>CPU峰值>=40%占比',
  `alert_vm_cpu_fourty_ratio` varchar(40) DEFAULT NULL COMMENT '云主机-40%>CPU峰值>=15%占比',
  `alert_vm_cpu_fifteen_ratio` varchar(40) DEFAULT NULL COMMENT '云主机-15%>内存峰值占比',
  `alert_vm_cpu_eighty_more_ratio` varchar(40) DEFAULT NULL COMMENT '云主机-CPU峰值>=80%机占比',
  `alert_vm_memory_eighty_ratio` varchar(40) DEFAULT NULL COMMENT '云主机-80%>内存峰值>=40%占比',
  `alert_vm_memory_fourty_ratio` varchar(40) DEFAULT NULL COMMENT '云主机-40%>内存峰值>=15%占比',
  `alert_vm_memory_fifteen_ratio` varchar(40) DEFAULT NULL COMMENT '云主机-15%>内存峰值占比',
  `alert_vm_memory_eighty_more_ratio` varchar(40) DEFAULT NULL COMMENT '云主机-内存峰值>=80%占比',
  `alert_fcsan_avg_utz` varchar(40) DEFAULT NULL COMMENT 'FCSAN月度资源均值利用率',
  `alert_ipsan_avg_utz` varchar(40) DEFAULT NULL COMMENT 'IPSAN月度资源均值利用率',
  `alert_block_avg_utz` varchar(40) DEFAULT NULL COMMENT '块存储月度资源均值利用率',
  `alert_file_avg_utz` varchar(40) DEFAULT NULL COMMENT '文件存储月度资源均值利用率',
  `alert_obj_avg_utz` varchar(40) DEFAULT NULL COMMENT '对象存储月度资源均值利用率',
  `alert_sum` varchar(40) DEFAULT NULL COMMENT '总数',
  `alert_serious` varchar(40) DEFAULT NULL COMMENT '严重告警数量',
  `alert_high` varchar(40) DEFAULT NULL COMMENT '重要告警数量',
  `alert_medium` varchar(40) DEFAULT NULL COMMENT '次要告警数量',
  `alert_low` varchar(40) DEFAULT NULL COMMENT '提醒告警数量',
  `bpm_level1_fault` varchar(40) DEFAULT NULL COMMENT '一级故障',
  `bpm_level2_fault` varchar(40) DEFAULT NULL COMMENT '二级故障',
  `bpm_level3_fault` varchar(40) DEFAULT NULL COMMENT '三级故障',
  `bpm_fault_handle_ontime` varchar(40) DEFAULT NULL COMMENT '故障处理及时率',
  `month` varchar(40) NOT NULL COMMENT '统计月份',
  PRIMARY KEY (`bizSystem`,`idcType`,`month`),
  UNIQUE KEY `CMDB_MR_DEPINFO_IDX_02` (`id`),
  KEY `CMDB_MR_DEPINFO_IDX_01` (`bizSystem`,`idcType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='运营月报-租户资源基本信息-租户资源';


DROP TABLE IF EXISTS `cmdb`.`cmdb_month_report_addeddep`;
CREATE TABLE `cmdb`.`cmdb_month_report_addeddep` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',,
  `bizSystem` varchar(40) NOT NULL COMMENT '业务系统名称',
  `name` varchar(40) DEFAULT NULL COMMENT '联系人',
  `phone` varchar(40) DEFAULT NULL COMMENT '联系电话',
  `department2` varchar(40) DEFAULT NULL COMMENT '归属部门（二级）',
  `department1` varchar(40) DEFAULT NULL COMMENT '所属租户（一级）',
  `idcType` varchar(40) NOT NULL COMMENT '所属资源池名称',
  `phy_dist_count` varchar(40) DEFAULT NULL COMMENT '分配物理机资源（台）',
  `vm_dist_count` varchar(40) DEFAULT NULL COMMENT '分配的虚拟机（台）',
  `storage_dist_count` varchar(40) DEFAULT NULL COMMENT '分配的存储资源（T）',
  `alert_phy_cpu_max` varchar(40) DEFAULT NULL COMMENT '分配物理机资源CPU 峰值利用率（%）',
  `alert_phy_memory_max` varchar(40) DEFAULT NULL COMMENT '分配物理机资源内存峰值利用率（%）',
  `vm_dist_vcpu_count` varchar(40) DEFAULT NULL COMMENT '分配虚拟机计算资源VCPU数量（个）',
  `alert_vm_cpu_max` varchar(40) DEFAULT NULL COMMENT '分配虚拟机计算资源CPU峰值利用率（%）',
  `vm_dist_memory_count` varchar(40) DEFAULT NULL COMMENT '分配虚拟机计算资源内存数量（G）',
  `alert_vm_memory_max` varchar(40) DEFAULT NULL COMMENT '分配虚拟机计算资源内存峰值利用率（%）',
  `storage_dist_type` varchar(40) DEFAULT NULL COMMENT '分配的存储类型',
  `storage_dist_count_size` varchar(40) DEFAULT NULL COMMENT '分配存储算资源数量（T）',
  `alert_storage_vm_utz` varchar(40) DEFAULT NULL COMMENT '云存储资源利用率(%)',
  `month` varchar(40) NOT NULL COMMENT '统计月份',
  PRIMARY KEY (`bizSystem`,`idcType`,`month`),
  UNIQUE KEY `CMDB_MR_ADEP_IDX_01` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='运营月报-新增租户信息-新增租户信息';


DROP TABLE IF EXISTS `cmdb`.`cmdb_month_report_phy_utzlow`;
CREATE TABLE `cmdb`.`cmdb_month_report_phy_utzlow` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `bizSystem` VARCHAR(40) NOT NULL COMMENT '业务系统名称',
  `name` VARCHAR(40) DEFAULT NULL COMMENT '联系人',
  `phone` VARCHAR(40) DEFAULT NULL COMMENT '联系电话',
  `department2` VARCHAR(40) DEFAULT NULL COMMENT '归属部门（二级）',
  `department1` VARCHAR(40) DEFAULT NULL COMMENT '所属租户（一级）',
  `idcType` VARCHAR(40) NOT NULL COMMENT '所属资源池名称',
  `phy_dist_count` VARCHAR(40) DEFAULT NULL COMMENT '分配物理机资源（台）',
  `alert_phy_cpu_max` VARCHAR(40) DEFAULT NULL COMMENT '分配物理机资源CPU 峰值利用率（%）',
  `alert_phy_memory_max` VARCHAR(40) DEFAULT NULL COMMENT '分配物理机资源内存峰值利用率（%）',
  `month` VARCHAR(40) NOT NULL COMMENT '统计月份',
  PRIMARY KEY (`bizSystem`,`idcType`,`month`),
  UNIQUE KEY `CMDB_MR_ADEP_IDX_01` (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='运营月报-租户资源物理计算资源峰值利用率低于30%';

DROP TABLE IF EXISTS `cmdb`.`cmdb_month_report_vm_cpu_utzlow`;
CREATE TABLE `cmdb`.`cmdb_month_report_vm_cpu_utzlow` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `bizSystem` VARCHAR(40) NOT NULL COMMENT '业务系统名称',
  `name` VARCHAR(40) DEFAULT NULL COMMENT '联系人',
  `phone` VARCHAR(40) DEFAULT NULL COMMENT '联系电话',
  `department2` VARCHAR(40) DEFAULT NULL COMMENT '归属部门（二级）',
  `department1` VARCHAR(40) DEFAULT NULL COMMENT '所属租户（一级）',
  `idcType` VARCHAR(40) NOT NULL COMMENT '所属资源池名称',
  `vm_dist_count` VARCHAR(40) DEFAULT NULL COMMENT '分配虚拟机计算资源数量（台）',
  `vm_dist_cpu_count` VARCHAR(40) DEFAULT NULL COMMENT '分配虚拟机计算资源CPU数量（个）',
  `alert_vm_cpu_max` VARCHAR(40) DEFAULT NULL COMMENT '分配虚拟机计算资源CPU峰值利用率（%）',
  `month` VARCHAR(40) NOT NULL COMMENT '统计月份',
  PRIMARY KEY (`bizSystem`,`idcType`,`month`),
  UNIQUE KEY `CMDB_MR_ADEP_IDX_01` (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='运营月报-各资源池租户虚拟计算资源CPU峰值利用率低于30%';

DROP TABLE IF EXISTS `cmdb`.`cmdb_month_report_vm_memory_utzlow`;
CREATE TABLE `cmdb`.`cmdb_month_report_vm_memory_utzlow` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `bizSystem` VARCHAR(40) NOT NULL COMMENT '业务系统名称',
  `name` VARCHAR(40) DEFAULT NULL COMMENT '联系人',
  `phone` VARCHAR(40) DEFAULT NULL COMMENT '联系电话',
  `department2` VARCHAR(40) DEFAULT NULL COMMENT '归属部门（二级）',
  `department1` VARCHAR(40) DEFAULT NULL COMMENT '所属租户（一级）',
  `idcType` VARCHAR(40) NOT NULL COMMENT '所属资源池名称',
  `vm_dist_count` VARCHAR(40) DEFAULT NULL COMMENT '分配虚拟机计算资源数量（台）',
  `vm_dist_memory_count` VARCHAR(40) DEFAULT NULL COMMENT '分配虚拟机计算资源内存数量（G）',
  `alert_vm_memory_max` VARCHAR(40) DEFAULT NULL COMMENT '分配虚拟机计算资源内存峰值利用率（%）',
  `month` VARCHAR(40) NOT NULL COMMENT '统计月份',
  PRIMARY KEY (`bizSystem`,`idcType`,`month`),
  UNIQUE KEY `CMDB_MR_ADEP_IDX_01` (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='运营月报-各资源池租户虚拟计算资源内存峰值利用率低于30%';


DROP TABLE IF EXISTS `cmdb`.`cmdb_month_report_diststorage_utzlow`;
CREATE TABLE `cmdb`.`cmdb_month_report_diststorage_utzlow` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `bizSystem` VARCHAR(40) NOT NULL COMMENT '业务系统名称',
  `name` VARCHAR(40) DEFAULT NULL COMMENT '联系人',
  `phone` VARCHAR(40) DEFAULT NULL COMMENT '联系电话',
  `department2` VARCHAR(40) DEFAULT NULL COMMENT '归属部门（二级）',
  `department1` VARCHAR(40) DEFAULT NULL COMMENT '所属租户（一级）',
  `idcType` VARCHAR(40) NOT NULL COMMENT '所属资源池名称',
  `storage_dist_type` VARCHAR(40) DEFAULT NULL COMMENT '分配的存储类型',
  `storage_dist_count` VARCHAR(40) DEFAULT NULL COMMENT '分配存储算资源数量（T）',
  `storage_vm_utz` VARCHAR(40) DEFAULT NULL COMMENT '云存储资源利用率(%)',
  `month` VARCHAR(40) NOT NULL COMMENT '统计月份',
  PRIMARY KEY (`bizSystem`,`idcType`,`month`),
  UNIQUE KEY `CMDB_MR_ADEP_IDX_01` (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='运营月报-各资源池租户分布式存储资源利用率低于40%';

DROP TABLE IF EXISTS `cmdb`.`cmdb_month_report_gpu`;
CREATE TABLE `cmdb_month_report_gpu` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `idcType` VARCHAR(40) NOT NULL COMMENT '资源池名称',
  `m_gpu_count` VARCHAR(40) DEFAULT NULL COMMENT '截至当月历史累计已建GPU设备总量（台）（资源池已交维） ',
  `m_dist_count` VARCHAR(40) DEFAULT NULL COMMENT '截至当月历史累计已分配设备总量（台）（资源池已交维设备分配）',
  `bpm_plan_apply_count` VARCHAR(40) DEFAULT NULL COMMENT '本月各系统计划申请资源合计（台）（工单申请）',
  `bpm_plan_apply_reply_count` VARCHAR(40) DEFAULT NULL COMMENT '本月各各系统计划申请资源批复合计（台）（工单申请中批复）',
  `bpm_apply_dist_count` VARCHAR(40) DEFAULT NULL COMMENT '本月各系统申请并分配设备总量（台）',
  `bpm_apply_undist_count` VARCHAR(40) DEFAULT NULL COMMENT '本月申请未分配设备总量（台）',
  `bpm_lm_apply_dist_count` VARCHAR(40) DEFAULT NULL COMMENT '往月申请本月分配设备总量（台）',
  `bpm_lm_apply_undist_count` VARCHAR(40) DEFAULT NULL COMMENT '往月申请本月仍未分配设备总量（台）',
  `unm_gpu_count` VARCHAR(40) DEFAULT NULL COMMENT '在建设备总量（台）（未交维部分）',
  `unm_gpu_predist_count` VARCHAR(40) DEFAULT NULL COMMENT '在建已预分配总量（台）（未交维部分）',
  `surplus_count` VARCHAR(40) DEFAULT NULL COMMENT '已建剩余设备总量（台）',
  `distribute_rate` VARCHAR(40) DEFAULT NULL COMMENT '分配率',
  `month` VARCHAR(40) NOT NULL COMMENT '统计月份',
  PRIMARY KEY (`idcType`,`month`),
  UNIQUE KEY `CMDB_MR_GPU_IDX_01` (`id`)
) ENGINE=INNODB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='运营月报-资源概况-GPU设备总量'







