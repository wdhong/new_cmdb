ALTER TABLE `alert_alerts`
ADD COLUMN `notify_status`  varchar(2) NULL DEFAULT 0 COMMENT '告警通知状态，0-未通知，1-已通知' AFTER `operate_status`;

ALTER TABLE `alert_alerts_his`
ADD COLUMN `operate_status`  varchar(2) NULL DEFAULT 0 COMMENT '操作状态' AFTER `order_status`;

ALTER TABLE `alert_alerts_his`
ADD COLUMN `notify_status`  varchar(2) NULL DEFAULT 0 COMMENT '告警通知状态，0-未通知，1-已通知' AFTER `operate_status`;

ALTER TABLE `alert_derive_alerts`
ADD COLUMN `item_key`  varchar(128) NULL COMMENT '监控项' AFTER `moni_object`;

ALTER TABLE `alert_derive_alerts`
ADD COLUMN `key_comment`  varchar(256) NULL COMMENT '监控项描述' AFTER `item_key`;

ALTER TABLE `alert_isolate_alerts`
ADD COLUMN `item_key`  varchar(128) NULL COMMENT '监控项' AFTER `moni_object`;

ALTER TABLE `alert_isolate_alerts`
ADD COLUMN `key_comment`  varchar(256) NULL COMMENT '监控项描述' AFTER `item_key`;

ALTER TABLE `alert_primary_secondary_alerts`
ADD COLUMN `item_key`  varchar(128) NULL COMMENT '监控项' AFTER `moni_object`;

ALTER TABLE `alert_primary_secondary_alerts`
ADD COLUMN `key_comment`  varchar(256) NULL COMMENT '监控项描述' AFTER `item_key`;

INSERT INTO `alert_filter_option` (`id`, `name`, `type`, `code`, `operate`, `source`, `content`, `method`, `status`, `jdbc_type`, `query_type`) VALUES ('13', '通知状态', 'select', 'notify_status', '等于,不等于,包含,不包含', '', '[{\"name\": \"未通知\", \"value\": \"0\"}, {\"name\": \"已通知\", \"value\": \"1\"}]', '', '1', 'string', '');
INSERT INTO `alert_filter_option` (`id`, `name`, `type`, `code`, `operate`, `source`, `content`, `method`, `status`, `jdbc_type`, `query_type`) VALUES ('14', '操作状态', 'select', 'operate_status', '等于,不等于,包含,不包含', '', '[{\"name\": \"待确认\", \"value\": \"0\"}, {\"name\": \"已确认\", \"value\": \"1\"}, {\"name\": \"待解决\", \"value\": \"2\"}, {\"name\": \"已处理\", \"value\": \"3\"}]', '', '1', 'string', '');
