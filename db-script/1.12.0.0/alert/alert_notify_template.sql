/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2020-03-30 17:39:31
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_notify_template
-- ----------------------------
DROP TABLE IF EXISTS `alert_notify_template`;
CREATE TABLE `alert_notify_template` (
  `id` varchar(64) NOT NULL COMMENT 'id',
  `template_name` varchar(128) NOT NULL COMMENT '模板名称',
  `sms_template` varchar(2000) DEFAULT NULL COMMENT '短信模板',
  `email_template` varchar(2000) DEFAULT NULL COMMENT '邮件模板',
  `is_email_merge` varchar(2) DEFAULT NULL COMMENT '是否多条合并',
  `email_merge_template` varchar(2000) DEFAULT NULL COMMENT '合并模板',
  `is_delete` varchar(2) DEFAULT NULL COMMENT '是否删除，0-删除，1-未删除',
  `creater` varchar(255) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updater` varchar(255) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警通知模板表';

-- ----------------------------
-- Records of alert_notify_template
-- ----------------------------
INSERT INTO `alert_notify_template` VALUES ('1', 'alert_template', '您好：\\n\\t 告警通知信息如下，请尽快处理：\\n\\t @{cur_moni_time}，【资源池：@{idc_type}】，【@{biz_sys}】，【@{device_class}-@{device_type}：@{device_ip}】，【设备描述：@{device_description}  设备名称：@{host_name}】，产生【@{alert_level}告警 @{moni_index}】，【告警值：@{cur_moni_value}】', '您好：\\n\\t 告警通知信息如下，请尽快处理：@{collectionPosition}', '1', '\\n\\t @{cur_moni_time}，【资源池：@{idc_type}】，【@{biz_sys}】，【@{device_class}-@{device_type}：@{device_ip}】，【设备描述：@{device_description}  设备名称：@{host_name}】，产生【@{alert_level}告警 @{moni_index}】，【告警值：@{cur_moni_value}】', '1', 'baiwenping', '2020-03-30 17:38:59', 'baiwenping', '2020-03-30 17:39:03');
