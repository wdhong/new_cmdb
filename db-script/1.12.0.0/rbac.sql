ALTER TABLE `sys_manage`
ADD COLUMN `manage_code`  varchar(128) NOT NULL COMMENT '业务系统编码' AFTER `name`;

UPDATE sys_manage set manage_code='it_devops_manage_platform' where id='fc57247e-bcc2-4773-8033-d0e01e7a3a74';
UPDATE sys_manage set manage_code='pctsot_network_tools' where id='gb90bf02-c141-43b1-9f0e-8b9de8fa869b';
-- 新增周报菜单------
INSERT INTO `sys_menu` (`id`, `parent_id`, `name`, `icon`, `menu_type`, `base`, `path`, `component`, `url`, `sort`, `is_show`, `system_id`, `del_status`, `creater`, `create_time`, `editer`, `update_time`) VALUES ('365bb205-a190-41c7-90a9-067350850a3b', '5e625422-4b8b-4bfb-84cf-ecce908ede1d', '设备类型统计', '', 'vue', '', 'type', 'view.vue', '', '2731', '1', 'fc57247e-bcc2-4773-8033-d0e01e7a3a74', '1', 'alauda', '2020-03-31 17:48:12', 'alauda', '2020-03-31 17:52:25');
INSERT INTO `sys_menu` (`id`, `parent_id`, `name`, `icon`, `menu_type`, `base`, `path`, `component`, `url`, `sort`, `is_show`, `system_id`, `del_status`, `creater`, `create_time`, `editer`, `update_time`) VALUES ('5e625422-4b8b-4bfb-84cf-ecce908ede1d', '6cf21f8a-fd04-4c1f-a44e-239d24cb6359', '设备类型统计', '', 'routers', '/report/alert_weekly/', '', '', '', '2730', '1', 'fc57247e-bcc2-4773-8033-d0e01e7a3a74', '1', 'alauda', '2020-03-31 17:47:30', 'alauda', '2020-03-31 17:51:55');
INSERT INTO `sys_menu` (`id`, `parent_id`, `name`, `icon`, `menu_type`, `base`, `path`, `component`, `url`, `sort`, `is_show`, `system_id`, `del_status`, `creater`, `create_time`, `editer`, `update_time`) VALUES ('cb645632-65c9-4207-8d87-57dc7cd8af85', '9435ed39-b000-4150-9b83-6071e1ae9c81', '告警名称统计', '', 'vue', '', 'title', 'view.vue', '', '2721', '1', 'fc57247e-bcc2-4773-8033-d0e01e7a3a74', '1', 'alauda', '2020-03-31 17:46:21', 'alauda', '2020-03-31 17:51:26');
INSERT INTO `sys_menu` (`id`, `parent_id`, `name`, `icon`, `menu_type`, `base`, `path`, `component`, `url`, `sort`, `is_show`, `system_id`, `del_status`, `creater`, `create_time`, `editer`, `update_time`) VALUES ('9435ed39-b000-4150-9b83-6071e1ae9c81', '6cf21f8a-fd04-4c1f-a44e-239d24cb6359', '告警名称统计', '', 'routers', '/report/alert_weekly/', '', '', '', '2720', '1', 'fc57247e-bcc2-4773-8033-d0e01e7a3a74', '1', 'alauda', '2020-03-31 17:41:19', 'alauda', '2020-03-31 17:50:35');
INSERT INTO `sys_menu` (`id`, `parent_id`, `name`, `icon`, `menu_type`, `base`, `path`, `component`, `url`, `sort`, `is_show`, `system_id`, `del_status`, `creater`, `create_time`, `editer`, `update_time`) VALUES ('0b9d7d45-8ea4-4de6-8c21-b21e3f706721', '1e45bb62-0f89-458b-b579-96b1e49b622d', '资源池/等级统计', '', 'vue', '', 'level', 'view.vue', '', '2711', '1', 'fc57247e-bcc2-4773-8033-d0e01e7a3a74', '1', 'alauda', '2020-03-31 17:39:10', 'alauda', '2020-03-31 17:50:05');
INSERT INTO `sys_menu` (`id`, `parent_id`, `name`, `icon`, `menu_type`, `base`, `path`, `component`, `url`, `sort`, `is_show`, `system_id`, `del_status`, `creater`, `create_time`, `editer`, `update_time`) VALUES ('1e45bb62-0f89-458b-b579-96b1e49b622d', '6cf21f8a-fd04-4c1f-a44e-239d24cb6359', '资源池/等级统计', '', 'routers', '/report/alert_weekly/', '', '', '', '2710', '1', 'fc57247e-bcc2-4773-8033-d0e01e7a3a74', '1', 'alauda', '2020-03-31 17:36:40', 'alauda', '2020-03-31 17:49:38');
INSERT INTO `sys_menu` (`id`, `parent_id`, `name`, `icon`, `menu_type`, `base`, `path`, `component`, `url`, `sort`, `is_show`, `system_id`, `del_status`, `creater`, `create_time`, `editer`, `update_time`) VALUES ('6cf21f8a-fd04-4c1f-a44e-239d24cb6359', 'c229f03e-e6f9-49ff-b633-fc21aca57dbd', '周报', '', 'children', '/iframe', '', '', '', '2700', '1', 'fc57247e-bcc2-4773-8033-d0e01e7a3a74', '1', 'alauda', '2020-03-31 17:34:51', NULL, NULL);
-- 新增周报菜单权限------
INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('5e625422-4b8b-4bfb-84cf-ecce908ede1d', 'f', '2020-03-31 17:47:30.506000', '设备类型统计', '6cf21f8a-fd04-4c1f-a44e-239d24cb6359');
INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('9435ed39-b000-4150-9b83-6071e1ae9c81', 'f', '2020-03-31 17:41:18.696000', '告警名称统计', '6cf21f8a-fd04-4c1f-a44e-239d24cb6359');
INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('1e45bb62-0f89-458b-b579-96b1e49b622d', 'f', '2020-03-31 17:36:39.633000', '资源池/等级统计', '6cf21f8a-fd04-4c1f-a44e-239d24cb6359');
INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('6cf21f8a-fd04-4c1f-a44e-239d24cb6359', 'f', '2020-03-31 17:34:50.770000', '周报', 'c229f03e-e6f9-49ff-b633-fc21aca57dbd');


-- {{ jinsu add 功能资源数据与菜单绑定
-- 删除原始数据
delete from resource_schema_actions;
delete from resource_schema where resource != 'all';



-- 创建备份数据
create table resource_schema_action_temp
SELECT rsa.resource resource_bak, rsa.`action`, rsa.action_name, rsa.action_type, sm.id resource, sm.parent_id parent_resource
FROM resource_schema_actions rsa
left join  resource_schema rs on rsa.resource = rs.resource
LEFT JOIN sys_menu sm ON rs.name = sm.name;

create table resource_schema_temp
SELECT rs.resource resource_bak, rs.general, rs.created_at, rs.name,rs.parent_resource parent_resource_bak, sm.id resource, sm.parent_id parent_resource
FROM resource_schema rs
LEFT JOIN sys_menu sm ON rs.name = sm.name and sm.menu_type != 'vue'

-- 初始化resource数据
insert into resource_schema (resource, general, created_at, name, parent_resource)
select sm.id, 'f', sysdate(), sm.name, sm.parent_id from sys_menu sm where sm.del_status = 1 and sm.menu_type != 'vue';
update resource_schema set parent_resource = 'all' where parent_resource = '-1';

INSERT INTO resource_schema_actions (resource, ACTION, action_name, action_type)
SELECT rt.resource, rt.action, rt.action_name, rt.action_type
FROM resource_schema_action_temp rt
WHERE resource IN (
SELECT resource
FROM resource_schema);

-- }}

-- {{ 自动化运维 功能权限添加
-- 脚本管理
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('25b079a6-839b-4f52-a8a5-1d876fa0923a', 'script:create', '创建脚本', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('25b079a6-839b-4f52-a8a5-1d876fa0923a', 'script:update', '修改脚本', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('25b079a6-839b-4f52-a8a5-1d876fa0923a', 'script:delete', '删除脚本', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('25b079a6-839b-4f52-a8a5-1d876fa0923a', 'script:view', '查询脚本', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('25b079a6-839b-4f52-a8a5-1d876fa0923a', 'script:exec', '执行脚本', 0);
-- 快速脚本执行
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('8790d29e-c79c-46fd-87fd-abd2afcf0bab', 'script:create', '创建脚本', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('8790d29e-c79c-46fd-87fd-abd2afcf0bab', 'script:exec', '执行脚本', 0);


-- 执行历史
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('8f01ada4-a29a-4077-96b2-eaaae772d2f4', 'pipelineInstance:view', '执行历史查询', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('8f01ada4-a29a-4077-96b2-eaaae772d2f4', 'pipeline:copy', '作业克隆', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('8f01ada4-a29a-4077-96b2-eaaae772d2f4', 'pipeline:reviewAppley', '指令审核申请', 0);

-- YUM源
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('393c93bb-e19c-4b4c-b99f-053135e83fb0', 'yum:create', 'yum文件新增', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('393c93bb-e19c-4b4c-b99f-053135e83fb0', 'yum:update', 'yum文件修改', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('393c93bb-e19c-4b4c-b99f-053135e83fb0', 'yum:delete', 'yum文件删除', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('393c93bb-e19c-4b4c-b99f-053135e83fb0', 'yum:view', 'yum文件查询', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('393c93bb-e19c-4b4c-b99f-053135e83fb0', 'yum:download', 'yum文件下载', 0);
-- YUM文件
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('185e6cfc-44a4-4f22-8cd3-454f78d0f3b3', 'yum:create', 'yum文件新增', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('185e6cfc-44a4-4f22-8cd3-454f78d0f3b3', 'yum:update', 'yum文件修改', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('185e6cfc-44a4-4f22-8cd3-454f78d0f3b3', 'yum:delete', 'yum文件删除', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('185e6cfc-44a4-4f22-8cd3-454f78d0f3b3', 'yum:view', 'yum文件查询', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('185e6cfc-44a4-4f22-8cd3-454f78d0f3b3', 'yum:download', 'yum文件下载', 0);

-- 常用作业管理
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('4b89b7f9-b15d-4834-a62d-7746a52133a1', 'pipeline:view', '作业查询', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('4b89b7f9-b15d-4834-a62d-7746a52133a1', 'pipeline:create', '作业新增', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('4b89b7f9-b15d-4834-a62d-7746a52133a1', 'pipeline:update', '作业修改', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('4b89b7f9-b15d-4834-a62d-7746a52133a1', 'pipeline:delete', '作业删除', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('4b89b7f9-b15d-4834-a62d-7746a52133a1', 'pipeline:exec', '作业执行', 0);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('4b89b7f9-b15d-4834-a62d-7746a52133a1', 'timedPipeline:create', '作业定时新增', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('4b89b7f9-b15d-4834-a62d-7746a52133a1', 'pipeline:copy', '作业克隆', 1);
-- 新增作业
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('0a13d83f-8cfc-4f76-bb74-22eeb8b80549', 'pipeline:create', '作业新增', 1);
-- 定时作业
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('0a6e1e6d-a394-420f-bd5b-c2c11e88e6bc', 'timedPipeline:create', '作业定时新增', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('0a6e1e6d-a394-420f-bd5b-c2c11e88e6bc', 'timedPipeline:view', '作业定时查询', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('0a6e1e6d-a394-420f-bd5b-c2c11e88e6bc', 'timedPipeline:update', '作业定时修改', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('0a6e1e6d-a394-420f-bd5b-c2c11e88e6bc', 'timedPipeline:delete', '作业定时删除', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('0a6e1e6d-a394-420f-bd5b-c2c11e88e6bc', 'timedPipeline:updateStatus', '作业定时启动/暂停', 1);

-- 快速文件分发
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('8790d29e-c79c-46fd-87fd-abd2afcf0bab', 'fileFistribution:exec', '执行快速文件分发', 1);
-- 场景管理
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('a5ad9560-9c5d-4d81-9448-be681d99615b', 'scenes:create', '场景新增', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('a5ad9560-9c5d-4d81-9448-be681d99615b', 'scenes:view', '场景新增', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('a5ad9560-9c5d-4d81-9448-be681d99615b', 'scenes:update', '场景修改', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('a5ad9560-9c5d-4d81-9448-be681d99615b', 'scenes:delete', '场景删除', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('a5ad9560-9c5d-4d81-9448-be681d99615b', 'pipeline:exec', '作业执行', 0);
-- 巡检任务管理
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('d3840165-a6f4-4e8d-aa6d-0c57237cd378', 'task:exec', '立即执行', 0);
-- 巡检报告管理
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('75ce949b-59a4-4c33-b120-44106e60ba85', 'report:regenarate', '重新生成', 0);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('75ce949b-59a4-4c33-b120-44106e60ba85', 'report:fileUpload', '上传报告', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('75ce949b-59a4-4c33-b120-44106e60ba85', 'report:fileDownload', '下载报告', 1);

-- 任务运行日志
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('97f644b1-1cdd-4fcc-a141-920d98ab0c58', 'report:view', '报告查询', 1);

-- 敏感指令
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('954e5e61-dcb1-41f9-98b5-d0ab0f0a97f0', 'sensitive:view', '敏感指令查询', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('954e5e61-dcb1-41f9-98b5-d0ab0f0a97f0', 'sensitive:update', '敏感指令修改', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('954e5e61-dcb1-41f9-98b5-d0ab0f0a97f0', 'sensitive:create', '敏感指令新增', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('954e5e61-dcb1-41f9-98b5-d0ab0f0a97f0', 'sensitive:delete', '敏感指令删除', 1);
-- 赋权审核
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('85c2d11f-5ed1-43a5-ad9f-29d1aa7736e3', 'sensitiveReview:view', '敏感指令审核查询', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('85c2d11f-5ed1-43a5-ad9f-29d1aa7736e3', 'sensitiveReview:review', '敏感指令审核操作', 1);
-- 分组管理
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('619bfa2d-6c08-4e42-851e-545938589555', 'group:view', '分组查询', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('619bfa2d-6c08-4e42-851e-545938589555', 'group:create', '分组新增', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('619bfa2d-6c08-4e42-851e-545938589555', 'group:update', '分组修改', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('619bfa2d-6c08-4e42-851e-545938589555', 'group:delete', '分组删除', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('619bfa2d-6c08-4e42-851e-545938589555', 'group:export', '分组导出', 1);
-- 自愈管理
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('1f82e4fe-6126-467c-9b3f-b0c1c1dec07c', 'selfHealing:create', '自愈新增', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('1f82e4fe-6126-467c-9b3f-b0c1c1dec07c', 'selfHealing:view', '自愈查询', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('1f82e4fe-6126-467c-9b3f-b0c1c1dec07c', 'selfHealing:update', '自愈修改', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('1f82e4fe-6126-467c-9b3f-b0c1c1dec07c', 'selfHealing:delete', '自愈删除', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('1f82e4fe-6126-467c-9b3f-b0c1c1dec07c', 'selfHealiing:copy', '自愈复制', 1);
-- 故障自愈日志
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('343cfadd-be02-4bff-a726-fd07d665117f', 'selfHealingLog:review', '故障自愈日志查询', 1);

-- }}
