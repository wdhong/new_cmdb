/*
 Navicat Premium Data Transfer

 Source Server         : 10.12.70.40
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : 10.12.70.40:3306
 Source Schema         : mirror

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 31/03/2020 19:39:49
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for alert_model
-- ----------------------------
DROP TABLE IF EXISTS `alert_model`;
CREATE TABLE `alert_model`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '模型id',
  `model_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模型名称',
  `table_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据表名称',
  `sort` int(255) NOT NULL COMMENT '排序',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `creator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '告警模型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of alert_model
-- ----------------------------
INSERT INTO `alert_model` VALUES ('987df07a-2299-4ea8-a053-4d038f16c9cd', '历史告警', 'alert_alerts_his', 2, '', 'alauda', '2020-03-11 13:56:05');
INSERT INTO `alert_model` VALUES ('c487abe6-d104-4edb-9103-27c0c603b379', '次要告警日志', 'alert_primary_secondary_alerts', 5, '', 'alauda', '2020-03-13 15:55:28');
INSERT INTO `alert_model` VALUES ('cea93af7-511e-4dde-bb2d-1663f4df073d', '告警衍生日志', 'alert_derive_alerts', 4, '', 'alauda', '2020-03-13 15:55:15');
INSERT INTO `alert_model` VALUES ('dc21f101-f8bd-4641-b1b8-559ca7dcbf74', '监控告警', 'alert_alerts', 1, '', 'alauda', '2020-03-11 13:53:36');
INSERT INTO `alert_model` VALUES ('f3e15db8-f9f8-4d45-8d23-3a09f0cc420c', '告警屏蔽日志', 'alert_isolate_alerts', 3, '', 'alauda', '2020-03-13 15:54:58');

SET FOREIGN_KEY_CHECKS = 1;
