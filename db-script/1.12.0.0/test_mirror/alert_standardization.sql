/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.38
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2020-03-25 10:09:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_standardization
-- ----------------------------
DROP TABLE IF EXISTS `alert_standardization`;
CREATE TABLE `alert_standardization` (
  `id` tinyint(12) unsigned NOT NULL AUTO_INCREMENT,
  `conditions` varchar(255) DEFAULT NULL COMMENT '拼的查询条件:and idc_type=''信息港资源池''and source_room=''3050''',
  `source` varchar(64) DEFAULT NULL COMMENT '来源	比如：东华..',
  `displayCols` varchar(2000) DEFAULT NULL COMMENT '需要的字段:source_room,idc_type',
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `topic` varchar(255) DEFAULT NULL COMMENT 'kafka主题',
  `status` tinyint(1) DEFAULT '1' COMMENT '1有效0失效',
  `extraCols` varchar(255) DEFAULT NULL COMMENT '其他返回字段：2 aa,''东华''  from',
  `countType` tinyint(1) DEFAULT NULL COMMENT '统计类型：null全部，0实时告警1历史告警',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8;
