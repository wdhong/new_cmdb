/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.38
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2020-03-25 10:09:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for kpi_book_item
-- ----------------------------
DROP TABLE IF EXISTS `kpi_book_item`;
CREATE TABLE `kpi_book_item` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `idc_type` varchar(255) NOT NULL COMMENT '设备类型',
  `pod` varchar(255) DEFAULT NULL COMMENT '多个逗号分隔',
  `roomId` varchar(255) DEFAULT NULL COMMENT '多个逗号分隔',
  `keys_value` varchar(4000) NOT NULL COMMENT '多个,,/;分隔',
  `kpi_id` int(12) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `kpi_id` (`kpi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='东华订阅性能数据配置';
