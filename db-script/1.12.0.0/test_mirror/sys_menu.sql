DELETE FROM `sys_menu` WHERE name like '%Http监控%';
INSERT INTO `sys_menu`(`id`, `parent_id`, `name`, `icon`, `menu_type`, `base`, `path`, `component`, `url`, `sort`, `is_show`, `system_id`, `del_status`, `creater`, `create_time`, `editer`, `update_time`) VALUES ('e05ed52d-fd5b-4b90-89d3-1929feb6f4cc', 'f17171f5-6154-429b-b21a-ce16a0c2a37a', 'Http监控', '', 'vue', '', '/http', 'list.vue', '', 1531, '1', 'fc57247e-bcc2-4773-8033-d0e01e7a3a74', '1', 'alauda', '2020-03-27 11:47:05', NULL, NULL);
INSERT INTO `sys_menu`(`id`, `parent_id`, `name`, `icon`, `menu_type`, `base`, `path`, `component`, `url`, `sort`, `is_show`, `system_id`, `del_status`, `creater`, `create_time`, `editer`, `update_time`) VALUES ('f17171f5-6154-429b-b21a-ce16a0c2a37a', 'c957b80a-1996-489b-ae53-8764484cf401', 'Http监控', '', 'routers', 'mirror_alert', '', '', '', 1530, '1', 'fc57247e-bcc2-4773-8033-d0e01e7a3a74', '1', 'alauda', '2020-03-27 11:46:07', NULL, NULL);

DELETE FROM `sys_menu` WHERE name like '%我的告警%';
INSERT INTO `sys_menu`(`id`, `parent_id`, `name`, `icon`, `menu_type`, `base`, `path`, `component`, `url`, `sort`, `is_show`, `system_id`, `del_status`, `creater`, `create_time`, `editer`, `update_time`) 
VALUES ('6206bf39-c55f-45f8-9736-0a057e1cc627', 'bb856a89-ea3c-424e-bcbc-f79e7fe61918', '我的告警', '', 'vue', '', '/list', 'mirror-main-alert-list.vue', '', 2161, '1', 'fc57247e-bcc2-4773-8033-d0e01e7a3a74', '1', 'alauda', '2020-03-27 10:59:10', NULL, NULL);
INSERT INTO `sys_menu`(`id`, `parent_id`, `name`, `icon`, `menu_type`, `base`, `path`, `component`, `url`, `sort`, `is_show`, `system_id`, `del_status`, `creater`, `create_time`, `editer`, `update_time`) 
VALUES ('bb856a89-ea3c-424e-bcbc-f79e7fe61918', 'e9f74ae0-2c83-4f0b-a92d-3326e4146557', '我的告警', '', 'routers', '/main_alert', '', '', '', 2160, '1', 'fc57247e-bcc2-4773-8033-d0e01e7a3a74', '1', 'alauda', '2020-03-27 10:58:10', 'alauda', '2020-03-27 10:59:30');

DELETE FROM `sys_menu` WHERE name like '%告警收敛看板%';
INSERT INTO `sys_menu`(`id`, `parent_id`, `name`, `icon`, `menu_type`, `base`, `path`, `component`, `url`, `sort`, `is_show`, `system_id`, `del_status`, `creater`, `create_time`, `editer`, `update_time`) VALUES ('0cca940a-e07d-4b67-b4d3-41f3e3f00bf8', '68176d20-6ef7-4546-8139-56d361617500', '告警收敛看板', '', 'vue', '', '/list', 'alert-intelligent-list.vue', '', 2161, '1', 'fc57247e-bcc2-4773-8033-d0e01e7a3a74', '1', 'alauda', '2020-03-27 11:33:00', NULL, NULL);
INSERT INTO `sys_menu`(`id`, `parent_id`, `name`, `icon`, `menu_type`, `base`, `path`, `component`, `url`, `sort`, `is_show`, `system_id`, `del_status`, `creater`, `create_time`, `editer`, `update_time`) VALUES ('68176d20-6ef7-4546-8139-56d361617500', 'e9f74ae0-2c83-4f0b-a92d-3326e4146557', '告警收敛看板', '', 'routers', '/alert_intelligent', '', '', '', 2170, '1', 'fc57247e-bcc2-4773-8033-d0e01e7a3a74', '1', 'alauda', '2020-03-27 11:32:06', NULL, NULL);

DELETE FROM `sys_menu` WHERE name like '%扫描对账%';
INSERT INTO `sys_menu`(`id`, `parent_id`, `name`, `icon`, `menu_type`, `base`, `path`, `component`, `url`, `sort`, `is_show`, `system_id`, `del_status`, `creater`, `create_time`, `editer`, `update_time`) VALUES ('5d936225-0941-4f99-8114-c076b861285e', 'e9f74ae0-2c83-4f0b-a92d-3326e4146557', '告警扫描对账', '', 'routers', '/scan_comparision', '', '', '', 2180, '1', 'fc57247e-bcc2-4773-8033-d0e01e7a3a74', '1', 'alauda', '2020-03-27 11:35:59', NULL, NULL);
INSERT INTO `sys_menu`(`id`, `parent_id`, `name`, `icon`, `menu_type`, `base`, `path`, `component`, `url`, `sort`, `is_show`, `system_id`, `del_status`, `creater`, `create_time`, `editer`, `update_time`) VALUES ('f2ff9bff-1011-45bd-9655-f7ce62dce7e1', '5d936225-0941-4f99-8114-c076b861285e', '告警扫描对账', '', 'vue', '', '/list', 'alert-scan-comparision-list.vue', '', 2181, '1', 'fc57247e-bcc2-4773-8033-d0e01e7a3a74', '1', 'alauda', '2020-03-27 11:37:03', NULL, NULL);

DELETE FROM `resource_schema` WHERE name like '%http监控%';
INSERT INTO `resource_schema`(`resource`, `general`, `created_at`, `name`, `parent_resource`) 
VALUES ('f17171f5-6154-429b-b21a-ce16a0c2a37a', 'f', '2020-03-27 11:46:07.430000', 'Http监控', 'c957b80a-1996-489b-ae53-8764484cf401');

DELETE FROM `resource_schema` WHERE name like '%我的告警%';
INSERT INTO `resource_schema`(`resource`, `general`, `created_at`, `name`, `parent_resource`) 
VALUES ('bb856a89-ea3c-424e-bcbc-f79e7fe61918', 'f', '2020-03-26 14:46:00.000000', '我的告警', 'e9f74ae0-2c83-4f0b-a92d-3326e4146557');

DELETE FROM `resource_schema` WHERE name like '%告警收敛看板%';
INSERT INTO `resource_schema`(`resource`, `general`, `created_at`, `name`, `parent_resource`) 
VALUES ('68176d20-6ef7-4546-8139-56d361617500', 'f', '2020-03-27 11:32:06.392000', '告警收敛看板', 'e9f74ae0-2c83-4f0b-a92d-3326e4146557');

DELETE FROM `resource_schema` WHERE name like '%扫描对账%';
INSERT INTO `resource_schema`(`resource`, `general`, `created_at`, `name`, `parent_resource`) 
VALUES ('5d936225-0941-4f99-8114-c076b861285e', 'f', '2020-03-27 11:35:59.019000', '告警扫描对账', 'e9f74ae0-2c83-4f0b-a92d-3326e4146557');

INSERT INTO `sys_menu` (`id`, `parent_id`, `name`, `icon`, `menu_type`, `base`, `path`, `component`, `url`, `sort`, `is_show`, `system_id`, `del_status`, `creater`, `create_time`, `editer`, `update_time`) VALUES ('8bec30fc-fd41-4578-98f4-6c39076a1c4f', 'd035cdab-322e-4a67-96a4-6efd1ee93f73', '告警派单配置列表', '', 'vue', '', '/list', 'alert-auto-order-config-list.vue', '', '2481', '1', 'fc57247e-bcc2-4773-8033-d0e01e7a3a74', '1', 'alauda', '2020-03-18 16:48:32', 'alauda', '2020-03-18 17:22:53');
INSERT INTO `sys_menu` (`id`, `parent_id`, `name`, `icon`, `menu_type`, `base`, `path`, `component`, `url`, `sort`, `is_show`, `system_id`, `del_status`, `creater`, `create_time`, `editer`, `update_time`) VALUES ('a1280f3f-4979-4b93-99f6-795b3118990c', 'd035cdab-322e-4a67-96a4-6efd1ee93f73', '告警派单日志', '', 'vue', '', '/order_log', 'alert-auto-order-config-log.vue', '', '2482', '1', 'fc57247e-bcc2-4773-8033-d0e01e7a3a74', '1', 'alauda', '2020-03-24 17:02:34', NULL, NULL);
INSERT INTO `sys_menu` (`id`, `parent_id`, `name`, `icon`, `menu_type`, `base`, `path`, `component`, `url`, `sort`, `is_show`, `system_id`, `del_status`, `creater`, `create_time`, `editer`, `update_time`) VALUES ('d035cdab-322e-4a67-96a4-6efd1ee93f73', '5a9f295f-9571-4af7-8e4c-6d187d79f58c', '告警派单配置', '', 'routers', '/alert_auto_order', '', '', '', '2480', '1', 'fc57247e-bcc2-4773-8033-d0e01e7a3a74', '1', 'alauda', '2020-03-18 16:46:47', 'alauda', '2020-03-18 17:17:12');
INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('d035cdab-322e-4a67-96a4-6efd1ee93f73', 'f', '2020-03-26 14:46:00.000000', '告警派单配置', '5a9f295f-9571-4af7-8e4c-6d187d79f58c');

INSERT INTO `sys_menu` (`id`, `parent_id`, `name`, `icon`, `menu_type`, `base`, `path`, `component`, `url`, `sort`, `is_show`, `system_id`, `del_status`, `creater`, `create_time`, `editer`, `update_time`) VALUES ('8abfd036-62f0-4d86-9f76-374786c08a1f', '0740d806-01d5-4354-9699-d24d05a0e010', '告警模型', '', 'children', '/mirror_alert', '', '', '', '9600', '1', 'fc57247e-bcc2-4773-8033-d0e01e7a3a74', '1', 'alauda', '2020-03-28 15:12:09', NULL, NULL);
INSERT INTO `sys_menu` (`id`, `parent_id`, `name`, `icon`, `menu_type`, `base`, `path`, `component`, `url`, `sort`, `is_show`, `system_id`, `del_status`, `creater`, `create_time`, `editer`, `update_time`) VALUES ('6030e988-75fa-47bd-8b81-d957ea1d11a6', '8abfd036-62f0-4d86-9f76-374786c08a1f', '告警模型', '', 'routers', '/alert_model_field', '', '', '', '9610', '1', 'fc57247e-bcc2-4773-8033-d0e01e7a3a74', '1', 'alauda', '2020-03-28 15:16:19', NULL, NULL);
INSERT INTO `sys_menu` (`id`, `parent_id`, `name`, `icon`, `menu_type`, `base`, `path`, `component`, `url`, `sort`, `is_show`, `system_id`, `del_status`, `creater`, `create_time`, `editer`, `update_time`) VALUES ('b973c058-2221-4b52-b5d7-aa3d94c1b34e', '6030e988-75fa-47bd-8b81-d957ea1d11a6', '告警模型', '', 'vue', '', '/list', 'model-list.vue', '', '9611', '1', 'fc57247e-bcc2-4773-8033-d0e01e7a3a74', '1', 'alauda', '2020-03-28 15:18:04', NULL, NULL);
INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('8abfd036-62f0-4d86-9f76-374786c08a1f', 'f', '2020-03-28 15:12:08.889000', '告警模型', '0740d806-01d5-4354-9699-d24d05a0e010');






