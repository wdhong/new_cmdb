/*
Navicat MySQL Data Transfer

Source Server         : 现网-ums
Source Server Version : 50729
Source Host           : localhost:9999
Source Database       : test_mirror

Target Server Type    : MYSQL
Target Server Version : 50729
File Encoding         : 65001

Date: 2020-05-07 09:47:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for kpi_config_dict
-- ----------------------------
DROP TABLE IF EXISTS `kpi_config_dict`;
CREATE TABLE `kpi_config_dict` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dict_type` varchar(255) NOT NULL,
  `dict_name` varchar(255) NOT NULL,
  `dict_value` varchar(255) NOT NULL,
  `description` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of kpi_config_dict
-- ----------------------------

-- ----------------------------
-- Table structure for kpi_config_key
-- ----------------------------
DROP TABLE IF EXISTS `kpi_config_key`;
CREATE TABLE `kpi_config_key` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `config_id` varchar(64) NOT NULL,
  `key_name` varchar(255) NOT NULL COMMENT '指标名称',
  `kpi_key` varchar(255) NOT NULL COMMENT '指标',
  `kpi_key_model` varchar(32) NOT NULL COMMENT '检索模式，1-精确，2-右模糊',
  `kpi_key_field` varchar(255) NOT NULL COMMENT '指标所属字段',
  `description` varchar(255) DEFAULT NULL COMMENT '指标描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=754 DEFAULT CHARSET=utf8 COMMENT='监控指标配置管理-指标项';

-- ----------------------------
-- Records of kpi_config_key
-- ----------------------------
INSERT INTO `kpi_config_key` VALUES ('1', '1', 'cpu利用率', 'cpu.usage_average', '1', 'key_', 'cpu利用率');
INSERT INTO `kpi_config_key` VALUES ('2', '1', '内存利用率', 'mem.usage_average', '1', 'key_', '内存利用率');
INSERT INTO `kpi_config_key` VALUES ('3', '1', '网络端口发送速率（KBS）', 'net.if.out', '2', 'key_', 'X86服务器/云主机网络端口发送速率（KBS）');
INSERT INTO `kpi_config_key` VALUES ('4', '1', '网络端口接收速率（KBS）', 'net.if.in', '2', 'key_', 'X86服务器/云主机网络端口接收速率（KBS）');
INSERT INTO `kpi_config_key` VALUES ('5', '1', '已用内存（G）', 'custom.memory.used', '1', 'key_', '已用内存（G）');
INSERT INTO `kpi_config_key` VALUES ('6', '1', 'cpu利用率', 'custom.cpu.avg5.pused', '1', 'key_', 'cpu利用率');
INSERT INTO `kpi_config_key` VALUES ('7', '1', '磁盘使用率', 'diskpused', '1', 'key_', '磁盘使用率');
INSERT INTO `kpi_config_key` VALUES ('8', '1', '已用内存（G）', 'vm.memory.size', '2', 'key_', '云主机已用内存（G）');
INSERT INTO `kpi_config_key` VALUES ('9', '1', '出口带宽利用率', 'ifOutOctets.Pused', '2', 'key_', '出口带宽利用率');
INSERT INTO `kpi_config_key` VALUES ('10', '1', '磁盘读速率（KBS）', 'vfs.dev.read', '2', 'key_', '云主机磁盘读速率（KBS）/硬盘读出IOPS（KBS）');
INSERT INTO `kpi_config_key` VALUES ('11', '1', '磁盘写速率（KBS）', 'vfs.dev.write', '2', 'key_', '云主机磁盘写速率（KBS）/硬盘写入IOPS（KBS）');
INSERT INTO `kpi_config_key` VALUES ('13', '1', '磁盘空间总大小（G）', 'disktotal', '1', 'key_', '云主机磁盘空间总大小（G）');
INSERT INTO `kpi_config_key` VALUES ('14', '1', '磁盘空间已使用大小（G）', 'diskused', '1', 'key_', '云主机磁盘空间已使用大小（G）');
INSERT INTO `kpi_config_key` VALUES ('15', '1', '出口/入口设计带宽', 'ifHighSpeed', '2', 'key_', '网络设备出口/入口设计带宽');
INSERT INTO `kpi_config_key` VALUES ('18', '1', '入口流量总量', 'ifHCInOctets.Total', '2', 'key_', '网络设备入口流量总量');
INSERT INTO `kpi_config_key` VALUES ('19', '1', '出口流量总量', 'ifHCOutOctets.Total', '2', 'key_', '网络设备出口流量总量');
INSERT INTO `kpi_config_key` VALUES ('20', '1', '入口带宽利用率', 'ifInOctets.pused', '2', 'key_', '入口带宽利用率');
INSERT INTO `kpi_config_key` VALUES ('21', '1', 'CPU利用率', 'cpuUsage', '1', 'key_', 'CPU利用率');
INSERT INTO `kpi_config_key` VALUES ('22', '1', '内存利用率', 'memUsage', '1', 'key_', '内存利用率');
INSERT INTO `kpi_config_key` VALUES ('82', '1', '系统的内存占用率', 'custom.memory.pused', '1', 'key_', '系统的内存占用率');
INSERT INTO `kpi_config_key` VALUES ('83', '1', '吞吐量', 'hwSecStatDeviceThroughput', '1', 'key_', '吞吐量');
INSERT INTO `kpi_config_key` VALUES ('84', '1', '主机可达性', 'icmpping', '1', 'key_', '主机可达性');
INSERT INTO `kpi_config_key` VALUES ('85', '1', '主机响应时间', 'icmppingsec', '1', 'key_', '主机响应时间');
INSERT INTO `kpi_config_key` VALUES ('86', '1', '接口接收速率', 'ifHCInOctets', '1', 'key_', '接口接收速率');
INSERT INTO `kpi_config_key` VALUES ('87', '1', '接口发送速率', 'ifHCOutOctets', '1', 'key_', '接口发送速率');
INSERT INTO `kpi_config_key` VALUES ('88', '1', '网络带宽使用速率', 'net.if.traffic.total', '1', 'key_', '网络带宽使用速率');
INSERT INTO `kpi_config_key` VALUES ('89', '1', '运行的进程', 'proc.num', '2', 'key_', '运行的进程');
INSERT INTO `kpi_config_key` VALUES ('90', '1', '当前会话总数', 'SecStatMonTotalSess', '1', 'key_', '当前会话总数');
INSERT INTO `kpi_config_key` VALUES ('91', '1', '服务器端流入速率', 'ServerBytesIn', '1', 'key_', '服务器端流入速率');
INSERT INTO `kpi_config_key` VALUES ('92', '1', '5分钟内单核CPU平均负载', 'system.cpu.load', '2', 'key_', '5分钟内单核CPU平均负载');
INSERT INTO `kpi_config_key` VALUES ('93', '1', 'CPU iowait时间占用的比率', 'system.cpu.util', '2', 'key_', 'CPU iowait时间占用的比率');
INSERT INTO `kpi_config_key` VALUES ('94', '1', '根目录', 'vfs.fs.size', '2', 'key_', '根目录/home目录使用率/home目录使用大小');
INSERT INTO `kpi_config_key` VALUES ('313', '2', 'cpu利用率', 'cpu.usage_average', '1', 'key_', 'cpu利用率');
INSERT INTO `kpi_config_key` VALUES ('314', '2', '内存利用率', 'mem.usage_average', '1', 'key_', '内存利用率');
INSERT INTO `kpi_config_key` VALUES ('315', '2', '网络端口发送速率（KBS）', 'net.if.out', '2', 'key_', 'X86服务器/云主机网络端口发送速率（KBS）');
INSERT INTO `kpi_config_key` VALUES ('316', '2', '网络端口接收速率（KBS）', 'net.if.in', '2', 'key_', 'X86服务器/云主机网络端口接收速率（KBS）');
INSERT INTO `kpi_config_key` VALUES ('317', '2', '已用内存（G）', 'custom.memory.used', '1', 'key_', '已用内存（G）');
INSERT INTO `kpi_config_key` VALUES ('318', '2', 'cpu利用率', 'custom.cpu.avg5.pused', '1', 'key_', 'cpu利用率');
INSERT INTO `kpi_config_key` VALUES ('319', '2', '磁盘使用率', 'diskpused', '1', 'key_', '磁盘使用率');
INSERT INTO `kpi_config_key` VALUES ('320', '2', '已用内存（G）', 'vm.memory.size', '2', 'key_', '云主机已用内存（G）');
INSERT INTO `kpi_config_key` VALUES ('321', '2', '出口带宽利用率', 'ifOutOctets.Pused', '2', 'key_', '出口带宽利用率');
INSERT INTO `kpi_config_key` VALUES ('322', '2', '磁盘读速率（KBS）', 'vfs.dev.read', '2', 'key_', '云主机磁盘读速率（KBS）/硬盘读出IOPS（KBS）');
INSERT INTO `kpi_config_key` VALUES ('323', '2', '磁盘写速率（KBS）', 'vfs.dev.write', '2', 'key_', '云主机磁盘写速率（KBS）/硬盘写入IOPS（KBS）');
INSERT INTO `kpi_config_key` VALUES ('324', '2', '磁盘空间总大小（G）', 'disktotal', '1', 'key_', '云主机磁盘空间总大小（G）');
INSERT INTO `kpi_config_key` VALUES ('325', '2', '磁盘空间已使用大小（G）', 'diskused', '1', 'key_', '云主机磁盘空间已使用大小（G）');
INSERT INTO `kpi_config_key` VALUES ('326', '2', '出口/入口设计带宽', 'ifHighSpeed', '2', 'key_', '网络设备出口/入口设计带宽');
INSERT INTO `kpi_config_key` VALUES ('327', '2', '入口流量总量', 'ifHCInOctets.Total', '2', 'key_', '网络设备入口流量总量');
INSERT INTO `kpi_config_key` VALUES ('328', '2', '出口流量总量', 'ifHCOutOctets.Total', '2', 'key_', '网络设备出口流量总量');
INSERT INTO `kpi_config_key` VALUES ('329', '2', '入口带宽利用率', 'ifInOctets.pused', '2', 'key_', '入口带宽利用率');
INSERT INTO `kpi_config_key` VALUES ('330', '2', 'CPU利用率', 'cpuUsage', '1', 'key_', 'CPU利用率');
INSERT INTO `kpi_config_key` VALUES ('331', '2', '内存利用率', 'memUsage', '1', 'key_', '内存利用率');
INSERT INTO `kpi_config_key` VALUES ('332', '2', '系统的内存占用率', 'custom.memory.pused', '1', 'key_', '系统的内存占用率');
INSERT INTO `kpi_config_key` VALUES ('333', '2', '吞吐量', 'hwSecStatDeviceThroughput', '1', 'key_', '吞吐量');
INSERT INTO `kpi_config_key` VALUES ('334', '2', '主机可达性', 'icmpping', '1', 'key_', '主机可达性');
INSERT INTO `kpi_config_key` VALUES ('335', '2', '主机响应时间', 'icmppingsec', '1', 'key_', '主机响应时间');
INSERT INTO `kpi_config_key` VALUES ('336', '2', '接口接收速率', 'ifHCInOctets', '1', 'key_', '接口接收速率');
INSERT INTO `kpi_config_key` VALUES ('337', '2', '接口发送速率', 'ifHCOutOctets', '1', 'key_', '接口发送速率');
INSERT INTO `kpi_config_key` VALUES ('338', '2', '网络带宽使用速率', 'net.if.traffic.total', '1', 'key_', '网络带宽使用速率');
INSERT INTO `kpi_config_key` VALUES ('339', '2', '运行的进程', 'proc.num', '2', 'key_', '运行的进程');
INSERT INTO `kpi_config_key` VALUES ('340', '2', '当前会话总数', 'SecStatMonTotalSess', '1', 'key_', '当前会话总数');
INSERT INTO `kpi_config_key` VALUES ('341', '2', '服务器端流入速率', 'ServerBytesIn', '1', 'key_', '服务器端流入速率');
INSERT INTO `kpi_config_key` VALUES ('342', '2', '5分钟内单核CPU平均负载', 'system.cpu.load', '2', 'key_', '5分钟内单核CPU平均负载');
INSERT INTO `kpi_config_key` VALUES ('343', '2', 'CPU iowait时间占用的比率', 'system.cpu.util', '2', 'key_', 'CPU iowait时间占用的比率');
INSERT INTO `kpi_config_key` VALUES ('344', '2', '根目录', 'vfs.fs.size', '2', 'key_', '根目录/home目录使用率/home目录使用大小');
INSERT INTO `kpi_config_key` VALUES ('376', '3', 'cpu利用率', 'cpu.usage_average', '1', 'key_', 'cpu利用率');
INSERT INTO `kpi_config_key` VALUES ('377', '3', '内存利用率', 'mem.usage_average', '1', 'key_', '内存利用率');
INSERT INTO `kpi_config_key` VALUES ('378', '3', '网络端口发送速率（KBS）', 'net.if.out', '2', 'key_', 'X86服务器/云主机网络端口发送速率（KBS）');
INSERT INTO `kpi_config_key` VALUES ('379', '3', '网络端口接收速率（KBS）', 'net.if.in', '2', 'key_', 'X86服务器/云主机网络端口接收速率（KBS）');
INSERT INTO `kpi_config_key` VALUES ('380', '3', '已用内存（G）', 'custom.memory.used', '1', 'key_', '已用内存（G）');
INSERT INTO `kpi_config_key` VALUES ('381', '3', 'cpu利用率', 'custom.cpu.avg5.pused', '1', 'key_', 'cpu利用率');
INSERT INTO `kpi_config_key` VALUES ('382', '3', '磁盘使用率', 'diskpused', '1', 'key_', '磁盘使用率');
INSERT INTO `kpi_config_key` VALUES ('383', '3', '已用内存（G）', 'vm.memory.size', '2', 'key_', '云主机已用内存（G）');
INSERT INTO `kpi_config_key` VALUES ('384', '3', '出口带宽利用率', 'ifOutOctets.Pused', '2', 'key_', '出口带宽利用率');
INSERT INTO `kpi_config_key` VALUES ('385', '3', '磁盘读速率（KBS）', 'vfs.dev.read', '2', 'key_', '云主机磁盘读速率（KBS）/硬盘读出IOPS（KBS）');
INSERT INTO `kpi_config_key` VALUES ('386', '3', '磁盘写速率（KBS）', 'vfs.dev.write', '2', 'key_', '云主机磁盘写速率（KBS）/硬盘写入IOPS（KBS）');
INSERT INTO `kpi_config_key` VALUES ('387', '3', '磁盘空间总大小（G）', 'disktotal', '1', 'key_', '云主机磁盘空间总大小（G）');
INSERT INTO `kpi_config_key` VALUES ('388', '3', '磁盘空间已使用大小（G）', 'diskused', '1', 'key_', '云主机磁盘空间已使用大小（G）');
INSERT INTO `kpi_config_key` VALUES ('389', '3', '出口/入口设计带宽', 'ifHighSpeed', '2', 'key_', '网络设备出口/入口设计带宽');
INSERT INTO `kpi_config_key` VALUES ('390', '3', '入口流量总量', 'ifHCInOctets.Total', '2', 'key_', '网络设备入口流量总量');
INSERT INTO `kpi_config_key` VALUES ('391', '3', '出口流量总量', 'ifHCOutOctets.Total', '2', 'key_', '网络设备出口流量总量');
INSERT INTO `kpi_config_key` VALUES ('392', '3', '入口带宽利用率', 'ifInOctets.pused', '2', 'key_', '入口带宽利用率');
INSERT INTO `kpi_config_key` VALUES ('393', '3', 'CPU利用率', 'cpuUsage', '1', 'key_', 'CPU利用率');
INSERT INTO `kpi_config_key` VALUES ('394', '3', '内存利用率', 'memUsage', '1', 'key_', '内存利用率');
INSERT INTO `kpi_config_key` VALUES ('395', '3', '系统的内存占用率', 'custom.memory.pused', '1', 'key_', '系统的内存占用率');
INSERT INTO `kpi_config_key` VALUES ('396', '3', '吞吐量', 'hwSecStatDeviceThroughput', '1', 'key_', '吞吐量');
INSERT INTO `kpi_config_key` VALUES ('397', '3', '主机可达性', 'icmpping', '1', 'key_', '主机可达性');
INSERT INTO `kpi_config_key` VALUES ('398', '3', '主机响应时间', 'icmppingsec', '1', 'key_', '主机响应时间');
INSERT INTO `kpi_config_key` VALUES ('399', '3', '接口接收速率', 'ifHCInOctets', '1', 'key_', '接口接收速率');
INSERT INTO `kpi_config_key` VALUES ('400', '3', '接口发送速率', 'ifHCOutOctets', '1', 'key_', '接口发送速率');
INSERT INTO `kpi_config_key` VALUES ('401', '3', '网络带宽使用速率', 'net.if.traffic.total', '1', 'key_', '网络带宽使用速率');
INSERT INTO `kpi_config_key` VALUES ('402', '3', '运行的进程', 'proc.num', '2', 'key_', '运行的进程');
INSERT INTO `kpi_config_key` VALUES ('403', '3', '当前会话总数', 'SecStatMonTotalSess', '1', 'key_', '当前会话总数');
INSERT INTO `kpi_config_key` VALUES ('404', '3', '服务器端流入速率', 'ServerBytesIn', '1', 'key_', '服务器端流入速率');
INSERT INTO `kpi_config_key` VALUES ('405', '3', '5分钟内单核CPU平均负载', 'system.cpu.load', '2', 'key_', '5分钟内单核CPU平均负载');
INSERT INTO `kpi_config_key` VALUES ('406', '3', 'CPU iowait时间占用的比率', 'system.cpu.util', '2', 'key_', 'CPU iowait时间占用的比率');
INSERT INTO `kpi_config_key` VALUES ('407', '3', '根目录', 'vfs.fs.size', '2', 'key_', '根目录/home目录使用率/home目录使用大小');
INSERT INTO `kpi_config_key` VALUES ('439', '4', 'cpu利用率', 'cpu.usage_average', '1', 'key_', 'cpu利用率');
INSERT INTO `kpi_config_key` VALUES ('440', '4', '内存利用率', 'mem.usage_average', '1', 'key_', '内存利用率');
INSERT INTO `kpi_config_key` VALUES ('441', '4', '网络端口发送速率（KBS）', 'net.if.out', '2', 'key_', 'X86服务器/云主机网络端口发送速率（KBS）');
INSERT INTO `kpi_config_key` VALUES ('442', '4', '网络端口接收速率（KBS）', 'net.if.in', '2', 'key_', 'X86服务器/云主机网络端口接收速率（KBS）');
INSERT INTO `kpi_config_key` VALUES ('443', '4', '已用内存（G）', 'custom.memory.used', '1', 'key_', '已用内存（G）');
INSERT INTO `kpi_config_key` VALUES ('444', '4', 'cpu利用率', 'custom.cpu.avg5.pused', '1', 'key_', 'cpu利用率');
INSERT INTO `kpi_config_key` VALUES ('445', '4', '磁盘使用率', 'diskpused', '1', 'key_', '磁盘使用率');
INSERT INTO `kpi_config_key` VALUES ('446', '4', '已用内存（G）', 'vm.memory.size', '2', 'key_', '云主机已用内存（G）');
INSERT INTO `kpi_config_key` VALUES ('447', '4', '出口带宽利用率', 'ifOutOctets.Pused', '2', 'key_', '出口带宽利用率');
INSERT INTO `kpi_config_key` VALUES ('448', '4', '磁盘读速率（KBS）', 'vfs.dev.read', '2', 'key_', '云主机磁盘读速率（KBS）/硬盘读出IOPS（KBS）');
INSERT INTO `kpi_config_key` VALUES ('449', '4', '磁盘写速率（KBS）', 'vfs.dev.write', '2', 'key_', '云主机磁盘写速率（KBS）/硬盘写入IOPS（KBS）');
INSERT INTO `kpi_config_key` VALUES ('450', '4', '磁盘空间总大小（G）', 'disktotal', '1', 'key_', '云主机磁盘空间总大小（G）');
INSERT INTO `kpi_config_key` VALUES ('451', '4', '磁盘空间已使用大小（G）', 'diskused', '1', 'key_', '云主机磁盘空间已使用大小（G）');
INSERT INTO `kpi_config_key` VALUES ('452', '4', '出口/入口设计带宽', 'ifHighSpeed', '2', 'key_', '网络设备出口/入口设计带宽');
INSERT INTO `kpi_config_key` VALUES ('453', '4', '入口流量总量', 'ifHCInOctets.Total', '2', 'key_', '网络设备入口流量总量');
INSERT INTO `kpi_config_key` VALUES ('454', '4', '出口流量总量', 'ifHCOutOctets.Total', '2', 'key_', '网络设备出口流量总量');
INSERT INTO `kpi_config_key` VALUES ('455', '4', '入口带宽利用率', 'ifInOctets.pused', '2', 'key_', '入口带宽利用率');
INSERT INTO `kpi_config_key` VALUES ('456', '4', 'CPU利用率', 'cpuUsage', '1', 'key_', 'CPU利用率');
INSERT INTO `kpi_config_key` VALUES ('457', '4', '内存利用率', 'memUsage', '1', 'key_', '内存利用率');
INSERT INTO `kpi_config_key` VALUES ('458', '4', '系统的内存占用率', 'custom.memory.pused', '1', 'key_', '系统的内存占用率');
INSERT INTO `kpi_config_key` VALUES ('459', '4', '吞吐量', 'hwSecStatDeviceThroughput', '1', 'key_', '吞吐量');
INSERT INTO `kpi_config_key` VALUES ('460', '4', '主机可达性', 'icmpping', '1', 'key_', '主机可达性');
INSERT INTO `kpi_config_key` VALUES ('461', '4', '主机响应时间', 'icmppingsec', '1', 'key_', '主机响应时间');
INSERT INTO `kpi_config_key` VALUES ('462', '4', '接口接收速率', 'ifHCInOctets', '1', 'key_', '接口接收速率');
INSERT INTO `kpi_config_key` VALUES ('463', '4', '接口发送速率', 'ifHCOutOctets', '1', 'key_', '接口发送速率');
INSERT INTO `kpi_config_key` VALUES ('464', '4', '网络带宽使用速率', 'net.if.traffic.total', '1', 'key_', '网络带宽使用速率');
INSERT INTO `kpi_config_key` VALUES ('465', '4', '运行的进程', 'proc.num', '2', 'key_', '运行的进程');
INSERT INTO `kpi_config_key` VALUES ('466', '4', '当前会话总数', 'SecStatMonTotalSess', '1', 'key_', '当前会话总数');
INSERT INTO `kpi_config_key` VALUES ('467', '4', '服务器端流入速率', 'ServerBytesIn', '1', 'key_', '服务器端流入速率');
INSERT INTO `kpi_config_key` VALUES ('468', '4', '5分钟内单核CPU平均负载', 'system.cpu.load', '2', 'key_', '5分钟内单核CPU平均负载');
INSERT INTO `kpi_config_key` VALUES ('469', '4', 'CPU iowait时间占用的比率', 'system.cpu.util', '2', 'key_', 'CPU iowait时间占用的比率');
INSERT INTO `kpi_config_key` VALUES ('470', '4', '根目录', 'vfs.fs.size', '2', 'key_', '根目录/home目录使用率/home目录使用大小');
INSERT INTO `kpi_config_key` VALUES ('502', '5', 'cpu利用率', 'cpu.usage_average', '1', 'key_', 'cpu利用率');
INSERT INTO `kpi_config_key` VALUES ('503', '5', '内存利用率', 'mem.usage_average', '1', 'key_', '内存利用率');
INSERT INTO `kpi_config_key` VALUES ('504', '5', '网络端口发送速率（KBS）', 'net.if.out', '2', 'key_', 'X86服务器/云主机网络端口发送速率（KBS）');
INSERT INTO `kpi_config_key` VALUES ('505', '5', '网络端口接收速率（KBS）', 'net.if.in', '2', 'key_', 'X86服务器/云主机网络端口接收速率（KBS）');
INSERT INTO `kpi_config_key` VALUES ('506', '5', '已用内存（G）', 'custom.memory.used', '1', 'key_', '已用内存（G）');
INSERT INTO `kpi_config_key` VALUES ('507', '5', 'cpu利用率', 'custom.cpu.avg5.pused', '1', 'key_', 'cpu利用率');
INSERT INTO `kpi_config_key` VALUES ('508', '5', '磁盘使用率', 'diskpused', '1', 'key_', '磁盘使用率');
INSERT INTO `kpi_config_key` VALUES ('509', '5', '已用内存（G）', 'vm.memory.size', '2', 'key_', '云主机已用内存（G）');
INSERT INTO `kpi_config_key` VALUES ('510', '5', '出口带宽利用率', 'ifOutOctets.Pused', '2', 'key_', '出口带宽利用率');
INSERT INTO `kpi_config_key` VALUES ('511', '5', '磁盘读速率（KBS）', 'vfs.dev.read', '2', 'key_', '云主机磁盘读速率（KBS）/硬盘读出IOPS（KBS）');
INSERT INTO `kpi_config_key` VALUES ('512', '5', '磁盘写速率（KBS）', 'vfs.dev.write', '2', 'key_', '云主机磁盘写速率（KBS）/硬盘写入IOPS（KBS）');
INSERT INTO `kpi_config_key` VALUES ('513', '5', '磁盘空间总大小（G）', 'disktotal', '1', 'key_', '云主机磁盘空间总大小（G）');
INSERT INTO `kpi_config_key` VALUES ('514', '5', '磁盘空间已使用大小（G）', 'diskused', '1', 'key_', '云主机磁盘空间已使用大小（G）');
INSERT INTO `kpi_config_key` VALUES ('515', '5', '出口/入口设计带宽', 'ifHighSpeed', '2', 'key_', '网络设备出口/入口设计带宽');
INSERT INTO `kpi_config_key` VALUES ('516', '5', '入口流量总量', 'ifHCInOctets.Total', '2', 'key_', '网络设备入口流量总量');
INSERT INTO `kpi_config_key` VALUES ('517', '5', '出口流量总量', 'ifHCOutOctets.Total', '2', 'key_', '网络设备出口流量总量');
INSERT INTO `kpi_config_key` VALUES ('518', '5', '入口带宽利用率', 'ifInOctets.pused', '2', 'key_', '入口带宽利用率');
INSERT INTO `kpi_config_key` VALUES ('519', '5', 'CPU利用率', 'cpuUsage', '1', 'key_', 'CPU利用率');
INSERT INTO `kpi_config_key` VALUES ('520', '5', '内存利用率', 'memUsage', '1', 'key_', '内存利用率');
INSERT INTO `kpi_config_key` VALUES ('521', '5', '系统的内存占用率', 'custom.memory.pused', '1', 'key_', '系统的内存占用率');
INSERT INTO `kpi_config_key` VALUES ('522', '5', '吞吐量', 'hwSecStatDeviceThroughput', '1', 'key_', '吞吐量');
INSERT INTO `kpi_config_key` VALUES ('523', '5', '主机可达性', 'icmpping', '1', 'key_', '主机可达性');
INSERT INTO `kpi_config_key` VALUES ('524', '5', '主机响应时间', 'icmppingsec', '1', 'key_', '主机响应时间');
INSERT INTO `kpi_config_key` VALUES ('525', '5', '接口接收速率', 'ifHCInOctets', '1', 'key_', '接口接收速率');
INSERT INTO `kpi_config_key` VALUES ('526', '5', '接口发送速率', 'ifHCOutOctets', '1', 'key_', '接口发送速率');
INSERT INTO `kpi_config_key` VALUES ('527', '5', '网络带宽使用速率', 'net.if.traffic.total', '1', 'key_', '网络带宽使用速率');
INSERT INTO `kpi_config_key` VALUES ('528', '5', '运行的进程', 'proc.num', '2', 'key_', '运行的进程');
INSERT INTO `kpi_config_key` VALUES ('529', '5', '当前会话总数', 'SecStatMonTotalSess', '1', 'key_', '当前会话总数');
INSERT INTO `kpi_config_key` VALUES ('530', '5', '服务器端流入速率', 'ServerBytesIn', '1', 'key_', '服务器端流入速率');
INSERT INTO `kpi_config_key` VALUES ('531', '5', '5分钟内单核CPU平均负载', 'system.cpu.load', '2', 'key_', '5分钟内单核CPU平均负载');
INSERT INTO `kpi_config_key` VALUES ('532', '5', 'CPU iowait时间占用的比率', 'system.cpu.util', '2', 'key_', 'CPU iowait时间占用的比率');
INSERT INTO `kpi_config_key` VALUES ('533', '5', '根目录', 'vfs.fs.size', '2', 'key_', '根目录/home目录使用率/home目录使用大小');
INSERT INTO `kpi_config_key` VALUES ('565', '6', 'cpu利用率', 'cpu.usage_average', '1', 'key_', 'cpu利用率');
INSERT INTO `kpi_config_key` VALUES ('566', '6', '内存利用率', 'mem.usage_average', '1', 'key_', '内存利用率');
INSERT INTO `kpi_config_key` VALUES ('567', '6', '网络端口发送速率（KBS）', 'net.if.out', '2', 'key_', 'X86服务器/云主机网络端口发送速率（KBS）');
INSERT INTO `kpi_config_key` VALUES ('568', '6', '网络端口接收速率（KBS）', 'net.if.in', '2', 'key_', 'X86服务器/云主机网络端口接收速率（KBS）');
INSERT INTO `kpi_config_key` VALUES ('569', '6', '已用内存（G）', 'custom.memory.used', '1', 'key_', '已用内存（G）');
INSERT INTO `kpi_config_key` VALUES ('570', '6', 'cpu利用率', 'custom.cpu.avg5.pused', '1', 'key_', 'cpu利用率');
INSERT INTO `kpi_config_key` VALUES ('571', '6', '磁盘使用率', 'diskpused', '1', 'key_', '磁盘使用率');
INSERT INTO `kpi_config_key` VALUES ('572', '6', '已用内存（G）', 'vm.memory.size', '2', 'key_', '云主机已用内存（G）');
INSERT INTO `kpi_config_key` VALUES ('573', '6', '出口带宽利用率', 'ifOutOctets.Pused', '2', 'key_', '出口带宽利用率');
INSERT INTO `kpi_config_key` VALUES ('574', '6', '磁盘读速率（KBS）', 'vfs.dev.read', '2', 'key_', '云主机磁盘读速率（KBS）/硬盘读出IOPS（KBS）');
INSERT INTO `kpi_config_key` VALUES ('575', '6', '磁盘写速率（KBS）', 'vfs.dev.write', '2', 'key_', '云主机磁盘写速率（KBS）/硬盘写入IOPS（KBS）');
INSERT INTO `kpi_config_key` VALUES ('576', '6', '磁盘空间总大小（G）', 'disktotal', '1', 'key_', '云主机磁盘空间总大小（G）');
INSERT INTO `kpi_config_key` VALUES ('577', '6', '磁盘空间已使用大小（G）', 'diskused', '1', 'key_', '云主机磁盘空间已使用大小（G）');
INSERT INTO `kpi_config_key` VALUES ('578', '6', '出口/入口设计带宽', 'ifHighSpeed', '2', 'key_', '网络设备出口/入口设计带宽');
INSERT INTO `kpi_config_key` VALUES ('579', '6', '入口流量总量', 'ifHCInOctets.Total', '2', 'key_', '网络设备入口流量总量');
INSERT INTO `kpi_config_key` VALUES ('580', '6', '出口流量总量', 'ifHCOutOctets.Total', '2', 'key_', '网络设备出口流量总量');
INSERT INTO `kpi_config_key` VALUES ('581', '6', '入口带宽利用率', 'ifInOctets.pused', '2', 'key_', '入口带宽利用率');
INSERT INTO `kpi_config_key` VALUES ('582', '6', 'CPU利用率', 'cpuUsage', '1', 'key_', 'CPU利用率');
INSERT INTO `kpi_config_key` VALUES ('583', '6', '内存利用率', 'memUsage', '1', 'key_', '内存利用率');
INSERT INTO `kpi_config_key` VALUES ('584', '6', '系统的内存占用率', 'custom.memory.pused', '1', 'key_', '系统的内存占用率');
INSERT INTO `kpi_config_key` VALUES ('585', '6', '吞吐量', 'hwSecStatDeviceThroughput', '1', 'key_', '吞吐量');
INSERT INTO `kpi_config_key` VALUES ('586', '6', '主机可达性', 'icmpping', '1', 'key_', '主机可达性');
INSERT INTO `kpi_config_key` VALUES ('587', '6', '主机响应时间', 'icmppingsec', '1', 'key_', '主机响应时间');
INSERT INTO `kpi_config_key` VALUES ('588', '6', '接口接收速率', 'ifHCInOctets', '1', 'key_', '接口接收速率');
INSERT INTO `kpi_config_key` VALUES ('589', '6', '接口发送速率', 'ifHCOutOctets', '1', 'key_', '接口发送速率');
INSERT INTO `kpi_config_key` VALUES ('590', '6', '网络带宽使用速率', 'net.if.traffic.total', '1', 'key_', '网络带宽使用速率');
INSERT INTO `kpi_config_key` VALUES ('591', '6', '运行的进程', 'proc.num', '2', 'key_', '运行的进程');
INSERT INTO `kpi_config_key` VALUES ('592', '6', '当前会话总数', 'SecStatMonTotalSess', '1', 'key_', '当前会话总数');
INSERT INTO `kpi_config_key` VALUES ('593', '6', '服务器端流入速率', 'ServerBytesIn', '1', 'key_', '服务器端流入速率');
INSERT INTO `kpi_config_key` VALUES ('594', '6', '5分钟内单核CPU平均负载', 'system.cpu.load', '2', 'key_', '5分钟内单核CPU平均负载');
INSERT INTO `kpi_config_key` VALUES ('595', '6', 'CPU iowait时间占用的比率', 'system.cpu.util', '2', 'key_', 'CPU iowait时间占用的比率');
INSERT INTO `kpi_config_key` VALUES ('596', '6', '根目录', 'vfs.fs.size', '2', 'key_', '根目录/home目录使用率/home目录使用大小');
INSERT INTO `kpi_config_key` VALUES ('628', '7', 'cpu利用率', 'cpu.usage_average', '1', 'key_', 'cpu利用率');
INSERT INTO `kpi_config_key` VALUES ('629', '7', '内存利用率', 'mem.usage_average', '1', 'key_', '内存利用率');
INSERT INTO `kpi_config_key` VALUES ('630', '7', '网络端口发送速率（KBS）', 'net.if.out', '2', 'key_', 'X86服务器/云主机网络端口发送速率（KBS）');
INSERT INTO `kpi_config_key` VALUES ('631', '7', '网络端口接收速率（KBS）', 'net.if.in', '2', 'key_', 'X86服务器/云主机网络端口接收速率（KBS）');
INSERT INTO `kpi_config_key` VALUES ('632', '7', '已用内存（G）', 'custom.memory.used', '1', 'key_', '已用内存（G）');
INSERT INTO `kpi_config_key` VALUES ('633', '7', 'cpu利用率', 'custom.cpu.avg5.pused', '1', 'key_', 'cpu利用率');
INSERT INTO `kpi_config_key` VALUES ('634', '7', '磁盘使用率', 'diskpused', '1', 'key_', '磁盘使用率');
INSERT INTO `kpi_config_key` VALUES ('635', '7', '已用内存（G）', 'vm.memory.size', '2', 'key_', '云主机已用内存（G）');
INSERT INTO `kpi_config_key` VALUES ('636', '7', '出口带宽利用率', 'ifOutOctets.Pused', '2', 'key_', '出口带宽利用率');
INSERT INTO `kpi_config_key` VALUES ('637', '7', '磁盘读速率（KBS）', 'vfs.dev.read', '2', 'key_', '云主机磁盘读速率（KBS）/硬盘读出IOPS（KBS）');
INSERT INTO `kpi_config_key` VALUES ('638', '7', '磁盘写速率（KBS）', 'vfs.dev.write', '2', 'key_', '云主机磁盘写速率（KBS）/硬盘写入IOPS（KBS）');
INSERT INTO `kpi_config_key` VALUES ('639', '7', '磁盘空间总大小（G）', 'disktotal', '1', 'key_', '云主机磁盘空间总大小（G）');
INSERT INTO `kpi_config_key` VALUES ('640', '7', '磁盘空间已使用大小（G）', 'diskused', '1', 'key_', '云主机磁盘空间已使用大小（G）');
INSERT INTO `kpi_config_key` VALUES ('641', '7', '出口/入口设计带宽', 'ifHighSpeed', '2', 'key_', '网络设备出口/入口设计带宽');
INSERT INTO `kpi_config_key` VALUES ('642', '7', '入口流量总量', 'ifHCInOctets.Total', '2', 'key_', '网络设备入口流量总量');
INSERT INTO `kpi_config_key` VALUES ('643', '7', '出口流量总量', 'ifHCOutOctets.Total', '2', 'key_', '网络设备出口流量总量');
INSERT INTO `kpi_config_key` VALUES ('644', '7', '入口带宽利用率', 'ifInOctets.pused', '2', 'key_', '入口带宽利用率');
INSERT INTO `kpi_config_key` VALUES ('645', '7', 'CPU利用率', 'cpuUsage', '1', 'key_', 'CPU利用率');
INSERT INTO `kpi_config_key` VALUES ('646', '7', '内存利用率', 'memUsage', '1', 'key_', '内存利用率');
INSERT INTO `kpi_config_key` VALUES ('647', '7', '系统的内存占用率', 'custom.memory.pused', '1', 'key_', '系统的内存占用率');
INSERT INTO `kpi_config_key` VALUES ('648', '7', '吞吐量', 'hwSecStatDeviceThroughput', '1', 'key_', '吞吐量');
INSERT INTO `kpi_config_key` VALUES ('649', '7', '主机可达性', 'icmpping', '1', 'key_', '主机可达性');
INSERT INTO `kpi_config_key` VALUES ('650', '7', '主机响应时间', 'icmppingsec', '1', 'key_', '主机响应时间');
INSERT INTO `kpi_config_key` VALUES ('651', '7', '接口接收速率', 'ifHCInOctets', '1', 'key_', '接口接收速率');
INSERT INTO `kpi_config_key` VALUES ('652', '7', '接口发送速率', 'ifHCOutOctets', '1', 'key_', '接口发送速率');
INSERT INTO `kpi_config_key` VALUES ('653', '7', '网络带宽使用速率', 'net.if.traffic.total', '1', 'key_', '网络带宽使用速率');
INSERT INTO `kpi_config_key` VALUES ('654', '7', '运行的进程', 'proc.num', '2', 'key_', '运行的进程');
INSERT INTO `kpi_config_key` VALUES ('655', '7', '当前会话总数', 'SecStatMonTotalSess', '1', 'key_', '当前会话总数');
INSERT INTO `kpi_config_key` VALUES ('656', '7', '服务器端流入速率', 'ServerBytesIn', '1', 'key_', '服务器端流入速率');
INSERT INTO `kpi_config_key` VALUES ('657', '7', '5分钟内单核CPU平均负载', 'system.cpu.load', '2', 'key_', '5分钟内单核CPU平均负载');
INSERT INTO `kpi_config_key` VALUES ('658', '7', 'CPU iowait时间占用的比率', 'system.cpu.util', '2', 'key_', 'CPU iowait时间占用的比率');
INSERT INTO `kpi_config_key` VALUES ('659', '7', '根目录', 'vfs.fs.size', '2', 'key_', '根目录/home目录使用率/home目录使用大小');
INSERT INTO `kpi_config_key` VALUES ('691', '8', 'cpu利用率', 'cpu.usage_average', '1', 'key_', 'cpu利用率');
INSERT INTO `kpi_config_key` VALUES ('692', '8', '内存利用率', 'mem.usage_average', '1', 'key_', '内存利用率');
INSERT INTO `kpi_config_key` VALUES ('693', '8', '网络端口发送速率（KBS）', 'net.if.out', '2', 'key_', 'X86服务器/云主机网络端口发送速率（KBS）');
INSERT INTO `kpi_config_key` VALUES ('694', '8', '网络端口接收速率（KBS）', 'net.if.in', '2', 'key_', 'X86服务器/云主机网络端口接收速率（KBS）');
INSERT INTO `kpi_config_key` VALUES ('695', '8', '已用内存（G）', 'custom.memory.used', '1', 'key_', '已用内存（G）');
INSERT INTO `kpi_config_key` VALUES ('696', '8', 'cpu利用率', 'custom.cpu.avg5.pused', '1', 'key_', 'cpu利用率');
INSERT INTO `kpi_config_key` VALUES ('697', '8', '磁盘使用率', 'diskpused', '1', 'key_', '磁盘使用率');
INSERT INTO `kpi_config_key` VALUES ('698', '8', '已用内存（G）', 'vm.memory.size', '2', 'key_', '云主机已用内存（G）');
INSERT INTO `kpi_config_key` VALUES ('699', '8', '出口带宽利用率', 'ifOutOctets.Pused', '2', 'key_', '出口带宽利用率');
INSERT INTO `kpi_config_key` VALUES ('700', '8', '磁盘读速率（KBS）', 'vfs.dev.read', '2', 'key_', '云主机磁盘读速率（KBS）/硬盘读出IOPS（KBS）');
INSERT INTO `kpi_config_key` VALUES ('701', '8', '磁盘写速率（KBS）', 'vfs.dev.write', '2', 'key_', '云主机磁盘写速率（KBS）/硬盘写入IOPS（KBS）');
INSERT INTO `kpi_config_key` VALUES ('702', '8', '磁盘空间总大小（G）', 'disktotal', '1', 'key_', '云主机磁盘空间总大小（G）');
INSERT INTO `kpi_config_key` VALUES ('703', '8', '磁盘空间已使用大小（G）', 'diskused', '1', 'key_', '云主机磁盘空间已使用大小（G）');
INSERT INTO `kpi_config_key` VALUES ('704', '8', '出口/入口设计带宽', 'ifHighSpeed', '2', 'key_', '网络设备出口/入口设计带宽');
INSERT INTO `kpi_config_key` VALUES ('705', '8', '入口流量总量', 'ifHCInOctets.Total', '2', 'key_', '网络设备入口流量总量');
INSERT INTO `kpi_config_key` VALUES ('706', '8', '出口流量总量', 'ifHCOutOctets.Total', '2', 'key_', '网络设备出口流量总量');
INSERT INTO `kpi_config_key` VALUES ('707', '8', '入口带宽利用率', 'ifInOctets.pused', '2', 'key_', '入口带宽利用率');
INSERT INTO `kpi_config_key` VALUES ('708', '8', 'CPU利用率', 'cpuUsage', '1', 'key_', 'CPU利用率');
INSERT INTO `kpi_config_key` VALUES ('709', '8', '内存利用率', 'memUsage', '1', 'key_', '内存利用率');
INSERT INTO `kpi_config_key` VALUES ('710', '8', '系统的内存占用率', 'custom.memory.pused', '1', 'key_', '系统的内存占用率');
INSERT INTO `kpi_config_key` VALUES ('711', '8', '吞吐量', 'hwSecStatDeviceThroughput', '1', 'key_', '吞吐量');
INSERT INTO `kpi_config_key` VALUES ('712', '8', '主机可达性', 'icmpping', '1', 'key_', '主机可达性');
INSERT INTO `kpi_config_key` VALUES ('713', '8', '主机响应时间', 'icmppingsec', '1', 'key_', '主机响应时间');
INSERT INTO `kpi_config_key` VALUES ('714', '8', '接口接收速率', 'ifHCInOctets', '1', 'key_', '接口接收速率');
INSERT INTO `kpi_config_key` VALUES ('715', '8', '接口发送速率', 'ifHCOutOctets', '1', 'key_', '接口发送速率');
INSERT INTO `kpi_config_key` VALUES ('716', '8', '网络带宽使用速率', 'net.if.traffic.total', '1', 'key_', '网络带宽使用速率');
INSERT INTO `kpi_config_key` VALUES ('717', '8', '运行的进程', 'proc.num', '2', 'key_', '运行的进程');
INSERT INTO `kpi_config_key` VALUES ('718', '8', '当前会话总数', 'SecStatMonTotalSess', '1', 'key_', '当前会话总数');
INSERT INTO `kpi_config_key` VALUES ('719', '8', '服务器端流入速率', 'ServerBytesIn', '1', 'key_', '服务器端流入速率');
INSERT INTO `kpi_config_key` VALUES ('720', '8', '5分钟内单核CPU平均负载', 'system.cpu.load', '2', 'key_', '5分钟内单核CPU平均负载');
INSERT INTO `kpi_config_key` VALUES ('721', '8', 'CPU iowait时间占用的比率', 'system.cpu.util', '2', 'key_', 'CPU iowait时间占用的比率');
INSERT INTO `kpi_config_key` VALUES ('722', '8', '根目录', 'vfs.fs.size', '2', 'key_', '根目录/home目录使用率/home目录使用大小');

-- ----------------------------
-- Table structure for kpi_config_logs
-- ----------------------------
DROP TABLE IF EXISTS `kpi_config_logs`;
CREATE TABLE `kpi_config_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `config_id` varchar(64) NOT NULL,
  `config_name` varchar(255) NOT NULL,
  `tag` varchar(64) DEFAULT NULL COMMENT '标签',
  `from_time` datetime NOT NULL COMMENT '统计开始时间',
  `to_time` datetime NOT NULL COMMENT '统计结束时间',
  `exec_time` datetime NOT NULL COMMENT '执行时间',
  `exec_duration` varchar(64) DEFAULT NULL COMMENT '执行时间',
  `status` varchar(32) DEFAULT NULL COMMENT '状态，success，error',
  `content` varchar(4000) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_c_to` (`config_id`,`status`,`to_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11732 DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for kpi_config_manage
-- ----------------------------
DROP TABLE IF EXISTS `kpi_config_manage`;
CREATE TABLE `kpi_config_manage` (
  `id` varchar(64) NOT NULL,
  `kpi_name` varchar(255) NOT NULL COMMENT '指标配置名称',
  `kpi_type` varchar(32) NOT NULL COMMENT '指标类型，1-hour，2-day，3-month，4-自定义',
  `date_source` varchar(32) NOT NULL COMMENT '统计来源，zabbix、db、es、api',
  `cron` varchar(255) NOT NULL COMMENT 'cron表达式',
  `insert_object` varchar(255) NOT NULL COMMENT '录入对象，如果按时间分按照{yyyyMMdd}格式',
  `insert_object_type` varchar(255) NOT NULL COMMENT '录入对象类型',
  `relation_ci_fields` varchar(512) DEFAULT NULL COMMENT '关联ci字段',
  `exec_object` varchar(255) DEFAULT NULL COMMENT '执行对象，db、zabbix为表，es为索引，api则为请求地址',
  `exec_format_type` varchar(64) DEFAULT NULL COMMENT '执行类型，1-sql，2-dsl，3-自定义表达式，4-内置公式',
  `exec_format` varchar(2000) DEFAULT NULL COMMENT '执行标准格式',
  `exec_filter` varchar(1024) DEFAULT NULL COMMENT '过滤条件',
  `status` varchar(2) DEFAULT '1' COMMENT '状态，1-启用，2-停用',
  `is_delete` varchar(2) DEFAULT NULL COMMENT '是否删除，0-未删除，1-删除',
  `start_time` datetime DEFAULT NULL COMMENT '历史开始时间',
  `duration` int(11) DEFAULT NULL COMMENT '时间周期，单位：小时',
  `end_time` datetime DEFAULT NULL COMMENT '历史结束时间',
  `creater` varchar(255) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updater` varchar(255) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='监控指标配置管理';

-- ----------------------------
-- Records of kpi_config_manage
-- ----------------------------
INSERT INTO `kpi_config_manage` VALUES ('1', 'zabbix小时指标配置-数字', '1', 'zabbix', '0 20/15 * * * ?', 'kpi-hour-uint_{yyyyMMdd}', 'es', 'idcType,bizSystem,device_class,roomId,device_type,department1,department2,pod_name', 'history_uint', '4', null, null, '1', '0', null, null, null, 'baiwenping', '2020-04-14 11:10:52', 'baiwenping', '2020-04-14 11:10:56');
INSERT INTO `kpi_config_manage` VALUES ('2', 'zabbix小时指标配置-百分比', '1', 'zabbix', '0 20/15 * * * ?', 'kpi-hour_{yyyyMMdd}', 'es', 'idcType,bizSystem,device_class,roomId,device_type,department1,department2,pod_name', 'history', '4', null, null, '1', '0', null, null, null, 'baiwenping', '2020-04-14 11:10:52', 'baiwenping', '2020-04-14 11:10:56');
INSERT INTO `kpi_config_manage` VALUES ('3', 'zabbix日指标配置-数字', '2', 'zabbix', '0 35 1/8 * * ?', 'kpi-day-uint_{yyyyMM}', 'es', 'idcType,bizSystem,device_class,roomId,device_type,department1,department2,pod_name', 'history_uint', '4', null, null, '1', '0', null, null, null, 'baiwenping', '2020-04-14 11:10:52', 'baiwenping', '2020-04-14 11:10:56');
INSERT INTO `kpi_config_manage` VALUES ('4', 'zabbix日指标配置-百分比', '2', 'zabbix', '0 35 1/8 * * ?', 'kpi-day_{yyyyMM}', 'es', 'idcType,bizSystem,device_class,roomId,device_type,department1,department2,pod_name', 'history', '4', null, null, '1', '0', null, null, null, 'baiwenping', '2020-04-14 11:10:52', 'baiwenping', '2020-04-14 11:10:56');
INSERT INTO `kpi_config_manage` VALUES ('5', 'zabbix小时指标配置-数字-历史', '4', 'zabbix', '0 */3 * * * ?', 'kpi-hour-uint_{yyyyMMdd}', 'es', 'idcType,bizSystem,device_class,roomId,device_type,department1,department2,pod_name', 'history_uint', '4', null, null, '2', '0', '2020-04-01 00:00:00', '1', '2020-04-28 18:43:13', 'baiwenping', '2020-04-14 11:10:52', 'baiwenping', '2020-04-14 11:10:56');
INSERT INTO `kpi_config_manage` VALUES ('6', 'zabbix小时指标配置-百分比-历史', '4', 'zabbix', '0 */3 * * * ?', 'kpi-hour_{yyyyMMdd}', 'es', 'idcType,bizSystem,device_class,roomId,device_type,department1,department2,pod_name', 'history', '4', null, null, '2', '0', '2020-04-01 00:00:00', '1', '2020-04-28 18:43:19', 'baiwenping', '2020-04-14 11:10:52', 'baiwenping', '2020-04-14 11:10:56');
INSERT INTO `kpi_config_manage` VALUES ('7', 'zabbix日指标配置-数字-历史', '4', 'zabbix', '0 15 */2 * * ?', 'kpi-day-uint_{yyyyMM}', 'es', 'idcType,bizSystem,device_class,roomId,device_type,department1,department2,pod_name', 'history_uint', '4', null, null, '2', '0', '2020-04-01 00:00:00', '24', '2020-04-28 18:54:19', 'baiwenping', '2020-04-14 11:10:52', 'baiwenping', '2020-04-14 11:10:56');
INSERT INTO `kpi_config_manage` VALUES ('8', 'zabbix日指标配置-百分比-历史', '4', 'zabbix', '0 15 */2 * * ?', 'kpi-day_{yyyyMM}', 'es', 'idcType,bizSystem,device_class,roomId,device_type,department1,department2,pod_name', 'history', '4', null, null, '2', '0', '2020-04-01 00:00:00', '24', '2020-04-28 18:54:22', 'baiwenping', '2020-04-14 11:10:52', 'baiwenping', '2020-04-14 11:10:56');

-- ----------------------------
-- Table structure for kpi_result_transfer
-- ----------------------------
DROP TABLE IF EXISTS `kpi_result_transfer`;
CREATE TABLE `kpi_result_transfer` (
  `id` int(64) NOT NULL AUTO_INCREMENT,
  `config_id` varchar(64) DEFAULT NULL,
  `result_transfer_name` varchar(255) NOT NULL COMMENT '名称',
  `field_group` varchar(255) DEFAULT NULL COMMENT '分组',
  `source_field` varchar(255) DEFAULT NULL COMMENT '源字段',
  `target_field` varchar(255) NOT NULL COMMENT '目标字段',
  `transfer_type` varchar(2) DEFAULT NULL COMMENT '转换类型，1-固定值(date为特殊情况)，2-groovy，3-java函数，4-字典转换',
  `transfer_formula` varchar(1024) DEFAULT NULL COMMENT '转换公式',
  `description` varchar(512) DEFAULT NULL COMMENT '描述',
  `is_delete` varchar(2) DEFAULT '0' COMMENT '是否删除，1-删除，0-未删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=370 DEFAULT CHARSET=utf8 COMMENT='监控配置管理结果转换表';

-- ----------------------------
-- Records of kpi_result_transfer
-- ----------------------------
INSERT INTO `kpi_result_transfer` VALUES ('1', null, '监控项id', '公共', 'itemid', 'itemid', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('2', null, '监控时间', '公共', null, 'clock', '1', 'date', null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('3', null, '均值', '公共', 'avg', 'value_avg', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('4', null, '峰值', '公共', 'max', 'value_max', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('5', null, '谷值', '公共', 'min', 'value_min', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('6', null, 'ip', '公共', 'ip', 'host', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('7', null, 'item', '公共', 'key_', 'item', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('8', null, '资源池', '设备', 'idcType', 'idcType', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('9', null, '一级部门', '设备', 'department1', 'department1', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('10', null, '二级部门', '设备', 'department2', 'department2', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('11', null, '业务系统', '设备', 'bizSystem', 'bizSystem', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('12', null, '设备分类', '设备', 'device_class', 'deviceClass', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('13', null, '设备类型', '设备', 'device_type', 'deviceType', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('14', null, '机房', '设备', 'roomId', 'roomId', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('15', null, 'pod池', '设备', 'pod_name', 'podName', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('16', null, '计算时间', '公共', 'datetime', 'datetime', '1', 'dateStr', null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('122', '1', '监控项id', null, 'itemid', 'itemid', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('123', '1', '监控时间', null, null, 'clock', '1', 'date', null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('124', '1', '均值', null, 'avg', 'value_avg', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('125', '1', '峰值', null, 'max', 'value_max', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('126', '1', '谷值', null, 'min', 'value_min', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('127', '1', 'ip', null, 'ip', 'host', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('128', '1', 'item', null, 'key_', 'item', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('129', '1', '资源池', null, 'idcType', 'idcType', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('130', '1', '一级部门', null, 'department1', 'department1', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('131', '1', '二级部门', null, 'department2', 'department2', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('132', '1', '业务系统', null, 'bizSystem', 'bizSystem', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('133', '1', '设备分类', null, 'device_class', 'deviceClass', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('134', '1', '设备类型', null, 'device_type', 'deviceType', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('135', '1', '机房', null, 'roomId', 'roomId', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('136', '1', 'pod池', null, 'pod_name', 'podName', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('137', '1', '计算时间', null, 'datetime', 'datetime', '1', 'dateStr', null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('153', '2', '监控项id', null, 'itemid', 'itemid', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('154', '2', '监控时间', null, null, 'clock', '1', 'date', null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('155', '2', '均值', null, 'avg', 'value_avg', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('156', '2', '峰值', null, 'max', 'value_max', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('157', '2', '谷值', null, 'min', 'value_min', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('158', '2', 'ip', null, 'ip', 'host', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('159', '2', 'item', null, 'key_', 'item', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('160', '2', '资源池', null, 'idcType', 'idcType', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('161', '2', '一级部门', null, 'department1', 'department1', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('162', '2', '二级部门', null, 'department2', 'department2', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('163', '2', '业务系统', null, 'bizSystem', 'bizSystem', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('164', '2', '设备分类', null, 'device_class', 'deviceClass', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('165', '2', '设备类型', null, 'device_type', 'deviceType', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('166', '2', '机房', null, 'roomId', 'roomId', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('167', '2', 'pod池', null, 'pod_name', 'podName', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('168', '2', '计算时间', null, 'datetime', 'datetime', '1', 'dateStr', null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('184', '3', '监控项id', null, 'itemid', 'itemid', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('185', '3', '监控时间', null, null, 'clock', '1', 'date', null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('186', '3', '均值', null, 'avg', 'value_avg', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('187', '3', '峰值', null, 'max', 'value_max', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('188', '3', '谷值', null, 'min', 'value_min', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('189', '3', 'ip', null, 'ip', 'host', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('190', '3', 'item', null, 'key_', 'item', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('191', '3', '资源池', null, 'idcType', 'idcType', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('192', '3', '一级部门', null, 'department1', 'department1', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('193', '3', '二级部门', null, 'department2', 'department2', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('194', '3', '业务系统', null, 'bizSystem', 'bizSystem', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('195', '3', '设备分类', null, 'device_class', 'deviceClass', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('196', '3', '设备类型', null, 'device_type', 'deviceType', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('197', '3', '机房', null, 'roomId', 'roomId', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('198', '3', 'pod池', null, 'pod_name', 'podName', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('199', '3', '计算时间', null, 'datetime', 'datetime', '1', 'dateStr', null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('215', '4', '监控项id', null, 'itemid', 'itemid', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('216', '4', '监控时间', null, null, 'clock', '1', 'date', null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('217', '4', '均值', null, 'avg', 'value_avg', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('218', '4', '峰值', null, 'max', 'value_max', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('219', '4', '谷值', null, 'min', 'value_min', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('220', '4', 'ip', null, 'ip', 'host', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('221', '4', 'item', null, 'key_', 'item', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('222', '4', '资源池', null, 'idcType', 'idcType', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('223', '4', '一级部门', null, 'department1', 'department1', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('224', '4', '二级部门', null, 'department2', 'department2', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('225', '4', '业务系统', null, 'bizSystem', 'bizSystem', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('226', '4', '设备分类', null, 'device_class', 'deviceClass', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('227', '4', '设备类型', null, 'device_type', 'deviceType', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('228', '4', '机房', null, 'roomId', 'roomId', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('229', '4', 'pod池', null, 'pod_name', 'podName', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('230', '4', '计算时间', null, 'datetime', 'datetime', '1', 'dateStr', null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('246', '5', '监控项id', null, 'itemid', 'itemid', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('247', '5', '监控时间', null, null, 'clock', '1', 'date', null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('248', '5', '均值', null, 'avg', 'value_avg', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('249', '5', '峰值', null, 'max', 'value_max', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('250', '5', '谷值', null, 'min', 'value_min', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('251', '5', 'ip', null, 'ip', 'host', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('252', '5', 'item', null, 'key_', 'item', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('253', '5', '资源池', null, 'idcType', 'idcType', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('254', '5', '一级部门', null, 'department1', 'department1', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('255', '5', '二级部门', null, 'department2', 'department2', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('256', '5', '业务系统', null, 'bizSystem', 'bizSystem', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('257', '5', '设备分类', null, 'device_class', 'deviceClass', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('258', '5', '设备类型', null, 'device_type', 'deviceType', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('259', '5', '机房', null, 'roomId', 'roomId', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('260', '5', 'pod池', null, 'pod_name', 'pod_name', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('261', '5', '计算时间', null, 'datetime', 'datetime', '1', 'dateStr', null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('277', '6', '监控项id', null, 'itemid', 'itemid', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('278', '6', '监控时间', null, null, 'clock', '1', 'date', null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('279', '6', '均值', null, 'avg', 'value_avg', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('280', '6', '峰值', null, 'max', 'value_max', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('281', '6', '谷值', null, 'min', 'value_min', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('282', '6', 'ip', null, 'ip', 'host', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('283', '6', 'item', null, 'key_', 'item', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('284', '6', '资源池', null, 'idcType', 'idcType', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('285', '6', '一级部门', null, 'department1', 'department1', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('286', '6', '二级部门', null, 'department2', 'department2', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('287', '6', '业务系统', null, 'bizSystem', 'bizSystem', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('288', '6', '设备分类', null, 'device_class', 'deviceClass', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('289', '6', '设备类型', null, 'device_type', 'deviceType', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('290', '6', '机房', null, 'roomId', 'roomId', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('291', '6', 'pod池', null, 'pod_name', 'pod_name', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('292', '6', '计算时间', null, 'datetime', 'datetime', '1', 'dateStr', null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('308', '7', '监控项id', null, 'itemid', 'itemid', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('309', '7', '监控时间', null, null, 'clock', '1', 'date', null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('310', '7', '均值', null, 'avg', 'value_avg', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('311', '7', '峰值', null, 'max', 'value_max', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('312', '7', '谷值', null, 'min', 'value_min', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('313', '7', 'ip', null, 'ip', 'host', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('314', '7', 'item', null, 'key_', 'item', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('315', '7', '资源池', null, 'idcType', 'idcType', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('316', '7', '一级部门', null, 'department1', 'department1', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('317', '7', '二级部门', null, 'department2', 'department2', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('318', '7', '业务系统', null, 'bizSystem', 'bizSystem', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('319', '7', '设备分类', null, 'device_class', 'deviceClass', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('320', '7', '设备类型', null, 'device_type', 'deviceType', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('321', '7', '机房', null, 'roomId', 'roomId', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('322', '7', 'pod池', null, 'pod_name', 'podName', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('323', '7', '计算时间', null, 'datetime', 'datetime', '1', 'dateStr', null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('339', '8', '监控项id', null, 'itemid', 'itemid', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('340', '8', '监控时间', null, null, 'clock', '1', 'date', null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('341', '8', '均值', null, 'avg', 'value_avg', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('342', '8', '峰值', null, 'max', 'value_max', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('343', '8', '谷值', null, 'min', 'value_min', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('344', '8', 'ip', null, 'ip', 'host', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('345', '8', 'item', null, 'key_', 'item', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('346', '8', '资源池', null, 'idcType', 'idcType', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('347', '8', '一级部门', null, 'department1', 'department1', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('348', '8', '二级部门', null, 'department2', 'department2', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('349', '8', '业务系统', null, 'bizSystem', 'bizSystem', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('350', '8', '设备分类', null, 'device_class', 'deviceClass', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('351', '8', '设备类型', null, 'device_type', 'deviceType', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('352', '8', '机房', null, 'roomId', 'roomId', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('353', '8', 'pod池', null, 'pod_name', 'podName', null, null, null, '0');
INSERT INTO `kpi_result_transfer` VALUES ('354', '8', '计算时间', null, 'datetime', 'datetime', '1', 'dateStr', null, '0');
