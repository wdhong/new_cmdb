/*
SQLyog Ultimate
MySQL - 5.7.30-log : Database - cmdb_online
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cmdb_online` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `cmdb_online`;

/*Table structure for table `cmdb_31_province_report_setting` */
DROP TABLE IF EXISTS `cmdb_31_province_report_setting`;
CREATE TABLE `cmdb_31_province_report_setting` (
  `id` varchar(40) NOT NULL,
  `table_id` varchar(100) NOT NULL COMMENT '表id',
  `resource_group` varchar(100) DEFAULT NULL COMMENT '一级标题',
  `resource_type` varchar(100) DEFAULT NULL COMMENT '二级标题',
  `resource_control_type` varchar(40) DEFAULT NULL COMMENT '表数据类型',
  `calc_valid` text COMMENT '列验证规则',
  `calc_exp` text COMMENT '列计算规则',
  `value_valid` text COMMENT '列取值范围',
  `sort_index` int(11) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cmdb_31_province_report_setting` */

insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('1','1','资源状况','存储服务器数量(台)','int',NULL,NULL,NULL,3);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('10','2','全部资源利用情况','全部物理计算资源内存均峰值利用率(%)','double',NULL,NULL,NULL,8);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('11','2','已分配资源利用情况','已分配物理计算资源CPU均值利用率(%)','double',NULL,NULL,NULL,9);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('12','2','已分配资源利用情况','已分配物理计算资源CPU均峰值利用率(%)','double',NULL,NULL,'',10);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('13','2','已分配资源利用情况','已分配物理计算资源内存均值利用率(%)','double',NULL,NULL,NULL,11);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('14','2','已分配资源利用情况','已分配物理计算资源内存均峰值利用率(%)','double',NULL,NULL,NULL,12);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('15','3','宿主机资源','宿主机数量(台)','int',NULL,NULL,NULL,1);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('16','3','宿主机资源','宿主机CPU总核数(个)','int',NULL,NULL,NULL,2);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('17','3','宿主机资源','宿主机总内存(G)','int',NULL,NULL,NULL,3);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('18','3','宿主机资源','宿主机CPU均值利用率(%)','double',NULL,NULL,NULL,4);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('19','3','宿主机资源','宿主机CPU均峰值利用率(%)','double',NULL,NULL,NULL,5);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('2','1','资源状况','云化存储总容量(T)','double','{\"operator\" : \">=\", \"anotherId\":\"3\", \"tip\": \"存储总量必须大于等于已分配\"}',NULL,NULL,4);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('20','3','宿主机资源','宿主机内存均值利用率(%)','double',NULL,NULL,NULL,6);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('21','3','宿主机资源','宿主机内存峰值利用率(%)','double',NULL,NULL,NULL,7);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('22','3','虚拟机资源','虚机数量(台)','int',NULL,NULL,NULL,8);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('23','3','虚拟机资源','虚机VCPU核数(个)','int',NULL,NULL,NULL,9);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('24','3','虚拟机资源','虚机总内存(G)','int','{\"operator\" : \"<=\", \"anotherId\":\"17\", \"tip\": \"虚拟机总内不能大于宿主机总内存\"  }',NULL,NULL,10);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('25','3','虚拟机资源','虚拟机CPU均值利用率(%)','double',NULL,NULL,NULL,11);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('26','3','虚拟机资源','虚拟机CPU均峰值利用率(%)','double',NULL,NULL,NULL,12);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('27','3','虚拟机资源','虚拟机内存均值利用率(%)','double',NULL,NULL,NULL,13);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('28','3','虚拟机资源','虚拟机内存均峰值利用率(%)','double',NULL,NULL,NULL,14);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('29','3','管理节点','管理节点数量(台)','int',NULL,NULL,NULL,15);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('3','1','资源状况','云化存储已分配(T)','double','{\"operator\" : \"<=\", \"anotherId\":\"2\" , \"tip\": \"存储已分配必须小于等于总量\"}',NULL,NULL,5);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('30','4','网络设备数量','网络设备数量(台)','int',NULL,NULL,NULL,3);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('31','4','网络设备数量','安全设备数量(台)','int',NULL,NULL,NULL,4);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('32','4','出口带宽','CMNET出口带宽(G)','double',NULL,NULL,NULL,5);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('33','4','出口带宽','IP承载网出口带宽(G)','double',NULL,NULL,NULL,6);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('34','5','故障统计','租户系统数','int',NULL,NULL,NULL,3);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('35','5','故障统计','故障数','int',NULL,NULL,NULL,4);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('36','7','故障信息','故障序号','int',NULL,NULL,NULL,3);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('37','7','故障信息','故障名称','string',NULL,NULL,NULL,4);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('38','7','故障信息','故障发生时间','datetime','{\"operator\" : \"<\", \"anotherId\":\"39\", \"tip\": \"发生时间应小于结束时间\"  }',NULL,'yyyy-MM-dd HH:mm:ss',5);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('39','7','故障信息','故障结束时间','datetime','{\"operator\" : \">\", \"anotherId\":\"38\", \"tip\": \"结束时间应大于发生时间\"}',NULL,'yyyy-MM-dd HH:mm:ss',6);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('4','1','资源状况','存储平均利用率(%)','double',NULL,NULL,NULL,6);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('40','7','故障信息','故障影响业务系统数量','int',NULL,NULL,NULL,7);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('41','7','故障信息','备注','string',NULL,NULL,NULL,8);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('42','8','网络策略工单','网络策略工单总数','int','{\"operator\" : \">=\", \"anotherId\":\"43\",\"tip\":\"工单总数不能小于超时工单\"  }',NULL,NULL,1);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('43','8','网络策略工单','超时网络策略工单','int','{\"operator\" : \"<=\", \"anotherId\":\"42\",\"tip\":\"超时工单不能大于工单总数\"  }',NULL,NULL,2);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('44','9','服务工单','服务工单总数','int','{\"operator\" : \">=\", \"anotherId\":\"45\" ,\"tip\":\"工单总数不能小于超时工单\" }',NULL,NULL,2);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('45','9','服务工单','超时服务工单总数','int','{\"operator\" : \"<=\", \"anotherId\":\"44\", \"tip\":\"超时工单不能大于工单总数\"}',NULL,NULL,3);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('46','10','变更工单','变更工单总数','int','{\"operator\" : \">=\", \"anotherId\":\"47\" ,\"tip\":\"工单总数不能小于超时工单\" }',NULL,NULL,2);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('47','10','变更工单','超时变更工单','int','{\"operator\" : \"<=\", \"anotherId\":\"46\",\"tip\":\"超时工单不能大于总数\" }',NULL,NULL,3);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('48','1','资源状况','云化存储分配率(%)','double',NULL,'select round(([3]/[2])*100,2) as count',NULL,7);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('49','2','资源状况','物理计算资源分配率(%)','double',NULL,'select round(([6]/[5])*100,2) as count',NULL,3);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('5','2','资源状况','物理计算资源数量(台)','int','{\"operator\" : \">=\", \"anotherId\":\"6\", \"tip\": \"物理计算资源数量必须大于等于已分配\"}',NULL,NULL,1);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('50','2','资源状况','物理计算资源未分配数量(台)','int',NULL,'select round([5]-[6],2) as count',NULL,4);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('51','3','虚拟机资源','剩余可分配内存(G)','double',NULL,'select truncate([17]-[24],2) as count',NULL,16);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('52','3','虚拟机资源','虚拟计算资源分配率(%)','double',NULL,'select truncate(([22]/[15])*100,2) as count',NULL,17);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('53','6','故障总计','租户系统总数','int',NULL,'SELECT SUM(`34`) as total',NULL,2);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('54','6','故障总计','故障总数',NULL,NULL,'SELECT SUM(`35`) as total',NULL,3);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('55','7','故障信息','故障时长(分钟)','int',NULL,'SELECT TIMESTAMPDIFF(MINUTE ,\'[38]\',\'[39]\') as count',NULL,9);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('56','8','网络策略工单','网络策略工单及时率(%)','double',NULL,'select truncate(([42]-[43])/[42]*100,2) as count',NULL,3);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('57','9','服务工单','服务工单及时率(%)','double',NULL,'select truncate(([44]-[45])/[44]*100,2) as count',NULL,4);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('58','10','变更工单','变更工单及时率(%)','double',NULL,'select truncate(([46]-[47])/[46]*100,2) as count',NULL,4);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('59','8','网络策略工单','网络策略工单及时率得分','double',NULL,'select case when [56]/100*0.4*0.4 > 0.4 then 0.4 else round([56]/100*0.4*0.4,2) end as count',NULL,4);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('6','2','资源状况','已分配物理计算资源数量(台)','int','{\"operator\" : \"<=\", \"anotherId\":\"5\", \"tip\": \"物理计算资源已分配必须小于等于总量\"}',NULL,NULL,2);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('60','10','变更工单','变更工单及时率得分','double',NULL,'select case when ([58]/100+0.1)*0.3*0.4 > 0.4 then 0.4 else round(([58]/100+0.1)*0.3*0.4,2) end as count',NULL,5);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('61','9','服务工单','服务工单及时率得分','double',NULL,'select case when [57]/100*0.3*0.4 > 0.4 then 0.4 else truncate([57]/100*0.3*0.4,2) end as count',NULL,5);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('62','6','故障总计','总体故障时长(分钟) ','int',NULL,'SELECT SUM(`55`) as total',NULL,4);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('63','6','故障总计','总体运行时长(分钟)','int',NULL,'select (sum(`53`)*60*14*DAY(LAST_DAY(CONCAT(\'[month]\',\'-01\')))) as total',NULL,5);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('65','12','省级均峰值利用率','省级IT云已分配给租户的物理服务器CPU均峰值利用率(%)','double',NULL,'select round((sum(`12`/100*`6`)+sum(`26`/100*`22`))/sum(`6`+`22`),2) as total\r\n',NULL,2);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('66','12','省级均峰值利用率','省级IT云已上线运行的物理服务器CPU均峰值利用率(%)','double',NULL,'select round((sum(`5`*`8`/100)+sum(`15`*`19`/100))/sum(`5`+`15`),2) as total\r\n',NULL,3);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('67','6','故障总计','平台可用性(%)','double',NULL,'select round(sum((`63`-`62`)/`63`*100),2) as total',NULL,6);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('68','6','故障总计','平台可用性指标得分','double',NULL,'select round(sum(`67`/100*0.4),2) as total',NULL,7);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('69','11','总得分','工单及时率总得分','double',NULL,'select round(sum(`59`+`60`+`61`), 2) as total',NULL,2);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('7','2','全部资源利用情况','全部物理计算资源CPU均值利用率(%)','double',NULL,NULL,NULL,5);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('70','1','基础信息','省份名称','string',NULL,NULL,NULL,1);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('71','1','基础信息','资源池名称','string','{\"unique\":true}',NULL,NULL,2);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('72','4','基础信息','省份名称','string',NULL,NULL,NULL,1);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('73','4','基础信息','资源池名称','string','{\"unique\":true}',NULL,NULL,2);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('74','10','基础信息','省份名称','string','{\"unique\":true}',NULL,NULL,1);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('75','12','基础信息','省份名称','string',NULL,NULL,NULL,1);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('76','11','基础信息','省份名称','string',NULL,NULL,NULL,1);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('77','6','基础信息','省份名称','string',NULL,NULL,NULL,1);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('78','7','基础信息','省份名称','string',NULL,NULL,NULL,1);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('79','7','基础信息','资源池名称','string',NULL,NULL,NULL,2);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('8','2','全部资源利用情况','全部物理计算资源CPU均峰值利用率(%)','double',NULL,NULL,NULL,6);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('80','9','基础信息','省份名称','string','{\"unique\":true}',NULL,NULL,1);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('81','8','基础信息','省份名称','string','{\"unique\":true}',NULL,NULL,NULL);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('82','2','基础信息','省份名称','string',NULL,NULL,NULL,NULL);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('83','2','基础信息','资源池名称','string','{\"unique\":true}',NULL,NULL,NULL);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('84','3','基础信息','省份名称','string',NULL,NULL,NULL,NULL);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('85','3','基础信息','资源池名称','string','{\"unique\":true}',NULL,NULL,NULL);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('86','5','基础信息','省份名称','string',NULL,NULL,NULL,1);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('87','5','基础信息','资源池名称','string','{\"unique\":true}',NULL,NULL,2);
insert  into `cmdb_31_province_report_setting`(`id`,`table_id`,`resource_group`,`resource_type`,`resource_control_type`,`calc_valid`,`calc_exp`,`value_valid`,`sort_index`) values ('88','2','全部资源利用情况','全部物理计算资源内存均值利用率(%)','double',NULL,NULL,NULL,7);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
