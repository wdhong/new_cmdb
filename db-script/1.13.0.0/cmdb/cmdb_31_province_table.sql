/*
SQLyog Ultimate
MySQL - 5.7.30-log : Database - cmdb_online
*********************************************************************
*/

/*Table structure for table `cmdb_31_province_table` */
DROP TABLE IF EXISTS `cmdb_31_province_table`;
CREATE TABLE `cmdb_31_province_table` (
  `id` varchar(40) NOT NULL COMMENT '表id',
  `resource_owner` varchar(40) NOT NULL COMMENT '表归属(reource/yunwei)',
  `name` varchar(40) NOT NULL COMMENT '表名',
  `show_page` varchar(40) DEFAULT NULL COMMENT '展示在哪个界面（update,query）',
  `sort_index` int(11) NOT NULL COMMENT '表顺序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='31省份表管理';

/*Data for the table `cmdb_31_province_table` */

insert  into `cmdb_31_province_table`(`id`,`resource_owner`,`name`,`show_page`,`sort_index`) values ('1','resource','存储资源','update,query',1);
insert  into `cmdb_31_province_table`(`id`,`resource_owner`,`name`,`show_page`,`sort_index`) values ('10','yunwei','变更工单及时率','update,query',6);
insert  into `cmdb_31_province_table`(`id`,`resource_owner`,`name`,`show_page`,`sort_index`) values ('11','yunwei','工单及时率总得分','query',7);
insert  into `cmdb_31_province_table`(`id`,`resource_owner`,`name`,`show_page`,`sort_index`) values ('12','resource','均峰值利用率','query',5);
insert  into `cmdb_31_province_table`(`id`,`resource_owner`,`name`,`show_page`,`sort_index`) values ('2','resource','物理计算资源(裸金属)','update,query',2);
insert  into `cmdb_31_province_table`(`id`,`resource_owner`,`name`,`show_page`,`sort_index`) values ('3','resource','虚拟计算资源','update,query',3);
insert  into `cmdb_31_province_table`(`id`,`resource_owner`,`name`,`show_page`,`sort_index`) values ('4','resource','传输资源','update,query',4);
insert  into `cmdb_31_province_table`(`id`,`resource_owner`,`name`,`show_page`,`sort_index`) values ('5','yunwei','平台故障统计','update',1);
insert  into `cmdb_31_province_table`(`id`,`resource_owner`,`name`,`show_page`,`sort_index`) values ('6','yunwei','平台故障总计','query',2);
insert  into `cmdb_31_province_table`(`id`,`resource_owner`,`name`,`show_page`,`sort_index`) values ('7','yunwei','平台故障明细','update,query',3);
insert  into `cmdb_31_province_table`(`id`,`resource_owner`,`name`,`show_page`,`sort_index`) values ('8','yunwei','网络策略工单及时率','update,query',4);
insert  into `cmdb_31_province_table`(`id`,`resource_owner`,`name`,`show_page`,`sort_index`) values ('9','yunwei','服务工单及时率','update,query',5);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
