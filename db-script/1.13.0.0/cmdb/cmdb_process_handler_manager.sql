/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50730
Source Host           : 10.12.70.40:3306
Source Database       : cmdb_online

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2020-04-29 20:05:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_process_handler_manager
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_process_handler_manager`;
CREATE TABLE `cmdb_process_handler_manager` (
  `id` varchar(40) NOT NULL COMMENT 'ID',
  `handler_type` varchar(40) NOT NULL COMMENT '处理类型',
  `handler_class` varchar(500) NOT NULL COMMENT '处理类',
  PRIMARY KEY (`id`,`handler_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cmdb_process_handler_manager
-- ----------------------------
INSERT INTO `cmdb_process_handler_manager` VALUES ('1', 'instance', 'com.aspire.ums.cmdb.v2.process.instance.InstanceImportFactory');
INSERT INTO `cmdb_process_handler_manager` VALUES ('10', 'equipmentproblem', 'com.aspire.ums.cmdb.v2.process.evaluation.EquipmentProblemImportFactory');
INSERT INTO `cmdb_process_handler_manager` VALUES ('11', 'deviceinfo', 'com.aspire.ums.cmdb.v2.process.evaluation.ITDeviceQuantityImportFactory');
INSERT INTO `cmdb_process_handler_manager` VALUES ('12', 'maintenance_project_bind_device', 'com.aspire.ums.cmdb.v2.process.maintenance.MaintenanceProjectDeviceImportFactory');
INSERT INTO `cmdb_process_handler_manager` VALUES ('13', 'maintenance_project', 'com.aspire.ums.cmdb.v2.process.maintenance.MaintenanceProjectImportFactory');
INSERT INTO `cmdb_process_handler_manager` VALUES ('14', 'mainten_component', 'com.aspire.ums.cmdb.v2.process.maintenance.MaintenComponentImportFactory');
INSERT INTO `cmdb_process_handler_manager` VALUES ('15', 'maintensoftwarerecord', 'com.aspire.ums.cmdb.v2.process.maintenance.MaintenSoftwareRecordImportFactory');
INSERT INTO `cmdb_process_handler_manager` VALUES ('16', 'maintensoftware', 'com.aspire.ums.cmdb.v2.process.maintenance.MaintSoftwareImportFactory');
INSERT INTO `cmdb_process_handler_manager` VALUES ('17', 'screen_resource_allocate', 'com.aspire.ums.cmdb.v2.process.tenant.screen.ResourceAllocateImportFactory');
INSERT INTO `cmdb_process_handler_manager` VALUES ('18', 'screen_avg_utilization', 'com.aspire.ums.cmdb.v2.process.tenant.screen.AvgUtilizationImportFactory');
INSERT INTO `cmdb_process_handler_manager` VALUES ('19', 'screen_max_utilization', 'com.aspire.ums.cmdb.v2.process.tenant.screen.MaxUtilizationImportFactory');
INSERT INTO `cmdb_process_handler_manager` VALUES ('2', '31_province_report', 'com.aspire.ums.cmdb.v2.process.report.Cmdb31ProvinceReportImportFactory');
INSERT INTO `cmdb_process_handler_manager` VALUES ('20', 'store_resource_low_utilization', 'com.aspire.ums.cmdb.v2.process.tenant.screen.StoreRsLowUtilizationImportFactory');
INSERT INTO `cmdb_process_handler_manager` VALUES ('21', 'bs_not_inspect', 'com.aspire.ums.cmdb.v2.process.tenant.screen.BizSystemNotInspectImportFactory');
INSERT INTO `cmdb_process_handler_manager` VALUES ('22', 'portrelation', 'com.aspire.ums.cmdb.v2.process.instance.InstancePortRalImportFactory');
INSERT INTO `cmdb_process_handler_manager` VALUES ('23', 'invalidresource', 'com.aspire.ums.cmdb.v2.process.resource.InvalidResImportFactory');
INSERT INTO `cmdb_process_handler_manager` VALUES ('24', 'hardware', 'com.aspire.ums.cmdb.v2.process.maintenance.HardWareImportFactory');
INSERT INTO `cmdb_process_handler_manager` VALUES ('25', 'hardwareuse', 'com.aspire.ums.cmdb.v2.process.maintenance.HardWareUseImportFactory');
INSERT INTO `cmdb_process_handler_manager` VALUES ('26', 'networkcard', 'com.aspire.ums.cmdb.v2.process.instance.NetworkCardImportFactory');
INSERT INTO `cmdb_process_handler_manager` VALUES ('27', 'bs_online_info', 'com.aspire.ums.cmdb.v2.process.tenant.screen.BizSystemOnlineInfoImportFactory');
INSERT INTO `cmdb_process_handler_manager` VALUES ('3', 'problemevent', 'com.aspire.ums.cmdb.v2.process.evaluation.ProblemEventImportFactory');
INSERT INTO `cmdb_process_handler_manager` VALUES ('4', 'produce', 'com.aspire.ums.cmdb.v2.process.evaluation.ProduceInofImportFactory');
INSERT INTO `cmdb_process_handler_manager` VALUES ('5', 'repairevent', 'com.aspire.ums.cmdb.v2.process.evaluation.RepairEventImportFactory');
INSERT INTO `cmdb_process_handler_manager` VALUES ('6', 'evaluate', 'com.aspire.ums.cmdb.v2.process.evaluation.AssessmentScoreImportFactory');
INSERT INTO `cmdb_process_handler_manager` VALUES ('7', 'resourceassign', 'com.aspire.ums.cmdb.v2.process.resource.AssignImportFactory');
INSERT INTO `cmdb_process_handler_manager` VALUES ('8', 'bizresource', 'com.aspire.ums.cmdb.v2.process.resource.BizResImportFactory');
INSERT INTO `cmdb_process_handler_manager` VALUES ('9', 'demand', 'com.aspire.ums.cmdb.v2.process.demand.DemandImportFactory');
