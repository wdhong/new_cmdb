/*
Navicat MySQL Data Transfer

Source Server         : bpm
Source Server Version : 50730
Source Host           : 10.12.70.40:3306
Source Database       : cmdb_online

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2020-04-29 16:30:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for screen_check_score
-- ----------------------------
DROP TABLE IF EXISTS `screen_check_score`;
CREATE TABLE `screen_check_score` (
  `id` varchar(50) NOT NULL,
  `system_title_id` int(11) NOT NULL,
  `department1` varchar(50) DEFAULT NULL,
  `department2` varchar(50) DEFAULT NULL,
  `biz_system` varchar(50) DEFAULT NULL COMMENT '业务系统',
  `score_type` varchar(20) DEFAULT NULL COMMENT '类型',
  `standard_value` varchar(255) DEFAULT NULL COMMENT '标准值',
  `assessed_value` varchar(50) DEFAULT NULL COMMENT '评估值',
  `score` varchar(20) DEFAULT NULL COMMENT '扣除的分数',
  `description` varchar(50) DEFAULT NULL COMMENT '描述说明',
  `monthly_time` varchar(10) DEFAULT NULL,
  `insert_time` date DEFAULT NULL,
  `is_delete` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
