/*
Navicat MySQL Data Transfer

Source Server         : bpm
Source Server Version : 50730
Source Host           : 10.12.70.40:3306
Source Database       : cmdb_online

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2020-04-29 16:30:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for screen_online_info
-- ----------------------------
DROP TABLE IF EXISTS `screen_online_info`;
CREATE TABLE `screen_online_info` (
  `id` varchar(50) NOT NULL,
  `system_title_id` int(11) NOT NULL,
  `department1` varchar(50) DEFAULT NULL,
  `department2` varchar(50) DEFAULT NULL,
  `biz_system` varchar(50) DEFAULT NULL COMMENT '业务系统',
  `target_deploy_date` varchar(20) DEFAULT NULL COMMENT '目标部署上线时间',
  `actual_deploy_date` varchar(20) DEFAULT NULL COMMENT '实际部署上线时间',
  `target_inform_date` varchar(20) DEFAULT NULL COMMENT '目标上线通知时间',
  `actual_inform_date` varchar(20) DEFAULT NULL COMMENT '实际上线通知时间',
  `monthly_time` varchar(10) DEFAULT NULL COMMENT '月报时间',
  `insert_time` datetime DEFAULT NULL,
  `is_delete` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
