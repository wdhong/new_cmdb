/*
Navicat MySQL Data Transfer

Source Server         : bpm
Source Server Version : 50730
Source Host           : 10.12.70.40:3306
Source Database       : cmdb_online

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2020-04-29 16:30:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for screen_store_low_utilization
-- ----------------------------
DROP TABLE IF EXISTS `screen_store_low_utilization`;
CREATE TABLE `screen_store_low_utilization` (
  `id` varchar(40) NOT NULL,
  `system_title_id` int(11) NOT NULL,
  `department1` varchar(40) DEFAULT NULL,
  `department2` varchar(40) DEFAULT NULL,
  `resource_pool` varchar(40) DEFAULT NULL,
  `pod` varchar(40) DEFAULT NULL,
  `biz_system` varchar(40) DEFAULT NULL,
  `store_type` varchar(40) DEFAULT NULL COMMENT '存储类型',
  `utilization` varchar(40) DEFAULT NULL,
  `monthly_time` varchar(20) DEFAULT NULL,
  `insert_time` datetime DEFAULT NULL,
  `is_delete` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
