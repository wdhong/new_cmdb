/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 5.7.30-log 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

insert into `t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`, `sort_index`) values('1e64e03f385d45b3ab68dfcca13b05ba','总览','month_report_idctype','总览','','','2020-05-12 10:46:33',NULL,'0',NULL);
insert into `t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`, `sort_index`) values('37cf282e45e144c9909913b0a80ac0e4','深圳池外','month_report_idctype','深圳池外','','','2020-05-12 10:46:17',NULL,'0',NULL);
insert into `t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`, `sort_index`) values('5d507edcc4d34b5eb16c608203dfae2d','信息港资源池','month_report_idctype','信息港资源池','','','2020-05-12 10:45:57',NULL,'0',NULL);
insert into `t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`, `sort_index`) values('8e70ef3a484d4353949879fa0558c5c8','哈尔滨资源池','month_report_idctype','哈尔滨资源池','','','2020-05-12 10:45:42',NULL,'0',NULL);
insert into `t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`, `sort_index`) values('8f77efd6d7ba46f9ab634f702b4fb25f','南方基地','month_report_idctype','南方基地','','','2020-05-12 10:46:10',NULL,'0',NULL);
insert into `t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`, `sort_index`) values('b7999caf007f46e3ae8ba6307bc7d672','呼和浩特资源池','month_report_idctype','呼和浩特资源池','','','2020-05-12 10:45:50',NULL,'0',NULL);
insert into `t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`, `sort_index`) values('dc439329ab0746f0aed27584b1762b32','业支域非池化','month_report_idctype','业支域非池化','','','2020-05-12 10:46:03',NULL,'0',NULL);
insert into `t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`, `sort_index`) values('2a760b13b7f9466cb2170e62f1caf42a','对象存储','month_report_storage_type','对象存储','','对象存储','2020-05-13 09:50:59','2020-05-13 09:51:50','0',NULL);
insert into `t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`, `sort_index`) values('52d4c9b8b4a04bf5a739411baf8b53ef','文件存储','month_report_storage_type','文件存储','','文件存储','2020-05-13 09:50:35','2020-05-13 09:50:46','0',NULL);
insert into `t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`, `sort_index`) values('a8127aebdbfc4b32ab3f2ab7c7684a84','块存储','month_report_storage_type','块存储','','块存储','2020-05-13 09:51:40',NULL,'0',NULL);
insert into `t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`, `sort_index`) values('d357f261a9ea47b7b6c24a2599ab708e','FC-SAN','month_report_storage_type','FC-SAN','','FC-SAN','2020-05-13 09:52:42','2020-05-15 13:46:56','0',NULL);
insert into `t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`, `sort_index`) values('e1e0caeda33c4f7b9cdb367fd894d5b6','IP-SAN','month_report_storage_type','IP-SAN','','IP-SAN','2020-05-13 09:52:20',NULL,'0',NULL);
insert into `t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`, `sort_index`) values('ffb5afb7ec174a8da585d6b7e65ff24d','备份存储','month_report_storage_type','备份存储','','备份存储','2020-05-13 09:51:23','2020-05-13 09:51:58','0',NULL);
