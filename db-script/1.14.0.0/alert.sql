-- ----------------------------
-- Table structure for alert_alerts_merge
-- ----------------------------
DROP TABLE IF EXISTS `alert_alerts_merge`;
CREATE TABLE `alert_alerts_merge` (
  `alert_id` varchar(64) NOT NULL,
  `r_alert_id` varchar(300) DEFAULT NULL,
  `device_id` varchar(50) DEFAULT NULL,
  `device_class` varchar(128) DEFAULT NULL COMMENT '设备类型',
  `biz_sys` varchar(128) DEFAULT NULL COMMENT '业务系统',
  `moni_index` varchar(1024) NOT NULL COMMENT '监控指标/内容，关联触发器name',
  `moni_object` varchar(512) DEFAULT NULL COMMENT '监控对象',
  `item_key` varchar(128) DEFAULT NULL COMMENT '监控项',
  `key_comment` varchar(512) DEFAULT NULL COMMENT '监控项描述',
  `cur_moni_value` varchar(1024) DEFAULT NULL COMMENT '当前监控值',
  `cur_moni_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '当前监控时间',
  `alert_level` varchar(20) NOT NULL COMMENT '告警级别\r\n1-提示\r\n2-低\r\n3-中\r\n4-高\r\n5-严重\r\n',
  `item_id` varchar(50) DEFAULT NULL,
  `alert_end_time` datetime DEFAULT NULL COMMENT '告警结束时间',
  `remark` varchar(512) DEFAULT NULL COMMENT '备注',
  `order_status` varchar(50) DEFAULT NULL COMMENT '1-未派单\r\n2-处理中\r\n3-已完成\r\n',
  `operate_status` varchar(2) DEFAULT '0' COMMENT '操作状态',
  `notify_status` varchar(2) DEFAULT '0' COMMENT '告警通知状态，0-未通知，1-已通知',
  `clear_user` varchar(32) DEFAULT NULL COMMENT '清除人',
  `clear_content` varchar(256) DEFAULT NULL COMMENT '清除内容',
  `source` varchar(100) DEFAULT NULL COMMENT '告警来源\r\nMIRROR',
  `idc_type` varchar(128) DEFAULT NULL COMMENT '所属位置-资源池',
  `source_room` varchar(100) DEFAULT NULL COMMENT '机房',
  `device_type` varchar(128) DEFAULT NULL COMMENT '设备类型',
  `device_mfrs` varchar(128) DEFAULT NULL COMMENT '设备提供商',
  `host_name` varchar(128) DEFAULT NULL COMMENT '主机名',
  `device_model` varchar(128) DEFAULT NULL COMMENT '设备型号',
  `project_name` varchar(128) DEFAULT NULL COMMENT '工程期数',
  `pod_name` varchar(128) DEFAULT NULL COMMENT 'pod池',
  `object_type` varchar(50) DEFAULT NULL,
  `object_id` varchar(50) DEFAULT NULL,
  `device_ip` varchar(100) DEFAULT NULL,
  `order_type` varchar(20) DEFAULT NULL COMMENT '工单类型\r\n1-告警\r\n2-故障',
  `order_id` varchar(100) DEFAULT NULL COMMENT '工单ID',
  `alert_start_time` datetime DEFAULT NULL COMMENT '告警开始时间',
  `count` int(11) DEFAULT '1',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`alert_id`),
  KEY `index_mrfs_top10` (`alert_level`,`device_mfrs`,`idc_type`,`device_type`,`device_model`,`cur_moni_time`) USING BTREE,
  KEY `index_time_level` (`alert_level`,`alert_end_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警合并记录表';

-- ----------------------------
-- Procedure structure for PRO_MERGE_ALERT
-- ----------------------------
DROP PROCEDURE IF EXISTS `PRO_MERGE_ALERT`;
DELIMITER ;;
CREATE PROCEDURE `PRO_MERGE_ALERT`()
BEGIN
DELETE from alert_alerts_merge;

INSERT INTO `alert_alerts_merge` (`alert_id`, `r_alert_id`, `device_id`, `device_class`, `biz_sys`, `moni_index`, `moni_object`, `item_key`, `key_comment`, `cur_moni_value`, `cur_moni_time`, `alert_level`, `item_id`, `alert_end_time`, `remark`, `order_status`, `operate_status`, `notify_status`, `clear_user`, `clear_content`, `source`, `idc_type`, `source_room`, `device_type`, `device_mfrs`, `host_name`, `device_model`, `project_name`, `pod_name`, `object_type`, `object_id`, `device_ip`, `order_type`, `order_id`, `alert_start_time`, `count`, `create_time`) 
select `alert_id`, `r_alert_id`, `device_id`, `device_class`, `biz_sys`, `moni_index`, `moni_object`, `item_key`, `key_comment`, `cur_moni_value`, `cur_moni_time`, `alert_level`, `item_id`, `alert_end_time`, `remark`, `order_status`, `operate_status`, `notify_status`, `clear_user`, `clear_content`, `source`, `idc_type`, `source_room`, `device_type`, `device_mfrs`, `host_name`, `device_model`, `project_name`, `pod_name`, `object_type`, `object_id`, `device_ip`, `order_type`, `order_id`, `alert_start_time`, `count`, `create_time`
from alert_alerts_his aah
JOIN (select device_ip device_ip1,alert_level alert_level1, item_id item_id1, max(cur_moni_time) cur_moni_time1, COUNT(1) count from alert_alerts_his GROUP BY device_ip,alert_level, item_id)a_tmp
on aah.device_ip=a_tmp.device_ip1 and aah.alert_level=a_tmp.alert_level1 and aah.item_id=a_tmp.item_id1 and aah.cur_moni_time=a_tmp.cur_moni_time1
where not EXISTS (select 1 from alert_alerts a where a.device_ip=a_tmp.device_ip1 and a.alert_level=a_tmp.alert_level1 and a.item_id=a_tmp.item_id1);


INSERT INTO `alert_alerts_merge` (`alert_id`, `r_alert_id`, `device_id`, `device_class`, `biz_sys`, `moni_index`, `moni_object`, `item_key`, `key_comment`, `cur_moni_value`, `cur_moni_time`, `alert_level`, `item_id`, `alert_end_time`, `remark`, `order_status`, `operate_status`, `notify_status`, `clear_user`, `clear_content`, `source`, `idc_type`, `source_room`, `device_type`, `device_mfrs`, `host_name`, `device_model`, `project_name`, `pod_name`, `object_type`, `object_id`, `device_ip`, `order_type`, `order_id`, `alert_start_time`, `count`, `create_time`) 
select `alert_id`, `r_alert_id`, `device_id`, `device_class`, `biz_sys`, `moni_index`, `moni_object`, `item_key`, `key_comment`, `cur_moni_value`, `cur_moni_time`, `alert_level`, `item_id`, null, `remark`, `order_status`, `operate_status`, `notify_status`, null, null, `source`, `idc_type`, `source_room`, `device_type`, `device_mfrs`, `host_name`, `device_model`, `project_name`, `pod_name`, `object_type`, `object_id`, `device_ip`, `order_type`, `order_id`, `alert_start_time`, `count` + 1 as count, `create_time`
from alert_alerts a
JOIN (select device_ip device_ip1,alert_level alert_level1, item_id item_id1, COUNT(1) count from alert_alerts_his GROUP BY device_ip,alert_level, item_id)a_tmp
on a.device_ip=a_tmp.device_ip1 and a.alert_level=a_tmp.alert_level1 and a.item_id=a_tmp.item_id1;

INSERT INTO `alert_alerts_merge` (`alert_id`, `r_alert_id`, `device_id`, `device_class`, `biz_sys`, `moni_index`, `moni_object`, `item_key`, `key_comment`, `cur_moni_value`, `cur_moni_time`, `alert_level`, `item_id`, `alert_end_time`, `remark`, `order_status`, `operate_status`, `notify_status`, `clear_user`, `clear_content`, `source`, `idc_type`, `source_room`, `device_type`, `device_mfrs`, `host_name`, `device_model`, `project_name`, `pod_name`, `object_type`, `object_id`, `device_ip`, `order_type`, `order_id`, `alert_start_time`, `count`, `create_time`) 
select `alert_id`, `r_alert_id`, `device_id`, `device_class`, `biz_sys`, `moni_index`, `moni_object`, `item_key`, `key_comment`, `cur_moni_value`, `cur_moni_time`, `alert_level`, `item_id`, null, `remark`, `order_status`, `operate_status`, `notify_status`, null, null, `source`, `idc_type`, `source_room`, `device_type`, `device_mfrs`, `host_name`, `device_model`, `project_name`, `pod_name`, `object_type`, `object_id`, `device_ip`, `order_type`, `order_id`, `alert_start_time`, 1, `create_time`
from alert_alerts a
where not EXISTS (select 1 from alert_alerts_his aah where aah.device_ip=a.device_ip and aah.alert_level=a.alert_level and aah.item_id=aah.item_id);

END
;;
DELIMITER ;

-- ----------------------------
-- Event structure for EVENT_MERGE_ALERT
-- ----------------------------
DROP EVENT IF EXISTS `EVENT_MERGE_ALERT`;
DELIMITER ;;
CREATE  EVENT `EVENT_MERGE_ALERT` ON SCHEDULE EVERY 1 DAY STARTS '2020-05-22 16:32:46' ON COMPLETION NOT PRESERVE ENABLE DO call PRO_MERGE_ALERT
;;
DELIMITER ;