/*
SQLyog Ultimate
MySQL - 5.7.30-log : Database - cmdb_online
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cmdb_online` /*!40100 DEFAULT CHARACTER SET utf8 */;

/*Table structure for table `cmdb_module_event_handler_class` */

DROP TABLE IF EXISTS `cmdb_module_event_handler_class`;

CREATE TABLE `cmdb_module_event_handler_class` (
  `id` varchar(40) NOT NULL COMMENT 'ID',
  `event_class` varchar(20) NOT NULL COMMENT '事件类型（module/code）',
  `handler_name` varchar(40) NOT NULL COMMENT '处理类名称',
  `handler_class` varchar(200) NOT NULL COMMENT '处理类',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cmdb_module_event_handler_class` */

insert  into `cmdb_module_event_handler_class`(`id`,`event_class`,`handler_name`,`handler_class`,`remark`) values ('1','module','内网IP地址操作日志处理类','com.aspire.ums.cmdb.v3.module.event.data.IPRepositoryInnerIPOperateLogEvent',NULL);
insert  into `cmdb_module_event_handler_class`(`id`,`event_class`,`handler_name`,`handler_class`,`remark`) values ('10','module','IPV6-网段管理操作日志处理类','com.aspire.ums.cmdb.v3.module.event.code.IPRepositoryIPV6SegmentOperateLogEvent',NULL);
insert  into `cmdb_module_event_handler_class`(`id`,`event_class`,`handler_name`,`handler_class`,`remark`) values ('11','module','公网IP掩码自动生成IP类','com.aspire.ums.cmdb.v3.module.event.data.IPRepositoryPublicSegmentInsertEvent',NULL);
insert  into `cmdb_module_event_handler_class`(`id`,`event_class`,`handler_name`,`handler_class`,`remark`) values ('12','module','IPV6掩码自动生成IP类','com.aspire.ums.cmdb.v3.module.event.data.IPRepositoryIPV6SegmentInsertEvent',NULL);
insert  into `cmdb_module_event_handler_class`(`id`,`event_class`,`handler_name`,`handler_class`,`remark`) values ('13','code','ALL—配置项变更-申请状态处理类','com.aspire.ums.cmdb.v3.module.event.code.InnerRequestPersonEvent',NULL);
insert  into `cmdb_module_event_handler_class`(`id`,`event_class`,`handler_name`,`handler_class`,`remark`) values ('2','module','内网网段管理操作日志处理类','com.aspire.ums.cmdb.v3.module.event.data.IPRepositoryInnerSegmentOperateLogEvent',NULL);
insert  into `cmdb_module_event_handler_class`(`id`,`event_class`,`handler_name`,`handler_class`,`remark`) values ('3','code','内网IP地址—配置项变更-所属网段地址处理类','com.aspire.ums.cmdb.v3.module.event.code.InnerNetworkSegmentOwnerEvent',NULL);
insert  into `cmdb_module_event_handler_class`(`id`,`event_class`,`handler_name`,`handler_class`,`remark`) values ('4','module','内网IPV4掩码自动生成IP类','com.aspire.ums.cmdb.v3.module.event.data.IPRepositoryIPV4SegmentInsertEvent',NULL);
insert  into `cmdb_module_event_handler_class`(`id`,`event_class`,`handler_name`,`handler_class`,`remark`) values ('5','module','外网IP地址操作日志处理类','com.aspire.ums.cmdb.v3.module.event.data.IPRepositoryPublicIPOperateLogEvent',NULL);
insert  into `cmdb_module_event_handler_class`(`id`,`event_class`,`handler_name`,`handler_class`,`remark`) values ('6','module','外网网段管理操作日志处理类','com.aspire.ums.cmdb.v3.module.event.data.IPRepositoryPublicSegmentOperateLogEvent',NULL);
insert  into `cmdb_module_event_handler_class`(`id`,`event_class`,`handler_name`,`handler_class`,`remark`) values ('7','code','外网IP地址—配置项变更-所属网段地址处理类','com.aspire.ums.cmdb.v3.module.event.code.PublicNetworkSegmentOwnerEvent',NULL);
insert  into `cmdb_module_event_handler_class`(`id`,`event_class`,`handler_name`,`handler_class`,`remark`) values ('8','code','IPV6-IP地址—配置项变更-所属网段地址处理类','com.aspire.ums.cmdb.v3.module.event.code.IPV6NetworkSegmentOwnerEvent',NULL);
insert  into `cmdb_module_event_handler_class`(`id`,`event_class`,`handler_name`,`handler_class`,`remark`) values ('9','module','IPV6-IP地址操作日志处理类','com.aspire.ums.cmdb.v3.module.event.data.IPRepositoryIPV6IPOperateLogEvent',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
