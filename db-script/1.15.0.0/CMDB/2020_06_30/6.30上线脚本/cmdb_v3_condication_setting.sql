/*
SQLyog Ultimate
MySQL - 5.7.18-log 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

insert into `cmdb_v3_condication_setting` (`id`, `condication_code`, `condication_name`, `condication_type`, `module_id`, `access_user_id`, `remark`, `is_delete`) values('1cfba3878bae4f91b048181c2f0d80db','search_ipv6','IPV4 IP地址列表查询条件','系统菜单','2ebf3cae36ab43a4bd25921a4ddac514','5245ed1b-6345-11e','IPV4 IP地址列表查询条件','0');
insert into `cmdb_v3_condication_setting` (`id`, `condication_code`, `condication_name`, `condication_type`, `module_id`, `access_user_id`, `remark`, `is_delete`) values('39ee705a7af7440d988ae9981a455611','ip_repository_inner','ip_repository_inner','系统菜单','7e6a5f63a0204b92bd783508eed0818d','5245ed1b-6345-11e','IP地址库内网网段查询条件配置','0');
insert into `cmdb_v3_condication_setting` (`id`, `condication_code`, `condication_name`, `condication_type`, `module_id`, `access_user_id`, `remark`, `is_delete`) values('5fe3d36570824d35aa24f1eb115b436a','search_public_ip','公网IP列表查询条件','系统菜单','bdea5664089d4fdfa975570b6348424e','5245ed1b-6345-11e','公网IP列表查询条件','0');
insert into `cmdb_v3_condication_setting` (`id`, `condication_code`, `condication_name`, `condication_type`, `module_id`, `access_user_id`, `remark`, `is_delete`) values('978cf4a1f1994e898abc695e1e85c27d','search_ip_repository_inner_ip','search_ip_repository_inner_ip','系统菜单','2893541da55d415c8f99220137cdc599','5245ed1b-6345-11e','内网IP地址管理配置条件','0');
insert into `cmdb_v3_condication_setting` (`id`, `condication_code`, `condication_name`, `condication_type`, `module_id`, `access_user_id`, `remark`, `is_delete`) values('c69a7713f02841759004382cd86557de','search_ipv6_segment','IPV6网段查询条件','系统菜单','b363417c8eac4fa3ad808f7edae7e67a','5245ed1b-6345-11e','IPV6网段查询条件','0');
insert into `cmdb_v3_condication_setting` (`id`, `condication_code`, `condication_name`, `condication_type`, `module_id`, `access_user_id`, `remark`, `is_delete`) values('f09ce172fe674ba78982c9c2449b531f','search_public_segment','公网网段查询条件','系统菜单','b363417c8eac4fa3ad808f7edae7e67a','5245ed1b-6345-11e','公网网段查询条件','0');
insert into `cmdb_v3_condication_setting` (`id`, `condication_code`, `condication_name`, `condication_type`, `module_id`, `access_user_id`, `remark`, `is_delete`) values('960fce6ce7a24b79ba2f1571c4586c2b','cmdb_operate_log','cmdb_operate_log','系统菜单','dd81515013bb44a68a740423bd42902b','5245ed1b-6345-11e','','0');

