/*
SQLyog Ultimate
MySQL - 5.7.30-log : Database - cmdb_online
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cmdb_online` /*!40100 DEFAULT CHARACTER SET utf8 */;

/*Table structure for table `cmdb_view` */

DROP TABLE IF EXISTS `cmdb_view`;

CREATE TABLE `cmdb_view` (
  `id` varchar(40) NOT NULL COMMENT '视图id',
  `view_code` varchar(40) NOT NULL COMMENT '视图编码',
  `view_name` varchar(40) NOT NULL COMMENT '视图名称',
  `catalog_id` varchar(40) NOT NULL COMMENT '模型分组id',
  `module_id` varchar(40) NOT NULL COMMENT '模型id',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `create_person` varchar(40) DEFAULT NULL COMMENT '创建人',
  `update_person` varchar(40) DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_CMDB_VIEW_01` (`view_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='自定视图';

/*Data for the table `cmdb_view` */

insert  into `cmdb_view`(`id`,`view_code`,`view_name`,`catalog_id`,`module_id`,`create_time`,`update_time`,`create_person`,`update_person`) values ('1','bizView','业务视图1','54ba8b02ad3748229adb39f9e7fdff38','9212e88a698d43cbbf9ec35b83773e2d','2020-05-25 23:09:53','2020-05-29 17:25:03','hftest','test_hf');
insert  into `cmdb_view`(`id`,`view_code`,`view_name`,`catalog_id`,`module_id`,`create_time`,`update_time`,`create_person`,`update_person`) values ('1a0c63d0a8db417da68a27cef0f05121','yanruitest111','yanruitest111','96603733-5793-11ea-ab96-fa163e982f89','80e427ff5bf44eef85d93602b2131ee0','2020-05-29 14:13:56','2020-05-29 14:13:56','test_hf','test_hf');
insert  into `cmdb_view`(`id`,`view_code`,`view_name`,`catalog_id`,`module_id`,`create_time`,`update_time`,`create_person`,`update_person`) values ('35f24e5f56034fb8b4c9e38dee402da1','yanruitest333444','yanruitest333444','96603733-5793-11ea-ab96-fa163e982f89','96603733-5793-11ea-ab96-fa163e982f89','2020-05-29 14:09:25','2020-06-10 14:05:14','alauda','alauda');
insert  into `cmdb_view`(`id`,`view_code`,`view_name`,`catalog_id`,`module_id`,`create_time`,`update_time`,`create_person`,`update_person`) values ('43d597d98aa742fe8b903da31c586443','yanruites6','yanruites6','96603733-5793-11ea-ab96-fa163e982f89','80e427ff5bf44eef85d93602b2131ee0','2020-05-29 14:11:59','2020-05-29 14:11:59','alauda','alauda');
insert  into `cmdb_view`(`id`,`view_code`,`view_name`,`catalog_id`,`module_id`,`create_time`,`update_time`,`create_person`,`update_person`) values ('4f34283310d941da978e3b7e0fc11831','test05291833','test05291833','96603733-5793-11ea-ab96-fa163e982f89','96603733-5793-11ea-ab96-fa163e982f89','2020-05-29 18:33:17','2020-05-29 18:33:17','alauda','alauda');
insert  into `cmdb_view`(`id`,`view_code`,`view_name`,`catalog_id`,`module_id`,`create_time`,`update_time`,`create_person`,`update_person`) values ('527f0009f9b64cc2b71b803e01568ec7','test0529','test0529','2aa2924a75fd4255a01dc77b255f1b04','94388e0b4742418581347958e6b5db65','2020-05-29 10:28:05','2020-05-29 10:29:18','alauda','alauda');
insert  into `cmdb_view`(`id`,`view_code`,`view_name`,`catalog_id`,`module_id`,`create_time`,`update_time`,`create_person`,`update_person`) values ('6f966fa1f2f44092840b71b14eb267d5','public_segment_view','公网IP地址视图','e7923392ba7e4822a6266b6ee616ef9c','b363417c8eac4fa3ad808f7edae7e67a','2020-05-29 17:53:25','2020-06-15 11:21:06','alauda','alauda');
insert  into `cmdb_view`(`id`,`view_code`,`view_name`,`catalog_id`,`module_id`,`create_time`,`update_time`,`create_person`,`update_person`) values ('70698228d0514344a9104fd8521a43e1','ipv6segment_view','IPV6库视图','e7923392ba7e4822a6266b6ee616ef9c','ca0950aa6f844223b9a7f266d9a2bbd1','2020-05-29 17:56:51','2020-06-15 10:48:53','alauda','alauda');
insert  into `cmdb_view`(`id`,`view_code`,`view_name`,`catalog_id`,`module_id`,`create_time`,`update_time`,`create_person`,`update_person`) values ('73b6e9281ced425c8283094644c6d9c6','test1111','test11','96603733-5793-11ea-ab96-fa163e982f89','96603733-5793-11ea-ab96-fa163e982f89','2020-06-01 13:35:34','2020-06-01 13:37:11','alauda','alauda');
insert  into `cmdb_view`(`id`,`view_code`,`view_name`,`catalog_id`,`module_id`,`create_time`,`update_time`,`create_person`,`update_person`) values ('745f273c408446a4a5986dbbce25dfc4','yanruites7','yanruites7','96603733-5793-11ea-ab96-fa163e982f89','e5077f28300e4ab98509d5fd845a8604','2020-05-29 14:12:18','2020-05-29 14:46:06','alauda','alauda');
insert  into `cmdb_view`(`id`,`view_code`,`view_name`,`catalog_id`,`module_id`,`create_time`,`update_time`,`create_person`,`update_person`) values ('74e05b7589b6429bb71717b13ef06182','aaaaa','334444','96603733-5793-11ea-ab96-fa163e982f89','80e427ff5bf44eef85d93602b2131ee0','2020-05-29 16:22:41','2020-05-29 16:22:41','alauda','alauda');
insert  into `cmdb_view`(`id`,`view_code`,`view_name`,`catalog_id`,`module_id`,`create_time`,`update_time`,`create_person`,`update_person`) values ('86a0306b310d4d66bce285909b82fd74','test06011323','test06011323','96603733-5793-11ea-ab96-fa163e982f89','96603733-5793-11ea-ab96-fa163e982f89','2020-06-01 13:23:34','2020-06-01 13:23:34','alauda','alauda');
insert  into `cmdb_view`(`id`,`view_code`,`view_name`,`catalog_id`,`module_id`,`create_time`,`update_time`,`create_person`,`update_person`) values ('96ede98f03f1423390f894ac1aa7abed','yanruites5','yanruites5','96603733-5793-11ea-ab96-fa163e982f89','80e427ff5bf44eef85d93602b2131ee0','2020-05-29 14:11:42','2020-05-29 14:11:42','alauda','alauda');
insert  into `cmdb_view`(`id`,`view_code`,`view_name`,`catalog_id`,`module_id`,`create_time`,`update_time`,`create_person`,`update_person`) values ('9ebc7fbb8ed844d6ad6d4b319d371c9b','yanruitest7','yanruitest7','96603733-5793-11ea-ab96-fa163e982f89','80e427ff5bf44eef85d93602b2131ee0','2020-05-29 15:15:03','2020-05-29 15:15:03','alauda','alauda');
insert  into `cmdb_view`(`id`,`view_code`,`view_name`,`catalog_id`,`module_id`,`create_time`,`update_time`,`create_person`,`update_person`) values ('a57e99f5b8f24b9caaa4b5ab10a0b2b4','tttt','rrrrr','2aa2924a75fd4255a01dc77b255f1b04','94388e0b4742418581347958e6b5db65','2020-05-27 01:03:08','2020-05-27 01:03:08','alauda','alauda');
insert  into `cmdb_view`(`id`,`view_code`,`view_name`,`catalog_id`,`module_id`,`create_time`,`update_time`,`create_person`,`update_person`) values ('b113b9c32d4b4d608699df763f15b67c','vvvvv','333333','96603733-5793-11ea-ab96-fa163e982f89','96603733-5793-11ea-ab96-fa163e982f89','2020-05-29 16:27:59','2020-05-29 16:54:03','alauda','test_hf');
insert  into `cmdb_view`(`id`,`view_code`,`view_name`,`catalog_id`,`module_id`,`create_time`,`update_time`,`create_person`,`update_person`) values ('b33c12e6d91047a0aa1a62c5c2349497','yanruitest999','yanruitest999','96603733-5793-11ea-ab96-fa163e982f89','80e427ff5bf44eef85d93602b2131ee0','2020-05-29 14:18:58','2020-05-29 14:18:58','test_hf','test_hf');
insert  into `cmdb_view`(`id`,`view_code`,`view_name`,`catalog_id`,`module_id`,`create_time`,`update_time`,`create_person`,`update_person`) values ('b722b74d708945109652235d6aff5489','aaa','aaa','96603733-5793-11ea-ab96-fa163e982f89','96603733-5793-11ea-ab96-fa163e982f89','2020-06-03 16:08:25','2020-06-03 16:08:25','alauda','alauda');
insert  into `cmdb_view`(`id`,`view_code`,`view_name`,`catalog_id`,`module_id`,`create_time`,`update_time`,`create_person`,`update_person`) values ('bab7fc6edfb74d82bc82228fe02adef2','test06011321','test06011321','96603733-5793-11ea-ab96-fa163e982f89','96603733-5793-11ea-ab96-fa163e982f89','2020-06-01 13:22:08','2020-06-01 13:23:00','alauda','alauda');
insert  into `cmdb_view`(`id`,`view_code`,`view_name`,`catalog_id`,`module_id`,`create_time`,`update_time`,`create_person`,`update_person`) values ('c0110e00cfca44bbb1f6f897f84469f3','testyanrui','严睿测试','96603733-5793-11ea-ab96-fa163e982f89','96603733-5793-11ea-ab96-fa163e982f89','2020-05-29 10:44:06','2020-06-10 14:18:35','alauda','alauda');
insert  into `cmdb_view`(`id`,`view_code`,`view_name`,`catalog_id`,`module_id`,`create_time`,`update_time`,`create_person`,`update_person`) values ('c1b84845524647c1842d4b925cd2cfd4','test06031602','test06031602','96603733-5793-11ea-ab96-fa163e982f89','96603733-5793-11ea-ab96-fa163e982f89','2020-06-03 16:03:03','2020-06-03 16:07:29','alauda','alauda');
insert  into `cmdb_view`(`id`,`view_code`,`view_name`,`catalog_id`,`module_id`,`create_time`,`update_time`,`create_person`,`update_person`) values ('c59ea01a7a9c4a2894d9d92c252f07f2','aa','222','96603733-5793-11ea-ab96-fa163e982f89','80e427ff5bf44eef85d93602b2131ee0','2020-05-29 15:22:16','2020-05-29 15:22:16','alauda','alauda');
insert  into `cmdb_view`(`id`,`view_code`,`view_name`,`catalog_id`,`module_id`,`create_time`,`update_time`,`create_person`,`update_person`) values ('e290e5490db64525a89d4e381dff150f','inner_segment_view','内网IP地址库视图','e7923392ba7e4822a6266b6ee616ef9c','7e6a5f63a0204b92bd783508eed0818d','2020-05-27 16:51:55','2020-06-09 20:14:23','alauda','alauda');
insert  into `cmdb_view`(`id`,`view_code`,`view_name`,`catalog_id`,`module_id`,`create_time`,`update_time`,`create_person`,`update_person`) values ('e94f0bfe98a34cb49e2a51569c914d84','yanruitest7811111','yanruitest7','96603733-5793-11ea-ab96-fa163e982f89','80e427ff5bf44eef85d93602b2131ee0','2020-05-29 15:16:56','2020-05-29 15:17:19','alauda','alauda');
insert  into `cmdb_view`(`id`,`view_code`,`view_name`,`catalog_id`,`module_id`,`create_time`,`update_time`,`create_person`,`update_person`) values ('efc2ee6b5841428090fabce43ce7d111','yanruites4','yanruites4','96603733-5793-11ea-ab96-fa163e982f89','80e427ff5bf44eef85d93602b2131ee0','2020-05-29 14:11:17','2020-05-29 14:11:17','alauda','alauda');
insert  into `cmdb_view`(`id`,`view_code`,`view_name`,`catalog_id`,`module_id`,`create_time`,`update_time`,`create_person`,`update_person`) values ('f706406c2ea547168ba172064c57d42a','test1','test1','96603733-5793-11ea-ab96-fa163e982f89','96603733-5793-11ea-ab96-fa163e982f89','2020-06-03 17:06:00','2020-06-03 17:07:19','alauda','alauda');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
