/*
SQLyog Ultimate
MySQL - 5.7.30-log : Database - cmdb_online
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cmdb_online` /*!40100 DEFAULT CHARACTER SET utf8 */;

/*Table structure for table `cmdb_view_node_show` */

DROP TABLE IF EXISTS `cmdb_view_node_show`;

CREATE TABLE `cmdb_view_node_show` (
  `id` varchar(40) NOT NULL COMMENT '显示子节点信息id',
  `node_id` varchar(40) NOT NULL COMMENT '节点id',
  `show_prefix` varchar(10) DEFAULT NULL COMMENT '显示前缀',
  `show_sql` text COMMENT 'SQL配置',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='显示子节点数量配置';

/*Data for the table `cmdb_view_node_show` */

insert  into `cmdb_view_node_show`(`id`,`node_id`,`show_prefix`,`show_sql`) values ('10ce43d0-9fe7-11ea-ae03-0242ac110004','f4f359a40a6c448cbe57d4c82baf40ed','5','5');
insert  into `cmdb_view_node_show`(`id`,`node_id`,`show_prefix`,`show_sql`) values ('15065038-a0bb-11ea-ae03-0242ac110004','728e14e54fae49219d609040004ec246','网段数量：','');
insert  into `cmdb_view_node_show`(`id`,`node_id`,`show_prefix`,`show_sql`) values ('1cc43e1a-aa19-11ea-ae03-0242ac110004','941c65112eb4415db2c9c5c5ebcfd25c','网段类型','');
insert  into `cmdb_view_node_show`(`id`,`node_id`,`show_prefix`,`show_sql`) values ('281749a1-a570-11ea-ae03-0242ac110004','f4aeba1a1f8c468c9298c8d85a49e7d6','test0603:','');
insert  into `cmdb_view_node_show`(`id`,`node_id`,`show_prefix`,`show_sql`) values ('28174be7-a570-11ea-ae03-0242ac110004','f4aeba1a1f8c468c9298c8d85a49e7d6','test0603:','');
insert  into `cmdb_view_node_show`(`id`,`node_id`,`show_prefix`,`show_sql`) values ('3fffb975-aeb7-11ea-ae03-0242ac110004','8265be75f2864b3daa1d6750fbfa5b64','已分配','SELECT se.public_segment_address,se.idcType,se.public_segment_type,ri.public_segment_owner FROM cmdb_ip_repository_public_segment se INNER JOIN cmdb_ip_repository_public_ip ri ON se.id = ri.id WHERE ri.assign_status = \'已分配\'');
insert  into `cmdb_view_node_show`(`id`,`node_id`,`show_prefix`,`show_sql`) values ('3fffbcd0-aeb7-11ea-ae03-0242ac110004','8265be75f2864b3daa1d6750fbfa5b64','已使用',' SELECT se.public_segment_address,se.idcType,se.public_segment_type,ri.public_segment_owner FROM cmdb_ip_repository_public_segment se INNER JOIN cmdb_ip_repository_public_ip ri ON se.id = ri.id WHERE ri.survival_status = \'已存活\'');
insert  into `cmdb_view_node_show`(`id`,`node_id`,`show_prefix`,`show_sql`) values ('508882ab-a15c-11ea-ae03-0242ac110004','e78472a8192e4785a0c6104d296e48c7','已分配','select se.network_segment_address,se.idcType,se.inner_segment_type,se.inner_segment_sub_type,ri.network_segment_owner from cmdb_ip_repository_inner_segment se inner join cmdb_ip_repository_inner_ip ri on se.id = ri.id where ri.assign_status = \'已分配\'');
insert  into `cmdb_view_node_show`(`id`,`node_id`,`show_prefix`,`show_sql`) values ('50888594-a15c-11ea-ae03-0242ac110004','e78472a8192e4785a0c6104d296e48c7','已使用','select se.network_segment_address,se.idcType,se.inner_segment_type,se.inner_segment_sub_type,ri.network_segment_owner from cmdb_ip_repository_inner_segment se inner join cmdb_ip_repository_inner_ip ri on se.id = ri.id where ri.survival_status = \'已存活\' or ri.is_cmdb_manager = \'是\'');
insert  into `cmdb_view_node_show`(`id`,`node_id`,`show_prefix`,`show_sql`) values ('728f396b-a638-11ea-ae03-0242ac110004','372fb6563cfc4e18a90748afb3bf096f','网段数量','');
insert  into `cmdb_view_node_show`(`id`,`node_id`,`show_prefix`,`show_sql`) values ('9a97b021-a9f9-11ea-ae03-0242ac110004','dde2df35faf847339647e94a8c0e47e3','网段数量：','');
insert  into `cmdb_view_node_show`(`id`,`node_id`,`show_prefix`,`show_sql`) values ('a6cc29e5-a638-11ea-ae03-0242ac110004','7c919e947ced4e329046cdaabf887e1e','网段数量：','');
insert  into `cmdb_view_node_show`(`id`,`node_id`,`show_prefix`,`show_sql`) values ('bbe75c76-a14d-11ea-ae03-0242ac110004','99088d7c56b240079b6f1ddf0ae8cece','网段数量：','');
insert  into `cmdb_view_node_show`(`id`,`node_id`,`show_prefix`,`show_sql`) values ('bfd1e5d3-aeb2-11ea-ae03-0242ac110004','d68217edc1fe4c5ab2bb571108a1ba87','已分配','SELECT se.ipv6_segment_address,se.idcType,se.ipv6_segment_type,se.ipv6_segment_sub_type,ri.ipv6_segment_owner FROM cmdb_ip_repository_ipv6_segment se INNER JOIN cmdb_ip_repository_ipv6_ip ri ON se.id = ri.id WHERE ri.assign_status = \'已分配\'');
insert  into `cmdb_view_node_show`(`id`,`node_id`,`show_prefix`,`show_sql`) values ('bfd1e9a6-aeb2-11ea-ae03-0242ac110004','d68217edc1fe4c5ab2bb571108a1ba87','已使用','SELECT se.ipv6_segment_address,se.idcType,se.ipv6_segment_type,se.ipv6_segment_sub_type,ri.ipv6_segment_owner FROM cmdb_ip_repository_ipv6_segment se INNER JOIN cmdb_ip_repository_ipv6_ip ri ON se.id = ri.id WHERE ri.survival_status = \'已存活\' OR ri.is_cmdb_manager = \'是\'');
insert  into `cmdb_view_node_show`(`id`,`node_id`,`show_prefix`,`show_sql`) values ('c12bdfb6-aa4a-11ea-ae03-0242ac110004','05d2fbee07e0488bbe19377a4dc18395','已分配','select se.network_segment_address,se.idcType,se.inner_segment_type,se.inner_segment_sub_type,ri.network_segment_owner from cmdb_ip_repository_inner_segment se inner join cmdb_ip_repository_inner_ip ri on se.id = ri.id where ri.assign_status = \'已分配\'');
insert  into `cmdb_view_node_show`(`id`,`node_id`,`show_prefix`,`show_sql`) values ('c12be9ca-aa4a-11ea-ae03-0242ac110004','05d2fbee07e0488bbe19377a4dc18395','已使用','select se.network_segment_address,se.idcType,se.inner_segment_type,se.inner_segment_sub_type,ri.network_segment_owner from cmdb_ip_repository_inner_segment se inner join cmdb_ip_repository_inner_ip ri on se.id = ri.id where ri.survival_status = \'已存活\' or ri.is_cmdb_manager = \'是\'');
insert  into `cmdb_view_node_show`(`id`,`node_id`,`show_prefix`,`show_sql`) values ('c2f2a492-a14d-11ea-ae03-0242ac110004','ea735c44c0484a3e82bd913c719d66de','网段类型','');
insert  into `cmdb_view_node_show`(`id`,`node_id`,`show_prefix`,`show_sql`) values ('e888fc92-9fe6-11ea-ae03-0242ac110004','c45a5cf48b084d4e80e64f1ffd0c88f5','3','3');
insert  into `cmdb_view_node_show`(`id`,`node_id`,`show_prefix`,`show_sql`) values ('fe5d82f2-a167-11ea-ae03-0242ac110004','a8203bea65f542fc8e7ac9638358cec7','安全设备数量：','SELECT device_name,department2 FROM  cmdb_instance a WHERE a.business_level2=\'安全设备（独立系统）\'');
insert  into `cmdb_view_node_show`(`id`,`node_id`,`show_prefix`,`show_sql`) values ('fe5d8666-a167-11ea-ae03-0242ac110004','a8203bea65f542fc8e7ac9638358cec7','777：','SELECT device_name,department2 FROM  cmdb_instance a WHERE a.business_level2=\'安全设备（独立系统）\'');
insert  into `cmdb_view_node_show`(`id`,`node_id`,`show_prefix`,`show_sql`) values ('fe9443f8-9fe6-11ea-ae03-0242ac110004','f07d6ce1a9b14906a4e4a9031febaf16','5','5');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
