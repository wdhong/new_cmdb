/*
SQLyog Ultimate
MySQL - 5.7.18-log 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

insert into `t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`, `sort_index`) values('7374a55bfc92471290819b81fce68766','变更事件','event_type_code','code_change','','模型绑定事件类型-配置项','2020-05-25 13:48:40',NULL,'0',NULL);
insert into `t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`, `sort_index`) values('8477733c78284296890695e8e8a60b20','删除事件','event_type_code','code_delete','','模型绑定事件类型-配置项','2020-05-25 13:48:18','2020-05-28 16:10:31','1',NULL);
insert into `t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`, `sort_index`) values('bd1b34ca40e94d34bda86b86027324a5','新增事件','event_type_code','code_insert','','模型绑定事件类型-配置项','2020-05-25 13:47:56','2020-05-28 16:10:34','1',NULL);
insert into `t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`, `sort_index`) values('ebf2c1f62b404d888c8c00399076bb27','修改事件','event_type_code','code_update','','模型绑定事件类型-配置项','2020-05-25 13:48:02','2020-05-28 16:10:36','1',NULL);
insert into `t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`, `sort_index`) values('4304498523904b96a79f7928081731eb','修改事件','event_type_module','data_update','','模型绑定事件类型-模型','2020-05-25 13:47:31','2020-05-25 13:49:21','0',NULL);
insert into `t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`, `sort_index`) values('436cb2fcce36410ca4ebad9c3afed753','新增事件','event_type_module','data_insert','','模型绑定事件类型-模型','2020-05-25 13:47:20','2020-05-25 13:49:14','0',NULL);
insert into `t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`, `sort_index`) values('77a4c3e5b50846ddae457c5820d0616a','删除事件','event_type_module','data_delete','','模型绑定事件类型-模型','2020-05-25 13:47:36','2020-05-25 13:49:31','0',NULL);


insert into `t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`, `sort_index`) values('3a793e5c66f04118a2cd47cb811bd1ad','业务系统','cmdb_view_icon','el-icon-share','','','2020-05-25 11:34:40',NULL,'0',NULL);
insert into `t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`, `sort_index`) values('7c8d846c75ca47febd3d55d71f4b4818','二级部门','cmdb_view_icon','el-icon-printer','','','2020-05-25 11:34:05',NULL,'0',NULL);
insert into `t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`, `sort_index`) values('b247a877c83042d1a9feb3b0b675ce5c','一级部门','cmdb_view_icon','el-icon-menu','','','2020-05-25 11:33:39',NULL,'0',NULL);
