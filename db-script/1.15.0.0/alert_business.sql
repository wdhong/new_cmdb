
-- ----------------------------
-- Table structure for alert_config_business
-- ----------------------------
DROP TABLE IF EXISTS `alert_config_business`;
CREATE TABLE `alert_config_business` (
  `id` varchar(64) NOT NULL COMMENT 'ID',
  `name` varchar(64) NOT NULL COMMENT '规则名称',
  `description` varchar(1024) DEFAULT NULL COMMENT '规则描述',
  `status` varchar(1) DEFAULT '0' COMMENT '启用状态，0-停用，1-启用',
  `operate_user` varchar(512) DEFAULT NULL COMMENT '维护用户',
  `option_condition` longtext COMMENT '过滤条件',
  `creater` varchar(64) DEFAULT NULL COMMENT '创建人',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `editer` varchar(64) DEFAULT NULL COMMENT '修改人',
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `is_delete` tinyint(4) DEFAULT '0' COMMENT '删除状态，0-未删除，1-已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='业务告警规则配置表';

-- ----------------------------
-- Table structure for alert_config_exception
-- ----------------------------
DROP TABLE IF EXISTS `alert_config_exception`;
CREATE TABLE `alert_config_exception` (
  `id` varchar(64) NOT NULL COMMENT 'ID',
  `name` varchar(64) NOT NULL COMMENT '规则名称',
  `description` varchar(1024) DEFAULT NULL COMMENT '规则描述',
  `status` varchar(1) DEFAULT '0' COMMENT '启用状态，0-停用，1-启用',
  `operate_user` varchar(512) DEFAULT NULL COMMENT '维护用户',
  `option_condition` longtext COMMENT '过滤条件',
  `creater` varchar(64) DEFAULT NULL COMMENT '创建人',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `editer` varchar(64) DEFAULT NULL COMMENT '修改人',
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `is_delete` tinyint(4) DEFAULT '0' COMMENT '删除状态，0-未删除，1-已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='异常信息规则配置表';

INSERT INTO `alert_model_field` (`id`, `model_id`, `field_code`, `field_name`, `field_type`, `is_lock`, `jdbc_type`, `jdbc_length`, `jdbc_tip`, `ci_field_code`, `is_ci_params`, `params_name`, `ci_field_name`, `is_list_show`, `list_show_sort`, `list_show_name`, `list_show_pattern`, `table_column_widch`, `is_detail_show`, `detail_show_sort`, `detail_show_name`, `detail_show_pattern`, `is_query_param`, `query_param_sort`, `query_param_name`, `query_param_type`, `query_param_source`, `is_derive_field`, `derive_field_sort`, `derive_field_name`, `derive_field_type`, `derive_field_source`, `creator`, `create_time`, `updater`, `update_time`) VALUES ('062b85e7-83a5-4cf8-a8c1-2c5932878efb', '987df07a-2299-4ea8-a053-4d038f16c9cd', 'ext_id', '扩展id', '1', '1', 'varchar', '128', '', '', '0', NULL, '', '0', '0', NULL, NULL, NULL, '0', '0', NULL, NULL, '0', '0', NULL, NULL, NULL, '0', '0', NULL, NULL, NULL, 'alauda', '2020-06-22 20:01:10', NULL, NULL);
INSERT INTO `alert_model_field` (`id`, `model_id`, `field_code`, `field_name`, `field_type`, `is_lock`, `jdbc_type`, `jdbc_length`, `jdbc_tip`, `ci_field_code`, `is_ci_params`, `params_name`, `ci_field_name`, `is_list_show`, `list_show_sort`, `list_show_name`, `list_show_pattern`, `table_column_widch`, `is_detail_show`, `detail_show_sort`, `detail_show_name`, `detail_show_pattern`, `is_query_param`, `query_param_sort`, `query_param_name`, `query_param_type`, `query_param_source`, `is_derive_field`, `derive_field_sort`, `derive_field_name`, `derive_field_type`, `derive_field_source`, `creator`, `create_time`, `updater`, `update_time`) VALUES ('2a092eb4-4691-4821-b202-3f02c3b1bdd9', 'dc21f101-f8bd-4641-b1b8-559ca7dcbf74', 'ext_id', '扩展id', '1', '1', 'varchar', '128', '', '', '0', NULL, '', '0', '0', NULL, NULL, NULL, '0', '0', NULL, NULL, '0', '0', NULL, NULL, NULL, '0', '0', NULL, NULL, NULL, 'alauda', '2020-06-22 19:55:57', NULL, NULL);


ALTER TABLE `alert_alerts`
ADD COLUMN `ext_id`  varchar(128) NULL COMMENT '扩展id';

ALTER TABLE `alert_alerts_his`
ADD COLUMN `ext_id`  varchar(128) NULL COMMENT '扩展id';