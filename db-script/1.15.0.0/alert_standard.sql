/*
Navicat MySQL Data Transfer

Source Server         : bpm
Source Server Version : 50730
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2020-06-29 09:15:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_standard
-- ----------------------------
DROP TABLE IF EXISTS `alert_standard`;
CREATE TABLE `alert_standard` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_class` varchar(20) DEFAULT NULL COMMENT '设备分类',
  `device_type` varchar(20) DEFAULT NULL COMMENT '设备类型',
  `monitor_key` varchar(40) DEFAULT NULL COMMENT '监控指标key',
  `standard_name` varchar(40) DEFAULT NULL COMMENT '标准名称',
  `alert_desc` varchar(100) DEFAULT NULL COMMENT '告警描述',
  `status` varchar(2) DEFAULT NULL COMMENT '状态 ( 1 启用、0 禁用)',
  `alert_level` varchar(20) DEFAULT NULL COMMENT '告警等级 (低、中、高、重大)',
  `insert_person` varchar(20) DEFAULT NULL COMMENT '创建人员',
  `insert_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_delete` varchar(2) DEFAULT '0' COMMENT '是否删除（0 否 | 1 是）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;
