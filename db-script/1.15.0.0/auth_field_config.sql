/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.38
Source Server Version : 50730
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2020-06-30 22:55:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for auth_field_config
-- ----------------------------
DROP TABLE IF EXISTS `auth_field_config`;
CREATE TABLE `auth_field_config` (
  `name` varchar(64) NOT NULL,
  `value` varchar(64) DEFAULT NULL,
  `type` smallint(1) NOT NULL COMMENT '1告警2性能3租户首页资源性能',
  PRIMARY KEY (`name`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_field_config
-- ----------------------------
INSERT INTO `auth_field_config` VALUES ('bizSystem_name', 'biz_sys', '1');
INSERT INTO `auth_field_config` VALUES ('bizSystem_name', 'bizSystem', '2');
INSERT INTO `auth_field_config` VALUES ('bizSystem_name', 'bizSystem', '3');
INSERT INTO `auth_field_config` VALUES ('device', 'device_id', '1');
INSERT INTO `auth_field_config` VALUES ('device_name', 'host', '2');
INSERT INTO `auth_field_config` VALUES ('device_type_name', 'device_type', '1');
INSERT INTO `auth_field_config` VALUES ('device_type_name', 'deviceType', '2');
INSERT INTO `auth_field_config` VALUES ('device_type_name', 'deviceType', '3');
INSERT INTO `auth_field_config` VALUES ('idcType_name', 'idc_type', '1');
INSERT INTO `auth_field_config` VALUES ('idcType_name', 'idcType', '2');
INSERT INTO `auth_field_config` VALUES ('idcType_name', 'idcType', '3');
INSERT INTO `auth_field_config` VALUES ('room_name', 'source_room', '1');
INSERT INTO `auth_field_config` VALUES ('room_name', 'roomId', '2');
