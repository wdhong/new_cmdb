/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50730
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2020-06-30 21:08:20
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_cfg_module_customizedview
-- ----------------------------
DROP TABLE IF EXISTS `t_cfg_module_customizedview`;
CREATE TABLE `t_cfg_module_customizedview` (
  `id` varchar(64) NOT NULL,
  `user_id` varchar(64) NOT NULL COMMENT '用户',
  `name` varchar(64) NOT NULL,
  `module_id` varchar(64) DEFAULT NULL COMMENT '试图名称',
  `create_time` datetime DEFAULT NULL,
  `systemId` varchar(64) DEFAULT NULL,
  `describe_` varchar(256) DEFAULT NULL,
  `content` text COMMENT 'json内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='模块定制化试图表';
