/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50730
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2020-07-07 13:53:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_user_classify
-- ----------------------------
DROP TABLE IF EXISTS `t_user_classify`;
CREATE TABLE `t_user_classify` (
  `uuid` varchar(36) NOT NULL COMMENT '人员类别code',
  `user_classify_name` varchar(100) DEFAULT NULL COMMENT '人员类别name',
  `namespace` varchar(100) DEFAULT NULL COMMENT 'namespace',
  `description` varchar(100) DEFAULT NULL COMMENT '描述',
  `creator` varchar(36) DEFAULT NULL COMMENT '创建人',
  `last_update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
