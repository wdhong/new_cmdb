/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50730
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2020-07-07 13:53:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_user_classify_account
-- ----------------------------
DROP TABLE IF EXISTS `t_user_classify_account`;
CREATE TABLE `t_user_classify_account` (
  `user_classify_uuid` varchar(36) NOT NULL COMMENT '人员类别code',
  `user_uuid` varchar(36) NOT NULL COMMENT '用户uuid'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
