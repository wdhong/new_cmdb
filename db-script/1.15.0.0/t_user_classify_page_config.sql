/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50730
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2020-07-07 13:54:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_user_classify_page_config
-- ----------------------------
DROP TABLE IF EXISTS `t_user_classify_page_config`;
CREATE TABLE `t_user_classify_page_config` (
  `uuid` varchar(60) NOT NULL,
  `user_classify_uuid` varchar(60) NOT NULL COMMENT '人员类别code',
  `customizedview_id` varchar(60) DEFAULT NULL COMMENT '试图id',
  `system_id` varchar(60) NOT NULL,
  `page_code` varchar(60) NOT NULL DEFAULT 'index' COMMENT '页面code',
  `page_alias` varchar(200) NOT NULL COMMENT '页面别名',
  `page_custom_config` longtext COMMENT '页面路径url配置',
  `namespace` varchar(100) DEFAULT NULL COMMENT 'namespace',
  `creator` varchar(60) DEFAULT NULL COMMENT '创建人',
  `last_update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`uuid`),
  UNIQUE KEY `uk_user_classify_page_config` (`user_classify_uuid`,`system_id`,`page_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
