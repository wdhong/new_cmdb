/*
 Navicat Premium Data Transfer

 Source Server         : 10.12.70.40
 Source Server Type    : MySQL
 Source Server Version : 50730
 Source Host           : 10.12.70.40:3306
 Source Schema         : mirror

 Target Server Type    : MySQL
 Target Server Version : 50730
 File Encoding         : 65001

 Date: 01/06/2020 10:57:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for alert_schedule_index
-- ----------------------------
DROP TABLE IF EXISTS `alert_schedule_index`;
CREATE TABLE `alert_schedule_index`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
  `index_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '指标名称',
  `index_value` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '指标值',
  `index_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '指标类型',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of alert_schedule_index
-- ----------------------------
INSERT INTO `alert_schedule_index` VALUES ('5e10aba5-c44b-40d5-836a-39a87d134bf5', '告警自动清除时间', '2020-05-26 08:07:00', 'alert_auto_clear_time', '不可更改不可删除');
INSERT INTO `alert_schedule_index` VALUES ('4ba3e182-760c-4975-9877-85d66da041cc', '告警自动确认时间', '2020-05-26 07:50:00', 'alert_auto_confirm_time', '不可更改不可删除');
INSERT INTO `alert_schedule_index` VALUES ('1', '严重', '5', 'alert_level', '不可更改不可删除');
INSERT INTO `alert_schedule_index` VALUES ('2', '高', '4', 'alert_level', '不可更改不可删除');
INSERT INTO `alert_schedule_index` VALUES ('3', '中', '3', 'alert_level', '不可更改不可删除');
INSERT INTO `alert_schedule_index` VALUES ('4', '低', '2', 'alert_level', '不可更改不可删除');
INSERT INTO `alert_schedule_index` VALUES ('5', '日', 'day', 'granularity', '不可更改不可删除');
INSERT INTO `alert_schedule_index` VALUES ('6', '周', 'week', 'granularity', '不可更改不可删除');
INSERT INTO `alert_schedule_index` VALUES ('7', '月', 'month', 'granularity', '不可更改不可删除');
INSERT INTO `alert_schedule_index` VALUES ('10', '全部', 'X86服务器,云主机', 'device_type', '不可更改不可删除');
INSERT INTO `alert_schedule_index` VALUES ('11', '物理机', 'X86服务器', 'device_type', '不可更改不可删除');
INSERT INTO `alert_schedule_index` VALUES ('12', '虚拟机', '云主机', 'device_type', '不可更改不可删除');
INSERT INTO `alert_schedule_index` VALUES ('21', 'OSN门户', '登陆', 'bizsystem', '不可更改不可删除');
INSERT INTO `alert_schedule_index` VALUES ('22', '乐知云', '登陆', 'bizsystem', '不可更改不可删除');
INSERT INTO `alert_schedule_index` VALUES ('23', 'ActionSpace', '登陆', 'bizsystem', '不可更改不可删除');
INSERT INTO `alert_schedule_index` VALUES ('24', '邮箱', '登陆', 'bizsystem', '不可更改不可删除');
INSERT INTO `alert_schedule_index` VALUES ('25', '集团OA ', '登陆', 'bizsystem', '不可更改不可删除');
INSERT INTO `alert_schedule_index` VALUES ('26', '工具链', '登陆', 'bizsystem', '不可更改不可删除');
INSERT INTO `alert_schedule_index` VALUES ('27', '项目管理系统', '登陆', 'bizsystem', '不可更改不可删除');
INSERT INTO `alert_schedule_index` VALUES ('28', 'teleport登录', '登陆', 'bizsystem', '不可更改不可删除');
INSERT INTO `alert_schedule_index` VALUES ('29', '科研管理平台', '登陆', 'bizsystem', '不可更改不可删除');
INSERT INTO `alert_schedule_index` VALUES ('30', '安全网盘', '登陆', 'bizsystem', '不可更改不可删除');
INSERT INTO `alert_schedule_index` VALUES ('31', '流程平台', '登陆', 'bizsystem', '不可更改不可删除');
INSERT INTO `alert_schedule_index` VALUES ('32', 'VPN登录系统', '登陆', 'bizsystem', '不可更改不可删除');
INSERT INTO `alert_schedule_index` VALUES ('33', '报账平台系统', '登陆', 'bizsystem', '不可更改不可删除');
INSERT INTO `alert_schedule_index` VALUES ('34', 'YF网络protal认证', '登陆', 'bizsystem', '不可更改不可删除');
INSERT INTO `alert_schedule_index` VALUES ('35', '智能问答', '访问', 'bizsystem', '不可更改不可删除');
INSERT INTO `alert_schedule_index` VALUES ('36', '邮箱系统单点登录', '访问', 'bizsystem', '不可更改不可删除');

SET FOREIGN_KEY_CHECKS = 1;
