/*
 Navicat Premium Data Transfer

 Source Server         : 10.12.70.40
 Source Server Type    : MySQL
 Source Server Version : 50730
 Source Host           : 10.12.70.40:3306
 Source Schema         : mirror

 Target Server Type    : MySQL
 Target Server Version : 50730
 File Encoding         : 65001

 Date: 03/06/2020 21:28:30
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for inspection_day_device
-- ----------------------------
DROP TABLE IF EXISTS `inspection_day_device`;
CREATE TABLE `inspection_day_device`  (
  `device_sum` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当日设备总量',
  `normal_sum` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当日设备正常数量',
  `normal_rate` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当日设备正常占比',
  `vm_sum` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当日虚拟机异常数量',
  `vm_rate` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当日虚拟机异常占比',
  `phy_sum` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当日物理机异常数量',
  `phy_rate` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当日物理机异常占比',
  `host_sum` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当日宿主机异常数量',
  `host_rate` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当日宿主机异常占比',
  `exception_rate` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '异常占比',
  `mon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '月份'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '每日设备巡检报表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
