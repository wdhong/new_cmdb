﻿

-- ----------------------------
-- Table structure for alert_order_handle
-- ----------------------------
DROP TABLE IF EXISTS `alert_order_handle`;
CREATE TABLE `alert_order_handle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(64) NOT NULL COMMENT '工单编号',
  `status` varchar(2) NOT NULL COMMENT '状态，0-待处理；1-处理中；2-已处理；3-已关闭',
  `account` varchar(255) DEFAULT NULL COMMENT '账号',
  `exec_user` varchar(255) DEFAULT NULL COMMENT '执行人',
  `exec_time` datetime NOT NULL,
  `expire_status` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_id_status` (`order_id`,`status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='告警工单处理表';
