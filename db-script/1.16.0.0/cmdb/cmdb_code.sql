     /*
SQLyog Ultimate
MySQL - 5.7.30-log 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

ALTER TABLE `cmdb_code`
  CHANGE `code_length` `code_length` VARCHAR (40) CHARSET utf8 COLLATE utf8_general_ci NULL COMMENT '码表长度';
  
  
  
insert into `cmdb_code` (`code_id`, `filed_code`, `filed_name`, `module_catalog_id`, `control_type_id`, `code_length`, `code_tip`, `display_style`, `is_bind_source`, `is_validate`, `is_approve`, `is_collect`, `default_value`, `add_read_only`, `update_read_only`, `sort_index`, `is_delete`) values('ccdf9ef9b35349598df8158b45ede033','id','ID','c5789963e6a24ddbbbcfb464af999811','debb3949-7bbc-11e9-b0c3-0242ac110002','40','','4','否','否','否','否','','1','1','10146','0');
insert into `cmdb_code` (`code_id`, `filed_code`, `filed_name`, `module_catalog_id`, `control_type_id`, `code_length`, `code_tip`, `display_style`, `is_bind_source`, `is_validate`, `is_approve`, `is_collect`, `default_value`, `add_read_only`, `update_read_only`, `sort_index`, `is_delete`) values('11520e2d05c249edb1ce06de09216cc3','insert_person','请求用户','c5789963e6a24ddbbbcfb464af999811','debb3949-7bbc-11e9-b0c3-0242ac110002','40','','4','否','否','否','否','','1','1','10149','0');
insert into `cmdb_code` (`code_id`, `filed_code`, `filed_name`, `module_catalog_id`, `control_type_id`, `code_length`, `code_tip`, `display_style`, `is_bind_source`, `is_validate`, `is_approve`, `is_collect`, `default_value`, `add_read_only`, `update_read_only`, `sort_index`, `is_delete`) values('3b0f8ea2f2dc4966b72c08b72f53235f','insert_time','请求时间','c5789963e6a24ddbbbcfb464af999811','debb3e53-7bbc-11e9-b0c3-0242ac110002','40','','4','否','否','否','否','','1','1','10148','0');
insert into `cmdb_code` (`code_id`, `filed_code`, `filed_name`, `module_catalog_id`, `control_type_id`, `code_length`, `code_tip`, `display_style`, `is_bind_source`, `is_validate`, `is_approve`, `is_collect`, `default_value`, `add_read_only`, `update_read_only`, `sort_index`, `is_delete`) values('53e4f1ab26ec4df5b3be05c343d89fba','module_id','模型ID','c5789963e6a24ddbbbcfb464af999811','debb3949-7bbc-11e9-b0c3-0242ac110002','40','','4','否','否','否','否','','1','1','10147','0');
insert into `cmdb_code` (`code_id`, `filed_code`, `filed_name`, `module_catalog_id`, `control_type_id`, `code_length`, `code_tip`, `display_style`, `is_bind_source`, `is_validate`, `is_approve`, `is_collect`, `default_value`, `add_read_only`, `update_read_only`, `sort_index`, `is_delete`) values('a4d9214a1a8e48a0b11a715f8f2163b5','request_params','请求参数','c5789963e6a24ddbbbcfb464af999811','debb3949-7bbc-11e9-b0c3-0242ac110002','999999999','','3','否','否','否','否','','1','1',NULL,'0');
insert into `cmdb_code` (`code_id`, `filed_code`, `filed_name`, `module_catalog_id`, `control_type_id`, `code_length`, `code_tip`, `display_style`, `is_bind_source`, `is_validate`, `is_approve`, `is_collect`, `default_value`, `add_read_only`, `update_read_only`, `sort_index`, `is_delete`) values('48caf73ccf0a4fba8f1d0e7c54b0e88b','request_url','请求链接','c5789963e6a24ddbbbcfb464af999811','debb3949-7bbc-11e9-b0c3-0242ac110002','400','','4','否','否','否','否','','1','1','10153','0');
insert into `cmdb_code` (`code_id`, `filed_code`, `filed_name`, `module_catalog_id`, `control_type_id`, `code_length`, `code_tip`, `display_style`, `is_bind_source`, `is_validate`, `is_approve`, `is_collect`, `default_value`, `add_read_only`, `update_read_only`, `sort_index`, `is_delete`) values('2c641f0cf9f24f37bed19319d06401c8','response_count','响应记录数','c5789963e6a24ddbbbcfb464af999811','debb3949-7bbc-11e9-b0c3-0242ac110002','40','','4','否','否','否','否','','1','1','10152','0');
insert into `cmdb_code` (`code_id`, `filed_code`, `filed_name`, `module_catalog_id`, `control_type_id`, `code_length`, `code_tip`, `display_style`, `is_bind_source`, `is_validate`, `is_approve`, `is_collect`, `default_value`, `add_read_only`, `update_read_only`, `sort_index`, `is_delete`) values('32d9f341011949b3bfed3bb23222018a','response_ip','请求IP地址','c5789963e6a24ddbbbcfb464af999811','debb3949-7bbc-11e9-b0c3-0242ac110002','40','','4','否','否','否','否','','1','1','10150','0');
insert into `cmdb_code` (`code_id`, `filed_code`, `filed_name`, `module_catalog_id`, `control_type_id`, `code_length`, `code_tip`, `display_style`, `is_bind_source`, `is_validate`, `is_approve`, `is_collect`, `default_value`, `add_read_only`, `update_read_only`, `sort_index`, `is_delete`) values('3fecd3e0d0e8461f9fef817da203dc94','response_time','响应时长(ms)','c5789963e6a24ddbbbcfb464af999811','debb3949-7bbc-11e9-b0c3-0242ac110002','40','','4','否','否','否','否','','1','1','10151','0');
insert into `cmdb_code` (`code_id`, `filed_code`, `filed_name`, `module_catalog_id`, `control_type_id`, `code_length`, `code_tip`, `display_style`, `is_bind_source`, `is_validate`, `is_approve`, `is_collect`, `default_value`, `add_read_only`, `update_read_only`, `sort_index`, `is_delete`) values('2b6bfae43265414f95b2063b3248cce5','update_person','最后请求用户','c5789963e6a24ddbbbcfb464af999811','debb3949-7bbc-11e9-b0c3-0242ac110002','40','','4','否','否','否','否','','1','1','10154','0');
insert into `cmdb_code` (`code_id`, `filed_code`, `filed_name`, `module_catalog_id`, `control_type_id`, `code_length`, `code_tip`, `display_style`, `is_bind_source`, `is_validate`, `is_approve`, `is_collect`, `default_value`, `add_read_only`, `update_read_only`, `sort_index`, `is_delete`) values('c76ac038b27b4b01b2678d7f354f844a','update_time','最后请求时间','c5789963e6a24ddbbbcfb464af999811','debb3e53-7bbc-11e9-b0c3-0242ac110002','40','','4','否','否','否','否','','1','1','10155','0');







