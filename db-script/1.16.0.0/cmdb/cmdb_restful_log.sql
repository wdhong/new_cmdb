/*
SQLyog Ultimate
MySQL - 5.7.30-log : Database - cmdb_online
*********************************************************************
*/



DROP TABLE IF EXISTS `cmdb_restful_log`;

CREATE TABLE `cmdb_restful_log` (
  `id` varchar(40) NOT NULL COMMENT '资产ID',
  `is_delete` int(11) DEFAULT NULL COMMENT '删除标识',
  `module_id` varchar(40) DEFAULT NULL COMMENT '模型ID',
  `request_url` varchar(4000) DEFAULT NULL COMMENT '请求链接',
  `response_ip` varchar(40) DEFAULT NULL COMMENT '请求IP地址',
  `request_params` text COMMENT '请求参数',
  `insert_person` varchar(40) DEFAULT NULL COMMENT '请求用户',
  `insert_time` varchar(40) DEFAULT NULL COMMENT '请求时间',
  `update_person` varchar(40) DEFAULT NULL COMMENT '最后请求用户',
  `update_time` varchar(40) DEFAULT NULL COMMENT '最后请求时间',
  `response_count` varchar(40) DEFAULT NULL COMMENT '响应记录数',
  `response_time` varchar(40) DEFAULT NULL COMMENT '响应时长(ms)',
  `response_status` varchar(40) DEFAULT NULL COMMENT '返回状态',
  `response_describe` text COMMENT '返回描述',
  PRIMARY KEY (`id`),
  KEY `IDX_cmdb_log_id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='日志记录资产表';

