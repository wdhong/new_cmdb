UPDATE `cmdb_instance` a 
LEFT JOIN t_cfg_dict t ON a.device_type = t.`dict_note` AND t.`col_name`='device_type'  AND t.`is_delete`='0'
LEFT JOIN t_cfg_dict c ON a.is_assigned = c.`dict_note` AND c.`col_name`='whether' AND c.`is_delete`='0' 
LEFT JOIN t_cfg_dict d ON a.is_ansible = d.`dict_note` AND d.`col_name`='whether'AND d.`is_delete`='0' 
LEFT JOIN `cmdb_pod_manager` e ON a.pod_name = e.`pod_name` AND a.`idcType` = e.`idc_id` AND e.is_delete=0 
LEFT JOIN t_cfg_dict j ON a.device_status = j.`dict_note` AND j.col_name='device_status' AND j.`is_delete`='0'
LEFT JOIN cmdb_idc_manager k ON a.idcType =k.`idc_name` AND k.`is_delete`='0' 
LEFT JOIN cmdb_org_system l ON a.department1 =l.`orgName` AND l.`parent_id`='0' AND l.`is_delete`='0' 
LEFT JOIN `cmdb_org_system` b ON a.department2 = b.`orgName` AND l.`id`=b.`parent_id` AND b.`is_delete`='0'
LEFT JOIN cmdb_business_system f ON a.bizSystem = f.`bizSystem` AND f.`department1` = l.`id` AND f.`department2` = b.`id` AND f.`is_delete`='0'
LEFT JOIN t_cfg_dict m ON a.device_os_type =m.`dict_note` AND m.col_name='device_os_type' AND m.`is_delete`='0' 
LEFT JOIN t_cfg_dict n ON a.device_class_3 =n.`dict_note` AND n.col_name='device_class_3' AND n.`is_delete`='0' 
LEFT JOIN t_cfg_dict o ON a.is_ipmi_monitor =o.`dict_note` AND o.col_name='whether' AND o.`is_delete`='0' 
LEFT JOIN t_cfg_dict p ON a.is_monitor =p.`dict_note` AND p.col_name='whether' AND p.`is_delete`='0' 
LEFT JOIN t_cfg_dict q ON a.is_stack =q.`dict_note` AND q.col_name='whether' AND q.`is_delete`='0' 
LEFT JOIN t_cfg_dict r ON a.node_type =r.`dict_note` AND r.col_name='cmdb_node_type' AND r.`is_delete`='0' 
LEFT JOIN t_cfg_dict s ON a.is_install_agent =s.`dict_note` AND s.col_name='whether' AND s.`is_delete`='0' 
SET a.department2 = IFNULL(b.id,a.department2),
 a.device_type = IFNULL(t.id,a.device_type),
 a.is_assigned=IFNULL(c.id,a.is_assigned),
 a.is_ansible=IFNULL(d.id,a.is_ansible),
 a.pod_name=IFNULL(e.id,a.pod_name),
 a.bizSystem=IFNULL(f.id,a.bizSystem),
 a.device_status=IFNULL(j.id,a.device_status),
 a.idcType=IFNULL(k.id,a.idcType),
 a.department1=IFNULL(l.id,a.department1),
 a.device_os_type = IFNULL(m.id,a.device_os_type),
 a.device_class_3=IFNULL(n.id,a.device_class_3),
 a.is_ipmi_monitor=IFNULL(o.id,a.is_ipmi_monitor),
 a.is_monitor=IFNULL(p.id,a.is_monitor),
 a.is_stack=IFNULL(q.id,a.is_stack),
 a.node_type=IFNULL(r.id,a.node_type),
 a.is_install_agent =IFNULL(s.id,a.is_install_agent) 

UPDATE `cmdb_instance` a, (select id, room_name, idc_id from cmdb_room_manager where is_delete = '0') room 
set a.roomId=IFNULL(room.id, a.roomId) where a.roomId = room.room_name and a.idcType = room.idc_id;

update cmdb_instance a, (select * from t_cfg_dict where col_name='device_class' and is_delete = '0') t
set a.device_class = IFNULL(t.id,a.device_class) where a.device_class = t.dict_note;

UPDATE `cmdb_instance` a, (select id, project_name, idc_id from cmdb_project_manager where is_delete = '0') room 
set a.project_name=IFNULL(room.id, a.project_name) where a.project_name = room.project_name and a.idcType = room.idc_id;

update cmdb_instance a, (select * from cmdb_dict_produce where produce_type='23824b01736a495e88caa28530950cb8' and is_delete = '0') t
set a.device_mfrs = IFNULL(t.id,a.device_mfrs) where a.device_mfrs = t.produce_name;




















--- 修正
update cmdb_instance_maintenance_new a, (select * from cmdb_dict_produce_new where produce_type='6790c81e7fdc456daf83dd90649776ac' and is_delete = '0') t
set a.provider = IFNULL(t.id,a.provider) where a.provider = t.produce_name;

update cmdb_instance_maintenance_new a, (select * from cmdb_dict_produce_new where produce_type='6790c81e7fdc456daf83dd90649776ac' and is_delete = '0') t
set a.mainten_factory = IFNULL(t.id,a.mainten_factory) where a.mainten_factory = t.produce_name;




