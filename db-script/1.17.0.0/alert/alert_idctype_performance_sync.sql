/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.38
Source Server Version : 50730
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2020-09-21 14:45:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_idctype_performance_sync
-- ----------------------------
DROP TABLE IF EXISTS `alert_idctype_performance_sync`;
CREATE TABLE `alert_idctype_performance_sync` (
  `idcType` varchar(64) DEFAULT NULL COMMENT '资源池,all就是查的所有资源池',
  `item` varchar(12) DEFAULT NULL COMMENT 'cpu/memory',
  `deviceType` varchar(12) DEFAULT NULL COMMENT '设备类型：物理计算资源（X86服务器）；虚拟计算资源（云主机）',
  `fifteen_ratio` decimal(12,2) DEFAULT NULL COMMENT '15%>CPU峰值占比',
  `thirty_ratio` decimal(12,2) DEFAULT NULL COMMENT '80%>CPU峰值>=40%占比',
  `sixty_ratio` decimal(12,2) DEFAULT NULL COMMENT '40%>CPU峰值>=15%占比',
  `eighty_five_ratio` decimal(12,2) DEFAULT NULL,
  `hundred_ratio` decimal(12,2) DEFAULT NULL COMMENT 'CPU峰值>=80%占比',
  `day` varchar(12) DEFAULT NULL COMMENT '日期',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `fifteen_count` int(12) DEFAULT NULL COMMENT '15%>CPU峰值占比',
  `thirty_count` int(12) DEFAULT NULL COMMENT '80%>CPU峰值>=40%占比',
  `sixty_count` int(12) DEFAULT NULL COMMENT '40%>CPU峰值>=15%占比',
  `eighty_five_count` int(12) DEFAULT NULL,
  `hundred_count` int(12) DEFAULT NULL COMMENT 'CPU峰值>=80%占比',
  KEY `day` (`day`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='物理计算资源/虚拟计算资源 表';
