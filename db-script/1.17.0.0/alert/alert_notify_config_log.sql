/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50730
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2020-08-06 09:38:41
*/

SET FOREIGN_KEY_CHECKS=0;

ALTER TABLE `alert_notify_config_log`
ADD COLUMN `id`  int NOT NULL AUTO_INCREMENT FIRST ,
ADD PRIMARY KEY (`id`);

ALTER TABLE `alert_notify_config`
ADD COLUMN `recurrence_count`  int NULL COMMENT '重发次数，-1为不限制次数' AFTER `recurrence_interval_util`;
alter table alert_notify_template add column  subject varchar(2000) not null;

-- ----------------------------
-- Table structure for alert_notify_config_log
-- ----------------------------
DROP TABLE IF EXISTS `alert_notify_config_log_his`;
CREATE TABLE `alert_notify_config_log_his` (
  `id`  int NOT NULL,
  `alert_notify_config_id` varchar(255) DEFAULT NULL,
  `send_status` varchar(255) DEFAULT NULL COMMENT '发送状态 0-失败 1-成功',
  `receiver` varchar(255) DEFAULT NULL COMMENT '接收人',
  `send_time` datetime DEFAULT NULL COMMENT '发送时间',
  `send_content` text COMMENT '发送内容',
  `send_alert_id` varchar(255) DEFAULT NULL,
  `send_ype` varchar(2) DEFAULT NULL COMMENT '发送类型 1-短信 2-邮件(单发) 3-邮件(合并)',
  `resend_time` varchar(255) DEFAULT NULL COMMENT '重发时间',
  `is_resend` varchar(2) DEFAULT NULL COMMENT '是否重发 0-否 1-是',
  PRIMARY KEY (`id`),
  KEY `alert_notify_index` (`receiver`,`send_alert_id`,`send_ype`) USING BTREE,
  KEY `idx_alertId_configId` (`send_alert_id`,`alert_notify_config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警通知配置发送记录历史表';


-- ----------------------------
-- Procedure structure for PRD_MERGE_NOTIFY_LOG
-- ----------------------------
DROP PROCEDURE IF EXISTS `PRD_MERGE_NOTIFY_LOG`;
DELIMITER ;;
CREATE PROCEDURE `PRD_MERGE_NOTIFY_LOG`()
BEGIN
DECLARE last_month_time VARCHAR(20);
SELECT DATE_SUB(curdate(),INTERVAL 1 MONTH) into last_month_time;
INSERT INTO alert_notify_config_log_his (id, `alert_notify_config_id`, `send_status`, `receiver`, `send_time`, `send_content`, `send_alert_id`, `send_ype`, `resend_time`, `is_resend`) 
select id, `alert_notify_config_id`, `send_status`, `receiver`, `send_time`, `send_content`, `send_alert_id`, `send_ype`, `resend_time`, `is_resend`
from alert_notify_config_log where send_time < last_month_time
on DUPLICATE KEY UPDATE id=VALUES(id);
DELETE from alert_notify_config_log where send_time < last_month_time;
SELECT DATE_SUB(curdate(),INTERVAL 1 MONTH) into last_month_time;
INSERT INTO alert_notify_config_log_his (id, `alert_notify_config_id`, `send_status`, `receiver`, `send_time`, `send_content`, `send_alert_id`, `send_ype`, `resend_time`, `is_resend`) 
select id, `alert_notify_config_id`, `send_status`, `receiver`, `send_time`, `send_content`, `send_alert_id`, `send_ype`, `resend_time`, `is_resend`
from alert_notify_config_log where resend_time < last_month_time
on DUPLICATE KEY UPDATE id=VALUES(id);
DELETE from alert_notify_config_log where resend_time < last_month_time;
END
;;
DELIMITER ;

-- ----------------------------
-- Event structure for EVENT_MERGE_NOTIFY_LOG
-- ----------------------------
DROP EVENT IF EXISTS `EVENT_MERGE_NOTIFY_LOG`;
DELIMITER ;;
CREATE EVENT `EVENT_MERGE_NOTIFY_LOG` ON SCHEDULE EVERY 1 DAY STARTS '2020-08-06 01:00:00' ON COMPLETION NOT PRESERVE ENABLE COMMENT '超过1月通知记录移入历史表' DO call PRD_MERGE_NOTIFY_LOG()
;;
DELIMITER ;
