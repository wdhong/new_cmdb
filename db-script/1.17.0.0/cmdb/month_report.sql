/*
SQLyog Ultimate
MySQL - 5.7.30-log : Database - cmdb_zy_online
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cmdb_zy_online` /*!40100 DEFAULT CHARACTER SET utf8 */;

/*Table structure for table `alert_month_idctype_sync` */

DROP TABLE IF EXISTS `alert_month_idctype_sync`;

CREATE TABLE `alert_month_idctype_sync` (
  `idcType` varchar(64) DEFAULT NULL COMMENT '资源池',
  `cpu_max` varchar(12) DEFAULT NULL COMMENT '月度CPU资源峰值利用率',
  `cpu_avg` varchar(12) DEFAULT NULL COMMENT '月度CPU资源均值利用率',
  `memory_avg` varchar(12) DEFAULT NULL COMMENT '月度内存均值利用率',
  `memory_max` varchar(12) DEFAULT NULL COMMENT '月度内存峰值利用率',
  `deviceType` varchar(12) DEFAULT NULL COMMENT '设备类型：物理计算资源（X86服务器）；虚拟计算资源（云主机）',
  `cpu_eighty_ratio` varchar(12) DEFAULT NULL COMMENT '80%>CPU峰值>=40%占比',
  `cpu_fourty_ratio` varchar(12) DEFAULT NULL COMMENT '40%>CPU峰值>=15%占比',
  `cpu_fifteen_ratio` varchar(12) DEFAULT NULL COMMENT '15%>CPU峰值占比',
  `cpu_eighty_more_ratio` varchar(12) DEFAULT NULL COMMENT 'CPU峰值>=80%占比',
  `memory_eighty_ratio` varchar(12) DEFAULT NULL COMMENT '80%>内存峰值>=40%占比',
  `memory_fourty_ratio` varchar(12) DEFAULT NULL COMMENT '40%>内存峰值>=15%占比',
  `memory_fifteen_ratio` varchar(12) DEFAULT NULL COMMENT '15%>内存峰值占比',
  `memory_eighty_more_ratio` varchar(12) DEFAULT NULL COMMENT '内存峰值>=80%占比',
  `month` varchar(12) DEFAULT NULL COMMENT '月份',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` smallint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_r_alert_id` (`memory_avg`) USING BTREE,
  KEY `index_isolate_id` (`cpu_max`) USING BTREE,
  KEY `type` (`type`,`month`)
) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=utf8 COMMENT='表2-资源性能-表2-1-1 已上线物理服务器资源利用率';

/*Table structure for table `alert_month_report_alert_sum` */

DROP TABLE IF EXISTS `alert_month_report_alert_sum`;

CREATE TABLE `alert_month_report_alert_sum` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `idcType` varchar(255) DEFAULT NULL COMMENT '资源池',
  `alert_sum` bigint(255) DEFAULT NULL COMMENT '告警总量',
  `serious_sum` bigint(255) DEFAULT NULL COMMENT '严重告警总量',
  `high_sum` bigint(255) DEFAULT NULL COMMENT '重要告警总量',
  `middle_sum` bigint(255) DEFAULT NULL COMMENT '次要告警总量',
  `low_sum` bigint(255) DEFAULT NULL COMMENT '低和提示告警总量',
  `month` varchar(255) DEFAULT NULL COMMENT '月份',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='表3-资源池告警-表3-1 告警数量总计';

/*Table structure for table `bpm_month_report_order` */

DROP TABLE IF EXISTS `bpm_month_report_order`;

CREATE TABLE `bpm_month_report_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idcType` varchar(20) DEFAULT NULL COMMENT '资源节点',
  `change_order` varchar(20) DEFAULT NULL COMMENT '变更请求',
  `alert_order` varchar(20) DEFAULT NULL COMMENT '告警工单',
  `error_order` varchar(20) DEFAULT NULL COMMENT '故障工单',
  `network_strategy_order` varchar(20) DEFAULT NULL COMMENT '网络策略工单',
  `maintaince_order` varchar(20) DEFAULT NULL COMMENT '维保工单',
  `resource_apply_order` varchar(20) DEFAULT NULL COMMENT '资源申请工单',
  `avg_process_time` varchar(20) DEFAULT NULL COMMENT '工单处理平均时(天)',
  `process_completion_rate` varchar(20) NOT NULL COMMENT '工单处理完成率',
  `month` varchar(20) DEFAULT NULL COMMENT '月份',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='表5-工单';

/*Table structure for table `bpm_month_report_trouble_all` */

DROP TABLE IF EXISTS `bpm_month_report_trouble_all`;

CREATE TABLE `bpm_month_report_trouble_all` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idcType` varchar(20) DEFAULT NULL COMMENT '资源池池名称',
  `extraordinary_accident` varchar(20) DEFAULT NULL COMMENT '异常事件',
  `soft_hardware_problem` varchar(20) DEFAULT NULL COMMENT '软硬件问题',
  `fault_promptness_rate` varchar(20) DEFAULT NULL COMMENT '问题处理及时率',
  `significant_trouble_state` varchar(500) DEFAULT NULL COMMENT '重大故障概述',
  `pool_total` varchar(20) DEFAULT NULL COMMENT '各类资源池合计',
  `month` varchar(20) DEFAULT NULL COMMENT '月份',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='表4-资源池故障-表4-1 故障数量总计';

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
