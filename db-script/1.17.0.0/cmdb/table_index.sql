ALTER TABLE `cmdb_instance`
  ADD INDEX `IDX_cmdb_instance_new_01` (
    `device_type`,
    `device_class`,
    `device_class_3`
  );

ALTER TABLE `cmdb_idc_manager`
  ADD INDEX `IDX_cmdb_idc_manager_01` (`is_delete`, `idc_name`);

ALTER TABLE `cmdb_org_system`
  ADD INDEX `IDX_cmdb_org_system_01` (
    `is_delete`,
    `orgName`,
    `parent_id`
  );

ALTER TABLE `t_cfg_dict`
  ADD INDEX `IDX_t_cfg_dict_02` (
    `is_delete`,
    `dict_note`,
    `col_name`,
    `up_dict`
  ),
  ADD INDEX `IDX_t_cfg_dict_03` (`is_delete`, `col_name`);

