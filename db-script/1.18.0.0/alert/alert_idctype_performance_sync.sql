/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.38
Source Server Version : 50730
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2020-09-21 14:45:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_idctype_performance_sync
-- ----------------------------
DROP TABLE IF EXISTS `alert_idctype_performance_sync`;
CREATE TABLE `alert_idctype_performance_sync` (
  `idcType` varchar(64) DEFAULT NULL COMMENT '资源池,all就是查的所有资源池',
  `item` varchar(12) DEFAULT NULL COMMENT 'cpu/memory',
  `deviceType` varchar(12) DEFAULT NULL COMMENT '设备类型：物理计算资源（X86服务器）；虚拟计算资源（云主机）',
  `fifteen_ratio` decimal(12,2) DEFAULT NULL COMMENT '15%>CPU峰值占比',
  `thirty_ratio` decimal(12,2) DEFAULT NULL COMMENT '80%>CPU峰值>=40%占比',
  `sixty_ratio` decimal(12,2) DEFAULT NULL COMMENT '40%>CPU峰值>=15%占比',
  `eighty_five_ratio` decimal(12,2) DEFAULT NULL,
  `hundred_ratio` decimal(12,2) DEFAULT NULL COMMENT 'CPU峰值>=80%占比',
  `day` varchar(12) DEFAULT NULL COMMENT '日期',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `fifteen_count` int(12) DEFAULT NULL COMMENT '15%>CPU峰值占比',
  `thirty_count` int(12) DEFAULT NULL COMMENT '80%>CPU峰值>=40%占比',
  `sixty_count` int(12) DEFAULT NULL COMMENT '40%>CPU峰值>=15%占比',
  `eighty_five_count` int(12) DEFAULT NULL,
  `hundred_count` int(12) DEFAULT NULL COMMENT 'CPU峰值>=80%占比',
  KEY `day` (`day`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='物理计算资源/虚拟计算资源 表';


INSERT INTO `alert_model_field` (`id`, `model_id`, `field_code`, `field_name`, `field_type`, `is_lock`, `jdbc_type`, `jdbc_length`, `jdbc_tip`, `ci_field_code`, `is_ci_params`, `params_name`, `ci_field_name`, `is_list_show`, `list_show_sort`, `list_show_name`, `list_show_pattern`, `table_column_widch`, `is_detail_show`, `detail_show_sort`, `detail_show_name`, `detail_show_pattern`, `is_query_param`, `query_param_sort`, `query_param_name`, `query_param_type`, `query_param_source`, `is_derive_field`, `derive_field_sort`, `derive_field_name`, `derive_field_type`, `derive_field_source`, `creator`, `create_time`, `updater`, `update_time`) VALUES ('ee186781-4172-4c09-8e3c-a592822885a6', 'abf0444b-e44f-46ef-83fa-59b05ddf6336', 'is_ipmi_monitor', '带外网是否监控', '2', '0', 'varchar', '40', '', 'is_ipmi_monitor_dict_note_name', '0', NULL, '带外网是否监控', '0', '0', NULL, NULL, NULL, '0', '0', NULL, NULL, '0', '0', NULL, NULL, NULL, '0', '0', NULL, NULL, NULL, 'alauda', '2020-09-17 19:50:31', NULL, NULL);
INSERT INTO `alert_model_field` (`id`, `model_id`, `field_code`, `field_name`, `field_type`, `is_lock`, `jdbc_type`, `jdbc_length`, `jdbc_tip`, `ci_field_code`, `is_ci_params`, `params_name`, `ci_field_name`, `is_list_show`, `list_show_sort`, `list_show_name`, `list_show_pattern`, `table_column_widch`, `is_detail_show`, `detail_show_sort`, `detail_show_name`, `detail_show_pattern`, `is_query_param`, `query_param_sort`, `query_param_name`, `query_param_type`, `query_param_source`, `is_derive_field`, `derive_field_sort`, `derive_field_name`, `derive_field_type`, `derive_field_source`, `creator`, `create_time`, `updater`, `update_time`) VALUES ('82d86b2c-0144-48b4-8e6d-38d25f020229', 'abf0444b-e44f-46ef-83fa-59b05ddf6336', 'is_ansible', '管理网是否监控', '2', '0', 'varchar', '40', '', 'is_ansible_dict_note_name', '0', NULL, '管理网是否监控', '0', '0', NULL, NULL, NULL, '0', '0', NULL, NULL, '0', '0', NULL, NULL, NULL, '0', '0', NULL, NULL, NULL, 'alauda', '2020-09-17 19:48:14', NULL, NULL);

ALTER TABLE cmdb_instance add  is_ansible varchar(40)  NULL COMMENT '管理网是否监控';
ALTER TABLE cmdb_instance add  is_ipmi_monitor varchar(40) NULL COMMENT '带外网是否监控';


