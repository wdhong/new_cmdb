/*
Navicat MySQL Data Transfer

Source Server         : ums测试环境
Source Server Version : 50730
Source Host           : 10.12.70.40:3306
Source Database       : bills

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2020-10-12 19:37:15
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_bills_account_balance
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_bills_account_balance`;
CREATE TABLE `cmdb_bills_account_balance` (
  `id` varchar(40) NOT NULL,
  `department_id` varchar(40) DEFAULT NULL COMMENT '租户ID',
  `account_code` varchar(40) DEFAULT NULL,
  `balance` double DEFAULT NULL COMMENT '余额',
  `account_manager` varchar(40) DEFAULT NULL COMMENT '账户经理',
  `account_manager_phone` varchar(20) DEFAULT NULL COMMENT '账户经理电话',
  `account_manager_email` varchar(50) DEFAULT NULL COMMENT '账户经理邮箱',
  `insert_time` datetime DEFAULT NULL COMMENT '录入时间',
  `insert_person` varchar(40) DEFAULT NULL COMMENT '录入人',
  `update_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  `update_person` varchar(40) DEFAULT NULL COMMENT '最后修改人',
  `is_delete` int(11) DEFAULT NULL COMMENT '删除表示 0 未删除 1 已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
