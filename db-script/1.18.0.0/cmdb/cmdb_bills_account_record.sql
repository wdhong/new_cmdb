/*
Navicat MySQL Data Transfer

Source Server         : ums测试环境
Source Server Version : 50730
Source Host           : 10.12.70.40:3306
Source Database       : bills

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2020-10-12 19:37:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_bills_account_record
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_bills_account_record`;
CREATE TABLE `cmdb_bills_account_record` (
  `id` varchar(40) NOT NULL,
  `account_id` varchar(40) DEFAULT NULL COMMENT '账户ID',
  `amount` double DEFAULT NULL COMMENT '交易金额',
  `deal_type` int(11) DEFAULT NULL COMMENT '交易类型 0 支出  1收入',
  `pay_method` varchar(40) DEFAULT NULL COMMENT '剩余金额',
  `deal_month` varchar(40) DEFAULT NULL COMMENT '交易的月份',
  `insert_time` datetime DEFAULT NULL COMMENT '录入时间',
  `insert_person` varchar(40) DEFAULT NULL COMMENT '录入人',
  `update_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  `update_person` varchar(40) DEFAULT NULL COMMENT '最后修改人',
  `is_delete` int(11) NOT NULL COMMENT '删除标识 0 未删除 1已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
