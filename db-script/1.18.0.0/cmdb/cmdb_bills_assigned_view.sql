create view cmdb_bills_assigned_view AS
SELECT
	`sys`.`department_type` AS `deptType`,
	`biz`.`department2` AS `deptId`,
	`sys`.`orgName` AS `deptName`,
	sum(
		ifnull(
			`quo`.`ljs_allocation_amount`,
			0
		)
	) AS `ljs`,
	sum(
		ifnull(
			`quo`.`yzj_allocation_amount`,
			0
		)
	) AS `yzj`,
	round(
		sum(
			(
				(
					(
						(
							(
								ifnull(
									`quo`.`fcsan_allocation_amount`,
									0
								) + ifnull(
									`quo`.`ipsan_allocation_amount`,
									0
								)
							) + ifnull(
								`quo`.`bfcc_allocation_amount`,
								0
							)
						) + ifnull(
							`quo`.`dxcc_allocation_amount`,
							0
						)
					) + ifnull(
						`quo`.`kcc_allocation_amount`,
						0
					)
				) + ifnull(
					`quo`.`wjcc_allocation_amount`,
					0
				)
			)
		),
		2
	) AS `cczy`
FROM
	(
		(
			(
				`cmdb_business_quota` `quo`
				LEFT JOIN `cmdb_business_system` `biz` ON (
					(
						`quo`.`owner_biz_system` = `biz`.`id`
					)
				)
			)
			LEFT JOIN `cmdb_org_system` `sys` ON (
				(
					`biz`.`department2` = `sys`.`id`
				)
			)
		)
		LEFT JOIN `t_cfg_dict` `d` ON (
			(
				`d`.`id` = `sys`.`department_type`
			)
		)
	)
WHERE
	(
		(`quo`.`is_delete` = 0)
		AND (`sys`.`is_delete` = 0)
		AND (`d`.`is_delete` = 0)
		AND (
			`d`.`col_name` = 'department_type'
		)
	)
GROUP BY
	`sys`.`department_type`,
	`sys`.`orgName`,
	`biz`.`department2`
UNION
	SELECT
		`sys`.`department_type` AS `deptType`,
		`biz`.`department1` AS `deptId`,
		`sys`.`orgName` AS `deptName`,
		sum(
			ifnull(
				`quo`.`ljs_allocation_amount`,
				0
			)
		) AS `ljs`,
		sum(
			ifnull(
				`quo`.`yzj_allocation_amount`,
				0
			)
		) AS `yzj`,
		round(
			sum(
				(
					(
						(
							(
								(
									ifnull(
										`quo`.`fcsan_allocation_amount`,
										0
									) + ifnull(
										`quo`.`ipsan_allocation_amount`,
										0
									)
								) + ifnull(
									`quo`.`bfcc_allocation_amount`,
									0
								)
							) + ifnull(
								`quo`.`dxcc_allocation_amount`,
								0
							)
						) + ifnull(
							`quo`.`kcc_allocation_amount`,
							0
						)
					) + ifnull(
						`quo`.`wjcc_allocation_amount`,
						0
					)
				)
			),
			2
		) AS `cczy`
	FROM
		(
			(
				(
					`cmdb_business_quota` `quo`
					LEFT JOIN `cmdb_business_system` `biz` ON (
						(
							`quo`.`owner_biz_system` = `biz`.`id`
						)
					)
				)
				LEFT JOIN `cmdb_org_system` `sys` ON (
					(
						`biz`.`department1` = `sys`.`id`
					)
				)
			)
			LEFT JOIN `t_cfg_dict` `d` ON (
				(
					`d`.`id` = `sys`.`department_type`
				)
			)
		)
	WHERE
		(
			(`quo`.`is_delete` = 0)
			AND (`sys`.`is_delete` = 0)
			AND (`d`.`is_delete` = 0)
			AND (
				`d`.`col_name` = 'department_type'
			)
		)
	GROUP BY
		`sys`.`department_type`,
		`sys`.`orgName`,
		`biz`.`department1`