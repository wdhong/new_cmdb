/*
Navicat MySQL Data Transfer

Source Server         : ums测试环境
Source Server Version : 50730
Source Host           : 10.12.70.40:3306
Source Database       : bills

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2020-10-12 19:37:31
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_bills_day_record
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_bills_day_record`;
CREATE TABLE `cmdb_bills_day_record` (
  `id` varchar(40) NOT NULL,
  `department_id` varchar(40) DEFAULT NULL COMMENT '租户ID',
  `business_system_id` varchar(40) DEFAULT NULL COMMENT '业务系统ID',
  `idc_id` varchar(40) DEFAULT NULL COMMENT '资源池ID',
  `pod_id` varchar(40) DEFAULT NULL,
  `device_type_id` varchar(40) DEFAULT NULL COMMENT '设备类型ID',
  `quota` varchar(40) DEFAULT NULL COMMENT '配额数量',
  `discount` double DEFAULT NULL COMMENT '折扣率',
  `price` double DEFAULT NULL COMMENT '单价',
  `discount_before` double DEFAULT NULL COMMENT '折扣前总价',
  `total_money` double DEFAULT NULL COMMENT '当日总价',
  `charge_time` datetime DEFAULT NULL COMMENT '计费日期',
  `insert_time` datetime DEFAULT NULL COMMENT '录入时间',
  `insert_person` varchar(40) DEFAULT NULL COMMENT '录入人',
  `update_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  `update_person` varchar(40) DEFAULT NULL COMMENT '最后修改人',
  `is_delete` int(11) DEFAULT NULL COMMENT '删除标识 0 未删除 1 已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
