/*
Navicat MySQL Data Transfer

Source Server         : ums测试环境
Source Server Version : 50730
Source Host           : 10.12.70.40:3306
Source Database       : bills

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2020-10-12 19:37:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_bills_discount
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_bills_discount`;
CREATE TABLE `cmdb_bills_discount` (
  `id` varchar(40) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `department_id` varchar(40) NOT NULL COMMENT '租户ID',
  `business_system_id` varchar(40) NOT NULL COMMENT '业务系统ID',
  `idc_id` varchar(40) NOT NULL COMMENT '资源池ID',
  `discount` double DEFAULT NULL COMMENT '折扣率',
  `insert_time` datetime DEFAULT NULL,
  `insert_person` varchar(40) DEFAULT NULL COMMENT '录入人',
  `update_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  `update_person` varchar(40) DEFAULT NULL COMMENT '最后修改人',
  `is_delete` int(11) NOT NULL COMMENT '删除标识 0 未删除 1 已删除',
  PRIMARY KEY (`id`,`department_id`,`business_system_id`,`idc_id`,`is_delete`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
