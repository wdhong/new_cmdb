/*
Navicat MySQL Data Transfer

Source Server         : ums测试环境
Source Server Version : 50730
Source Host           : 10.12.70.40:3306
Source Database       : bills

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2020-10-12 19:37:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_bills_month_record
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_bills_month_record`;
CREATE TABLE `cmdb_bills_month_record` (
  `id` varchar(40) NOT NULL,
  `department_id` varchar(40) DEFAULT NULL COMMENT '租户ID',
  `business_system_id` varchar(40) DEFAULT NULL COMMENT '业务系统ID',
  `idc_id` varchar(40) DEFAULT NULL COMMENT '资源池ID',
  `pod_id` varchar(40) DEFAULT NULL,
  `device_type_id` varchar(40) DEFAULT NULL COMMENT '设备类型ID',
  `account_id` varchar(40) DEFAULT NULL COMMENT '缴费账号',
  `need_pay` bigint(20) DEFAULT NULL COMMENT '应缴费用',
  `real_pay` bigint(20) DEFAULT NULL COMMENT '租户的实缴费用',
  `discount_before_pay` bigint(20) DEFAULT NULL COMMENT '折扣前费用',
  `charge_time` varchar(40) DEFAULT NULL,
  `insert_time` datetime DEFAULT NULL COMMENT '录入时间',
  `insert_person` varchar(40) DEFAULT NULL COMMENT '录入人',
  `update_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  `update_person` varchar(40) DEFAULT NULL COMMENT '最后修改人',
  `is_delete` int(11) DEFAULT NULL COMMENT '删除标识 0 未删除 1 已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
