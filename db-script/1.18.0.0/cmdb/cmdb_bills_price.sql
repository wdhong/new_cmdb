/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : localhost:3306
 Source Schema         : bills

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 12/10/2020 17:48:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cmdb_bills_price
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_bills_price`;
CREATE TABLE `cmdb_bills_price`  (
  `id` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `device_type_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备类型ID',
  `idc_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资源池ID',
  `price` double NULL DEFAULT NULL COMMENT '单价',
  `unit` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单位',
  `insert_time` datetime(0) NULL DEFAULT NULL COMMENT '录入时间',
  `insert_person` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '录入人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '最后修改时间',
  `update_person` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改人',
  `is_delete` int(0) NULL DEFAULT NULL COMMENT '是否删除 0 未删除 1 已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmdb_bills_price
-- ----------------------------
INSERT INTO `cmdb_bills_price` VALUES ('09e5e1c172b441e8bd3f2bd5db640545', '0028b830fab64280a08e8eb05c38ba13', '6d40d7d3-90a7-11e9-bb30-0242ac110002', 300, '元/T/月', '2020-08-26 03:07:24', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('1f94afc761e64715b102c061f553b8ae', '0028b822fab64280a08e8eb05c38ba13', '6d40d7d3-90a7-11e9-bb30-0242ac110002', 3578, '元/台/月', '2020-08-27 08:05:51', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('257a21b0ec404eb49fae9cc29f8e4681', '0028b827fab64280a08e8eb05c38ba13', '6d40dbaf-90a7-11e9-bb30-0242ac110002', 5, '元/T/月', '2020-08-27 07:55:37', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('2621e34122714157b5c7fd65f9245713', '0028b820fab64280a08e8eb05c38ba13', '6d40d7d3-90a7-11e9-bb30-0242ac110002', 3133, '元/台/月', '2020-08-27 07:57:41', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('294c1a1c55ce47aa9479fe66aef383ce', '0028b822fab64280a08e8eb05c38ba13', '6d40dbaf-90a7-11e9-bb30-0242ac110002', 3252, '元/台/月', '2020-08-27 08:05:39', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('2dea4d81f79b468b81e5d11aba05d9cc', '0028b826fab64280a08e8eb05c38ba13', '6d40d847-90a7-11e9-bb30-0242ac110002', 16, '元/个/月', '2020-08-27 07:56:22', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('35233c1d30504331aefc2cbe1ead6bc8', '0028b823fab64280a08e8eb05c38ba13', '6d40d847-90a7-11e9-bb30-0242ac110002', 3539, '元/台/月', '2020-08-27 08:01:07', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('38e310398d5f4ad5b1f47a4ab77e3b6d', '0028b832fab64280a08e8eb05c38ba13', '6d40dbaf-90a7-11e9-bb30-0242ac110002', 100, '元/T/月', '2020-08-26 02:40:46', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('38f02a6c1c1f4c4ba63125bd59538f1f', '0028b828fab64280a08e8eb05c38ba13', '6d40db4e-90a7-11e9-bb30-0242ac110002', 350, '元/T/月', '2020-08-26 02:47:13', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('3b1ea52cddd942718c15d053ecd0cb9a', '0028b824fab64280a08e8eb05c38ba13', '6d40db4e-90a7-11e9-bb30-0242ac110002', 3914, '元/台/月', '2020-08-27 08:00:10', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('3d75de08d72c4366b82fcb18f55b39e5', '0028b830fab64280a08e8eb05c38ba13', '6d40dbaf-90a7-11e9-bb30-0242ac110002', 300, '元/T/月', '2020-08-26 03:07:14', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('4f5ef1640c334333b42fa0c32ed0264b', '0028b823fab64280a08e8eb05c38ba13', '6d40dbaf-90a7-11e9-bb30-0242ac110002', 3539, '元/台/月', '2020-08-27 08:01:15', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('50d9ce71c6054dccabe1597e5d908f82', '0028b825fab64280a08e8eb05c38ba13', '6d40dbaf-90a7-11e9-bb30-0242ac110002', 9422, '元/台/月', '2020-08-27 07:58:52', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('51404b392e954913bee6d9a5e0cc7632', '0028b822fab64280a08e8eb05c38ba13', '6d40d847-90a7-11e9-bb30-0242ac110002', 3252, '元/台/月', '2020-08-27 08:05:34', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('53940cf210aa4281aa208838af8457d5', '0028b825fab64280a08e8eb05c38ba13', '6d40d847-90a7-11e9-bb30-0242ac110002', 9422, '元/台/月', '2020-08-27 07:58:41', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('596a0d9af8254199beae9c5acaa2ff24', '0028b833fab64280a08e8eb05c38ba13', '6d40d847-90a7-11e9-bb30-0242ac110002', 70, '元/T/月', '2020-08-26 02:39:39', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('5a5e43bc22c041418d68f25210a66f2b', '0028b833fab64280a08e8eb05c38ba13', '6d40db4e-90a7-11e9-bb30-0242ac110002', 70, '元/T/月', '2020-08-26 02:39:52', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('5b3cc1bc06d34229b2200effca0cc717', '0028b823fab64280a08e8eb05c38ba13', '6d40d7d3-90a7-11e9-bb30-0242ac110002', 3951, '元/台/月', '2020-08-27 08:01:26', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('60353219e78c43a0a25b2d86d05eab5b', '0028b831fab64280a08e8eb05c38ba13', '6d40db4e-90a7-11e9-bb30-0242ac110002', 100, '元/T/月', '2020-08-26 02:41:36', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('715b5d425e8d41f69adb52a28a3edb5e', '0028b826fab64280a08e8eb05c38ba13', '6d40d7d3-90a7-11e9-bb30-0242ac110002', 21, '元/个/月', '2020-08-27 07:56:41', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('77a1678c48484578be541817acfc6908', '0028b824fab64280a08e8eb05c38ba13', '6d40d847-90a7-11e9-bb30-0242ac110002', 3914, '元/台/月', '2020-08-27 07:59:57', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('8ac86eff73e2479fb0878c4d337ab6ad', '0028b832fab64280a08e8eb05c38ba13', '6d40db4e-90a7-11e9-bb30-0242ac110002', 100, '元/T/月', '2020-08-26 02:40:52', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('9c9a02a606bb4600b571990682491418', '0028b826fab64280a08e8eb05c38ba13', '6d40db4e-90a7-11e9-bb30-0242ac110002', 16, '元/个/月', '2020-08-27 07:56:35', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('9cdf770f5b6041f4a012149147730ce1', '0028b827fab64280a08e8eb05c38ba13', '6d40db4e-90a7-11e9-bb30-0242ac110002', 5, '元/T/月', '2020-08-27 07:55:44', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('9e56981b9e5b423492313ac6d294e497', '0028b831fab64280a08e8eb05c38ba13', '6d40dbaf-90a7-11e9-bb30-0242ac110002', 100, '元/T/月', '2020-08-26 02:41:30', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('a048ce3e45474abfa79c2696b8922a29', '0028b821fab64280a08e8eb05c38ba13', '6d40dbaf-90a7-11e9-bb30-0242ac110002', 3392, '元/台/月', '2020-08-27 08:06:32', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('aba3d2f3c4c04a56ba17c78b7b79a7d8', '0028b830fab64280a08e8eb05c38ba13', '6d40d847-90a7-11e9-bb30-0242ac110002', 300, '元/T/月', '2020-08-26 02:48:10', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('b550e2fc16a24db0a47296e6ee348d61', '0028b828fab64280a08e8eb05c38ba13', '6d40d7d3-90a7-11e9-bb30-0242ac110002', 350, '元/T/月', '2020-08-26 02:47:19', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('bbddab37a52a49aa819327120522946b', '0028b821fab64280a08e8eb05c38ba13', '6d40d847-90a7-11e9-bb30-0242ac110002', 3392, '元/台/月', '2020-08-27 08:06:26', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('be2fee611bc747fb8ac677f9a6e69e60', '0028b831fab64280a08e8eb05c38ba13', '6d40d847-90a7-11e9-bb30-0242ac110002', 100, '元/T/月', '2020-08-26 02:41:23', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('bf4a033f05e843798597cff12ceff117', '0028b825fab64280a08e8eb05c38ba13', '6d40db4e-90a7-11e9-bb30-0242ac110002', 9422, '元/台/月', '2020-08-27 07:59:01', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('bf51f6c21410454a80900de9e6eee8a5', '0028b828fab64280a08e8eb05c38ba13', '6d40d847-90a7-11e9-bb30-0242ac110002', 350, '元/T/月', '2020-08-26 02:47:03', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('c01af1806a0e4cfb81a22fdafcc7cf74', '0028b821fab64280a08e8eb05c38ba13', '6d40d7d3-90a7-11e9-bb30-0242ac110002', 3600, '元/台/月', '2020-08-27 08:06:45', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('c6d0d12d4eae420783e213e7b1c819ed', '0028b825fab64280a08e8eb05c38ba13', '6d40d7d3-90a7-11e9-bb30-0242ac110002', 9681, '元/台/月', '2020-08-27 07:59:08', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('c9574b162fb44493996390c7dec5fdd4', '0028b823fab64280a08e8eb05c38ba13', '6d40db4e-90a7-11e9-bb30-0242ac110002', 3539, '元/台/月', '2020-08-27 08:01:21', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('cf66f37fc492439aa11de5a0b090d80c', '0028b824fab64280a08e8eb05c38ba13', '6d40d7d3-90a7-11e9-bb30-0242ac110002', 4614, '元/台/月', '2020-08-27 08:00:16', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('d27b9657afea4095a986f72c51721f29', '0028b833fab64280a08e8eb05c38ba13', '6d40dbaf-90a7-11e9-bb30-0242ac110002', 70, '元/T/月', '2020-08-26 02:39:45', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('dd4f15d8ea714b70920910296e54bf6e', '0028b824fab64280a08e8eb05c38ba13', '6d40dbaf-90a7-11e9-bb30-0242ac110002', 3914, '元/台/月', '2020-08-27 08:00:03', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('deac35243ec8421cb8e6943f3f817acb', '0028b822fab64280a08e8eb05c38ba13', '6d40db4e-90a7-11e9-bb30-0242ac110002', 3252, '元/台/月', '2020-08-27 08:05:46', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('e37ab9b033564c7a9534924070c175ac', '0028b830fab64280a08e8eb05c38ba13', '6d40db4e-90a7-11e9-bb30-0242ac110002', 300, '元/T/月', '2020-08-26 03:07:19', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('e6fcde8d08d3466f88088f08af89cc25', '0028b821fab64280a08e8eb05c38ba13', '6d40db4e-90a7-11e9-bb30-0242ac110002', 3392, '元/台/月', '2020-08-27 08:06:38', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('e9fa3c875ac6469fba5135ca6e070590', '0028b828fab64280a08e8eb05c38ba13', '6d40dbaf-90a7-11e9-bb30-0242ac110002', 350, '元/T/月', '2020-08-26 02:47:08', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('eeacf11cbafa4bf6839a52889d568c01', '0028b826fab64280a08e8eb05c38ba13', '6d40dbaf-90a7-11e9-bb30-0242ac110002', 16, '元/个/月', '2020-08-27 07:56:29', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('eebaddaf59a4418d9957d49d94da5326', '0028b831fab64280a08e8eb05c38ba13', '6d40d7d3-90a7-11e9-bb30-0242ac110002', 100, '元/T/月', '2020-08-26 02:41:42', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('f57be95cd4e74666b6cc8972e9c1a244', '0028b827fab64280a08e8eb05c38ba13', '6d40d847-90a7-11e9-bb30-0242ac110002', 5, '元/T/月', '2020-08-27 07:55:28', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('f92e81e1b4144676bda967909506d29a', '0028b833fab64280a08e8eb05c38ba13', '6d40d7d3-90a7-11e9-bb30-0242ac110002', 70, '元/T/月', '2020-08-26 02:39:58', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('f989dbaa1e284dc0b7e6423ef1be54e9', '0028b827fab64280a08e8eb05c38ba13', '6d40d7d3-90a7-11e9-bb30-0242ac110002', 6, '元/T/月', '2020-08-27 07:55:50', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('fae462adb4804968bac0597eb71f551c', '0028b832fab64280a08e8eb05c38ba13', '6d40d847-90a7-11e9-bb30-0242ac110002', 100, '元/T/月', '2020-08-26 02:40:41', 'admin', NULL, NULL, 0);
INSERT INTO `cmdb_bills_price` VALUES ('fd9b7ff60af14392990ae114dfba535a', '0028b832fab64280a08e8eb05c38ba13', '6d40d7d3-90a7-11e9-bb30-0242ac110002', 100, '元/T/月', '2020-08-26 02:40:59', 'admin', NULL, NULL, 0);

SET FOREIGN_KEY_CHECKS = 1;
