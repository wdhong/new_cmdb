ALTER TABLE `cmdb_module`
  ADD COLUMN `is_vice` INT (2) DEFAULT 0 NOT NULL COMMENT '是否附属模型（0:否 1:是）' AFTER `is_delete`;