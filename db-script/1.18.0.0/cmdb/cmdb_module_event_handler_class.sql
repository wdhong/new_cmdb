/*
SQLyog Ultimate
MySQL - 5.7.30-log 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

insert into `cmdb_module_event_handler_class` (`id`, `event_class`, `handler_name`, `handler_class`, `remark`) values('32','kafka','通用-发送kafka信息处理类','com.aspire.ums.cmdb.v3.module.event.data.SyncInstanceToKafkaNormalEvent',NULL);
insert into `cmdb_module_event_handler_class` (`id`, `event_class`, `handler_name`, `handler_class`, `remark`) values('33','kafka','告警-发送kafka信息处理类','com.aspire.ums.cmdb.v3.module.event.data.SyncInstanceToKafkaAlertEvent',NULL);
insert into `cmdb_module_event_handler_class` (`id`, `event_class`, `handler_name`, `handler_class`, `remark`) values('31','module','实例变更(新增、更新、删除)发送至kafka处理类','com.aspire.ums.cmdb.v3.module.event.data.SyncInstanceToKafkaEvent',NULL);
