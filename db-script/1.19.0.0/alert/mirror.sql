
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_idctype_performance_sync
-- ----------------------------
DROP TABLE IF EXISTS `alert_device_sync`;
CREATE TABLE `alert_device_sync` (
  `resourceId` varchar(64) NOT NULL COMMENT '资源池',
  `avg_value_week` decimal(12,2) DEFAULT NULL COMMENT '周均值利用率',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `date` varchar(22) NOT NULL COMMENT '数据时间',
  `type` smallint(1) NOT NULL COMMENT '1:周；2：月',
  `diff_value` decimal(12,2) DEFAULT NULL COMMENT '近一周与近一个月使用率差值',
  `item` varchar(12) NOT NULL COMMENT 'cpu/memory',
  `avg_value_month` decimal(12,2) DEFAULT NULL COMMENT '月平均利用率',
  PRIMARY KEY (`resourceId`,`date`,`type`,`item`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='物理计算资源/虚拟计算资源 表';




DROP TABLE IF EXISTS `alert_device_sync_log`;
CREATE TABLE `alert_device_sync_log` (
  `date` varchar(22) NOT NULL COMMENT '数据时间',
  `exec_time` datetime DEFAULT NULL COMMENT '执行时间',
  `status` varchar(12) NOT NULL COMMENT '状态，success，error',
  `content` varchar(4000) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `type` smallint(32) DEFAULT NULL COMMENT '1周2月',
  `item` varchar(12) DEFAULT NULL COMMENT 'cpu/memory',
  KEY `idx_c_to` (`status`,`date`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



ALTER TABLE device_alert_sync ADD suyanUuid varchar(50) NULL;

CREATE INDEX device_alert_sync_date_IDX USING BTREE ON device_alert_sync (`date`);

ALTER TABLE alert_month_department_sync add bizSystem_id varchar(50) NULL;
ALTER TABLE alert_month_bizsystem_sync add bizSystem_id varchar(50) NULL;
ALTER TABLE alert_month_bizsystem_sync add department1_id varchar(50) NULL;
ALTER TABLE alert_month_bizsystem_sync add department2_id varchar(50) NULL;

update alert_model_field set ci_field_code='bizSystem' where model_id='abf0444b-e44f-46ef-83fa-59b05ddf6336' and field_code='bizSystem';
update alert_model_field set ci_field_code='bizSystem_bizSystem_name' where model_id='abf0444b-e44f-46ef-83fa-59b05ddf6336' and field_code='bizSystem_name';
update alert_model_field set ci_field_code='department1_orgName_name' where model_id='abf0444b-e44f-46ef-83fa-59b05ddf6336' and field_code='department1_name';
update alert_model_field set ci_field_code='department1' where model_id='abf0444b-e44f-46ef-83fa-59b05ddf6336' and field_code='department1';
update alert_model_field set ci_field_code='department2' where model_id='abf0444b-e44f-46ef-83fa-59b05ddf6336' and field_code='department2';
update alert_model_field set ci_field_code='department2_orgName_name' where model_id='abf0444b-e44f-46ef-83fa-59b05ddf6336' and field_code='department2_name';
update alert_model_field set ci_field_code='device_mfrs_value_name' where ci_field_code='device_mfrs';
