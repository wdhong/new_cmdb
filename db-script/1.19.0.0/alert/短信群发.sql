-- ----------------------------
-- Table structure for sms_record
-- ----------------------------
DROP TABLE IF EXISTS `sms_record`;
CREATE TABLE `sms_record` (
  `id` varchar(60) NOT NULL DEFAULT '' COMMENT '主键',
  `content` text COMMENT '短信内容',
  `receiver_mobile` varchar(60) DEFAULT NULL COMMENT '接收人电话',
  `receiver_name` varchar(60) DEFAULT NULL COMMENT '接收人姓名',
  `receiver_uuid` varchar(60) DEFAULT NULL COMMENT '接收人id',
  `sender_uuid` varchar(60) DEFAULT NULL COMMENT '发送人账号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `status` int(4) DEFAULT NULL COMMENT '发送状态 1成功 0失败',
  `is_delete` int(4) DEFAULT NULL COMMENT '是否已删除 1是 0否',
  `error_log` varchar(255) DEFAULT NULL COMMENT '异常日志',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='短信记录表';

-- ----------------------------
-- Table structure for sms_template
-- ----------------------------
DROP TABLE IF EXISTS `sms_template`;
CREATE TABLE `sms_template` (
  `id` varchar(60) NOT NULL COMMENT '主键',
  `name` varchar(255) DEFAULT NULL COMMENT '模板名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='短信模板';

-- ----------------------------
-- Table structure for sms_template_detail
-- ----------------------------
DROP TABLE IF EXISTS `sms_template_detail`;
CREATE TABLE `sms_template_detail` (
  `id` varchar(60) NOT NULL DEFAULT '' COMMENT '主键id',
  `content` varchar(60) DEFAULT NULL COMMENT '短信记录id',
  `sms_template_id` varchar(60) DEFAULT NULL COMMENT '短信模板id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='短信模板详情';
