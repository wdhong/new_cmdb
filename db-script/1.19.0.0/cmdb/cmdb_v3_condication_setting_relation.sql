ALTER TABLE `cmdb_v3_condication_setting_relation`
  ADD COLUMN `show_input` INT (1) DEFAULT 0 NULL COMMENT '手工输入 0 否 1 是' AFTER `is_require`,
  ADD COLUMN `show_option` INT (1) DEFAULT 0 NULL COMMENT '显示选项 0 否 1 是' AFTER `show_input`;

