CREATE TABLE `department_user` (
	`dept_id` CHAR(36) NOT NULL COMMENT '部门ID' COLLATE 'utf8_bin',
	`user_id` CHAR(36) NOT NULL COMMENT '用户ID' COLLATE 'utf8_bin'
)
COLLATE='utf8_bin'
ENGINE=InnoDB
;
-- 初始化部门用户关系
insert into department_user (dept_id, user_id) select dept_id , uuid user_id from user where dept_id is not null;