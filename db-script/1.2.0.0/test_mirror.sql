--ALTER TABLE `alert_alerts` ADD COLUMN `order_time`  datetime NULL DEFAULT NULL COMMENT '工单请求时间' AFTER `order_type`;
--ALTER TABLE `alert_alerts_his` ADD COLUMN `order_time`  datetime NULL DEFAULT NULL COMMENT '工单请求时间' AFTER `order_type`;
ALTER TABLE `alert_alerts`
MODIFY COLUMN `order_id`  bigint(20) NULL DEFAULT NULL AFTER `device_ip`;
ALTER TABLE `alert_alerts_his`
MODIFY COLUMN `order_id`  bigint(20) NULL DEFAULT NULL AFTER `device_ip`;

ALTER TABLE `alert_alerts`
ADD INDEX `index_group_order_id` (`device_ip`, `item_id`(20), `alert_level`, order_id) USING BTREE ;
ALTER TABLE `alert_alerts`
ADD INDEX `index_group_order_status` (`device_ip`, `item_id`(20), `alert_level`, order_status) USING BTREE ;

DROP TABLE IF EXISTS `alert_proxy_idc`;
CREATE TABLE `alert_proxy_idc` (
  `id` bigint(11) NOT NULL,
  `proxy_name` varchar(64) DEFAULT NULL,
  `idc` varchar(64) DEFAULT NULL,
  `remark` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alert_proxy_idc
-- ----------------------------
INSERT INTO `alert_proxy_idc` VALUES ('1', 'xxg_proxy245', 'XXGZYC', '信息港资源池');
INSERT INTO `alert_proxy_idc` VALUES ('2', 'xxg_proxy76', 'XXGZYC', '信息港资源池');
INSERT INTO `alert_proxy_idc` VALUES ('3', 'xxg_proxy77', 'XXGZYC', '信息港资源池');
INSERT INTO `alert_proxy_idc` VALUES ('4', 'xxg_proxy78', 'XXGZYC', '信息港资源池');
INSERT INTO `alert_proxy_idc` VALUES ('5', 'xxg_proxy79', 'XXGZYC', '信息港资源池');
INSERT INTO `alert_proxy_idc` VALUES ('6', 'xxg_proxy80', 'XXGZYC', '信息港资源池');
INSERT INTO `alert_proxy_idc` VALUES ('7', 'ccjk5_proxy', 'YZYFCH', '业支域非池化');
INSERT INTO `alert_proxy_idc` VALUES ('8', 'hrb_proxy80', 'HEBZYC', '哈尔滨资源池');
INSERT INTO `alert_proxy_idc` VALUES ('9', 'hrb_proxy81', 'HEBZYC', '哈尔滨资源池');
INSERT INTO `alert_proxy_idc` VALUES ('10', 'Hohhot_ZAproxy1', 'HHHTZYC', '呼和浩特资源池');
INSERT INTO `alert_proxy_idc` VALUES ('11', 'xxg', 'XXGZYC', '信息港资源池');
INSERT INTO `alert_proxy_idc` VALUES ('12', 'fch', 'YZYFCH', '业支域非池化');
INSERT INTO `alert_proxy_idc` VALUES ('13', 'hac', 'HEBZYC', '哈尔滨资源池');
INSERT INTO `alert_proxy_idc` VALUES ('14', 'huc', 'HHHTZYC', '呼和浩特资源池');