/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-05-31 15:28:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_work_config
-- ----------------------------
DROP TABLE IF EXISTS `alert_work_config`;
CREATE TABLE `alert_work_config` (
  `uuid` varchar(255) NOT NULL COMMENT 'uuid',
  `day_start_time` varchar(255) DEFAULT NULL COMMENT '白班 开始时间',
  `day_end_time` varchar(255) DEFAULT NULL COMMENT '白班 结束时间内',
  `night_start_time` varchar(255) DEFAULT NULL COMMENT '夜班 开始时间',
  `night_end_time` varchar(255) DEFAULT NULL COMMENT '夜班 结束时间',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='值班人员配置表';

-- ----------------------------
-- Records of alert_work_config
-- ----------------------------
INSERT INTO `alert_work_config` VALUES ('05757c87-9a7b-4a37-8e85-8131824895d1', '09:00', '18:00', '18:00', '09:00');
