/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : cmdb

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-05-31 12:02:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_biz_system
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_biz_system`;
CREATE TABLE `cmdb_biz_system` (
  `id` varchar(50) NOT NULL,
  `bizName` varchar(100) NOT NULL COMMENT '业务名称',
  `orgId` varchar(50) DEFAULT NULL COMMENT '所属组织ID',
  `parent_id` varchar(50) DEFAULT NULL COMMENT '父级ID',
  `isdisable` varchar(20) DEFAULT NULL COMMENT '是否禁用 0：可用，1：禁用',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `isdel` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='业务系统管理表';
