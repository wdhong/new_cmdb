/*
SQLyog Ultimate v11.24 (32 bit)
MySQL - 5.7.26 : Database - cmdb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cmdb` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `cmdb`;

/*Table structure for table `cmdb_maintenance_software` */

DROP TABLE IF EXISTS `cmdb_maintenance_software`;

CREATE TABLE `cmdb_maintenance_software` (
  `id` int(32) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `project` varchar(64) DEFAULT NULL COMMENT '项目',
  `classify` varchar(64) DEFAULT NULL COMMENT '分类',
  `software_name` varchar(255) DEFAULT NULL COMMENT '软件名称',
  `unit` varchar(20) DEFAULT NULL COMMENT '单位',
  `number` varchar(20) DEFAULT NULL COMMENT '数量',
  `company` varchar(64) DEFAULT NULL COMMENT '厂商',
  `user_name` varchar(32) DEFAULT NULL COMMENT '联系人姓名',
  `telephone` varchar(32) DEFAULT NULL COMMENT '电话',
  `mainten_begin_date` date DEFAULT NULL COMMENT '本期维保开始时间',
  `mainten_end_date` date DEFAULT NULL COMMENT '本期维保结束时间',
  `admin` varchar(20) DEFAULT NULL COMMENT '管理员',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `project` (`project`,`software_name`,`company`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
