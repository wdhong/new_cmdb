/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : cmdb

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-05-31 12:02:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_org_system
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_org_system`;
CREATE TABLE `cmdb_org_system` (
  `id` varchar(50) NOT NULL,
  `orgName` varchar(100) NOT NULL COMMENT '组织名称',
  `orgType` varchar(50) DEFAULT NULL COMMENT '组织类型',
  `parent_id` varchar(50) DEFAULT NULL COMMENT '父级ID',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `isdel` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='组织管理表';
