INSERT INTO `cmdb`.`t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`) VALUES ('11457', '部门', 'org_type', 'department', NULL, '组织类型', '2019-05-27 19:54:43', '2019-05-27 19:56:24', '0');
INSERT INTO `cmdb`.`t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`) VALUES ('11458', '科室', 'org_type', 'section_office', NULL, '科室', '2019-05-27 19:57:27', NULL, '0');
INSERT INTO `cmdb`.`t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`) VALUES ('11459', '小组', 'org_type', 'team', NULL, '小组', '2019-05-27 19:58:08', NULL, '0');

alter table `cmdb`.`cmdb_demand_manager` add column cycle_time varchar(40) NULL DEFAULT NULL COMMENT '需求满足周期';
alter table `cmdb`.`cmdb_demand_manager` drop system_description;