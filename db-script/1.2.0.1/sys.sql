INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('big_screen', 'f', '2019-04-09 08:10:52.000000', '大屏视图', 'monitor');
INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('one_view', 'f', '2019-04-09 08:10:52.000000', '统一视图', 'big_screen');
INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('network_view', 'f', '2019-04-09 08:10:52.000000', '网络视图', 'big_screen');
INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('host_view', 'f', '2019-04-09 08:10:52.000000', '主机视图', 'big_screen');
INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('storage_view', 'f', '2019-04-09 08:10:52.000000', '存储视图', 'big_screen');
INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('work_log', 'f', '2019-05-31 15:04:19.000000', '值班日志', 'home');
