/*
SQLyog Trial v10.51 
MySQL - 5.7.30-log 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `cmdb_bills_discount` (
	`res_id` varchar (300),
	`res_type` varchar (300),
	`id` varchar (120),
	`discount` double ,
	`insert_time` datetime ,
	`insert_person` varchar (120),
	`update_time` datetime ,
	`update_person` varchar (120),
	`is_delete` int (11)
); 
