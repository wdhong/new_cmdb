create table `cmdb_bills_month_bill` (
	`id` varchar (120),
	`department_id` varchar (120),
	`account_id` varchar (120),
	`need_pay` bigint (20),
	`real_pay` bigint (20),
	`charge_time` varchar (60),
	`description` varchar (1500),
	`is_delete` int (1)
);