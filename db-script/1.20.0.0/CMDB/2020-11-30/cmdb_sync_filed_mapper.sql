/*
SQLyog Ultimate
MySQL - 5.7.30-log : Database - cmdb_zy_online
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cmdb_zy_online` /*!40100 DEFAULT CHARACTER SET utf8 */;

/*Table structure for table `cmdb_sync_filed_mapper` */

DROP TABLE IF EXISTS `cmdb_sync_filed_mapper`;

CREATE TABLE `cmdb_sync_filed_mapper` (
  `id` varchar(40) NOT NULL DEFAULT '0' COMMENT 'ID',
  `other_filed_code` varchar(40) NOT NULL COMMENT '第三方映射字段编码',
  `ums_filed_code` varchar(100) NOT NULL COMMENT 'UMS映射字段编码',
  `filed_type` varchar(40) DEFAULT NULL COMMENT '字段类型（用来判断日期等需要转换字段）',
  `mapper_type` varchar(100) DEFAULT NULL COMMENT '映射归属类型(X86服务器、云主机等)',
  `source` varchar(40) NOT NULL COMMENT '来源 苏研/华为等等',
  PRIMARY KEY (`id`),
  UNIQUE KEY `CMDB_SFM_UNIQUE` (`other_filed_code`,`ums_filed_code`,`mapper_type`,`source`),
  KEY `CMDB_SFM_IDX01` (`other_filed_code`,`mapper_type`,`source`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cmdb_sync_filed_mapper` */

insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('1','cabinet','idc_cabinet',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('10','createdAt','insert_time','date','X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('11','createdBy','insert_person',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('12','updatedAt','update_time','date','X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('13','resourceId','suyan_uuid',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('14','status','device_status',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('15','macAddress','mac_address',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('16','manageIp','ip',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('17','manageIpv6','IP_V6',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('18','manageNetmask','ip_mask',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('19','impiIp','ipmi_ip',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('2','idcType','idcType',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('20','impiIpv6','ipmi_ipv6',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('21','impiNetmask','ipmi_mask',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('22','storageIp','storage_ip',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('23','storageIpv6','storage_ipv6',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('24','storageNetmask','storage_ip_mask',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('25','businessIp','ServiceIP',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('26','businessIpv6','ServiceIPV6',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('27','businessNetmask','ServiceIP_mask',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('28','publicIp','public_ip',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('29','publicIpv6','public_ipv6',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('3','pod','pod_name',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('30','publicNetmask','public_ip_mask',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('31','model','device_model',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('32','serialNo','device_sn',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('33','monitorstatus','is_ansible',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('34','manufacturer','mainten_factory',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('4','department1','department1',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('5','department2','department2',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('51','physicalServer','exsi_ip',NULL,'云主机','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('52','department1','department1',NULL,'云主机','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('53','department2','department2',NULL,'云主机','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('54','business_name','bizSystem',NULL,'云主机','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('55','name','HOST_NAME',NULL,'云主机','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('56','code','asset_number',NULL,'云主机','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('57','description','device_description',NULL,'云主机','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('58','createdAt','insert_time','date','云主机','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('59','createdBy','insert_person',NULL,'云主机','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('6','business_name','bizSystem',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('60','updatedAt','update_time','date','云主机','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('61','resourceId','suyan_uuid',NULL,'云主机','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('62','status','device_status',NULL,'云主机','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('64','cpuNum','cpu_number',NULL,'云主机','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('65','memorySize','memory_size',NULL,'云主机','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('66','publicIp','vm_ip',NULL,'云主机','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('67','privateIp','ServiceIP',NULL,'云主机','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('68','cpuCoreNum','cpu_core_number',NULL,'云主机','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('69','cpuFrequency','cpu_type',NULL,'云主机','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('7','name','HOST_NAME',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('8','code','asset_number',NULL,'X86服务器','苏研');
insert  into `cmdb_sync_filed_mapper`(`id`,`other_filed_code`,`ums_filed_code`,`filed_type`,`mapper_type`,`source`) values ('9','description','device_description',NULL,'X86服务器','苏研');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
