/*
SQLyog Ultimate
MySQL - 5.7.30-log : Database - cmdb_zy_online
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cmdb_zy_online` /*!40100 DEFAULT CHARACTER SET utf8 */;

/*Table structure for table `cmdb_sync_suyan_relation` */

DROP TABLE IF EXISTS `cmdb_sync_suyan_relation`;

CREATE TABLE `cmdb_sync_suyan_relation` (
  `suyan_pod_id` varchar(40) NOT NULL COMMENT '苏研数据POD_ID',
  `suyan_pod_name` varchar(40) DEFAULT NULL COMMENT '苏研pod名称',
  `suyan_idc_name` varchar(40) DEFAULT NULL COMMENT '苏研资源池名称',
  `suyan_project_name` varchar(40) DEFAULT NULL COMMENT '苏研工期名称',
  `ums_pod_id` varchar(40) DEFAULT NULL,
  `ums_idc_id` varchar(40) DEFAULT NULL,
  `ums_project_id` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cmdb_sync_suyan_relation` */

insert  into `cmdb_sync_suyan_relation`(`suyan_pod_id`,`suyan_pod_name`,`suyan_idc_name`,`suyan_project_name`,`ums_pod_id`,`ums_idc_id`,`ums_project_id`) values ('7b8f0f5e2fbb4d9aa2d5fd55466d638e','POD-0','呼和浩特资源池','一期一','15','6d40d847-90a7-11e9-bb30-0242ac110002','f6b62273-1cb1-11ea-a3b4-0242ac110002');
insert  into `cmdb_sync_suyan_relation`(`suyan_pod_id`,`suyan_pod_name`,`suyan_idc_name`,`suyan_project_name`,`ums_pod_id`,`ums_idc_id`,`ums_project_id`) values ('9fecd66ecd5311e8a0aa48d5398dd228','POD-8','呼和浩特资源池','二期一','8','6d40d847-90a7-11e9-bb30-0242ac110002','f6b62316-1cb1-11ea-a3b4-0242ac110002');
insert  into `cmdb_sync_suyan_relation`(`suyan_pod_id`,`suyan_pod_name`,`suyan_idc_name`,`suyan_project_name`,`ums_pod_id`,`ums_idc_id`,`ums_project_id`) values ('9fecd66ecd5311e8a0aa48d5398dd229','POD-M','呼和浩特资源池','三期一','18','6d40d847-90a7-11e9-bb30-0242ac110002','f6b623e9-1cb1-11ea-a3b4-0242ac110002');
insert  into `cmdb_sync_suyan_relation`(`suyan_pod_id`,`suyan_pod_name`,`suyan_idc_name`,`suyan_project_name`,`ums_pod_id`,`ums_idc_id`,`ums_project_id`) values ('8ef14944cd5311e8a0aa48d5398dd228','POD-6','呼和浩特资源池','一期二','6','6d40d847-90a7-11e9-bb30-0242ac110002','f6b622c7-1cb1-11ea-a3b4-0242ac110002');
insert  into `cmdb_sync_suyan_relation`(`suyan_pod_id`,`suyan_pod_name`,`suyan_idc_name`,`suyan_project_name`,`ums_pod_id`,`ums_idc_id`,`ums_project_id`) values ('9a4340e3cd5311e8a0aa48d5398dd228','POD-7','呼和浩特资源池','二期一','7','6d40d847-90a7-11e9-bb30-0242ac110002','f6b62316-1cb1-11ea-a3b4-0242ac110002');
insert  into `cmdb_sync_suyan_relation`(`suyan_pod_id`,`suyan_pod_name`,`suyan_idc_name`,`suyan_project_name`,`ums_pod_id`,`ums_idc_id`,`ums_project_id`) values ('8a1546b7cd5311e8a0aa48d5398dd228','POD-5','呼和浩特资源池','一期二','5','6d40d847-90a7-11e9-bb30-0242ac110002','f6b622c7-1cb1-11ea-a3b4-0242ac110002');
insert  into `cmdb_sync_suyan_relation`(`suyan_pod_id`,`suyan_pod_name`,`suyan_idc_name`,`suyan_project_name`,`ums_pod_id`,`ums_idc_id`,`ums_project_id`) values ('7ac44963cd5311e8a0aa48d5398dd228','POD-3','呼和浩特资源池','二期一','3','6d40d847-90a7-11e9-bb30-0242ac110002','f6b62316-1cb1-11ea-a3b4-0242ac110002');
insert  into `cmdb_sync_suyan_relation`(`suyan_pod_id`,`suyan_pod_name`,`suyan_idc_name`,`suyan_project_name`,`ums_pod_id`,`ums_idc_id`,`ums_project_id`) values ('75627fd1cd5311e8a0aa48d5398dd228','POD-2','呼和浩特资源池','一期二 ','2','6d40d847-90a7-11e9-bb30-0242ac110002','f6b622c7-1cb1-11ea-a3b4-0242ac110002');
insert  into `cmdb_sync_suyan_relation`(`suyan_pod_id`,`suyan_pod_name`,`suyan_idc_name`,`suyan_project_name`,`ums_pod_id`,`ums_idc_id`,`ums_project_id`) values ('81b597accd5311e8a0aa48d5398dd228','POD-4','呼和浩特资源池','一期一','4','6d40d847-90a7-11e9-bb30-0242ac110002','f6b62273-1cb1-11ea-a3b4-0242ac110002');
insert  into `cmdb_sync_suyan_relation`(`suyan_pod_id`,`suyan_pod_name`,`suyan_idc_name`,`suyan_project_name`,`ums_pod_id`,`ums_idc_id`,`ums_project_id`) values ('b7a9650860de11e98e09bc305bf0c618','POD-10','呼和浩特资源池','二期一','10','6d40d847-90a7-11e9-bb30-0242ac110002','f6b62316-1cb1-11ea-a3b4-0242ac110002');
insert  into `cmdb_sync_suyan_relation`(`suyan_pod_id`,`suyan_pod_name`,`suyan_idc_name`,`suyan_project_name`,`ums_pod_id`,`ums_idc_id`,`ums_project_id`) values ('6f54eb0ecd5311e8a0aa48d5398dd228','POD-1','呼和浩特资源池','二期一','1','6d40d847-90a7-11e9-bb30-0242ac110002','f6b62316-1cb1-11ea-a3b4-0242ac110002');
insert  into `cmdb_sync_suyan_relation`(`suyan_pod_id`,`suyan_pod_name`,`suyan_idc_name`,`suyan_project_name`,`ums_pod_id`,`ums_idc_id`,`ums_project_id`) values ('b7a9650860de11e98e09bc305bf00709','POD-8','哈尔滨资源池','二期二','8','6d40d7d3-90a7-11e9-bb30-0242ac110002','f6b62357-1cb1-11ea-a3b4-0242ac110002');
insert  into `cmdb_sync_suyan_relation`(`suyan_pod_id`,`suyan_pod_name`,`suyan_idc_name`,`suyan_project_name`,`ums_pod_id`,`ums_idc_id`,`ums_project_id`) values ('49f888d2e9ae11e88f78c88d8393d154','POD-M','哈尔滨资源池','三期一','18','6d40d7d3-90a7-11e9-bb30-0242ac110002','f6b623e9-1cb1-11ea-a3b4-0242ac110002');
insert  into `cmdb_sync_suyan_relation`(`suyan_pod_id`,`suyan_pod_name`,`suyan_idc_name`,`suyan_project_name`,`ums_pod_id`,`ums_idc_id`,`ums_project_id`) values ('602e9e19b7f911e89f970242ac1e020c','POD-6','哈尔滨资源池','二期二','6','6d40d7d3-90a7-11e9-bb30-0242ac110002','f6b62357-1cb1-11ea-a3b4-0242ac110002');
insert  into `cmdb_sync_suyan_relation`(`suyan_pod_id`,`suyan_pod_name`,`suyan_idc_name`,`suyan_project_name`,`ums_pod_id`,`ums_idc_id`,`ums_project_id`) values ('8920d0c1bcaf11e89ba80242ac1e1d08','POD-5','哈尔滨资源池','二期一','5','6d40d7d3-90a7-11e9-bb30-0242ac110002','f6b62316-1cb1-11ea-a3b4-0242ac110002');
insert  into `cmdb_sync_suyan_relation`(`suyan_pod_id`,`suyan_pod_name`,`suyan_idc_name`,`suyan_project_name`,`ums_pod_id`,`ums_idc_id`,`ums_project_id`) values ('58c6ee42bcaf11e89ba80242ac1e1d08','POD-4','哈尔滨资源池','二期一','4','6d40d7d3-90a7-11e9-bb30-0242ac110002','f6b62316-1cb1-11ea-a3b4-0242ac110002');
insert  into `cmdb_sync_suyan_relation`(`suyan_pod_id`,`suyan_pod_name`,`suyan_idc_name`,`suyan_project_name`,`ums_pod_id`,`ums_idc_id`,`ums_project_id`) values ('55ca22a5b7f911e89f970242ac1e020c','POD-3','哈尔滨资源池','二期一','3','6d40d7d3-90a7-11e9-bb30-0242ac110002','f6b62316-1cb1-11ea-a3b4-0242ac110002');
insert  into `cmdb_sync_suyan_relation`(`suyan_pod_id`,`suyan_pod_name`,`suyan_idc_name`,`suyan_project_name`,`ums_pod_id`,`ums_idc_id`,`ums_project_id`) values ('4432c8ffb7f911e89f970242ac1e020c','POD-2','哈尔滨资源池','二期一','2','6d40d7d3-90a7-11e9-bb30-0242ac110002','f6b62316-1cb1-11ea-a3b4-0242ac110002');
insert  into `cmdb_sync_suyan_relation`(`suyan_pod_id`,`suyan_pod_name`,`suyan_idc_name`,`suyan_project_name`,`ums_pod_id`,`ums_idc_id`,`ums_project_id`) values ('637258dabcaa11e89ba80242ac1e1d08','POD-1','哈尔滨资源池','二期一','1','6d40d7d3-90a7-11e9-bb30-0242ac110002','f6b62316-1cb1-11ea-a3b4-0242ac110002');
insert  into `cmdb_sync_suyan_relation`(`suyan_pod_id`,`suyan_pod_name`,`suyan_idc_name`,`suyan_project_name`,`ums_pod_id`,`ums_idc_id`,`ums_project_id`) values ('7b8f0f5e2fbb4d9aa2d5fd55466d638f','POD-0','哈尔滨资源池','一期一','15','6d40d7d3-90a7-11e9-bb30-0242ac110002','f6b62273-1cb1-11ea-a3b4-0242ac110002');
insert  into `cmdb_sync_suyan_relation`(`suyan_pod_id`,`suyan_pod_name`,`suyan_idc_name`,`suyan_project_name`,`ums_pod_id`,`ums_idc_id`,`ums_project_id`) values ('088abc1387a245f78b15e7f85769d063','POD-14','哈尔滨资源池','三期一','14','6d40d7d3-90a7-11e9-bb30-0242ac110002','f6b623e9-1cb1-11ea-a3b4-0242ac110002');
insert  into `cmdb_sync_suyan_relation`(`suyan_pod_id`,`suyan_pod_name`,`suyan_idc_name`,`suyan_project_name`,`ums_pod_id`,`ums_idc_id`,`ums_project_id`) values ('403de82180ef4a919891a0c9249e1e5f','POD-13','哈尔滨资源池','三期一','13','6d40d7d3-90a7-11e9-bb30-0242ac110002','f6b623e9-1cb1-11ea-a3b4-0242ac110002');
insert  into `cmdb_sync_suyan_relation`(`suyan_pod_id`,`suyan_pod_name`,`suyan_idc_name`,`suyan_project_name`,`ums_pod_id`,`ums_idc_id`,`ums_project_id`) values ('54c4688e94c44602b9fcdfd17438b0c8','POD-12','哈尔滨资源池','三期一','12','6d40d7d3-90a7-11e9-bb30-0242ac110002','f6b623e9-1cb1-11ea-a3b4-0242ac110002');
insert  into `cmdb_sync_suyan_relation`(`suyan_pod_id`,`suyan_pod_name`,`suyan_idc_name`,`suyan_project_name`,`ums_pod_id`,`ums_idc_id`,`ums_project_id`) values ('b7a9650860de11e98e09bc305bfd0710','POD-9','哈尔滨资源池','三期二','9','6d40d7d3-90a7-11e9-bb30-0242ac110002','f6b62428-1cb1-11ea-a3b4-0242ac110002');
insert  into `cmdb_sync_suyan_relation`(`suyan_pod_id`,`suyan_pod_name`,`suyan_idc_name`,`suyan_project_name`,`ums_pod_id`,`ums_idc_id`,`ums_project_id`) values ('b7a9650860de11e98e09bc305bfd0717','POD-11','哈尔滨资源池','二期一','11','6d40d7d3-90a7-11e9-bb30-0242ac110002','f6b62316-1cb1-11ea-a3b4-0242ac110002');
insert  into `cmdb_sync_suyan_relation`(`suyan_pod_id`,`suyan_pod_name`,`suyan_idc_name`,`suyan_project_name`,`ums_pod_id`,`ums_idc_id`,`ums_project_id`) values ('b7a9650860de11e98e09bc305bfd0716','POD-10','哈尔滨资源池','二期二','10','6d40d7d3-90a7-11e9-bb30-0242ac110002','f6b62357-1cb1-11ea-a3b4-0242ac110002');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
