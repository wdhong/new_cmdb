/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50730
Source Host           : 10.12.70.40:3306
Source Database       : cmdb_zy_online

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2020-11-17 14:36:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for month_report
-- ----------------------------
DROP TABLE IF EXISTS `month_report`;
CREATE TABLE `month_report` (
  `id` varchar(40) NOT NULL COMMENT '资产ID',
  `is_delete` int(11) DEFAULT NULL COMMENT '删除标识',
  `module_id` varchar(40) DEFAULT NULL COMMENT '模型ID',
  `insert_person` varchar(40) DEFAULT NULL COMMENT '录入人',
  `insert_time` varchar(40) DEFAULT NULL COMMENT '新增时间',
  `update_person` varchar(40) DEFAULT NULL COMMENT '修改人',
  `update_time` varchar(40) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `IDX_month_report_id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='运营月报资产表';

-- ----------------------------
-- Table structure for month_report_dept_info
-- ----------------------------
DROP TABLE IF EXISTS `month_report_dept_info`;
CREATE TABLE `month_report_dept_info` (
  `id` varchar(40) NOT NULL COMMENT '资产ID',
  `is_delete` int(11) DEFAULT NULL COMMENT '删除标识',
  `bizSystem` varchar(40) DEFAULT NULL COMMENT '业务系统名称',
  `name` varchar(40) DEFAULT NULL COMMENT '联系人',
  `phone` varchar(40) DEFAULT NULL COMMENT '联系电话',
  `department2` varchar(40) DEFAULT NULL COMMENT '归属部门（二级）',
  `department1` varchar(40) DEFAULT NULL COMMENT '所属租户（一级）',
  `idcType` varchar(40) DEFAULT NULL COMMENT '资源池名称',
  `pod_name` varchar(40) DEFAULT NULL COMMENT 'POD名称',
  `phy_dist_count` varchar(40) DEFAULT NULL COMMENT '截至本月底裸金属已分配总量（台）',
  `phy_use_count` varchar(40) DEFAULT NULL COMMENT '截至本月底裸金属已使用总量（台）',
  `phy_use_rate` varchar(40) DEFAULT NULL COMMENT '裸金属资源使用率（%）',
  `gpu_dist_count` varchar(40) DEFAULT NULL COMMENT '截至本月度GPU已分配总量（台）',
  `gpu_use_count` varchar(40) DEFAULT NULL COMMENT '截至本月度GPU已使用总量（台）',
  `gpu_use_rate` varchar(40) DEFAULT NULL COMMENT 'GPU资源使用率（%）',
  `vm_dist_count` varchar(40) DEFAULT NULL COMMENT '已分配的云主机总量（台）',
  `vm_dist_vcpu_core_number` varchar(40) DEFAULT NULL COMMENT '云主机已分配核数VCPU总量（个）',
  `vm_dist_memory_count` varchar(40) DEFAULT NULL COMMENT '云主机已分配内存总量（G）',
  `vm_use_count` varchar(40) DEFAULT NULL COMMENT '已使用云主机数量（台）',
  `vm_use_vcpu_core_number` varchar(40) DEFAULT NULL COMMENT '云主机已使用核数VCPU总量（个）',
  `vm_use_memory_size` varchar(40) DEFAULT NULL COMMENT '已使用云主机总内存（G）',
  `vm_use_rate` varchar(40) DEFAULT NULL COMMENT '云主机使用率（%）',
  `fcsan_dist_count` varchar(40) DEFAULT NULL COMMENT 'FCSAN已分配总量（T）',
  `ipsan_use_count` varchar(40) DEFAULT NULL COMMENT 'FCSAN已使用总量（T）',
  `fcsan_use_rate` varchar(40) DEFAULT NULL COMMENT 'FCSAN使用率（%）',
  `ipsan_dist_count` varchar(40) DEFAULT NULL COMMENT 'IPSAN已分配总量（T）',
  `fcsan_use_count` varchar(40) DEFAULT NULL COMMENT 'FCSAN已使用总量（T）',
  `ipsan_use_rate` varchar(40) DEFAULT NULL COMMENT 'IPSAN使用率（%）',
  `block_dist_count` varchar(40) DEFAULT NULL COMMENT '块存储已分配总量（T）',
  `block_use_count` varchar(40) DEFAULT NULL COMMENT '块存储已使用总量（T）',
  `block_use_rate` varchar(40) DEFAULT NULL COMMENT '块存储的使用率（%）',
  `file_dist_count` varchar(40) DEFAULT NULL COMMENT '文件存储已分配总量（T）',
  `file_use_count` varchar(40) DEFAULT NULL COMMENT '文件存储已使用总量（T）',
  `file_use_rate` varchar(40) DEFAULT NULL COMMENT '文件存储的使用率（%）',
  `obj_dist_count` varchar(40) DEFAULT NULL COMMENT '对象存储已分配总量（T）',
  `obj_use_count` varchar(40) DEFAULT NULL COMMENT '对象存储已使用总量（T）',
  `obj_use_rate` varchar(40) DEFAULT NULL COMMENT '对象存储的使用率（%）',
  `backup_dist_count` varchar(40) DEFAULT NULL COMMENT '备份存储已分配总量（T）',
  `backup_use_count` varchar(40) DEFAULT NULL COMMENT '备份存储已使用总量（T）',
  `backup_use_rate` varchar(40) DEFAULT NULL COMMENT '备份存储使用率（%）',
  `month` varchar(40) DEFAULT NULL COMMENT '统计月份',
  `module_id` varchar(40) DEFAULT NULL COMMENT '模型ID',
  `insert_person` varchar(40) DEFAULT NULL COMMENT '录入人',
  `insert_time` varchar(40) DEFAULT NULL COMMENT '新增时间',
  `update_person` varchar(40) DEFAULT NULL COMMENT '修改人',
  `update_time` varchar(40) DEFAULT NULL COMMENT '修改时间',
  `month_report_type` varchar(40) DEFAULT NULL COMMENT '运营月份分类',
  `dist_unuse_phy_count` varchar(40) DEFAULT NULL COMMENT '分配满1个月但未使用的裸金属数量（台）',
  `dist_unuse_gpu_count` varchar(40) DEFAULT NULL COMMENT '分配满1个月但未使用的GPU数量（台）',
  `dist_unuse_vm_count` varchar(40) DEFAULT NULL COMMENT '分配满1个月但未使用的云主机数量（台）',
  PRIMARY KEY (`id`),
  KEY `IDX_month_report_dept_info_id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='租户资源基本信息表资产表';

-- ----------------------------
-- Table structure for month_report_dept_utl
-- ----------------------------
DROP TABLE IF EXISTS `month_report_dept_utl`;
CREATE TABLE `month_report_dept_utl` (
  `id` varchar(40) NOT NULL COMMENT '资产ID',
  `is_delete` int(11) DEFAULT NULL COMMENT '删除标识',
  `month_report_type` varchar(40) DEFAULT NULL COMMENT '运营月份分类',
  `month` varchar(40) DEFAULT NULL COMMENT '统计月份',
  `department_type` varchar(40) DEFAULT NULL COMMENT '租户范围',
  `cpu_max` varchar(40) DEFAULT NULL COMMENT '月度CPU资源峰值利用率',
  `memory_max` varchar(40) DEFAULT NULL COMMENT '月度内存峰值利用率',
  `cpu_avg` varchar(40) DEFAULT NULL COMMENT '月度CPU资源均值利用率',
  `memory_avg` varchar(40) DEFAULT NULL COMMENT '月度内存均值利用率',
  PRIMARY KEY (`id`),
  KEY `IDX_month_report_dept_utl_id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='IT公司租户资源利用率资产表';

-- ----------------------------
-- Table structure for month_report_performance
-- ----------------------------
DROP TABLE IF EXISTS `month_report_performance`;
CREATE TABLE `month_report_performance` (
  `id` varchar(40) NOT NULL COMMENT '资产ID',
  `is_delete` int(11) DEFAULT NULL COMMENT '删除标识',
  `module_id` varchar(40) DEFAULT NULL COMMENT '模型ID',
  PRIMARY KEY (`id`),
  KEY `IDX_month_report_performance_id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资源性能资产表';

-- ----------------------------
-- Table structure for month_report_pfm_index
-- ----------------------------
DROP TABLE IF EXISTS `month_report_pfm_index`;
CREATE TABLE `month_report_pfm_index` (
  `id` varchar(40) NOT NULL COMMENT '资产ID',
  `is_delete` int(11) DEFAULT NULL COMMENT '删除标识',
  `index_name` varchar(40) DEFAULT NULL COMMENT '指标名称',
  `index_value` varchar(40) DEFAULT NULL COMMENT '指标值',
  `index_comp_range` varchar(400) DEFAULT NULL COMMENT '指标计算范围',
  `remark` varchar(40) DEFAULT NULL COMMENT '备注',
  `month_report_type` varchar(40) DEFAULT NULL COMMENT '运营月份分类',
  `month` varchar(40) DEFAULT NULL COMMENT '统计月份',
  PRIMARY KEY (`id`),
  KEY `IDX_month_report_pfm_index_id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资源性能_指标情况资产表';

-- ----------------------------
-- Table structure for month_report_pfm_utl
-- ----------------------------
DROP TABLE IF EXISTS `month_report_pfm_utl`;
CREATE TABLE `month_report_pfm_utl` (
  `id` varchar(40) NOT NULL COMMENT '资产ID',
  `is_delete` int(11) DEFAULT NULL COMMENT '删除标识',
  `month_report_type` varchar(40) DEFAULT NULL COMMENT '运营月份分类',
  `month` varchar(40) DEFAULT NULL COMMENT '统计月份',
  `idcType` varchar(40) DEFAULT NULL COMMENT '资源池名称',
  `memory_avg` varchar(40) DEFAULT NULL COMMENT '月度内存均值利用率',
  `cpu_avg` varchar(40) DEFAULT NULL COMMENT '月度CPU资源均值利用率',
  `cpu_max` varchar(40) DEFAULT NULL COMMENT '月度CPU资源峰值利用率',
  `memory_max` varchar(40) DEFAULT NULL COMMENT '月度内存峰值利用率',
  `cpu_eighty_more_ratio` varchar(40) DEFAULT NULL COMMENT 'CPU峰值>=80%占比',
  `cpu_eighty_ratio` varchar(40) DEFAULT NULL COMMENT '80%>CPU峰值>=40%占比',
  `cpu_fourty_ratio` varchar(40) DEFAULT NULL COMMENT '40%>CPU峰值>=15%占比',
  `cpu_fifteen_ratio` varchar(40) DEFAULT NULL COMMENT '15%>CPU峰值占比',
  `memory_eighty_more_ratio` varchar(40) DEFAULT NULL COMMENT '内存峰值>=80%占比',
  `memory_eighty_ratio` varchar(40) DEFAULT NULL COMMENT '80%>内存峰值>=40%占比',
  `memory_fourty_ratio` varchar(40) DEFAULT NULL COMMENT '40%>内存峰值>=15%占比',
  `memory_fifteen_ratio` varchar(40) DEFAULT NULL COMMENT '15%>内存峰值占比',
  `report_table_type` varchar(40) DEFAULT NULL COMMENT '报表类型',
  PRIMARY KEY (`id`),
  KEY `IDX_month_report_pfm_online_phy_id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资源性能_已上线物理服务器资源利用率资产表';

-- ----------------------------
-- Table structure for month_report_resource
-- ----------------------------
DROP TABLE IF EXISTS `month_report_resource`;
CREATE TABLE `month_report_resource` (
  `id` varchar(40) NOT NULL COMMENT '资产ID',
  `is_delete` int(11) DEFAULT NULL COMMENT '删除标识',
  PRIMARY KEY (`id`),
  KEY `IDX_month_report_resource_id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资源规模资产表';

-- ----------------------------
-- Table structure for month_report_resource_comp
-- ----------------------------
DROP TABLE IF EXISTS `month_report_resource_comp`;
CREATE TABLE `month_report_resource_comp` (
  `id` varchar(40) NOT NULL COMMENT '资产ID',
  `is_delete` int(11) DEFAULT NULL COMMENT '删除标识',
  `month_report_type` varchar(40) DEFAULT NULL COMMENT '运营月份分类',
  `month` varchar(40) DEFAULT NULL COMMENT '统计月份',
  `idcType` varchar(40) DEFAULT NULL COMMENT '资源池名称',
  `m_phy_comp_count` varchar(40) DEFAULT NULL COMMENT '已交维物理计算资源（台）-不含管理节点',
  `use_comp_count` varchar(40) DEFAULT NULL COMMENT '截至当月已使用设备总量（台）',
  `m_dist_count` varchar(40) DEFAULT NULL COMMENT '截至当月已分配设备总量（台）',
  `surplus_count` varchar(40) DEFAULT NULL COMMENT '已建剩余设备总量（台）',
  `distribute_rate` varchar(40) DEFAULT NULL COMMENT '分配率',
  `use_rate` varchar(40) DEFAULT NULL COMMENT '资源使用率（%）',
  PRIMARY KEY (`id`),
  KEY `IDX_month_report_resource_comp_id` (`month_report_type`,`month`,`idcType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资源概况_裸金属资源资产表';

-- ----------------------------
-- Table structure for month_report_resource_gpu
-- ----------------------------
DROP TABLE IF EXISTS `month_report_resource_gpu`;
CREATE TABLE `month_report_resource_gpu` (
  `id` varchar(40) NOT NULL COMMENT '资产ID',
  `is_delete` int(11) DEFAULT NULL COMMENT '删除标识',
  `month_report_type` varchar(40) DEFAULT NULL COMMENT '运营月份分类',
  `month` varchar(40) DEFAULT NULL COMMENT '统计月份',
  `idcType` varchar(40) DEFAULT NULL COMMENT '资源池名称',
  `m_phy_gup_comp_count` varchar(40) DEFAULT NULL COMMENT '已交维GUP计算资源（台）-不含管理节点',
  `m_dist_count` varchar(40) DEFAULT NULL COMMENT '截至当月已分配设备总量（台）',
  `use_comp_count` varchar(40) DEFAULT NULL COMMENT '截至当月已使用设备总量（台）',
  `surplus_count` varchar(40) DEFAULT NULL COMMENT '已建剩余设备总量（台）',
  `distribute_rate` varchar(40) DEFAULT NULL COMMENT '分配率',
  `use_rate` varchar(40) DEFAULT NULL COMMENT '资源使用率（%）',
  PRIMARY KEY (`id`),
  KEY `IDX_month_report_resource_cpu_id` (`month_report_type`,`month`,`idcType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资源概况_GPU资产表';

-- ----------------------------
-- Table structure for month_report_resource_network
-- ----------------------------
DROP TABLE IF EXISTS `month_report_resource_network`;
CREATE TABLE `month_report_resource_network` (
  `id` varchar(40) NOT NULL COMMENT '资产ID',
  `is_delete` int(11) DEFAULT NULL COMMENT '删除标识',
  `module_id` varchar(40) DEFAULT NULL COMMENT '模型ID',
  `insert_person` varchar(40) DEFAULT NULL COMMENT '录入人',
  `insert_time` varchar(40) DEFAULT NULL COMMENT '新增时间',
  `update_person` varchar(40) DEFAULT NULL COMMENT '修改人',
  `update_time` varchar(40) DEFAULT NULL COMMENT '修改时间',
  `month_report_type` varchar(40) DEFAULT NULL COMMENT '运营月份分类',
  `month` varchar(40) DEFAULT NULL COMMENT '统计月份',
  `idcType` varchar(40) DEFAULT NULL COMMENT '资源池名称',
  `unm_network_count` varchar(40) DEFAULT NULL COMMENT '在建设备总量（台）',
  `m_network_count` varchar(40) DEFAULT NULL COMMENT '已建设设备总量（台）',
  PRIMARY KEY (`id`),
  KEY `IDX_month_report_resource_network_id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资源概况_网络资源资产表';

-- ----------------------------
-- Table structure for month_report_resource_overview
-- ----------------------------
DROP TABLE IF EXISTS `month_report_resource_overview`;
CREATE TABLE `month_report_resource_overview` (
  `id` varchar(40) NOT NULL COMMENT '资产ID',
  `is_delete` int(11) DEFAULT NULL COMMENT '删除标识',
  `month_report_type` varchar(40) DEFAULT NULL COMMENT '运营月份分类',
  `month` varchar(40) DEFAULT NULL COMMENT '统计月份',
  `idcType` varchar(40) DEFAULT NULL COMMENT '资源池名称',
  `m_phy_all_count` varchar(40) DEFAULT NULL COMMENT '已交维所有物理服务器数量（台）',
  `m_phy_comp_count` varchar(40) DEFAULT NULL COMMENT '已交维物理计算资源（台）-不含管理节点',
  `m_phy_gup_comp_count` varchar(40) DEFAULT NULL COMMENT '已交维GUP计算资源（台）-不含管理节点',
  `m_phy_host_comp_count` varchar(40) DEFAULT NULL COMMENT '已交维宿主机数（台）-不含管理节点',
  `m_phy_manage_count` varchar(40) DEFAULT NULL COMMENT '已交维管理节点（台）-中非计算和存储资源的节点',
  `m_phy_storage_count` varchar(40) DEFAULT NULL COMMENT '已交维存储服务器数量（台）-不含管理节点',
  `unm_phy_all_count` varchar(40) DEFAULT NULL COMMENT '未交维所有物理服务器数量（台）',
  `unm_phy_gup_comp_count` varchar(40) DEFAULT NULL COMMENT '未交维GPU计算资源（台）-不含管理节点',
  `unm_phy_comp_count` varchar(40) DEFAULT NULL COMMENT '未交维物理计算资源（台）-不含管理节点',
  `unm_phy_host_comp_count` varchar(40) DEFAULT NULL COMMENT '未交维宿主机数（台）-不含管理节点',
  `unm_phy_manage_count` varchar(40) DEFAULT NULL COMMENT '未交维管理节点（台）-资源中非计算和存储资源的节点',
  `unm_phy_storage_count` varchar(40) DEFAULT NULL COMMENT '未交维存储服务器（台）-不含管理节点',
  PRIMARY KEY (`id`),
  KEY `IDX_month_report_resource_overview_id` (`month_report_type`,`month`,`idcType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资源概况资产表';

-- ----------------------------
-- Table structure for month_report_resource_storage
-- ----------------------------
DROP TABLE IF EXISTS `month_report_resource_storage`;
CREATE TABLE `month_report_resource_storage` (
  `id` varchar(40) NOT NULL COMMENT '资产ID',
  `is_delete` int(11) DEFAULT NULL COMMENT '删除标识',
  `month_report_type` varchar(40) DEFAULT NULL COMMENT '运营月份分类',
  `month` varchar(40) DEFAULT NULL COMMENT '统计月份',
  `idcType` varchar(40) DEFAULT NULL COMMENT '资源池名称',
  `storage_type` varchar(40) DEFAULT NULL COMMENT '存储资源类型',
  `m_storage_count` varchar(40) DEFAULT NULL COMMENT '已交维存储资源总量（T）',
  `dist_storage_count` varchar(40) DEFAULT NULL COMMENT '已分配存储资源总量（T）',
  `use_storage_count` varchar(40) DEFAULT NULL COMMENT '已使用存储资源总量（T）',
  `unm_storage_count` varchar(40) DEFAULT NULL COMMENT '在建存储资源总量（T）（未交维部分）',
  `unm_predist_storage_count` varchar(40) DEFAULT NULL COMMENT '在建已预分配总量（T）（未交维部分）',
  `m_surplus_storage_count` varchar(40) DEFAULT NULL COMMENT '已交维剩余存储资源总量（T）',
  `distribute_rate` varchar(40) DEFAULT NULL COMMENT '分配率',
  `use_rate` varchar(40) DEFAULT NULL COMMENT '资源使用率（%）',
  PRIMARY KEY (`id`),
  KEY `IDX_month_report_resource_storage_id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资源概况_存储资源资产表';

-- ----------------------------
-- Table structure for month_report_resource_vm
-- ----------------------------
DROP TABLE IF EXISTS `month_report_resource_vm`;
CREATE TABLE `month_report_resource_vm` (
  `id` varchar(40) NOT NULL COMMENT '资产ID',
  `is_delete` int(11) DEFAULT NULL COMMENT '删除标识',
  `month_report_type` varchar(40) DEFAULT NULL COMMENT '运营月份分类',
  `month` varchar(40) DEFAULT NULL COMMENT '统计月份',
  `idcType` varchar(40) DEFAULT NULL COMMENT '资源池名称',
  `vm_host_count` varchar(40) DEFAULT NULL COMMENT '宿主机总量（台）',
  `vm_host_cpu_core_number` varchar(40) DEFAULT NULL COMMENT '宿主机CPU核数（个）',
  `vm_host_memory_size` varchar(40) DEFAULT NULL COMMENT '宿主机总内存（G）',
  `vm_manage_count` varchar(40) DEFAULT NULL COMMENT '云主机管理节点数量（台）',
  `vm_manage_vcpu_count` varchar(40) DEFAULT NULL COMMENT '云主机管理节点VCPU数量（个）',
  `vm_manage_memory_size` varchar(40) DEFAULT NULL COMMENT '云主机管理节点内存数量（G）',
  `vm_dist_count` varchar(40) DEFAULT NULL COMMENT '已分配的云主机总量（台）',
  `vm_dist_vcpu_core_count` varchar(40) DEFAULT NULL COMMENT '已分配云主机VCPU核数（个）',
  `vm_dist_memory_size` varchar(40) DEFAULT NULL COMMENT '已分配云主机总内存（G）',
  `vm_use_count` varchar(40) DEFAULT NULL COMMENT '已使用云主机数量（台）',
  `vm_use_vcpu_core_count` varchar(40) DEFAULT NULL COMMENT '已使用云主机VCPU核数（个）',
  `vm_use_memory_size` varchar(40) DEFAULT NULL COMMENT '已使用云主机总内存（G）',
  `vm_host_w_dist_memory_size` varchar(40) DEFAULT NULL COMMENT '宿主机待分配内存（G）',
  `vm_use_rate` varchar(40) DEFAULT NULL COMMENT '云主机使用率（%）',
  `vm_rate` varchar(40) DEFAULT NULL COMMENT '虚拟化比（%）(云主机数量/物理机数量)',
  PRIMARY KEY (`id`),
  KEY `IDX_month_report_resource_vm_id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资源概况_云主机资产表';
