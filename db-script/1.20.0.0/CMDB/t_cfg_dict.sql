/*
SQLyog Ultimate
MySQL - 5.7.30-log 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

insert into `t_cfg_dict` (`id`, `is_delete`, `dict_code`, `dict_note`, `col_name`, `up_dict`, `description`, `sort_index`, `delete_flag`, `create_date`, `update_date`) values('06c1684f60b14ade8fc07dbf6d1afbdf','0','租户运营月报','租户运营月报','month_report_type',NULL,'运营月报类型','1',NULL,NULL,NULL);
insert into `t_cfg_dict` (`id`, `is_delete`, `dict_code`, `dict_note`, `col_name`, `up_dict`, `description`, `sort_index`, `delete_flag`, `create_date`, `update_date`) values('ad3359ac205046fd982cb7d710c7b544','0','一级资源池运营月报','一级资源池运营月报','month_report_type',NULL,'运营月报类型','2',NULL,NULL,NULL);

insert into `t_cfg_dict` (`id`, `is_delete`, `dict_code`, `dict_note`, `col_name`, `up_dict`, `description`, `sort_index`, `delete_flag`, `create_date`, `update_date`) values('5b43e50afa814e5ca350c30147b05591','0','资源性能_已上线物理服务器资源利用率','资源性能_已上线物理服务器资源利用率','report_table_type',NULL,NULL,NULL,NULL,NULL,NULL);
insert into `t_cfg_dict` (`id`, `is_delete`, `dict_code`, `dict_note`, `col_name`, `up_dict`, `description`, `sort_index`, `delete_flag`, `create_date`, `update_date`) values('bd6e9dcb208b408099f53ccfbe88ca13','0','资源性能_已分配的物理服务器资源利用率','资源性能_已分配的物理服务器资源利用率','report_table_type',NULL,NULL,NULL,NULL,NULL,NULL);
