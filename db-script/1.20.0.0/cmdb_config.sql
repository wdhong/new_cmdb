/*
SQLyog Ultimate
MySQL - 5.7.30-log 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

insert into `cmdb_config` (`id`, `config_code`, `config_value`, `config_value_type`, `config_remark`) values('117','business_list_info','{\"module_id\":\"9212e88a698d43cbbf9ec35b83773e2d\",\"condicationCode\":\"business_list\"}','json','业务系统管理模型id');
insert into `cmdb_config` (`id`, `config_code`, `config_value`, `config_value_type`, `config_remark`) values('77','org_list_info','{\"module_id\":\"c45c3fee780943b0bf233e03913ca5b4\"}','json','部门租户管理信息');
insert into `cmdb_config` (`id`, `config_code`, `config_value`, `config_value_type`, `config_remark`) values('88','scan_biz_urge_bpm','{\"urgeDays\": [15, 5],\"rwmc\": \"业务系统上线提醒\", \"rwclnr\":\"业务系统请于[day]日内提交业务系统工单上线流程，如延期发起工单上线流程或未发起工单上线流程会影响运营考核计分。若已经提交了业务系统上线流程请忽略。\" }','json','业务催办工单信息');

insert into `cmdb_config` (`id`, `config_code`, `config_value`, `config_value_type`, `config_remark`) values('99','department_type_map','{\"专业公司及直属单位\": \"ET\",\r\n\"集团各部门\": \"GT\",\r\n\"IT公司内部\":  \"IT\",\r\n\"其他\":  \"OT\"}','json','部门类型对应');
