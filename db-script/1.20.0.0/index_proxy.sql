/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 5.7.29-log : Database - index_proxy2
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`index_proxy2` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `index_proxy2`;

/*Table structure for table `cache_map` */

DROP TABLE IF EXISTS `cache_map`;

CREATE TABLE `cache_map` (
  `cache_key` varchar(260) NOT NULL COMMENT '缓存键',
  `cache_value` text NOT NULL COMMENT '缓存值',
  PRIMARY KEY (`cache_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `data_sync_mark` */

DROP TABLE IF EXISTS `data_sync_mark`;

CREATE TABLE `data_sync_mark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_identity` varchar(30) NOT NULL,
  `sync_seq` int(11) DEFAULT '0',
  `last_sync_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Table structure for table `monitor_events` */
INSERT INTO `data_sync_mark` (`item_identity`, `sync_seq`, `last_sync_time`) VALUES ('monitor_actions', 0, '2019-07-12 18:11:23');
INSERT INTO `data_sync_mark` (`item_identity`, `sync_seq`, `last_sync_time`) VALUES ('monitor_items', 13815, '2021-01-07 11:41:57');
INSERT INTO `data_sync_mark` (`item_identity`, `sync_seq`, `last_sync_time`) VALUES ('monitor_triggers', 13812, '2021-01-07 13:59:00');
INSERT INTO `data_sync_mark` (`item_identity`, `sync_seq`, `last_sync_time`) VALUES ('monitor_template', 13814, '2021-01-07 11:42:05');
INSERT INTO `data_sync_mark` (`item_identity`, `sync_seq`, `last_sync_time`) VALUES ('monitor_trigger_model', 13835, '2021-01-07 11:45:22');

DROP TABLE IF EXISTS `monitor_events`;

CREATE TABLE `monitor_events` (
  `event_id` varchar(50) NOT NULL COMMENT '序列号',
  `source_type` varchar(20) NOT NULL COMMENT '事件来源：\r\nTRIGGERS-触发器\r\nDISCOVERY-新发现\r\nREGISTRATION-自动注册（agent/proxy）',
  `source_id` varchar(50) NOT NULL COMMENT 'source_type对应的id',
  `source` varchar(4000) DEFAULT NULL COMMENT '保留字段',
  `object_type` varchar(20) NOT NULL COMMENT '1-设备\r\n2-业务系统',
  `object_id` varchar(50) NOT NULL COMMENT 'object_type对应的object的ID',
  `object` text COMMENT 'object_id相关的扩展存储属性\r\n相关业务使用',
  `value` varchar(20) NOT NULL COMMENT '事件类型\r\n1-异常\r\n0-正常',
  `acknowledged` varchar(20) NOT NULL COMMENT '确认标识：\r\n0-未确认\r\n1-已确认',
  `clock` int(10) NOT NULL COMMENT '事件产生时间',
  `ns` int(10) NOT NULL COMMENT '纳秒\r\n小于秒的部分',
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `sync_monitor_actions` */

DROP TABLE IF EXISTS `sync_monitor_actions`;

CREATE TABLE `sync_monitor_actions` (
  `action_id` varchar(50) NOT NULL COMMENT '事件ID',
  `name` varchar(500) NOT NULL COMMENT '事件名',
  `event_source` varchar(20) NOT NULL COMMENT '事件来源\r\n0-指来源为触发器trigger\r\n1-指来源为自动发现descover\r\n2-指来源为自动登记auto_register\r\n3-为网络发现产生的事件源\r\n',
  `eval_type` varchar(20) DEFAULT NULL COMMENT '表示执行action的前提条件的逻辑关系\r\n0表示and/or\r\n1表示and\r\n2表示or\r\n',
  `status` varchar(20) NOT NULL COMMENT '状态\r\nON-启动\r\nOFF-禁用',
  `type` varchar(20) NOT NULL COMMENT '类型\r\n1-回调url\r\n2-函数',
  `dealer` varchar(500) NOT NULL COMMENT '处理程序\r\n如果type为1，则为url\r\n如果type为2，则为类名.方法名\r\n入参包含事件、指标、触发器信息，需定义',
  `trigger_id` varchar(50) NOT NULL COMMENT '触发器ID',
  `event_type` int(11) NOT NULL COMMENT '事件类型：\r\n1-异常事件\r\n2-正常事件\r\n3-通用事件',
  PRIMARY KEY (`action_id`),
  KEY `trigger_id` (`trigger_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `sync_monitor_items` */

DROP TABLE IF EXISTS `sync_monitor_items`;

CREATE TABLE `sync_monitor_items` (
  `item_id` varchar(50) NOT NULL COMMENT '监控项ID',
  `name` varchar(100) DEFAULT NULL COMMENT '监控项名称',
  `type` varchar(20) DEFAULT NULL COMMENT '监控项类型\r\nSCRIPT-脚本（现有）\r\nINDEX-监控指标（现有）',
  `template_id` varchar(50) DEFAULT NULL COMMENT '模版ID',
  `key_` varchar(255) DEFAULT NULL COMMENT '监控键值',
  `delay` varchar(50) DEFAULT NULL COMMENT '监控周期，单位分钟',
  `history` int(8) DEFAULT NULL COMMENT '保留时间，单位天',
  `status` varchar(20) DEFAULT NULL COMMENT '状态：\r\nON-启用\r\nOFF-禁用\r\nNONSUPPORT-不支持（启用状态的监控项采集不到数据）（暂时不用）',
  `value_type` varchar(20) DEFAULT NULL COMMENT '监控项数据格式：\r\nFLOAT-浮点数\r\nSTR-字符串\r\nLOG-日志\r\nUINT64-整数\r\nTEXT-文本',
  `units` varchar(20) DEFAULT NULL COMMENT '单位',
  `error` varchar(4000) DEFAULT NULL COMMENT '错误信息',
  `data_type` varchar(20) DEFAULT NULL COMMENT '监控项数据类型\r\nDECIMAL-十进制\r\nOCTAL-八进制\r\nHEXADECIMAL-十六进制\r\nBOOLEAN-布尔值\r\n',
  `sys_type` varchar(50) DEFAULT NULL COMMENT '接入监控系统类型（目前为zabbix）\r\nMIRROR\r\nZABBIX\r\nNONE',
  PRIMARY KEY (`item_id`),
  KEY `template_id` (`template_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `sync_monitor_object` */

DROP TABLE IF EXISTS `sync_monitor_object`;

CREATE TABLE `sync_monitor_object` (
  `object_type` varchar(50) NOT NULL COMMENT '关联对象类型\\\\r\\\\n1-设备ID\\\\r\\\\n2-业务系统\\\\ 3.自监控设备ID',
  `object_id` varchar(50) NOT NULL COMMENT '关联对象ID',
  `name` varchar(255) DEFAULT NULL COMMENT '关联对象名称',
  `sharding_base` int(20) NOT NULL COMMENT '分片字段',
  `description` varchar(500) DEFAULT NULL COMMENT '描述',
  `extend_obj` text COMMENT '扩展对象'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `sync_monitor_template` */

DROP TABLE IF EXISTS `sync_monitor_template`;

CREATE TABLE `sync_monitor_template` (
  `template_id` varchar(50) NOT NULL COMMENT '模版ID',
  `name` varchar(100) DEFAULT NULL COMMENT '模版名称',
  `description` varchar(4000) DEFAULT NULL COMMENT '描述',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `type` varchar(20) DEFAULT NULL COMMENT '模版类型：\\r\\n1-硬件\\r\\n2-网络\\r\\n3-主机操作系统\\r\\n4-应用\\r\\n',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `fun_type` varchar(20) DEFAULT NULL COMMENT '功能类型：\\r\\n1-监控\\r\\n2-巡检',
  `sys_type` varchar(20) DEFAULT NULL COMMENT '系统类型 ZABBIX PROMETHEUS THEME MIRROR SCRIPT ',
  `mon_type` varchar(20) DEFAULT NULL COMMENT '监控类型 1：系统2：业务3：自监控系统设备',
  PRIMARY KEY (`template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `sync_monitor_template_object` */

DROP TABLE IF EXISTS `sync_monitor_template_object`;

CREATE TABLE `sync_monitor_template_object` (
  `template_object_id` varchar(50) NOT NULL COMMENT '模版设备关系ID',
  `template_id` varchar(50) NOT NULL COMMENT '模版ID',
  `object_type` varchar(50) NOT NULL COMMENT '关联对象类型\\r\\n1-设备ID\\r\\n2-业务系统',
  `object_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`template_object_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `sync_monitor_triggers` */

DROP TABLE IF EXISTS `sync_monitor_triggers`;

CREATE TABLE `sync_monitor_triggers` (
  `trigger_id` varchar(50) NOT NULL COMMENT '触发器ID',
  `name` varchar(4000) DEFAULT NULL COMMENT '触发器名称',
  `expression` varchar(4000) DEFAULT NULL COMMENT '表达式',
  `url` varchar(2000) DEFAULT NULL COMMENT 'URL',
  `status` varchar(20) DEFAULT NULL COMMENT '状态：\\r\\nON-启用\\r\\nOFF-禁用',
  `value` varchar(20) DEFAULT NULL COMMENT '值类型\r\nOK\r\nPROBLEM\r\nUNKNOWN',
  `type` varchar(20) DEFAULT NULL COMMENT '触发器类型1：静态表达式2：动态阈值',
  `priority` varchar(20) DEFAULT NULL COMMENT '优先级\\r\\n0-Not classified\\r\\n1 -Information\\r\\n2-Warning\\r\\n3-Average\\r\\n4-High\\r\\n5-Disaster\\r\\n',
  `item_id` varchar(50) DEFAULT NULL COMMENT '监控项ID',
  `param` varchar(2000) DEFAULT NULL COMMENT '脚本类监控触发器的参数值',
  PRIMARY KEY (`trigger_id`),
  KEY `item_id` (`item_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `sync_monitor_triggers_dynamic_model` */

DROP TABLE IF EXISTS `sync_monitor_triggers_dynamic_model`;

CREATE TABLE `sync_monitor_triggers_dynamic_model` (
  `model_id` varchar(50) NOT NULL COMMENT '模型ID',
  `trigger_id` varchar(50) NOT NULL COMMENT '触发器ID',
  `device_item_id` varchar(50) NOT NULL COMMENT '设备监控ID',
  `device_id` varchar(50) NOT NULL COMMENT '设备id',
  `ip` varchar(50) DEFAULT NULL COMMENT 'ip',
  `idc_type` varchar(20) NOT NULL COMMENT '资源池',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  `model_json` text COMMENT '模型json',
  `thrid_system_id` varchar(50) NOT NULL COMMENT '第三方监控系统id',
  `model_status` varchar(2) NOT NULL COMMENT '模型状态 ON:开启 OFF:关闭',
  PRIMARY KEY (`model_id`,`thrid_system_id`),
  KEY `trigger_id` (`trigger_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
/*!50100 PARTITION BY KEY (thrid_system_id) */;

/*Table structure for table `sync_monitor_triggers_dynamic_model_ext` */

DROP TABLE IF EXISTS `sync_monitor_triggers_dynamic_model_ext`;

CREATE TABLE `sync_monitor_triggers_dynamic_model_ext` (
  `model_id` varchar(50) NOT NULL COMMENT '模型ID',
  `source_type` varchar(2) NOT NULL COMMENT '1:东华',
  `resource_id` varchar(50) NOT NULL COMMENT '模型任务唯一标识',
  `model_type` varchar(2) NOT NULL COMMENT '模型类型1：周期2：平稳',
  `model_status` varchar(2) NOT NULL COMMENT '模型状态：SUCCESS，FAIL',
  `model_fail_msg` varchar(4000) DEFAULT NULL COMMENT '模型异常原因',
  `zhixindu` int(4) DEFAULT NULL COMMENT '置信度',
  `desc` varchar(4000) DEFAULT NULL COMMENT '模型描述',
  `model_content` varchar(4000) DEFAULT NULL COMMENT '模型内容'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 导出  表 index_proxy.item_standard_alert_def 结构
DROP TABLE IF EXISTS `item_standard_alert_def`;
CREATE TABLE IF NOT EXISTS `item_standard_alert_def` (
  `item_key` varchar(32) NOT NULL COMMENT '监控项key',
  `alert_title` varchar(128) DEFAULT NULL COMMENT '告警标题',
  `alert_content_template` tinytext COMMENT '告警内容模板',
  UNIQUE KEY `uk_standard_alert_item_def` (`item_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  index_proxy.item_standard_alert_def 的数据：~28 rows (大约)
/*!40000 ALTER TABLE `item_standard_alert_def` DISABLE KEYS */;
INSERT INTO `item_standard_alert_def` (`item_key`, `alert_title`, `alert_content_template`) VALUES
	('BoardCurrentPower', '电源设备', '{HOST.IP} 电源设备告警'),
	('ClientBytesIn', '客户端流入速率超过阈值', '{HOST.IP} 客户端流入速率超过阈值告警'),
	('ClientBytesOut', '客户端流出速率超过阈值', '{HOST.IP} 客户端流出速率超过阈值告警'),
	('ClientCurConns', '统计时间内并发链接数最大值', '{HOST.IP} 客户端并发连接数超过阈值告警'),
	('cpuUsage', '交换机CPU利用率超阈值', '{HOST.IP} 交换机CPU利用率超过80%阈值告警'),
	('EntityFanState', '电源模块风扇异常或停止', '{HOST.IP} 风扇异常或停止'),
	('icmpping', 'Ping设备的活动性', '{HOST.IP} PING不可达'),
	('ifAdminStatus', '交换机端口状态由UP变为DOWN', '{HOST.IP} 交换机端口状态由UP变为DOWN'),
	('ifHCInBroadtPktSpeed', '统计时间内接收广播包速率', '{HOST.IP} 接口接收接收广播包速率大于60个/秒'),
	('ifHCInOctetsSpeed', '交换机接口接收速率超阈值', '{HOST.IP} 接口接收流量5分钟内突然变化50%告警'),
	('ifHCOutBroadPktSpeed', '统计时间内发送广播包速率', '{HOST.IP} 接口接收接收广播包速率大于300个/秒'),
	('ifHCOutOctetsSpeed', '交换机发送接收速率超阈值', '{HOST.IP} 接口发送流量5分钟内突然变化50%告警'),
	('ifInBandRate', '交换机接口入口带宽利用率超阈值', '{HOST.IP} 接口入口带宽利用率超70%阈值告警'),
	('ifInDiscards', '统计时间内接口丢弃接收包数', '{HOST.IP} 接口丢弃接收包数大于10个/秒'),
	('ifInErrors', '统计时间内接口接收包Errors错误数', '{HOST.IP} 接口接收包错误数大于10个/秒'),
	('ifInOctets.pused[{#SNMPVALUE}]', '进口流量使用率过高', '{HOST.IP}接口{SNMPVALUE}进口流量：{ITEM.VALUE}'),
	('ifOperStatus', '端口的物理状态由UP变为DOWN', '{HOST.IP} 端口状态由UP变为DOWN'),
	('ifOutBandRate', '交换机接口出口带宽利用率超阈值', '{HOST.IP} CMNET接口出口带宽利用率超70%阈值告警'),
	('ifOutDiscards', '统计时间内接口丢弃发送包数', '{HOST.IP} 接口丢弃发送包数大于10个/秒'),
	('ifOutErrors', '统计时间内接口发送包Errors错误数', '{HOST.IP} 接口发送包错误数大于10个/秒'),
	('ifOutOctets.Pused[{#SNMPVALUE}]', '接口出口带宽利用率超阈值告警', '{HOST.IP}接口{SNMPVALUE}出口流量：{ITEM.VALUE}'),
	('memUsage', '交换机内存利用率超阈值', '{HOST.IP} 交换机内存利用率超过90%阈值告警'),
	('neContrlableRate', '交换机60S内不可达比率', '{HOST.IP} 设备60S内不可达比率超过5%'),
	('nePingRespTime', 'ping设备的响应时间', '{HOST.IP} 设备60S内响应时间超过200ms'),
	('SecVSYSstatSessCount', '统计时间内并发链接数最大值', '{HOST.IP} 5分钟内并发链接数最大值'),
	('SecVSYSstatSessSpeed', '统计时间内新建连接数', '{HOST.IP} 60s时间内新建连接数'),
	('ServerBytesIn', '服务器端流入速率超过阈值', '{HOST.IP} 服务器端流入速率超过阈值告警'),
	('ServerBytesOut', '服务器端流出速率超过阈值', '{HOST.IP} 服务器端流出速率超过阈值告警'),
	('sysUpTime', '检查交换机运行系统运行时间', '{HOST.IP} 设备运行时间少于1800S');
/*!40000 ALTER TABLE `item_standard_alert_def` ENABLE KEYS */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
