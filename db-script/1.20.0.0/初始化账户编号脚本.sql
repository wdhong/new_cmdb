UPDATE `cmdb_bills_account_balance` cbab 
LEFT JOIN 
(
SELECT *,@i:=@i+1 rank,CONCAT('IT', (CASE WHEN dict_note IN ('总部','内部公司') THEN 'IT' ELSE 'ET' END ), '2020',(CASE WHEN IFNULL(parent_id, '') != '' AND parent_id != '0'  THEN '02' ELSE '01' END ),LPAD( @i,3,0)) `code`
 FROM
(
SELECT a.department_id, c.dict_note,b.parent_id
 FROM `cmdb_bills_account_balance` a
LEFT JOIN cmdb_v3.cmdb_org_system b ON a.department_id = b.id AND b.is_delete=0
LEFT JOIN cmdb_v3.t_cfg_dict c ON b.department_type   = c.id AND c.col_name='department_type'
WHERE IFNULL(c.dict_note, '') != ''
) res,
 (SELECT @i:=0) s 
) res1
ON cbab.department_id = res1.department_id
SET cbab.account_code = res1.code
,NUMBER = rank