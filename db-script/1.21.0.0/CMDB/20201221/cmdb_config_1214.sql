/*
SQLyog Ultimate
MySQL - 5.7.30-log 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

insert into `cmdb_config` (`id`, `config_code`, `config_value`, `config_value_type`, `config_remark`) values('de07db90-3ada-11eb-b82c-0242ac110004','sync_from_other_module','[\r\n	{\r\n		\"toModule\": {\r\n			\"id\": \"38de00ee103b4bafb82489b3a0dc3311\",\r\n			\"relationKey\": \"owner_biz_system\"\r\n		},\r\n		\"syncFields\": [\r\n			{\"fromKey\":\"department1\",\r\n			\"toKey\":\"department1\"},\r\n			{\"fromKey\":\"department2\",\r\n			\"toKey\":\"department2\"},\r\n			{\"fromKey\":\"business_concat\",\r\n			\"toKey\":\"business_concat\"}\r\n			{\"fromKey\":\"business_concat_phone\",\r\n			\"toKey\":\"business_concat_phone\"}\r\n			{\"fromKey\":\"business_concat_email\",\r\n			\"toKey\":\"business_concat_email\"}\r\n		],\r\n		\"fromModule\": {\r\n			\"id\": \"9212e88a698d43cbbf9ec35b83773e2d\",\r\n			\"relationKey\": \"id\"\r\n		}\r\n	}\r\n]','json','同步另一模型数据到当前模型配置');
