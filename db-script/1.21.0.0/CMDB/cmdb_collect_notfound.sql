ALTER TABLE `cmdb_collect_notfound`
  ADD COLUMN `vm_name` VARCHAR (40) NULL COMMENT '虚拟机名称' AFTER `device_type`,

  ADD COLUMN `exsi_ip` VARCHAR (40) NULL COMMENT '宿主机IP' AFTER `vm_name`,

  ADD COLUMN `other_ip` VARCHAR (400) NULL COMMENT '其他ip' AFTER `exsi_ip`
 
  ;

