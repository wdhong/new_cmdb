ALTER TABLE `bills`.`cmdb_bills_account_balance`   
  ADD PRIMARY KEY (`id`),
  ADD  INDEX `CBAB_IDX_01` (`department_id` , `is_delete`);


ALTER TABLE `bills`.`cmdb_bills_account_record`   
  ADD  INDEX `CBAR_IDX_01` (`account_id` , `deal_month` , `is_delete`);

ALTER TABLE `bills`.`cmdb_bills_month_record`   
  ADD  INDEX `CBMR_IDX_01` (`department_id` , `charge_time` , `is_delete`);
