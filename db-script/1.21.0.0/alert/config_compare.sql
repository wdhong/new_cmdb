/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50730
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2020-12-30 14:26:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for config_compare
-- ----------------------------
DROP TABLE IF EXISTS `config_compare`;
CREATE TABLE `config_compare` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `idc_type` varchar(64) NOT NULL COMMENT '资源池',
  `master_ip` varchar(64) NOT NULL COMMENT '主ip',
  `backup_ip` varchar(64) NOT NULL COMMENT '备ip',
  `brand` varchar(64) DEFAULT NULL COMMENT '厂商',
  `add_count` int(11) DEFAULT NULL,
  `modify_count` int(11) DEFAULT NULL,
  `del_count` int(11) DEFAULT NULL,
  `add_datas` text COMMENT '新增未同步',
  `modify_datas` text COMMENT '修改未同步',
  `del_datas` text COMMENT '删除未同步',
  `compare_time` datetime DEFAULT NULL COMMENT '最新比对时间',
  `master_config_file` varchar(255) DEFAULT NULL COMMENT '主配置原始文件',
  `backup_config_file` varchar(255) DEFAULT NULL COMMENT '备配置原始文件',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_user` varchar(64) NOT NULL COMMENT '创建人',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_uni_idc_ip` (`idc_type`,`master_ip`,`backup_ip`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COMMENT='主备配置比对清单表';

-- ----------------------------
-- Table structure for config_compare_logs
-- ----------------------------
DROP TABLE IF EXISTS `config_compare_logs`;
CREATE TABLE `config_compare_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `compare_id` int(11) NOT NULL,
  `idc_type` varchar(64) NOT NULL,
  `master_ip` varchar(64) NOT NULL,
  `backup_ip` varchar(64) NOT NULL,
  `master_config_file` varchar(255) DEFAULT NULL COMMENT '主配置原始文件',
  `backup_config_file` varchar(255) DEFAULT NULL COMMENT '备配置原始文件',
  `add_result` text COMMENT '新增未同步',
  `modify_result` text COMMENT '修改未同步',
  `del_result` text COMMENT '删除未同步',
  `compare_time` datetime DEFAULT NULL COMMENT '比对时间',
  `compare_user` varchar(255) DEFAULT NULL COMMENT '操作人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='主备配置比对日志表';
