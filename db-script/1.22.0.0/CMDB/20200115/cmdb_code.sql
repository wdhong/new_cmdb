/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 5.7.30-log 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

 
insert into `cmdb_code` (`code_id`, `filed_code`, `filed_name`, `module_catalog_id`, `control_type_id`, `code_length`, `code_tip`, `display_style`, `is_bind_source`, `is_validate`, `is_approve`, `is_collect`, `sort_index`, `is_delete`, `add_read_only`, `update_read_only`, `default_value`) values('89e4048d4a814593ba710c48a832ff99','is_charge','是否计费','54ba8b02ad3748229adb39f9e7fdff38','debb3cfd-7bbc-11e9-b0c3-0242ac110002','40','','4','是','否','否','否','10443','0','0','0','');
insert into `cmdb_code` (`code_id`, `filed_code`, `filed_name`, `module_catalog_id`, `control_type_id`, `code_length`, `code_tip`, `display_style`, `is_bind_source`, `is_validate`, `is_approve`, `is_collect`, `sort_index`, `is_delete`, `add_read_only`, `update_read_only`, `default_value`) values('b2b7e660733e4154b2f7f299e7892116','business_level','业务系统级别','54ba8b02ad3748229adb39f9e7fdff38','debb3cfd-7bbc-11e9-b0c3-0242ac110002','40','','4','是','否','否','否',NULL,'0','0','0','707562dc55ab4a85a3d8ace02dc4b682');
insert into `cmdb_code` (`code_id`, `filed_code`, `filed_name`, `module_catalog_id`, `control_type_id`, `code_length`, `code_tip`, `display_style`, `is_bind_source`, `is_validate`, `is_approve`, `is_collect`, `sort_index`, `is_delete`, `add_read_only`, `update_read_only`, `default_value`) values('cc06851719e747c687fcb66bbb6e71a9','assess_start_month','考核起始月份','2aa2924a75fd4255a01dc77b255f1b04','debb3949-7bbc-11e9-b0c3-0242ac110002','40','','4','否','否','否','否',NULL,'0','0','0','1');



insert into `cmdb_v3_code_table` (`id`, `code_id`, `col_title`, `col_key`, `sort_index`, `is_delete`) values('2d925b66eee1419f960ebf645f2b7124','89e4048d4a814593ba710c48a832ff99','','','1','0');
insert into `cmdb_v3_code_table` (`id`, `code_id`, `col_title`, `col_key`, `sort_index`, `is_delete`) values('54a77ae2a8c74d64ad8a091821e9f374','3bfc9958e0df490a8a404b14298f5807','','','1','0');
insert into `cmdb_v3_code_table` (`id`, `code_id`, `col_title`, `col_key`, `sort_index`, `is_delete`) values('ec4207cc85c74007ab2e10432b260c01','b2b7e660733e4154b2f7f299e7892116','','','1','0');
insert into `cmdb_v3_code_table` (`id`, `code_id`, `col_title`, `col_key`, `sort_index`, `is_delete`) values('4b1025f00a044f4c9734620a9ef9b340','be345232eac3421aa28a75d01cc88c11','','','1','0');
insert into `cmdb_v3_code_table` (`id`, `code_id`, `col_title`, `col_key`, `sort_index`, `is_delete`) values('35d1cce30cb0412b9c1767e00cec1cff','cc06851719e747c687fcb66bbb6e71a9','','','1','0');



insert into `cmdb_v3_module_code_setting` (`id`, `module_id`, `owner_module_id`, `group_id`, `code_id`, `display`, `sort_index`, `is_delete`) values('8c42dc20fbe649a8a8991b535e683770','38de00ee103b4bafb82489b3a0dc3311','38de00ee103b4bafb82489b3a0dc3311','216afa525e524b148298c14742b8c91c','89e4048d4a814593ba710c48a832ff99','0','17','0');
insert into `cmdb_v3_module_code_setting` (`id`, `module_id`, `owner_module_id`, `group_id`, `code_id`, `display`, `sort_index`, `is_delete`) values('b65594ea483d4fe0a47747df4262814c','38de00ee103b4bafb82489b3a0dc3311','38de00ee103b4bafb82489b3a0dc3311','24324de153154f0593cda492cf7b5487','3bfc9958e0df490a8a404b14298f5807','0','2','0');
insert into `cmdb_v3_module_code_setting` (`id`, `module_id`, `owner_module_id`, `group_id`, `code_id`, `display`, `sort_index`, `is_delete`) values('19e154fd93f5452e817303ef9a63efa3','38de00ee103b4bafb82489b3a0dc3311','38de00ee103b4bafb82489b3a0dc3311','24324de153154f0593cda492cf7b5487','b2b7e660733e4154b2f7f299e7892116','0','9','0');
insert into `cmdb_v3_module_code_setting` (`id`, `module_id`, `owner_module_id`, `group_id`, `code_id`, `display`, `sort_index`, `is_delete`) values('310555aee57b472a9ee3fb1a0cdb6426','38de00ee103b4bafb82489b3a0dc3311','38de00ee103b4bafb82489b3a0dc3311','24324de153154f0593cda492cf7b5487','be345232eac3421aa28a75d01cc88c11','0','4','0');
insert into `cmdb_v3_module_code_setting` (`id`, `module_id`, `owner_module_id`, `group_id`, `code_id`, `display`, `sort_index`, `is_delete`) values('ac10456067b2465188dd8e6e7f7683b1','9212e88a698d43cbbf9ec35b83773e2d','9212e88a698d43cbbf9ec35b83773e2d','e8459a37275946ed84c0cdfe0bce8c74','b2b7e660733e4154b2f7f299e7892116','0','21','0');
insert into `cmdb_v3_module_code_setting` (`id`, `module_id`, `owner_module_id`, `group_id`, `code_id`, `display`, `sort_index`, `is_delete`) values('eb915baf88d9400499e4eee3bf10d954','c45c3fee780943b0bf233e03913ca5b4','c45c3fee780943b0bf233e03913ca5b4','e7bb0b7d312842248395898a23a4f097','cc06851719e747c687fcb66bbb6e71a9','0','8','0');





insert into `cmdb_v3_code_cascade` (`id`, `code_id`, `sub_code_id`, `sql_string`, `sort_index`, `is_delete`) values('1249f6a84a44494b8b33c881ee9b366d','89e4048d4a814593ba710c48a832ff99','','','1','0');
insert into `cmdb_v3_code_cascade` (`id`, `code_id`, `sub_code_id`, `sql_string`, `sort_index`, `is_delete`) values('308cc12e38fb4cb4bec528b5503ec6ba','3bfc9958e0df490a8a404b14298f5807','be345232eac3421aa28a75d01cc88c11','select c.id `id`, c.orgName `key` ,c.orgName `value` from cmdb_org_system c where c.parent_id=\'?\' and is_delete=\'0\'',NULL,'0');
insert into `cmdb_v3_code_cascade` (`id`, `code_id`, `sub_code_id`, `sql_string`, `sort_index`, `is_delete`) values('4fb42b94028f44ecbf849deea004b773','be345232eac3421aa28a75d01cc88c11','','','1','0');

insert into `cmdb_v3_code_cascade` (`id`, `code_id`, `sub_code_id`, `sql_string`, `sort_index`, `is_delete`) values('e10c968708cf481cbc15e6eb85633bf1','cc06851719e747c687fcb66bbb6e71a9','','','1','0');



insert into `cmdb_v3_code_bind_source` (`id`, `code_id`, `bind_source_type`, `dict_source`, `table_source`, `table_sql`, `ref_module_id`, `show_module_code_id`, `ref_module_query`, `is_delete`) values('a2ea77fbde22449a87f9c0c5bfa75253','89e4048d4a814593ba710c48a832ff99','引用模型','',NULL,'','3b30d3f751924c2e89cd15c16e3d92e2','1e8f1176a2124ebca08fbfc167d555ed','[{\"filed\":\"col_name\",\"operator\":\"=\",\"value\":\"whether\"}]','0');
insert into `cmdb_v3_code_bind_source` (`id`, `code_id`, `bind_source_type`, `dict_source`, `table_source`, `table_sql`, `ref_module_id`, `show_module_code_id`, `ref_module_query`, `is_delete`) values('3120135564a6450992a8bb0b528d1984','3bfc9958e0df490a8a404b14298f5807','引用模型','',NULL,'','c45c3fee780943b0bf233e03913ca5b4','dd864289dfdc4e0287f6f7edcb0d44be','[{\"filed\":\"parent_id\", \"operator\":\"=\", \"value\":\"0\"}]','0');
insert into `cmdb_v3_code_bind_source` (`id`, `code_id`, `bind_source_type`, `dict_source`, `table_source`, `table_sql`, `ref_module_id`, `show_module_code_id`, `ref_module_query`, `is_delete`) values('00f94b64323340f5a1f5190bd1cc4cdc','b2b7e660733e4154b2f7f299e7892116','引用模型','',NULL,'','3b30d3f751924c2e89cd15c16e3d92e2','1e8f1176a2124ebca08fbfc167d555ed','[{\"filed\":\"col_name\",\"operator\":\"=\",\"value\":\"business_level\"}]','0');
insert into `cmdb_v3_code_bind_source` (`id`, `code_id`, `bind_source_type`, `dict_source`, `table_source`, `table_sql`, `ref_module_id`, `show_module_code_id`, `ref_module_query`, `is_delete`) values('0c74dacbac4241759b1718b55fe9c694','be345232eac3421aa28a75d01cc88c11','引用模型','',NULL,'','c45c3fee780943b0bf233e03913ca5b4','dd864289dfdc4e0287f6f7edcb0d44be','[{\"filed\":\"parent_id\", \"operator\":\"!=\", \"value\":\"0\"}]','0');



ALTER TABLE `cmdb_business_quota`
  ADD COLUMN `department1` VARCHAR (40) NULL COMMENT '一级部门',
  ADD COLUMN `department2` VARCHAR (40) NULL COMMENT '二级部门' ,
  ADD COLUMN `business_level` VARCHAR (40) NULL COMMENT '业务系统级别' ,
    ADD COLUMN `is_charge` VARCHAR (40) NULL COMMENT '是否计费' ;
  
  ALTER TABLE `cmdb_business_system`
  ADD COLUMN `business_level` VARCHAR (40) NULL COMMENT '业务系统级别'  ;

  ALTER TABLE `cmdb_org_system`
  ADD COLUMN `assess_start_month` VARCHAR (40) NULL COMMENT '考核起始月份'  ;
  
  
  ALTER TABLE `bills`.`cmdb_bills_discount`
CHANGE `res_id` `res_id` VARCHAR (100) CHARSET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '折扣资源ID',
ADD PRIMARY KEY (`res_id`);






