ALTER TABLE alert_mail_resolve_filter ADD name VARCHAR(64) NULL;

update alert_schedule_index set index_value='[{"deviceClass":"服务器","keyComment":"IPMI监控采集异常","source":"Prometheus","itemTpye":"IPMI"}]' 
where index_name ='列头柜下电监控项' ;

update alert_schedule_index set index_value='[{"alertCol":"告警名称","content":"疑似机柜下电告警","dsc":"","sort":1},{"alertCol":"告警内容","content":"{idc_type}-{source_room}-{idc_cabinet} 疑似设备下电超过 {alert_percentage} %告警","value":"{idc_type}-{source_room}-{idc_cabinet} 设备下电超过 {alert_percentage} 告警","dsc":"{idc_type} --机柜资源池名称 &{source_room} --机房位置 &{idc_cabinet}--机柜名称&{alert_percentage} --默认是50%，以最新配置为准","sort":2},{"alertCol":"告警等级","content":"重大","value":"5","dsc":"","sort":3},{"alertCol":"资源池","content":"机柜下的告警设备对应的资源池","dsc":"","sort":4},{"alertCol":"机房位置","content":"机柜下的告警设备对应的机房位置","dsc":"","sort":5},{"alertCol":"设备分类","content":"服务器","dsc":"默认告警的设备分类为服务器","sort":6},{"alertCol":"工程期数","content":"机柜下的告警设备对应的工程期数","dsc":"","sort":7}]' 
where index_name ='机柜告警生成信息' ;

UPDATE alert_schedule_index
SET index_value='[{"alertCol":"告警名称","content":"疑似列头柜下电告警","dsc":"","sort":1},{"alertCol":"告警内容","content":"{idc_type}-{source_room}-{idc_cabinet_column} 列头柜下电告警","value":"{idc_type}-{source_room}-{idc_cabinet_column} 疑似列头柜下电告警","dsc":"{idc_type} --机柜资源池名称 &{source_room} --机房位置 &{idc_cabinet_column}--获取机柜名称的英文编码，如F","sort":2},{"alertCol":"告警等级","content":"重大","value":"5","dsc":"","sort":3},{"alertCol":"资源池","content":"列头柜下的机柜告警对应的资源池","dsc":"","sort":4},{"alertCol":"机房位置","content":"列头柜下的机柜告警对应的机房位置","dsc":"","sort":5},{"alertCol":"设备分类","content":"服务器","dsc":"默认告警的设备分类为服务器","sort":6},{"alertCol":"工程期数","content":"列头柜下的机柜告警对应的工程期数","dsc":"","sort":7}]', index_type='cabinet_column_alert_info', remark=NULL
WHERE index_name='列头柜告警生成信息';