
/*备份组织数据 */
CREATE TABLE cmdb_org_system_bak1231 AS SELECT * FROM cmdb_org_system;
/*清空组织数据*/
TRUNCATE TABLE cmdb_org_system;
/*更新原一级部门数据*/
UPDATE  cmdb_org_system a
INNER JOIN (SELECT * FROM  `cmdb_org_system_bak1218` WHERE parent_id='0' AND is_delete=0) b ON a.orgname=b.orgname
SET a.id = b.id,a.orgType = b.orgType,a.is_external_org=b.is_external_org,
a.remark = b.remark, a.`assess_start_month`=b.assess_start_month,a.`department_type`=b.department_type,
a.`contact`=b.contact,a.`contact_email_address`=b.contact_email_address,a.`contact_number`=b.contact_number
WHERE a.parent_id='0';

/*更新原二级部门数据 */
 UPDATE   cmdb_org_system org
 INNER JOIN  
(
SELECT C.id s_id,d.* FROM
(SELECT a.id,b.orgname parent,b.id p_id,a.orgname FROM cmdb_org_system  a
 INNER JOIN (SELECT * FROM cmdb_org_system WHERE parent_id='0' AND IFNULL(orgType,'') !='') b
 ON a.parent_id = b.source_id) c
 INNER JOIN (SELECT * FROM cmdb_org_system_bak1218 WHERE parent_id !='0') d ON c.p_id = d.parent_id AND c.orgname = d.orgname
 )res ON org.id = res.s_id
 SET org.id = res.id,org.orgType = res.orgType,org.is_external_org=res.is_external_org,
org.remark = res.remark, org.`assess_start_month`=res.assess_start_month,org.`department_type`=res.department_type,
org.`contact`=res.contact,org.`contact_email_address`=res.contact_email_address,org.`contact_number`=res.contact_number
  