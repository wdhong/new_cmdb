/*
SQLyog Ultimate
MySQL - 5.7.30-log 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

insert into `cmdb_code` (`code_id`, `filed_code`, `filed_name`, `module_catalog_id`, `control_type_id`, `code_length`, `code_tip`, `display_style`, `is_bind_source`, `is_validate`, `is_approve`, `is_collect`, `sort_index`, `is_delete`, `add_read_only`, `update_read_only`, `default_value`) values('dfa1202849cb4d4d8173ce91a2651b91','data_disk_size','数据磁盘大小（GB）','96603733-5793-11ea-ab96-fa163e982f89','debb3949-7bbc-11e9-b0c3-0242ac110002','40','','4','否','否','否','否','10443','0','0','0','');
insert into `cmdb_code` (`code_id`, `filed_code`, `filed_name`, `module_catalog_id`, `control_type_id`, `code_length`, `code_tip`, `display_style`, `is_bind_source`, `is_validate`, `is_approve`, `is_collect`, `sort_index`, `is_delete`, `add_read_only`, `update_read_only`, `default_value`) values('3a73763942a6470d9fe8fd8123c28169','disk_number','磁盘个数','96603733-5793-11ea-ab96-fa163e982f89','debb3949-7bbc-11e9-b0c3-0242ac110002','40','','4','否','否','否','否','10444','0','0','0','');
insert into `cmdb_code` (`code_id`, `filed_code`, `filed_name`, `module_catalog_id`, `control_type_id`, `code_length`, `code_tip`, `display_style`, `is_bind_source`, `is_validate`, `is_approve`, `is_collect`, `sort_index`, `is_delete`, `add_read_only`, `update_read_only`, `default_value`) values('b454181e99e04fd3bca4f7cf6088f3b4','load_system','承载系统名称','96603733-5793-11ea-ab96-fa163e982f89','debb3949-7bbc-11e9-b0c3-0242ac110002','40','','4','否','否','否','否','10446','0','0','0','');

insert into `cmdb_v3_code_cascade` (`id`, `code_id`, `sub_code_id`, `sql_string`, `sort_index`, `is_delete`) values('8f0e305e853c4565a466c570e9b7604e','dfa1202849cb4d4d8173ce91a2651b91','','','1','0');
insert into `cmdb_v3_code_cascade` (`id`, `code_id`, `sub_code_id`, `sql_string`, `sort_index`, `is_delete`) values('32343f0c233d4f22ac0923deff2663aa','b454181e99e04fd3bca4f7cf6088f3b4','','','1','0');


insert into `cmdb_v3_module_code_setting` (`id`, `module_id`, `owner_module_id`, `group_id`, `code_id`, `display`, `sort_index`, `is_delete`) values('25b49306c04b45b3bb6762b2af4752a2','353f0967f68d40d18ecaecafae6b3f15','353f0967f68d40d18ecaecafae6b3f15','b73d28ea95c548b198e9b664990334d2','dfa1202849cb4d4d8173ce91a2651b91','0','6','1');
insert into `cmdb_v3_module_code_setting` (`id`, `module_id`, `owner_module_id`, `group_id`, `code_id`, `display`, `sort_index`, `is_delete`) values('54073b8f01724c0fb9c3526fc5d28ddd','353f0967f68d40d18ecaecafae6b3f15','353f0967f68d40d18ecaecafae6b3f15','fe306c6c26214f9aa097b480c7bbca54','dfa1202849cb4d4d8173ce91a2651b91','0','6','0');
insert into `cmdb_v3_module_code_setting` (`id`, `module_id`, `owner_module_id`, `group_id`, `code_id`, `display`, `sort_index`, `is_delete`) values('e6f5b3a389ac4267af4c620386d050b5','ce5362623c5a47869173a43ff6e982f8','ce5362623c5a47869173a43ff6e982f8','51ca8b0bd28e44b19f2b36ec2777470a','32a44773568011e998180242ac110001','0','1','0');
insert into `cmdb_v3_module_code_setting` (`id`, `module_id`, `owner_module_id`, `group_id`, `code_id`, `display`, `sort_index`, `is_delete`) values('d1209df2f00e4790ace6c5af0c33912b','ce5362623c5a47869173a43ff6e982f8','ce5362623c5a47869173a43ff6e982f8','51ca8b0bd28e44b19f2b36ec2777470a','b454181e99e04fd3bca4f7cf6088f3b4','0','6','0');
insert into `cmdb_v3_module_code_setting` (`id`, `module_id`, `owner_module_id`, `group_id`, `code_id`, `display`, `sort_index`, `is_delete`) values('ff43d630c32b4bef9f75a42944b5e3a6','ce5362623c5a47869173a43ff6e982f8','ce5362623c5a47869173a43ff6e982f8','51ca8b0bd28e44b19f2b36ec2777470a','dfa1202849cb4d4d8173ce91a2651b91','0','2','0');


insert into `cmdb_v3_code_table` (`id`, `code_id`, `col_title`, `col_key`, `sort_index`, `is_delete`) values('68f62279672247a1bf237dfb004c411c','dfa1202849cb4d4d8173ce91a2651b91','','','1','0');
insert into `cmdb_v3_code_table` (`id`, `code_id`, `col_title`, `col_key`, `sort_index`, `is_delete`) values('ffdb0f5c65bd40948b5d1220f1ffcc89','b454181e99e04fd3bca4f7cf6088f3b4','','','1','0');


ALTER TABLE `cmdb_instance_physical_machine`
  ADD COLUMN `data_disk_size` VARCHAR (40) NULL COMMENT '数据磁盘大小（GB）' AFTER `disk_bare_capacity`
  ADD COLUMN `disk_number` VARCHAR (40) NULL COMMENT '磁盘个数' AFTER `data_disk_size`;
  
  
  ALTER TABLE `cmdb_instance_vm_000001`
  ADD COLUMN `disk_size` VARCHAR (40) NULL COMMENT '磁盘大小' AFTER `vm_name`,
  ADD COLUMN `data_disk_size` VARCHAR (40) NULL COMMENT '数据磁盘大小（GB）' AFTER `disk_size`,
  ADD COLUMN `load_system` VARCHAR (40) NULL COMMENT '承载系统名称' AFTER `data_disk_size`;


