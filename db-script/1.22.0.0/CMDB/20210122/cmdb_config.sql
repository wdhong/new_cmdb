/*
SQLyog Ultimate
MySQL - 5.7.30-log 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

update `cmdb_config` set config_value = '[\r\n	{\r\n		\"toModule\": {\r\n			\"id\": \"38de00ee103b4bafb82489b3a0dc3311\",\r\n			\"relationKey\": \"owner_biz_system\"\r\n		},\r\n		\"syncFields\": [\r\n			{\"fromKey\":\"department1\",\r\n			\"toKey\":\"department1\"},\r\n			{\"fromKey\":\"department2\",\r\n			\"toKey\":\"department2\"},\r\n			{\"fromKey\":\"business_concat\",\r\n			\"toKey\":\"business_concat\"}\r\n			{\"fromKey\":\"business_concat_phone\",\r\n			\"toKey\":\"business_concat_phone\"}\r\n			{\"fromKey\":\"business_concat_email\",\r\n			\"toKey\":\"business_concat_email\"}\r\n		],\r\n		\"fromModule\": {\r\n			\"id\": \"9212e88a698d43cbbf9ec35b83773e2d\",\r\n			\"relationKey\": \"id\"\r\n		},\r\n		\"type\" : \"业务配额同步\"\r\n	},\r\n	{\r\n		\"toModule\": {\r\n			\"id\": \"96603733-5793-11ea-ab96-fa163e982f89\",\r\n			\"relationKey\": \"bizSystem\"\r\n		},\r\n		\"syncFields\": [\r\n			{\"fromKey\":\"business_level\",\r\n			\"toKey\":\"bizsystem_level\"}\r\n			\r\n		],\r\n		\"fromModule\": {\r\n			\"id\": \"9212e88a698d43cbbf9ec35b83773e2d\",\r\n			\"relationKey\": \"id\"\r\n		},\r\n		\"type\" : \"业务系统级别同步\"\r\n	}\r\n]' where config_code = 'sync_from_other_module';
