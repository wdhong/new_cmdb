/*
Navicat MySQL Data Transfer

Source Server         : 10.1.5.119
Source Server Version : 50718
Source Host           : 10.1.5.119:3306
Source Database       : test_mirror

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2019-05-05 10:17:10
*/

SET FOREIGN_KEY_CHECKS=0;

rename table alert_alerts to alert_alerts_0618;
rename table alert_alerts_his to alert_alerts_his_0618;

-- ----------------------------
-- Table structure for alert_alerts
-- ----------------------------
DROP TABLE IF EXISTS `alert_alerts`;
CREATE TABLE `alert_alerts` (
  `alert_id` varchar(64) NOT NULL,
  `r_alert_id` varchar(300) DEFAULT NULL,
  `event_id` varchar(80) DEFAULT NULL,
  `action_id` bigint(10) DEFAULT NULL,
  `device_id` varchar(50) DEFAULT NULL,
  `device_class` varchar(128) DEFAULT NULL COMMENT '设备类型',
  `biz_sys` varchar(128) DEFAULT NULL COMMENT '业务系统',
  `moni_index` varchar(1024) DEFAULT NULL COMMENT '监控指标/内容，关联触发器name',
  `moni_object` varchar(512) DEFAULT NULL COMMENT '监控对象',
  `cur_moni_value` varchar(512) DEFAULT NULL COMMENT '当前监控值',
  `cur_moni_time` datetime DEFAULT NULL COMMENT '当前监控时间',
  `alert_level` varchar(20) NOT NULL COMMENT '告警级别\r\n1-提示\r\n2-低\r\n3-中\r\n4-高\r\n5-严重\r\n',
  `item_id` varchar(50) DEFAULT NULL,
  `remark` varchar(512) DEFAULT NULL COMMENT '备注',
  `order_status` varchar(50) DEFAULT NULL COMMENT '1-未派单\r\n2-处理中\r\n3-已完成\r\n',
  `operate_status` tinyint(1) DEFAULT '0' COMMENT '警报操作状态:0-待确认,1-已确认,2-待解决,3-已处理',
  `source` varchar(100) DEFAULT NULL COMMENT '告警来源\r\nMIRROR\r\nZABBIX\r\n',
  `idc_type` varchar(128) DEFAULT NULL COMMENT '所属位置-资源池',
  `source_room` varchar(100) DEFAULT NULL COMMENT '机房/资源池',
  `object_type` varchar(50) DEFAULT NULL COMMENT '告警类型\r\n1-系统\r\n2-业务',
  `object_id` varchar(50) DEFAULT NULL COMMENT '对象ID，如果是设备告警则是设备ID，如果是业务则是业务系统code',
  `region` varchar(50) DEFAULT NULL COMMENT '域/资源池code',
  `device_ip` varchar(100) DEFAULT NULL,
  `order_id` varchar(100) DEFAULT NULL,
  `order_type` varchar(100) DEFAULT NULL,
  `alert_start_time` datetime DEFAULT NULL COMMENT '告警开始时间',
  `prefix` varchar(64) DEFAULT NULL COMMENT '告警前缀，用于多套告警系统时候区分标识',
  `alert_count` int default 1,
  PRIMARY KEY (`alert_id`),
  key `index_r_alert_id` (`r_alert_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '告警记录表';

-- ----------------------------
-- Table structure for alert_alerts_his
-- ----------------------------
DROP TABLE IF EXISTS `alert_alerts_his`;
CREATE TABLE `alert_alerts_his` (
  `alert_id` varchar(64) NOT NULL,
  `r_alert_id` varchar(300) DEFAULT NULL,
  `event_id` varchar(80) NULL,
  `action_id` bigint(10) DEFAULT NULL,
  `device_id` varchar(50) DEFAULT NULL,
  `device_class` varchar(128) DEFAULT NULL COMMENT '设备类型',
  `biz_sys` varchar(128) DEFAULT NULL COMMENT '业务系统',
  `moni_index` varchar(1024) NOT NULL COMMENT '监控指标/内容，关联触发器name',
  `moni_object` varchar(512) DEFAULT NULL COMMENT '监控对象',
  `cur_moni_value` varchar(512) DEFAULT NULL COMMENT '当前监控值',
  `cur_moni_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '当前监控时间',
  `alert_level` varchar(20) NOT NULL COMMENT '告警级别\r\n1-提示\r\n2-低\r\n3-中\r\n4-高\r\n5-严重\r\n',
  `item_id` varchar(50) DEFAULT NULL,
  `alert_end_time` datetime DEFAULT NULL COMMENT '告警结束时间',
  `remark` varchar(512) DEFAULT NULL COMMENT '备注',
  `order_status` varchar(50) DEFAULT NULL COMMENT '1-未派单\r\n2-处理中\r\n3-已完成\r\n',
  `clear_time` datetime DEFAULT NULL COMMENT '清除时间',
  `clear_user` varchar(32) DEFAULT NULL COMMENT '清除人',
  `clear_content` varchar(256) DEFAULT NULL COMMENT '清除内容',
  `source` varchar(100) DEFAULT NULL COMMENT '告警来源\r\nMIRROR',
  `idc_type` varchar(128) DEFAULT NULL COMMENT '所属位置-资源池',
  `source_room` varchar(100) DEFAULT NULL COMMENT '机房',
  `object_type` varchar(50) DEFAULT NULL,
  `object_id` varchar(50) DEFAULT NULL,
  `region` varchar(50) DEFAULT NULL,
  `device_ip` varchar(100) DEFAULT NULL,
  `order_type` varchar(20) DEFAULT NULL COMMENT '工单类型\r\n1-告警\r\n2-故障',
  `order_id` varchar(100) DEFAULT NULL COMMENT '工单ID',
  `biz_sys_desc` varchar(200) DEFAULT NULL COMMENT '业务系统名称（中文）',
  `alert_start_time` datetime DEFAULT NULL COMMENT '告警开始时间',
  `prefix` varchar(64) DEFAULT NULL COMMENT '告警前缀，用于多套告警系统时候区分标识',
  `alert_count` int default 1,
  PRIMARY KEY (`alert_id`),
  key `index_r_alert_id` (`r_alert_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '告警历史记录表';

-- ----------------------------
-- Table structure for alert_alerts_detail
-- ----------------------------
DROP TABLE IF EXISTS `alert_alerts_detail`;
CREATE TABLE `alert_alerts_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `alert_id` varchar(64) NOT NULL,
  `action_id` bigint(10) DEFAULT NULL,
  `event_id` varchar(80) NULL,
  `moni_index` varchar(1024) NULL COMMENT '监控指标/内容，关联触发器name',
  `moni_object` varchar(512) DEFAULT NULL COMMENT '监控对象',
  `cur_moni_value` varchar(512) DEFAULT NULL COMMENT '当前监控值',
  `cur_moni_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '当前监控时间',
  `alert_level` varchar(20) NOT NULL COMMENT '告警级别\r\n1-提示\r\n2-低\r\n3-中\r\n4-高\r\n5-严重\r\n',
  `item_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  key `index_alert_id` (`alert_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=10000000 COMMENT '告警上报记录表';

DROP TABLE IF EXISTS `alert_proxy_idc`;
CREATE TABLE `alert_proxy_idc` (
  `id` bigint(11) NOT NULL,
  `proxy_name` varchar(64) DEFAULT NULL,
  `idc` varchar(64) DEFAULT NULL,
  `remark` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '告警代理名称和资源池对应表';

-- ----------------------------
-- Records of alert_proxy_idc
-- ----------------------------
INSERT INTO `alert_proxy_idc` VALUES ('1', 'xxg_proxy245', '信息港资源池', '信息港资源池');
INSERT INTO `alert_proxy_idc` VALUES ('2', 'xxg_proxy76', '信息港资源池', '信息港资源池');
INSERT INTO `alert_proxy_idc` VALUES ('3', 'xxg_proxy77', '信息港资源池', '信息港资源池');
INSERT INTO `alert_proxy_idc` VALUES ('4', 'xxg_proxy78', '信息港资源池', '信息港资源池');
INSERT INTO `alert_proxy_idc` VALUES ('5', 'xxg_proxy79', '信息港资源池', '信息港资源池');
INSERT INTO `alert_proxy_idc` VALUES ('6', 'xxg_proxy80', '信息港资源池', '信息港资源池');
INSERT INTO `alert_proxy_idc` VALUES ('7', 'ccjk5_proxy', '业支域非池化', '业支域非池化');
INSERT INTO `alert_proxy_idc` VALUES ('8', 'hrb_proxy80', '哈尔滨资源池', '哈尔滨资源池');
INSERT INTO `alert_proxy_idc` VALUES ('9', 'hrb_proxy81', '哈尔滨资源池', '哈尔滨资源池');
INSERT INTO `alert_proxy_idc` VALUES ('10', 'Hohhot_ZAproxy1', '呼和浩特资源池', '呼和浩特资源池');
INSERT INTO `alert_proxy_idc` VALUES ('11', 'xxg', '信息港资源池', '信息港资源池');
INSERT INTO `alert_proxy_idc` VALUES ('12', 'fch', '业支域非池化', '业支域非池化');
INSERT INTO `alert_proxy_idc` VALUES ('13', 'hac', '哈尔滨资源池', '哈尔滨资源池');
INSERT INTO `alert_proxy_idc` VALUES ('14', 'huc', '呼和浩特资源池', '呼和浩特资源池');

INSERT INTO `alert_alerts` (`alert_id`, `r_alert_id`, `event_id`, `action_id`, `device_id`, `device_class`, `biz_sys`, `moni_index`, `moni_object`, `cur_moni_value`, `cur_moni_time`, `alert_level`, `item_id`, `remark`, `order_status`, `operate_status`, `source`, `idc_type`, `source_room`, `object_type`, `object_id`, `region`, `device_ip`, `order_id`, `order_type`, `alert_start_time`,`prefix`, `alert_count`) 
select a.`alert_id`, CONCAT(substring_index(IFNULL(a.`r_alert_id`,''),'_',1),'_',a.item_id,'_',a.alert_level), a.`event_id`, a.`action_id`, a.`device_id`, a.`device_class`, a.`biz_sys`, a.`moni_index`, a.`moni_object`, a.`cur_moni_value`, a.`cur_moni_time`, a.`alert_level`, a.`item_id`, a.`remark`, a.`order_status`, a.`operate_status`, a.`source`, a.`idc_type`, a.`source_room`, a.`object_type`, a.`object_id`, a.`region`, a.`device_ip`, a.`order_id`, a.`order_type`, a.`alert_start_time`,substring_index(IFNULL(a.`r_alert_id`,''),'_',1), n.alert_count from alert_alerts_0618 a INNER JOIN ( select i.device_ip,i.item_id,i.alert_level,max(alert_id) alert_id, count(1) alert_count, max(order_id) order_id, max(order_status) order_status from alert_alerts_0618 i GROUP BY i.device_ip, i.item_id, i.alert_level) n on a.alert_id=n.alert_id ;

INSERT INTO `alert_alerts_detail` (`alert_id`, `action_id`, `event_id`, `moni_index`, `moni_object`, `cur_moni_value`, `cur_moni_time`, `alert_level`, `item_id`) 
select n.alert_id, a.`action_id`,a.`event_id`, a.`moni_index`, a.`moni_object`, a.`cur_moni_value`, a.`cur_moni_time`, a.`alert_level`, a.`item_id` from alert_alerts_0618 a INNER JOIN ( select i.device_ip,i.item_id,i.alert_level,max(alert_id) alert_id, count(1) alert_count, max(order_id) order_id, max(order_status) order_status from alert_alerts_0618 i GROUP BY i.device_ip, i.item_id, i.alert_level) n on a.device_ip=n.device_ip and a.item_id=n.item_id and a.alert_level=n.alert_level ;


INSERT INTO `alert_alerts_his` (`alert_id`, `r_alert_id`, `event_id`, `action_id`, `device_id`, `device_class`, `biz_sys`, `moni_index`, `moni_object`, `cur_moni_value`, `cur_moni_time`, `alert_level`, `item_id`, `alert_end_time`, `remark`, `order_status`, `clear_time`, `clear_user`, `clear_content`, `source`, `idc_type`, `source_room`, `object_type`, `object_id`, `region`, `device_ip`, `order_type`, `order_id`, `biz_sys_desc`, `alert_start_time`,`prefix`, `alert_count`) 
select a.`alert_id`, CONCAT(substring_index(IFNULL(a.`r_alert_id`,''),'_',1),'_',a.item_id,'_',a.alert_level), a.`event_id`, a.`action_id`, a.`device_id`, a.`device_class`, a.`biz_sys`, a.`moni_index`, a.`moni_object`, a.`cur_moni_value`, a.`cur_moni_time`, a.`alert_level`, a.`item_id`, a.`alert_end_time`, a.`remark`, a.`order_status`, a.`clear_time`, a.`clear_user`, a.`clear_content`, a.`source`, a.`idc_type`, a.`source_room`, a.`object_type`, a.`object_id`, a.`region`, a.`device_ip`, a.`order_type`, a.`order_id`, a.`biz_sys_desc`, a.`alert_start_time`,substring_index(IFNULL(a.`r_alert_id`,''),'_',1), n.`alert_count` from alert_alerts_his_0618 a INNER JOIN ( select i.device_ip,i.item_id,i.alert_level,max(alert_id) alert_id,count(1) alert_count, max(order_id) order_id, max(order_status) order_status from alert_alerts_his_0618 i GROUP BY i.device_ip, i.item_id, i.alert_level) n on a.alert_id=n.alert_id ;

INSERT INTO `alert_alerts_detail` (`alert_id`, `action_id`, `event_id`, `moni_index`, `moni_object`, `cur_moni_value`, `cur_moni_time`, `alert_level`, `item_id`) 
select n.alert_id, a.`action_id`,a.`event_id`, a.`moni_index`, a.`moni_object`, a.`cur_moni_value`, a.`cur_moni_time`, a.`alert_level`, a.`item_id` from alert_alerts_his_0618 a INNER JOIN ( select i.device_ip,i.item_id,i.alert_level,max(alert_id) alert_id, count(1) alert_count, max(order_id) order_id, max(order_status) order_status from alert_alerts_his_0618 i GROUP BY i.device_ip, i.item_id, i.alert_level) n on a.device_ip=n.device_ip and a.item_id=n.item_id and a.alert_level=n.alert_level ;
