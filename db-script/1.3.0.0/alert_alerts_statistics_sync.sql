/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-07-01 14:43:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_alerts_statistics_sync
-- ----------------------------
DROP TABLE IF EXISTS `alert_alerts_statistics_sync`;
CREATE TABLE `alert_alerts_statistics_sync` (
  `idcType` varchar(300) DEFAULT NULL,
  `alertTotal` bigint(128) DEFAULT NULL,
  `seriousCount` bigint(128) DEFAULT NULL COMMENT '严重告警',
  `importantCount` bigint(128) DEFAULT NULL COMMENT '重要告警',
  `secondaryCount` bigint(128) DEFAULT NULL COMMENT '次要告警',
  `tipsCount` bigint(128) DEFAULT NULL COMMENT '提示告警',
  `month` varchar(24) DEFAULT NULL COMMENT '告警产生月份',
  `updateTime` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  KEY `index_r_alert_id` (`idcType`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警数量统计表';
