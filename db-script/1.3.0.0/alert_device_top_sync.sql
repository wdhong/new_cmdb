/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-07-01 14:44:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_device_top_sync
-- ----------------------------
DROP TABLE IF EXISTS `alert_device_top_sync`;
CREATE TABLE `alert_device_top_sync` (
  `idcType` varchar(300) DEFAULT NULL COMMENT '资源池',
  `alertLevel` varchar(12) DEFAULT NULL COMMENT '告警级别',
  `deviceType` varchar(10) DEFAULT NULL COMMENT '设备分类',
  `countOrder` int(1) DEFAULT NULL COMMENT '排名',
  `ip` varchar(128) DEFAULT NULL COMMENT '设备ip',
  `pod` varchar(128) DEFAULT NULL COMMENT 'pod',
  `alertCount` bigint(128) DEFAULT NULL COMMENT '告警数量',
  `roomId` varchar(128) DEFAULT NULL COMMENT '机房位置',
  `month` varchar(24) DEFAULT NULL COMMENT '告警产生时间',
  `deviceMfrs` varchar(255) DEFAULT NULL COMMENT '厂家',
  `updateTime` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  KEY `index_r_alert_id` (`idcType`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警分类top10统计表';
