/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-06-25 09:52:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_devicetype_config
-- ----------------------------
DROP TABLE IF EXISTS `alert_devicetype_config`;
CREATE TABLE `alert_devicetype_config` (
  `id` varchar(56) NOT NULL,
  `code` varchar(128) NOT NULL COMMENT '表里面存的名称',
  `name` varchar(255) DEFAULT NULL COMMENT '展示名称',
  `type` varchar(255) DEFAULT NULL COMMENT '类型：网络、物理服务器等'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='设备分类统计配置表';

-- ----------------------------
-- Records of alert_devicetype_config
-- ----------------------------
INSERT INTO `alert_devicetype_config` VALUES ('physicalMachine', '物理机', '物理服务器', 'physicServer_moniter_object');
INSERT INTO `alert_devicetype_config` VALUES ('firewall', '防火墙', '物理防火墙', 'network_moniter_object');
INSERT INTO `alert_devicetype_config` VALUES ('router', '路由器', '物理路由器', 'network_moniter_object');
INSERT INTO `alert_devicetype_config` VALUES ('switch', '交换机', '物理交换机', 'network_moniter_object');
INSERT INTO `alert_devicetype_config` VALUES ('diskArray', '磁盘阵列', '磁盘阵列', 'other');
INSERT INTO `alert_devicetype_config` VALUES ('tapeLibrary', '磁带库', '磁带库', 'other');
INSERT INTO `alert_devicetype_config` VALUES ('cloudStorage', '云存储', '云存储设备', 'other');
INSERT INTO `alert_devicetype_config` VALUES ('SDNController', 'SDN控制器', 'SDN控制器', 'other');
