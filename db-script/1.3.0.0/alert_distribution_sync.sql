/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-07-01 14:44:14
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_distribution_sync
-- ----------------------------
DROP TABLE IF EXISTS `alert_distribution_sync`;
CREATE TABLE `alert_distribution_sync` (
  `idcType` varchar(128) DEFAULT NULL,
  `alertLevel` varchar(12) DEFAULT NULL,
  `month` varchar(24) DEFAULT NULL,
  `physicalMachineCount` bigint(128) DEFAULT NULL COMMENT '防火墙',
  `firewallCount` bigint(128) DEFAULT NULL,
  `routerCount` bigint(128) DEFAULT NULL,
  `switchCount` bigint(128) DEFAULT NULL,
  `diskArrayCount` bigint(128) DEFAULT NULL,
  `tapeLibraryCount` bigint(128) DEFAULT NULL,
  `cloudStorageCount` bigint(128) DEFAULT NULL,
  `SDNControllerCount` bigint(128) DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  KEY `index_r_alert_id` (`idcType`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警分布统计表';
