DROP TABLE IF EXISTS `alert_mail_recipients`;
CREATE TABLE `alert_mail_recipients`(
`id` INT(32) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
`recipient` VARCHAR(64) NOT NULL COMMENT '收件邮箱',
`password` VARCHAR(64) NOT NULL COMMENT '收件邮箱密码',
`mail_server` VARCHAR(64) NOT NULL COMMENT '收件邮箱邮件服务器',
`receive_protocal` TINYINT(1) DEFAULT 0 COMMENT '收件协议, 0: pop3, 1: imap',
`receive_port` INT(8) NOT NULL COMMENT '收件邮箱邮件服务器收件端口',
`status` TINYINT(1) DEFAULT 0 COMMENT '是否启用, 0: 未启用, 1: 启用',
`recipient_desc` VARCHAR(128) DEFAULT NULL COMMENT '收件邮箱描述',
`period` INT DEFAULT NULL COMMENT '邮箱采集周期-单位:分钟',
`period_unit` tinyint(1) NOT NULL DEFAULT 0 COMMENT '周期时间单位 0:分钟,1:小时,2:天',
`create_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`update_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY (`id`)
)ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='邮件采集账号表'
;
CREATE UNIQUE INDEX uniq_recipient_alert_mail_recipient ON alert_mail_receivers(`recipient`); 



DROP TABLE IF EXISTS alert_mail_resolve_filter;
CREATE TABLE `alert_mail_resolve_filter`
(
`id` VARCHAR(50) NOT NULL COMMENT '主键ID',
`receiver` VARCHAR(64) NOT NULL COMMENT '收件邮箱',
`sender` VARCHAR(64) NOT NULL COMMENT '发件邮箱',
`title_incl` VARCHAR(64) NOT NULL COMMENT '邮件过滤:标题包含关键字',
`content_incl` VARCHAR(64) DEFAULT NULL COMMENT '邮件过滤:内容包含关键字',
`status` TINYINT(1) DEFAULT 0 COMMENT '是否启用, 0: 未启用, 1: 启用',
PRIMARY KEY(`id`)
)ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='告警采集配置表'
;



DROP TABLE IF EXISTS `alert_mail_resolve_strategy`;
CREATE TABLE alert_mail_resolve_strategy(
`id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
`filter_id` VARCHAR(50) NOT NULL COMMENT 'Filter主键ID',
`alert_field` VARCHAR(64) NOT NULL COMMENT '匹配警报的字段名',
`mail_field` TINYINT DEFAULT NULL COMMENT '邮件的字段名, 0: 标题, 1: 内容, 2:发件人, 3:发件时间',
`field_match_val` VARCHAR(64) DEFAULT NULL COMMENT '配置字段的值',
`use_reg` TINYINT(1) DEFAULT 0 COMMENT '使用正则匹配, 0: 不使用, 1:使用',
`field_match_reg` VARCHAR(64) DEFAULT NULL COMMENT '自定义正则表达式',
`field_match_target` VARCHAR(64) DEFAULT NULL COMMENT '字段匹配后映射值',
PRIMARY KEY (`id`)
)ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='告警采集自定义规则表'
;


DROP TABLE IF EXISTS `alert_mail_resolve_record`;
CREATE TABLE `alert_mail_resolve_record` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `filter_id` VARCHAR(50)  NOT NULL COMMENT 'Filter主键ID',
  `mail_title` VARCHAR(64)  NOT NULL COMMENT '邮件标题',
  `mail_content` TEXT  COMMENT '邮件内容',
  `mail_sender` VARCHAR(64)  NOT NULL COMMENT '发件人',
  `mail_receiver` VARCHAR(64)  NOT NULL COMMENT '收件人',
  `mail_send_time` DATETIME NOT NULL COMMENT '发件时间',
  `resolve_time` DATETIME NOT NULL COMMENT '采集时间',
  `device_ip` VARCHAR(64)  NOT NULL COMMENT '设备IP',
  `moni_index` VARCHAR(64)  DEFAULT NULL COMMENT '告警描述/告警内容',
  `moni_object` VARCHAR(128)  DEFAULT NULL COMMENT '告警指标',
  `alert_level` VARCHAR(20)  NOT NULL COMMENT '告警级别\r\n1-提示\r\n2-低\r\n3-中\r\n4-高\r\n5-严重\r\n',
  `alert_id` varchar(64) NOT NULL COMMENT '告警编号',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='邮件告警采集记录表'
;


DROP TABLE  IF EXISTS alert_mail_substance;
CREATE TABLE alert_mail_substance
(
`id` INT NOT NULL AUTO_INCREMENT COMMENT '主键ID',
`receiver` VARCHAR(64) NOT NULL COMMENT '收件邮箱',
`sender` VARCHAR(64) NOT NULL COMMENT '发件邮箱',
`send_time` DATETIME DEFAULT NULL COMMENT '发件时间',
`uid` VARCHAR(64) DEFAULT NULL COMMENT '邮件唯一标识',
`subject` VARCHAR(64) DEFAULT NULL COMMENT '邮件标题',
`content` TEXT DEFAULT NULL COMMENT '邮件内容',
`create_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY(`id`)
)ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='邮件读取记录表'
;

