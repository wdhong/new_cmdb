/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-07-01 14:44:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_moniter_top_sync
-- ----------------------------
DROP TABLE IF EXISTS `alert_moniter_top_sync`;
CREATE TABLE `alert_moniter_top_sync` (
  `idcType` varchar(300) DEFAULT NULL,
  `alertLevel` varchar(12) DEFAULT NULL,
  `deviceType` varchar(10) DEFAULT NULL,
  `countOrder` int(1) DEFAULT NULL,
  `moniterObject` varchar(128) DEFAULT NULL COMMENT '监控对象',
  `moniterAlertCount` bigint(128) DEFAULT NULL COMMENT '告警数量',
  `rate` decimal(20,4) DEFAULT NULL COMMENT '告警占比',
  `month` varchar(24) DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  KEY `index_r_alert_id` (`idcType`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警监控对象top10统计表';
