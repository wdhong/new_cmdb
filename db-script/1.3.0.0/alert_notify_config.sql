/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-07-02 22:37:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_notify_config
-- ----------------------------
DROP TABLE IF EXISTS `alert_notify_config`;
CREATE TABLE `alert_notify_config` (
  `uuid` varchar(255) NOT NULL COMMENT 'uuid',
  `name` varchar(255) DEFAULT NULL COMMENT '策略配置名称',
  `is_open` int(10) DEFAULT NULL COMMENT '是否启用 0-关闭(默认) 1-启用',
  `alert_filter_id` int(255) DEFAULT NULL COMMENT '告警筛选器id',
  `alert_filter_scene_id` int(255) DEFAULT NULL COMMENT '告警筛选场景id',
  `notify_alert_type` varchar(255) DEFAULT NULL COMMENT '告警通知类型(0-转派, 1-确认,2-派发工单, 3-清除, 4-通知, 5-过滤, 6-工程, 7-维护模式)',
  `notify_type` varchar(255) DEFAULT NULL COMMENT '通知类型 0-邮件/短信 1-邮件 2-短信',
  `is_recurrence_interval` int(10) DEFAULT NULL COMMENT '是否重发  0-关闭(默认) 1-启用',
  `recurrence_interval` varchar(255) DEFAULT NULL COMMENT '重发间隔时间',
  `recurrence_interval_util` varchar(255) DEFAULT NULL COMMENT '重发间隔单位 ',
  `resend_time` varchar(255) DEFAULT NULL COMMENT '重发时间',
  `email_type` int(10) DEFAULT NULL COMMENT '邮件发送类型 1-合并 2-单条',
  `send_operation` varchar(10) DEFAULT NULL COMMENT '发送记录',
  `cur_send_time` datetime DEFAULT NULL COMMENT '最新发送时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_first_send` varchar(2) DEFAULT NULL COMMENT '是否发送过 1-已发送',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警通知策略配置表';
