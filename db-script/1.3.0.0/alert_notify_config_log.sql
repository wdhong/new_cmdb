/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-07-02 19:16:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_notify_config_log
-- ----------------------------
DROP TABLE IF EXISTS `alert_notify_config_log`;
CREATE TABLE `alert_notify_config_log` (
  `alert_notify_config_id` varchar(255) DEFAULT NULL COMMENT '告警配置id',
  `send_status` varchar(255) DEFAULT NULL COMMENT '发送状态 0-失败 1-成功',
  `receiver` varchar(255) DEFAULT NULL COMMENT '接收人',
  `send_time` datetime DEFAULT NULL COMMENT '发送时间',
  `send_content` text COMMENT '发送内容',
  `send_ype` varchar(2) DEFAULT NULL COMMENT '发送类型 1-短信 2-邮件(单发) 3-邮件(合并)',
  `resend_time` varchar(255) DEFAULT NULL COMMENT '重发时间',
  `is_resend` varchar(2) DEFAULT NULL COMMENT '是否重发 0-否 1-是'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警通知配置发送记录表';
