/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-07-02 19:16:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_notify_config_receiver
-- ----------------------------
DROP TABLE IF EXISTS `alert_notify_config_receiver`;
CREATE TABLE `alert_notify_config_receiver` (
  `uuid` varchar(255) NOT NULL COMMENT 'uuid',
  `alert_notify_config_id` varchar(255) DEFAULT NULL COMMENT '告警通知策略配置表uuid',
  `notify_obj_type` varchar(255) DEFAULT NULL COMMENT '通知对象类型 1-团队 2-个人',
  `notify_obj_info` varchar(255) DEFAULT NULL COMMENT '通知对象信息',
  `notify_type` varchar(10) DEFAULT NULL COMMENT '通知类型 0-邮件/短信 1-邮件 2-短信',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警通知策略配置人员表';
