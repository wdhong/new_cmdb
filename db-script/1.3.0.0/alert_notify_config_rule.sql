/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-07-02 19:26:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_notify_config_rule
-- ----------------------------
DROP TABLE IF EXISTS `alert_notify_config_rule`;
CREATE TABLE `alert_notify_config_rule` (
  `id` varchar(10) NOT NULL COMMENT 'id',
  `rule_name` varchar(255) DEFAULT NULL COMMENT '定时任务规则名称',
  `rule_cron` varchar(255) DEFAULT NULL COMMENT 'cron',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alert_notify_config_rule
-- ----------------------------
INSERT INTO `alert_notify_config_rule` VALUES ('1', 'send', '0 0/10 * * * ? ');
INSERT INTO `alert_notify_config_rule` VALUES ('2', 'resend', '0 0/1 * * * ? ');
