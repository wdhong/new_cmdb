INSERT INTO `cmdb`.`t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`) VALUES ('9941', '呼和资源池', 'dicType_statistics', '呼和资源池', NULL, '', NULL, NULL, '0');
INSERT INTO `cmdb`.`t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`) VALUES ('9942', '哈尔滨资源池', 'dicType_statistics', '哈尔滨资源池', NULL, '', NULL, NULL, '0');
INSERT INTO `cmdb`.`t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`) VALUES ('9944', '国际信息港', 'dicType_statistics', '国际信息港', NULL, '', NULL, NULL, '0');
INSERT INTO `cmdb`.`t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`) VALUES ('9945', '南方基地', 'dicType_statistics', '南方基地', NULL, '', NULL, NULL, '0');


INSERT INTO `cmdb`.`t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`) VALUES ('9945', '国际信息港', 'dicType_default', '国际信息港', NULL, '', NULL, NULL, '0');


INSERT INTO `cmdb`.`t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`) VALUES ('9946', 'interfaces', 'network_moniter_object', 'interfaces', NULL, '', NULL, NULL, '0');


INSERT INTO `cmdb`.`t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`) VALUES ('9948', 'CPU运行状态', 'physicServer_zabbix', 'CPU', NULL, '', NULL, NULL, '0');
INSERT INTO `cmdb`.`t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`) VALUES ('9949', '内存运行状态', 'physicServer_zabbix', 'Memory', NULL, '', NULL, NULL, '0');
INSERT INTO `cmdb`.`t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`) VALUES ('9950', 'Filesystems', 'physicServer_zabbix', 'Filesystems', NULL, '', NULL, NULL, '0');
INSERT INTO `cmdb`.`t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`) VALUES ('9951', 'OS', 'physicServer_zabbix', 'OS', NULL, '', NULL, NULL, '0');
INSERT INTO `cmdb`.`t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`) VALUES ('9952', '电压运行状态', 'physicServer_prometheus', 'Power', NULL, '', NULL, NULL, '0');
INSERT INTO `cmdb`.`t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`) VALUES ('9953', 'CPU运行状态', 'physicServer_prometheus', 'CPU', NULL, '', NULL, NULL, '0');
INSERT INTO `cmdb`.`t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`) VALUES ('9954', '风扇运行状态', 'physicServer_prometheus', 'Fan', NULL, '', NULL, NULL, '0');
INSERT INTO `cmdb`.`t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`) VALUES ('9956', 'Temp', 'physicServer_prometheus', 'Temp', NULL, '', NULL, NULL, '0');



INSERT INTO `cmdb`.`t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`) VALUES ('9957', 'physicServer_zabbix', 'physicServer_source_zabbix', 'ZABBIX', NULL, '', NULL, NULL, '0');
INSERT INTO `cmdb`.`t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`) VALUES ('9958', 'physicServer_prometheus', 'physicServer_source_prometheus', 'PROMETHEUS', NULL, '', NULL, NULL, '0');




INSERT INTO `cmdb`.`t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`) VALUES ('9be958ba594a4736a2890377f64553d5', 'cpuMoniterItem', 'moniter_object', 'ipmi_cpu_error', NULL, '', '2019-06-19 14:30:30', NULL, '0');
INSERT INTO `cmdb`.`t_cfg_dict` (`dict_id`, `dict_code`, `col_name`, `dict_note`, `up_dict`, `description`, `create_date`, `update_date`, `delete_flag`) VALUES ('eff817ec9cee4a2683defe696094822b', 'memoryMoniterItem', 'moniter_object', 'ipmi_mem_error', NULL, '', '2019-06-19 14:31:52', NULL, '0');



