/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : cmdb

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-06-11 11:17:23
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_demand_changed
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_demand_changed`;
CREATE TABLE `cmdb_demand_changed` (
  `demand_id` varchar(40) NOT NULL COMMENT '需求ID',
  `name` varchar(100) DEFAULT NULL COMMENT '资源项',
  `oldVal` varchar(255) DEFAULT NULL COMMENT '历史值',
  `newVal` varchar(20) DEFAULT NULL COMMENT '当前值',
  `updateTime` varchar(40) DEFAULT NULL COMMENT '修改时间',
  `updateUser` varchar(50) DEFAULT NULL COMMENT '修改人员'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
