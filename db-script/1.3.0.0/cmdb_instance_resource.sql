/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : cmdb

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-06-20 09:58:21
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_instance_resource
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_instance_resource`;
CREATE TABLE `cmdb_instance_resource` (
  `bizSystem` varchar(40) DEFAULT NULL COMMENT '业务系统',
  `device_class` varchar(40) DEFAULT NULL COMMENT '设备分类',
  `device_type` varchar(40) DEFAULT NULL COMMENT '设备类型： 物理机，虚拟机',
  `department1` varchar(40) DEFAULT NULL COMMENT '一级部门',
  `department2` varchar(40) DEFAULT NULL COMMENT '二级部门',
  `idcType` varchar(40) DEFAULT NULL COMMENT '资源池名称',
  `pod_name` varchar(40) DEFAULT NULL COMMENT 'POD名称',
  `total_planned_application` varchar(40) DEFAULT NULL COMMENT '计划申请总量',
  `total_allocated_equipment` varchar(40) DEFAULT NULL COMMENT '已分配设备总量',
  `delivery_cycle` varchar(40) DEFAULT NULL COMMENT '交付周期',
  `delivery_ratio` varchar(40) DEFAULT NULL COMMENT '交付比例',
  `vcpu` varchar(40) DEFAULT NULL COMMENT '虚拟核数VCPU (虚拟机)',
  `total_memory` varchar(40) DEFAULT NULL COMMENT '总内存 (虚拟机)',
  `createTime` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='各业务物理资源分配分析和虚拟资源使用率分析报表';
