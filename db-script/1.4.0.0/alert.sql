ALTER TABLE `alert_alerts`
ADD COLUMN `device_type`  varchar(128) NULL COMMENT '设备类型' AFTER `source_room`,
ADD COLUMN `device_mfrs`  varchar(128) NULL COMMENT '设备提供商' AFTER `device_type`,
ADD COLUMN `device_model`  varchar(128) NULL COMMENT '设备型号' AFTER `device_mfrs`;

ALTER TABLE `alert_alerts_his`
ADD COLUMN `device_type`  varchar(128) NULL COMMENT '设备类型' AFTER `source_room`,
ADD COLUMN `device_mfrs`  varchar(128) NULL COMMENT '设备提供商' AFTER `device_type`,
ADD COLUMN `device_model`  varchar(128) NULL COMMENT '设备型号' AFTER `device_mfrs`;


ALTER TABLE `alert_alerts`
ADD COLUMN `create_time`  datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间' AFTER `mail_send`;
ALTER TABLE `alert_alerts_his`
ADD COLUMN `create_time`  datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间' AFTER `mail_send`;

---------2019-07-24------------------
ALTER TABLE `alert_alerts`
ADD COLUMN `host_name`  varchar(128) NULL COMMENT '主机名' AFTER `device_mfrs`;
ALTER TABLE `alert_alerts_his`
ADD COLUMN `host_name` varchar(128) NULL COMMENT '主机名' AFTER `device_mfrs`;
