/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-07-30 17:12:07
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_rep_panel
-- ----------------------------
DROP TABLE IF EXISTS `alert_rep_panel`;
CREATE TABLE `alert_rep_panel` (
  `id` int(128) NOT NULL AUTO_INCREMENT,
  `panel_name` varchar(128) DEFAULT NULL,
  `show_item` varchar(20000) DEFAULT NULL COMMENT '布局信息',
  `editer` varchar(24) DEFAULT NULL,
  `creater` varchar(24) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_r_alert_id` (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COMMENT='配置项信息表';
