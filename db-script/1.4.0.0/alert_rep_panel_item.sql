/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-07-30 16:29:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_rep_panel_item
-- ----------------------------
DROP TABLE IF EXISTS `alert_rep_panel_item`;
CREATE TABLE `alert_rep_panel_item` (
  `id` int(128) NOT NULL AUTO_INCREMENT,
  `panel_id` varchar(24) DEFAULT NULL COMMENT '关联的大屏id',
  `item_id` varchar(24) DEFAULT NULL COMMENT '布局的id',
  `report_type` varchar(10) DEFAULT NULL COMMENT '报表类型',
  `report_name` varchar(128) DEFAULT NULL COMMENT '名称',
  `time_value` int(128) DEFAULT NULL COMMENT '时间刻度值',
  `time_unit` varchar(8) DEFAULT NULL COMMENT '横轴时间单位',
  `report_unit` varchar(24) DEFAULT NULL COMMENT '单位',
  `conversion_type` tinyint(1) DEFAULT NULL COMMENT '换算：1相乘2相除',
  `conversion_val` int(128) DEFAULT NULL COMMENT '换算数值',
  PRIMARY KEY (`id`),
  KEY `index_r_alert_id` (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=225 DEFAULT CHARSET=utf8 COMMENT='大屏单个报表配置';
