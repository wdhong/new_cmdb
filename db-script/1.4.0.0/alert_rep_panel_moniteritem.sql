/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-07-30 16:30:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_rep_panel_moniteritem
-- ----------------------------
DROP TABLE IF EXISTS `alert_rep_panel_moniteritem`;
CREATE TABLE `alert_rep_panel_moniteritem` (
  `id` int(128) NOT NULL AUTO_INCREMENT,
  `item_id` varchar(24) DEFAULT NULL COMMENT '关联大屏配置项id',
  `resource_device` varchar(4000) DEFAULT NULL COMMENT '资源设备',
  `moniter_item` varchar(256) DEFAULT NULL COMMENT '监控指标',
  `view_name` varchar(128) DEFAULT NULL COMMENT '显示名称',
  `count_type` varchar(24) DEFAULT NULL COMMENT '统计方式：1最大2最小3平均',
  `resource_type` tinyint(1) DEFAULT NULL COMMENT '资源类型：1业务系统2资源池3机房4设备大类5设备小类6设备ip',
  `unit` varchar(24) DEFAULT NULL COMMENT '监控项单位',
  `mointer_index` varchar(24) DEFAULT NULL COMMENT '所属es索引',
  `resource_device_ipStr` varchar(8000) DEFAULT NULL COMMENT '资源设备ip',
  PRIMARY KEY (`id`),
  KEY `index_r_alert_id` (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=284 DEFAULT CHARSET=utf8 COMMENT='大屏单个报表配置';
