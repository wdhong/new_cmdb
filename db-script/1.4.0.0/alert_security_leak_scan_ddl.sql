
DROP TABLE IF EXISTS security_leak_scan_records;
CREATE TABLE `security_leak_scan_records` (
  `id` varchar(50) NOT NULL COMMENT '主键ID',
  `biz_line` varchar(64) NOT NULL COMMENT '归属业务线',
  `report_file_name` varchar(64) NOT NULL COMMENT '报告文件名',
  `report_file_url` varchar(128) NOT NULL COMMENT '服务端-报告文件下载地址',
  `scan_date` date NOT NULL COMMENT '扫描日期',
  `file_id` varchar(50) DEFAULT NULL COMMENT '工单上传文件ID',
  `bpm_id` varchar(50) DEFAULT NULL COMMENT '工单流程ID',
  `repair_stat` tinyint(1) DEFAULT '0' COMMENT '修复状态: 0 未修复，1 已修复',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='安全漏洞扫描记录表'
;

DROP TABLE IF EXISTS security_leak_scan_reports;
CREATE TABLE `security_leak_scan_reports` (
  `id` varchar(50) NOT NULL COMMENT '主键ID',
  `scan_id` varchar(50) NOT NULL COMMENT '扫描记录主键ID',
  `ip` varchar(64) NOT NULL COMMENT 'IP 地址',
  `report_path` VARCHAR(128) DEFAULT NULL COMMENT '报告路径',
  `high_leaks` int(11) DEFAULT '0' COMMENT '高危漏洞数量',
  `medium_leaks` int(11) DEFAULT '0' COMMENT '中危漏洞数量',
  `low_leaks` int(11) DEFAULT '0' COMMENT '低微漏洞数量',
  `risk_val` float DEFAULT '0' COMMENT '风险值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='安全漏洞扫描记录表'
;

DROP TABLE IF EXISTS security_leak_scan_report_files;
CREATE TABLE `security_leak_scan_report_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `scan_id` varchar(50) NOT NULL COMMENT '扫描记录主键ID',
  `file_name` varchar(64) NOT NULL COMMENT '报告压缩包文件名',
  `ftp_path` varchar(120) NOT NULL COMMENT 'FTP-报告压缩包文件下载地址',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='安全漏洞扫描记录文件解析表'
;