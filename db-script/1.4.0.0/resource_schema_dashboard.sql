DELETE from `mirror`.`resource_schema` s where s.name ='值班日志';
DELETE from `mirror`.`resource_schema` s where s.name ='自动化'
OR s.parent_resource IN (
	SELECT
		ss.resource
	FROM
		`mirror`.`resource_schema` ss
	WHERE
		ss.name = '自动化');

delete from `mirror`.`resource_schema` s		WHERE
	s.parent_resource = 'monitor'
OR s.parent_resource IN (
	SELECT
		ss.resource
	FROM
		`mirror`.`resource_schema` ss
	WHERE
		ss.parent_resource = 'monitor'
);




INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('alert', 'f', '2019-04-09 08:10:52.000000', '告警管理看板', 'monitor');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('alert_filter', 'f', '2019-04-09 08:10:52.000000', '告警筛选设置', 'alert_strategy');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('alert_filterscene', 'f', '2019-04-09 08:10:52.000000', '告警筛选场景', 'alert_strategy');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('alert_intelligent', 'f', '2019-07-03 05:00:00.000000', '告警收敛看板', 'monitor');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('alert_notify_config', 'f', '2019-07-03 05:00:00.000000', '告警通知策略', 'alert_strategy');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('alert_strategy', 'f', '2019-04-09 08:10:52.000000', '告警策略管理', 'monitor');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('big_screen', 'f', '2019-04-09 08:10:52.000000', '监控大屏管理', 'monitor');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('biz_monitor', 'f', '2019-04-09 08:10:52.000000', '业务监控管理', 'monitor');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('biz_template', 'f', '2019-04-09 08:10:52.000000', '指标定义管理', 'biz_monitor');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('config_center_search', 'f', '2019-07-03 05:00:00.000000', '配置文件管理', 'net_config_manage');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('current_alert', 'f', '2019-04-09 08:10:52.000000', '当前告警', 'alert');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('daily_manage', 'f', '2019-07-03 05:00:00.000000', '日常管理', 'monitor');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('dashboard_manage', 'f', '2019-07-03 05:00:00.000000', '监控dashboard', 'monitor_screen_manage');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('email_config', 'f', '2019-07-03 05:00:00.000000', '邮件告警设置', 'alert_strategy');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('his_alert', 'f', '2019-04-09 08:10:52.000000', '历史告警', 'alert');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('host_view', 'f', '2019-04-09 08:10:52.000000', '主机视图', 'big_screen');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('inspection_daily', 'f', '2019-07-03 05:00:00.000000', '巡检日报', 'daily_manage');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('monitor_screen_manage', 'f', '2019-04-09 08:10:52.000000', '监控视图管理', 'monitor');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('network_view', 'f', '2019-04-09 08:10:52.000000', '网络视图', 'big_screen');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('net_config_manage', 'f', '2019-07-03 05:00:00.000000', '网络监控管理', 'monitor');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('net_device_log', 'f', '2019-07-03 05:00:00.000000', 'sys日志管理', 'net_config_manage');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('one_view', 'f', '2019-04-09 08:10:52.000000', '统一视图', 'big_screen');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('plan_config_manage', 'f', '2019-04-09 08:10:52.000000', '策略配置管理', 'net_config_manage');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('report', 'f', '2019-04-09 08:10:52.000000', '巡检报表管理', 'sys_monitor');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('security_leak_manage', 'f', '2019-07-25 14:18:53.000000', '安全漏洞管理', 'monitor');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('storage_view', 'f', '2019-04-09 08:10:52.000000', '存储视图', 'big_screen');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('sys_monitor', 'f', '2019-04-09 08:10:52.000000', '监控巡检管理', 'monitor');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('task', 'f', '2019-04-09 08:10:52.000000', '巡检任务管理', 'sys_monitor');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('task_run_search', 'f', '2019-04-09 08:10:52.000000', '任务巡检检索', 'sys_monitor');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('template', 'f', '2019-04-09 08:10:52.000000', '巡检模板管理', 'sys_monitor');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('theme', 'f', '2019-04-09 08:10:52.000000', '主题定义管理', 'biz_monitor');
INSERT INTO `mirror`.`resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('work_log', 'f', '2019-05-31 15:04:19.000000', '值班日志', 'daily_manage');
