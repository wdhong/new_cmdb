/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : cmdb

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-13 09:42:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_mainten_hardware
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_mainten_hardware`;
CREATE TABLE `cmdb_mainten_hardware` (
  `id` varchar(40) NOT NULL COMMENT 'id',
  `project_instance_id` varchar(64) DEFAULT NULL COMMENT '维保项目与主机绑定关系ID',
  `buy_mainten` varchar(32) DEFAULT NULL COMMENT '是否购买维保 0-否 1-是',
  `origin_buy` varchar(32) DEFAULT NULL COMMENT '是否原厂维保 0-否 1-是',
  `origin_buy_explain` varchar(64) DEFAULT NULL COMMENT '原厂维保购买必要性说明',
  `admin` varchar(20) DEFAULT NULL COMMENT '维保管理员',
  `create_time` datetime DEFAULT NULL COMMENT '数据创建时间',
  PRIMARY KEY (`id`),
  KEY `index_id_projectInstanceId` (`id`,`project_instance_id`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='硬件维保管理表';
