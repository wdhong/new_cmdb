/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : cmdb

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-13 09:43:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_mainten_hardware_use
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_mainten_hardware_use`;
CREATE TABLE `cmdb_mainten_hardware_use` (
  `id` varchar(40) NOT NULL COMMENT 'id',
  `hardware_id` varchar(40) DEFAULT NULL COMMENT '硬件维保ID',
  `project_instance_id` varchar(40) DEFAULT NULL COMMENT '维保项目与主机绑定关系ID',
  `server_person` varchar(64) DEFAULT NULL COMMENT '服务人员',
  `server_level` varchar(64) DEFAULT NULL COMMENT '服务级别',
  `start_date` date DEFAULT NULL COMMENT '本期维保开始时间',
  `end_date` date DEFAULT NULL COMMENT '本期维保结束时间',
  `process_time` varchar(64) DEFAULT NULL COMMENT '处理时长',
  `actual_man_day` varchar(64) DEFAULT NULL COMMENT '实际人天',
  `mobile_approver` varchar(32) DEFAULT NULL COMMENT '移动审批人',
  `operate_approver` varchar(64) DEFAULT NULL COMMENT '运维审批人',
  `creater` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `isdel` int(10) unsigned zerofill DEFAULT NULL COMMENT '是否删除 0未删除 1已删除',
  PRIMARY KEY (`id`),
  KEY `index_id_hardwareId` (`id`,`hardware_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='硬件维保使用管理表';
