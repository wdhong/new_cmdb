/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : cmdb

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-13 19:25:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_mainten_produce
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_mainten_produce`;
CREATE TABLE `cmdb_mainten_produce` (
  `id` varchar(40) NOT NULL COMMENT '厂商ID',
  `produce` varchar(200) DEFAULT NULL COMMENT '厂商',
  `produce_type` varchar(200) DEFAULT NULL COMMENT '厂商类型',
  `remark` varchar(2000) DEFAULT NULL COMMENT '备注',
  `isdel` int(2) DEFAULT NULL COMMENT '是否删除 0:未删除 1:已删除',
  `create_time` datetime DEFAULT NULL COMMENT '入库时间',
  `update_time` datetime DEFAULT NULL COMMENT ' 最近修改时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='维保厂商信息表';
