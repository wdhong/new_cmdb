/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : cmdb

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-13 19:25:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_mainten_produce_concat
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_mainten_produce_concat`;
CREATE TABLE `cmdb_mainten_produce_concat` (
  `produceId` varchar(200) DEFAULT NULL COMMENT '厂商ID',
  `name` varchar(200) DEFAULT NULL COMMENT '联系人',
  `phone` varchar(20) DEFAULT NULL COMMENT '电话',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `remark` varchar(2000) DEFAULT NULL COMMENT '备注'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='维保厂商联系人信息表';
