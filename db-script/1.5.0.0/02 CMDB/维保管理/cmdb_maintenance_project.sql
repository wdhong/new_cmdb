/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : cmdb

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-13 09:58:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_maintenance_project
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_maintenance_project`;
CREATE TABLE `cmdb_maintenance_project` (
  `id` varchar(40) NOT NULL COMMENT 'ID',
  `project_name` varchar(100) NOT NULL COMMENT '项目名称',
  `project_no` varchar(100) NOT NULL COMMENT '项目编码',
  `produce_id` varchar(40) NOT NULL COMMENT '维保厂商ID',
  `service_type` varchar(20) DEFAULT NULL COMMENT '服务类型',
  `service_number` int(11) DEFAULT NULL COMMENT '服务数量',
  `service_start_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '服务开始时间',
  `service_end_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '服务结束时间',
  `is_delete` varchar(2) DEFAULT '0' COMMENT '是否删除 0未删除 1已删除',
  PRIMARY KEY (`id`),
  KEY `IDX_cmdb_maintenance_project_01` (`project_name`,`project_no`,`produce_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='维保项目管理表';
