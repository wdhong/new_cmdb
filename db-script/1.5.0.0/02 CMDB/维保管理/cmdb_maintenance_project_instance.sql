/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : cmdb

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-13 09:58:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_maintenance_project_instance
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_maintenance_project_instance`;
CREATE TABLE `cmdb_maintenance_project_instance` (
  `id` varchar(40) NOT NULL COMMENT 'ID',
  `project_id` varchar(40) NOT NULL COMMENT '项目ID',
  `device_sn` varchar(100) NOT NULL COMMENT '设备序列号',
  `instance_id` varchar(40) DEFAULT NULL COMMENT '设备ID',
  `is_delete` varchar(2) DEFAULT '0' COMMENT '是否删除 0未删除 1已删除',
  PRIMARY KEY (`id`),
  KEY `IDX_cmdb_maintenance_project_instance_01` (`project_id`,`device_sn`,`instance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='维保项目与主机绑定关系表';
