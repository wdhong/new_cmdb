/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : cmdb

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-13 09:58:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_maintenance_project_mfrs
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_maintenance_project_mfrs`;
CREATE TABLE `cmdb_maintenance_project_mfrs` (
  `id` varchar(40) NOT NULL COMMENT 'ID',
  `project_id` varchar(40) NOT NULL COMMENT '项目ID',
  `produce_id` varchar(40) NOT NULL COMMENT '厂家ID',
  `produce_concat_name` varchar(40) NOT NULL COMMENT '厂家联系人名称',
  `produce_concat_phone` varchar(20) DEFAULT NULL COMMENT '厂家联系人电话',
  `produce_concat_email` varchar(40) DEFAULT NULL COMMENT '厂家联系人邮箱',
  `is_delete` varchar(2) DEFAULT '0' COMMENT '是否删除 0未删除 1已删除',
  PRIMARY KEY (`id`),
  KEY `IDX_cmdb_maintenance_project_mfrs_01` (`project_id`),
  KEY `IDX_cmdb_maintenance_project_mfrs_02` (`produce_id`),
  KEY `IDX_cmdb_maintenance_project_mfrs_03` (`project_id`,`produce_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='维保项目厂商关系表';
