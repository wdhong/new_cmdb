/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 5.7.26-log 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;


DROP TABLE IF EXISTS `cmdb_maintenance_software`;
CREATE TABLE `cmdb_maintenance_software` (
  `id` varchar(40) NOT NULL COMMENT 'ID',
  `project_id` varchar(40) NOT NULL COMMENT '项目id',
  `classify` varchar(40) NOT NULL COMMENT '分类',
  `software_name` varchar(255) NOT NULL COMMENT '软件名称',
  `unit` varchar(20) NOT NULL COMMENT '单位',
  `number` varchar(20) NOT NULL COMMENT '数量',
  `admin` varchar(20) DEFAULT NULL COMMENT '管理员',
  `remark` text COMMENT '备注',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `project` (`project_id`,`software_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
