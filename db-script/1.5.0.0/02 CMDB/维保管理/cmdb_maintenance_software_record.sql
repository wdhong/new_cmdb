/*
Navicat MySQL Data Transfer

Source Server         : online_151
Source Server Version : 50718
Source Host           : localhost:8765
Source Database       : cmdb

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2019-08-13 20:41:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_maintenance_software_record
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_maintenance_software_record`;
CREATE TABLE `cmdb_maintenance_software_record` (
  `id` varchar(40) NOT NULL COMMENT 'id',
  `software_id` varchar(40) NOT NULL COMMENT '软件维保id',
  `server_start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '服务开始时间',
  `server_end_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '服务开始时间',
  `server_person` varchar(40) NOT NULL COMMENT '服务人员',
  `server_level` varchar(40) NOT NULL COMMENT '服务等级',
  `handle_long` varchar(40) NOT NULL COMMENT '处理时长',
  `real_days` varchar(10) NOT NULL COMMENT '实际人天',
  `yidong_approver` varchar(40) NOT NULL COMMENT '移动审批人',
  `devops_approver` varchar(40) NOT NULL COMMENT '运维审批人',
  `create_user` varchar(40) NOT NULL COMMENT '创建人',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `software_record_softId` (`software_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='软件维保使用管理';
