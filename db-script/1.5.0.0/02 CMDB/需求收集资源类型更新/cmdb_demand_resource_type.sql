/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : cmdb

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-13 10:58:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_demand_resource_type
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_demand_resource_type`;
CREATE TABLE `cmdb_demand_resource_type` (
  `resource_type_id` varchar(40) NOT NULL COMMENT 'id',
  `resource_owner` varchar(40) DEFAULT NULL COMMENT '所属分类',
  `resource_owner_order` int(11) DEFAULT NULL COMMENT '所属分类排序',
  `resource_code` varchar(100) NOT NULL COMMENT '分类编码',
  `resource_type` varchar(100) NOT NULL COMMENT '分类名称',
  `parent_type_id` varchar(40) DEFAULT NULL COMMENT '父分类Id',
  `resource_order` int(11) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`resource_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='需求收集资源类型';

-- ----------------------------
-- Records of cmdb_demand_resource_type
-- ----------------------------
INSERT INTO `cmdb_demand_resource_type` VALUES ('1', '计算资源类', '1', 'vm', '虚拟机资源', ' ', '1');
INSERT INTO `cmdb_demand_resource_type` VALUES ('10', '', '0', 'basic_1', '分布式文件存储（TB）', '9', '1');
INSERT INTO `cmdb_demand_resource_type` VALUES ('11', '', '0', 'basic_2', '分布式块存储（TB）', '9', '2');
INSERT INTO `cmdb_demand_resource_type` VALUES ('12', '', '0', 'basic_3', '对象存储（TB）', '9', '3');
INSERT INTO `cmdb_demand_resource_type` VALUES ('13', '', '0', 'basic_4', 'FC-SAN存储 (TB)', '9', '4');
INSERT INTO `cmdb_demand_resource_type` VALUES ('14', '', '0', 'basic_5', 'IP-SAN存储 (TB)', '9', '5');
INSERT INTO `cmdb_demand_resource_type` VALUES ('15', '', '0', 'basic_6', '备份存储容量需求（TB）', '9', '6');
INSERT INTO `cmdb_demand_resource_type` VALUES ('16', '', '0', 'network_1', 'CMNET地址需求（个）', '69', '7');
INSERT INTO `cmdb_demand_resource_type` VALUES ('17', '', '0', 'network_2', '至CMNET带宽（Gbps）', '69', '8');
INSERT INTO `cmdb_demand_resource_type` VALUES ('18', '', '0', 'network_3', 'IP承载网地址需求（个）', '69', '9');
INSERT INTO `cmdb_demand_resource_type` VALUES ('19', '', '0', 'network_14', '至IP专网带宽（Gbps）', '69', '10');
INSERT INTO `cmdb_demand_resource_type` VALUES ('2', '', '0', 'vm_1', '16核、128G、200G系统磁盘', '1', '1');
INSERT INTO `cmdb_demand_resource_type` VALUES ('20', '标准资源类', '4', 'database', '数据库', '', '5');
INSERT INTO `cmdb_demand_resource_type` VALUES ('21', '', '0', 'database_1', 'MySQL', '20', '1');
INSERT INTO `cmdb_demand_resource_type` VALUES ('22', '', '0', 'database_2', 'MongoDB', '20', '2');
INSERT INTO `cmdb_demand_resource_type` VALUES ('23', '', '0', 'database_3', 'PostgreSQL', '20', '3');
INSERT INTO `cmdb_demand_resource_type` VALUES ('24', '标准资源类', '4', 'fbs', '分布式缓存', '', '6');
INSERT INTO `cmdb_demand_resource_type` VALUES ('25', '', '0', 'fbs_1', 'Redis', '24', '1');
INSERT INTO `cmdb_demand_resource_type` VALUES ('26', '标准资源类', '4', 'message', '消息中间件', '', '7');
INSERT INTO `cmdb_demand_resource_type` VALUES ('27', '', '0', 'message_1', 'ActiveMQ', '26', '1');
INSERT INTO `cmdb_demand_resource_type` VALUES ('28', '', '0', 'message_2', 'Kafka', '26', '3');
INSERT INTO `cmdb_demand_resource_type` VALUES ('29', '标准资源类', '4', 'app', '应用中间件', '', '8');
INSERT INTO `cmdb_demand_resource_type` VALUES ('3', '', '0', 'vm_2', '8核、32G、200G系统磁盘', '1', '2');
INSERT INTO `cmdb_demand_resource_type` VALUES ('30', '', '0', 'app_1', 'Apache HTTP Server', '29', '1');
INSERT INTO `cmdb_demand_resource_type` VALUES ('31', '', '0', 'app_2', 'Tomcat', '29', '3');
INSERT INTO `cmdb_demand_resource_type` VALUES ('32', '标准资源类', '4', 'haproxy', '负载均衡', '', '9');
INSERT INTO `cmdb_demand_resource_type` VALUES ('33', '', '0', 'haproxy_1', 'Nginx', '32', '1');
INSERT INTO `cmdb_demand_resource_type` VALUES ('34', '', '0', 'haproxy_2', 'Haproxy', '32', '2');
INSERT INTO `cmdb_demand_resource_type` VALUES ('35', '标准资源类', '4', 'fbs_service', '分布式协调服务', '', '10');
INSERT INTO `cmdb_demand_resource_type` VALUES ('36', '', '0', 'fbs_service_1', 'Zookeeper', '35', '1');
INSERT INTO `cmdb_demand_resource_type` VALUES ('37', '', '0', 'fbs_service_2', 'ETCD', '35', '2');
INSERT INTO `cmdb_demand_resource_type` VALUES ('38', '标准资源类', '4', 'search', '搜索中间件', '', '11');
INSERT INTO `cmdb_demand_resource_type` VALUES ('39', '', '0', 'search_1', 'Elastic search', '38', '1');
INSERT INTO `cmdb_demand_resource_type` VALUES ('4', '', '0', 'vm_3', '4核、16G、200G系统磁盘', '1', '5');
INSERT INTO `cmdb_demand_resource_type` VALUES ('40', '标准资源类', '4', 'registry', '镜像仓库', '', '13');
INSERT INTO `cmdb_demand_resource_type` VALUES ('41', '', '0', 'registry_1', 'harbor', '40', '1');
INSERT INTO `cmdb_demand_resource_type` VALUES ('42', '标准资源类', '4', 'flow', '工作流', '', '16');
INSERT INTO `cmdb_demand_resource_type` VALUES ('44', '标准资源类', '4', 'lang', '语音类基础镜像', '', '17');
INSERT INTO `cmdb_demand_resource_type` VALUES ('45', '', '0', 'lang_1', 'openjdk', '44', '1');
INSERT INTO `cmdb_demand_resource_type` VALUES ('46', '标准资源类', '4', 'fbs_gz_system', '分布式跟踪系统', '', '14');
INSERT INTO `cmdb_demand_resource_type` VALUES ('47', '', '0', 'fbs_gz_system_1', 'Jenkins', '46', '1');
INSERT INTO `cmdb_demand_resource_type` VALUES ('48', '标准资源类', '4', 'develop', '开发框架', '', '18');
INSERT INTO `cmdb_demand_resource_type` VALUES ('49', '', '0', 'develop_1', 'config-server(svn/git/mysql)', '48', '2');
INSERT INTO `cmdb_demand_resource_type` VALUES ('5', '计算资源类', '1', 'physical', '物理机资源', ' ', '2');
INSERT INTO `cmdb_demand_resource_type` VALUES ('50', '', '0', 'vm_4', '8核、16G、200G系统磁盘', '1', '3');
INSERT INTO `cmdb_demand_resource_type` VALUES ('51', '', '0', 'vm_5', '4核、32G、200G系统磁盘', '1', '4');
INSERT INTO `cmdb_demand_resource_type` VALUES ('52', '', '0', 'vm_6', '4核、8G、200G系统磁盘', '1', '6');
INSERT INTO `cmdb_demand_resource_type` VALUES ('53', '', '0', 'vm_7', '2核、4G、200G系统磁盘', '1', '7');
INSERT INTO `cmdb_demand_resource_type` VALUES ('54', '', '0', 'physical_4', '缓存型服务器（台）', '5', '4');
INSERT INTO `cmdb_demand_resource_type` VALUES ('55', '', '0', 'physical_5', '高端应用服务器（台）', '5', '5');
INSERT INTO `cmdb_demand_resource_type` VALUES ('57', '', '0', 'database_5', '其它数据库', '20', '5');
INSERT INTO `cmdb_demand_resource_type` VALUES ('58', '', '0', 'fbs_2', 'Memcached', '24', '2');
INSERT INTO `cmdb_demand_resource_type` VALUES ('59', '', '0', 'message_3', 'RocketMQ', '26', '4');
INSERT INTO `cmdb_demand_resource_type` VALUES ('6', '', '0', 'physical_1', '虚拟机宿主机/低端应用服务器', '5', '1');
INSERT INTO `cmdb_demand_resource_type` VALUES ('60', '', '0', 'app_3', 'Jboos EAP', '29', '2');
INSERT INTO `cmdb_demand_resource_type` VALUES ('62', '', '0', 'flow_2', 'activiti', '42', '2');
INSERT INTO `cmdb_demand_resource_type` VALUES ('63', '', '0', 'lang_2', 'Python', '44', '2');
INSERT INTO `cmdb_demand_resource_type` VALUES ('64', '', '0', 'lang_3', 'Go', '44', '3');
INSERT INTO `cmdb_demand_resource_type` VALUES ('65', '', '0', 'lang_4', 'Node.js', '44', '4');
INSERT INTO `cmdb_demand_resource_type` VALUES ('66', '', '0', 'lang_5', 'Ruby / Ruby on Rails', '44', '5');
INSERT INTO `cmdb_demand_resource_type` VALUES ('67', '', '0', 'lang_6', '.NET Core', '44', '6');
INSERT INTO `cmdb_demand_resource_type` VALUES ('69', '网络资源类', '3', 'network', '网络资源', ' ', '4');
INSERT INTO `cmdb_demand_resource_type` VALUES ('7', '', '0', 'physical_2', '分析型服务器（MPP服务器）（台）', '5', '2');
INSERT INTO `cmdb_demand_resource_type` VALUES ('77', '', '0', 'message_4', 'RabbitMQ', '26', '2');
INSERT INTO `cmdb_demand_resource_type` VALUES ('78', '标准资源类', '4', 'fbs_file_system', '分布式文件系统', ' ', '12');
INSERT INTO `cmdb_demand_resource_type` VALUES ('79', null, '0', 'fbs_file_system_1', 'fastDFS', '78', '1');
INSERT INTO `cmdb_demand_resource_type` VALUES ('8', '', '0', 'physical_3', '分布式服务器（Hadoop服务器）（台）', '5', '3');
INSERT INTO `cmdb_demand_resource_type` VALUES ('80', '标准资源类', '4', 'api_network', 'API网关', ' ', '15');
INSERT INTO `cmdb_demand_resource_type` VALUES ('81', null, '0', 'api_network_1', 'kong/konga', '80', '1');
INSERT INTO `cmdb_demand_resource_type` VALUES ('82', '', '0', 'develop_2', 'erueka', '48', '3');
INSERT INTO `cmdb_demand_resource_type` VALUES ('83', null, '0', 'develop_3', 'zuul', '48', '4');
INSERT INTO `cmdb_demand_resource_type` VALUES ('84', null, '0', 'develop_4', '其它开发框架', '48', '5');
INSERT INTO `cmdb_demand_resource_type` VALUES ('85', null, '0', 'develop_5', 'Spring Cloud', '48', '1');
INSERT INTO `cmdb_demand_resource_type` VALUES ('9', '存储资源类', '2', 'basic', '存储资源', ' ', '3');
