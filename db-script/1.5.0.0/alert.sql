﻿/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-28 20:20:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_isolate
-- ----------------------------
DROP TABLE IF EXISTS `alert_isolate`;
CREATE TABLE `alert_isolate` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '告警屏蔽ID',
  `name` varchar(64) NOT NULL COMMENT '规则名称',
  `description` varchar(1024) DEFAULT NULL COMMENT '规则描述',
  `status` varchar(1) DEFAULT '0' COMMENT '启用状态，0-停用，1-启用',
  `effective_date` datetime DEFAULT NULL COMMENT '生效时间',
  `expire_date` datetime DEFAULT NULL COMMENT '失效时间',
  `operate_user` varchar(512) DEFAULT NULL COMMENT '维护用户',
  `option_condition` varchar(4000) DEFAULT NULL COMMENT '过滤条件',
  `creater` varchar(64) DEFAULT NULL COMMENT '创建人',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `editer` varchar(64) DEFAULT NULL COMMENT '修改人',
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `is_delete` tinyint(4) DEFAULT '0' COMMENT '删除状态，0-未删除，1-已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='告警屏蔽表';

-- ----------------------------
-- Table structure for alert_isolate_alerts
-- ----------------------------
DROP TABLE IF EXISTS `alert_isolate_alerts`;
CREATE TABLE `alert_isolate_alerts` (
  `alert_id` varchar(64) NOT NULL,
  `isolate_id` bigint(11) NOT NULL COMMENT '屏蔽ID',
  `r_alert_id` varchar(300) DEFAULT NULL,
  `event_id` varchar(80) DEFAULT NULL,
  `action_id` bigint(10) DEFAULT NULL,
  `device_id` varchar(50) DEFAULT NULL,
  `device_class` varchar(128) DEFAULT NULL COMMENT '设备类型',
  `biz_sys` varchar(128) DEFAULT NULL COMMENT '业务系统',
  `moni_index` varchar(1024) DEFAULT NULL COMMENT '监控指标/内容，关联触发器name',
  `moni_object` varchar(512) DEFAULT NULL COMMENT '监控对象',
  `cur_moni_value` varchar(512) DEFAULT NULL COMMENT '当前监控值',
  `cur_moni_time` datetime DEFAULT NULL COMMENT '当前监控时间',
  `alert_level` varchar(20) NOT NULL COMMENT '告警级别\r\n1-提示\r\n2-低\r\n3-中\r\n4-高\r\n5-严重\r\n',
  `item_id` varchar(50) DEFAULT NULL,
  `remark` varchar(512) DEFAULT NULL COMMENT '备注',
  `source` varchar(100) DEFAULT NULL COMMENT '告警来源\r\nMIRROR\r\nZABBIX\r\n',
  `idc_type` varchar(128) DEFAULT NULL COMMENT '所属位置-资源池',
  `source_room` varchar(100) DEFAULT NULL COMMENT '机房/资源池',
  `device_type` varchar(128) DEFAULT NULL COMMENT '设备类型',
  `device_mfrs` varchar(128) DEFAULT NULL COMMENT '设备提供商',
  `host_name` varchar(128) DEFAULT NULL COMMENT '主机名',
  `device_model` varchar(128) DEFAULT NULL COMMENT '设备型号',
  `object_type` varchar(50) DEFAULT NULL COMMENT '告警类型\r\n1-系统\r\n2-业务',
  `object_id` varchar(50) DEFAULT NULL COMMENT '对象ID，如果是设备告警则是设备ID，如果是业务则是业务系统code',
  `region` varchar(50) DEFAULT NULL COMMENT '域/资源池code',
  `device_ip` varchar(100) DEFAULT NULL,
  `alert_start_time` datetime DEFAULT NULL COMMENT '告警开始时间',
  `prefix` varchar(64) DEFAULT NULL COMMENT '告警前缀，用于多套告警系统时候区分标识',
  `alert_count` int(11) DEFAULT '1',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`alert_id`),
  KEY `index_r_alert_id` (`r_alert_id`) USING BTREE,
  KEY `index_isolate_id` (`isolate_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='屏蔽告警日志表';

-- ----------------------------
-- Table structure for alert_operate_log
-- ----------------------------
DROP TABLE IF EXISTS `alert_operate_log`;
CREATE TABLE `alert_operate_log` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `relation_id` varchar(64) DEFAULT NULL COMMENT '关联的ID',
  `operate_model` varchar(128) NOT NULL COMMENT '操作模块,如告警、屏蔽、筛选等',
  `operate_model_desc` varchar(256) DEFAULT NULL,
  `operate_type` varchar(64) NOT NULL COMMENT '操作类型,如新增、修改等',
  `operate_type_desc` varchar(256) DEFAULT NULL,
  `operater` varchar(64) DEFAULT NULL COMMENT '操作人',
  `operate_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  `operate_content` varchar(4000) DEFAULT NULL COMMENT '操作内容',
  `remark` varchar(256) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `index_relation_id` (`relation_id`) USING BTREE,
  KEY `index_relation_id_group` (`relation_id`,`operate_model`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COMMENT='操作记录表';

ALTER TABLE `alert_alerts`
ADD COLUMN `pod_name`  varchar(128) NULL COMMENT 'pod池' AFTER `device_model`;
ALTER TABLE `alert_alerts_his`
ADD COLUMN `pod_name`  varchar(128) NULL COMMENT 'pod池' AFTER `device_model`;
