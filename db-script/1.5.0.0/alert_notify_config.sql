ALTER TABLE `alert_notify_config`
ADD COLUMN `period`  varchar(128) NULL COMMENT '告警通知配置执行时间段 0-全天 1-时间段' ,
ADD COLUMN `start_period`  varchar(128) NULL COMMENT '配置起始执行时间' ,
ADD COLUMN `end_period`  varchar(128) NULL COMMENT '配置执行结束' ;