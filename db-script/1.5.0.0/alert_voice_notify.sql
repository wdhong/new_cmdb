/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-30 21:26:57
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_voice_notify
-- ----------------------------
DROP TABLE IF EXISTS `alert_voice_notify`;
CREATE TABLE `alert_voice_notify` (
  `uuid` varchar(255) NOT NULL COMMENT 'uuis',
  `voice_language` varchar(255) NOT NULL COMMENT '语言类型',
  `voice_content` varchar(255) DEFAULT NULL COMMENT '语音内容',
  `is_open` int(2) NOT NULL COMMENT '0-禁用 1-启用',
  `alert_filter_id` int(255) NOT NULL COMMENT '过滤器id',
  `alert_filter_scene_id` int(255) NOT NULL COMMENT '告警筛选场景id',
  `content` varchar(255) DEFAULT NULL COMMENT '通知内容',
  `alert_exist_time` int(11) DEFAULT NULL COMMENT '告警存在时长',
  `voice_alert_id` text COMMENT '已播报的告警id',
  `creator` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
