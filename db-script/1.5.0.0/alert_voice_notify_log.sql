/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-30 21:27:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_voice_notify_log
-- ----------------------------
DROP TABLE IF EXISTS `alert_voice_notify_log`;
CREATE TABLE `alert_voice_notify_log` (
  `alert_notify_config_id` varchar(255) NOT NULL COMMENT '播报配置id',
  `voice_alert_id` text NOT NULL COMMENT '播报告警id',
  `operator` varchar(255) DEFAULT NULL COMMENT '操作人',
  `operation_time` datetime DEFAULT NULL COMMENT '操作时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
