CREATE TABLE IF NOT EXISTS `t_cfg_module_customized` (
  `user_id` varchar(64) NOT NULL COMMENT '用户',
  `module_id` varchar(64) NOT NULL COMMENT '模块',
  `content` text COMMENT 'json内容',
  PRIMARY KEY (`user_id`,`module_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='模块定制化表';