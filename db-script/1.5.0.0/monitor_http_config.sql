/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.38
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-09-05 16:28:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for monitor_http_config
-- ----------------------------
DROP TABLE IF EXISTS `monitor_http_config`;
CREATE TABLE `monitor_http_config` (
  `id` int(38) unsigned NOT NULL AUTO_INCREMENT,
  `monitor_name` varchar(255) NOT NULL,
  `biz_system_id` varchar(128) DEFAULT NULL COMMENT '业务系统id',
  `alert_level` tinyint(2) NOT NULL,
  `test_period` int(255) NOT NULL COMMENT '检测周期',
  `idcType` varchar(128) NOT NULL COMMENT '归属资源池',
  `isIntranet` tinyint(1) DEFAULT NULL COMMENT '是否内网（1内网0外网）',
  `Intranet_idcType` varchar(128) DEFAULT NULL COMMENT '检测资源池',
  `extranet` varchar(128) DEFAULT NULL COMMENT '检测外网',
  `create_staff` varchar(128) DEFAULT NULL COMMENT '创建人',
  `update_staff` varchar(128) DEFAULT NULL COMMENT '修改人',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `request_http_addr` varchar(255) DEFAULT NULL COMMENT '监控地址',
  `request_method` varchar(128) DEFAULT NULL COMMENT '请求方法',
  `response_type` varchar(255) DEFAULT NULL COMMENT '请求返回类型(json/html)',
  `regular_check` varchar(255) DEFAULT NULL COMMENT '正则表达式',
  `response_code` tinyint(1) DEFAULT NULL COMMENT '返回码（1:200,0：非200）',
  `request_parm` varchar(4000) DEFAULT NULL COMMENT '请求参数',
  `json_attribute` varchar(255) DEFAULT NULL COMMENT 'JSON属性',
  `json_value` varchar(255) DEFAULT NULL COMMENT 'JSON正常参考值',
  `json_operator` varchar(255) DEFAULT NULL COMMENT 'Json属性运算符（>,<,=...）',
  `time_out` int(11) DEFAULT NULL COMMENT '超时时间',
  `html_format` varchar(4000) DEFAULT NULL COMMENT '返回的Html格式',
  `http_content_type` varchar(255) DEFAULT NULL COMMENT '请求媒体类型',
  `status` tinyint(2) DEFAULT NULL COMMENT '状态（:0：暂停1：运行）',
  `idcTypeUrl` varchar(2000) DEFAULT NULL COMMENT '资源池访问的代理记账地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='http监控配置表';
