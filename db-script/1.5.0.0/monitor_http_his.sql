/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.38
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-09-05 16:28:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for monitor_http_his
-- ----------------------------
DROP TABLE IF EXISTS `monitor_http_his`;
CREATE TABLE `monitor_http_his` (
  `id` int(38) NOT NULL AUTO_INCREMENT,
  `http_config_id` int(38) DEFAULT NULL COMMENT '监控配置表ID',
  `request_method` varchar(255) DEFAULT NULL COMMENT '请求方法(post/get)',
  `http_content_type` varchar(255) DEFAULT NULL COMMENT '请求媒体类型',
  `request_parm` varchar(4000) DEFAULT NULL COMMENT '请求参数',
  `request_http_addr` varchar(255) DEFAULT NULL COMMENT '监控地址',
  `time_out` int(11) DEFAULT NULL COMMENT '超时时间',
  `start_time` varchar(128) DEFAULT NULL COMMENT '请求时间',
  `end_time` varchar(128) DEFAULT NULL COMMENT '响应时间',
  `time_con` varchar(128) DEFAULT NULL COMMENT '响应耗时（毫秒）',
  `response_type` varchar(255) DEFAULT NULL COMMENT '请求返回类型(json/html)',
  `response_code` tinyint(1) DEFAULT NULL COMMENT '返回码（1:200,0：非200）',
  `json_attribute` varchar(255) DEFAULT NULL COMMENT 'JSON属性',
  `json_value` varchar(255) DEFAULT NULL COMMENT 'JSON正常参考值',
  `json_operator` varchar(255) DEFAULT NULL COMMENT 'Json属性运算符（>,<,=...）',
  `regular_check` varchar(255) DEFAULT NULL COMMENT '正则表达式',
  `html_format` varchar(4000) DEFAULT NULL COMMENT '返回的Html格式',
  `request_result` text COMMENT '返回结果字符串',
  `conclusion` varchar(255) DEFAULT NULL COMMENT '返回结果判定',
  `normal` tinyint(1) DEFAULT NULL COMMENT '返回结果(1正常0响应超时2主机连接异常3服务器连接错误)',
  `head_response` varchar(4000) DEFAULT NULL COMMENT '返回的head信息',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `result` tinyint(1) DEFAULT NULL COMMENT '结果：1正常0异常',
  `response_status_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=252 DEFAULT CHARSET=utf8 COMMENT='http监控历史表';
