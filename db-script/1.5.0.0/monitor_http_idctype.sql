/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.38
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-09-05 16:28:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for monitor_http_idctype
-- ----------------------------
DROP TABLE IF EXISTS `monitor_http_idctype`;
CREATE TABLE `monitor_http_idctype` (
  `idcType` varchar(255) NOT NULL,
  `url` varchar(2000) NOT NULL,
  PRIMARY KEY (`idcType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of monitor_http_idctype
-- ----------------------------

