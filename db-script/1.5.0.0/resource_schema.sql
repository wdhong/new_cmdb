INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('grafana_manage', 'f', '2019-07-03 05:00:00.000000', 'grafana报表', 'monitor_screen_manage');
update resource_schema set name = '运营分析' where resource = 'grafana';

INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES 
('isolate_config', 'f', '2019-07-03 05:00:00.000000', '告警屏蔽管理', 'alert_strategy');

-- {{ 金素 add 2019/9/3
INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('complexHome', 'f', '2019-04-09 08:10:52.000000', '综合首页', 'home');
-- }}

-- {{ 龙凤 add 2019/9/11
INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('http_monitor_manage', 'f', '2019-07-03 05:00:00.000000', 'Http监控管理', 'monitor');
-- }}
