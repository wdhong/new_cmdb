/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-10-09 13:56:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for log_filter_rule
-- ----------------------------
DROP TABLE IF EXISTS `log_filter_rule`;
CREATE TABLE `log_filter_rule` (
  `uuid` varchar(255) NOT NULL COMMENT '日志过滤规则id',
  `name` varchar(255) NOT NULL COMMENT '规则名称',
  `description` varchar(255) DEFAULT NULL COMMENT '规则描述',
  `idc_type` varchar(255) DEFAULT NULL COMMENT '资源池',
  `ip` varchar(255) DEFAULT NULL COMMENT '设备ip',
  `param` varchar(255) DEFAULT NULL COMMENT '关键字',
  `start_time` varchar(255) DEFAULT NULL COMMENT '开始时间',
  `end_time` varchar(255) DEFAULT NULL COMMENT '结束时间',
  `creator` varchar(255) DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='sys日志过滤规则配置表';
