update resource_schema set name = '监控' where resource = 'monitor';

INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) 
VALUES ('my_alert', 'f', '2019-07-03 05:00:00.000000', '我的告警', 'home_alert');

INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) 
VALUES ('http_monitor', 'f', '2019-07-03 05:00:00.000000', 'Http监控', 'http_monitor_manage');

INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) 
VALUES ('head_alert', 'f', '2019-07-03 05:00:00.000000', '告警', 'all');

update resource_schema set parent_resource = 'head_alert', name='告警看板管理' where resource = 'alert';

update resource_schema set parent_resource = 'head_alert' where resource = 'alert_intelligent';

update resource_schema set parent_resource = 'head_alert' where resource = 'alert_strategy';

update resource_schema set parent_resource = 'head_alert' where resource = 'daily_manage';

INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) 
VALUES ('security', 'f', '2019-07-03 05:00:00.000000', '安全', 'all');

update resource_schema set parent_resource = 'security' where resource = 'security_leak_manage';

INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) 
VALUES ('leak_file_list', 'f', '2019-07-03 05:00:00.000000', '文件扫描列表', 'security_leak_manage');

INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) 
VALUES ('resource_index_map', 'f', '2019-10-09 17:49:08', '资源首页', 'resource_view');

