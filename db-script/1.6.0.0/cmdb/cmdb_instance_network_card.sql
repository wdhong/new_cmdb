/*
Navicat MySQL Data Transfer

Source Server         : bpm
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : cmdb

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-09-20 15:18:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_instance_network_card
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_instance_network_card`;
CREATE TABLE `cmdb_instance_network_card` (
  `id` varchar(40) NOT NULL,
  `instance_id` varchar(40) DEFAULT NULL COMMENT '资产编号',
  `network_card_name` varchar(40) DEFAULT NULL COMMENT '网卡名称',
  `network_card_status` varchar(20) DEFAULT NULL COMMENT '状态',
  `port_type` varchar(20) DEFAULT NULL COMMENT '端口类型',
  `port_rate` varchar(20) DEFAULT NULL COMMENT '端口速率',
  `ipv4_address` varchar(20) DEFAULT NULL COMMENT 'IPv4地址',
  `ipv6_address` varchar(20) DEFAULT NULL COMMENT 'IPv6地址',
  `mac_address` varchar(20) DEFAULT NULL COMMENT 'mac地址',
  `default_gateway` varchar(20) DEFAULT NULL COMMENT '默认网关',
  `is_dhcp` varchar(10) DEFAULT NULL COMMENT '是否DHCP',
  `dhcp_address` varchar(20) DEFAULT NULL COMMENT 'DHCP地址',
  `dns_server` varchar(20) DEFAULT NULL COMMENT 'DNS服务器',
  `remark` varchar(40) DEFAULT NULL COMMENT '网卡描述',
  `is_delete` varchar(20) DEFAULT NULL COMMENT '0 未删除 1 已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
