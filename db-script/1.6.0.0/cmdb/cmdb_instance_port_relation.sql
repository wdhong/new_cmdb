/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 5.7.26-log 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;


DROP TABLE IF EXISTS `cmdb_instance_port_relation`;
CREATE TABLE `cmdb_instance_port_relation` (
  `id` varchar(40) NOT NULL COMMENT 'ID',
  `a_instance_id` varchar(40) NOT NULL COMMENT '本端设备ID',
  `a_port_id` varchar(40) NOT NULL COMMENT '本端端口',
  `z_instance_id` varchar(40) NOT NULL COMMENT '对端设备ID',
  `z_port_id` varchar(40) NOT NULL COMMENT '对端设备端口名称',
  `connect_type` varchar(20) NOT NULL COMMENT '连接属性',
  `remark` text COMMENT '描述',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `PORT_IP` (`a_instance_id`,`a_port_id`,`z_instance_id`,`z_port_id`) COMMENT '本端ip端口与对端端口一对一'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='设备关联关系视图'
