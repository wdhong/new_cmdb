alter  table cmdb_maintenance_project add column contract_contact varchar(40);
alter  table cmdb_maintenance_project add column contract_contact_phone varchar(40);
alter  table cmdb_maintenance_project add column contract_contact_email varchar(40);
alter  table cmdb_maintenance_project add column maintenance_type varchar(40);