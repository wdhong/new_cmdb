alter table alert_rep_panel_moniteritem add device_class varchar(255)  comment'设备分类';
alter table alert_rep_panel_moniteritem add device_type varchar(255)  comment'设备类型';
alter table alert_rep_panel_moniteritem add subclass varchar(255)  comment'子类';
alter table alert_rep_panel_moniteritem add moniter_item_name varchar(255)  comment'监控项';
alter table alert_rep_panel_moniteritem add sub_moniter_items varchar(8000)  comment'多个子监控项///,分隔';

