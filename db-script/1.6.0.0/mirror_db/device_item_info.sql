/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.38
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-09-26 15:11:23
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for device_item_info
-- ----------------------------
DROP TABLE IF EXISTS `device_item_info`;
CREATE TABLE `device_item_info` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `device_class` varchar(255) DEFAULT NULL COMMENT '设备分类',
  `device_type` varchar(255) DEFAULT NULL COMMENT '设备类型',
  `subclass` varchar(255) DEFAULT NULL COMMENT '监控逻辑子类',
  `moniter_item_name` varchar(255) DEFAULT NULL COMMENT '监控标准名称',
  `moniter_item_key` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL COMMENT '解释',
  `alert_level` varchar(255) DEFAULT NULL,
  `is_create_alert` varchar(255) DEFAULT NULL COMMENT '是否产生告警',
  `default_value` varchar(255) DEFAULT NULL COMMENT '缺省阀值设定',
  `monitor_rate` varchar(255) DEFAULT NULL COMMENT '监控频率',
  `protocol` varchar(255) DEFAULT NULL COMMENT '实现方式',
  `is_show_idcType` varchar(255) DEFAULT NULL COMMENT '是否显示资源池位置',
  `is_show_room` varchar(255) DEFAULT NULL COMMENT '是否显示机房位置',
  `is_show_frame` varchar(255) DEFAULT NULL COMMENT '是否显示机架位置',
  `note` varchar(255) DEFAULT NULL COMMENT '备注',
  `alert_tips` varchar(255) DEFAULT NULL COMMENT '告警提示语',
  `object_type` varchar(255) DEFAULT NULL COMMENT '告警分类',
  `is_create_order` varchar(255) DEFAULT NULL COMMENT '是否产生工单',
  PRIMARY KEY (`id`),
  KEY `device_class` (`device_class`) USING HASH,
  KEY `moniter_item_name` (`moniter_item_name`) USING HASH,
  KEY `device_type-device_class` (`device_type`,`device_class`) USING BTREE,
  KEY `subclass-device_type-device_class` (`subclass`,`device_class`,`device_type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=388 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of device_item_info
-- ----------------------------
INSERT INTO `device_item_info` VALUES ('1', '服务器', '物理机', '硬件状态', 'CPU运行状态', '', 'CPU当前运行状态', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}CPU故障', '是', '异常');
INSERT INTO `device_item_info` VALUES ('2', '服务器', '物理机', '硬件状态', '内存运行状态', '', '内存当前运行状态', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}MEM故障', '是', '异常');
INSERT INTO `device_item_info` VALUES ('3', '服务器', '物理机', '硬件状态', '风扇运行状态', '', '风扇当前运行状态', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}风扇故障', '是', '异常');
INSERT INTO `device_item_info` VALUES ('4', '服务器', '物理机', '硬件状态', '电源运行状态', '', '电源当前运行状态', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}电源故障', '是', '异常');
INSERT INTO `device_item_info` VALUES ('5', '服务器', '物理机', '硬件状态', '驱动器运行状态', '', '驱动器当前运行状态', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}驱动器故障', '是', '异常');
INSERT INTO `device_item_info` VALUES ('6', '服务器', '物理机', '硬件状态', '远程管理端口可达性', '', 'ping主机远程管理端口IP地址可达性检查', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}远程管理端口ping不通', '是', '异常');
INSERT INTO `device_item_info` VALUES ('7', '服务器', '物理机', '硬件状态', '电压运行状态', '', '电压当前运行状态', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}电压出现故障', '是', '异常');
INSERT INTO `device_item_info` VALUES ('8', '服务器', '物理机', '硬件状态', '其它固件指标等', '', '其它固件指标等', '', '否', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}固件异常', '否', '');
INSERT INTO `device_item_info` VALUES ('9', '服务器', '物理机', 'CPU占用率', '一分钟CPU占用率', 'system.cpu.load[percpu,avg1] ', '主机一分钟CPU占用率', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}CPU占用率高', '是', '>70%');
INSERT INTO `device_item_info` VALUES ('10', '服务器', '物理机', 'CPU占用率', '五分钟CPU占用率', 'system.cpu.load[percpu,avg5] ', '主机五分钟CPU占用率', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('11', '服务器', '物理机', 'CPU占用率', '十五分钟CPU占用率', 'system.cpu.load[percpu,avg15] ', '主机十五分钟CPU占用率', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('12', '服务器', '物理机', 'CPU占用率', 'CPU系统时间占用比率', 'system.cpu.util[,system,avg5] ', 'CPU系统时间占用比率', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}CPU系统时间占用比率高', '是', '>70%');
INSERT INTO `device_item_info` VALUES ('13', '服务器', '物理机', 'CPU占用率', 'CPU iowait时间占用的比率', 'system.cpu.util[,iowait,avg5] ', 'CPU iowait时间占用的比率', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('14', '服务器', '物理机', 'CPU占用率', 'CPU nice时间占用的比率', 'system.cpu.util[,nice,avg5] ', 'CPU nice时间占用的比率', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('15', '服务器', '物理机', 'CPU占用率', 'CPU steal时间占用的比率', 'system.cpu.util[,steal,avg5] ', 'CPU steal时间占用的比率', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('16', '服务器', '物理机', 'CPU占用率', 'CPU 用户时间占用的比率', 'system.cpu.util[,user,avg5] ', 'CPU 用户时间占用的比率', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('17', '服务器', '物理机', 'CPU占用率', 'CPU 软中断时间占用的比率', 'system.cpu.util[,softirq,avg5] ', 'CPU 软中断时间占用的比率', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('18', '服务器', '物理机', 'CPU占用率', 'CPU空闲时间占用的比率', 'system.cpu.util[,idle,avg5] ', 'CPU空闲时间占用的比率', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('19', '服务器', '物理机', 'CPU占用率', '5分钟内单核CPU平均负载', 'system.cpu.load[all,avg5] ', '5分钟内单核CPU平均负载', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('20', '服务器', '物理机', 'CPU占用率', 'CPU interrupt时间占用的比率', '', 'CPU interrupt时间占用的比率', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('21', '服务器', '物理机', '内存占用率', '总物理内存的大小', 'vm.memory.size[total]', '总物理内存的大小', '', '否', '600s', '是', '是', '是', '是', '', null, '', '是', '>90%');
INSERT INTO `device_item_info` VALUES ('22', '服务器', '物理机', '内存占用率', '系统的内存占用率', 'custom.memory.pused ', '从操作系统获取的内存占用率', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}系统的内存占用率高', '否', '');
INSERT INTO `device_item_info` VALUES ('23', '服务器', '物理机', '内存占用率', '文件系统元数据缓存大小', '', '文件系统元数据缓存大小', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('24', '服务器', '物理机', '内存占用率', '其它缓存大小', 'vm.memory.size[cached] ', '其它缓存大小', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('25', '服务器', '物理机', '内存占用率', '共享内存大小', 'vm.memory.size[shared] ', '共享内存大小', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('26', '服务器', '物理机', '内存占用率', '可用内存的大小', 'vm.memory.size[available] ', '可用内存的大小', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('27', '服务器', '物理机', '内存占用率', '物理内存当中近期在用的内存', 'custom.memory.used ', '物理内存当中近期在用的内存', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('28', '服务器', '物理机', '内存占用率', '物理内存当中近期未被使用的内存', '', '物理内存当中近期未被使用的内存', '', '否', '600s', '是', '是', '是', '是', '', null, '', '是', '');
INSERT INTO `device_item_info` VALUES ('29', '服务器', '物理机', '端口状态', '端口状态up/down', '', '端口状态up/down', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', 'down');
INSERT INTO `device_item_info` VALUES ('30', '服务器', '物理机', '磁盘占用率\n', '系统核心目录', 'vfs.fs.size[{#FSNAME},pused]', '存放系统的大多数信息，重点监控', '中', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}/opt占用超过90%', '是', '>90%');
INSERT INTO `device_item_info` VALUES ('31', '服务器', '物理机', '磁盘占用率\n', '系统高目录', 'vfs.fs.size[{#FSNAME},pused]', '系统高目录磁盘使用率', '中', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}/oracle占用超过90%', '是', '');
INSERT INTO `device_item_info` VALUES ('32', '服务器', '物理机', '磁盘占用率\n', 'home目录', 'vfs.fs.size[{#FSNAME},pused]', 'home目录磁盘使用率', '中', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}/home占用超过90%', '是', '');
INSERT INTO `device_item_info` VALUES ('33', '服务器', '物理机', '磁盘占用率\n', '根目录', 'vfs.fs.size[{#FSNAME},pused]', '根目录磁盘使用率', '中', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}/占用超过90%', '是', '');
INSERT INTO `device_item_info` VALUES ('34', '服务器', '物理机', '磁盘占用率\n', 'usr文件系统', 'vfs.fs.size[{#FSNAME},pused]', 'usr文件系统磁盘使用率', '中', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}/usr占用超过90%', '是', '');
INSERT INTO `device_item_info` VALUES ('35', '服务器', '物理机', '磁盘占用率\n', 'var文件系统', ' vfs.fs.size[{#FSNAME},pused]', 'var文件系统磁盘使用率', '中', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}/var占用超过90%', '是', '');
INSERT INTO `device_item_info` VALUES ('36', '服务器', '物理机', '磁盘占用率\n', 'tmp文件系统', ' vfs.fs.size[{#FSNAME},pused]', 'tmp文件系统磁盘使用率', '中', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}/tmp占用超过90%', '是', '');
INSERT INTO `device_item_info` VALUES ('37', '服务器', '物理机', '磁盘占用率\n', 'arch文件系统', ' vfs.fs.size[{#FSNAME},pused]', 'arch文件系统磁盘使用率', '中', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}/xx占用超过90%', '是', '>90%');
INSERT INTO `device_item_info` VALUES ('38', '服务器', '物理机', '系统性能', '主机可达性', 'icmpping ', 'Ping主机操作系统IP地址是否可达', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}操作系统ping不通', '是', '异常');
INSERT INTO `device_item_info` VALUES ('39', '服务器', '物理机', '系统性能', '每秒成功读磁盘的次数', '', '每秒成功读磁盘的次数', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '无');
INSERT INTO `device_item_info` VALUES ('40', '服务器', '物理机', '系统性能', '每秒成功写磁盘的次数', '', '每秒成功写磁盘的次数', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '无');
INSERT INTO `device_item_info` VALUES ('41', '服务器', '物理机', '系统性能', 'IO的当前进度（IOPS）', '', 'IO的当前进度（IOPS）', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '<>0');
INSERT INTO `device_item_info` VALUES ('42', '服务器', '物理机', '系统性能', '每秒成功读扇区的次数', '', '每秒成功读扇区的次数', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '无');
INSERT INTO `device_item_info` VALUES ('43', '服务器', '物理机', '系统性能', '每秒成功写扇区的次数', '', '每秒成功写扇区的次数', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '无');
INSERT INTO `device_item_info` VALUES ('44', '服务器', '物理机', '系统性能', '花在I/O操作时间', '', '花在I/O操作时间', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '无');
INSERT INTO `device_item_info` VALUES ('45', '服务器', '物理机', '系统性能', 'I/O每次读的时间', '', 'I/O每次读的时间', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '无');
INSERT INTO `device_item_info` VALUES ('46', '服务器', '物理机', '系统性能', 'I/O每次写的时间', '', 'I/O每次写的时间', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '无');
INSERT INTO `device_item_info` VALUES ('47', '服务器', '物理机', '系统性能', '运行的进程', 'proc.num[] ', '查看运行的进程', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '无');
INSERT INTO `device_item_info` VALUES ('48', '服务器', '物理机', '系统性能', '系统垃圾进程', '', '查看系统垃圾进程', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '>5');
INSERT INTO `device_item_info` VALUES ('49', '服务器', '物理机', '系统性能', '日志告警', '', '监控日志error,warning,failure等字段', '中', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}日志出现告警', '是', '异常');
INSERT INTO `device_item_info` VALUES ('50', '服务器', '物理机', '系统性能', '单个进程CPU消耗', '', '单个进程CPU消耗百分比', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('51', '服务器', '物理机', '系统性能', '单个进程内存消耗', '', '单个进程内存消耗', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('52', '服务器', '物理机', '系统性能', '判断文件系统可读写', '', '判断文件系统可读写', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}文件系统只读', '是', '文件系统只读告警');
INSERT INTO `device_item_info` VALUES ('53', '服务器', '物理机', '系统性能', 'I/O等待', '', 'I/O等待', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}IO等待高', '是', '10分钟出现2次以上大于100');
INSERT INTO `device_item_info` VALUES ('54', '服务器', '物理机', '系统性能', '判断磁盘链路', '', '判断磁盘链路有效性', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}磁盘链路错误', '是', '出现错误次数大于0');
INSERT INTO `device_item_info` VALUES ('55', '服务器', '物理机', '系统性能', '系统打开文件数', 'kernel.maxfiles', '查看系统打开文件数', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '无');
INSERT INTO `device_item_info` VALUES ('56', '服务器', '物理机', '主机网卡', '网络连接总数', 'net.if.traffic.total[{#IFNAME}] ', '查看网络连接总数', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '无');
INSERT INTO `device_item_info` VALUES ('57', '服务器', '物理机', '主机网卡', '各种连接数', '', '查看各种连接数', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '无');
INSERT INTO `device_item_info` VALUES ('58', '服务器', '物理机', '主机网卡', '网流入络带宽使用率', 'net.if.in[{#IFNAME}] ', '查看网流入络带宽使用率', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '>80%');
INSERT INTO `device_item_info` VALUES ('59', '服务器', '物理机', '主机网卡', '网络流出带宽使用率', 'net.if.out[{#IFNAME}] ', '查看网络流出带宽使用率', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '>80%');
INSERT INTO `device_item_info` VALUES ('60', '服务器', '物理机', '主机网卡', '网口丢包数', 'net.if.total[{#IFNAME},dropped] ', '监控网口丢包数', '中', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}网口丢包数比上一周期增加100个', '是', '比上一周期增加100个');
INSERT INTO `device_item_info` VALUES ('61', '服务器', '物理机', '主机网卡', '网口错误数', 'net.if.total[{#IFNAME},errors] ', '监控网口错误数', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('62', '服务器', '物理机', '交换分区', 'swap的in交换大小', 'system.swap.in[,pages] ', '查看swap的in交换大小', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}swap的in交换增大', '是', '>10%');
INSERT INTO `device_item_info` VALUES ('63', '服务器', '物理机', '交换分区', 'swap的out交换大小', 'system.swap.out[,pages] ', '查看swap的out交换大小', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}swap的out交换增大', '是', '>10%');
INSERT INTO `device_item_info` VALUES ('64', '服务器', '物理机', '交换分区', 'swap空间使用率', 'system.swap.size[,pused] ', '查看swap空间使用率', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '>20%');
INSERT INTO `device_item_info` VALUES ('65', '服务器', '虚拟机', '硬件状态', 'CPU当前运行状态', '', 'CPU当前运行状态', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}CPU故障', '是', '异常');
INSERT INTO `device_item_info` VALUES ('66', '服务器', '虚拟机', '硬件状态', '内存当前运行状态', '', '内存当前运行状态', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}MEM故障', '是', '异常');
INSERT INTO `device_item_info` VALUES ('67', '服务器', '虚拟机', '硬件状态', '风扇当前运行状态', '', '风扇当前运行状态', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}风扇故障', '是', '异常');
INSERT INTO `device_item_info` VALUES ('68', '服务器', '虚拟机', '硬件状态', '电源当前运行状态', '', '电源当前运行状态', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}电源故障', '是', '异常');
INSERT INTO `device_item_info` VALUES ('69', '服务器', '虚拟机', '硬件状态', '驱动器当前运行状态', '', '驱动器当前运行状态', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}驱动器故障', '是', '异常');
INSERT INTO `device_item_info` VALUES ('70', '服务器', '虚拟机', '硬件状态', '远程管理端口可达性', '', 'ping主机远程管理端口IP地址可达性检查', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}远程管理端口ping不通', '是', '异常');
INSERT INTO `device_item_info` VALUES ('71', '服务器', '虚拟机', '硬件状态', '电压当前运行状态', '', '电压当前运行状态', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}电压出现故障', '是', '异常');
INSERT INTO `device_item_info` VALUES ('72', '服务器', '虚拟机', '硬件状态', '其它固件指标等', '', '其它固件指标等', '', '否', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}固件异常', '否', '');
INSERT INTO `device_item_info` VALUES ('73', '服务器', '虚拟机', '虚拟机', '主机可达性', '无', 'Ping主机操作系统IP地址是否可达', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}操作系统ping不通', '是', '异常');
INSERT INTO `device_item_info` VALUES ('74', '服务器', '虚拟机', '虚拟机', 'CPU使用情况', 'cpu.usage_average', 'CPU 使用情况百分比（%）', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}CPU 使用率高', '是', '>70%');
INSERT INTO `device_item_info` VALUES ('75', '服务器', '虚拟机', '虚拟机', '工作负载', 'cpu.workload ', '工作负载百分比 (%)', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('76', '服务器', '虚拟机', '虚拟机', '数据存储读取速度', 'datastore.read_average ', '收集时间间隔内每秒读取的数据量。', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('77', '服务器', '虚拟机', '虚拟机', '数据存储写入速度', 'datastore.write_average ', '收集时间间隔内每秒写入磁盘的数据量。', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('78', '服务器', '虚拟机', '虚拟机', '磁盘读取速率 (KBps) ', 'disk.read_average ', '收集时间间隔内每秒读取的数据量。', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('79', '服务器', '虚拟机', '虚拟机', '磁盘使用速率 (KBps) ', 'disk.usage_average ', '收集时间间隔内每秒使用速率（以千字节每秒为单位）。', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('80', '服务器', '虚拟机', '虚拟机', '写入速率 (KBps) ', 'disk.write_average ', '收集时间间隔内每秒写入磁盘的数据量。', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('81', '服务器', '虚拟机', '虚拟机', '主机工作负载', 'mem-host.workload ', '主机工作负载 (%)。', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('82', '服务器', '虚拟机', '虚拟机', '内存使用情况', 'mem.usage_average ', '当前正在使用的内存占可用总内存的百分比。', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}内存占用使用高', '是', '>70%');
INSERT INTO `device_item_info` VALUES ('83', '服务器', '虚拟机', '虚拟机', '数据接收速率 (KBps) ', 'net.received_average ', '每秒收到的平均数据量。', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('84', '服务器', '虚拟机', '虚拟机', '使用速率 (KBps) ', 'net.usage_average ', '主机或虚拟机的所有网卡实例所传输和接收的数据总计。', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('85', '服务器', '虚拟机', '虚拟机', '正常运行时间 (秒)', 'sys.osUptime_latest ', '自系统启动后经过的秒数。', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('86', '服务器', '虚拟机', '虚拟机', '日志告警', '', '监控日志error,warning,failure等字段', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}日志出现告警', '是', '');
INSERT INTO `device_item_info` VALUES ('87', '服务器', '虚拟机', '虚拟机', '判断文件系统可读写', '', '判断文件系统可读写', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}文件系统只读', '是', '文件系统只读告警');
INSERT INTO `device_item_info` VALUES ('88', '服务器', '虚拟机', '虚拟机', 'I/O等待', '', 'I/O等待', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}IO等待高', '是', '10分钟出现2次以上大于100');
INSERT INTO `device_item_info` VALUES ('89', '服务器', '虚拟机', '虚拟机', '判断磁盘链路', '', '判断磁盘链路有效性', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}磁盘链路错误', '是', '出现错误次数大于0');
INSERT INTO `device_item_info` VALUES ('90', '服务器', '虚拟机', '虚拟机', '已打开电源已启动的虚拟机', 'sys.poweredOn ', '如果打开电源则为 1，如果关闭 电源则为 0，如果未知则为 -1', '重大', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}虚拟机关闭', '是', '异常');
INSERT INTO `device_item_info` VALUES ('91', '服务器', '虚拟机', '宿主机', '容量利用率', 'cpu.capacity_usagepct_average ', '已用 CPU 容量百分比。', '高', '是', '600s', '是', '是', '是', '是', '', null, '', '是', '');
INSERT INTO `device_item_info` VALUES ('92', '服务器', '虚拟机', '宿主机', '工作负载百分比', 'cpu.workload ', '工作负载百分比', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('93', '服务器', '虚拟机', '宿主机', '读取速度', 'datastore.read_average ', '收集时间间隔内读取的数据量。', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('94', '服务器', '虚拟机', '宿主机', '写入速度', 'datastore.write_average ', '收集时间间隔内写入磁盘的数据量。', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('95', '服务器', '虚拟机', '宿主机', '每秒命令数', 'disk.commandsAveraged_average ', '收集时间间隔内每秒平均发出的命令数量。', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('96', '服务器', '虚拟机', '宿主机', '磁盘命令滞后时间 (毫秒) ', 'disk.totalLatency_average ', '从客户机操作系统角度看，命令耗费的平均时间量。这是内核命令滞后时间与物理设备命令滞后时间之和。', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('97', '服务器', '虚拟机', '宿主机', '使用速率 (KBps) ', 'disk.usage_average ', '主机或虚拟机的所有磁盘实例平均读取的数据和写入的数据总计。', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('98', '服务器', '虚拟机', '宿主机', '已使用的主机内存百分比', 'mem.host_usagePct ', '已使用的主机内存百分比。', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}虚拟机内存高', '是', '>70%');
INSERT INTO `device_item_info` VALUES ('99', '服务器', '虚拟机', '宿主机', '客户机工作负载', 'mem.workload ', '客户机工作负载 (%)', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('100', '服务器', '虚拟机', '宿主机', '数据接收速率 (KBps)', 'net.received_average ', '每秒收到的平均数据量。', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('101', '服务器', '虚拟机', '宿主机', '使用速率 (KBps)', 'net.usage_average ', '主机或虚拟机的所有网卡实例所传输和接收的数据总计。', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('102', '服务器', '虚拟机', '宿主机', '关掉电源的虚拟机数', 'summary.number_poweredOff_vms ', '关掉电源的虚拟机数', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('103', '服务器', '虚拟机', '宿主机', '正在运行的虚拟机数', 'summary.number_running_vms ', '已打开电源的虚拟机数。', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('104', '服务器', '虚拟机', '宿主机', '虚拟机总数', 'summary.total_number_vms ', '虚拟机总数。', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('105', '服务器', '虚拟机', '集群', '容量利用率 (%) ', 'cpu.capacity_usagepct_average ', '已用 CPU 容量百分比。', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}容量利用率已用 CPU 容量百分比。', '是', '>90%');
INSERT INTO `device_item_info` VALUES ('106', '服务器', '虚拟机', '集群', '工作负载 (%)', 'cpu.workload ', '工作负载百分比。', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('107', '服务器', '虚拟机', '集群', '数据存储读取速度', 'datastore.read_average ', '收集时间间隔内每秒读取数据存储的数据量。', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('108', '服务器', '虚拟机', '集群', '数据存储写入速度', 'datastore.write_average ', '收集时间间隔内每秒写入数据存储的数据量。', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('109', '服务器', '虚拟机', '集群', '磁盘读取速率 (KBps) ', 'disk.read_average ', '收集时间间隔内每秒读取的数据量。', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('110', '服务器', '虚拟机', '集群', '磁盘使用速率 (KBps) ', 'disk.usage_average ', '收集时间间隔内每秒使用速率 (KBps) 。', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('111', '服务器', '虚拟机', '集群', '磁盘写入速率 (KBps) ', 'disk.write_average ', '收集时间间隔内每秒写入速率 (KBps) 。', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('112', '服务器', '虚拟机', '集群', '已使用的主机内存百分比', 'mem.host_usagePct ', '已使用的主机内存百分比。', '高', '是', '600s', '是', '是', '是', '是', '', null, '{HOST.IP}容量利用率 (%) 已用 内存 容量百分比。', '是', '>90%');
INSERT INTO `device_item_info` VALUES ('113', '服务器', '虚拟机', '集群', '客户机工作负载', 'mem.workload ', '客户机工作负载 (%)', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('114', '服务器', '虚拟机', '集群', '正在运行的主机数', 'summary.number_running_hosts ', '正在运行的主机数正在运行的主机数。', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('115', '服务器', '虚拟机', '集群', '正在运行的虚拟机数', 'summary.number_running_vms ', '正在运行的虚拟机数正在运行的虚拟机数。', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('116', '服务器', '虚拟机', '集群', '主机总数', 'summary.total_number_hosts ', '主机总数。', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('117', '服务器', '虚拟机', '集群', '虚拟机总数', 'summary.total_number_vms ', '虚拟机总数。', '', '否', '600s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('118', '存储设备', '集中存储', '管理信息', '端口配置状态', '', '管理端口配置状态', '次要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 接口的当前接口状态变为故障状态', '是', '异常');
INSERT INTO `device_item_info` VALUES ('119', '存储设备', '集中存储', '管理信息', '端口操作状态', '', '管理端口操作状态', '次要', '是', '300s', '是', '是', '是', '是', '', null, '', '是', '异常');
INSERT INTO `device_item_info` VALUES ('120', '存储设备', '集中存储', '管理信息', '系统描述', '', '系统设备描述', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('121', '存储设备', '集中存储', '管理信息', '系统名', '', '系统设备名称', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('122', '存储设备', '集中存储', '管理信息', '运行时间', '', '运行时间总和', '重要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 设备被重启', '是', '异常');
INSERT INTO `device_item_info` VALUES ('123', '存储设备', '集中存储', '设备信息\n', '设备unit状态', '', '设备unit状态是否异常', '重要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 存储设备Unit状态警告', '是', '异常');
INSERT INTO `device_item_info` VALUES ('124', '存储设备', '集中存储', '设备信息\n', '设备状态', '', '设备状态是否异常', '重要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 存储设备状态异常', '是', '异常');
INSERT INTO `device_item_info` VALUES ('125', '存储设备', '集中存储', '设备信息\n', '设备子状态', '', '设备子状态是否异常', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('126', '存储设备', '集中存储', '链路状态', '链路端口状态', '', '链路端口状态是否异常', '重要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 链路端口异常', '是', '异常');
INSERT INTO `device_item_info` VALUES ('127', '存储设备', '集中存储', 'Raid群组', '逻辑卷数量', '', '逻辑卷数量', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('128', '存储设备', '集中存储', 'Raid群组', '逻辑卷状态', '', '逻辑卷状态', '重大', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 卷组状态异常Error', '是', '异常');
INSERT INTO `device_item_info` VALUES ('129', '存储设备', '集中存储', 'Raid群组', '逻辑卷容量', '', '逻辑卷容量', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('130', '存储设备', '集中存储', 'Raid群组', 'RAID群组数', '', 'RAID群组数', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('131', '存储设备', '集中存储', 'Raid群组', 'Raid群组状态', '', 'Raid群组状态', '重大', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} Raid群组状态异常Error', '是', '异常');
INSERT INTO `device_item_info` VALUES ('132', '存储设备', '集中存储', 'Raid群组', 'Raid群组容量', '', 'Raid群组容量', '', '否', '300s', '是', '是', '是', '是', '', null, '', '', '');
INSERT INTO `device_item_info` VALUES ('133', '存储设备', '集中存储', '组件状态', '控制背板CPU', '', '控制背板CPU状态', '重要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 控制背板CPU状态状态异常', '是', '异常');
INSERT INTO `device_item_info` VALUES ('134', '存储设备', '集中存储', '组件状态', '控制背板Memory', '', '控制背板Memory状态', '重要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 控制背板Memory状态状态异常', '是', '异常');
INSERT INTO `device_item_info` VALUES ('135', '存储设备', '集中存储', '组件状态', '风扇单元', '', '风扇单元状态', '次要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 风扇单元状态异常', '是', '异常');
INSERT INTO `device_item_info` VALUES ('136', '存储设备', '集中存储', '组件状态', 'I/O模块', '', 'I/O模块状态', '次要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} I/O模块状态异常', '是', '异常');
INSERT INTO `device_item_info` VALUES ('137', '存储设备', '集中存储', 'CPU性能信息', 'CPU繁忙比率', '', 'CPU繁忙比率', '次要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} CPU使用率高', '是', '异常');
INSERT INTO `device_item_info` VALUES ('138', '存储设备', '集中存储', 'Trap告警', '冷启动', '', '设备发生冷启动', '重大', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 设备被冷启动', '是', '异常');
INSERT INTO `device_item_info` VALUES ('139', '存储设备', '集中存储', 'Trap告警', '认证失败', '', '设备发生认证失败', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('140', '存储设备', '集中存储', 'Trap告警', '企业指定告警', '', '设备发生企业指定告警', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('141', '存储设备', '集中存储', 'Trap告警', '设备电源到期', '', '设备电源到期', '次要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 设备电源到期', '是', '异常');
INSERT INTO `device_item_info` VALUES ('142', '存储设备', '集中存储', 'Trap告警', 'trap一般信息', '', 'trap一般信息', '次要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} trap一般信息', '是', '异常');
INSERT INTO `device_item_info` VALUES ('143', '存储设备', '集中存储', 'Trap告警', '设备零件出现故障', '', '设备零件出现故障', '重要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 设备零件出现故障', '是', '异常');
INSERT INTO `device_item_info` VALUES ('144', '存储设备', '集中存储', 'Trap告警', '设备零件需要预防', '', '设备零件需要预防', '次要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 设备零件需要预防', '是', '异常');
INSERT INTO `device_item_info` VALUES ('145', '存储设备', '集中存储', 'Trap告警', '需要维护', '', '设备产生需要维护事件', '次要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 设备需要维护', '是', '异常');
INSERT INTO `device_item_info` VALUES ('146', '存储设备', '集中存储', 'Trap告警', '设备温度过高', '', '设备温度过高', '重要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 设备温度过高', '是', '异常');
INSERT INTO `device_item_info` VALUES ('147', '存储设备', '集中存储', 'Trap告警', '告警信息', '', '告警信息', '重要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} -告警', '是', '异常');
INSERT INTO `device_item_info` VALUES ('148', '存储设备', '集中存储', 'Trap告警', '告警系统组件', '', '告警系统组件', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('149', '存储设备', '集中存储', 'Trap告警', '告警详细信息', '', '告警详细信息', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('150', '存储设备', '集中存储', 'Trap告警', '节点标识', '', '节点标识', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('151', '存储设备', '集中存储', 'Trap告警', '告警级别', '', '告警级别', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('152', '存储设备', '集中存储', 'Trap告警', '告警时间', '', '告警时间', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('153', '存储设备', '集中存储', 'Trap告警', '告警识别号', '', '告警识别号', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('154', '存储设备', '集中存储', 'Trap告警', '告警消息代码', '', '告警消息代码', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('155', '存储设备', '集中存储', 'Trap告警', '当前告警状态', '', '当前告警状态', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('156', '存储设备', '集中存储', '控制器硬件状态', '控制器序列号', '', '控制器序列号', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('157', '存储设备', '集中存储', '控制器硬件状态', '控制器处理器状态', '', '控制器处理器状态', '次要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 控制器处理器状态异常', '是', '异常');
INSERT INTO `device_item_info` VALUES ('158', '存储设备', '集中存储', '控制器硬件状态', '控制器内部总线状态', '', '控制器内部总线状态', '次要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 内部总线状态异常', '是', '异常');
INSERT INTO `device_item_info` VALUES ('159', '存储设备', '集中存储', '控制器硬件状态', '控制器缓存状态', '', '控制器缓存状态', '次要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 控制器缓存状态异常', '是', '异常');
INSERT INTO `device_item_info` VALUES ('160', '存储设备', '集中存储', '控制器硬件状态', '控制器共享内存状态', '', '控制器共享内存状态', '次要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 控制器共享内存状态异常', '是', '异常');
INSERT INTO `device_item_info` VALUES ('161', '存储设备', '集中存储', '控制器硬件状态', '控制器电源状态', '', '控制器电源状态', '次要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 控制器电源状态异常', '是', '异常');
INSERT INTO `device_item_info` VALUES ('162', '存储设备', '集中存储', '控制器硬件状态', '控制器电池状态', '', '控制器电池状态', '次要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 控制器电池状态异常', '是', '异常');
INSERT INTO `device_item_info` VALUES ('163', '存储设备', '集中存储', '控制器硬件状态', '控制器风扇状态', '', '控制器风扇状态', '次要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 控制器风扇状态异常', '是', '异常');
INSERT INTO `device_item_info` VALUES ('164', '存储设备', '集中存储', '控制器硬件状态', '控制器环境状态', '', '控制器环境状态', '次要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 控制器环境状态异常', '是', '异常');
INSERT INTO `device_item_info` VALUES ('165', '存储设备', '集中存储', '扩展柜硬件状态', '扩展柜序列号', '', '扩展柜序列号', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('166', '存储设备', '集中存储', '扩展柜硬件状态', '扩展柜电源状态', '', '扩展柜电源状态', '次要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 扩展柜电源状态异常', '是', '异常');
INSERT INTO `device_item_info` VALUES ('167', '存储设备', '集中存储', '扩展柜硬件状态', '扩展柜风扇状态', '', '扩展柜风扇状态', '次要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 扩展柜风扇状态异常', '是', '异常');
INSERT INTO `device_item_info` VALUES ('168', '存储设备', '集中存储', '扩展柜硬件状态', '扩展柜环境状态', '', '扩展柜环境状态', '次要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 扩展柜环境状态异常', '是', '异常');
INSERT INTO `device_item_info` VALUES ('169', '存储设备', '集中存储', '扩展柜硬件状态', '扩展柜驱动器状态', '', '扩展柜驱动器状态', '次要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 扩展柜驱动器状态异常', '是', '异常');
INSERT INTO `device_item_info` VALUES ('170', '存储设备', '分布式存储', '集群信息\n', '集群名称', '', '集群名称', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('171', '存储设备', '分布式存储', '集群信息\n', '集群系统健康状态', '', '集群系统健康状态', '重大', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 集群健康状态变为故障', '是', '异常');
INSERT INTO `device_item_info` VALUES ('172', '存储设备', '分布式存储', '集群信息\n', '集群系统运行状态', '', '集群系统运行状态', '重大', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 集群运行状态变为未运行', '是', '异常');
INSERT INTO `device_item_info` VALUES ('173', '存储设备', '分布式存储', '集群信息\n', '系统提供总的可用容量', '', '系统提供总的可用容量', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('174', '存储设备', '分布式存储', '集群信息\n', '系统已用容量', '', '系统已用容量', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('175', '存储设备', '分布式存储', '集群信息\n', '系统剩余容量', '', '系统剩余容量', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('176', '存储设备', '分布式存储', '集群节点信息', '节点名称', '', '节点名称', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('177', '存储设备', '分布式存储', '集群节点信息', '节点健康状态', '', '节点健康状态', '重大', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 集群节点健康状态异常', '是', '异常');
INSERT INTO `device_item_info` VALUES ('178', '存储设备', '分布式存储', '集群节点信息', '节点运行状态', '', '节点运行状态', '重大', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 集群节点处于未运行状态离线', '是', '异常');
INSERT INTO `device_item_info` VALUES ('179', '存储设备', '分布式存储', 'cpu', 'NAS系统CPU_ID', '', 'NAS系统CPU_ID', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('180', '存储设备', '分布式存储', 'cpu', 'cpu健康状态', '', 'cpu健康状态', '次要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} CPU健康状态异常', '是', '异常');
INSERT INTO `device_item_info` VALUES ('181', '存储设备', '分布式存储', 'cpu', 'cpu核心温度', '', 'cpu核心温度', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('182', '存储设备', '分布式存储', 'cpu', 'cpu当前运行频率', '', 'cpu当前运行频率', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('183', '存储设备', '分布式存储', 'cpu', 'cpu温度状态', '', 'cpu温度状态', '次要', '是', '300s', '是', '是', '是', '是', '', null, 'HOST.IP} CPU温度异常', '是', '异常');
INSERT INTO `device_item_info` VALUES ('184', '存储设备', '分布式存储', 'cpu', 'CPU核数', '', 'CPU核数', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('185', '存储设备', '分布式存储', '文件系统', '文件系统服务名称', '', '文件系统服务名称', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('186', '存储设备', '分布式存储', '文件系统', '健康状态', '', '健康状态', '重大', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 文件系统-健康状态异常', '是', '异常');
INSERT INTO `device_item_info` VALUES ('187', '存储设备', '分布式存储', '文件系统', '运行状态', '', '运行状态', '重大', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 文件系统-运行状态异常', '是', '异常');
INSERT INTO `device_item_info` VALUES ('188', '存储设备', '分布式存储', '文件系统', '系统提供总的可用容量', '', '系统提供总的可用容量', '重要', '是', '300s', '是', '是', '是', '是', '', null, '', '是', '异常');
INSERT INTO `device_item_info` VALUES ('189', '存储设备', '分布式存储', '文件系统', '系统已用容量', '', '系统已用容量', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('190', '存储设备', '分布式存储', '文件系统', '系统剩余容量', '', '系统剩余容量', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('191', '存储设备', '分布式存储', '内存', '容量，单位Byte', '', '容量，单位Byte', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('192', '存储设备', '分布式存储', '内存', '内存ID', '', '内存ID', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('193', '存储设备', '分布式存储', '磁盘\n', '磁盘健康状态', '', '磁盘健康状态', '次要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} DISK健康状态异常', '是', '异常');
INSERT INTO `device_item_info` VALUES ('194', '存储设备', '分布式存储', '磁盘\n', '磁盘运行状态', '', '磁盘运行状态', '次要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} DISK处于离线状态', '是', '异常');
INSERT INTO `device_item_info` VALUES ('195', '存储设备', '分布式存储', '磁盘\n', '磁盘温度', '', '磁盘温度', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('196', '存储设备', '分布式存储', '磁盘\n', '磁盘转速', '', '磁盘转速', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('197', '存储设备', '分布式存储', '磁盘\n', '磁盘接口带宽', '', '磁盘接口带宽', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('198', '存储设备', '分布式存储', '磁盘\n', '磁盘槽位ID', '', '磁盘槽位ID', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('199', '存储设备', '分布式存储', '网络', '网口速率', '', '网口速率', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('200', '存储设备', '分布式存储', '网络', '网口健康状态', '', '网口健康状态', '次要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 网口健康状态异常', '是', '异常');
INSERT INTO `device_item_info` VALUES ('201', '存储设备', '分布式存储', '网络', '网口运行状态', '', '网口运行状态', '次要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 网口处于未连接状态', '是', '异常');
INSERT INTO `device_item_info` VALUES ('202', '存储设备', '分布式存储', '网络', '网口编号', '', '网口编号', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('203', '存储设备', '分布式存储', '文件数', '当前文件数', '', '当前文件数', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('204', '存储设备', '分布式存储', '文件数', '最大文件数', '', '最大文件数', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('205', '存储设备', '分布式存储', '文件数', '文件系统ID', '', '文件系统ID', '', '否', '300s', '是', '是', '是', '是', '', null, '', '否', '');
INSERT INTO `device_item_info` VALUES ('206', '存储设备', '分布式存储', 'Trap告警', '告警信息', '', '告警信息', '重要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} Trap告警', '是', '异常');
INSERT INTO `device_item_info` VALUES ('207', '存储设备', '分布式存储', 'Trap告警', '其它Trap告警', '', '其它Trap告警', '重要', '是', '300s', '是', '是', '是', '是', '', null, '{HOST.IP} 其它Trap告警 ', '是', '异常');
INSERT INTO `device_item_info` VALUES ('208', '网络设备', '交换机', '管理接入交换机', '风扇故障', 'EntityFanState', '电源模块风扇异常或停止', '高', '是', '', '设备', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 风扇异常或停止', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('209', '网络设备', '交换机', '管理接入交换机', '电源故障', 'BoardCurrentPower', '电源设备告警', '高', '是', '', '设备', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 电源设备告警', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('210', '网络设备', '交换机', '管理接入交换机', '设备端口状态', 'ifAdminStatus', '交换机端口状态由UP变为DOWN', '中', '是', '', '设备', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 交换机端口状态由UP变为DOWN', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('211', '网络设备', '交换机', '管理接入交换机', '设备不可达率', 'neContrlableRate', '交换机60S内不可达比率', '重大', '是', '超过5%', '每1分钟检查', 'Simple Check', '是', '是', '是', '', '{HOST.IP} 设备60S内不可达比率超过5%', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('212', '网络设备', '交换机', '管理接入交换机', '设备不可达', 'icmpping', 'Ping网络设备的活动性', '重大', '是', '', '每1分钟检查', 'SNMP', '是', '是', '是', '', '{HOST.IP} PING不可达', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('213', '网络设备', '交换机', '管理接入交换机', '设备CPU利用率', 'cpuUsage', '交换机CPU利用率超阈值告警', '高', '是', '超过50%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 交换机CPU利用率超过80%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('214', '网络设备', '交换机', '管理接入交换机', '设备内存利用率', 'memUsage', '交换机内存利用率超阈值告警', '高', '是', '超过90%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 交换机内存利用率超过90%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('215', '网络设备', '交换机', '接入层交换机', '风扇故障', 'EntityFanState', '电源模块风扇异常或停止', '高', '是', '', '设备', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 风扇异常或停止', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('216', '网络设备', '交换机', '接入层交换机', '电源故障', 'BoardCurrentPower', '电源设备告警', '高', '是', '', '设备', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 电源设备告警', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('217', '网络设备', '交换机', '接入层交换机', '设备端口状态', 'ifOperStatus', '端口的物理状态由UP变为DOWN告警', '高', '是', '', '设备', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 端口状态由UP变为DOWN', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('218', '网络设备', '交换机', '接入层交换机', '设备不可达率', 'neContrlableRate', '交换机60S内不可达比率', '重大', '是', '超过5%', '每1分钟检查', 'Simple Check', '是', '是', '是', '', '{HOST.IP} 设备60S内不可达比率超过5%', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('219', '网络设备', '交换机', '接入层交换机', '设备不可达', 'icmpping', 'Ping设备的活动性', '重大', '是', '', '每1分钟检查', 'SNMP', '是', '是', '是', '', '{HOST.IP} PING不可达', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('220', '网络设备', '交换机', '接入层交换机', '设备响应时间', 'nePingRespTime', 'ping设备的响应时间', '中', '是', '超过200ms', '每1分钟检查', 'Simple Check', '是', '是', '是', '', '{HOST.IP} 设备60S内响应时间超过200ms', '性能阈值告警', '');
INSERT INTO `device_item_info` VALUES ('221', '网络设备', '交换机', '接入层交换机', '系统运行时间', 'sysUpTime', '检查交换机运行系统运行时间', '重大', '是', '运行时间少于1800S', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 设备运行时间少于1800S', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('222', '网络设备', '交换机', '接入层交换机', '设备（含单板）CPU利用率', 'cpuUsage', '交换机CPU利用率超阈值告警', '重大', '是', '超过50%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} CPU利用率超80%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('223', '网络设备', '交换机', '接入层交换机', '设备（含单板）内存利用率', 'memUsage', '交换机内存利用率超阈值告警', '高', '是', '超过90%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 内存利用率超90%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('224', '网络设备', '交换机', '接入层交换机', '接口发送包错误数', 'ifOutErrors', '统计时间内接口发送包Errors错误数', '高', '是', '大于10', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口发送包错误数大于10个/秒', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('225', '网络设备', '交换机', '接入层交换机', '接口接收包错误数', 'ifInErrors', '统计时间内接口接收包Errors错误数', '高', '是', '大于10', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口接收包错误数大于10个/秒', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('226', '网络设备', '交换机', '接入层交换机', '接口入口带宽利用率', 'ifInBandRate', '交换机接口入口带宽利用率超阈值告警', '高', '是', '超过70%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口入口带宽利用率超70%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('227', '网络设备', '交换机', '接入层交换机', '接口出口带宽利用率', 'ifOutBandRate', '交换机接口出口带宽利用率超阈值告警', '高', '是', '超过70%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} CMNET接口出口带宽利用率超70%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('228', '网络设备', '交换机', '接入层交换机', '接口丢弃接收包数', 'ifInDiscards', '统计时间内接口丢弃接收包数', '高', '是', '大于10', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口丢弃接收包数大于10个/秒', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('229', '网络设备', '交换机', '接入层交换机', '接口丢弃发送包数', 'ifOutDiscards', '统计时间内接口丢弃发送包数', '高', '是', '大于10', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口丢弃发送包数大于10个/秒', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('230', '网络设备', '交换机', '接入层交换机', '接收广播包速率', 'ifHCInBroadtPktSpeed', '统计时间内接收广播包速率', '高', '是', '大于60个', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口接收接收广播包速率大于60个/秒', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('231', '网络设备', '交换机', '接入层交换机', '发送广播包速率', 'ifHCOutBroadPktSpeed', '统计时间内发送广播包速率', '高', '是', '大于300个', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口接收接收广播包速率大于300个/秒', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('232', '网络设备', '交换机', '服务节点交换机', '风扇故障', 'EntityFanState', '电源模块风扇异常或停止', '高', '是', '', '设备', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 风扇异常或停止', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('233', '网络设备', '交换机', '核心交换机', '风扇故障', 'EntityFanState', '电源模块风扇异常或停止', '高', '是', '', '设备', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 风扇异常或停止', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('234', '网络设备', '交换机', '服务节点交换机', '电源故障', 'BoardCurrentPower', '电源设备告警', '重大', '是', '', '设备', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 电源设备告警', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('235', '网络设备', '交换机', '核心交换机', '电源故障', 'BoardCurrentPower', '电源设备告警', '重大', '是', '', '设备', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 电源设备告警', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('236', '网络设备', '交换机', '服务节点交换机', '设备端口状态', 'ifOperStatus', '端口的物理状态由UP变为DOWN告警', '重大', '是', '', '设备', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 端口状态由UP变为DOWN', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('237', '网络设备', '交换机', '核心交换机', '设备端口状态', 'ifOperStatus', '端口的物理状态由UP变为DOWN告警', '重大', '是', '', '设备', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 端口状态由UP变为DOWN', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('238', '网络设备', '交换机', '服务节点交换机', '设备不可达率', 'neContrlableRate', '交换机60S内不可达比率', '重大', '是', '超过5%', '每1分钟检查', 'Simple Check', '是', '是', '是', '', '{HOST.IP} 设备60S内不可达比率超过5%', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('239', '网络设备', '交换机', '核心交换机', '设备不可达率', 'neContrlableRate', '交换机60S内不可达比率', '重大', '是', '超过5%', '每1分钟检查', 'Simple Check', '是', '是', '是', '', '{HOST.IP} 设备60S内不可达比率超过5%', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('240', '网络设备', '交换机', '服务节点交换机', '设备不可达', 'icmpping', 'Ping设备的活动性', '重大', '是', '', '每1分钟检查', 'SNMP', '是', '是', '是', '', '{HOST.IP} PING不可达', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('241', '网络设备', '交换机', '核心交换机', '设备不可达', 'icmpping', 'Ping设备的活动性', '重大', '是', '', '每1分钟检查', 'SNMP', '是', '是', '是', '', '{HOST.IP} PING不可达', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('242', '网络设备', '交换机', '服务节点交换机', '设备响应时间', 'nePingRespTime', 'ping设备的响应时间', '高', '是', '超过200ms', '每1分钟检查', 'SNMP', '是', '是', '是', '', '{HOST.IP} 设备60S内响应时间超过200ms', '性能阈值告警', '');
INSERT INTO `device_item_info` VALUES ('243', '网络设备', '交换机', '核心交换机', '设备响应时间', 'nePingRespTime', 'ping设备的响应时间', '高', '是', '超过200ms', '每1分钟检查', 'SNMP', '是', '是', '是', '', '{HOST.IP} 设备60S内响应时间超过200ms', '性能阈值告警', '');
INSERT INTO `device_item_info` VALUES ('244', '网络设备', '交换机', '服务节点交换机', '设备（含单板）CPU利用率', 'cpuUsage', '交换机CPU利用率超阈值告警', '重大', '是', '超过50%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} CPU利用率超80%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('245', '网络设备', '交换机', '核心交换机', '设备（含单板）CPU利用率', 'cpuUsage', '交换机CPU利用率超阈值告警', '重大', '是', '超过50%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} CPU利用率超80%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('246', '网络设备', '交换机', '服务节点交换机', '设备（含单板）内存利用率', 'memUsage', '交换机内存利用率超阈值告警', '重大', '是', '超过90%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 内存利用率超90%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('247', '网络设备', '交换机', '核心交换机', '设备（含单板）内存利用率', 'memUsage', '交换机内存利用率超阈值告警', '重大', '是', '超过90%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 内存利用率超90%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('248', '网络设备', '交换机', '服务节点交换机', '系统运行时间', 'sysUpTime', '检查交换机运行系统运行时间', '重大', '是', '运行时间少于1800S', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 设备运行时间少于1800S', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('249', '网络设备', '交换机', '核心交换机', '系统运行时间', 'sysUpTime', '检查交换机运行系统运行时间', '重大', '是', '运行时间少于1800S', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 设备运行时间少于1800S', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('250', '网络设备', '交换机', '服务节点交换机', '接口发送包错误数', 'ifOutErrors', '统计时间内接口发送包Errors错误数', '高', '是', '大于10', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口发送包错误数大于10个/秒', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('251', '网络设备', '交换机', '核心交换机', '接口发送包错误数', 'ifOutErrors', '统计时间内接口发送包Errors错误数', '高', '是', '大于10', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口发送包错误数大于10个/秒', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('252', '网络设备', '交换机', '服务节点交换机', '接口接收包错误数', 'ifInErrors', '统计时间内接口接收包Errors错误数', '高', '是', '大于10', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口接收包错误数大于10个/秒', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('253', '网络设备', '交换机', '核心交换机', '接口接收包错误数', 'ifInErrors', '统计时间内接口接收包Errors错误数', '高', '是', '大于10', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口接收包错误数大于10个/秒', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('254', '网络设备', '交换机', '服务节点交换机', '接口接收速率', 'ifHCInOctetsSpeed', '交换机接口接收速率超阈值告警', '高', '是', '变化50%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口接收流量5分钟内突然变化50%告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('255', '网络设备', '交换机', '核心交换机', '接口接收速率', 'ifHCInOctetsSpeed', '交换机接口接收速率超阈值告警', '高', '是', '变化50%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口接收流量5分钟内突然变化50%告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('256', '网络设备', '交换机', '服务节点交换机', '接口发送速率', 'ifHCOutOctetsSpeed', '交换机发送接收速率超阈值告警', '高', '是', '变化50%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口发送流量5分钟内突然变化50%告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('257', '网络设备', '交换机', '核心交换机', '接口发送速率', 'ifHCOutOctetsSpeed', '交换机发送接收速率超阈值告警', '高', '是', '变化50%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口发送流量5分钟内突然变化50%告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('258', '网络设备', '交换机', '服务节点交换机', '接口丢弃接收包数', 'ifInDiscards', '统计时间内接口丢弃接收包数', '高', '是', '大于10', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口丢弃接收包数大于10个/秒', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('259', '网络设备', '交换机', '核心交换机', '接口丢弃接收包数', 'ifInDiscards', '统计时间内接口丢弃接收包数', '高', '是', '大于10', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口丢弃接收包数大于10个/秒', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('260', '网络设备', '交换机', '服务节点交换机', '接口丢弃发送包数', 'ifOutDiscards', '统计时间内接口丢弃发送包数', '高', '是', '大于10', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口丢弃发送包数大于10个/秒', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('261', '网络设备', '交换机', '核心交换机', '接口丢弃发送包数', 'ifOutDiscards', '统计时间内接口丢弃发送包数', '高', '是', '大于10', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口丢弃发送包数大于10个/秒', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('262', '网络设备', '交换机', '服务节点交换机', '接口入口带宽利用率', 'ifInBandRate', '交换机接口入口带宽利用率超阈值告警', '高', '是', '超过70%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} CMNET接口入口带宽利用率超90%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('263', '网络设备', '交换机', '核心交换机', '接口入口带宽利用率', 'ifInBandRate', '交换机接口入口带宽利用率超阈值告警', '高', '是', '超过70%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} CMNET接口入口带宽利用率超90%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('264', '网络设备', '交换机', '服务节点交换机', '接口出口带宽利用率', 'ifOutBandRate', '交换机接口出口带宽利用率超阈值告警', '高', '是', '超过70%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} CMNET接口出口带宽利用率超90%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('265', '网络设备', '交换机', '核心交换机', '接口出口带宽利用率', 'ifOutBandRate', '交换机接口出口带宽利用率超阈值告警', '高', '是', '超过70%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} CMNET接口出口带宽利用率超90%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('266', '网络设备', '交换机', '服务节点交换机', '虚接口接收速率', 'ifHCInOctetsSpeed', '交换机接口接收速率', '高', '是', '根据现网需要调整阈值', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 虚接口接收流量突然变化告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('267', '网络设备', '交换机', '核心交换机', '虚接口接收速率', 'ifHCInOctetsSpeed', '交换机接口接收速率', '高', '是', '根据现网需要调整阈值', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 虚接口接收流量突然变化告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('268', '网络设备', '交换机', '服务节点交换机', '虚接口发送速率', 'ifHCOutOctetsSpeed', '交换机发送接收速率', '高', '是', '根据现网需要调整阈值', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 虚接口发送流量突然变化告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('269', '网络设备', '交换机', '核心交换机', '虚接口发送速率', 'ifHCOutOctetsSpeed', '交换机发送接收速率', '高', '是', '根据现网需要调整阈值', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 虚接口发送流量突然变化告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('270', '网络设备', '交换机', '服务节点交换机', 'OSPF邻居状态改变', '', 'OSPF邻居状态发生变化', '高', '是', '', '每5分钟主动轮询获取', 'SNMP/SNMPTrap', '', '', '是', '', '{HOST.IP} OSPF邻居状态发生变化，可能是由于该邻居所在的接口状态发生变化', '通信告警', '是');
INSERT INTO `device_item_info` VALUES ('271', '网络设备', '交换机', '核心交换机', 'OSPF邻居状态改变', '', 'OSPF邻居状态发生变化', '高', '是', '', '每5分钟主动轮询获取', 'SNMP/SNMPTrap', '', '', '是', '', '{HOST.IP} OSPF邻居状态发生变化，可能是由于该邻居所在的接口状态发生变化', '通信告警', '是');
INSERT INTO `device_item_info` VALUES ('272', '网络设备', '交换机', '服务节点交换机', 'OSPF V3 邻居状态改变', '', 'OSPF V3邻居状态发生变化', '高', '是', '', '每5分钟主动轮询获取', 'SNMP/SNMPTrap', '', '', '是', '', '{HOST.IP} OSPF V3邻居状态发生变化，可能是由于该邻居所在的接口状态发生变化', '通信告警', '是');
INSERT INTO `device_item_info` VALUES ('273', '网络设备', '交换机', '核心交换机', 'OSPF V3 邻居状态改变', '', 'OSPF V3邻居状态发生变化', '高', '是', '', '每5分钟主动轮询获取', 'SNMP/SNMPTrap', '', '', '是', '', '{HOST.IP} OSPF V3邻居状态发生变化，可能是由于该邻居所在的接口状态发生变化', '通信告警', '是');
INSERT INTO `device_item_info` VALUES ('274', '网络设备', '交换机', '服务节点交换机', 'OSPF虚邻居状态改变', '', 'OSPF虚邻居状态改变', '高', '是', '', '每5分钟主动轮询获取', 'SNMP/SNMPTrap', '', '', '是', '', '{HOST.IP} OSPF虚连接邻居状态发生变化，当虚连接接口状态变化会引起虚连接邻居状态变化', '通信告警', '是');
INSERT INTO `device_item_info` VALUES ('275', '网络设备', '交换机', '核心交换机', 'OSPF虚邻居状态改变', '', 'OSPF虚邻居状态改变', '高', '是', '', '每5分钟主动轮询获取', 'SNMP/SNMPTrap', '', '', '是', '', '{HOST.IP} OSPF虚连接邻居状态发生变化，当虚连接接口状态变化会引起虚连接邻居状态变化', '通信告警', '是');
INSERT INTO `device_item_info` VALUES ('276', '网络设备', '交换机', '服务节点交换机', 'BGP状态改变告警', '', 'BGP状态机的状态值从高值状态变为低值状态', '高', '是', '', '每5分钟主动轮询获取', 'SNMP/SNMPTrap', '', '', '是', '', '{HOST.IP} 当前BGP状态是Openconfirm状态或Established状态', '通信告警', '是');
INSERT INTO `device_item_info` VALUES ('277', '网络设备', '交换机', '核心交换机', 'BGP状态改变告警', '', 'BGP状态机的状态值从高值状态变为低值状态', '高', '是', '', '每5分钟主动轮询获取', 'SNMP/SNMPTrap', '', '', '是', '', '{HOST.IP} 当前BGP状态是Openconfirm状态或Established状态', '通信告警', '是');
INSERT INTO `device_item_info` VALUES ('278', '网络设备', '交换机', '服务节点交换机', 'IS-IS邻接改变', '', 'IS-IS邻接改变', '高', '', '', '', 'SNMP', '', '', '是', '', '{HOST.IP} IS-IS邻接改变，置不一致或邻居所在的接口状态发生变化', '设备告警', '');
INSERT INTO `device_item_info` VALUES ('279', '网络设备', '交换机', '核心交换机', 'IS-IS邻接改变', '', 'IS-IS邻接改变', '高', '', '', '', 'SNMP', '', '', '是', '', '{HOST.IP} IS-IS邻接改变，置不一致或邻居所在的接口状态发生变化', '设备告警', '');
INSERT INTO `device_item_info` VALUES ('280', '网络设备', '路由器', '互联网路由器', '风扇故障', 'EntityFanState', '电源模块风扇异常或停止', '高', '是', '', '设备', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 风扇异常或停止', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('281', '网络设备', '路由器', '互联网路由器', '电源故障', 'BoardCurrentPower', '电源设备告警', '重大', '是', '', '设备', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 电源设备告警', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('282', '网络设备', '路由器', '互联网路由器', '设备端口状态', 'ifOperStatus', '端口的物理状态由UP变为DOWN告警', '重大', '是', '', '设备', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 端口状态由UP变为DOWN', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('283', '网络设备', '路由器', '互联网路由器', '设备不可达率', 'neContrlableRate', '设备60S内不可达比率', '重大', '是', '超过5%', '每1分钟检查', 'Simple Check', '是', '是', '是', '', '{HOST.IP} 设备60S内不可达比率超过5%', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('284', '网络设备', '路由器', '互联网路由器', '设备不可达', 'icmpping', 'Ping设备的活动性', '重大', '是', '', '每1分钟检查', 'Simple Check', '是', '是', '是', '', '{HOST.IP} PING不可达', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('285', '网络设备', '路由器', '互联网路由器', '设备响应时间', 'nePingRespTime', 'ping设备的响应时间', '重大', '是', '超过200ms', '每1分钟检查', 'SNMP', '是', '是', '是', '', '{HOST.IP} 设备60S内响应时间超过200ms', '性能阈值告警', '否');
INSERT INTO `device_item_info` VALUES ('286', '网络设备', '路由器', '互联网路由器', '系统运行时间', 'sysUpTime', '检查交换机运行系统运行时间', '重大', '是', '运行时间少于1800S', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 设备运行时间少于1800S', '设备告警', '否');
INSERT INTO `device_item_info` VALUES ('287', '网络设备', '路由器', '互联网路由器', '设备（含单板）CPU利用率', 'cpuUsage', '路由器CPU利用率超阈值告警', '重大', '是', '超过50%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} CPU利用率超80%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('288', '网络设备', '路由器', '互联网路由器', '设备（含单板）内存利用率', 'memUsage', '路由器内存利用率超阈值告警', '重大', '是', '超过90%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 内存利用率超90%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('289', '网络设备', '路由器', '互联网路由器', '接口接收速率', 'ifHCInOctetsSpeed', '路由器接口接收速率超阈值告警', '高', '是', '变化50%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口接收流量5分钟内突然变化50%告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('290', '网络设备', '路由器', '互联网路由器', '接口发送速率', 'ifHCOutOctetsSpeed', '路由器发送接收速率超阈值告警', '高', '是', '变化50%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口发送流量5分钟内突然变化50%告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('291', '网络设备', '路由器', '互联网路由器', '接口入口带宽利用率', 'ifInBandRate', 'CMNET接口入口带宽利用率超阈值告警', '重大', '是', '超过70%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} CMNET接口入口带宽利用率超70%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('292', '网络设备', '路由器', '互联网路由器', '接口出口带宽利用率', 'ifOutBandRate', 'CMNET接口出口带宽利用率超阈值告警', '重大', '是', '超过70%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} CMNET接口出口带宽利用率超70%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('293', '网络设备', '路由器', '互联网路由器', '接口发送包错误数', 'ifOutErrors', '统计时间内接口发送包Errors错误数', '高', '是', '大于10', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口发送包错误数大于10个/秒', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('294', '网络设备', '路由器', '互联网路由器', '接口接收包错误数', 'ifInErrors', '统计时间内接口接收包Errors错误数', '高', '是', '大于10', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口接收包错误数大于10个/秒', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('295', '网络设备', '路由器', '互联网路由器', 'OSPF邻居状态改变', '', 'OSPF邻居状态发生变化', '高', '是', '', '每5分钟主动轮询获取', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 路由器OSPF邻居状态异常', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('296', '网络设备', '路由器', '互联网路由器', 'OSPF V3 邻居状态改变', '', 'OSPF V3邻居状态发生变化', '高', '是', '', '每5分钟主动轮询获取', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} OSPF V3邻居状态发生变化，可能是由于该邻居所在的接口状态发生变化', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('297', '网络设备', '路由器', '互联网路由器', 'BGP邻居状态', '', 'BGP邻居状态发生变化', '高', '是', '', '每5分钟主动轮询获取', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 路由器BGP邻居状态异常', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('298', '网络设备', '防火墙', '防火墙', '风扇故障', 'EntityFanState', '电源模块风扇异常或停止', '高', '是', '', '设备', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 风扇异常或停止', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('299', '网络设备', '防火墙', '防火墙', '电源故障', 'BoardCurrentPower', '电源设备告警', '重大', '是', '', '设备', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 电源设备告警', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('300', '网络设备', '防火墙', '防火墙', '设备端口状态', 'ifOperStatus', '端口的物理状态由UP变为DOWN告警', '重大', '是', '', '设备', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 端口状态由UP变为DOWN', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('301', '网络设备', '防火墙', '防火墙', '设备不可达率', 'neContrlableRate', '设备60S内不可达比率', '重大', '是', '超过5%', '每1分钟检查', 'Simple Check', '是', '是', '是', '', '{HOST.IP} 设备60S内不可达比率超过5%', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('302', '网络设备', '防火墙', '防火墙', '设备不可达', 'icmpping', 'Ping设备的活动性', '重大', '是', '', '每1分钟检查', 'Simple Check', '是', '是', '是', '', '{HOST.IP} PING不可达', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('303', '网络设备', '防火墙', '防火墙', '设备响应时间', 'nePingRespTime', 'ping设备的响应时间', '高', '是', '超过200ms', '每1分钟检查', 'SNMP', '是', '是', '是', '', '{HOST.IP} 设备60S内响应时间超过200ms', '性能阈值告警', '');
INSERT INTO `device_item_info` VALUES ('304', '网络设备', '防火墙', '防火墙', '设备（含单板）CPU利用率', 'cpuUsage', '防火墙CPU利用率超阈值告警', '重大', '是', '超过50%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} CPU利用率超80%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('305', '网络设备', '防火墙', '防火墙', '设备（含单板）内存利用率', 'memUsage', '防火墙内存利用率超阈值告警', '重大', '是', '超过90%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 内存利用率超90%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('306', '网络设备', '防火墙', '防火墙', '系统运行时间', 'sysUpTime', '检查交换机运行系统运行时间', '重大', '是', '运行时间少于1800S', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 设备运行时间少于1800S', '设备告警', '否');
INSERT INTO `device_item_info` VALUES ('307', '网络设备', '防火墙', '防火墙', '双机热备心跳状态异常', 'hwHrpState ', '防火墙双机热备心跳状态异常', '重大', '是', '', '每1分钟检查', 'SNMP', '是', '是', '是', '', '{HOST.IP} 防火墙双机热备心跳状态异常', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('308', '网络设备', '防火墙', '防火墙', '检测发现双主故障', 'hwHrpState ', '检测发现防火墙双主故障', '重大', '是', '', '每1分钟检查', 'SNMP', '是', '是', '是', '', '{HOST.IP} 检测发现防火墙双主故障', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('309', '网络设备', '防火墙', '防火墙', '防火墙主备倒换告警', '', '防火墙主备倒换告警', '重大', '是', '', '每1分钟检查', '是', '是', '是', '是', '', '{HOST.IP} 防火墙主备倒换告警', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('310', '网络设备', '防火墙', '防火墙', '吞吐量', 'hwSecStatDeviceThroughput ', '防火墙整机吞吐量', '重大', '是', '超过70%', '每1分钟检查', 'SNMP', '是', '是', '是', '', '{HOST.IP} 防火墙整机吞吐量超过70%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('311', '网络设备', '防火墙', '防火墙', '接口发送包错误数', 'ifOutErrors', '统计时间内接口发送包Errors错误数', '高', '是', '大于10', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口发送包错误数大于10个/秒', '性能阈值告警', '否');
INSERT INTO `device_item_info` VALUES ('312', '网络设备', '防火墙', '防火墙', '接口接收包错误数', 'ifInErrors', '统计时间内接口接收包Errors错误数', '高', '是', '大于10', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口接收包错误数大于10个/秒', '性能阈值告警', '否');
INSERT INTO `device_item_info` VALUES ('313', '网络设备', '防火墙', '防火墙', '接口入口带宽利用率', 'ifInOctets.pused[{#SNMPVALUE}] ', '接口入口带宽利用率超阈值告警', '高', '是', '超过90%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} CMNET接口入口带宽利用率超90%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('314', '网络设备', '防火墙', '防火墙', '接口出口带宽利用率', 'ifOutOctets.Pused[{#SNMPVALUE}] ', '接口出口带宽利用率超阈值告警', '高', '是', '超过90%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} CMNET接口出口带宽利用率超90%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('315', '网络设备', '防火墙', '防火墙', '接口接收速率', 'ifHCInOctetsSpeed', '防火墙接口接收速率超阈值告警', '高', '是', '变化50%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口接收流量5分钟内突然变化50%告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('316', '网络设备', '防火墙', '防火墙', '接口发送速率', 'ifHCOutOctetsSpeed', '防火墙发送接收速率超阈值告警', '高', '是', '变化50%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口发送流量5分钟内突然变化50%告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('317', '网络设备', '防火墙', '防火墙', '接口丢弃接收包数', 'ifInDiscards', '统计时间内接口丢弃接收包数', '高', '是', '大于10', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口丢弃接收包数大于10个/秒', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('318', '网络设备', '防火墙', '防火墙', '接口丢弃发送包数', 'ifOutDiscards', '统计时间内接口丢弃发送包数', '高', '是', '大于10', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口丢弃发送包数大于10个/秒', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('319', '网络设备', '防火墙', '防火墙', 'PAT并发会话数', 'NatStatConPatSession', '统计时间内PAT并发会话数', '高', '是', '根据实际并发调整阈值', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '', '性能阈值告警', '');
INSERT INTO `device_item_info` VALUES ('320', '网络设备', '防火墙', '防火墙', 'PAT会话速率', 'NatStatPatSessionRate', '统计时间内PAT会话速率', '高', '是', '根据实际会话数调整阈值', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '', '性能阈值告警', '');
INSERT INTO `device_item_info` VALUES ('321', '网络设备', '防火墙', '防火墙', '被NAT策略引用的地址池的数目', 'NatStatAddrGrpUsed', '统计时间内被NAT策略引用的地址池的数目', '高', '是', '根据实际地址池数调整阈值', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '', '性能阈值告警', '');
INSERT INTO `device_item_info` VALUES ('322', '网络设备', '防火墙', '防火墙', '配置地址池的数目', 'NatStatAddrGrpCount', '配置地址池的数目', '高', '是', '根据地址池数调整阈值', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '', '性能阈值告警', '');
INSERT INTO `device_item_info` VALUES ('323', '网络设备', '防火墙', '防火墙', '当前会话新建速率', 'SecStatMonCurSessSpeed', '当前会话新建速率', '高', '是', '根据现网需要调整阈值', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '', '性能阈值告警', '');
INSERT INTO `device_item_info` VALUES ('324', '网络设备', '防火墙', '防火墙', '当前会话总数', 'SecStatMonTotalSess', '当前会话总数', '高', '是', '根据实际会话数调整阈值', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '', '性能阈值告警', '');
INSERT INTO `device_item_info` VALUES ('325', '网络设备', '防火墙', '虚拟防火墙', '会话并发连接数', 'SecVSYSstatSessCount', '统计时间内并发链接数最大值', '高', '是', '根据实际并发，调整阈值', '每5分钟检查', 'SNMP', '是', '是', '是', '', '{HOST.IP} 5分钟内并发链接数最大值', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('326', '网络设备', '防火墙', '虚拟防火墙', '并发连接数利用率', '', '虚拟防火墙并发连接数利用率', '高', '是', '超过80%', '每5分钟检查', 'SNMP', '是', '是', '是', '', '{HOST.IP} 虚拟防火墙并发链接数利用率超过80%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('327', '网络设备', '防火墙', '虚拟防火墙', '会话新建连接数', 'SecVSYSstatSessSpeed', '统计时间内新建连接数', '高', '是', '根据实际连接，调整阈值', '每1分钟检查', 'SNMP', '是', '是', '是', '', '{HOST.IP} 60s时间内新建连接数', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('328', '网络设备', '防火墙', '虚拟防火墙', 'OSPF邻居状态改变', '', 'OSPF邻居状态发生变化', '重大', '是', '', '每5分钟主动轮询获取', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 路由器OSPF邻居状态异常', '通信告警', '是');
INSERT INTO `device_item_info` VALUES ('329', '网络设备', '防火墙', '虚拟防火墙', 'OSPF V3 邻居状态改变', '', 'OSPF V3邻居状态发生变化', '重大', '是', '', '每5分钟主动轮询获取', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} OSPF V3邻居状态发生变化，可能是由于该邻居所在的接口状态发生变化', '通信告警', '是');
INSERT INTO `device_item_info` VALUES ('330', '网络设备', '负载均衡', '负载均衡', '风扇故障', 'EntityFanState', '电源模块风扇异常或停止', '高', '是', '', '设备', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 风扇异常或停止', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('331', '网络设备', '负载均衡', '负载均衡', '电源故障', 'BoardCurrentPower', '电源设备告警', '重大', '是', '', '设备', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 电源设备告警', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('332', '网络设备', '负载均衡', '负载均衡', '设备端口状态', 'ifOperStatus', '端口的物理状态由UP变为DOWN告警', '重大', '是', '', '设备', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 端口状态由UP变为DOWN', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('333', '网络设备', '负载均衡', '负载均衡', '设备不可达率', 'neContrlableRate', '设备60S内不可达比率', '重大', '是', '超过5%', '每1分钟检查', 'Simple Check', '是', '是', '是', '', '{HOST.IP} 设备60S内不可达比率超过5%', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('334', '网络设备', '负载均衡', '负载均衡', '设备不可达', 'icmpping', 'Ping设备的活动性', '重大', '是', '', '每1分钟检查', 'Simple Check', '是', '是', '是', '', '{HOST.IP} PING不可达', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('335', '网络设备', '负载均衡', '负载均衡', '设备响应时间', 'nePingRespTime', 'ping设备的响应时间', '高', '是', '超过200ms', '每1分钟检查', 'Simple Check', '是', '是', '是', '', '{HOST.IP} 设备60S内响应时间超过200ms', '性能阈值告警', '否');
INSERT INTO `device_item_info` VALUES ('336', '网络设备', '负载均衡', '负载均衡', '设备（含单板）CPU利用率', 'cpuUsage', '负载均衡CPU利用率超阈值告警', '重大', '是', '超过50%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} CPU利用率超80%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('337', '网络设备', '负载均衡', '负载均衡', '设备（含单板）内存利用率', 'memUsage', '负载均衡内存利用率超阈值告警', '重大', '是', '超过90%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 内存利用率超90%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('338', '网络设备', '负载均衡', '负载均衡', '系统运行时间', 'sysUpTime', '检查交换机运行系统运行时间', '重大', '是', '运行时间少于1800S', '每5分钟主动轮询获取', 'SNMP', '是', '是', '', '', '{HOST.IP} 设备运行时间少于1800S', '设备告警', '否');
INSERT INTO `device_item_info` VALUES ('339', '网络设备', '负载均衡', '负载均衡', '双机热备心跳状态异常', 'hwHrpState ', '负载均衡双机热备心跳状态异常', '重大', '是', '', '每1分钟检查', 'SNMP', '是', '是', '是', '', '{HOST.IP} 负载均衡双机热备心跳状态异常', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('340', '网络设备', '负载均衡', '负载均衡', '检测发现双主故障', 'hwHrpState ', '检测发现负载均衡双主故障', '重大', '是', '', '每1分钟检查', 'SNMP', '是', '是', '是', '', '{HOST.IP} 检测发现负载均衡双主故障', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('341', '网络设备', '负载均衡', '负载均衡', '客户端并发连接数', 'ClientCurConns', '统计时间内并发链接数最大值', '高', '是', '根据实际并发，调整阈值', '每5分钟检查', 'SNMP', '是', '是', '是', '', '{HOST.IP} 客户端并发连接数超过阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('342', '网络设备', '负载均衡', '负载均衡', '客户端流入速率', 'ClientBytesIn', '客户端流入速率超过阈值告警', '高', '是', '根据实际连接，调整阈值', '每5分钟检查', 'SNMP', '是', '是', '是', '', '{HOST.IP} 客户端流入速率超过阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('343', '网络设备', '负载均衡', '负载均衡', '客户端流出速率', 'ClientBytesOut', '客户端流出速率超过阈值告警', '高', '是', '根据实际流量，调整阈值', '每5分钟检查', 'SNMP', '是', '是', '是', '', '{HOST.IP} 客户端流出速率超过阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('344', '网络设备', '负载均衡', '负载均衡', '服务器端流入速率', 'ServerBytesIn', '服务器端流入速率超过阈值告警', '高', '是', '根据实际并发，调整阈值', '每5分钟检查', 'SNMP', '是', '是', '是', '', '{HOST.IP} 服务器端流入速率超过阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('345', '网络设备', '负载均衡', '负载均衡', '服务器端流出速率', 'ServerBytesOut', '服务器端流出速率超过阈值告警', '高', '是', '根据实际连接，调整阈值', '每5分钟检查', 'SNMP', '是', '是', '是', '', '{HOST.IP} 服务器端流出速率超过阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('346', '网络设备', '负载均衡', '负载均衡', '接口发送包错误数', 'ifOutErrors', '统计时间内接口发送包Errors错误数', '高', '是', '大于10', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口发送包错误数大于10个/秒', '性能阈值告警', '否');
INSERT INTO `device_item_info` VALUES ('347', '网络设备', '负载均衡', '负载均衡', '接口接收包错误数', 'ifInErrors', '统计时间内接口接收包Errors错误数', '高', '是', '大于10', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口接收包错误数大于10个/秒', '性能阈值告警', '否');
INSERT INTO `device_item_info` VALUES ('348', '网络设备', '负载均衡', '负载均衡', '接口入口带宽利用率', 'ifInBandRate', '接口入口带宽利用率超阈值告警', '高', '是', '超过70%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} CMNET接口入口带宽利用率超90%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('349', '网络设备', '负载均衡', '负载均衡', '接口出口带宽利用率', 'ifOutBandRate', '接口出口带宽利用率超阈值告警', '高', '是', '超过70%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} CMNET接口出口带宽利用率超90%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('350', '网络设备', '负载均衡', '负载均衡', '接口接收速率', 'ifHCInOctetsSpeed', '设备接口接收速率超阈值告警', '高', '是', '变化50%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口接收流量5分钟内突然变化50%告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('351', '网络设备', '负载均衡', '负载均衡', '接口发送速率', 'ifHCOutOctetsSpeed', '设备发送接收速率超阈值告警', '高', '是', '变化50%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口发送流量5分钟内突然变化50%告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('352', '网络设备', '安全设备', '入侵检测', '风扇故障', 'EntityFanState', '电源模块风扇异常或停止', '高', '是', '', '设备', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 风扇异常或停止', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('353', '网络设备', '安全设备', '抗DDOS', '风扇故障', 'EntityFanState', '电源模块风扇异常或停止', '高', '是', '', '设备', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 风扇异常或停止', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('354', '网络设备', '安全设备', '漏洞扫描', '风扇故障', 'EntityFanState', '电源模块风扇异常或停止', '高', '是', '', '设备', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 风扇异常或停止', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('355', '网络设备', '安全设备', '入侵检测', '电源故障', 'BoardCurrentPower', '电源设备告警', '高', '是', '', '设备', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 电源设备告警', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('356', '网络设备', '安全设备', '抗DDOS', '电源故障', 'BoardCurrentPower', '电源设备告警', '高', '是', '', '设备', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 电源设备告警', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('357', '网络设备', '安全设备', '漏洞扫描', '电源故障', 'BoardCurrentPower', '电源设备告警', '高', '是', '', '设备', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 电源设备告警', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('358', '网络设备', '安全设备', '入侵检测', '设备端口状态', 'ifOperStatus', '端口的物理状态由UP变为DOWN告警', '重大', '是', '', '设备', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 端口状态由UP变为DOWN', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('359', '网络设备', '安全设备', '抗DDOS', '设备端口状态', 'ifOperStatus', '端口的物理状态由UP变为DOWN告警', '重大', '是', '', '设备', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 端口状态由UP变为DOWN', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('360', '网络设备', '安全设备', '漏洞扫描', '设备端口状态', 'ifOperStatus', '端口的物理状态由UP变为DOWN告警', '重大', '是', '', '设备', 'SNMP/SNMPTrap', '是', '是', '是', '', '{HOST.IP} 端口状态由UP变为DOWN', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('361', '网络设备', '安全设备', '入侵检测', '设备不可达率', 'neContrlableRate', '设备60S内不可达比率', '重大', '是', '超过5%', '每1分钟检查', 'Simple Check', '是', '是', '是', '', '{HOST.IP} 设备60S内不可达比率超过5%', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('362', '网络设备', '安全设备', '抗DDOS', '设备不可达率', 'neContrlableRate', '设备60S内不可达比率', '重大', '是', '超过5%', '每1分钟检查', 'Simple Check', '是', '是', '是', '', '{HOST.IP} 设备60S内不可达比率超过5%', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('363', '网络设备', '安全设备', '漏洞扫描', '设备不可达率', 'neContrlableRate', '设备60S内不可达比率', '重大', '是', '超过5%', '每1分钟检查', 'Simple Check', '是', '是', '是', '', '{HOST.IP} 设备60S内不可达比率超过5%', '设备告警', '是');
INSERT INTO `device_item_info` VALUES ('364', '网络设备', '安全设备', '入侵检测', '设备不可达', 'neContrlableRate', 'Ping设备的活动性', '重大', '是', '', '每1分钟检查', 'Simple Check', '是', '是', '是', '', '{HOST.IP} PING不可达', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('365', '网络设备', '安全设备', '抗DDOS', '设备不可达', 'neContrlableRate', 'Ping设备的活动性', '重大', '是', '', '每1分钟检查', 'Simple Check', '是', '是', '是', '', '{HOST.IP} PING不可达', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('366', '网络设备', '安全设备', '漏洞扫描', '设备不可达', 'neContrlableRate', 'Ping设备的活动性', '重大', '是', '', '每1分钟检查', 'Simple Check', '是', '是', '是', '', '{HOST.IP} PING不可达', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('367', '网络设备', '安全设备', '入侵检测', '设备（含单板）CPU利用率', 'cpuUsage', '负载均衡CPU利用率超阈值告警', '重大', '是', '超过50%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} CPU利用率超80%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('368', '网络设备', '安全设备', '抗DDOS', '设备（含单板）CPU利用率', 'cpuUsage', '负载均衡CPU利用率超阈值告警', '重大', '是', '超过50%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} CPU利用率超80%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('369', '网络设备', '安全设备', '漏洞扫描', '设备（含单板）CPU利用率', 'cpuUsage', '负载均衡CPU利用率超阈值告警', '重大', '是', '超过50%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} CPU利用率超80%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('370', '网络设备', '安全设备', '入侵检测', '设备（含单板）内存利用率', 'memUsage', '负载均衡内存利用率超阈值告警', '重大', '是', '超过90%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 内存利用率超90%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('371', '网络设备', '安全设备', '抗DDOS', '设备（含单板）内存利用率', 'memUsage', '负载均衡内存利用率超阈值告警', '重大', '是', '超过90%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 内存利用率超90%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('372', '网络设备', '安全设备', '漏洞扫描', '设备（含单板）内存利用率', 'memUsage', '负载均衡内存利用率超阈值告警', '重大', '是', '超过90%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 内存利用率超90%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('373', '网络设备', '安全设备', '入侵检测', '系统运行时间', 'sysUpTime', '检查交换机运行系统运行时间', '高', '是', '运行时间少于1800S', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 设备运行时间少于1800S', '设备告警', '否');
INSERT INTO `device_item_info` VALUES ('374', '网络设备', '安全设备', '抗DDOS', '系统运行时间', 'sysUpTime', '检查交换机运行系统运行时间', '高', '是', '运行时间少于1800S', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 设备运行时间少于1800S', '设备告警', '否');
INSERT INTO `device_item_info` VALUES ('375', '网络设备', '安全设备', '漏洞扫描', '系统运行时间', 'sysUpTime', '检查交换机运行系统运行时间', '高', '是', '运行时间少于1800S', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 设备运行时间少于1800S', '设备告警', '否');
INSERT INTO `device_item_info` VALUES ('376', '网络设备', '安全设备', '入侵检测', '接口发送包错误数', 'ifOutErrors', '统计时间内接口发送包Errors错误数', '高', '是', '大于10', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口发送包错误数大于10个/秒', '性能阈值告警', '否');
INSERT INTO `device_item_info` VALUES ('377', '网络设备', '安全设备', '抗DDOS', '接口发送包错误数', 'ifOutErrors', '统计时间内接口发送包Errors错误数', '高', '是', '大于10', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口发送包错误数大于10个/秒', '性能阈值告警', '否');
INSERT INTO `device_item_info` VALUES ('378', '网络设备', '安全设备', '漏洞扫描', '接口发送包错误数', 'ifOutErrors', '统计时间内接口发送包Errors错误数', '高', '是', '大于10', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口发送包错误数大于10个/秒', '性能阈值告警', '否');
INSERT INTO `device_item_info` VALUES ('379', '网络设备', '安全设备', '入侵检测', '接口接收包错误数', 'ifInErrors', '统计时间内接口接收包Errors错误数', '高', '是', '大于10', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口接收包错误数大于10个/秒', '性能阈值告警', '否');
INSERT INTO `device_item_info` VALUES ('380', '网络设备', '安全设备', '抗DDOS', '接口接收包错误数', 'ifInErrors', '统计时间内接口接收包Errors错误数', '高', '是', '大于10', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口接收包错误数大于10个/秒', '性能阈值告警', '否');
INSERT INTO `device_item_info` VALUES ('381', '网络设备', '安全设备', '漏洞扫描', '接口接收包错误数', 'ifInErrors', '统计时间内接口接收包Errors错误数', '高', '是', '大于10', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} 接口接收包错误数大于10个/秒', '性能阈值告警', '否');
INSERT INTO `device_item_info` VALUES ('382', '网络设备', '安全设备', '入侵检测', '接口入口带宽利用率', 'ifInOctets.pused[{#SNMPVALUE}] ', '接口入口带宽利用率超阈值告警', '高', '是', '超过70%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} CMNET接口入口带宽利用率超70%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('383', '网络设备', '安全设备', '抗DDOS', '接口入口带宽利用率', 'ifInOctets.pused[{#SNMPVALUE}] ', '接口入口带宽利用率超阈值告警', '高', '是', '超过70%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} CMNET接口入口带宽利用率超70%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('384', '网络设备', '安全设备', '漏洞扫描', '接口入口带宽利用率', 'ifInOctets.pused[{#SNMPVALUE}] ', '接口入口带宽利用率超阈值告警', '高', '是', '超过70%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} CMNET接口入口带宽利用率超70%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('385', '网络设备', '安全设备', '入侵检测', '接口出口带宽利用率', 'ifOutOctets.Pused[{#SNMPVALUE}] ', '接口出口带宽利用率超阈值告警', '高', '是', '超过70%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} CMNET接口出口带宽利用率超70%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('386', '网络设备', '安全设备', '抗DDOS', '接口出口带宽利用率', 'ifOutOctets.Pused[{#SNMPVALUE}] ', '接口出口带宽利用率超阈值告警', '高', '是', '超过70%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} CMNET接口出口带宽利用率超70%阈值告警', '性能阈值告警', '是');
INSERT INTO `device_item_info` VALUES ('387', '网络设备', '安全设备', '漏洞扫描', '接口出口带宽利用率', 'ifOutOctets.Pused[{#SNMPVALUE}] ', '接口出口带宽利用率超阈值告警', '高', '是', '超过70%', '每5分钟主动轮询获取', 'SNMP', '是', '是', '是', '', '{HOST.IP} CMNET接口出口带宽利用率超70%阈值告警', '性能阈值告警', '是');
