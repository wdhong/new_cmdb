CREATE TABLE `t_cfg_scada_canvas` (
	`id` VARCHAR(50) NOT NULL COMMENT '视图id',
	`name` VARCHAR(255) NULL DEFAULT NULL COMMENT '视图标题',
	`content` LONGTEXT NULL COMMENT '视图内容',
	`picture_type` INT(4) NULL DEFAULT NULL COMMENT '1-物理图 2-逻辑图 3-租户图',
	`page_type` INT(4) NOT NULL COMMENT '页面类型 0普通页面  1模板',
	`idc` VARCHAR(200) NULL DEFAULT NULL COMMENT '资源池code',
	`pod` VARCHAR(50) NULL DEFAULT NULL COMMENT 'pod',
	`project_name` VARCHAR(200) NULL DEFAULT NULL COMMENT '工程期数',
	`biz_system` VARCHAR(200) NULL DEFAULT NULL COMMENT '业务系统',
	`is_default` VARCHAR(1) NULL DEFAULT NULL COMMENT '是否是默认拓扑，1-是 0-否（预留）',
	`create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
	`update_time` timestamp NOT NULL COMMENT '修改时间',
	PRIMARY KEY (`id`) USING BTREE
)
COMMENT='视图表'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DYNAMIC
;
-- {{ jinsu add 2019/10/25
alter table t_cfg_scada_canvas add column bind_obj LONGTEXT COMMENT '绑定对象'
-- }}