/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-10-31 20:25:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for auto_confirm_clear
-- ----------------------------
DROP TABLE IF EXISTS `auto_confirm_clear`;
CREATE TABLE `auto_confirm_clear` (
  `uuid` varchar(255) NOT NULL COMMENT 'id',
  `device_ip` varchar(255) DEFAULT NULL COMMENT '设备ip',
  `idc_type` varchar(255) DEFAULT NULL COMMENT '资源池',
  `biz_sys` varchar(255) DEFAULT NULL COMMENT '业务线',
  `alert_level` varchar(255) DEFAULT NULL COMMENT '告警等级',
  `item_id` varchar(255) DEFAULT NULL COMMENT '监控项id',
  `source` varchar(255) DEFAULT NULL COMMENT '告警来源',
  `auto_type` tinyint(2) DEFAULT NULL COMMENT '1-确认 2-清除',
  `content` text COMMENT '内容',
  `start_time` varchar(255) DEFAULT NULL COMMENT '开始时间',
  `end_time` varchar(255) DEFAULT NULL COMMENT '结束时间',
  `operator` varchar(255) DEFAULT NULL COMMENT '操作人'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
