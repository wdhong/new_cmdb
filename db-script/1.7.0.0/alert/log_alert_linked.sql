/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-10-31 22:43:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for log_alert_linked
-- ----------------------------
DROP TABLE IF EXISTS `log_alert_linked`;
CREATE TABLE `log_alert_linked` (
  `log_alert_rule_uuid` varchar(255) NOT NULL COMMENT '日志告警规则id',
  `alert_id` varchar(255) NOT NULL COMMENT '告警id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
