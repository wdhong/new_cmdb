/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-10-31 22:44:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for log_alert_rule
-- ----------------------------
DROP TABLE IF EXISTS `log_alert_rule`;
CREATE TABLE `log_alert_rule` (
  `uuid` varchar(255) NOT NULL COMMENT '日志告警规则id',
  `name` varchar(255) NOT NULL COMMENT '规则名称',
  `run_status` varchar(10) DEFAULT NULL COMMENT '运行状态 0-停用 1-启用',
  `description` varchar(255) DEFAULT NULL COMMENT '规则描述',
  `idc_type` varchar(255) DEFAULT NULL COMMENT '资源池',
  `ip` varchar(255) DEFAULT NULL COMMENT '设备ip',
  `param` varchar(255) DEFAULT NULL COMMENT '关键字',
  `filter_time` tinyint(255) DEFAULT NULL COMMENT '过滤时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `creator` varchar(255) DEFAULT NULL COMMENT '创建人',
  `user_name` varchar(255) DEFAULT NULL COMMENT 'user_name',
  `alert_level` varchar(255) NOT NULL COMMENT '告警等级',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='sys日志告警规则配置表';
