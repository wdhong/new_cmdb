/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : cmdb

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-10-16 09:15:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_dict_mapper
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_dict_mapper`;
CREATE TABLE `cmdb_dict_mapper` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `mapper_dict_type` varchar(40) NOT NULL COMMENT '字典类型',
  `mapper_dict_code` varchar(100) NOT NULL COMMENT '第三方字典值',
  `ums_dict_code` varchar(100) NOT NULL COMMENT 'UMS字典值编码',
  `ums_dict_name` varchar(100) NOT NULL COMMENT 'UMS字典文本值',
  `mapper_device_type` varchar(100) DEFAULT NULL COMMENT '设备类型',
  `mapper_source` varchar(40) NOT NULL COMMENT '来源 苏研/华为等等',
  PRIMARY KEY (`id`),
  KEY `IDX_CMDB_DICT_MAPPER_01` (`mapper_dict_code`,`mapper_source`,`mapper_dict_type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8 COMMENT='UMS与第三方字典值对应关系表';

-- ----------------------------
-- Records of cmdb_dict_mapper
-- ----------------------------
INSERT INTO `cmdb_dict_mapper` VALUES ('2', 'POD', '呼和浩特二期一阶段管理POD', '管理POD', '管理POD', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('3', 'POD', '呼和浩特资源池', 'POD-0', 'POD-0', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('4', 'POD', '呼和浩特二期一阶段POD8', 'POD-8', 'POD-8', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('5', 'POD', '呼和浩特二期一阶段POD9', 'POD-9', 'POD-9', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('6', 'POD', '呼和浩特二期一阶段POD6', 'POD-6', 'POD-6', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('7', 'POD', '呼和浩特二期一阶段POD7', 'POD-7', 'POD-7', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('8', 'POD', '呼和浩特二期一阶段POD5', 'POD-5', 'POD-5', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('9', 'POD', '呼和浩特二期一阶段POD3', 'POD-3', 'POD-3', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('10', 'POD', '呼和浩特二期一阶段POD2', 'POD-2', 'POD-2', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('11', 'POD', '呼和浩特二期一阶段POD4', 'POD-4', 'POD-4', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('12', 'POD', '呼和浩特二期一阶段POD1', 'POD-1', 'POD-1', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('13', 'POD', '呼和浩特二期二阶段POD10', 'POD-10', 'POD-10', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('14', 'POD', '哈尔滨二期二阶段POD9', 'POD-9', 'POD-9', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('15', 'POD', '哈尔滨二期一阶段POD3', 'POD-3', 'POD-3', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('16', 'POD', '哈尔滨二期一阶段POD6', 'POD-6', 'POD-6', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('17', 'POD', '哈尔滨资源池', 'POD-9', 'POD-0', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('18', 'POD', '哈尔滨二期二阶段POD11', 'POD-11', 'POD-11', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('19', 'POD', '哈尔滨二期一阶段POD1', 'POD-1', 'POD-1', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('20', 'POD', '哈尔滨二期一阶段POD2', 'POD-2', 'POD-2', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('21', 'POD', '哈尔滨二期一阶段POD5', 'POD-5', 'POD-5', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('22', 'POD', '哈尔滨二期一阶段管理', '管理POD', '管理POD', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('23', 'POD', '哈尔滨二期二阶段POD8', 'POD-8', 'POD-8', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('24', 'POD', '哈尔滨二期二阶段POD10', 'POD-10', 'POD-10', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('25', 'POD', '哈尔滨二期一阶段POD4', 'POD-4', 'POD-4', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('26', 'POD', '哈尔滨二期一阶段POD7', 'POD-7', 'POD-7', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('27', 'device_status', '0', '已关闭电源', '已关闭电源', '物理机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('28', 'device_status', '1', '已打开电源', '已打开电源', '物理机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('29', 'device_status', '2', '运行正常', '运行正常', '物理机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('30', 'device_status', '3', '已关闭电源', '已关闭电源', '物理机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('31', 'device_status', '4', '休眠', '休眠', '物理机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('32', 'device_status', '5', '维护中', '维护中', '物理机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('33', 'device_status', '0', '运行正常', '运行正常', '虚拟机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('34', 'device_status', '1', '闲置', '闲置', '虚拟机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('35', 'device_status', '2', '报废', '报废', '虚拟机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('36', 'device_status', '3', '维护中', '维护中', '虚拟机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('37', 'device_mfrs', '0', '惠普', '惠普', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('38', 'device_mfrs', '1', '华为', '华为', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('39', 'device_mfrs', '2', 'IBM', 'IBM', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('40', 'device_mfrs', '3', 'DELL', 'DELL', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('41', 'device_mfrs', '4', 'FiberHome', 'FiberHome', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('42', 'device_mfrs', '5', 'H3C', 'H3C', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('43', 'device_mfrs', '6', 'Inspur', 'Inspur', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('44', 'device_mfrs', '7', 'LENOVO', 'LENOVO', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('45', 'device_mfrs', '8', 'LTEON', 'LTEON', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('46', 'device_mfrs', '9', 'VMAX', 'VMAX', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('47', 'device_mfrs', '10', 'ZTE', 'ZTE', null, '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('48', 'filed_mapper', 'name', 'device_name', 'device_name', '物理机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('49', 'filed_mapper', 'os', 'device_os_type', 'device_os_type', '物理机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('50', 'filed_mapper', 'osDetail', 'os_detail_version', 'os_detail_version', '物理机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('51', 'filed_mapper', 'podName', 'pod_name', 'pod_name', '物理机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('52', 'filed_mapper', 'model', 'device_model', 'device_model', '物理机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('53', 'filed_mapper', 'firstOrg', 'department1', 'department1', '物理机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('54', 'filed_mapper', 'secondaryOrg', 'department2', 'department2', '物理机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('55', 'filed_mapper', 'businessName', 'bizSystem', 'bizSystem', '物理机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('56', 'filed_mapper', 'manufacturer', 'device_mfrs', 'device_mfrs', '物理机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('57', 'filed_mapper', 'status', 'device_status', 'device_status', '物理机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('58', 'filed_mapper', 'osStorage', 'disk_size', 'disk_size', '物理机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('59', 'filed_mapper', 'serialNo', 'device_sn', 'device_sn', '物理机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('60', 'filed_mapper', 'resourceId', 'asset_number', 'asset_number', '物理机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('61', 'filed_mapper', 'cpuType', 'cpu_type', 'cpu_type', '物理机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('62', 'filed_mapper', 'hbaSpec', 'HBA', 'HBA', '物理机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('63', 'filed_mapper', 'cpuSpec', 'cpu_number', 'cpu_number', '物理机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('64', 'filed_mapper', 'cpuSpec', 'cpu_core_number', 'cpu_core_number', '物理机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('65', 'filed_mapper', 'memorySize', 'memory_size', 'memory_size', '物理机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('66', 'filed_mapper', 'hostName', 'HOST_NAME', 'HOST_NAME', '物理机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('67', 'filed_mapper', 'manageNetmask', 'ip_mask', 'ip_mask', '物理机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('68', 'filed_mapper', 'impiNetmask', 'ipmi_mask', 'ipmi_mask', '物理机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('69', 'filed_mapper', 'impiIp', 'ipmi_ip', 'ipmi_ip', '物理机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('70', 'filed_mapper', 'name', 'device_name', 'device_name', '虚拟机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('71', 'filed_mapper', 'os', 'device_os_type', 'device_os_type', '虚拟机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('72', 'filed_mapper', 'osDetail', 'os_detail_version', 'os_detail_version', '虚拟机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('73', 'filed_mapper', 'podName', 'pod_name', 'pod_name', '虚拟机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('74', 'filed_mapper', 'model', 'device_model', 'device_model', '虚拟机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('75', 'filed_mapper', 'firstOrg', 'department1', 'department1', '虚拟机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('76', 'filed_mapper', 'secondaryOrg', 'department2', 'department2', '虚拟机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('77', 'filed_mapper', 'businessName', 'bizSystem', 'bizSystem', '虚拟机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('78', 'filed_mapper', 'status', 'device_status', 'device_status', '虚拟机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('79', 'filed_mapper', 'osStorage', 'disk_size', 'disk_size', '虚拟机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('80', 'filed_mapper', 'cpuNum', 'cpu_core_number', 'cpu_core_number', '虚拟机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('81', 'filed_mapper', 'memorySize', 'memory_size', 'memory_size', '虚拟机', '苏研');
INSERT INTO `cmdb_dict_mapper` VALUES ('82', 'filed_mapper', 'hostName', 'HOST_NAME', 'HOST_NAME', '虚拟机', '苏研');
