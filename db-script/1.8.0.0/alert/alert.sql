ALTER TABLE `log_alert_rule`
ADD COLUMN `include_key`  varchar(128) NULL COMMENT '包含关键字' AFTER `include` ;

ALTER TABLE `log_alert_rule`
ADD COLUMN `no_include`  varchar(128) NULL COMMENT '不包含' AFTER `include_key` ;

ALTER TABLE `log_alert_rule`
ADD COLUMN `no_include_key`  varchar(128) NULL COMMENT '不包含关键字' AFTER `no_include` ;

ALTER TABLE `log_alert_rule` DROP  param;

ALTER TABLE `security_leak_scan_reports`
ADD COLUMN `idc_type`  varchar(128) NULL COMMENT '资源池' AFTER `scan_id` ;


