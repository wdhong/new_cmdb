/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-11-26 16:33:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_scan_comparision
-- ----------------------------
DROP TABLE IF EXISTS `alert_scan_comparision`;
CREATE TABLE `alert_scan_comparision` (
  `id` varchar(50) NOT NULL COMMENT 'id',
  `device_ip` varchar(50) DEFAULT NULL COMMENT '设别ip',
  `idc_type` varchar(50) DEFAULT NULL COMMENT '资源池',
  `start_scan_time` datetime DEFAULT NULL COMMENT '首次扫描时间',
  `cur_scan_time` datetime DEFAULT NULL COMMENT '当前扫描时间',
  `syn_status` varchar(10) DEFAULT NULL COMMENT '同步状态 1-同步 2-未同步 3-同步失败',
  `cur_syn_time` datetime DEFAULT NULL COMMENT '当前同步时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警扫描对账记录表';
