/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-11-28 09:37:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_schedule_index
-- ----------------------------
DROP TABLE IF EXISTS `alert_schedule_index`;
CREATE TABLE `alert_schedule_index` (
  `id` varchar(50) NOT NULL COMMENT 'id',
  `index_name` varchar(50) DEFAULT NULL COMMENT '指标名称',
  `index_value` varchar(50) DEFAULT NULL COMMENT '指标值',
  `index_type` varchar(50) DEFAULT NULL COMMENT '指标类型',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alert_schedule_index
-- ----------------------------
INSERT INTO `alert_schedule_index` VALUES ('1', '告警自动确认时间', '2019-11-20 00:00:00', 'alert_auto_confirm_time', '不可更改不可删除');
INSERT INTO `alert_schedule_index` VALUES ('2', '告警自动清除时间', '2019-11-20 00:00:00', 'alert_auto_clear_time', '不可更改不可删除');
