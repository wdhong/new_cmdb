/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.38
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-11-28 10:05:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_storage_userate
-- ----------------------------
DROP TABLE IF EXISTS `alert_storage_userate`;
CREATE TABLE `alert_storage_userate` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `idc_type` varchar(255) DEFAULT NULL,
  `block_useRate` decimal(20,0) DEFAULT NULL COMMENT '块平均使用率',
  `san_useRate` decimal(20,0) DEFAULT NULL COMMENT 'san平均使用率',
  `createTime` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `device_class` (`idc_type`) USING HASH,
  KEY `device_type-device_class` (`block_useRate`,`idc_type`) USING BTREE,
  KEY `subclass-device_type-device_class` (`san_useRate`,`idc_type`,`block_useRate`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='磁盘利用率数据';
