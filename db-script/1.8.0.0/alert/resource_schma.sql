DELETE FROM resource_schema WHERE resource='os_appraval';

INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) 
VALUES ('os_appraval', 'f', '2019-10-09 17:49:08', '版本变更记录', 'net_config_manage');

INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) 
VALUES ('alert_scan_compare', 'f', '2019-07-03 05:00:00.000000', '告警扫描对账', 'head_alert');

INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) 
VALUES ('alert_scan_compare_child', 'f', '2019-07-03 05:00:00.000000', '告警扫描对账', 'alert_scan_compare');
