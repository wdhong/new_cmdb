INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('net_performance_analysis', 'f', '2019-10-09 17:49:08.000000', '网络性能分析', 'net_config_manage');


INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES 
('menu_manage', 'f', '2019-04-09 08:10:52.000000', '菜单管理', 'user_auth_manage');

INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('alert_index_page', 'f', '2019-04-09 08:10:52.000000', '告警首页', 'alert');
INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('monitor_index', 'f', '2019-07-03 05:00:00.000000', '监控首页', 'monitor_screen_manage');


-- 自动化运维
INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`)
 VALUES ('automation', 'f', '2019-04-09 08:10:52.000000', '自动化', 'all');

INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`)
 VALUES ('auto_operation', 'f', '2019-04-09 08:10:52.000000', '自动化运维', 'automation');

INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`)
 VALUES ('auto_operation_home', 'f', '2019-04-09 08:10:52.000000', '自动化首页', 'auto_operation');

INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`)
 VALUES ('script_manage', 'f', '2019-04-09 08:10:52.000000', '脚本管理', 'auto_operation');

INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`)
 VALUES ('script_exec', 'f', '2019-04-09 08:10:52.000000', '快速脚本执行', 'auto_operation');

 INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`)
 VALUES ('script_exec_history', 'f', '2019-04-09 08:10:52.000000', '执行历史', 'auto_operation');

  INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`)
 VALUES ('exec_history', 'f', '2019-04-09 08:10:52.000000', '执行历史', 'auto_operation');

 INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`)
 VALUES ('yum', 'f', '2019-04-09 08:10:52.000000', 'YUM源', 'auto_operation');
 INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`)
 VALUES ('yum_config', 'f', '2019-04-09 08:10:52.000000', 'YUM配置', 'auto_operation');


 INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`)
 VALUES ('pipeline', 'f', '2019-04-09 08:10:52.000000', '作业', 'automation');
 INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`)
 VALUES ('pipeline_manage', 'f', '2019-04-09 08:10:52.000000', '常用作业执行', 'pipeline');
 INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`)
 VALUES ('pipeline_add', 'f', '2019-04-09 08:10:52.000000', '新建作业', 'pipeline');

 INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`)
 VALUES ('sensitive', 'f', '2019-04-09 08:10:52.000000', '敏感指令', 'automation');

 INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`)
 VALUES ('sensitive_config', 'f', '2019-04-09 08:10:52.000000', '敏感指令管理', 'sensitive');

INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`)
 VALUES ('sensitive_review', 'f', '2019-04-09 08:10:52.000000', '赋权审核', 'sensitive');


 INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`)
 VALUES ('inspection', 'f', '2019-04-09 08:10:52.000000', '巡检管理', 'automation');

 INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`)
 VALUES ('task_exec', 'f', '2019-04-09 08:10:52.000000', '任务运行日志', 'inspection');

update resource_schema set parent_resource = 'inspection' where name in ('巡检报表管理','巡检任务管理','巡检模板管理');


 INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`)
 VALUES ('pipeline_schedule', 'f', '2019-04-09 08:10:52.000000', '定时作业', 'pipeline');
 
 INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`)
 VALUES ('file_distribution', 'f', '2019-04-09 08:10:52.000000', '快速文件分发', 'pipeline');

 INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`)
 VALUES ('pipeline_scenes', 'f', '2019-04-09 08:10:52.000000', '场景管理', 'pipeline');


 INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`)
 VALUES ('group', 'f', '2019-04-09 08:10:52.000000', '分组管理', 'automation');

 INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`)
 VALUES ('group_manage', 'f', '2019-04-09 08:10:52.000000', '分组管理', 'group');


 INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`)
 VALUES ('self_healing', 'f', '2019-04-09 08:10:52.000000', '自愈管理', 'automation');

 INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`)
 VALUES ('self_healing_manage', 'f', '2019-04-09 08:10:52.000000', '自愈管理', 'self_healing');

INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`)
 VALUES ('self_healing_log', 'f', '2019-04-09 08:10:52.000000', '故障自愈日志', 'self_healing');
