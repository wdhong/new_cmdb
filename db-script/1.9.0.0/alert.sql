ALTER TABLE `log_filter_rule`
ADD COLUMN `rule_type`  varchar(128) NULL COMMENT '规则类型' AFTER `uuid` ;

ALTER TABLE `log_filter_rule`
ADD COLUMN `include_key`  varchar(128) NULL COMMENT '包含关键字' AFTER `param` ;

ALTER TABLE `log_filter_rule`
ADD COLUMN `no_include_key`  varchar(128) NULL COMMENT '不包含关键字' AFTER `include_key` ;

ALTER TABLE `alert_scan_comparision`
ADD COLUMN `cur_moni_time`  varchar(128) NULL COMMENT '当前告警时间' AFTER `cur_scan_time` ;

INSERT INTO `alert_schedule_index`(`id`, `index_name`, `index_value`, `index_type`, `remark`)
 VALUES ('1', '严重', '5', 'alert_level', '不可更改不可删除');
INSERT INTO `alert_schedule_index`(`id`, `index_name`, `index_value`, `index_type`, `remark`)
 VALUES ('2', '高', '4', 'alert_level', '不可更改不可删除');
INSERT INTO `alert_schedule_index`(`id`, `index_name`, `index_value`, `index_type`, `remark`)
 VALUES ('3', '中', '3', 'alert_level', '不可更改不可删除');
INSERT INTO `alert_schedule_index`(`id`, `index_name`, `index_value`, `index_type`, `remark`)
 VALUES ('4', '低', '2', 'alert_level', '不可更改不可删除');
INSERT INTO `alert_schedule_index`(`id`, `index_name`, `index_value`, `index_type`, `remark`) 
 VALUES ('5', '日', 'day', 'granularity', '不可更改不可删除');
INSERT INTO `alert_schedule_index`(`id`, `index_name`, `index_value`, `index_type`, `remark`)
 VALUES ('6', '周', 'week', 'granularity', '不可更改不可删除');
INSERT INTO `alert_schedule_index`(`id`, `index_name`, `index_value`, `index_type`, `remark`)
 VALUES ('7', '月', 'month', 'granularity', '不可更改不可删除');
