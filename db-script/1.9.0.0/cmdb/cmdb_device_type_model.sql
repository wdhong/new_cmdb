/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 5.7.26-log 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;
DROP TABLE IF EXISTS `cmdb_device_type_model`;
CREATE TABLE `cmdb_device_type_model` (
  `device_type_id` varchar(40) NOT NULL COMMENT '设备类型id',
  `device_model` varchar(40) NOT NULL COMMENT '设备型号',
  PRIMARY KEY (`device_type_id`,`device_model`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='设备类型设备型号关系表'

insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('04','中兴ZXR10 5960-72DL-H');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('04','中兴ZXR10 9908');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('04','中兴ZXR10 9916');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('04','华三S12508F-AF');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('04','华三S12516F-AF');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('04','华三S6900-54QF-F');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('04','思科Nexus 5672');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('04','思科Nexus 7710');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('04','思科Nexus 7718');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('04','锐捷RG-N18010-X');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('04','锐捷RG-N18018-X');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('04','锐捷RG-S6220-48XS6QXS-H');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('12','中兴ZXR10 5228F-PS');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('12','中兴ZXR10 5928D-FI');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('12','中兴ZXR10 5952D');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('12','华三S5530C-EI-D');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('12','华三S5530C-PWR-EI-D');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('12','华三S5530F-EI-D');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('12','华三S5554C-EI-D');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('12','华三S5554C-PWR-EI-D');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('12','华三S5820V2-54QF');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('12','烽火S5700-28F-X');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('12','烽火S5700-28T-S-P');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('12','烽火S5700-52T-X');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('12','烽火S5800-28F-S');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('12','烽火S5800-28T-S');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('12','烽火S5800-28T-S-PE');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('12','烽火S5800-52T-S');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('12','烽火S5800-52T-S-PE');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('12','烽火S6000-64F');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('12','迈普MyPower S4320-28FC');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('12','迈普MyPower S4320-28TP');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('12','迈普MyPower S4320-56TC');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('12','锐捷RG-S5510-24GT8SFP-E');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('12','锐捷RG-S5750-24GT8SFP-P');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('12','锐捷RG-S5750-24SFP12GT');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('12','锐捷RG-S5750C-48GT4XS-H');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('12','锐捷RG-S6220-48XS4QXS');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('12','锐捷RG-S7808C');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('14','华三S7603');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('14','华三S7606');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('14','华三S7610');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('14','华为S9306');
insert into `cmdb_device_type_model` (`device_type_id`, `device_model`) values('14','华为S9310X');
