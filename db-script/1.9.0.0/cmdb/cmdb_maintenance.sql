################################################################### 菜单权限
insert INTO resource_schema(resource,general,created_at,name,parent_resource)
VALUES('resource_mainten_time_statist','f',NOW(),'维保状态统计','resource_mainten_manager');

################################################################### 维保合同表
ALTER table cmdb_maintenance_project
ADD COLUMN  money_bak DECIMAL(20,6);

ALTER table cmdb_maintenance_project
ADD COLUMN  pre_tax_bak DECIMAL(20,6);

ALTER table cmdb_maintenance_project
ADD COLUMN  after_tax_bak DECIMAL(20,6);

ALTER table cmdb_maintenance_project
ADD COLUMN  unit_price_bak DECIMAL(20,6);

ALTER table cmdb_maintenance_project
ADD COLUMN  total_price_bak DECIMAL(20,6);

ALTER table cmdb_maintenance_project
ADD COLUMN  discount_amount_bak DECIMAL(20,6);

UPDATE cmdb_maintenance_project
SET 
	money_bak = ROUND(money,6),
	pre_tax_bak = ROUND(pre_tax,6),
	after_tax_bak = ROUND(after_tax,6),
	unit_price_bak = ROUND(unit_price,6),
	total_price_bak = ROUND(total_price,6),
	discount_amount_bak = ROUND(discount_amount,6);

ALTER table cmdb_maintenance_project
DROP COLUMN money;

ALTER table cmdb_maintenance_project
DROP COLUMN pre_tax;

ALTER table cmdb_maintenance_project
DROP COLUMN after_tax;

ALTER table cmdb_maintenance_project
DROP COLUMN unit_price;

ALTER table cmdb_maintenance_project
DROP COLUMN total_price;

ALTER table cmdb_maintenance_project
DROP COLUMN discount_amount;

ALTER table cmdb_maintenance_project
CHANGE money_bak money DECIMAL(20,6);

ALTER table cmdb_maintenance_project
CHANGE pre_tax_bak pre_tax DECIMAL(20,6);

ALTER table cmdb_maintenance_project
CHANGE after_tax_bak after_tax DECIMAL(20,6);

ALTER table cmdb_maintenance_project
CHANGE unit_price_bak unit_price DECIMAL(20,6);

ALTER table cmdb_maintenance_project
CHANGE total_price_bak total_price DECIMAL(20,6);

ALTER table cmdb_maintenance_project
CHANGE discount_amount_bak discount_amount DECIMAL(20,6);

################################################################### 硬件维保表
ALTER table cmdb_mainten_hardware
ADD COLUMN  pre_tax_bak DECIMAL(20,6);

ALTER table cmdb_mainten_hardware
ADD COLUMN  after_tax_bak DECIMAL(20,6);

ALTER table cmdb_mainten_hardware
ADD COLUMN  unit_price_bak DECIMAL(20,6);

ALTER table cmdb_mainten_hardware
ADD COLUMN  total_price_bak DECIMAL(20,6);

ALTER table cmdb_mainten_hardware
ADD COLUMN  discount_amount_bak DECIMAL(20,6);

UPDATE cmdb_mainten_hardware
SET 
	pre_tax_bak = ROUND(pre_tax,6),
	after_tax_bak = ROUND(after_tax,6),
	unit_price_bak = ROUND(unit_price,6),
	total_price_bak = ROUND(total_price,6),
	discount_amount_bak = ROUND(discount_amount,6);

ALTER table cmdb_mainten_hardware
DROP COLUMN pre_tax;

ALTER table cmdb_mainten_hardware
DROP COLUMN after_tax;

ALTER table cmdb_mainten_hardware
DROP COLUMN unit_price;

ALTER table cmdb_mainten_hardware
DROP COLUMN total_price;

ALTER table cmdb_mainten_hardware
DROP COLUMN discount_amount;

ALTER table cmdb_mainten_hardware
CHANGE pre_tax_bak pre_tax DECIMAL(20,6);

ALTER table cmdb_mainten_hardware
CHANGE after_tax_bak after_tax DECIMAL(20,6);

ALTER table cmdb_mainten_hardware
CHANGE unit_price_bak unit_price DECIMAL(20,6);

ALTER table cmdb_mainten_hardware
CHANGE total_price_bak total_price DECIMAL(20,6);

ALTER table cmdb_mainten_hardware
CHANGE discount_amount_bak discount_amount DECIMAL(20,6);