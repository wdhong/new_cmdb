/*
Navicat MySQL Data Transfer

Source Server         : bpm
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : cmdb

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2020-01-02 11:23:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmdb_maintenance_person_info
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_maintenance_person_info`;
CREATE TABLE `cmdb_maintenance_person_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `resource_pool` varchar(50) DEFAULT NULL COMMENT '资源池',
  `device_class` varchar(50) DEFAULT NULL COMMENT '设备分类',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `phone` varchar(50) DEFAULT NULL COMMENT '电话',
  `email` varchar(50) DEFAULT NULL COMMENT '邮件',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='维护人员信息';


insert into cmdb_maintenance_person_info(resource_pool,device_class,name,phone,email) VALUES('信息港资源池','服务器','郭海鹏','18710032554','guohaipeng@aspirecn.com');
insert into cmdb_maintenance_person_info(resource_pool,device_class,name,phone,email) VALUES('信息港资源池','网络设备','信海鹏','13466591860','xinhaipeng@aspirecn.com');
insert into cmdb_maintenance_person_info(resource_pool,device_class,name,phone,email) VALUES('信息港资源池','安全设备','曹瑞需','13811241927','caoruixue@aspirecn.com');
insert into cmdb_maintenance_person_info(resource_pool,device_class,name,phone,email) VALUES('信息港资源池','存储设备','王鑫','13810134841','wangxin_b@aspirecn.com');

insert into cmdb_maintenance_person_info(resource_pool,device_class,name,phone,email) VALUES('呼和浩特资源池','服务器','龚文远','13947143381','gongwenyuan@aspirecn.com');
insert into cmdb_maintenance_person_info(resource_pool,device_class,name,phone,email) VALUES('呼和浩特资源池','网络设备','张西现','13664883230','zhangxixian@aspirecn.com');
insert into cmdb_maintenance_person_info(resource_pool,device_class,name,phone,email) VALUES('呼和浩特资源池','安全设备','张志刚','15848926088','zhangzhigang@aspirecn.com');
insert into cmdb_maintenance_person_info(resource_pool,device_class,name,phone,email) VALUES('呼和浩特资源池','存储设备','曲前龙','13214010196','quqianlong@aspirecn.com');

insert into cmdb_maintenance_person_info(resource_pool,device_class,name,phone,email) VALUES('哈尔滨资源池','服务器','陶向阳','15004667312','taoxy@aspirecn.com');
insert into cmdb_maintenance_person_info(resource_pool,device_class,name,phone,email) VALUES('哈尔滨资源池','网络设备','李卓','13895700282','lizhuo_a@aspirecn.com');
insert into cmdb_maintenance_person_info(resource_pool,device_class,name,phone,email) VALUES('哈尔滨资源池','安全设备','李卓','13895700282','lizhuo_a@aspirecn.com');
insert into cmdb_maintenance_person_info(resource_pool,device_class,name,phone,email) VALUES('哈尔滨资源池','存储设备','陶向阳','15004667312','taoxy@aspirecn.com');

insert into cmdb_maintenance_person_info(resource_pool,device_class,name,phone,email) VALUES('北京池外','服务器','王伟','13720070600','wangwei_e@aspirecn.com');
insert into cmdb_maintenance_person_info(resource_pool,device_class,name,phone,email) VALUES('北京池外','网络设备','祁冬雨','13910704770','qidongyu@aspirecn.com');
insert into cmdb_maintenance_person_info(resource_pool,device_class,name,phone,email) VALUES('北京池外','安全设备','祁冬雨','13910704770','qidongyu@aspirecn.com');
insert into cmdb_maintenance_person_info(resource_pool,device_class,name,phone,email) VALUES('北京池外','存储设备','王伟','13720070600','wangwei_e@aspirecn.com');

insert into cmdb_maintenance_person_info(resource_pool,device_class,name,phone,email) VALUES('深圳池外','服务器','杜左虎','13903029612','duzuohu@aspirecn.com');
insert into cmdb_maintenance_person_info(resource_pool,device_class,name,phone,email) VALUES('深圳池外','网络设备','杜左虎','13903029612','duzuohu@aspirecn.com');
insert into cmdb_maintenance_person_info(resource_pool,device_class,name,phone,email) VALUES('深圳池外','安全设备','杜左虎','13903029612','duzuohu@aspirecn.com');
insert into cmdb_maintenance_person_info(resource_pool,device_class,name,phone,email) VALUES('深圳池外','存储设备','杜左虎','13903029612','duzuohu@aspirecn.com');
