
INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) 
VALUES ('alert_analyse', 'f', '2019-10-09 17:49:08', '告警分析', 'alert');

INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) 
VALUES ('idc_device_type_alert', 'f', '2019-07-03 05:00:00.000000', '资源池设备类型告警', 'alert_analyse');

INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) 
VALUES ('idc_alert', 'f', '2019-07-03 05:00:00.000000', '资源池告警趋势', 'alert_analyse');

INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) 
VALUES ('device_mfrs_alert', 'f', '2019-07-03 05:00:00.000000', '厂家告警TOP10', 'alert_analyse');

INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) 
VALUES ('moni_index_alert', 'f', '2019-07-03 05:00:00.000000', '告警指标TOP10', 'alert_analyse');

INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) 
VALUES ('device_model_alert', 'f', '2019-07-03 05:00:00.000000', '设备型号TOP10', 'alert_analyse');

INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) 
VALUES ('device_ip_alert', 'f', '2019-07-03 05:00:00.000000', '告警设备TOP10', 'alert_analyse');

INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('network-optimize', 'f', '2019-10-09 17:49:08.000000', '网络优化', 'net_config_manage');
