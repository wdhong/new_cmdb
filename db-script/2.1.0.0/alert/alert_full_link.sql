insert  into `kpi_config_key`(`config_id`,`key_name`,`kpi_key`,`kpi_key_model`,`kpi_key_field`,`description`) values 
('1001','磁盘利用率','vfs.fs.size[%,pused]','2','key_','磁盘利用率'),
('1001','内存利用率','custom.memory.pused','1','key_','内存利用率'),
('1001','cpu利用率','custom.cpu.avg5.pused','1','key_','cpu利用率'),
('1001','Zabbix_Proxy进程','proc.num[,,,zabbix_proxy]','1','key_','Zabbix_Proxy进程'),
('1001','DB进程','proc.num[,,,mysql]','1','key_','DB进程'),
('1001','Zabbix_Server进程','proc.num[,,,zabbix_server]','1','key_','Zabbix_Server进程'),
('1001','进程存活状态','proc.num[,,,logstash]','1','key_','进程存活状态'),
('1001','端口存活状态','net.tcp.listen[9092]','1','key_','端口存活状态'),
('1001','{#TOPICNAME} TOPIC 待消费数据量','lag_total[','2','key_','{#TOPICNAME} TOPIC 待消费数据量'),
('1001','ElasticSearch集群节点数','key_elastic[number_nodes]','1','key_','ElasticSearch集群节点数'),
('1001','ElasticSearch集群可用的分片数','key_elastic[active_shards]','1','key_','ElasticSearch集群可用的分片数'),
('1001','ElasticSearch集群正在迁移的分片数','key_elastic[relocating_shards]','1','key_','ElasticSearch集群正在迁移的分片数'),
('1001','ElasticSearch集群正在初始化的分片数','key_elastic[initializing_shards]','1','key_','ElasticSearch集群正在初始化的分片数'),
('1001','ElasticSearch集群未分配的分片数','key_elastic[unassigned_shards]','1','key_','ElasticSearch集群未分配的分片数'),
('1001','ElasticSearch集群数据节点数','key_elastic[data_nodes]','1','key_','ElasticSearch集群数据节点数'),
('1001','ElasticSearch集群状态','key_elastic[status]','1','key_','ElasticSearch集群状态'),
('1001','ElasticSearch集群可用分片数占总分片的比例','key_elastic[active_shards_percent_as_number]','1','key_','ElasticSearch集群可用分片数占总分片的比例'),
('1001','ElasticSearch集群主分片数','key_elastic[active_primary_shards]','1','key_','ElasticSearch集群主分片数'),
('1001','ElasticSearch进程','proc.num[,,,elasticsearch]','1','key_','ElasticSearch进程'),
('1001','ElasticSearch端口','net.tcp.listen[9200]','1','key_','ElasticSearch端口'),
('1001','ElasticSearch端口','net.tcp.listen[9201]','1','key_','ElasticSearch端口'),
('1001','ElasticSearch端口','net.tcp.listen[9202]','1','key_','ElasticSearch端口'),
('1001','ElasticSearch端口','net.tcp.listen[9203]','1','key_','ElasticSearch端口'),
('1001','ElasticSearch端口','net.tcp.listen[9204]','1','key_','ElasticSearch端口'),
('1001','{#NODERNAME} 磁盘总量','disk_total[','2','key_','{#NODERNAME} 磁盘总量'),
('1001','{#NODERNAME} 磁盘使用率','disk_pused[','2','key_','{#NODERNAME} 磁盘使用率'),
('1002','磁盘利用率','vfs.fs.size[%,pused]','2','key_','磁盘利用率'),
('1002','内存利用率','custom.memory.pused','1','key_','内存利用率'),
('1002','cpu利用率','custom.cpu.avg5.pused','1','key_','cpu利用率'),
('1002','Zabbix_Proxy进程','proc.num[,,,zabbix_proxy]','1','key_','Zabbix_Proxy进程'),
('1002','DB进程','proc.num[,,,mysql]','1','key_','DB进程'),
('1002','Zabbix_Server进程','proc.num[,,,zabbix_server]','1','key_','Zabbix_Server进程'),
('1002','进程存活状态','proc.num[,,,logstash]','1','key_','进程存活状态'),
('1002','端口存活状态','net.tcp.listen[9092]','1','key_','端口存活状态'),
('1002','{#TOPICNAME} TOPIC 待消费数据量','lag_total[','2','key_','{#TOPICNAME} TOPIC 待消费数据量'),
('1002','ElasticSearch集群节点数','key_elastic[number_nodes]','1','key_','ElasticSearch集群节点数'),
('1002','ElasticSearch集群可用的分片数','key_elastic[active_shards]','1','key_','ElasticSearch集群可用的分片数'),
('1002','ElasticSearch集群正在迁移的分片数','key_elastic[relocating_shards]','1','key_','ElasticSearch集群正在迁移的分片数'),
('1002','ElasticSearch集群正在初始化的分片数','key_elastic[initializing_shards]','1','key_','ElasticSearch集群正在初始化的分片数'),
('1002','ElasticSearch集群未分配的分片数','key_elastic[unassigned_shards]','1','key_','ElasticSearch集群未分配的分片数'),
('1002','ElasticSearch集群数据节点数','key_elastic[data_nodes]','1','key_','ElasticSearch集群数据节点数'),
('1002','ElasticSearch集群状态','key_elastic[status]','1','key_','ElasticSearch集群状态'),
('1002','ElasticSearch集群可用分片数占总分片的比例','key_elastic[active_shards_percent_as_number]','1','key_','ElasticSearch集群可用分片数占总分片的比例'),
('1002','ElasticSearch集群主分片数','key_elastic[active_primary_shards]','1','key_','ElasticSearch集群主分片数'),
('1002','ElasticSearch进程','proc.num[,,,elasticsearch]','1','key_','ElasticSearch进程'),
('1002','ElasticSearch端口','net.tcp.listen[9200]','1','key_','ElasticSearch端口'),
('1002','ElasticSearch端口','net.tcp.listen[9201]','1','key_','ElasticSearch端口'),
('1002','ElasticSearch端口','net.tcp.listen[9202]','1','key_','ElasticSearch端口'),
('1002','ElasticSearch端口','net.tcp.listen[9203]','1','key_','ElasticSearch端口'),
('1002','ElasticSearch端口','net.tcp.listen[9204]','1','key_','ElasticSearch端口'),
('1002','{#NODERNAME} 磁盘总量','disk_total[','2','key_','{#NODERNAME} 磁盘总量'),
('1002','{#NODERNAME} 磁盘使用率','disk_pused[','2','key_','{#NODERNAME} 磁盘使用率');

INSERT  INTO `kpi_config_key`(`config_id`,`key_name`,`kpi_key`,`kpi_key_model`,`kpi_key_field`,`description`) VALUES 
('full_link_key','Zabbix_Proxy进程','proc.num[,,,zabbix_proxy]','1','key_','Zabbix_Proxy进程'),
('full_link_key','Zabbix_Server进程','proc.num[,,,zabbix_server]','1','key_','Zabbix_Server进程'),
('full_link_key','进程存活状态','proc.num[,,,logstash]','1','key_','进程存活状态'),
('full_link_key','端口存活状态','net.tcp.listen[9092]','1','key_','端口存活状态'),
('full_link_key','{#TOPICNAME} TOPIC 待消费数据量','lag_total[','2','key_','{#TOPICNAME} TOPIC 待消费数据量'),
('full_link_key','ElasticSearch集群节点数','key_elastic[number_nodes]','1','key_','ElasticSearch集群节点数'),
('full_link_key','ElasticSearch集群可用的分片数','key_elastic[active_shards]','1','key_','ElasticSearch集群可用的分片数'),
('full_link_key','ElasticSearch集群正在迁移的分片数','key_elastic[relocating_shards]','1','key_','ElasticSearch集群正在迁移的分片数'),
('full_link_key','ElasticSearch集群正在初始化的分片数','key_elastic[initializing_shards]','1','key_','ElasticSearch集群正在初始化的分片数'),
('full_link_key','ElasticSearch集群未分配的分片数','key_elastic[unassigned_shards]','1','key_','ElasticSearch集群未分配的分片数'),
('full_link_key','ElasticSearch集群数据节点数','key_elastic[data_nodes]','1','key_','ElasticSearch集群数据节点数'),
('full_link_key','ElasticSearch集群状态','key_elastic[status]','1','key_','ElasticSearch集群状态'),
('full_link_key','ElasticSearch集群可用分片数占总分片的比例','key_elastic[active_shards_percent_as_number]','1','key_','ElasticSearch集群可用分片数占总分片的比例'),
('full_link_key','ElasticSearch集群主分片数','key_elastic[active_primary_shards]','1','key_','ElasticSearch集群主分片数'),
('full_link_key','ElasticSearch进程','proc.num[,,,elasticsearch]','1','key_','ElasticSearch进程'),
('full_link_key','ElasticSearch端口','net.tcp.listen[920','2','key_','ElasticSearch端口');

insert  into `kpi_config_manage`(`id`,`kpi_name`,`kpi_type`,`date_source`,`cron`,`insert_object`,`insert_object_type`,`relation_ci_fields`,`exec_object`,`exec_format_type`,`exec_format`,`exec_filter`,`status`,`is_delete`,`start_time`,`duration`,`end_time`,`creater`,`create_time`,`updater`,`update_time`) values 
('1001','自监控history数据','4','zabbix','0 */5 * * * ?','kpi_monitor_full_link','db','idcType,device_class',NULL,'1','select h.itemid item_id, ite.key_ component_name, ite.key_ kpi_key, ite.name kpi_name, ite.ip ip,h.VALUE value, ite.units, h.clock , from_unixtime(h.clock) date_time from history h INNER JOIN (SELECT i.itemid, i.key_, i.name, i.units, it.ip  from items i INNER JOIN interface it on it.hostid=i.hostid where it.ip in #{ipList} and #{keys} and i.value_type= 0 ) ite on ite.itemid=h.itemid where  h.clock >= UNIX_TIMESTAMP(#{fromTime}) AND h.clock < UNIX_TIMESTAMP(#{toTime})',NULL,'1','0',NULL,10,NULL,'baiwenping','2021-01-12 16:48:13','baiwenping','2021-01-12 16:48:13'),
('1002','自监控history_uint','4','zabbix','0 */5 * * * ?','kpi_monitor_full_link','db','idcType,device_class',NULL,'1','select h.itemid item_id, ite.key_ component_name, ite.key_ kpi_key, ite.name kpi_name, ite.ip ip,h.VALUE value, ite.units, h.clock , from_unixtime(h.clock) date_time from history_uint h INNER JOIN (SELECT i.itemid, i.key_, i.name, i.units, it.ip  from items i INNER JOIN interface it on it.hostid=i.hostid where it.ip in #{ipList} and #{keys} and i.value_type= 3 ) ite on ite.itemid=h.itemid where  h.clock >= UNIX_TIMESTAMP(#{fromTime}) AND h.clock < UNIX_TIMESTAMP(#{toTime})',NULL,'1','0',NULL,10,NULL,'baiwenping','2021-01-12 16:48:13','baiwenping','2021-01-12 16:48:13');

insert  into `kpi_result_transfer`(`config_id`,`result_transfer_name`,`field_group`,`source_field`,`target_field`,`transfer_type`,`transfer_formula`,`description`,`is_delete`) values 
('1001','资源池','设备','idcType','idc_type',NULL,NULL,NULL,'0'),
('1002','资源池','设备','idcType','idc_type',NULL,NULL,NULL,'0'),
('1001','指标名称','公共','kpi_name','kpi_name',NULL,NULL,NULL,'0'),
('1002','指标名称','公共','kpi_name','kpi_name',NULL,NULL,NULL,'0'),
('1001','监控项id','公共','item_id','item_id',NULL,NULL,NULL,'0'),
('1002','监控项id','公共','item_id','item_id',NULL,NULL,NULL,'0'),
('1001','监控项key','公共','kpi_key','kpi_key',NULL,NULL,NULL,'0'),
('1002','监控项key','公共','kpi_key','kpi_key',NULL,NULL,NULL,'0'),
('1001','时间','公共','clock','clock',NULL,NULL,NULL,'0'),
('1002','时间','公共','clock','clock',NULL,NULL,NULL,'0'),
('1001','设备ip','设备','ip','ip',NULL,NULL,NULL,'0'),
('1002','设备ip','设备','ip','ip',NULL,NULL,NULL,'0'),
('1001','设备分类','设备','device_class','device_class',NULL,NULL,NULL,'0'),
('1002','设备分类','设备','device_class','device_class',NULL,NULL,NULL,'0'),
('1001','监控值','公共','value','value',NULL,NULL,NULL,'0'),
('1002','监控值','公共','value','value',NULL,NULL,NULL,'0'),
('1001','单位','公共','units','units',NULL,NULL,NULL,'0'),
('1002','单位','公共','units','units',NULL,NULL,NULL,'0');
INSERT  INTO `kpi_result_transfer`(`config_id`,`result_transfer_name`,`field_group`,`source_field`,`target_field`,`transfer_type`,`transfer_formula`,`description`,`is_delete`) VALUES 
('1001','组件名称','公共','component_name','component_name','4','component_name',NULL,'0'),
('1002','组件名称','公共','component_name','component_name','4','component_name',NULL,'0');

insert into `kpi_config_dict` (`dict_type`, `dict_name`, `dict_value`, `description`) values('component_name','vfs.fs.size','OS维度',NULL);
insert into `kpi_config_dict` (`dict_type`, `dict_name`, `dict_value`, `description`) values('component_name','custom.memory.pused','OS维度',NULL);
insert into `kpi_config_dict` (`dict_type`, `dict_name`, `dict_value`, `description`) values('component_name','custom.cpu.avg5.pused','OS维度',NULL);

insert into `kpi_config_dict` (`dict_type`, `dict_name`, `dict_value`, `description`) values('component_name','proc.num[,,,zabbix_proxy]','zabbix',NULL);
insert into `kpi_config_dict` (`dict_type`, `dict_name`, `dict_value`, `description`) values('component_name','proc.num[,,,mysql]','zabbix',NULL);
insert into `kpi_config_dict` (`dict_type`, `dict_name`, `dict_value`, `description`) values('component_name','proc.num[,,,zabbix_server]','zabbix',NULL);
insert into `kpi_config_dict` (`dict_type`, `dict_name`, `dict_value`, `description`) values('component_name','proc.num[,,,logstash]','logstash',NULL);
insert into `kpi_config_dict` (`dict_type`, `dict_name`, `dict_value`, `description`) values('component_name','net.tcp.listen[9092]','kafka',NULL);
insert into `kpi_config_dict` (`dict_type`, `dict_name`, `dict_value`, `description`) values('component_name','lag_total','kafka',NULL);
insert into `kpi_config_dict` (`dict_type`, `dict_name`, `dict_value`, `description`) values('component_name','key_elastic[number_nodes]','elasticsearch',NULL);
insert into `kpi_config_dict` (`dict_type`, `dict_name`, `dict_value`, `description`) values('component_name','key_elastic[active_shards]','elasticsearch',NULL);
insert into `kpi_config_dict` (`dict_type`, `dict_name`, `dict_value`, `description`) values('component_name','key_elastic[relocating_shards]','elasticsearch',NULL);
insert into `kpi_config_dict` (`dict_type`, `dict_name`, `dict_value`, `description`) values('component_name','key_elastic[initializing_shards]','elasticsearch',NULL);
insert into `kpi_config_dict` (`dict_type`, `dict_name`, `dict_value`, `description`) values('component_name','key_elastic[unassigned_shards]','elasticsearch',NULL);
insert into `kpi_config_dict` (`dict_type`, `dict_name`, `dict_value`, `description`) values('component_name','key_elastic[data_nodes]','elasticsearch',NULL);
insert into `kpi_config_dict` (`dict_type`, `dict_name`, `dict_value`, `description`) values('component_name','key_elastic[status]','elasticsearch',NULL);
insert into `kpi_config_dict` (`dict_type`, `dict_name`, `dict_value`, `description`) values('component_name','key_elastic[active_shards_percent_as_number]','elasticsearch',NULL);
insert into `kpi_config_dict` (`dict_type`, `dict_name`, `dict_value`, `description`) values('component_name','key_elastic[active_primary_shards]','elasticsearch',NULL);
insert into `kpi_config_dict` (`dict_type`, `dict_name`, `dict_value`, `description`) values('component_name','proc.num[,,,elasticsearch]','elasticsearch',NULL);
insert into `kpi_config_dict` (`dict_type`, `dict_name`, `dict_value`, `description`) values('component_name','net.tcp.listen[920','elasticsearch',NULL);
insert into `kpi_config_dict` (`dict_type`, `dict_name`, `dict_value`, `description`) values('component_name','disk_total[','elasticsearch',NULL);
insert into `kpi_config_dict` (`dict_type`, `dict_name`, `dict_value`, `description`) values('component_name','disk_pused[','elasticsearch',NULL);

DROP TABLE IF EXISTS `kpi_monitor_full_link`;

CREATE TABLE `kpi_monitor_full_link` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `item_id` varchar(32) NOT NULL,
  `ip` varchar(64) NOT NULL,
  `idc_type` varchar(64) DEFAULT NULL,
  `device_class` varchar(64) DEFAULT NULL,
  `component_name` varchar(64) default null,
  `kpi_key` varchar(255) NOT NULL,
  `kpi_name` varchar(255) DEFAULT NULL,
  `value` double NOT NULL,
  `units` varchar(32) DEFAULT NULL,
  `clock` int(11) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5533 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `kpi_monitor_full_link_ip`;
CREATE TABLE `kpi_monitor_full_link_ip` (  
  `tag` VARCHAR(64) NOT NULL,
  `ip` VARCHAR(64) NOT NULL,
  `date_time` DATETIME,
  PRIMARY KEY (`tag`, `ip`) 
)
COMMENT='自监控ip校验表';
