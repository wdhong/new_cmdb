/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 5.7.30-log 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

insert into `cmdb_config` (`id`, `config_code`, `config_value`, `config_value_type`, `config_remark`) values('88a2b137bc5c43f5986cb3049103c766','module_right_grant','{\r\n	\"96603733-5793-11ea-ab96-fa163e982f89\": {\r\n		\"idcType\": \"idcType\",\r\n		\"device_type\": \"device_type\",\r\n		\"department1\": \"department1\",\r\n		\"department2\": \"department2\",\r\n		\"bizSystem\": \"bizSystem\",\r\n		\"device\": \"id\",\r\n		\"roomId\": \"roomId\"\r\n	},\r\n	\"9409510cdcc14228a1ac2bbef35f9eca\": {\r\n		\"idcType\": \"id\"\r\n	},\r\n	\"9212e88a698d43cbbf9ec35b83773e2d\": {\r\n		\"bizSystem\": \"id\"\r\n	},\r\n	\"c45c3fee780943b0bf233e03913ca5b4\": {\r\n		\"department1\": \"id\",\r\n		\"department2\": \"id\"\r\n	}\r\n}','json','CMDB各模型权限配置');
