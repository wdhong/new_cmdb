/*
Navicat MySQL Data Transfer

Source Server         : UMS
Source Server Version : 50730
Source Host           : 10.12.70.40:3306
Source Database       : cmdb_zy_online

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2021-01-29 19:17:15
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `year_report_department_info`
-- ----------------------------
DROP TABLE IF EXISTS `year_report_department_info`;
CREATE TABLE `year_report_department_info` (
  `department1` varchar(40) DEFAULT NULL COMMENT '一级部门',
  `department2` varchar(40) DEFAULT NULL COMMENT '二级部门',
  `content` varchar(1000) DEFAULT NULL COMMENT '内容',
  `sort_index` int(11) DEFAULT NULL COMMENT '排序'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of year_report_department_info
-- ----------------------------
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '安全管理中心', '2020年10月30日，您的\"全网安全运营管理平台\"顺利上云；', '3');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '大数据应用部', '2020年09月07日，您的\"统一欺诈防控平台FMS系统\"顺利上云；', '3');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '管理信息系统部', '2020年10月29日，您的\"集中化计划建设管理系统\"顺利上云；', '3');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '广州业务支撑中心', '2020年07月31日，您的\"集中化BOMC系统（哈池节点）\"顺利上云；', '3');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '基础平台部', '2020年06月10日，您的\"IT云业务上线测试\"顺利上云；', '3');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '政企业务支撑中心', '2020年07月16日，您的\"物联网智能服务支撑系统\"顺利上云；', '3');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '中移信息公司', '2020年02月25日，您的\"一级BOMC系统\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '交易业务运营中心', '2020年03月25日，您的\"一级充值支付中心\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '企业管理支撑中心', '2020年03月06日，您的\"中国移动ERP域系统集中化\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '信息技术中心', '2020年04月07日，您的\"中国移动计费支撑网一级内容计费结算系统\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '全网监控与支持中心', '2020年06月03日，您的\"中移信息公司合作外包工作信息化建设\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '北京业务支撑中心', '2020年03月10日，您的\"SIMS\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '国际业务运营中心', '2020年03月30日，您的\"国际业务支撑系统一期\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '基础平台部', '2020年07月02日，您的\"IT云服务台\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '大数据平台部', '2020年02月17日，您的\"中国移动一级增值业务综合运营平台(VGOP)\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '大数据应用部', '2020年04月01日，您的\"中国移动一级增值业务综合运营平台(VGOP)\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '天津中心', '2020年01月21日，您的\"基础平台--PAAS平台\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '安全管理中心', '2020年01月19日，您的\"全网威胁与预警平台\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '广州业务支撑中心', '2020年06月01日，您的\"4A系统广州\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '政企业务支撑中心', '2020年01月07日，您的\"CTBOSS系统\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '深圳业务支撑中心', '2020年05月07日，您的\"4A系统\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '电子渠道运营', '2020年02月26日，您的\"中国移动一级电渠\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '电子渠道运营中心', '2020年06月08日，您的\"中国移动一级电渠\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '研发创新中心', '2020年05月06日，您的\"ailink业务系统\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '研发测试中心', '2020年03月27日，您的\"云计算PaaS平台\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '管理信息系统部', '2020年02月25日，您的\"中国移动党建云平台\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '运营管理部', '2020年05月25日，您的\"全网监控系统\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '黑龙江中心', '2020年03月31日，您的\"业支资源管理平台\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '数字化运营支撑中心', '2020年12月17日，您的\"一级营销资源管理系统\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '苏州研发中心', '2020年08月03日，您的\"IT云管平台\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('上海公司', null, '2020年08月27日，您的\"4A系统\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('中国移动设计院', null, '2020年07月02日，您的\"设计院数字设计平台\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('中移互联网公司', null, '2020年04月10日，您的\"中国移动一级业务支撑系统4A管理平台\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('中移系统集成有限公司', null, '2020年03月30日，您的\"云计算PaaS平台\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('互联网公司', null, '2020年08月03日，您的\"互联网公司5G消息不良信息管控平台\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('信安中心', null, '2020年11月10日，您的\"APT集中监测分析平台\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('吉林公司', null, '2020年10月29日，您的\"全网集中监控系统\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('在线服务公司', null, '2020年05月12日，您的\"在线服务\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('宁夏公司', null, '2020年10月12日，您的\"集中化crm-boss全网中心华北节点\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('山西公司', null, '2020年07月07日，您的\"制度管理系统\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('成都产业研究院', null, '2020年07月06日，您的\"中国移动网上大学\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('新疆公司', null, '2020年08月31日，您的\"银企互联系统\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('杭州研发中心', null, '2020年11月12日，您的\"云计算PaaS平台\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('江苏公司', null, '2020年10月27日，您的\"江苏4A系统\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('河北公司', null, '2020年08月03日，您的\"集中化BOMC\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('河南公司', null, '2020年08月14日，您的\"4A系统\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('浙江公司', null, '2020年11月23日，您的\"安全漏洞管理平台\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('湖南公司', null, '2020年10月15日，您的\"集中化CRMBOSS全网中心华南节点-湖南\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('省公司', null, '2020年09月28日，您的\"支撑网测试平台\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('福建公司', null, '2020年11月19日，您的\"数字身份创新运营平台\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('网络事业部', null, '2020年07月24日，您的\"CDN流量管理系统\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('网络部', null, '2020年02月19日，您的\"家宽综合运维支撑系统\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('苏州研发中心', null, '2020年12月23日，您的\"云资源池统一运维管理平台BPM\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('西藏公司', null, '2020年08月31日，您的\"西藏工程测试域\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('计划建设部', null, '2020年05月08日，您的\"CMNet骨干网间流控项目\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('设计院', null, '2020年08月26日，您的\"文件中转站\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('财务公司', null, '2020年10月29日，您的\"中国移动通信集团财务有限公司信息系统云化迁移项目\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('贵州公司', null, '2020年09月03日，您的\"第三方应用测试平台\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('采购共享中心', null, '2020年07月21日，您的\"采购共享服务中心人力资源系统\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('铁通公司', null, '2020年11月19日，您的\"云文档办公协同系统\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('销售分公司', null, '2020年02月12日，您的\"中移销售分公司插码系统\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('陕西公司', null, '2020年09月09日，您的\"一级IT云资源池\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('集成公司', null, '2020年07月10日，您的\"智慧城市\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('青海公司', null, '2020年10月13日，您的\"集中化CRMBOSS全网中心华南节点-青海\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('黑龙江公司', null, '2020年07月04日，您的\"业支资源管理平台\"申请扩容；', '2');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '政企业务支撑中心', '2020年05月08日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '黑龙江中心', '2020年05月09日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '电子渠道运营中心', '2020年05月11日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '广州业务支撑中心', '2020年05月13日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '天津中心', '2020年05月19日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '全网监控与支持中心', '2020年05月27日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '企业管理支撑中心', '2020年05月28日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '青海公司', '2020年05月28日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '大数据平台部', '2020年05月29日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '甘肃中心', '2020年05月29日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '规划技术部', '2020年05月29日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '北京业务支撑中心', '2020年06月02日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '研发创新中心', '2020年06月02日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '苏州研发中心', '2020年06月02日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '交易业务运营中心', '2020年06月10日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '物联网运营中心', '2020年06月12日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '大数据应用部', '2020年06月17日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '管理信息系统部', '2020年06月22日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '河北中心', '2020年06月29日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '深圳业务支撑中心', '2020年06月30日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '运营管理部', '2020年07月01日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '安全管理中心', '2020年07月03日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '数字化运营支撑中心', '2020年07月03日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '基础平台部', '2020年07月06日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '国际业务运营中心', '2020年07月07日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '业务支撑中心', '2020年11月24日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('中移系统集成有限公司', null, '2020年05月08日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('中移互联网公司', null, '2020年05月13日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('杭州研发中心', null, '2020年06月01日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('网络事业部', null, '2020年06月02日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('中国移动设计院', null, '2020年06月04日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('卓望', null, '2020年06月22日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('黑龙江公司', null, '2020年07月01日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('山西公司', null, '2020年07月07日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('政企分公司', null, '2020年07月10日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('咪咕公司', null, '2020年07月29日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('互联网公司', null, '2020年07月31日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('在线服务公司', null, '2020年08月03日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('内蒙古公司', null, '2020年08月10日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('计划建设部', null, '2020年08月25日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('设计院', null, '2020年08月26日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('南方基地', null, '2020年08月26日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('中国移动学院', null, '2020年08月27日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('采购共享中心', null, '2020年08月31日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('新疆公司', null, '2020年08月31日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('西藏公司', null, '2020年09月01日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('河北公司', null, '2020年09月01日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('研究院', null, '2020年09月08日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('省公司', null, '2020年09月28日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('天津公司', null, '2020年10月09日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('福建公司', null, '2020年10月10日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('终端公司', null, '2020年10月10日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('上海公司', null, '2020年10月14日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('重庆公司', null, '2020年10月14日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('北京公司', null, '2020年10月14日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('四川公司', null, '2020年10月14日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('辽宁公司', null, '2020年10月15日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('吉林公司', null, '2020年10月15日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('销售分公司', null, '2020年10月19日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('信安中心', null, '2020年10月20日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('成都产业研究院', null, '2020年10月22日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('政企事业部', null, '2020年11月03日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('苏州研发中心', null, '2020年11月12日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('福建移动', null, '2020年11月17日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('铁通公司', null, '2020年11月19日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('省侧一级部门', null, '2020年11月23日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('青海公司', null, '2020年11月24日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('集成公司', null, '2020年11月25日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('香港移动', null, '2020年11月26日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('安徽公司', null, '2020年12月01日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('海南公司', null, '2020年12月01日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('宁夏公司', null, '2020年12月02日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心IT', null, '2020年12月03日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('浙江公司', null, '2020年12月03日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('网络与信息安全管理中心', null, '2020年12月04日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('政企事业部（雄安办）', null, '2020年12月07日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('集团网络事业部', null, '2020年12月07日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('中移信息技术有限公司', null, '2020年12月09日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('市场经营部', null, '2020年12月10日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('财务公司', null, '2020年12月10日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('中移在线', null, '2020年12月10日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('集团客户部', null, '2020年12月10日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('工程建设部', null, '2020年12月11日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('河南公司', null, '2020年12月11日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('卓望公司', null, '2020年12月11日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('江西公司', null, '2020年12月14日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('北京移动', null, '2020年12月14日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('湖南公司', null, '2020年12月17日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('业务支撑系统部', null, '2020年12月22日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('江苏公司', null, '2020年12月24日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('山东公司', null, '2020年12月28日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('广东公司', null, '2020年12月28日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('广西公司', null, '2020年12月28日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('贵州公司', null, '2020年12月28日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('云南公司', null, '2020年12月28日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('陕西公司', null, '2020年12月28日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('湖北公司', null, '2020年12月28日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('甘肃公司', null, '2020年12月28日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('内蒙公司', null, '2020年12月30日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('网络部', null, '2020年12月31日，您申请工单数量是全年最多的一天；', '4');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '交易业务运营中心', '2020年05月03日，是您全年CPU均峰值利用率最高的一天，达到57%；', '1');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '企业管理支撑中心', '2020年11月11日，是您全年CPU均峰值利用率最高的一天，达到34.8%；', '1');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '北京业务支撑中心', '2020年11月19日，是您全年CPU均峰值利用率最高的一天，达到41.88%；', '1');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '国际业务运营中心', '2020年06月16日，是您全年CPU均峰值利用率最高的一天，达到64.85%；', '1');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '基础平台部', '2020年08月21日，是您全年CPU均峰值利用率最高的一天，达到14.87%；', '1');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '大数据平台部', '2020年05月18日，是您全年CPU均峰值利用率最高的一天，达到58.71%；', '1');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '大数据应用部', '2020年12月19日，是您全年CPU均峰值利用率最高的一天，达到51.95%；', '1');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '安全管理中心', '2020年10月29日，是您全年CPU均峰值利用率最高的一天，达到48.04%；', '1');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '广州业务支撑中心', '2020年11月16日，是您全年CPU均峰值利用率最高的一天，达到44.37%；', '1');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '政企业务支撑中心', '2020年04月10日，是您全年CPU均峰值利用率最高的一天，达到50.4%；', '1');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '数字化运营支撑中心', '2020年07月11日，是您全年CPU均峰值利用率最高的一天，达到33.21%；', '1');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '深圳业务支撑中心', '2020年06月01日，是您全年CPU均峰值利用率最高的一天，达到50.63%；', '1');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '研发创新中心', '2020年11月18日，是您全年CPU均峰值利用率最高的一天，达到42.38%；', '1');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '管理信息系统部', '2020年10月24日，是您全年CPU均峰值利用率最高的一天，达到46.21%；', '1');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '规划技术部', '2020年05月28日，是您全年CPU均峰值利用率最高的一天，达到0.13%；', '1');
INSERT INTO `year_report_department_info` VALUES ('信息技术中心', '运营管理部', '2020年10月30日，是您全年CPU均峰值利用率最高的一天，达到52.93%；', '1');
INSERT INTO `year_report_department_info` VALUES ('上海公司', null, '2020年11月11日，是您全年CPU均峰值利用率最高的一天，达到35.9%；', '1');
INSERT INTO `year_report_department_info` VALUES ('互联网公司', null, '2020年09月29日，是您全年CPU均峰值利用率最高的一天，达到34.54%；', '1');
INSERT INTO `year_report_department_info` VALUES ('信安中心', null, '2020年08月21日，是您全年CPU均峰值利用率最高的一天，达到49.56%；', '1');
INSERT INTO `year_report_department_info` VALUES ('党组工作部', null, '2020年08月11日，是您全年CPU均峰值利用率最高的一天，达到0.36%；', '1');
INSERT INTO `year_report_department_info` VALUES ('卓望公司', null, '2020年10月30日，是您全年CPU均峰值利用率最高的一天，达到69.49%；', '1');
INSERT INTO `year_report_department_info` VALUES ('吉林公司', null, '2020年12月25日，是您全年CPU均峰值利用率最高的一天，达到7.96%；', '1');
INSERT INTO `year_report_department_info` VALUES ('咪咕公司', null, '2020年08月25日，是您全年CPU均峰值利用率最高的一天，达到37.02%；', '1');
INSERT INTO `year_report_department_info` VALUES ('在线服务公司', null, '2020年08月21日，是您全年CPU均峰值利用率最高的一天，达到36.72%；', '1');
INSERT INTO `year_report_department_info` VALUES ('天津公司', null, '2020年10月29日，是您全年CPU均峰值利用率最高的一天，达到41.72%；', '1');
INSERT INTO `year_report_department_info` VALUES ('宁夏公司', null, '2020年12月12日，是您全年CPU均峰值利用率最高的一天，达到17.55%；', '1');
INSERT INTO `year_report_department_info` VALUES ('山西公司', null, '2020年10月30日，是您全年CPU均峰值利用率最高的一天，达到50.04%；', '1');
INSERT INTO `year_report_department_info` VALUES ('广东公司', null, '2020年07月24日，是您全年CPU均峰值利用率最高的一天，达到14.23%；', '1');
INSERT INTO `year_report_department_info` VALUES ('成都产业研究院', null, '2020年10月30日，是您全年CPU均峰值利用率最高的一天，达到11.35%；', '1');
INSERT INTO `year_report_department_info` VALUES ('技术部', null, '2020年10月29日，是您全年CPU均峰值利用率最高的一天，达到52.22%；', '1');
INSERT INTO `year_report_department_info` VALUES ('政企事业部', null, '2020年08月25日，是您全年CPU均峰值利用率最高的一天，达到67.65%；', '1');
INSERT INTO `year_report_department_info` VALUES ('杭州研发中心', null, '2020年04月25日，是您全年CPU均峰值利用率最高的一天，达到36.38%；', '1');
INSERT INTO `year_report_department_info` VALUES ('河北公司', null, '2020年12月12日，是您全年CPU均峰值利用率最高的一天，达到3.4%；', '1');
INSERT INTO `year_report_department_info` VALUES ('湖南公司', null, '2020年12月16日，是您全年CPU均峰值利用率最高的一天，达到0.46%；', '1');
INSERT INTO `year_report_department_info` VALUES ('研究院', null, '2020年10月29日，是您全年CPU均峰值利用率最高的一天，达到59.18%；', '1');
INSERT INTO `year_report_department_info` VALUES ('福建公司', null, '2020年11月11日，是您全年CPU均峰值利用率最高的一天，达到43.05%；', '1');
INSERT INTO `year_report_department_info` VALUES ('网络事业部', null, '2020年11月11日，是您全年CPU均峰值利用率最高的一天，达到14.3%；', '1');
INSERT INTO `year_report_department_info` VALUES ('网络部', null, '2020年03月11日，是您全年CPU均峰值利用率最高的一天，达到9.31%；', '1');
INSERT INTO `year_report_department_info` VALUES ('苏州研发中心', null, '2020年08月02日，是您全年CPU均峰值利用率最高的一天，达到11.33%；', '1');
INSERT INTO `year_report_department_info` VALUES ('西藏公司', null, '2020年12月20日，是您全年CPU均峰值利用率最高的一天，达到6.66%；', '1');
INSERT INTO `year_report_department_info` VALUES ('计划建设部', null, '2020年06月02日，是您全年CPU均峰值利用率最高的一天，达到40.87%；', '1');
INSERT INTO `year_report_department_info` VALUES ('设计院', null, '2020年10月29日，是您全年CPU均峰值利用率最高的一天，达到23.04%；', '1');
INSERT INTO `year_report_department_info` VALUES ('采购共享中心', null, '2020年08月07日，是您全年CPU均峰值利用率最高的一天，达到42.84%；', '1');
INSERT INTO `year_report_department_info` VALUES ('销售分公司', null, '2020年10月30日，是您全年CPU均峰值利用率最高的一天，达到87.21%；', '1');
INSERT INTO `year_report_department_info` VALUES ('集成公司', null, '2020年11月11日，是您全年CPU均峰值利用率最高的一天，达到8.96%；', '1');
INSERT INTO `year_report_department_info` VALUES ('青海公司', null, '2020年12月10日，是您全年CPU均峰值利用率最高的一天，达到2.31%；', '1');
INSERT INTO `year_report_department_info` VALUES ('香港移动', null, '2020年08月31日，是您全年CPU均峰值利用率最高的一天，达到55.21%；', '1');
INSERT INTO `year_report_department_info` VALUES ('黑龙江公司', null, '2020年03月06日，是您全年CPU均峰值利用率最高的一天，达到41.04%；', '1');
