alter table alert_alerts add column message_open varchar(10) comment '是否发短信，0：不需要发送短信；1：需要发送短信';

alter table alert_alerts add column message_send varchar(10) comment '是否已发短信，0：未发；1：已发';

alter table alert_alerts add column mail_open varchar(10) comment '是否需要发送邮件，0：不需要发送邮件；1：需要发送邮件';

alter table alert_alerts add column mail_send varchar(10) comment '是否已发邮件，0：未发；1：已发';


alter table alert_alerts_his add column message_open varchar(10) comment '是否发短信，0：不需要发送短信；1：需要发送短信';

alter table alert_alerts_his add column message_send varchar(10) comment '是否已发短信，0：未发；1：已发';

alter table alert_alerts_his add column mail_open varchar(10) comment '是否需要发送邮件，0：不需要发送邮件；1：需要发送邮件';

alter table alert_alerts_his add column mail_send varchar(10) comment '是否已发邮件，0：未发；1：已发';

CREATE TABLE `monitor_log_theme_flush_time` (
	`flush_time` VARCHAR(50) NULL DEFAULT NULL COMMENT '刷新时间'
)
COMMENT='日志主题处理刷新时间'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

INSERT INTO `monitor_log_theme_flush_time` (`flush_time`) VALUES ('20190702203100');


-- ----------------------------
-- Table structure for cache_map
-- ----------------------------
DROP TABLE IF EXISTS `cache_map`;
CREATE TABLE `cache_map` (
  `cache_key` varchar(260) NOT NULL COMMENT '缓存键',
  `cache_value` text NOT NULL COMMENT '缓存值',
  PRIMARY KEY (`cache_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

