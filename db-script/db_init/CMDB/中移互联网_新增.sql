CREATE TABLE `cmdb_ip_address_pool` (
  `id` varchar(32) NOT NULL,
  `instance_id` varchar(100) NOT NULL COMMENT '实例id',
  `ip` varchar(100) DEFAULT NULL COMMENT 'IP地址',
  `iptype` varchar(100) DEFAULT NULL COMMENT 'IP类型',
  `mac` varchar(100) DEFAULT NULL COMMENT 'MAC地址',
  `resource` varchar(100) DEFAULT NULL COMMENT '所属资源池',
  `time` datetime DEFAULT NULL COMMENT '检查时间',
  `gateway` varchar(100) DEFAULT NULL COMMENT '网关设备IP',
  `event_id` varchar(255) DEFAULT NULL COMMENT '云管eventId',
  `src_creator` varchar(100) DEFAULT NULL COMMENT '云端创建人',
  `src_create_time` datetime DEFAULT NULL COMMENT '云管端的创建时间',
  `src_opt_time` datetime DEFAULT NULL COMMENT '云管端的修改时间',
  `src_pre_opt_time` datetime DEFAULT NULL COMMENT '该实例上次修改的时间',
  `version` int(4) DEFAULT NULL COMMENT '当前实例版本',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '状态 0-正常,1-删除',
  PRIMARY KEY (`id`),
  KEY `cmdb_ip_address_pool_ip_IDX` (`ip`,`time`,`del_flag`) USING BTREE,
  KEY `cmdb_ip_address_create_time_IDX` (`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='存活ip采集';

CREATE TABLE `cmdb_ip_arp_pool` (
  `id` varchar(32) NOT NULL,
  `instance_id` varchar(100) NOT NULL COMMENT '实例id',
  `ip` varchar(100) DEFAULT NULL COMMENT 'IP地址',
  `iptype` varchar(100) DEFAULT NULL COMMENT 'IP类型',
  `mac` varchar(100) DEFAULT NULL COMMENT 'MAC地址',
  `resource` varchar(100) DEFAULT NULL COMMENT '所属资源池',
  `time` datetime DEFAULT NULL COMMENT '检查时间',
  `srcip` varchar(100) DEFAULT NULL COMMENT '来源设备IP',
  `event_id` varchar(255) DEFAULT NULL COMMENT '云管eventId',
  `src_creator` varchar(100) DEFAULT NULL COMMENT '云端创建人',
  `src_create_time` datetime DEFAULT NULL COMMENT '云管端的创建时间',
  `src_opt_time` datetime DEFAULT NULL COMMENT '云管端的修改时间',
  `src_pre_opt_time` datetime DEFAULT NULL COMMENT '该实例上次修改的时间',
  `version` int(4) DEFAULT NULL COMMENT '当前实例版本',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '状态 0-正常,1-删除',
  `cpu` varchar(100) DEFAULT NULL COMMENT '指令占用内存百分比',
  `mem` varchar(100) DEFAULT NULL COMMENT '指令占用内存百分比',
  PRIMARY KEY (`id`),
  KEY `cmdb_ip_arp_pool_ip_IDX` (`ip`,`del_flag`,`time`) USING BTREE,
  KEY `cmdb_ip_address_create_time_IDX` (`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='arp扫描网段地址';

CREATE TABLE `cmdb_ip_conf_pool` (
  `id` varchar(32) NOT NULL,
  `instance_id` varchar(100) NOT NULL COMMENT '实例id',
  `ip` varchar(100) DEFAULT NULL COMMENT 'IP地址',
  `iptype` varchar(100) DEFAULT NULL COMMENT 'IP类型',
  `mac` varchar(100) DEFAULT NULL COMMENT 'MAC地址',
  `resource` varchar(100) DEFAULT NULL COMMENT '所属资源池',
  `time` datetime DEFAULT NULL COMMENT '检查时间',
  `srcip` varchar(100) DEFAULT NULL COMMENT '来源设备IP',
  `event_id` varchar(255) DEFAULT NULL COMMENT '云管eventId',
  `src_creator` varchar(100) DEFAULT NULL COMMENT '云端创建人',
  `src_create_time` datetime DEFAULT NULL COMMENT '云管端的创建时间',
  `src_opt_time` datetime DEFAULT NULL COMMENT '云管端的修改时间',
  `src_pre_opt_time` datetime DEFAULT NULL COMMENT '该实例上次修改的时间',
  `version` int(4) DEFAULT NULL COMMENT '当前实例版本',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '状态 0-正常,1-删除',
  PRIMARY KEY (`id`),
  KEY `cmdb_ip_conf_pool_ip_IDX` (`ip`,`time`,`del_flag`) USING BTREE,
  KEY `cmdb_ip_address_create_time_IDX` (`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='网络设备配置文件解析';

CREATE TABLE `cmdb_ip_clash_main` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `record_id` varchar(32) DEFAULT NULL COMMENT '关联子表ID',
  `clash_ip` varchar(100) DEFAULT NULL COMMENT '冲突IP',
  `handle_status` char(1) DEFAULT NULL COMMENT '处理状态 0-待处理,1-暂不处理,2-处理中,3-已处理',
  `not_handle_reason` varchar(255) DEFAULT NULL COMMENT '暂不处理原因',
  `operator` varchar(100) DEFAULT NULL COMMENT '操作人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '删除 0-使用,1-删除',
  `collect_type` char(1) NOT NULL DEFAULT '0' COMMENT '采集方式 0-系统,1-自动化',
  PRIMARY KEY (`id`),
  KEY `cmdb_ip_clash_main_clash_ip_IDX` (`clash_ip`) USING BTREE,
  KEY `cmdb_ip_clash_main_collect_type_IDX` (`collect_type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='冲突IP主表';

CREATE TABLE `cmdb_ip_clash_rebuild` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `request_content` text COMMENT '请求参数',
  `reponse_content` text COMMENT '响应参数',
  `convert_content` text COMMENT '响应参数Unicode转换',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `type` char(1) NOT NULL DEFAULT '0' COMMENT '发起or接收：0-发起检测,1-接收检测结果',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='冲突IP二次检测历史表';

CREATE TABLE `cmdb_ip_clash_record` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `main_id` varchar(32) DEFAULT NULL COMMENT '关联主表ID',
  `ip` varchar(50) DEFAULT NULL COMMENT 'IP地址',
  `old_mac` varchar(500) DEFAULT NULL COMMENT '上次绑定的MAC地址',
  `now_mac` varchar(500) DEFAULT NULL COMMENT '当前绑定的MAC地址',
  `gateway` varchar(50) DEFAULT NULL COMMENT '网关设备IP',
  `resource` varchar(255) DEFAULT NULL COMMENT '所属资源池',
  `job_number` varchar(50) DEFAULT NULL COMMENT '工单号',
  `check_time` datetime DEFAULT NULL COMMENT '检测时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '是否删除 0-否,1-是',
  `statistic_flag` char(1) NOT NULL DEFAULT '0' COMMENT '统计标识: 0-系统比对,1-复核比对',
  PRIMARY KEY (`id`),
  KEY `cmdb_ip_clash_record_check_time_IDX` (`check_time`,`ip`) USING BTREE,
  KEY `cmdb_ip_clash_record_main_id_IDX` (`main_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='冲突IP记录表';

