/*
Navicat MySQL Data Transfer

Source Server         : 现网-ums
Source Server Version : 50718
Source Host           : localhost:9999
Source Database       : test_mirror

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2019-12-02 14:35:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_alerts
-- ----------------------------
DROP TABLE IF EXISTS `alert_alerts`;
CREATE TABLE `alert_alerts` (
  `alert_id` varchar(64) NOT NULL,
  `r_alert_id` varchar(300) DEFAULT NULL,
  `device_id` varchar(50) DEFAULT NULL,
  `device_class` varchar(128) DEFAULT NULL COMMENT '设备类型',
  `biz_sys` varchar(128) DEFAULT NULL COMMENT '业务系统',
  `moni_index` varchar(1024) DEFAULT NULL COMMENT '监控指标/内容，关联触发器name',
  `moni_object` varchar(512) DEFAULT NULL COMMENT '监控对象',
  `item_key`  varchar(128) NULL COMMENT '监控项',
  `key_comment`  varchar(512) NULL COMMENT '监控项描述',
  `cur_moni_value` varchar(1024) DEFAULT NULL COMMENT '当前监控值',
  `cur_moni_time` datetime DEFAULT NULL COMMENT '当前监控时间',
  `alert_level` varchar(20) NOT NULL COMMENT '告警级别\r\n1-提示\r\n2-低\r\n3-中\r\n4-高\r\n5-严重\r\n',
  `item_id` varchar(50) DEFAULT NULL,
  `remark` varchar(512) DEFAULT NULL COMMENT '备注',
  `order_status` varchar(50) DEFAULT NULL COMMENT '1-未派单\r\n2-处理中\r\n3-已完成\r\n',
  `operate_status` int DEFAULT '0' COMMENT '警报操作状态:0-待确认,1-已确认,2-待解决,3-已处理,5-业务告警,6-异常信息',
  `source` varchar(100) DEFAULT NULL COMMENT '告警来源\r\nMIRROR\r\nZABBIX\r\n',
  `idc_type` varchar(128) DEFAULT NULL COMMENT '所属位置-资源池',
  `source_room` varchar(100) DEFAULT NULL COMMENT '机房/资源池',
  `device_type` varchar(128) DEFAULT NULL COMMENT '设备类型',
  `device_mfrs` varchar(128) DEFAULT NULL COMMENT '设备提供商',
  `host_name` varchar(128) DEFAULT NULL COMMENT '主机名',
  `device_model` varchar(128) DEFAULT NULL COMMENT '设备型号',
  `pod_name` varchar(128) DEFAULT NULL COMMENT 'pod池',
  `object_type` varchar(50) DEFAULT NULL COMMENT '告警类型\r\n1-系统\r\n2-业务',
  `device_ip` varchar(100) DEFAULT NULL,
  `order_id` varchar(100) DEFAULT NULL,
  `order_type` varchar(100) DEFAULT NULL,
  `alert_start_time` datetime DEFAULT NULL COMMENT '告警开始时间',
  `alert_count` int(11) DEFAULT '1',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `project_name` varchar(50) DEFAULT NULL COMMENT '工程期数',
  `ext_id`  varchar(128) NULL COMMENT '扩展id',
  PRIMARY KEY (`alert_id`),
  KEY `index_ip_level` (`device_ip`,`alert_level`,`item_id`,`cur_moni_time`) USING BTREE,
  KEY `index_biz_level` (`biz_sys`,`alert_level`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警记录表';

-- ----------------------------
-- Table structure for alert_alerts_detail
-- ----------------------------
DROP TABLE IF EXISTS `alert_alerts_detail`;
CREATE TABLE `alert_alerts_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `alert_id` varchar(64) NOT NULL,
  `action_id` bigint(10) DEFAULT NULL,
  `event_id` varchar(80) DEFAULT NULL,
  `moni_index` varchar(1024) DEFAULT NULL COMMENT '监控指标/内容，关联触发器name',
  `moni_object` varchar(512) DEFAULT NULL COMMENT '监控对象',
  `cur_moni_value` varchar(1024) DEFAULT NULL COMMENT '当前监控值',
  `cur_moni_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '当前监控时间',
  `alert_level` varchar(20) NOT NULL COMMENT '告警级别\r\n1-提示\r\n2-低\r\n3-中\r\n4-高\r\n5-严重\r\n',
  `item_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_alert_id` (`alert_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=25129925 DEFAULT CHARSET=utf8 COMMENT='告警上报记录表';

-- ----------------------------
-- Table structure for alert_alerts_his
-- ----------------------------
DROP TABLE IF EXISTS `alert_alerts_his`;
CREATE TABLE `alert_alerts_his` (
  `alert_id` varchar(64) NOT NULL,
  `r_alert_id` varchar(300) DEFAULT NULL,
  `device_id` varchar(50) DEFAULT NULL,
  `device_class` varchar(128) DEFAULT NULL COMMENT '设备类型',
  `biz_sys` varchar(128) DEFAULT NULL COMMENT '业务系统',
  `moni_index` varchar(1024) NOT NULL COMMENT '监控指标/内容，关联触发器name',
  `moni_object` varchar(512) DEFAULT NULL COMMENT '监控对象',
  `item_key`  varchar(128) NULL COMMENT '监控项',
  `key_comment`  varchar(512) NULL COMMENT '监控项描述',
  `cur_moni_value` varchar(1024) DEFAULT NULL COMMENT '当前监控值',
  `cur_moni_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '当前监控时间',
  `alert_level` varchar(20) NOT NULL COMMENT '告警级别\r\n1-提示\r\n2-低\r\n3-中\r\n4-高\r\n5-严重\r\n',
  `item_id` varchar(50) DEFAULT NULL,
  `alert_end_time` datetime DEFAULT NULL COMMENT '告警结束时间',
  `remark` varchar(512) DEFAULT NULL COMMENT '备注',
  `order_status` varchar(50) DEFAULT NULL COMMENT '1-未派单\r\n2-处理中\r\n3-已完成\r\n',
  `operate_status` int DEFAULT '0' COMMENT '警报操作状态:0-待确认,1-已确认,2-待解决,3-已处理',
  `clear_user` varchar(32) DEFAULT NULL COMMENT '清除人',
  `clear_content` varchar(256) DEFAULT NULL COMMENT '清除内容',
  `source` varchar(100) DEFAULT NULL COMMENT '告警来源\r\nMIRROR',
  `idc_type` varchar(128) DEFAULT NULL COMMENT '所属位置-资源池',
  `source_room` varchar(100) DEFAULT NULL COMMENT '机房',
  `device_type` varchar(128) DEFAULT NULL COMMENT '设备类型',
  `device_mfrs` varchar(128) DEFAULT NULL COMMENT '设备提供商',
  `host_name` varchar(128) DEFAULT NULL COMMENT '主机名',
  `device_model` varchar(128) DEFAULT NULL COMMENT '设备型号',
  `project_name` varchar(50) DEFAULT NULL COMMENT '工程期数',
  `pod_name` varchar(128) DEFAULT NULL COMMENT 'pod池',
  `object_type` varchar(50) DEFAULT NULL,
  `device_ip` varchar(100) DEFAULT NULL,
  `order_type` varchar(20) DEFAULT NULL COMMENT '工单类型\r\n1-告警\r\n2-故障',
  `order_id` varchar(100) DEFAULT NULL COMMENT '工单ID',
  `biz_sys_desc` varchar(200) DEFAULT NULL COMMENT '业务系统名称（中文）',
  `alert_start_time` datetime DEFAULT NULL COMMENT '告警开始时间',
  `alert_count` int(11) DEFAULT '1',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `ext_id`  varchar(128) NULL COMMENT '扩展id',
  PRIMARY KEY (`alert_id`),
  KEY `index_alert_analyse` (`alert_level`, `cur_moni_time`, `device_mfrs`, `idc_type`, `device_type`, `device_model`) USING BTREE,
  KEY `index_ip` (`device_ip`,`alert_level`,`item_id`,`cur_moni_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警历史记录表';

-- ----------------------------
-- Table structure for alert_alerts_statistics_sync
-- ----------------------------
DROP TABLE IF EXISTS `alert_alerts_statistics_sync`;
CREATE TABLE `alert_alerts_statistics_sync` (
  `idcType` varchar(300) DEFAULT NULL,
  `alertTotal` bigint(128) DEFAULT NULL,
  `seriousCount` bigint(128) DEFAULT NULL COMMENT '严重告警',
  `importantCount` bigint(128) DEFAULT NULL COMMENT '重要告警',
  `secondaryCount` bigint(128) DEFAULT NULL COMMENT '次要告警',
  `tipsCount` bigint(128) DEFAULT NULL COMMENT '提示告警',
  `month` varchar(24) DEFAULT NULL COMMENT '告警产生月份',
  `updateTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `index_r_alert_id` (`idcType`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警数量统计表';

-- ----------------------------
-- Table structure for alert_config
-- ----------------------------
DROP TABLE IF EXISTS `alert_config`;
CREATE TABLE `alert_config` (
  `ID` varchar(255) NOT NULL,
  `TITLE` varchar(64) DEFAULT NULL,
  `DESCRIPTION` text,
  `CREATOR` varchar(64) DEFAULT NULL,
  `CRATE_TIME` datetime DEFAULT NULL,
  `IS_START` char(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for alert_config_detail
-- ----------------------------
DROP TABLE IF EXISTS `alert_config_detail`;
CREATE TABLE `alert_config_detail` (
  `ID` varchar(32) NOT NULL,
  `ALERT_CONFIG_ID` varchar(255) DEFAULT NULL,
  `CONFIG_TYPE` char(1) DEFAULT NULL,
  `TARGET_ID` text,
  `MONIT_TYPE` varchar(64) DEFAULT NULL,
  `ALERT_LEVEL` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ALERT_CONFIG_ID_f` (`ALERT_CONFIG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for alert_derive
-- ----------------------------
DROP TABLE IF EXISTS `alert_derive`;
CREATE TABLE `alert_derive` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '告警衍生ID',
  `name` varchar(64) NOT NULL COMMENT '规则名称',
  `description` varchar(1024) DEFAULT NULL COMMENT '规则描述',
  `device_relation_type` varchar(2) DEFAULT NULL COMMENT '是否同一设备衍生：1-同一设备,2-否',
  `status` varchar(1) DEFAULT '0' COMMENT '启用状态，0-停用，1-启用',
  `effective_date` datetime DEFAULT NULL COMMENT '生效时间',
  `expire_date` datetime DEFAULT NULL COMMENT '失效时间',
  `derive_active_timeout` int(11) DEFAULT NULL COMMENT '关联时间窗(m)',
  `alert_threshold` int(11) DEFAULT NULL COMMENT '告警阈值个数',
  `operate_user` varchar(512) DEFAULT NULL COMMENT '维护用户',
  `option_condition` varchar(4000) DEFAULT NULL COMMENT '过滤条件',
  `alert_setting` varchar(4000) NOT NULL COMMENT '告警生成设置',
  `creater` varchar(64) DEFAULT NULL COMMENT '创建人',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `editer` varchar(64) DEFAULT NULL COMMENT '修改人',
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `is_delete` tinyint(4) DEFAULT '0' COMMENT '删除状态，0-未删除，1-已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8 COMMENT='告警衍生表';

-- ----------------------------
-- Table structure for alert_derive_alerts
-- ----------------------------
DROP TABLE IF EXISTS `alert_derive_alerts`;
CREATE TABLE `alert_derive_alerts` (
  `alert_id` varchar(64) NOT NULL,
  `derive_id` bigint(11) NOT NULL COMMENT '衍生ID',
  `derive_alert_id` varchar(64) NOT NULL COMMENT '衍生告警ID',
  `r_alert_id` varchar(300) DEFAULT NULL,
  `event_id` varchar(80) DEFAULT NULL,
  `action_id` bigint(10) DEFAULT NULL,
  `device_id` varchar(50) DEFAULT NULL,
  `device_class` varchar(128) DEFAULT NULL COMMENT '设备类型',
  `biz_sys` varchar(128) DEFAULT NULL COMMENT '业务系统',
  `moni_index` varchar(1024) DEFAULT NULL COMMENT '监控指标/内容，关联触发器name',
  `moni_object` varchar(512) DEFAULT NULL COMMENT '监控对象',
  `cur_moni_value` varchar(512) DEFAULT NULL COMMENT '当前监控值',
  `cur_moni_time` datetime DEFAULT NULL COMMENT '当前监控时间',
  `alert_level` varchar(20) NOT NULL COMMENT '告警级别\r\n1-提示\r\n2-低\r\n3-中\r\n4-高\r\n5-严重\r\n',
  `item_id` varchar(50) DEFAULT NULL,
  `remark` varchar(512) DEFAULT NULL COMMENT '备注',
  `source` varchar(100) DEFAULT NULL COMMENT '告警来源\r\nMIRROR\r\nZABBIX\r\n',
  `idc_type` varchar(128) DEFAULT NULL COMMENT '所属位置-资源池',
  `source_room` varchar(100) DEFAULT NULL COMMENT '机房/资源池',
  `device_type` varchar(128) DEFAULT NULL COMMENT '设备类型',
  `device_mfrs` varchar(128) DEFAULT NULL COMMENT '设备提供商',
  `host_name` varchar(128) DEFAULT NULL COMMENT '主机名',
  `device_model` varchar(128) DEFAULT NULL COMMENT '设备型号',
  `object_type` varchar(50) DEFAULT NULL COMMENT '告警类型\r\n1-系统\r\n2-业务',
  `object_id` varchar(50) DEFAULT NULL COMMENT '对象ID，如果是设备告警则是设备ID，如果是业务则是业务系统code',
  `region` varchar(50) DEFAULT NULL COMMENT '域/资源池code',
  `device_ip` varchar(100) DEFAULT NULL,
  `alert_start_time` datetime DEFAULT NULL COMMENT '告警开始时间',
  `prefix` varchar(64) DEFAULT NULL COMMENT '告警前缀，用于多套告警系统时候区分标识',
  `alert_count` int(11) DEFAULT '1',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `alert_type` varchar(2) DEFAULT '1' COMMENT '告警结果，1-告警，2-消警',
  PRIMARY KEY (`alert_id`),
  KEY `index_r_alert_id` (`r_alert_id`) USING BTREE,
  KEY `index_derive_alert_id` (`derive_alert_id`) USING BTREE,
  KEY `index_create_time` (`create_time`,`derive_id`) USING BTREE,
  KEY `index_ip` (`device_ip`,`item_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='衍生告警日志表';

-- ----------------------------
-- Table structure for alert_derive_alerts_his
-- ----------------------------
DROP TABLE IF EXISTS `alert_derive_alerts_his`;
CREATE TABLE `alert_derive_alerts_his` (
  `alert_id` varchar(64) NOT NULL,
  `derive_id` bigint(11) NOT NULL COMMENT '衍生ID',
  `derive_alert_id` varchar(64) NOT NULL COMMENT '衍生告警ID',
  `r_alert_id` varchar(300) DEFAULT NULL,
  `event_id` varchar(80) DEFAULT NULL,
  `action_id` bigint(10) DEFAULT NULL,
  `device_id` varchar(50) DEFAULT NULL,
  `device_class` varchar(128) DEFAULT NULL COMMENT '设备类型',
  `biz_sys` varchar(128) DEFAULT NULL COMMENT '业务系统',
  `moni_index` varchar(1024) DEFAULT NULL COMMENT '监控指标/内容，关联触发器name',
  `moni_object` varchar(512) DEFAULT NULL COMMENT '监控对象',
  `item_key` varchar(128) DEFAULT NULL COMMENT '监控项',
  `key_comment` varchar(256) DEFAULT NULL COMMENT '监控项描述',
  `cur_moni_value` varchar(512) DEFAULT NULL COMMENT '当前监控值',
  `cur_moni_time` datetime DEFAULT NULL COMMENT '当前监控时间',
  `alert_level` varchar(20) NOT NULL COMMENT '告警级别\r\n1-提示\r\n2-低\r\n3-中\r\n4-高\r\n5-严重\r\n',
  `item_id` varchar(50) DEFAULT NULL,
  `remark` varchar(512) DEFAULT NULL COMMENT '备注',
  `source` varchar(100) DEFAULT NULL COMMENT '告警来源\r\nMIRROR\r\nZABBIX\r\n',
  `idc_type` varchar(128) DEFAULT NULL COMMENT '所属位置-资源池',
  `source_room` varchar(100) DEFAULT NULL COMMENT '机房/资源池',
  `device_type` varchar(128) DEFAULT NULL COMMENT '设备类型',
  `device_mfrs` varchar(128) DEFAULT NULL COMMENT '设备提供商',
  `host_name` varchar(128) DEFAULT NULL COMMENT '主机名',
  `device_model` varchar(128) DEFAULT NULL COMMENT '设备型号',
  `object_type` varchar(50) DEFAULT NULL COMMENT '告警类型\r\n1-系统\r\n2-业务',
  `object_id` varchar(50) DEFAULT NULL COMMENT '对象ID，如果是设备告警则是设备ID，如果是业务则是业务系统code',
  `region` varchar(50) DEFAULT NULL COMMENT '域/资源池code',
  `device_ip` varchar(100) DEFAULT NULL,
  `alert_start_time` datetime DEFAULT NULL COMMENT '告警开始时间',
  `prefix` varchar(64) DEFAULT NULL COMMENT '告警前缀，用于多套告警系统时候区分标识',
  `alert_count` int(11) DEFAULT '1',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `alert_type` varchar(2) DEFAULT '1' COMMENT '告警结果，1-告警，2-消警',
  PRIMARY KEY (`alert_id`),
  KEY `index_derive_id` (`derive_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='衍生告警日志历史表';

-- ----------------------------
-- Table structure for alert_devicetype_config
-- ----------------------------
DROP TABLE IF EXISTS `alert_devicetype_config`;
CREATE TABLE `alert_devicetype_config` (
  `id` varchar(56) NOT NULL,
  `code` varchar(128) NOT NULL COMMENT '表里面存的名称',
  `name` varchar(255) DEFAULT NULL COMMENT '展示名称',
  `type` varchar(255) DEFAULT NULL COMMENT '类型：网络、物理服务器等'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='设备分类统计配置表';

-- ----------------------------
-- Table structure for alert_device_top_sync
-- ----------------------------
DROP TABLE IF EXISTS `alert_device_top_sync`;
CREATE TABLE `alert_device_top_sync` (
  `idcType` varchar(300) DEFAULT NULL COMMENT '资源池',
  `alertLevel` varchar(12) DEFAULT NULL COMMENT '告警级别',
  `deviceType` varchar(10) DEFAULT NULL COMMENT '设备分类',
  `countOrder` int(1) DEFAULT NULL COMMENT '排名',
  `ip` varchar(128) DEFAULT NULL COMMENT '设备ip',
  `pod` varchar(128) DEFAULT NULL COMMENT 'pod',
  `alertCount` bigint(128) DEFAULT NULL COMMENT '告警数量',
  `roomId` varchar(128) DEFAULT NULL COMMENT '机房位置',
  `month` varchar(24) DEFAULT NULL COMMENT '告警产生时间',
  `deviceMfrs` varchar(255) DEFAULT NULL COMMENT '厂家',
  `updateTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `index_r_alert_id` (`idcType`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警分类top10统计表';

-- ----------------------------
-- Table structure for alert_distribution_sync
-- ----------------------------
DROP TABLE IF EXISTS `alert_distribution_sync`;
CREATE TABLE `alert_distribution_sync` (
  `idcType` varchar(128) DEFAULT NULL,
  `alertLevel` varchar(12) DEFAULT NULL,
  `month` varchar(24) DEFAULT NULL,
  `physicalMachineCount` bigint(128) DEFAULT NULL COMMENT '防火墙',
  `firewallCount` bigint(128) DEFAULT NULL,
  `routerCount` bigint(128) DEFAULT NULL,
  `switchCount` bigint(128) DEFAULT NULL,
  `diskArrayCount` bigint(128) DEFAULT NULL,
  `tapeLibraryCount` bigint(128) DEFAULT NULL,
  `cloudStorageCount` bigint(128) DEFAULT NULL,
  `SDNControllerCount` bigint(128) DEFAULT NULL,
  `updateTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `index_r_alert_id` (`idcType`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警分布统计表';

-- ----------------------------
-- Table structure for alert_filter
-- ----------------------------
DROP TABLE IF EXISTS `alert_filter`;
CREATE TABLE `alert_filter` (
  `id` tinyint(12) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `creater` varchar(64) DEFAULT NULL,
  `editer` varchar(64) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for alert_filter_option
-- ----------------------------
DROP TABLE IF EXISTS `alert_filter_option`;
CREATE TABLE `alert_filter_option`  (
  `id` int(12) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '筛选条件名称',
  `type` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '展示类型select,string,datetime',
  `code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `operate` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '操作：大于、小于、等于、大于等于、小于等于...分号分割',
  `source` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据来源',
  `transform` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段转换',
  `content` varchar(10000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '配置数据',
  `method` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '1:使用，0：停用',
  `jdbc_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '数据类型:string,number',
  `query_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查询类型，支持填多个，1-设备;2-监控;3-次要选项;',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

-- ----------------------------
-- Table structure for alert_filter_scene
-- ----------------------------
DROP TABLE IF EXISTS `alert_filter_scene`;
CREATE TABLE `alert_filter_scene` (
  `id` tinyint(12) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL COMMENT '场景名称',
  `filter_id` tinyint(12) DEFAULT NULL COMMENT '过滤器id',
  `option_condition` varchar(4000) NOT NULL COMMENT '过滤条件',
  `operate_user` varchar(255) DEFAULT NULL COMMENT '维护用户',
  `creater` varchar(64) DEFAULT NULL,
  `editer` varchar(64) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8 COMMENT='告警过滤场景表';

-- ----------------------------
-- Table structure for alert_isolate
-- ----------------------------
DROP TABLE IF EXISTS `alert_isolate`;
CREATE TABLE `alert_isolate` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '告警屏蔽ID',
  `name` varchar(64) NOT NULL COMMENT '规则名称',
  `description` varchar(1024) DEFAULT NULL COMMENT '规则描述',
  `status` varchar(1) DEFAULT '0' COMMENT '启用状态，0-停用，1-启用',
  `effective_date` datetime DEFAULT NULL COMMENT '生效时间',
  `expire_date` datetime DEFAULT NULL COMMENT '失效时间',
  `operate_user` varchar(512) DEFAULT NULL COMMENT '维护用户',
  `option_condition` varchar(4000) DEFAULT NULL COMMENT '过滤条件',
  `creater` varchar(64) DEFAULT NULL COMMENT '创建人',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `editer` varchar(64) DEFAULT NULL COMMENT '修改人',
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `is_delete` tinyint(4) DEFAULT '0' COMMENT '删除状态，0-未删除，1-已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='告警屏蔽表';

-- ----------------------------
-- Table structure for alert_isolate_alerts
-- ----------------------------
DROP TABLE IF EXISTS `alert_isolate_alerts`;
CREATE TABLE `alert_isolate_alerts` (
  `alert_id` varchar(64) NOT NULL,
  `isolate_id` bigint(11) NOT NULL COMMENT '屏蔽ID',
  `r_alert_id` varchar(300) DEFAULT NULL,
  `event_id` varchar(80) DEFAULT NULL,
  `action_id` bigint(10) DEFAULT NULL,
  `device_id` varchar(50) DEFAULT NULL,
  `device_class` varchar(128) DEFAULT NULL COMMENT '设备类型',
  `biz_sys` varchar(128) DEFAULT NULL COMMENT '业务系统',
  `moni_index` varchar(1024) DEFAULT NULL COMMENT '监控指标/内容，关联触发器name',
  `moni_object` varchar(512) DEFAULT NULL COMMENT '监控对象',
  `cur_moni_value` varchar(512) DEFAULT NULL COMMENT '当前监控值',
  `cur_moni_time` datetime DEFAULT NULL COMMENT '当前监控时间',
  `alert_level` varchar(20) NOT NULL COMMENT '告警级别\r\n1-提示\r\n2-低\r\n3-中\r\n4-高\r\n5-严重\r\n',
  `item_id` varchar(50) DEFAULT NULL,
  `remark` varchar(512) DEFAULT NULL COMMENT '备注',
  `source` varchar(100) DEFAULT NULL COMMENT '告警来源\r\nMIRROR\r\nZABBIX\r\n',
  `order_status` varchar(50) DEFAULT NULL COMMENT '1-未派单\r\n2-处理中\r\n3-已完成\r\n',
  `order_id` varchar(100) DEFAULT NULL COMMENT '工单编号',
  `order_type` varchar(100) DEFAULT NULL COMMENT '工单类型',
  `idc_type` varchar(128) DEFAULT NULL COMMENT '所属位置-资源池',
  `source_room` varchar(100) DEFAULT NULL COMMENT '机房/资源池',
  `device_type` varchar(128) DEFAULT NULL COMMENT '设备类型',
  `device_mfrs` varchar(128) DEFAULT NULL COMMENT '设备提供商',
  `host_name` varchar(128) DEFAULT NULL COMMENT '主机名',
  `device_model` varchar(128) DEFAULT NULL COMMENT '设备型号',
  `object_type` varchar(50) DEFAULT NULL COMMENT '告警类型\r\n1-系统\r\n2-业务',
  `object_id` varchar(50) DEFAULT NULL COMMENT '对象ID，如果是设备告警则是设备ID，如果是业务则是业务系统code',
  `region` varchar(50) DEFAULT NULL COMMENT '域/资源池code',
  `device_ip` varchar(100) DEFAULT NULL,
  `alert_start_time` datetime DEFAULT NULL COMMENT '告警开始时间',
  `prefix` varchar(64) DEFAULT NULL COMMENT '告警前缀，用于多套告警系统时候区分标识',
  `alert_count` int(11) DEFAULT '1',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`alert_id`),
  KEY `index_r_alert_id` (`r_alert_id`) USING BTREE,
  KEY `index_isolate_id` (`isolate_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='屏蔽告警日志表';

-- ----------------------------
-- Table structure for alert_isolate_alerts
-- ----------------------------
DROP TABLE IF EXISTS `alert_isolate_alerts_his`;
CREATE TABLE `alert_isolate_alerts_his` (
  `alert_id` varchar(64) NOT NULL,
  `isolate_id` bigint(11) NOT NULL COMMENT '屏蔽ID',
  `r_alert_id` varchar(300) DEFAULT NULL,
  `event_id` varchar(80) DEFAULT NULL,
  `action_id` bigint(10) DEFAULT NULL,
  `device_id` varchar(50) DEFAULT NULL,
  `device_class` varchar(128) DEFAULT NULL COMMENT '设备类型',
  `biz_sys` varchar(128) DEFAULT NULL COMMENT '业务系统',
  `moni_index` varchar(1024) DEFAULT NULL COMMENT '监控指标/内容，关联触发器name',
  `moni_object` varchar(512) DEFAULT NULL COMMENT '监控对象',
  `item_key` varchar(128) DEFAULT NULL COMMENT '监控项',
  `key_comment` varchar(256) DEFAULT NULL COMMENT '监控项描述',
  `cur_moni_value` varchar(512) DEFAULT NULL COMMENT '当前监控值',
  `cur_moni_time` datetime DEFAULT NULL COMMENT '当前监控时间',
  `alert_level` varchar(20) NOT NULL COMMENT '告警级别\r\n1-提示\r\n2-低\r\n3-中\r\n4-高\r\n5-严重\r\n',
  `item_id` varchar(50) DEFAULT NULL,
  `remark` varchar(512) DEFAULT NULL COMMENT '备注',
  `source` varchar(100) DEFAULT NULL COMMENT '告警来源\r\nMIRROR\r\nZABBIX\r\n',
  `order_status` varchar(50) DEFAULT NULL COMMENT '1-未派单\r\n2-处理中\r\n3-已完成\r\n',
  `order_id` varchar(100) DEFAULT NULL,
  `order_type` varchar(100) DEFAULT NULL,
  `idc_type` varchar(128) DEFAULT NULL COMMENT '所属位置-资源池',
  `source_room` varchar(100) DEFAULT NULL COMMENT '机房/资源池',
  `device_type` varchar(128) DEFAULT NULL COMMENT '设备类型',
  `device_mfrs` varchar(128) DEFAULT NULL COMMENT '设备提供商',
  `host_name` varchar(128) DEFAULT NULL COMMENT '主机名',
  `device_model` varchar(128) DEFAULT NULL COMMENT '设备型号',
  `object_type` varchar(50) DEFAULT NULL COMMENT '告警类型\r\n1-系统\r\n2-业务',
  `object_id` varchar(50) DEFAULT NULL COMMENT '对象ID，如果是设备告警则是设备ID，如果是业务则是业务系统code',
  `region` varchar(50) DEFAULT NULL COMMENT '域/资源池code',
  `device_ip` varchar(100) DEFAULT NULL,
  `alert_start_time` datetime DEFAULT NULL COMMENT '告警开始时间',
  `prefix` varchar(64) DEFAULT NULL COMMENT '告警前缀，用于多套告警系统时候区分标识',
  `alert_count` int(11) DEFAULT '1',
  `alert_end_time` datetime COMMENT '告警结束时间',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`alert_id`),
  KEY `index_isolate_id` (`isolate_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='屏蔽告警日志表';

-- ----------------------------
-- Table structure for alert_mail_failed_record
-- ----------------------------
DROP TABLE IF EXISTS `alert_mail_failed_record`;
CREATE TABLE `alert_mail_failed_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `receiver` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '收件邮箱',
  `method` varchar(20) COLLATE utf8_bin NOT NULL COMMENT '收件方式pop3 或 imap',
  `sender` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '发件邮箱',
  `uid` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '邮件唯一标识',
  `message` varchar(128) COLLATE utf8_bin DEFAULT NULL COMMENT '失败详情',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='邮件读取失败记录表';

-- ----------------------------
-- Table structure for alert_mail_recipients
-- ----------------------------
DROP TABLE IF EXISTS `alert_mail_recipients`;
CREATE TABLE `alert_mail_recipients` (
  `id` int(32) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `recipient` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '收件邮箱',
  `password` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '收件邮箱密码',
  `mail_server` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '收件邮箱邮件服务器',
  `receive_protocal` tinyint(1) DEFAULT '0' COMMENT '收件协议, 0: pop3, 1: imap',
  `receive_port` int(8) NOT NULL COMMENT '收件邮箱邮件服务器收件端口',
  `status` tinyint(1) DEFAULT '0' COMMENT '是否启用, 0: 未启用, 1: 启用',
  `recipient_desc` varchar(128) COLLATE utf8_bin DEFAULT NULL COMMENT '收件邮箱描述',
  `period` int(11) DEFAULT NULL COMMENT '邮箱采集周期-单位:分钟',
  `period_unit` tinyint(1) NOT NULL DEFAULT '0' COMMENT '周期时间单位 0:分钟,1:小时,2:天',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='邮件采集账号表';

-- ----------------------------
-- Table structure for alert_mail_resolve_filter
-- ----------------------------
DROP TABLE IF EXISTS `alert_mail_resolve_filter`;
CREATE TABLE `alert_mail_resolve_filter` (
  `id` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '主键ID',
  `recipient_id`  int(32) NOT NULL,
  `receiver` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '收件邮箱',
  `sender` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '发件邮箱',
  `title_incl` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '邮件过滤:标题包含关键字',
  `content_incl` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '邮件过滤:内容包含关键字',
  `status` tinyint(1) DEFAULT '0' COMMENT '是否启用, 0: 未启用, 1: 启用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='告警采集配置表';

-- ----------------------------
-- Table structure for alert_mail_resolve_record
-- ----------------------------
DROP TABLE IF EXISTS `alert_mail_resolve_record`;
CREATE TABLE `alert_mail_resolve_record` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `filter_id` varchar(50) NOT NULL COMMENT 'Filter主键ID',
  `mail_title` varchar(500) DEFAULT NULL COMMENT '邮件标题',
  `mail_content` text COMMENT '邮件内容',
  `mail_sender` varchar(64) DEFAULT NULL COMMENT '发件人',
  `mail_receiver` varchar(64) DEFAULT NULL COMMENT '收件人',
  `mail_send_time` datetime DEFAULT NULL COMMENT '发件时间',
  `resolve_time` datetime DEFAULT NULL COMMENT '采集时间',
  `device_ip` varchar(64) DEFAULT NULL COMMENT '设备IP',
  `moni_index` varchar(1024) DEFAULT NULL,
  `moni_object` varchar(128) DEFAULT NULL COMMENT '告警指标',
  `alert_level` varchar(20) NOT NULL COMMENT '告警级别\r\n1-提示\r\n2-低\r\n3-中\r\n4-高\r\n5-严重\r\n',
  `alert_id` varchar(64) NOT NULL COMMENT '告警编号',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '邮件告警采集记录表' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for alert_mail_resolve_strategy
-- ----------------------------
DROP TABLE IF EXISTS `alert_mail_resolve_strategy`;
CREATE TABLE `alert_mail_resolve_strategy` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `filter_id` varchar(50) COLLATE utf8_bin NOT NULL COMMENT 'Filter主键ID',
  `alert_field` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '匹配警报的字段名',
  `mail_field` tinyint(4) DEFAULT NULL COMMENT '邮件的字段名, 0: 标题, 1: 内容, 2:发件人, 3:发件时间',
  `field_match_val` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '配置字段的值',
  `use_reg` tinyint(1) DEFAULT '0' COMMENT '使用正则匹配, 0: 不使用, 1:使用',
  `field_match_reg` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '自定义正则表达式',
  `field_match_target` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '字段匹配后映射值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=255 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='告警采集自定义规则表';

-- ----------------------------
-- Table structure for alert_mail_substance
-- ----------------------------
DROP TABLE IF EXISTS `alert_mail_substance`;
CREATE TABLE `alert_mail_substance` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `receiver` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '收件邮箱',
  `sender` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '发件邮箱',
  `send_time` datetime DEFAULT NULL COMMENT '发件时间',
  `uid` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '邮件唯一标识',
  `subject` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '邮件标题',
  `content` text COLLATE utf8_bin COMMENT '邮件内容',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_receiver_uid` (`receiver`,`uid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1284 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='邮件读取记录表';

-- ----------------------------
-- Table structure for alert_moniter_top_sync
-- ----------------------------
DROP TABLE IF EXISTS `alert_moniter_top_sync`;
CREATE TABLE `alert_moniter_top_sync` (
  `idcType` varchar(300) DEFAULT NULL,
  `alertLevel` varchar(12) DEFAULT NULL,
  `deviceType` varchar(10) DEFAULT NULL,
  `countOrder` int(1) DEFAULT NULL,
  `moniterObject` varchar(128) DEFAULT NULL COMMENT '监控对象',
  `moniterAlertCount` bigint(128) DEFAULT NULL COMMENT '告警数量',
  `rate` decimal(20,4) DEFAULT NULL COMMENT '告警占比',
  `month` varchar(24) DEFAULT NULL,
  `updateTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `index_r_alert_id` (`idcType`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警监控对象top10统计表';

-- ----------------------------
-- Table structure for alert_notify_config
-- ----------------------------
DROP TABLE IF EXISTS `alert_notify_config`;
CREATE TABLE `alert_notify_config` (
  `uuid` varchar(255) NOT NULL COMMENT 'uuid',
  `name` varchar(255) DEFAULT NULL COMMENT '策略配置名称',
  `is_open` int(10) DEFAULT NULL COMMENT '是否启用 0-关闭(默认) 1-启用',
  `alert_filter_id` int(255) DEFAULT NULL COMMENT '告警筛选器id',
  `alert_filter_scene_id` int(255) DEFAULT NULL COMMENT '告警筛选场景id',
  `notify_alert_type` varchar(255) DEFAULT NULL COMMENT '告警通知类型(0-转派, 1-确认,2-派发工单, 3-清除, 4-通知, 5-过滤, 6-工程, 7-维护模式)',
  `notify_type` varchar(255) DEFAULT NULL COMMENT '通知类型 0-邮件/短信 1-邮件 2-短信',
  `is_recurrence_interval` int(10) DEFAULT NULL COMMENT '是否重发  0-关闭(默认) 1-启用',
  `recurrence_interval` varchar(255) DEFAULT NULL COMMENT '重发间隔时间',
  `recurrence_interval_util` varchar(255) DEFAULT NULL COMMENT '重发间隔单位 ',
  `recurrence_count` int NULL COMMENT '重发次数，-1为不限制次数',
  `resend_time` varchar(255) DEFAULT NULL COMMENT '重发时间',
  `email_type` int(10) DEFAULT NULL COMMENT '邮件发送类型 1-合并 2-单条',
  `send_operation` varchar(10) DEFAULT NULL COMMENT '发送记录',
  `cur_send_time` datetime DEFAULT NULL COMMENT '最新发送时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_first_send` varchar(2) DEFAULT NULL,
  `creator` varchar(255) DEFAULT NULL,
  `period` varchar(128) DEFAULT NULL COMMENT '告警通知配置执行时间段 0-全天 1-时间段',
  `start_period` varchar(128) DEFAULT NULL COMMENT '配置起始执行时间',
  `end_period` varchar(128) DEFAULT NULL COMMENT '配置执行结束',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警通知策略配置表';

-- ----------------------------
-- Table structure for alert_notify_config_log
-- ----------------------------
DROP TABLE IF EXISTS `alert_notify_config_log`;
CREATE TABLE `alert_notify_config_log` (
  `id`  int NOT NULL AUTO_INCREMENT,
  `alert_notify_config_id` text COMMENT '告警配置id',
  `send_status` varchar(255) DEFAULT NULL COMMENT '发送状态 0-失败 1-成功',
  `receiver` varchar(255) DEFAULT NULL COMMENT '接收人',
  `send_time` datetime DEFAULT NULL COMMENT '发送时间',
  `send_content` text COMMENT '发送内容',
  `send_alert_id` text COMMENT '发送的告警id',
  `send_ype` varchar(2) DEFAULT NULL COMMENT '发送类型 1-短信 2-邮件(单发) 3-邮件(合并)',
  `resend_time` varchar(255) DEFAULT NULL COMMENT '重发时间',
  `is_resend` varchar(2) DEFAULT NULL COMMENT '是否重发 0-否 1-是',
  KEY `i_receiver` (`receiver`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警通知配置发送记录表';

-- ----------------------------
-- Table structure for alert_notify_config_log
-- ----------------------------
DROP TABLE IF EXISTS `alert_notify_config_log_his`;
CREATE TABLE `alert_notify_config_log_his` (
  `id`  int NOT NULL,
  `alert_notify_config_id` varchar(255) DEFAULT NULL,
  `send_status` varchar(255) DEFAULT NULL COMMENT '发送状态 0-失败 1-成功',
  `receiver` varchar(255) DEFAULT NULL COMMENT '接收人',
  `send_time` datetime DEFAULT NULL COMMENT '发送时间',
  `send_content` text COMMENT '发送内容',
  `send_alert_id` varchar(255) DEFAULT NULL,
  `send_ype` varchar(2) DEFAULT NULL COMMENT '发送类型 1-短信 2-邮件(单发) 3-邮件(合并)',
  `resend_time` varchar(255) DEFAULT NULL COMMENT '重发时间',
  `is_resend` varchar(2) DEFAULT NULL COMMENT '是否重发 0-否 1-是',
  PRIMARY KEY (`id`),
  KEY `alert_notify_index` (`receiver`,`send_alert_id`,`send_ype`) USING BTREE,
  KEY `idx_alertId_configId` (`send_alert_id`,`alert_notify_config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警通知配置发送记录历史表';

-- ----------------------------
-- Table structure for alert_notify_config_receiver
-- ----------------------------
DROP TABLE IF EXISTS `alert_notify_config_receiver`;
CREATE TABLE `alert_notify_config_receiver` (
  `uuid` varchar(255) NOT NULL COMMENT 'uuid',
  `alert_notify_config_id` varchar(255) DEFAULT NULL COMMENT '告警通知策略配置表uuid',
  `notify_obj_type` varchar(255) DEFAULT NULL COMMENT '通知对象类型 1-团队 2-个人',
  `notify_obj_info` varchar(255) DEFAULT NULL COMMENT '通知对象信息',
  `notify_type` varchar(10) DEFAULT NULL COMMENT '通知类型 0-邮件/短信 1-邮件 2-短信',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警通知策略配置人员表';

-- ----------------------------
-- Table structure for alert_notify_config_rule
-- ----------------------------
DROP TABLE IF EXISTS `alert_notify_config_rule`;
CREATE TABLE `alert_notify_config_rule` (
  `id` varchar(10) NOT NULL COMMENT 'id',
  `rule_name` varchar(255) DEFAULT NULL COMMENT '定时任务规则名称',
  `rule_cron` varchar(255) DEFAULT NULL COMMENT 'cron',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for alert_operate_log
-- ----------------------------
DROP TABLE IF EXISTS `alert_operate_log`;
CREATE TABLE `alert_operate_log` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `relation_id` varchar(64) DEFAULT NULL COMMENT '关联的ID',
  `operate_model` varchar(128) NOT NULL COMMENT '操作模块,如告警、屏蔽、筛选等',
  `operate_model_desc` varchar(256) DEFAULT NULL,
  `operate_type` varchar(64) NOT NULL COMMENT '操作类型,如新增、修改等',
  `operate_type_desc` varchar(256) DEFAULT NULL,
  `operater` varchar(64) DEFAULT NULL COMMENT '操作人',
  `operate_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  `operate_content` varchar(4000) DEFAULT NULL COMMENT '操作内容',
  `remark` varchar(256) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `index_relation_id` (`relation_id`) USING BTREE,
  KEY `index_relation_id_group` (`relation_id`,`operate_model`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=324 DEFAULT CHARSET=utf8 COMMENT='操作记录表';

-- ----------------------------
-- Table structure for alert_operation_record
-- ----------------------------
DROP TABLE IF EXISTS `alert_operation_record`;
CREATE TABLE `alert_operation_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `alert_id` varchar(128) DEFAULT NULL,
  `user_id` varchar(32) DEFAULT NULL COMMENT '操作人',
  `user_name` varchar(32) DEFAULT NULL COMMENT '操作人名称',
  `operation_type` tinyint(1) NOT NULL COMMENT '操作类型 0-转派, 1-确认,2-派发工单, 3-清除, 4-通知, 5-过滤, 6-工程, 7-维护模式',
  `operation_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  `operation_status` tinyint(1) DEFAULT '1' COMMENT '操作状态 0-失败 1-成功,',
  `content` varchar(256) DEFAULT NULL COMMENT '操作内容',
  PRIMARY KEY (`id`),
  KEY `index_alert_id` (`alert_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=87061 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for alert_primary_secondary
-- ----------------------------
DROP TABLE IF EXISTS `alert_primary_secondary`;
CREATE TABLE `alert_primary_secondary` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '告警主次ID',
  `name` varchar(64) NOT NULL COMMENT '规则名称',
  `description` varchar(1024) DEFAULT NULL COMMENT '规则描述',
  `status` varchar(1) DEFAULT '0' COMMENT '启用状态，0-停用，1-启用',
  `device_relation_type` varchar(2) DEFAULT NULL COMMENT '设备关联类型,1-相同设备；2-不同设备',
  `primary_idc` varchar(128) DEFAULT NULL COMMENT '主要告警-资源池',
  `primary_ip` varchar(32) DEFAULT NULL COMMENT '主要告警-设备ip',
  `primary_moni_index` varchar(256) DEFAULT NULL COMMENT '主要告警-告警内容',
  `primary_alert_level` varchar(2) DEFAULT NULL COMMENT '主要告警-告警等级',
  `primary_option_condition` varchar(4000) DEFAULT NULL COMMENT '主要告警-设备过滤条件',
  `operate_user` varchar(512) DEFAULT NULL COMMENT '维护用户',
  `secondary_option_condition` varchar(4000) DEFAULT NULL COMMENT '次要告警-过滤条件',
  `creater` varchar(64) DEFAULT NULL COMMENT '创建人',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `editer` varchar(64) DEFAULT NULL COMMENT '修改人',
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `is_delete` tinyint(4) DEFAULT '0' COMMENT '删除状态，0-未删除，1-已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COMMENT='告警主次表';

-- ----------------------------
-- Table structure for alert_primary_secondary_alerts
-- ----------------------------
DROP TABLE IF EXISTS `alert_primary_secondary_alerts`;
CREATE TABLE `alert_primary_secondary_alerts` (
  `alert_id` varchar(64) NOT NULL,
  `primary_secondary_id` bigint(11) NOT NULL COMMENT '主次ID',
  `primary_secondary_alert_id` varchar(64) NOT NULL COMMENT '主次告警ID',
  `r_alert_id` varchar(300) DEFAULT NULL,
  `event_id` varchar(80) DEFAULT NULL,
  `action_id` bigint(10) DEFAULT NULL,
  `device_id` varchar(50) DEFAULT NULL,
  `device_class` varchar(128) DEFAULT NULL COMMENT '设备类型',
  `biz_sys` varchar(128) DEFAULT NULL COMMENT '业务系统',
  `moni_index` varchar(1024) DEFAULT NULL COMMENT '监控指标/内容，关联触发器name',
  `moni_object` varchar(512) DEFAULT NULL COMMENT '监控对象',
  `cur_moni_value` varchar(512) DEFAULT NULL COMMENT '当前监控值',
  `cur_moni_time` datetime DEFAULT NULL COMMENT '当前监控时间',
  `alert_level` varchar(20) NOT NULL COMMENT '告警级别\r\n1-提示\r\n2-低\r\n3-中\r\n4-高\r\n5-严重\r\n',
  `item_id` varchar(50) DEFAULT NULL,
  `remark` varchar(512) DEFAULT NULL COMMENT '备注',
  `source` varchar(100) DEFAULT NULL COMMENT '告警来源\r\nMIRROR\r\nZABBIX\r\n',
  `idc_type` varchar(128) DEFAULT NULL COMMENT '所属位置-资源池',
  `source_room` varchar(100) DEFAULT NULL COMMENT '机房/资源池',
  `device_type` varchar(128) DEFAULT NULL COMMENT '设备类型',
  `device_mfrs` varchar(128) DEFAULT NULL COMMENT '设备提供商',
  `host_name` varchar(128) DEFAULT NULL COMMENT '主机名',
  `device_model` varchar(128) DEFAULT NULL COMMENT '设备型号',
  `object_type` varchar(50) DEFAULT NULL COMMENT '告警类型\r\n1-系统\r\n2-业务',
  `object_id` varchar(50) DEFAULT NULL COMMENT '对象ID，如果是设备告警则是设备ID，如果是业务则是业务系统code',
  `region` varchar(50) DEFAULT NULL COMMENT '域/资源池code',
  `device_ip` varchar(100) DEFAULT NULL,
  `alert_start_time` datetime DEFAULT NULL COMMENT '告警开始时间',
  `prefix` varchar(64) DEFAULT NULL COMMENT '告警前缀，用于多套告警系统时候区分标识',
  `alert_count` int(11) DEFAULT '1',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `alert_type` varchar(2) DEFAULT '1' COMMENT '告警结果，1-告警，2-消警',
  PRIMARY KEY (`alert_id`),
  KEY `index_r_alert_id` (`r_alert_id`) USING BTREE,
  KEY `index_isolate_id` (`primary_secondary_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='衍生告警日志表';

-- ----------------------------
-- Table structure for alert_proxy_idc
-- ----------------------------
DROP TABLE IF EXISTS `alert_proxy_idc`;
CREATE TABLE `alert_proxy_idc` (
  `id` bigint(11) NOT NULL,
  `proxy_name` varchar(64) DEFAULT NULL,
  `idc` varchar(64) DEFAULT NULL,
  `remark` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for alert_report_operate_record
-- ----------------------------
DROP TABLE IF EXISTS `alert_report_operate_record`;
CREATE TABLE `alert_report_operate_record` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ALERT_ID` varchar(64) DEFAULT NULL COMMENT '警报ID',
  `USER_ID` varchar(32) DEFAULT NULL COMMENT '发件人ID',
  `USER_NAME` varchar(32) DEFAULT NULL COMMENT '发件人姓名',
  `REPORT_TYPE` tinyint(1) DEFAULT '0' COMMENT '通知类型, 0:短信,1:邮件',
  `USER_EMAIL` varchar(32) DEFAULT NULL COMMENT '发件人邮箱',
  `DESTINATION` varchar(64) DEFAULT NULL COMMENT '短信/邮件 地址',
  `MESSAGE` text COMMENT '短信/邮件 内容',
  `STATUS` tinyint(1) DEFAULT '1' COMMENT '发送状态,0:失败,1:成功',
  `CREATE_TIME` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  KEY `index_alert_id` (`ALERT_ID`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=395 DEFAULT CHARSET=utf8 COMMENT='告警通知操作记录表';

-- ----------------------------
-- Table structure for alert_rep_panel
-- ----------------------------
DROP TABLE IF EXISTS `alert_rep_panel`;
CREATE TABLE `alert_rep_panel` (
  `id` int(128) NOT NULL AUTO_INCREMENT,
  `panel_name` varchar(128) DEFAULT NULL,
  `show_item` varchar(20000) DEFAULT NULL COMMENT '布局信息',
  `editer` varchar(24) DEFAULT NULL,
  `creater` varchar(24) DEFAULT NULL,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_time` datetime DEFAULT NULL,
  `access_type` tinyint(1) DEFAULT NULL COMMENT '分享类型：1私人2组内3所有人',
  PRIMARY KEY (`id`),
  KEY `index_r_alert_id` (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8 COMMENT='配置项信息表';

-- ----------------------------
-- Table structure for alert_rep_panel_item
-- ----------------------------
DROP TABLE IF EXISTS `alert_rep_panel_item`;
CREATE TABLE `alert_rep_panel_item` (
  `id` int(128) NOT NULL AUTO_INCREMENT,
  `panel_id` varchar(24) DEFAULT NULL COMMENT '关联的大屏id',
  `item_id` varchar(24) DEFAULT NULL COMMENT '布局的id',
  `report_type` varchar(10) DEFAULT NULL COMMENT '报表类型',
  `report_name` varchar(128) DEFAULT NULL COMMENT '名称',
  `time_value` int(128) DEFAULT NULL COMMENT '时间刻度值',
  `time_unit` varchar(8) DEFAULT NULL COMMENT '横轴时间单位',
  `report_unit` varchar(24) DEFAULT NULL COMMENT '单位',
  `conversion_type` tinyint(1) DEFAULT NULL COMMENT '换算：1相乘2相除',
  `conversion_val` int(128) DEFAULT NULL COMMENT '换算数值',
  PRIMARY KEY (`id`),
  KEY `index_r_alert_id` (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1045 DEFAULT CHARSET=utf8 COMMENT='大屏单个报表配置';

-- ----------------------------
-- Table structure for alert_rep_panel_moniteritem
-- ----------------------------
DROP TABLE IF EXISTS `alert_rep_panel_moniteritem`;
CREATE TABLE `alert_rep_panel_moniteritem` (
  `id` int(128) NOT NULL AUTO_INCREMENT,
  `item_id` varchar(24) DEFAULT NULL COMMENT '关联大屏配置项id',
  `resource_device` varchar(4000) DEFAULT NULL COMMENT '资源设备',
  `moniter_item` varchar(256) DEFAULT NULL COMMENT '监控指标',
  `view_name` varchar(128) DEFAULT NULL COMMENT '显示名称',
  `count_type` varchar(24) DEFAULT NULL COMMENT '统计方式：1最大2最小3平均',
  `resource_type` tinyint(1) DEFAULT NULL COMMENT '资源类型：1业务系统2资源池3机房4设备大类5设备小类6设备ip',
  `unit` varchar(24) DEFAULT NULL COMMENT '监控项单位',
  `mointer_index` varchar(24) DEFAULT NULL COMMENT '所属es索引',
  `resource_device_ipStr` varchar(8000) DEFAULT NULL COMMENT '资源设备ip',
  `device_class` varchar(255) DEFAULT NULL COMMENT '设备分类',
  `device_type` varchar(255) DEFAULT NULL COMMENT '设备类型',
  `subclass` varchar(255) DEFAULT NULL COMMENT '子类',
  `moniter_item_name` varchar(255) DEFAULT NULL COMMENT '监控项',
  `sub_moniter_items` varchar(8000) DEFAULT NULL COMMENT '多个子监控项///,分隔',
  PRIMARY KEY (`id`),
  KEY `index_r_alert_id` (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1175 DEFAULT CHARSET=utf8 COMMENT='大屏单个报表配置';

-- ----------------------------
-- Table structure for alert_root_cause
-- ----------------------------
DROP TABLE IF EXISTS `alert_root_cause`;
CREATE TABLE `alert_root_cause` (
  `time1` varchar(255) DEFAULT NULL,
  `time2` varchar(255) DEFAULT NULL,
  `cur_alert_id` varchar(255) NOT NULL,
  `cur_item_id` varchar(255) NOT NULL,
  `cur_moni_time` varchar(255) NOT NULL,
  `his_alert_item_id` varchar(255) DEFAULT NULL,
  `similarity` varchar(255) DEFAULT NULL,
  `root_cause_alert_id` varchar(255) DEFAULT NULL,
  `root_cause_start_time` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for alert_sms_notify_record
-- ----------------------------
DROP TABLE IF EXISTS `alert_sms_notify_record`;
CREATE TABLE `alert_sms_notify_record` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ALERT_IDS` varchar(64) DEFAULT NULL,
  `SENDER` varchar(64) DEFAULT NULL,
  `MOBILES` varchar(64) DEFAULT NULL,
  `MESSAGE` text,
  `STATUS` tinyint(1) DEFAULT '0',
  `CREATE_TIME` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for alert_transfer
-- ----------------------------
DROP TABLE IF EXISTS `alert_transfer`;
CREATE TABLE `alert_transfer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `alert_id` varchar(50) NOT NULL COMMENT '告警id',
  `user_id` varchar(32) DEFAULT NULL COMMENT '操作人id',
  `user_name` varchar(32) DEFAULT NULL COMMENT '操作人名称',
  `confirm_user_id` varchar(32) DEFAULT NULL COMMENT '待确认人id',
  `confirm_user_name` varchar(32) DEFAULT NULL COMMENT '待确认人name',
  `operation_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for alert_tuning_order
-- ----------------------------
DROP TABLE IF EXISTS `alert_tuning_order`;
CREATE TABLE `alert_tuning_order` (
  `alert_id` varchar(64) NOT NULL COMMENT '告警id',
  `order_id` varchar(100) DEFAULT NULL COMMENT '工单id',
  `order_type` varchar(100) DEFAULT NULL COMMENT '工单类型',
  `order_status` varchar(50) DEFAULT NULL COMMENT '1-未派单\r\n2-处理中\r\n3-已完成\r\n',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`alert_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警记录表';

-- ----------------------------
-- Table structure for alert_voice_notify
-- ----------------------------
DROP TABLE IF EXISTS `alert_voice_notify`;
CREATE TABLE `alert_voice_notify` (
  `uuid` varchar(255) NOT NULL COMMENT 'uuis',
  `voice_language` varchar(255) NOT NULL COMMENT '语言类型',
  `voice_content` varchar(255) DEFAULT NULL COMMENT '语音内容',
  `is_open` int(2) NOT NULL COMMENT '0-禁用 1-启用',
  `alert_filter_id` int(255) NOT NULL COMMENT '过滤器id',
  `alert_filter_scene_id` int(255) NOT NULL COMMENT '告警筛选场景id',
  `content` varchar(255) DEFAULT NULL COMMENT '通知内容',
  `alert_exist_time` int(11) DEFAULT NULL COMMENT '告警存在时长',
  `voice_alert_id` text COMMENT '已播报的告警id',
  `creator` varchar(255) NOT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for alert_voice_notify_log
-- ----------------------------
DROP TABLE IF EXISTS `alert_voice_notify_log`;
CREATE TABLE `alert_voice_notify_log` (
  `alert_notify_config_id` varchar(255) NOT NULL COMMENT '播报配置id',
  `voice_alert_id` text NOT NULL COMMENT '播报告警id',
  `operator` varchar(255) DEFAULT NULL COMMENT '操作人',
  `operation_time` datetime DEFAULT NULL COMMENT '操作时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for alert_work_config
-- ----------------------------
DROP TABLE IF EXISTS `alert_work_config`;
CREATE TABLE `alert_work_config` (
  `uuid` varchar(255) NOT NULL COMMENT 'uuid',
  `day_start_time` varchar(255) DEFAULT NULL COMMENT '白班 开始时间',
  `day_end_time` varchar(255) DEFAULT NULL COMMENT '白班 结束时间内',
  `night_start_time` varchar(255) DEFAULT NULL COMMENT '夜班 开始时间',
  `night_end_time` varchar(255) DEFAULT NULL COMMENT '夜班 结束时间',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='值班人员配置表';

-- ----------------------------
-- Table structure for alert_zyc_maintence_staff
-- ----------------------------
DROP TABLE IF EXISTS `alert_zyc_maintence_staff`;
CREATE TABLE `alert_zyc_maintence_staff` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `account` varchar(100) NOT NULL COMMENT '账号名',
  `fullname` varchar(100) NOT NULL COMMENT '用户名',
  `email` varchar(128) DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(128) DEFAULT NULL COMMENT '手机号',
  `zyc` varchar(128) DEFAULT NULL COMMENT '资源池',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='告警资源池工单监控维护接口人';

-- ----------------------------
-- Table structure for auto_alert_record
-- ----------------------------
DROP TABLE IF EXISTS `auto_alert_record`;
CREATE TABLE `auto_alert_record` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ALARM_ID` varchar(64) DEFAULT NULL,
  `FLAG` varchar(64) DEFAULT NULL,
  `MSG` varchar(64) DEFAULT NULL,
  `STATUS` char(1) DEFAULT NULL,
  `TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=51258 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for auto_confirm_clear
-- ----------------------------
DROP TABLE IF EXISTS `auto_confirm_clear`;
CREATE TABLE `auto_confirm_clear` (
  `uuid` varchar(255) NOT NULL COMMENT 'id',
  `device_ip` varchar(255) DEFAULT NULL COMMENT '设备ip',
  `idc_type` varchar(255) DEFAULT NULL COMMENT '资源池',
  `biz_sys` varchar(255) DEFAULT NULL COMMENT '业务线',
  `alert_level` varchar(255) DEFAULT NULL COMMENT '告警等级',
  `item_id` varchar(255) DEFAULT NULL COMMENT '监控项id',
  `source` varchar(255) DEFAULT NULL COMMENT '告警来源',
  `auto_type` tinyint(2) DEFAULT NULL COMMENT '1-确认 2-清除',
  `content` text COMMENT '内容',
  `start_time` varchar(255) DEFAULT NULL COMMENT '开始时间',
  `end_time` varchar(255) DEFAULT NULL COMMENT '结束时间',
  `operator` varchar(255) DEFAULT NULL COMMENT '操作人',
  PRIMARY KEY (`uuid`),
  KEY `i_bizsys_level` (`biz_sys`,`alert_level`) USING BTREE,
  KEY `i_ip_level` (`device_ip`,`alert_level`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for cyc_report_resource
-- ----------------------------
DROP TABLE IF EXISTS `cyc_report_resource`;
CREATE TABLE `cyc_report_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_type` tinyint(1) NOT NULL COMMENT '数据文件类型',
  `file_path` varchar(200) NOT NULL COMMENT '数据文件的全路径',
  `file_name` varchar(100) NOT NULL COMMENT '数据文件名',
  `start_time` datetime DEFAULT NULL COMMENT '数据文件名中的数据开始时间',
  `session_id` varchar(50) DEFAULT NULL COMMENT '数据文件名中的sessionId',
  `request_id` int(11) NOT NULL COMMENT '请求消息的流水号',
  `read_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '数据文件读取状态, 0:未读, 1:已读',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `idx_cyc_rpt_res_start_time` (`start_time`)
) ENGINE=InnoDB AUTO_INCREMENT=103788 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for device_item_info
-- ----------------------------
DROP TABLE IF EXISTS `device_item_info`;
CREATE TABLE `device_item_info` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `device_class` varchar(255) DEFAULT NULL COMMENT '设备分类',
  `device_type` varchar(255) DEFAULT NULL COMMENT '设备类型',
  `subclass` varchar(255) DEFAULT NULL COMMENT '监控逻辑子类',
  `moniter_item_name` varchar(255) DEFAULT NULL COMMENT '监控标准名称',
  `moniter_item_key` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL COMMENT '解释',
  `alert_level` varchar(255) DEFAULT NULL,
  `is_create_alert` varchar(255) DEFAULT NULL COMMENT '是否产生告警',
  `default_value` varchar(255) DEFAULT NULL COMMENT '缺省阀值设定',
  `monitor_rate` varchar(255) DEFAULT NULL COMMENT '监控频率',
  `protocol` varchar(255) DEFAULT NULL COMMENT '实现方式',
  `is_show_idcType` varchar(255) DEFAULT NULL COMMENT '是否显示资源池位置',
  `is_show_room` varchar(255) DEFAULT NULL COMMENT '是否显示机房位置',
  `is_show_frame` varchar(255) DEFAULT NULL COMMENT '是否显示机架位置',
  `note` varchar(255) DEFAULT NULL COMMENT '备注',
  `alert_tips` varchar(255) DEFAULT NULL COMMENT '告警提示语',
  `object_type` varchar(255) DEFAULT NULL COMMENT '告警分类',
  `is_create_order` varchar(255) DEFAULT NULL COMMENT '是否产生工单',
  PRIMARY KEY (`id`),
  KEY `device_class` (`device_class`) USING HASH,
  KEY `moniter_item_name` (`moniter_item_name`) USING HASH,
  KEY `device_type-device_class` (`device_type`,`device_class`) USING BTREE,
  KEY `subclass-device_type-device_class` (`subclass`,`device_class`,`device_type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=301 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for log_alert_linked
-- ----------------------------
DROP TABLE IF EXISTS `log_alert_linked`;
CREATE TABLE `log_alert_linked` (
  `log_alert_rule_uuid` varchar(255) NOT NULL COMMENT '日志告警规则id',
  `alert_id` varchar(255) NOT NULL COMMENT '告警id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for log_alert_rule
-- ----------------------------
DROP TABLE IF EXISTS `log_alert_rule`;
CREATE TABLE `log_alert_rule`  (
  `uuid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '日志告警规则id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '规则名称',
  `run_status` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '运行状态 0-停用 1-启用',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '规则描述',
  `idc_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资源池',
  `ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备ip',
  `key_comment` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '告警名称',
  `filter_time` tinyint(255) NULL DEFAULT NULL COMMENT '过滤时间',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `creator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'user_name',
  `alert_level` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '告警等级',
  `include` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '1-包含/2-不包含',
  `include_key` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '包含关键字',
  `no_include` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '不包含',
  `no_include_key` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '不包含关键字',
  PRIMARY KEY (`uuid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'sys日志告警规则配置表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

-- ----------------------------
-- Table structure for log_filter_rule
-- ----------------------------
DROP TABLE IF EXISTS `log_filter_rule`;
CREATE TABLE `log_filter_rule`  (
  `uuid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '日志过滤规则id',
  `rule_type` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '规则类型',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '规则名称',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '规则描述',
  `idc_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资源池',
  `ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备ip',
  `param` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关键字',
  `include_key` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '包含关键字',
  `no_include_key` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '不包含关键字',
  `start_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '结束时间',
  `creator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`uuid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'sys日志过滤规则配置表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

-- ----------------------------
-- Table structure for security_leak_scan_records
-- ----------------------------
DROP TABLE IF EXISTS `security_leak_scan_records`;
CREATE TABLE `security_leak_scan_records` (
  `id` varchar(50) NOT NULL COMMENT '主键ID',
  `biz_line` varchar(64) NOT NULL COMMENT '归属业务线',
  `report_file_name` varchar(64) NOT NULL COMMENT '报告文件名',
  `report_file_url` varchar(128) NOT NULL COMMENT '服务端-报告文件下载地址',
  `scan_date` date NOT NULL COMMENT '扫描日期',
  `file_id` varchar(50) DEFAULT NULL COMMENT '工单上传文件ID',
  `bpm_id` varchar(50) DEFAULT NULL COMMENT '工单流程ID',
  `repair_stat` tinyint(1) DEFAULT '0' COMMENT '修复状态: 0 未修复，1 已修复',
  `department1` varchar(50) DEFAULT NULL COMMENT '工程期数',
  `department2` varchar(50) DEFAULT NULL COMMENT '工程期数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='安全漏洞扫描记录表';

-- ----------------------------
-- Table structure for security_leak_scan_reports
-- ----------------------------
DROP TABLE IF EXISTS `security_leak_scan_reports`;
CREATE TABLE `security_leak_scan_reports` (
  `id` varchar(50) NOT NULL COMMENT '主键ID',
  `scan_id` varchar(50) NOT NULL COMMENT '扫描记录主键ID',
  `ip` varchar(64) NOT NULL COMMENT 'IP 地址',
  `report_path` varchar(128) DEFAULT NULL COMMENT '报告路径',
  `high_leaks` int(11) DEFAULT '0' COMMENT '高危漏洞数量',
  `medium_leaks` int(11) DEFAULT '0' COMMENT '中危漏洞数量',
  `low_leaks` int(11) DEFAULT '0' COMMENT '低微漏洞数量',
  `risk_val` float DEFAULT '0' COMMENT '风险值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='安全漏洞扫描记录表';

-- ----------------------------
-- Table structure for security_leak_scan_report_files
-- ----------------------------
DROP TABLE IF EXISTS `security_leak_scan_report_files`;
CREATE TABLE `security_leak_scan_report_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `scan_id` varchar(50) NOT NULL COMMENT '扫描记录主键ID',
  `file_name` varchar(64) NOT NULL COMMENT '报告压缩包文件名',
  `ftp_path` varchar(120) NOT NULL COMMENT 'FTP-报告压缩包文件下载地址',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=204 DEFAULT CHARSET=utf8 COMMENT='安全漏洞扫描记录文件解析表';

-- ----------------------------
-- Table structure for alert_scan_comparision
-- ----------------------------
DROP TABLE IF EXISTS `alert_scan_comparision`;
CREATE TABLE `alert_scan_comparision`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
  `device_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设别ip',
  `idc_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资源池',
  `start_scan_time` datetime(0) NULL DEFAULT NULL COMMENT '首次扫描时间',
  `cur_scan_time` datetime(0) NULL DEFAULT NULL COMMENT '当前扫描时间',
  `cur_moni_time` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当前告警时间',
  `syn_status` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '同步状态 1-同步 2-未同步 3-同步失败',
  `cur_syn_time` datetime(0) NULL DEFAULT NULL COMMENT '当前同步时间',
  `type` smallint NULL DEFAULT 0 COMMENT '0:告警1监控',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '告警扫描对账记录表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

-- ----------------------------
-- Table structure for alert_schedule_index
-- ----------------------------
DROP TABLE IF EXISTS `alert_schedule_index`;
CREATE TABLE `alert_schedule_index` (
  `id` varchar(50) NOT NULL COMMENT 'id',
  `index_name` varchar(50) DEFAULT NULL COMMENT '指标名称',
  `index_value` varchar(50) DEFAULT NULL COMMENT '指标值',
  `index_type` varchar(50) DEFAULT NULL COMMENT '指标类型',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for alert_storage_userate
-- ----------------------------
DROP TABLE IF EXISTS `alert_storage_userate`;
CREATE TABLE `alert_storage_userate` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `idc_type` varchar(255) DEFAULT NULL,
  `block_useRate` decimal(20,0) DEFAULT NULL COMMENT '块平均使用率',
  `san_useRate` decimal(20,0) DEFAULT NULL COMMENT 'san平均使用率',
  `createTime` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `device_class` (`idc_type`) USING HASH,
  KEY `device_type-device_class` (`block_useRate`,`idc_type`) USING BTREE,
  KEY `subclass-device_type-device_class` (`san_useRate`,`idc_type`,`block_useRate`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='磁盘利用率数据';

-- ----------------------------
-- Table structure for t_cfg_scada_canvas
-- ----------------------------
DROP TABLE IF EXISTS `t_cfg_scada_canvas`;
CREATE TABLE `t_cfg_scada_canvas` (
  `id` varchar(50) NOT NULL COMMENT '视图id',
  `name` varchar(255) DEFAULT NULL COMMENT '视图标题',
  `content` longtext COMMENT '视图内容',
  `picture_type` int(4) DEFAULT NULL COMMENT '1-物理图 2-逻辑图 3-租户图',
  `page_type` int(4) NOT NULL COMMENT '页面类型 0普通页面  1模板',
  `idc` varchar(200) DEFAULT NULL COMMENT '资源池code',
  `pod` varchar(50) DEFAULT NULL COMMENT 'pod',
  `project_name` varchar(200) DEFAULT NULL COMMENT '工程期数',
  `biz_system` varchar(200) DEFAULT NULL COMMENT '业务系统',
  `is_default` varchar(1) DEFAULT NULL COMMENT '是否是默认拓扑，1-是 0-否（预留）',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `bind_obj` longtext COMMENT '绑定对象',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='topo视图表';

-- ----------------------------
-- Table structure for alert_notify_template
-- ----------------------------
DROP TABLE IF EXISTS `alert_notify_template`;
CREATE TABLE `alert_notify_template` (
  `id` varchar(64) NOT NULL COMMENT 'id',
  `template_name` varchar(128) NOT NULL COMMENT '模板名称',
  `sms_template` varchar(2000) DEFAULT NULL COMMENT '短信模板',
  `email_template` varchar(2000) DEFAULT NULL COMMENT '邮件模板',
  `is_email_merge` varchar(2) DEFAULT NULL COMMENT '是否多条合并',
  `email_merge_template` varchar(2000) DEFAULT NULL COMMENT '合并模板',
  `is_delete` varchar(2) DEFAULT NULL COMMENT '是否删除，0-删除，1-未删除',
  `creater` varchar(255) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updater` varchar(255) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `subject` varchar(255) NULL COMMENT '邮件主题',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警通知模板表';


/*
 Navicat Premium Data Transfer

 Source Server         : 10.12.70.40
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : 10.12.70.40:3306
 Source Schema         : mirror

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 31/03/2020 18:08:04
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for alert_auto_order_config
-- ----------------------------
DROP TABLE IF EXISTS `alert_auto_order_config`;
CREATE TABLE `alert_auto_order_config`  (
  `uuid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '告警自动派单配置uuid',
  `config_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `config_time_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生效类型 1-永久生效 2-范围生效',
  `start_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '范围生效开始时间',
  `end_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '范围生效结束时间',
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户',
  `config_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `is_open` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '配置状态 1-开启 2-关闭',
  `alert_filter` varchar(2550) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '告警过滤',
  `order_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '工单类型 1-告警工单 2-故障工单 3-维保工单 4-调优工单',
  `order_time_space` int(10) NULL DEFAULT NULL COMMENT '派单间隔时间',
  `order_time_interval` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '派单时段',
  `creator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updater` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `is_delete` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除状态，0-未删除，1-已删除'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

/*
 Navicat Premium Data Transfer

 Source Server         : 10.12.70.40
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : 10.12.70.40:3306
 Source Schema         : mirror

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 31/03/2020 18:08:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for alert_auto_order_log
-- ----------------------------
DROP TABLE IF EXISTS `alert_auto_order_log`;
CREATE TABLE `alert_auto_order_log`  (
  `alert_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '告警id',
  `config_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '告警自动派单配置id',
  `device_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备id',
  `device_class` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备类型',
  `biz_sys` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '业务系统',
  `moni_index` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '监控指标/内容，关联触发器name',
  `moni_object` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '监控对象',
  `cur_moni_value` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当前监控值',
  `cur_moni_time` datetime(0) NULL DEFAULT NULL COMMENT '当前监控时间',
  `alert_level` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '告警级别\r\n1-提示\r\n2-低\r\n3-中\r\n4-高\r\n5-严重\r\n',
  `item_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remark` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `source` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '告警来源\r\nMIRROR\r\nZABBIX\r\n',
  `idc_type` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属位置-资源池',
  `source_room` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '机房/资源池',
  `device_type` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备类型',
  `device_mfrs` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备提供商',
  `host_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主机名',
  `device_model` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备型号',
  `object_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '告警类型\r\n1-系统\r\n2-业务',
  `object_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对象ID，如果是设备告警则是设备ID，如果是业务则是业务系统code',
  `region` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '域/资源池code',
  `device_ip` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `alert_start_time` datetime(0) NULL DEFAULT NULL COMMENT '告警开始时间',
  `prefix` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '告警前缀，用于多套告警系统时候区分标识',
  `order_time` datetime(0) NULL DEFAULT NULL COMMENT '当前派单时间',
  `order_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '派单类型',
  `order_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '工单号',
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '派单状态 0-失败 1-成功',
  INDEX `index_alert_id_order_type`(`alert_id`, `order_type`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '告警自动派单日志表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

/*
 Navicat Premium Data Transfer

 Source Server         : 10.12.70.40
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : 10.12.70.40:3306
 Source Schema         : mirror

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 31/03/2020 19:39:49
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for alert_model
-- ----------------------------
DROP TABLE IF EXISTS `alert_model`;
CREATE TABLE `alert_model`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '模型id',
  `parent_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父id',
  `model_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模型名称',
  `table_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据表名称',
  `sort` int(255) NOT NULL COMMENT '排序',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `creator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updater` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `is_delete` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否删除 0-未删除 1-已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '告警模型表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

/*
 Navicat Premium Data Transfer

 Source Server         : 10.12.70.40
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : 10.12.70.40:3306
 Source Schema         : mirror

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 31/03/2020 19:40:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for alert_model_field
-- ----------------------------
DROP TABLE IF EXISTS `alert_model_field`;
CREATE TABLE `alert_model_field`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '模型字段id',
  `model_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '模型id',
  `field_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段代码',
  `field_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段名称',
  `field_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段类型  1-基础字段 2-扩展字段',
  `is_lock` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否锁定',
  `jdbc_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据类型',
  `jdbc_length` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据长度',
  `jdbc_tip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '提示信息',
  `ci_field_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'CI字段编码',
  `is_ci_params` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否为参数ci参数',
  `params_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数名称',
  `ci_field_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'CI字段名称',
  `is_list_show` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否为列表展示',
  `list_show_sort` int(255) NULL DEFAULT NULL COMMENT '列表展示排序',
  `list_show_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '列表展示名称',
  `list_show_pattern` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列表展示格式',
  `table_column_widch` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表列宽度',
  `is_detail_show` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否为详情展示',
  `detail_show_sort` int(255) NULL DEFAULT NULL COMMENT '详情展示排序',
  `detail_show_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '详情展示名称',
  `detail_show_pattern` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '详情展示格式',
  `is_query_param` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否为查询参数',
  `query_param_sort` int(255) NULL DEFAULT NULL COMMENT '查询参数排序',
  `query_param_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查询参数名称',
  `query_param_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查询参数类型',
  `query_param_source` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查询参数来源',
  `is_derive_field` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否屏蔽字段',
  `derive_field_sort` int(255) NULL DEFAULT NULL COMMENT '屏蔽字段排序',
  `derive_field_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '屏蔽字段名称',
  `derive_field_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '屏蔽字段类型',
  `derive_field_source` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '屏蔽字段来源',
  `creator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updater` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


SET FOREIGN_KEY_CHECKS = 1;




-- ----------------------------
-- Table structure for kpi_config_dict
-- ----------------------------
DROP TABLE IF EXISTS `kpi_config_dict`;
CREATE TABLE `kpi_config_dict` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dict_type` varchar(255) NOT NULL,
  `dict_name` varchar(255) NOT NULL,
  `dict_value` varchar(255) NOT NULL,
  `description` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for kpi_config_key
-- ----------------------------
DROP TABLE IF EXISTS `kpi_config_key`;
CREATE TABLE `kpi_config_key` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `config_id` varchar(64) NOT NULL,
  `key_name` varchar(255) NOT NULL COMMENT '指标名称',
  `kpi_key` varchar(255) NOT NULL COMMENT '指标',
  `kpi_key_model` varchar(32) NOT NULL COMMENT '检索模式，1-精确，2-右模糊',
  `kpi_key_field` varchar(255) NOT NULL COMMENT '指标所属字段',
  `description` varchar(255) DEFAULT NULL COMMENT '指标描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=754 DEFAULT CHARSET=utf8 COMMENT='监控指标配置管理-指标项';

-- ----------------------------
-- Table structure for kpi_config_logs
-- ----------------------------
DROP TABLE IF EXISTS `kpi_config_logs`;
CREATE TABLE `kpi_config_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `config_id` varchar(64) NOT NULL,
  `config_name` varchar(255) NOT NULL,
  `tag` varchar(64) DEFAULT NULL COMMENT '标签',
  `from_time` datetime NOT NULL COMMENT '统计开始时间',
  `to_time` datetime NOT NULL COMMENT '统计结束时间',
  `exec_time` datetime NOT NULL COMMENT '执行时间',
  `exec_duration` varchar(64) DEFAULT NULL COMMENT '执行时间',
  `status` varchar(32) DEFAULT NULL COMMENT '状态，success，error',
  `content` varchar(4000) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_c_to` (`config_id`,`status`,`to_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11732 DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for kpi_config_manage
-- ----------------------------
DROP TABLE IF EXISTS `kpi_config_manage`;
CREATE TABLE `kpi_config_manage` (
  `id` varchar(64) NOT NULL,
  `kpi_name` varchar(255) NOT NULL COMMENT '指标配置名称',
  `kpi_type` varchar(32) NOT NULL COMMENT '指标类型，1-hour，2-day，3-month，4-自定义',
  `date_source` varchar(32) NOT NULL COMMENT '统计来源，zabbix、db、es、api',
  `cron` varchar(255) NOT NULL COMMENT 'cron表达式',
  `insert_object` varchar(255) NOT NULL COMMENT '录入对象，如果按时间分按照{yyyyMMdd}格式',
  `insert_object_type` varchar(255) NOT NULL COMMENT '录入对象类型',
  `relation_ci_fields` varchar(512) DEFAULT NULL COMMENT '关联ci字段',
  `exec_object` varchar(255) DEFAULT NULL COMMENT '执行对象，db、zabbix为表，es为索引，api则为请求地址',
  `exec_format_type` varchar(64) DEFAULT NULL COMMENT '执行类型，1-sql，2-dsl，3-自定义表达式，4-内置公式',
  `exec_format` varchar(2000) DEFAULT NULL COMMENT '执行标准格式',
  `exec_filter` varchar(1024) DEFAULT NULL COMMENT '过滤条件',
  `status` varchar(2) DEFAULT '1' COMMENT '状态，1-启用，2-停用',
  `is_delete` varchar(2) DEFAULT NULL COMMENT '是否删除，0-未删除，1-删除',
  `start_time` datetime DEFAULT NULL COMMENT '历史开始时间',
  `duration` int(11) DEFAULT NULL COMMENT '时间周期，单位：小时',
  `end_time` datetime DEFAULT NULL COMMENT '历史结束时间',
  `creater` varchar(255) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updater` varchar(255) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='监控指标配置管理';

-- ----------------------------
-- Table structure for kpi_result_transfer
-- ----------------------------
DROP TABLE IF EXISTS `kpi_result_transfer`;
CREATE TABLE `kpi_result_transfer` (
  `id` int(64) NOT NULL AUTO_INCREMENT,
  `config_id` varchar(64) DEFAULT NULL,
  `result_transfer_name` varchar(255) NOT NULL COMMENT '名称',
  `field_group` varchar(255) DEFAULT NULL COMMENT '分组',
  `source_field` varchar(255) DEFAULT NULL COMMENT '源字段',
  `target_field` varchar(255) NOT NULL COMMENT '目标字段',
  `transfer_type` varchar(2) DEFAULT NULL COMMENT '转换类型，1-固定值(date为特殊情况)，2-groovy，3-java函数，4-字典转换',
  `transfer_formula` varchar(1024) DEFAULT NULL COMMENT '转换公式',
  `description` varchar(512) DEFAULT NULL COMMENT '描述',
  `is_delete` varchar(2) DEFAULT '0' COMMENT '是否删除，1-删除，0-未删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=370 DEFAULT CHARSET=utf8 COMMENT='监控配置管理结果转换表';




SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for alert_isolate
-- ----------------------------
DROP TABLE IF EXISTS `alert_isolate`;
CREATE TABLE `alert_isolate`  (
  `id` bigint(11) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT COMMENT '告警屏蔽ID',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '规则名称',
  `description` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '规则描述',
  `status` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '启用状态，0-停用，1-启用',
  `effective_date` datetime(0) NULL DEFAULT NULL COMMENT '生效时间',
  `expire_date` datetime(0) NULL DEFAULT NULL COMMENT '失效时间',
  `operate_user` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '维护用户',
  `option_condition` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '过滤条件',
  `creater` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `editer` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `is_delete` tinyint(4) NULL DEFAULT 0 COMMENT '删除状态，0-未删除，1-已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 63 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '告警屏蔽表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `kpi_book_item`;
CREATE TABLE `kpi_book_item` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `idc_type` varchar(255) NOT NULL COMMENT '设备类型',
  `pod` varchar(255) DEFAULT NULL COMMENT '多个逗号分隔',
  `roomId` varchar(255) DEFAULT NULL COMMENT '多个逗号分隔',
  `keys_value` varchar(4000) NOT NULL COMMENT '多个,,/;分隔',
  `kpi_id` int(12) DEFAULT NULL,
  `deviceClass` varchar(255) DEFAULT '',
  `deviceType` varchar(255) DEFAULT NULL,
  `status` smallint(1) DEFAULT '1' COMMENT '1有效0无效',
  PRIMARY KEY (`id`),
  KEY `kpi_id` (`kpi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='性能数据配置';


SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for kpi_book_info
-- ----------------------------
DROP TABLE IF EXISTS `kpi_book_info`;
CREATE TABLE `kpi_book_info` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `interval_value` int(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '1' COMMENT '1有效0无效',
  `source` varchar(255) DEFAULT NULL COMMENT '来源',
  `subTopic` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='性能数据配置';

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_standardization
-- ----------------------------
DROP TABLE IF EXISTS `alert_standardization`;
CREATE TABLE `alert_standardization` (
  `id` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `conditions` varchar(255) DEFAULT NULL COMMENT '拼的查询条件:and idc_type=''信息港资源池''and source_room=''3050''',
  `source` varchar(64) DEFAULT NULL COMMENT '来源	比如：东华..',
  `displayCols` varchar(2000) DEFAULT NULL COMMENT '需要的字段:source_room,idc_type',
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `topic` varchar(255) DEFAULT NULL COMMENT 'kafka主题',
  `status` tinyint(1) DEFAULT '1' COMMENT '1有效0失效',
  `extraCols` varchar(255) DEFAULT NULL COMMENT '其他返回字段：2 aa,''东华''  from',
  `countType` tinyint(1) DEFAULT NULL COMMENT '统计类型：null全部，0实时告警1历史告警',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for cmdb_instance
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_instance`;
CREATE TABLE `cmdb_instance` (
  `instance_id` varchar(64) DEFAULT NULL COMMENT '设备id',
  `ip` varchar(40) DEFAULT NULL COMMENT '设备ip',
  `idcType` varchar(40) DEFAULT NULL COMMENT '资源池',
  `idcType_name` varchar(100) DEFAULT NULL COMMENT '资源池中文',
  `bizSystem` varchar(40) DEFAULT NULL COMMENT '业务系统',
  `bizSystem_name` varchar(100) DEFAULT NULL COMMENT '业务系统中文',
  `device_class` varchar(40) DEFAULT NULL COMMENT '设备分类',
  `device_class_name` varchar(100) DEFAULT NULL COMMENT '设备分类中文',
  `roomId` varchar(40) DEFAULT NULL COMMENT '机房位置',
  `roomId_name` varchar(100) DEFAULT NULL COMMENT '机房中文',
  `device_type` varchar(40) DEFAULT NULL COMMENT '设备类型',
  `device_type_name` varchar(100) DEFAULT NULL COMMENT '设备类型中文',
  `device_mfrs` varchar(40) DEFAULT NULL COMMENT '设备厂商',
  `host_name` varchar(300) DEFAULT NULL COMMENT '主机名',
  `department1` varchar(40) DEFAULT NULL COMMENT '一级部门',
  `department1_name` varchar(100) DEFAULT NULL COMMENT '一级部门中文',
  `department2` varchar(40) DEFAULT NULL COMMENT '二级部门',
  `department2_name` varchar(100) DEFAULT NULL COMMENT '二级部门中文',
  `device_model` varchar(40) DEFAULT NULL COMMENT '设备型号',
  `pod_name` varchar(40) DEFAULT NULL COMMENT 'Pod池',
  `pod_name_name` varchar(100) DEFAULT NULL COMMENT 'pod名称中文',
  `project_name` varchar(40) DEFAULT NULL COMMENT '工程期数',
  `project_name_name` varchar(100) DEFAULT NULL COMMENT '项目名称中文',
  `insert_time` varchar(40) DEFAULT NULL COMMENT '录入时间',
  `device_class_3` varchar(40) DEFAULT NULL COMMENT '设备三级分类',
  `device_name` varchar(225) DEFAULT NULL COMMENT '设备名称',
  `node_type` varchar(40) DEFAULT NULL,
  `node_type_name` varchar(100) DEFAULT NULL COMMENT '节点类型中文',
  `suyan_uuid` varchar(255) DEFAULT NULL,
  `is_ansible` varchar(40)  DEFAULT NULL COMMENT '管理网是否监控',
  `is_ipmi_monitor` varchar(40)  DEFAULT NULL COMMENT '带外网是否监控',
  UNIQUE KEY `idx_id_ip` (`instance_id`,`ip`) USING BTREE,
  KEY `idcTpe_ip` (`ip`,`idcType`),
  KEY `idx_ip_idc` (`ip`,`idcType_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for alert_config_business
-- ----------------------------
DROP TABLE IF EXISTS `alert_config_business`;
CREATE TABLE `alert_config_business` (
  `id` varchar(64) NOT NULL COMMENT 'ID',
  `name` varchar(64) NOT NULL COMMENT '规则名称',
  `description` varchar(1024) DEFAULT NULL COMMENT '规则描述',
  `status` varchar(1) DEFAULT '0' COMMENT '启用状态，0-停用，1-启用',
  `operate_user` varchar(512) DEFAULT NULL COMMENT '维护用户',
  `option_condition` longtext COMMENT '过滤条件',
  `creater` varchar(64) DEFAULT NULL COMMENT '创建人',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `editer` varchar(64) DEFAULT NULL COMMENT '修改人',
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `is_delete` tinyint(4) DEFAULT '0' COMMENT '删除状态，0-未删除，1-已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='业务告警规则配置表';

-- ----------------------------
-- Table structure for alert_config_exception
-- ----------------------------
DROP TABLE IF EXISTS `alert_config_exception`;
CREATE TABLE `alert_config_exception` (
  `id` varchar(64) NOT NULL COMMENT 'ID',
  `name` varchar(64) NOT NULL COMMENT '规则名称',
  `description` varchar(1024) DEFAULT NULL COMMENT '规则描述',
  `status` varchar(1) DEFAULT '0' COMMENT '启用状态，0-停用，1-启用',
  `operate_user` varchar(512) DEFAULT NULL COMMENT '维护用户',
  `option_condition` longtext COMMENT '过滤条件',
  `creater` varchar(64) DEFAULT NULL COMMENT '创建人',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `editer` varchar(64) DEFAULT NULL COMMENT '修改人',
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `is_delete` tinyint(4) DEFAULT '0' COMMENT '删除状态，0-未删除，1-已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='异常信息规则配置表';


-- ----------------------------
-- Table structure for alert_order_handle
-- ----------------------------
DROP TABLE IF EXISTS `alert_order_handle`;
CREATE TABLE `alert_order_handle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(64) NOT NULL COMMENT '工单编号',
  `status` varchar(2) NOT NULL COMMENT '状态，0-待处理；1-处理中；2-已处理；3-已关闭',
  `account` varchar(255) DEFAULT NULL COMMENT '账号',
  `exec_user` varchar(255) DEFAULT NULL COMMENT '执行人',
  `exec_time` datetime NOT NULL,
  `expire_status` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_id_status` (`order_id`,`status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='告警工单处理表';

-- ----------------------------
-- Table structure for alert_standard
-- ----------------------------
DROP TABLE IF EXISTS `alert_standard`;
CREATE TABLE `alert_standard` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_class` varchar(20) DEFAULT NULL COMMENT '设备分类',
  `device_type` varchar(20) DEFAULT NULL COMMENT '设备类型',
  `monitor_key` varchar(40) DEFAULT NULL COMMENT '监控指标key',
  `standard_name` varchar(40) DEFAULT NULL COMMENT '标准名称',
  `alert_desc` varchar(100) DEFAULT NULL COMMENT '告警描述',
  `status` varchar(2) DEFAULT NULL COMMENT '状态 ( 1 启用、0 禁用)',
  `alert_level` varchar(20) DEFAULT NULL COMMENT '告警等级 (低、中、高、重大)',
  `insert_person` varchar(20) DEFAULT NULL COMMENT '创建人员',
  `insert_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_delete` varchar(2) DEFAULT '0' COMMENT '是否删除（0 否 | 1 是）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8;
-- ----------------------------
-- Table structure for alert_alerts_merge
-- ----------------------------
DROP TABLE IF EXISTS `alert_alerts_merge`;
CREATE TABLE `alert_alerts_merge` (
  `alert_id` varchar(64) NOT NULL,
  `r_alert_id` varchar(300) DEFAULT NULL,
  `device_id` varchar(50) DEFAULT NULL,
  `device_class` varchar(128) DEFAULT NULL COMMENT '设备类型',
  `biz_sys` varchar(128) DEFAULT NULL COMMENT '业务系统',
  `moni_index` varchar(1024) NOT NULL COMMENT '监控指标/内容，关联触发器name',
  `moni_object` varchar(512) DEFAULT NULL COMMENT '监控对象',
  `item_key` varchar(128) DEFAULT NULL COMMENT '监控项',
  `key_comment` varchar(512) DEFAULT NULL COMMENT '监控项描述',
  `cur_moni_value` varchar(1024) DEFAULT NULL COMMENT '当前监控值',
  `cur_moni_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '当前监控时间',
  `alert_level` varchar(20) NOT NULL COMMENT '告警级别\r\n1-提示\r\n2-低\r\n3-中\r\n4-高\r\n5-严重\r\n',
  `item_id` varchar(50) DEFAULT NULL,
  `alert_end_time` datetime DEFAULT NULL COMMENT '告警结束时间',
  `remark` varchar(512) DEFAULT NULL COMMENT '备注',
  `order_status` varchar(50) DEFAULT NULL COMMENT '1-未派单\r\n2-处理中\r\n3-已完成\r\n',
  `operate_status` varchar(2) DEFAULT '0' COMMENT '操作状态',
  `notify_status` varchar(2) DEFAULT '0' COMMENT '告警通知状态，0-未通知，1-已通知',
  `clear_user` varchar(32) DEFAULT NULL COMMENT '清除人',
  `clear_content` varchar(256) DEFAULT NULL COMMENT '清除内容',
  `source` varchar(100) DEFAULT NULL COMMENT '告警来源\r\nMIRROR',
  `idc_type` varchar(128) DEFAULT NULL COMMENT '所属位置-资源池',
  `source_room` varchar(100) DEFAULT NULL COMMENT '机房',
  `device_type` varchar(128) DEFAULT NULL COMMENT '设备类型',
  `device_mfrs` varchar(128) DEFAULT NULL COMMENT '设备提供商',
  `host_name` varchar(128) DEFAULT NULL COMMENT '主机名',
  `device_model` varchar(128) DEFAULT NULL COMMENT '设备型号',
  `project_name` varchar(128) DEFAULT NULL COMMENT '工程期数',
  `pod_name` varchar(128) DEFAULT NULL COMMENT 'pod池',
  `object_type` varchar(50) DEFAULT NULL,
  `object_id` varchar(50) DEFAULT NULL,
  `device_ip` varchar(100) DEFAULT NULL,
  `order_type` varchar(20) DEFAULT NULL COMMENT '工单类型\r\n1-告警\r\n2-故障',
  `order_id` varchar(100) DEFAULT NULL COMMENT '工单ID',
  `alert_start_time` datetime DEFAULT NULL COMMENT '告警开始时间',
  `count` int(11) DEFAULT '1',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`alert_id`),
  KEY `index_mrfs_top10` (`alert_level`,`device_mfrs`,`idc_type`,`device_type`,`device_model`,`cur_moni_time`) USING BTREE,
  KEY `index_time_level` (`alert_level`,`alert_end_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警历史记录表';

-- ----------------------------
-- Table structure for fault_report_manage
-- ----------------------------
DROP TABLE IF EXISTS `fault_report_manage`;
CREATE TABLE `fault_report_manage` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `fault_id` varchar(64) DEFAULT NULL COMMENT '故障id',
  `fault_report_user` varchar(64) DEFAULT NULL COMMENT '上报人',
  `fault_report_mobile` varchar(64) DEFAULT NULL COMMENT '上报人电话',
  `fault_report_email` varchar(64) DEFAULT NULL COMMENT '上报人邮箱',
  `fault_report_bizsys` varchar(128) DEFAULT NULL COMMENT '上报人业务组',
  `fault_report_time` datetime DEFAULT NULL COMMENT '上报时间',
  `fault_happen_time` datetime DEFAULT NULL COMMENT '故障发生时间',
  `fault_report_timely` varchar(2) DEFAULT NULL COMMENT '上报是否及时，1-及时，2-超时',
  `fault_idc_type` varchar(128) DEFAULT NULL COMMENT '资源池',
  `fault_order_id` varchar(64) DEFAULT NULL COMMENT '上报工单id',
  `fault_content` varchar(1024) DEFAULT NULL COMMENT '上报内容简述',
  `fault_regtime` datetime DEFAULT NULL COMMENT '接收故障时间',
  `report_title` varchar(1024) DEFAULT NULL COMMENT '故障报告名称',
  `report_user` varchar(64) DEFAULT NULL COMMENT '报告编写人',
  `report_mobile` varchar(64) DEFAULT NULL COMMENT '编写人电话',
  `report_email` varchar(64) DEFAULT NULL COMMENT '编写人邮箱',
  `report_bizsys` varchar(128) DEFAULT NULL COMMENT '编写人业务组',
  `report_wn_id` varchar(64) DEFAULT NULL COMMENT '全网id',
  `report_order_id` varchar(64) DEFAULT NULL COMMENT '报告工单id',
  `report_create_time` datetime DEFAULT NULL COMMENT '事件单下发时间',
  `report_plain_finish` datetime DEFAULT NULL COMMENT '事件要求完成时间',
  `report_finish_time` datetime DEFAULT NULL COMMENT '事件实际完成时间',
  `report_timely` varchar(2) DEFAULT NULL COMMENT '故障上报及时性，1-及时，2-超时',
  `report_platform_recoverytime` datetime DEFAULT NULL COMMENT '平台恢复时间',
  `report_platform_recoveryhours` float(21,2) DEFAULT NULL COMMENT '平台恢复时长',
  `report_bizsys_recoverytime` datetime DEFAULT NULL COMMENT '业务恢复时间',
  `report_bizsys_recoveryhours` float(21,2) DEFAULT NULL COMMENT '业务恢复时长',
  `report_affect_bizsyss` int(11) DEFAULT NULL COMMENT '故障影响业务数量',
  `report_affect_describe` varchar(1024) DEFAULT NULL COMMENT '故障影响描述',
  `report_type` varchar(64) DEFAULT NULL COMMENT '故障分类',
  `report_resson` varchar(1024) DEFAULT NULL COMMENT '故障原因描述',
  `report_producer` varchar(64) DEFAULT NULL COMMENT '故障厂家',
  `report_nature` varchar(64) DEFAULT NULL COMMENT '事件性质',
  `report_deduct_points` varchar(64) DEFAULT NULL COMMENT '扣分',
  `report_enclosure` varchar(255) DEFAULT NULL COMMENT '故障报告附件',
  `report_follow_plan` varchar(1024) DEFAULT NULL COMMENT '后续计划',
  `report_follow_plan_explain` varchar(1024) DEFAULT NULL COMMENT '后续计划说明',
  `report_remark` varchar(1024) DEFAULT NULL COMMENT '其它附注信息',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='故障数据表';

-- ----------------------------
-- Table structure for config_compare
-- ----------------------------
DROP TABLE IF EXISTS `config_compare`;
CREATE TABLE `config_compare` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `idc_type` varchar(64) NOT NULL COMMENT '资源池',
  `master_ip` varchar(64) NOT NULL COMMENT '主ip',
  `backup_ip` varchar(64) NOT NULL COMMENT '备ip',
  `brand` varchar(64) DEFAULT NULL COMMENT '厂商',
  `add_count` int(11) DEFAULT NULL,
  `modify_count` int(11) DEFAULT NULL,
  `del_count` int(11) DEFAULT NULL,
  `add_datas` text COMMENT '新增未同步',
  `modify_datas` text COMMENT '修改未同步',
  `del_datas` text COMMENT '删除未同步',
  `compare_time` datetime DEFAULT NULL COMMENT '最新比对时间',
  `master_config_file` varchar(255) DEFAULT NULL COMMENT '主配置原始文件',
  `backup_config_file` varchar(255) DEFAULT NULL COMMENT '备配置原始文件',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_user` varchar(64) NOT NULL COMMENT '创建人',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_uni_idc_ip` (`idc_type`,`master_ip`,`backup_ip`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COMMENT='主备配置比对清单表';

-- ----------------------------
-- Table structure for config_compare_logs
-- ----------------------------
DROP TABLE IF EXISTS `config_compare_logs`;
CREATE TABLE `config_compare_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `compare_id` int(11) NOT NULL,
  `idc_type` varchar(64) NOT NULL,
  `master_ip` varchar(64) NOT NULL,
  `backup_ip` varchar(64) NOT NULL,
  `master_config_file` varchar(255) DEFAULT NULL COMMENT '主配置原始文件',
  `backup_config_file` varchar(255) DEFAULT NULL COMMENT '备配置原始文件',
  `add_result` text COMMENT '新增未同步',
  `modify_result` text COMMENT '修改未同步',
  `del_result` text COMMENT '删除未同步',
  `compare_time` datetime DEFAULT NULL COMMENT '比对时间',
  `compare_user` varchar(255) DEFAULT NULL COMMENT '操作人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='主备配置比对日志表';

-- ----------------------------
-- Table structure for kpi_monitor_full_link
-- ----------------------------
DROP TABLE IF EXISTS `kpi_monitor_full_link`;
CREATE TABLE `kpi_monitor_full_link` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `item_id` varchar(32) NOT NULL,
  `ip` varchar(64) NOT NULL,
  `idc_type` varchar(64) DEFAULT NULL,
  `device_class` varchar(64) DEFAULT NULL,
  `component_name` varchar(64) default null,
  `kpi_key` varchar(255) NOT NULL,
  `kpi_name` varchar(255) DEFAULT NULL,
  `value` double NOT NULL,
  `units` varchar(32) DEFAULT NULL,
  `clock` int(11) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5533 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for kpi_monitor_full_link_ip
-- ----------------------------
DROP TABLE IF EXISTS `kpi_monitor_full_link_ip`;
CREATE TABLE `kpi_monitor_full_link_ip` (  
  `tag` VARCHAR(64) NOT NULL,
  `ip` VARCHAR(64) NOT NULL,
  `date_time` DATETIME,
  PRIMARY KEY (`tag`, `ip`) 
)
COMMENT='自监控ip校验表';

-- ----------------------------
-- Procedure structure for PRO_MERGE_ALERT
-- ----------------------------
DROP PROCEDURE IF EXISTS `PRO_MERGE_ALERT`;
DELIMITER ;;
CREATE PROCEDURE `PRO_MERGE_ALERT`()
BEGIN
DELETE from alert_alerts_merge;

INSERT INTO `alert_alerts_merge` (`alert_id`, `r_alert_id`, `device_id`, `device_class`, `biz_sys`, `moni_index`, `moni_object`, `item_key`, `key_comment`, `cur_moni_value`, `cur_moni_time`, `alert_level`, `item_id`, `alert_end_time`, `remark`, `order_status`, `operate_status`, `notify_status`, `clear_user`, `clear_content`, `source`, `idc_type`, `source_room`, `device_type`, `device_mfrs`, `host_name`, `device_model`, `project_name`, `pod_name`, `object_type`, `object_id`, `device_ip`, `order_type`, `order_id`, `alert_start_time`, `count`, `create_time`) 
select `alert_id`, `r_alert_id`, `device_id`, `device_class`, `biz_sys`, `moni_index`, `moni_object`, `item_key`, `key_comment`, `cur_moni_value`, `cur_moni_time`, `alert_level`, `item_id`, `alert_end_time`, `remark`, `order_status`, `operate_status`, `notify_status`, `clear_user`, `clear_content`, `source`, `idc_type`, `source_room`, `device_type`, `device_mfrs`, `host_name`, `device_model`, `project_name`, `pod_name`, `object_type`, `object_id`, `device_ip`, `order_type`, `order_id`, `alert_start_time`, `count`, `create_time`
from alert_alerts_his aah
JOIN (select device_ip device_ip1,alert_level alert_level1, item_id item_id1, max(cur_moni_time) cur_moni_time1, COUNT(1) count from alert_alerts_his GROUP BY device_ip,alert_level, item_id)a_tmp
on aah.device_ip=a_tmp.device_ip1 and aah.alert_level=a_tmp.alert_level1 and aah.item_id=a_tmp.item_id1 and aah.cur_moni_time=a_tmp.cur_moni_time1
where not EXISTS (select 1 from alert_alerts a where a.device_ip=a_tmp.device_ip1 and a.alert_level=a_tmp.alert_level1 and a.item_id=a_tmp.item_id1);


INSERT INTO `alert_alerts_merge` (`alert_id`, `r_alert_id`, `device_id`, `device_class`, `biz_sys`, `moni_index`, `moni_object`, `item_key`, `key_comment`, `cur_moni_value`, `cur_moni_time`, `alert_level`, `item_id`, `alert_end_time`, `remark`, `order_status`, `operate_status`, `notify_status`, `clear_user`, `clear_content`, `source`, `idc_type`, `source_room`, `device_type`, `device_mfrs`, `host_name`, `device_model`, `project_name`, `pod_name`, `object_type`, `object_id`, `device_ip`, `order_type`, `order_id`, `alert_start_time`, `count`, `create_time`) 
select `alert_id`, `r_alert_id`, `device_id`, `device_class`, `biz_sys`, `moni_index`, `moni_object`, `item_key`, `key_comment`, `cur_moni_value`, `cur_moni_time`, `alert_level`, `item_id`, null, `remark`, `order_status`, `operate_status`, `notify_status`, null, null, `source`, `idc_type`, `source_room`, `device_type`, `device_mfrs`, `host_name`, `device_model`, `project_name`, `pod_name`, `object_type`, `object_id`, `device_ip`, `order_type`, `order_id`, `alert_start_time`, `count` + 1 as count, `create_time`
from alert_alerts a
JOIN (select device_ip device_ip1,alert_level alert_level1, item_id item_id1, COUNT(1) count from alert_alerts_his GROUP BY device_ip,alert_level, item_id)a_tmp
on a.device_ip=a_tmp.device_ip1 and a.alert_level=a_tmp.alert_level1 and a.item_id=a_tmp.item_id1;

INSERT INTO `alert_alerts_merge` (`alert_id`, `r_alert_id`, `device_id`, `device_class`, `biz_sys`, `moni_index`, `moni_object`, `item_key`, `key_comment`, `cur_moni_value`, `cur_moni_time`, `alert_level`, `item_id`, `alert_end_time`, `remark`, `order_status`, `operate_status`, `notify_status`, `clear_user`, `clear_content`, `source`, `idc_type`, `source_room`, `device_type`, `device_mfrs`, `host_name`, `device_model`, `project_name`, `pod_name`, `object_type`, `object_id`, `device_ip`, `order_type`, `order_id`, `alert_start_time`, `count`, `create_time`) 
select `alert_id`, `r_alert_id`, `device_id`, `device_class`, `biz_sys`, `moni_index`, `moni_object`, `item_key`, `key_comment`, `cur_moni_value`, `cur_moni_time`, `alert_level`, `item_id`, null, `remark`, `order_status`, `operate_status`, `notify_status`, null, null, `source`, `idc_type`, `source_room`, `device_type`, `device_mfrs`, `host_name`, `device_model`, `project_name`, `pod_name`, `object_type`, `object_id`, `device_ip`, `order_type`, `order_id`, `alert_start_time`, 1, `create_time`
from alert_alerts a
where not EXISTS (select 1 from alert_alerts_his aah where aah.device_ip=a.device_ip and aah.alert_level=a.alert_level and aah.item_id=aah.item_id);

END
;;
DELIMITER ;

-- ----------------------------
-- Event structure for EVENT_MERGE_ALERT
-- ----------------------------
DROP EVENT IF EXISTS `EVENT_MERGE_ALERT`;
DELIMITER ;;
CREATE  EVENT `EVENT_MERGE_ALERT` ON SCHEDULE EVERY 1 DAY STARTS '2020-05-22 16:32:46' ON COMPLETION NOT PRESERVE ENABLE DO call PRO_MERGE_ALERT
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for PRD_MERGE_NOTIFY_LOG
-- ----------------------------
DROP PROCEDURE IF EXISTS `PRD_MERGE_NOTIFY_LOG`;
DELIMITER ;;
CREATE PROCEDURE `PRD_MERGE_NOTIFY_LOG`()
BEGIN
DECLARE last_month_time VARCHAR(20);
SELECT DATE_SUB(curdate(),INTERVAL 1 MONTH) into last_month_time;
INSERT INTO alert_notify_config_log_his (id, `alert_notify_config_id`, `send_status`, `receiver`, `send_time`, `send_content`, `send_alert_id`, `send_ype`, `resend_time`, `is_resend`) 
select id, `alert_notify_config_id`, `send_status`, `receiver`, `send_time`, `send_content`, `send_alert_id`, `send_ype`, `resend_time`, `is_resend`
from alert_notify_config_log where send_time < last_month_time
on DUPLICATE KEY UPDATE id=VALUES(id);
DELETE from alert_notify_config_log where send_time < last_month_time;
SELECT DATE_SUB(curdate(),INTERVAL 1 MONTH) into last_month_time;
INSERT INTO alert_notify_config_log_his (id, `alert_notify_config_id`, `send_status`, `receiver`, `send_time`, `send_content`, `send_alert_id`, `send_ype`, `resend_time`, `is_resend`) 
select id, `alert_notify_config_id`, `send_status`, `receiver`, `send_time`, `send_content`, `send_alert_id`, `send_ype`, `resend_time`, `is_resend`
from alert_notify_config_log where resend_time < last_month_time
on DUPLICATE KEY UPDATE id=VALUES(id);
DELETE from alert_notify_config_log where resend_time < last_month_time;
END
;;
DELIMITER ;

-- ----------------------------
-- Event structure for EVENT_MERGE_NOTIFY_LOG
-- ----------------------------
DROP EVENT IF EXISTS `EVENT_MERGE_NOTIFY_LOG`;
DELIMITER ;;
CREATE EVENT `EVENT_MERGE_NOTIFY_LOG` ON SCHEDULE EVERY 1 DAY STARTS '2020-08-06 01:00:00' ON COMPLETION NOT PRESERVE ENABLE COMMENT '超过1月通知记录移入历史表' DO call PRD_MERGE_NOTIFY_LOG()
;;
DELIMITER ;

DROP TABLE IF EXISTS `alert_idctype_performance_sync`;
CREATE TABLE `alert_idctype_performance_sync` (
  `idcType` varchar(64) DEFAULT NULL COMMENT '资源池,all就是查的所有资源池',
  `item` varchar(12) DEFAULT NULL COMMENT 'cpu/memory',
  `deviceType` varchar(12) DEFAULT NULL COMMENT '设备类型：物理计算资源（X86服务器）；虚拟计算资源（云主机）',
  `fifteen_ratio` decimal(12,2) DEFAULT NULL COMMENT '15%>CPU峰值占比',
  `thirty_ratio` decimal(12,2) DEFAULT NULL COMMENT '80%>CPU峰值>=40%占比',
  `sixty_ratio` decimal(12,2) DEFAULT NULL COMMENT '40%>CPU峰值>=15%占比',
  `eighty_five_ratio` decimal(12,2) DEFAULT NULL,
  `hundred_ratio` decimal(12,2) DEFAULT NULL COMMENT 'CPU峰值>=80%占比',
  `day` varchar(12) DEFAULT NULL COMMENT '日期',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `fifteen_count` int(12) DEFAULT NULL COMMENT '15%>CPU峰值占比',
  `thirty_count` int(12) DEFAULT NULL COMMENT '80%>CPU峰值>=40%占比',
  `sixty_count` int(12) DEFAULT NULL COMMENT '40%>CPU峰值>=15%占比',
  `eighty_five_count` int(12) DEFAULT NULL,
  `hundred_count` int(12) DEFAULT NULL COMMENT 'CPU峰值>=80%占比',
  KEY `day` (`day`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='物理计算资源/虚拟计算资源 表';

