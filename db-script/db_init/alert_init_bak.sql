/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 16:58:54
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_alerts
-- ----------------------------
DROP TABLE IF EXISTS `alert_alerts`;
CREATE TABLE `alert_alerts` (
  `alert_id` varchar(64) NOT NULL,
  `r_alert_id` varchar(300) DEFAULT NULL,
  `event_id` varchar(80) DEFAULT NULL,
  `action_id` bigint(10) DEFAULT NULL,
  `device_id` varchar(50) DEFAULT NULL,
  `device_class` varchar(128) DEFAULT NULL COMMENT '设备类型',
  `biz_sys` varchar(128) DEFAULT NULL COMMENT '业务系统',
  `moni_index` varchar(1024) DEFAULT NULL COMMENT '监控指标/内容，关联触发器name',
  `moni_object` varchar(512) DEFAULT NULL COMMENT '监控对象',
  `cur_moni_value` varchar(512) DEFAULT NULL COMMENT '当前监控值',
  `cur_moni_time` datetime DEFAULT NULL COMMENT '当前监控时间',
  `alert_level` varchar(20) NOT NULL COMMENT '告警级别\r\n1-提示\r\n2-低\r\n3-中\r\n4-高\r\n5-严重\r\n',
  `item_id` varchar(50) DEFAULT NULL,
  `remark` varchar(512) DEFAULT NULL COMMENT '备注',
  `order_status` varchar(50) DEFAULT NULL COMMENT '1-未派单\r\n2-处理中\r\n3-已完成\r\n',
  `operate_status` tinyint(1) DEFAULT '0' COMMENT '警报操作状态:0-待确认,1-已确认,2-待解决,3-已处理',
  `source` varchar(100) DEFAULT NULL COMMENT '告警来源\r\nMIRROR\r\nZABBIX\r\n',
  `idc_type` varchar(128) DEFAULT NULL COMMENT '所属位置-资源池',
  `source_room` varchar(100) DEFAULT NULL COMMENT '机房/资源池',
  `device_type` varchar(128) DEFAULT NULL COMMENT '设备类型',
  `device_mfrs` varchar(128) DEFAULT NULL COMMENT '设备提供商',
  `host_name` varchar(128) DEFAULT NULL COMMENT '主机名',
  `device_model` varchar(128) DEFAULT NULL COMMENT '设备型号',
  `object_type` varchar(50) DEFAULT NULL COMMENT '告警类型\r\n1-系统\r\n2-业务',
  `object_id` varchar(50) DEFAULT NULL COMMENT '对象ID，如果是设备告警则是设备ID，如果是业务则是业务系统code',
  `region` varchar(50) DEFAULT NULL COMMENT '域/资源池code',
  `device_ip` varchar(100) DEFAULT NULL,
  `order_id` varchar(100) DEFAULT NULL,
  `order_type` varchar(100) DEFAULT NULL,
  `alert_start_time` datetime DEFAULT NULL COMMENT '告警开始时间',
  `prefix` varchar(64) DEFAULT NULL COMMENT '告警前缀，用于多套告警系统时候区分标识',
  `alert_count` int(11) DEFAULT '1',
  `message_open` varchar(10) DEFAULT NULL COMMENT '是否发短信，0：不需要发送短信；1：需要发送短信',
  `message_send` varchar(10) DEFAULT NULL COMMENT '是否已发短信，0：未发；1：已发',
  `mail_open` varchar(10) DEFAULT NULL COMMENT '是否需要发送邮件，0：不需要发送邮件；1：需要发送邮件',
  `mail_send` varchar(10) DEFAULT NULL COMMENT '是否已发邮件，0：未发；1：已发',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`alert_id`),
  KEY `index_r_alert_id` (`r_alert_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警记录表';

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 16:59:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_alerts_big
-- ----------------------------
DROP TABLE IF EXISTS `alert_alerts_big`;
CREATE TABLE `alert_alerts_big` (
  `alert_id` varchar(64) NOT NULL,
  `r_alert_id` varchar(300) DEFAULT NULL,
  `event_id` varchar(80) DEFAULT NULL,
  `action_id` bigint(10) DEFAULT NULL,
  `device_id` varchar(50) DEFAULT NULL,
  `device_class` varchar(128) DEFAULT NULL COMMENT '设备类型',
  `biz_sys` varchar(128) DEFAULT NULL COMMENT '业务系统',
  `moni_index` varchar(1024) DEFAULT NULL COMMENT '监控指标/内容，关联触发器name',
  `moni_object` varchar(512) DEFAULT NULL COMMENT '监控对象',
  `cur_moni_value` varchar(128) DEFAULT NULL COMMENT '当前监控值',
  `cur_moni_time` datetime DEFAULT NULL COMMENT '当前监控时间',
  `alert_level` varchar(20) NOT NULL COMMENT '告警级别\r\n1-提示\r\n2-低\r\n3-中\r\n4-高\r\n5-严重\r\n',
  `item_id` varchar(50) DEFAULT NULL,
  `remark` varchar(512) DEFAULT NULL COMMENT '备注',
  `order_status` varchar(50) DEFAULT NULL COMMENT '1-未派单\r\n2-处理中\r\n3-已完成\r\n',
  `operate_status` tinyint(1) DEFAULT '0' COMMENT '警报操作状态:0-待确认,1-已确认,2-待解决,3-已处理',
  `source` varchar(100) DEFAULT NULL COMMENT '告警来源\r\nMIRROR\r\nZABBIX\r\n',
  `idc_type` varchar(128) DEFAULT NULL COMMENT '所属位置-资源池',
  `source_room` varchar(100) DEFAULT NULL COMMENT '机房/资源池',
  `device_type` varchar(128) DEFAULT NULL COMMENT '设备类型',
  `device_mfrs` varchar(128) DEFAULT NULL COMMENT '设备提供商',
  `device_model` varchar(128) DEFAULT NULL COMMENT '设备型号',
  `object_type` varchar(50) DEFAULT NULL COMMENT '告警类型\r\n1-系统\r\n2-业务',
  `object_id` varchar(50) DEFAULT NULL COMMENT '对象ID，如果是设备告警则是设备ID，如果是业务则是业务系统code',
  `region` varchar(50) DEFAULT NULL COMMENT '域/资源池code',
  `device_ip` varchar(100) DEFAULT NULL,
  `order_id` varchar(100) DEFAULT NULL,
  `order_type` varchar(100) DEFAULT NULL COMMENT '1、告警工单2、故障工单',
  `alert_start_time` datetime DEFAULT NULL COMMENT '告警开始时间',
  `prefix` varchar(64) DEFAULT NULL,
  `alert_count` int(11) DEFAULT '1',
  `message_open` varchar(10) DEFAULT NULL COMMENT '是否发短信，0：不需要发送短信；1：需要发送短信',
  `message_send` varchar(10) DEFAULT NULL COMMENT '是否已发短信，0：未发；1：已发',
  `mail_open` varchar(10) DEFAULT NULL COMMENT '是否需要发送邮件，0：不需要发送邮件；1：需要发送邮件',
  `mail_send` varchar(10) DEFAULT NULL COMMENT '是否已发邮件，0：未发；1：已发',
  PRIMARY KEY (`alert_id`),
  KEY `index_r_alert_id` (`r_alert_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警记录表';

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:00:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_alerts_detail
-- ----------------------------
DROP TABLE IF EXISTS `alert_alerts_detail`;
CREATE TABLE `alert_alerts_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `alert_id` varchar(64) NOT NULL,
  `action_id` bigint(10) DEFAULT NULL,
  `event_id` varchar(80) DEFAULT NULL,
  `moni_index` varchar(1024) DEFAULT NULL COMMENT '监控指标/内容，关联触发器name',
  `moni_object` varchar(512) DEFAULT NULL COMMENT '监控对象',
  `cur_moni_value` varchar(128) DEFAULT NULL COMMENT '当前监控值',
  `cur_moni_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '当前监控时间',
  `alert_level` varchar(20) NOT NULL COMMENT '告警级别\r\n1-提示\r\n2-低\r\n3-中\r\n4-高\r\n5-严重\r\n',
  `item_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_alert_id` (`alert_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10263690 DEFAULT CHARSET=utf8 COMMENT='告警上报记录表';

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:00:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_alerts_his
-- ----------------------------
DROP TABLE IF EXISTS `alert_alerts_his`;
CREATE TABLE `alert_alerts_his` (
  `alert_id` varchar(64) NOT NULL,
  `r_alert_id` varchar(300) DEFAULT NULL,
  `event_id` varchar(80) DEFAULT NULL,
  `action_id` bigint(10) DEFAULT NULL,
  `device_id` varchar(50) DEFAULT NULL,
  `device_class` varchar(128) DEFAULT NULL COMMENT '设备类型',
  `biz_sys` varchar(128) DEFAULT NULL COMMENT '业务系统',
  `moni_index` varchar(1024) NOT NULL COMMENT '监控指标/内容，关联触发器name',
  `moni_object` varchar(512) DEFAULT NULL COMMENT '监控对象',
  `cur_moni_value` varchar(128) DEFAULT NULL COMMENT '当前监控值',
  `cur_moni_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '当前监控时间',
  `alert_level` varchar(20) NOT NULL COMMENT '告警级别\r\n1-提示\r\n2-低\r\n3-中\r\n4-高\r\n5-严重\r\n',
  `item_id` varchar(50) DEFAULT NULL,
  `alert_end_time` datetime DEFAULT NULL COMMENT '告警结束时间',
  `remark` varchar(512) DEFAULT NULL COMMENT '备注',
  `order_status` varchar(50) DEFAULT NULL COMMENT '1-未派单\r\n2-处理中\r\n3-已完成\r\n',
  `clear_time` datetime DEFAULT NULL COMMENT '清除时间',
  `clear_user` varchar(32) DEFAULT NULL COMMENT '清除人',
  `clear_content` varchar(256) DEFAULT NULL COMMENT '清除内容',
  `source` varchar(100) DEFAULT NULL COMMENT '告警来源\r\nMIRROR',
  `idc_type` varchar(128) DEFAULT NULL COMMENT '所属位置-资源池',
  `source_room` varchar(100) DEFAULT NULL COMMENT '机房',
  `device_type` varchar(128) DEFAULT NULL COMMENT '设备类型',
  `device_mfrs` varchar(128) DEFAULT NULL COMMENT '设备提供商',
  `host_name` varchar(128) DEFAULT NULL COMMENT '主机名',
  `device_model` varchar(128) DEFAULT NULL COMMENT '设备型号',
  `object_type` varchar(50) DEFAULT NULL,
  `object_id` varchar(50) DEFAULT NULL,
  `region` varchar(50) DEFAULT NULL,
  `device_ip` varchar(100) DEFAULT NULL,
  `order_type` varchar(20) DEFAULT NULL COMMENT '工单类型\r\n1-告警\r\n2-故障',
  `order_id` varchar(100) DEFAULT NULL COMMENT '工单ID',
  `biz_sys_desc` varchar(200) DEFAULT NULL COMMENT '业务系统名称（中文）',
  `alert_start_time` datetime DEFAULT NULL COMMENT '告警开始时间',
  `prefix` varchar(64) DEFAULT NULL,
  `alert_count` int(11) DEFAULT '1',
  `message_open` varchar(10) DEFAULT NULL COMMENT '是否发短信，0：不需要发送短信；1：需要发送短信',
  `message_send` varchar(10) DEFAULT NULL COMMENT '是否已发短信，0：未发；1：已发',
  `mail_open` varchar(10) DEFAULT NULL COMMENT '是否需要发送邮件，0：不需要发送邮件；1：需要发送邮件',
  `mail_send` varchar(10) DEFAULT NULL COMMENT '是否已发邮件，0：未发；1：已发',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`alert_id`),
  KEY `index_r_alert_id` (`r_alert_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警历史记录表';

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:02:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_alerts_statistics_sync
-- ----------------------------
DROP TABLE IF EXISTS `alert_alerts_statistics_sync`;
CREATE TABLE `alert_alerts_statistics_sync` (
  `idcType` varchar(300) DEFAULT NULL,
  `alertTotal` bigint(128) DEFAULT NULL,
  `seriousCount` bigint(128) DEFAULT NULL COMMENT '严重告警',
  `importantCount` bigint(128) DEFAULT NULL COMMENT '重要告警',
  `secondaryCount` bigint(128) DEFAULT NULL COMMENT '次要告警',
  `tipsCount` bigint(128) DEFAULT NULL COMMENT '提示告警',
  `month` varchar(24) DEFAULT NULL COMMENT '告警产生月份',
  `updateTime` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  KEY `index_r_alert_id` (`idcType`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警数量统计表';

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:05:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_config
-- ----------------------------
DROP TABLE IF EXISTS `alert_config`;
CREATE TABLE `alert_config` (
  `ID` varchar(255) NOT NULL,
  `TITLE` varchar(64) DEFAULT NULL,
  `DESCRIPTION` text,
  `CREATOR` varchar(64) DEFAULT NULL,
  `CRATE_TIME` datetime DEFAULT NULL,
  `IS_START` char(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alert_config
-- ----------------------------
INSERT INTO `alert_config` VALUES ('1', '自动派单', '自动派单', 'alauda', '2019-06-06 14:18:55', '0');
INSERT INTO `alert_config` VALUES ('2a6e74cb-72d3-4a7d-a3e2-c30285a687bf', '告警自动派单', '--组自动派单', 'admin', '2019-05-22 15:01:37', '0');

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:06:21
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_config_detail
-- ----------------------------
DROP TABLE IF EXISTS `alert_config_detail`;
CREATE TABLE `alert_config_detail` (
  `ID` varchar(32) NOT NULL,
  `ALERT_CONFIG_ID` varchar(255) DEFAULT NULL,
  `CONFIG_TYPE` char(1) DEFAULT NULL,
  `TARGET_ID` text,
  `MONIT_TYPE` varchar(64) DEFAULT NULL,
  `ALERT_LEVEL` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ALERT_CONFIG_ID_f` (`ALERT_CONFIG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alert_config_detail
-- ----------------------------
INSERT INTO `alert_config_detail` VALUES ('1', '1', null, null, null, '5');
INSERT INTO `alert_config_detail` VALUES ('715651f57c5f11e9b0c30242ac110002', '2a6e74cb-72d3-4a7d-a3e2-c30285a687bf', '0', null, null, null);

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:06:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_device_top_sync
-- ----------------------------
DROP TABLE IF EXISTS `alert_device_top_sync`;
CREATE TABLE `alert_device_top_sync` (
  `idcType` varchar(300) DEFAULT NULL COMMENT '资源池',
  `alertLevel` varchar(12) DEFAULT NULL COMMENT '告警级别',
  `deviceType` varchar(10) DEFAULT NULL COMMENT '设备分类',
  `countOrder` int(1) DEFAULT NULL COMMENT '排名',
  `ip` varchar(128) DEFAULT NULL COMMENT '设备ip',
  `pod` varchar(128) DEFAULT NULL COMMENT 'pod',
  `alertCount` bigint(128) DEFAULT NULL COMMENT '告警数量',
  `roomId` varchar(128) DEFAULT NULL COMMENT '机房位置',
  `month` varchar(24) DEFAULT NULL COMMENT '告警产生时间',
  `deviceMfrs` varchar(255) DEFAULT NULL COMMENT '厂家',
  `updateTime` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  KEY `index_r_alert_id` (`idcType`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警分类top10统计表';

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:07:21
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_devicetype_config
-- ----------------------------
DROP TABLE IF EXISTS `alert_devicetype_config`;
CREATE TABLE `alert_devicetype_config` (
  `id` varchar(56) NOT NULL,
  `code` varchar(128) NOT NULL COMMENT '表里面存的名称',
  `name` varchar(255) DEFAULT NULL COMMENT '展示名称',
  `type` varchar(255) DEFAULT NULL COMMENT '类型：网络、物理服务器等'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='设备分类统计配置表';

-- ----------------------------
-- Records of alert_devicetype_config
-- ----------------------------
INSERT INTO `alert_devicetype_config` VALUES ('physicalMachine', '物理机', '物理服务器', 'physicServer_moniter_object');
INSERT INTO `alert_devicetype_config` VALUES ('firewall', '防火墙', '物理防火墙', 'network_moniter_object');
INSERT INTO `alert_devicetype_config` VALUES ('router', '路由器', '物理路由器', 'network_moniter_object');
INSERT INTO `alert_devicetype_config` VALUES ('switch', '交换机', '物理交换机', 'network_moniter_object');
INSERT INTO `alert_devicetype_config` VALUES ('diskArray', '磁盘阵列', '磁盘阵列', 'other');
INSERT INTO `alert_devicetype_config` VALUES ('tapeLibrary', '磁带库', '磁带库', 'other');
INSERT INTO `alert_devicetype_config` VALUES ('cloudStorage', '云存储', '云存储设备', 'other');
INSERT INTO `alert_devicetype_config` VALUES ('SDNController', 'SDN控制器', 'SDN控制器', 'other');

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:07:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_distribution_sync
-- ----------------------------
DROP TABLE IF EXISTS `alert_distribution_sync`;
CREATE TABLE `alert_distribution_sync` (
  `idcType` varchar(128) DEFAULT NULL,
  `alertLevel` varchar(12) DEFAULT NULL,
  `month` varchar(24) DEFAULT NULL,
  `physicalMachineCount` bigint(128) DEFAULT NULL COMMENT '防火墙',
  `firewallCount` bigint(128) DEFAULT NULL,
  `routerCount` bigint(128) DEFAULT NULL,
  `switchCount` bigint(128) DEFAULT NULL,
  `diskArrayCount` bigint(128) DEFAULT NULL,
  `tapeLibraryCount` bigint(128) DEFAULT NULL,
  `cloudStorageCount` bigint(128) DEFAULT NULL,
  `SDNControllerCount` bigint(128) DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  KEY `index_r_alert_id` (`idcType`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警分布统计表';

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:08:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_filter
-- ----------------------------
DROP TABLE IF EXISTS `alert_filter`;
CREATE TABLE `alert_filter` (
  `id` tinyint(12) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `creater` varchar(64) DEFAULT NULL,
  `editer` varchar(64) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:09:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_filter_option
-- ----------------------------
DROP TABLE IF EXISTS `alert_filter_option`;
CREATE TABLE `alert_filter_option` (
  `id` tinyint(12) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL COMMENT '筛选条件名称',
  `type` varchar(64) NOT NULL COMMENT '展示类型select,string,datetime',
  `code` varchar(64) NOT NULL,
  `operate` varchar(255) NOT NULL COMMENT '操作：大于、小于、等于、大于等于、小于等于...分号分割',
  `source` varchar(255) DEFAULT NULL COMMENT '数据来源',
  `content` varchar(255) DEFAULT NULL COMMENT '配置数据',
  `method` varchar(64) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1:使用，0：停用',
  `jdbc_type` varchar(64) DEFAULT '' COMMENT '数据类型:string,number',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alert_filter_option
-- ----------------------------
INSERT INTO `alert_filter_option` VALUES ('1', '设备ip', 'string', 'device_ip', '模糊匹配,等于,不等于,包含', null, null, null, '1', 'string');
INSERT INTO `alert_filter_option` VALUES ('2', '告警级别', 'select', 'alert_level', '等于,不等于,包含,不包含', null, '[{\"name\": \"提示\", \"value\": \"1\"}, {\"name\": \"低\", \"value\": \"2\"}, {\"name\": \"中\", \"value\": \"3\"}, {\"name\": \"高\", \"value\": \"4\"}, {\"name\": \"严重\", \"value\": \"5\"}]', null, '1', 'string');
INSERT INTO `alert_filter_option` VALUES ('3', '监控对象', 'select', 'moni_object', '等于,不等于,包含,不包含', 'alerts/statistic/monit-obj-list', null, 'GET', '1', 'string');
INSERT INTO `alert_filter_option` VALUES ('4', '资源池', 'select', 'idc_type', '等于,不等于,包含,不包含', 'cmdb/configDict/getDictsByType?type=idcType', null, 'GET', '1', 'string');
INSERT INTO `alert_filter_option` VALUES ('5', '业务系统', 'select', 'biz_sys', '等于,不等于,包含,不包含', 'cmdb/getBizSysList', '', 'GET', '1', 'string');
INSERT INTO `alert_filter_option` VALUES ('6', '告警来源', 'select', 'source', '等于,不等于,包含,不包含', null, '[{\"value\":\"东软\", \"name\":\"东软\"}, {\"value\":\"ZABBIX\", \"name\":\"ZABBIX\"}, {\"value\":\"prometheus\", \"name\":\"prometheus\"}, {\"value\":\"内部告警\", \"name\":\"内部告警\"}]', null, '1', 'string');
INSERT INTO `alert_filter_option` VALUES ('7', '设备类型', 'select', 'device_class', '等于,不等于,包含,不包含', 'cmdb/configDict/getDictsByType?type=device_class', null, 'GET', '1', 'string');
INSERT INTO `alert_filter_option` VALUES ('8', '机房', 'select', 'source_room', '等于,不等于,包含,不包含', 'cmdb/configDict/getDictsByType?type=roomId', null, 'GET', '1', 'string');
INSERT INTO `alert_filter_option` VALUES ('9', '告警内容', 'string', 'moni_index', '模糊匹配,等于,不等于', null, null, null, '1', 'string');
INSERT INTO `alert_filter_option` VALUES ('10', '工单状态', 'select', 'order_status', '等于,不等于,包含,不包含', null, '[{\"name\": \"未派单\", \"value\": \"1\"}, {\"name\": \"处理中\", \"value\": \"2\"}, {\"name\": \"已完成\", \"value\": \"3\"}]', null, '1', 'string');
INSERT INTO `alert_filter_option` VALUES ('11', '派单类型', 'select', 'order_type', '等于,不等于,包含,不包含', null, '[{\"name\": \"告警工单\", \"value\": \"1\"}, {\"name\": \"故障工单\", \"value\": \"2\"}]', null, '1', 'string');

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:09:46
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_filter_scene
-- ----------------------------
DROP TABLE IF EXISTS `alert_filter_scene`;
CREATE TABLE `alert_filter_scene` (
  `id` tinyint(12) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL COMMENT '场景名称',
  `filter_id` tinyint(12) DEFAULT NULL COMMENT '过滤器id',
  `option_condition` varchar(4000) NOT NULL COMMENT '过滤条件',
  `operate_user` varchar(255) DEFAULT NULL COMMENT '维护用户',
  `creater` varchar(64) DEFAULT NULL,
  `editer` varchar(64) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8 COMMENT='告警过滤场景表';

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:10:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_mail_recipients
-- ----------------------------
DROP TABLE IF EXISTS `alert_mail_recipients`;
CREATE TABLE `alert_mail_recipients` (
  `id` int(32) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `recipient` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '收件邮箱',
  `password` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '收件邮箱密码',
  `mail_server` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '收件邮箱邮件服务器',
  `receive_protocal` tinyint(1) DEFAULT '0' COMMENT '收件协议, 0: pop3, 1: imap',
  `receive_port` int(8) NOT NULL COMMENT '收件邮箱邮件服务器收件端口',
  `status` tinyint(1) DEFAULT '0' COMMENT '是否启用, 0: 未启用, 1: 启用',
  `recipient_desc` varchar(128) COLLATE utf8_bin DEFAULT NULL COMMENT '收件邮箱描述',
  `period` int(11) DEFAULT NULL COMMENT '邮箱采集周期-单位:分钟',
  `period_unit` tinyint(1) NOT NULL DEFAULT '0' COMMENT '周期时间单位 0:分钟,1:小时,2:天',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='邮件采集账号表';

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:10:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_mail_resolve_filter
-- ----------------------------
DROP TABLE IF EXISTS `alert_mail_resolve_filter`;
CREATE TABLE `alert_mail_resolve_filter` (
  `id` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '主键ID',
  `receiver` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '收件邮箱',
  `sender` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '发件邮箱',
  `title_incl` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '邮件过滤:标题包含关键字',
  `content_incl` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '邮件过滤:内容包含关键字',
  `status` tinyint(1) DEFAULT '0' COMMENT '是否启用, 0: 未启用, 1: 启用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='告警采集配置表';

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:11:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_mail_resolve_record
-- ----------------------------
DROP TABLE IF EXISTS `alert_mail_resolve_record`;
CREATE TABLE `alert_mail_resolve_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `filter_id` varchar(50) NOT NULL COMMENT 'Filter主键ID',
  `mail_title` varchar(64) NOT NULL COMMENT '邮件标题',
  `mail_content` text COMMENT '邮件内容',
  `mail_sender` varchar(64) NOT NULL COMMENT '发件人',
  `mail_receiver` varchar(64) NOT NULL COMMENT '收件人',
  `mail_send_time` datetime NOT NULL COMMENT '发件时间',
  `resolve_time` datetime NOT NULL COMMENT '采集时间',
  `device_ip` varchar(64) NOT NULL COMMENT '设备IP',
  `moni_index` varchar(64) DEFAULT NULL COMMENT '告警描述/告警内容',
  `moni_object` varchar(128) DEFAULT NULL COMMENT '告警指标',
  `alert_level` varchar(20) NOT NULL COMMENT '告警级别\r\n1-提示\r\n2-低\r\n3-中\r\n4-高\r\n5-严重\r\n',
  `alert_id` varchar(64) NOT NULL COMMENT '告警编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='邮件告警采集记录表';

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:12:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_mail_resolve_strategy
-- ----------------------------
DROP TABLE IF EXISTS `alert_mail_resolve_strategy`;
CREATE TABLE `alert_mail_resolve_strategy` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `filter_id` varchar(50) COLLATE utf8_bin NOT NULL COMMENT 'Filter主键ID',
  `alert_field` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '匹配警报的字段名',
  `mail_field` tinyint(4) DEFAULT NULL COMMENT '邮件的字段名, 0: 标题, 1: 内容, 2:发件人, 3:发件时间',
  `field_match_val` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '配置字段的值',
  `use_reg` tinyint(1) DEFAULT '0' COMMENT '使用正则匹配, 0: 不使用, 1:使用',
  `field_match_reg` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '自定义正则表达式',
  `field_match_target` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '字段匹配后映射值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=255 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='告警采集自定义规则表';

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:12:46
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_mail_substance
-- ----------------------------
DROP TABLE IF EXISTS `alert_mail_substance`;
CREATE TABLE `alert_mail_substance` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `receiver` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '收件邮箱',
  `sender` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '发件邮箱',
  `send_time` datetime DEFAULT NULL COMMENT '发件时间',
  `uid` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '邮件唯一标识',
  `subject` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '邮件标题',
  `content` text COLLATE utf8_bin COMMENT '邮件内容',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1284 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='邮件读取记录表';

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:13:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_manual_statistic
-- ----------------------------
DROP TABLE IF EXISTS `alert_manual_statistic`;
CREATE TABLE `alert_manual_statistic` (
  `room_id` varchar(100) NOT NULL COMMENT '机房id',
  `source_alert_id` varchar(150) NOT NULL COMMENT '原始告警id',
  `auto_count` int(11) DEFAULT '0' COMMENT '自动清除次数',
  `manual_count` int(11) NOT NULL DEFAULT '0' COMMENT '手工清除次数',
  UNIQUE KEY `unique_idx_manual_statistic` (`room_id`,`source_alert_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:13:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_moniter_top_sync
-- ----------------------------
DROP TABLE IF EXISTS `alert_moniter_top_sync`;
CREATE TABLE `alert_moniter_top_sync` (
  `idcType` varchar(300) DEFAULT NULL,
  `alertLevel` varchar(12) DEFAULT NULL,
  `deviceType` varchar(10) DEFAULT NULL,
  `countOrder` int(1) DEFAULT NULL,
  `moniterObject` varchar(128) DEFAULT NULL COMMENT '监控对象',
  `moniterAlertCount` bigint(128) DEFAULT NULL COMMENT '告警数量',
  `rate` decimal(20,4) DEFAULT NULL COMMENT '告警占比',
  `month` varchar(24) DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  KEY `index_r_alert_id` (`idcType`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警监控对象top10统计表';

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:14:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_notify_config
-- ----------------------------
DROP TABLE IF EXISTS `alert_notify_config`;
CREATE TABLE `alert_notify_config` (
  `uuid` varchar(255) NOT NULL COMMENT 'uuid',
  `name` varchar(255) DEFAULT NULL COMMENT '策略配置名称',
  `is_open` int(10) DEFAULT NULL COMMENT '是否启用 0-关闭(默认) 1-启用',
  `alert_filter_id` int(255) DEFAULT NULL COMMENT '告警筛选器id',
  `alert_filter_scene_id` int(255) DEFAULT NULL COMMENT '告警筛选场景id',
  `notify_alert_type` varchar(255) DEFAULT NULL COMMENT '告警通知类型(0-转派, 1-确认,2-派发工单, 3-清除, 4-通知, 5-过滤, 6-工程, 7-维护模式)',
  `notify_type` varchar(255) DEFAULT NULL COMMENT '通知类型 0-邮件/短信 1-邮件 2-短信',
  `is_recurrence_interval` int(10) DEFAULT NULL COMMENT '是否重发  0-关闭(默认) 1-启用',
  `recurrence_interval` varchar(255) DEFAULT NULL COMMENT '重发间隔时间',
  `recurrence_interval_util` varchar(255) DEFAULT NULL COMMENT '重发间隔单位 ',
  `resend_time` varchar(255) DEFAULT NULL COMMENT '重发时间',
  `email_type` int(10) DEFAULT NULL COMMENT '邮件发送类型 1-合并 2-单条',
  `send_operation` varchar(10) DEFAULT NULL COMMENT '发送记录',
  `cur_send_time` datetime DEFAULT NULL COMMENT '最新发送时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `creator` varchar(255) DEFAULT NULL COMMENT '创建人',
  `is_first_send` varchar(2) DEFAULT NULL COMMENT '是否发送过 1-已发送',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警通知策略配置表';

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:14:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_notify_config_log
-- ----------------------------
DROP TABLE IF EXISTS `alert_notify_config_log`;
CREATE TABLE `alert_notify_config_log` (
  `alert_notify_config_id` text COMMENT '告警配置id',
  `send_status` varchar(255) DEFAULT NULL COMMENT '发送状态 0-失败 1-成功',
  `receiver` varchar(255) DEFAULT NULL COMMENT '接收人',
  `send_time` datetime DEFAULT NULL COMMENT '发送时间',
  `send_content` text COMMENT '发送内容',
  `send_alert_id` text COMMENT '发送的告警id',
  `send_ype` varchar(2) DEFAULT NULL COMMENT '发送类型 1-短信 2-邮件(单发) 3-邮件(合并)',
  `resend_time` varchar(255) DEFAULT NULL COMMENT '重发时间',
  `is_resend` varchar(2) DEFAULT NULL COMMENT '是否重发 0-否 1-是'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警通知配置发送记录表';

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:15:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_notify_config_receiver
-- ----------------------------
DROP TABLE IF EXISTS `alert_notify_config_receiver`;
CREATE TABLE `alert_notify_config_receiver` (
  `uuid` varchar(255) NOT NULL COMMENT 'uuid',
  `alert_notify_config_id` varchar(255) DEFAULT NULL COMMENT '告警通知策略配置表uuid',
  `notify_obj_type` varchar(255) DEFAULT NULL COMMENT '通知对象类型 1-团队 2-个人',
  `notify_obj_info` varchar(255) DEFAULT NULL COMMENT '通知对象信息',
  `notify_type` varchar(10) DEFAULT NULL COMMENT '通知类型 0-邮件/短信 1-邮件 2-短信',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警通知策略配置人员表';

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:15:31
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_notify_config_rule
-- ----------------------------
DROP TABLE IF EXISTS `alert_notify_config_rule`;
CREATE TABLE `alert_notify_config_rule` (
  `id` varchar(10) NOT NULL COMMENT 'id',
  `rule_name` varchar(255) DEFAULT NULL COMMENT '定时任务规则名称',
  `rule_cron` varchar(255) DEFAULT NULL COMMENT 'cron',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alert_notify_config_rule
-- ----------------------------
INSERT INTO `alert_notify_config_rule` VALUES ('1', 'send', '0 0/1 * * * ? ');
INSERT INTO `alert_notify_config_rule` VALUES ('2', 'resend', '0 0/1 * * * ? ');

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:15:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_operation_record
-- ----------------------------
DROP TABLE IF EXISTS `alert_operation_record`;
CREATE TABLE `alert_operation_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `alert_id` varchar(200) NOT NULL COMMENT '告警id',
  `user_id` varchar(32) DEFAULT NULL COMMENT '操作人',
  `user_name` varchar(32) DEFAULT NULL COMMENT '操作人名称',
  `operation_type` tinyint(1) NOT NULL COMMENT '操作类型 0-转派, 1-确认,2-派发工单, 3-清除, 4-通知, 5-过滤, 6-工程, 7-维护模式',
  `operation_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  `operation_status` tinyint(1) DEFAULT '1' COMMENT '操作状态 0-失败 1-成功,',
  `content` varchar(256) DEFAULT NULL COMMENT '操作内容',
  PRIMARY KEY (`id`),
  KEY `index_alert_id` (`alert_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=506 DEFAULT CHARSET=utf8;

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:16:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_proxy_idc
-- ----------------------------
DROP TABLE IF EXISTS `alert_proxy_idc`;
CREATE TABLE `alert_proxy_idc` (
  `id` bigint(11) NOT NULL,
  `proxy_name` varchar(64) DEFAULT NULL,
  `idc` varchar(64) DEFAULT NULL,
  `remark` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警代理名称和资源池对应表';

-- ----------------------------
-- Records of alert_proxy_idc
-- ----------------------------
INSERT INTO `alert_proxy_idc` VALUES ('1', 'xxg_proxy245', 'XXGZYC', '信息港资源池');
INSERT INTO `alert_proxy_idc` VALUES ('2', 'xxg_proxy76', 'XXGZYC', '信息港资源池');
INSERT INTO `alert_proxy_idc` VALUES ('3', 'xxg_proxy77', 'XXGZYC', '信息港资源池');
INSERT INTO `alert_proxy_idc` VALUES ('4', 'xxg_proxy78', 'XXGZYC', '信息港资源池');
INSERT INTO `alert_proxy_idc` VALUES ('5', 'xxg_proxy79', 'XXGZYC', '信息港资源池');
INSERT INTO `alert_proxy_idc` VALUES ('6', 'xxg_proxy80', 'XXGZYC', '信息港资源池');
INSERT INTO `alert_proxy_idc` VALUES ('7', 'ccjk5_proxy', 'YZYFCH', '业支域非池化');
INSERT INTO `alert_proxy_idc` VALUES ('8', 'hrb_proxy80', 'HEBZYC', '哈尔滨资源池');
INSERT INTO `alert_proxy_idc` VALUES ('9', 'hrb_proxy81', 'HEBZYC', '哈尔滨资源池');
INSERT INTO `alert_proxy_idc` VALUES ('10', 'Hohhot_ZAproxy1', 'HHHTZYC', '呼和浩特资源池');
INSERT INTO `alert_proxy_idc` VALUES ('11', 'xxg', 'XXGZYC', '信息港资源池');
INSERT INTO `alert_proxy_idc` VALUES ('12', 'fch', 'YZYFCH', '业支域非池化');
INSERT INTO `alert_proxy_idc` VALUES ('13', 'hac', 'HEBZYC', '哈尔滨资源池');
INSERT INTO `alert_proxy_idc` VALUES ('14', 'huc', 'HHHTZYC', '呼和浩特资源池');

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:16:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_rep_panel
-- ----------------------------
DROP TABLE IF EXISTS `alert_rep_panel`;
CREATE TABLE `alert_rep_panel` (
  `id` int(128) NOT NULL AUTO_INCREMENT,
  `panel_name` varchar(128) DEFAULT NULL,
  `show_item` varchar(20000) DEFAULT NULL COMMENT '布局信息',
  `editer` varchar(24) DEFAULT NULL,
  `creater` varchar(24) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_r_alert_id` (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COMMENT='配置项信息表';

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:16:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_rep_panel_item
-- ----------------------------
DROP TABLE IF EXISTS `alert_rep_panel_item`;
CREATE TABLE `alert_rep_panel_item` (
  `id` int(128) NOT NULL AUTO_INCREMENT,
  `panel_id` varchar(24) DEFAULT NULL COMMENT '关联的大屏id',
  `item_id` varchar(24) DEFAULT NULL COMMENT '布局的id',
  `report_type` varchar(10) DEFAULT NULL COMMENT '报表类型',
  `report_name` varchar(128) DEFAULT NULL COMMENT '名称',
  `time_value` int(128) DEFAULT NULL COMMENT '时间刻度值',
  `time_unit` varchar(8) DEFAULT NULL COMMENT '横轴时间单位',
  `report_unit` varchar(24) DEFAULT NULL COMMENT '单位',
  `conversion_type` tinyint(1) DEFAULT NULL COMMENT '换算：1相乘2相除',
  `conversion_val` int(128) DEFAULT NULL COMMENT '换算数值',
  PRIMARY KEY (`id`),
  KEY `index_r_alert_id` (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=405 DEFAULT CHARSET=utf8 COMMENT='大屏单个报表配置';

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:17:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_rep_panel_moniteritem
-- ----------------------------
DROP TABLE IF EXISTS `alert_rep_panel_moniteritem`;
CREATE TABLE `alert_rep_panel_moniteritem` (
  `id` int(128) NOT NULL AUTO_INCREMENT,
  `item_id` varchar(24) DEFAULT NULL COMMENT '关联大屏配置项id',
  `resource_device` varchar(4000) DEFAULT NULL COMMENT '资源设备',
  `moniter_item` varchar(256) DEFAULT NULL COMMENT '监控指标',
  `view_name` varchar(128) DEFAULT NULL COMMENT '显示名称',
  `count_type` varchar(24) DEFAULT NULL COMMENT '统计方式：1最大2最小3平均',
  `resource_type` tinyint(1) DEFAULT NULL COMMENT '资源类型：1业务系统2资源池3机房4设备大类5设备小类6设备ip',
  `unit` varchar(24) DEFAULT NULL COMMENT '监控项单位',
  `mointer_index` varchar(24) DEFAULT NULL COMMENT '所属es索引',
  `resource_device_ipStr` varchar(8000) DEFAULT NULL COMMENT '资源设备ip',
  PRIMARY KEY (`id`),
  KEY `index_r_alert_id` (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=521 DEFAULT CHARSET=utf8 COMMENT='大屏单个报表配置';

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:17:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_report_operate_record
-- ----------------------------
DROP TABLE IF EXISTS `alert_report_operate_record`;
CREATE TABLE `alert_report_operate_record` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ALERT_ID` varchar(200) DEFAULT NULL COMMENT '警报ID',
  `USER_ID` varchar(32) DEFAULT NULL COMMENT '发件人ID',
  `USER_NAME` varchar(32) DEFAULT NULL COMMENT '发件人姓名',
  `REPORT_TYPE` tinyint(1) DEFAULT '0' COMMENT '通知类型, 0:短信,1:邮件',
  `USER_EMAIL` varchar(32) DEFAULT NULL COMMENT '发件人邮箱',
  `DESTINATION` varchar(64) DEFAULT NULL COMMENT '短信/邮件 地址',
  `MESSAGE` text COMMENT '短信/邮件 内容',
  `STATUS` tinyint(1) DEFAULT '1' COMMENT '发送状态,0:失败,1:成功',
  `CREATE_TIME` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  KEY `index_alert_id` (`ALERT_ID`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=192 DEFAULT CHARSET=utf8 COMMENT='告警通知操作记录表';

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:18:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_root_cause
-- ----------------------------
DROP TABLE IF EXISTS `alert_root_cause`;
CREATE TABLE `alert_root_cause` (
  `time1` varchar(255) DEFAULT NULL,
  `time2` varchar(255) DEFAULT NULL,
  `cur_alert_id` varchar(255) NOT NULL,
  `cur_item_id` varchar(255) NOT NULL,
  `cur_moni_time` varchar(255) NOT NULL,
  `his_alert_item_id` varchar(255) DEFAULT NULL,
  `similarity` varchar(255) DEFAULT NULL,
  `root_cause_alert_id` varchar(255) DEFAULT NULL,
  `root_cause_start_time` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:18:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_snmp_event_his
-- ----------------------------
DROP TABLE IF EXISTS `alert_snmp_event_his`;
CREATE TABLE `alert_snmp_event_his` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alert_id` varchar(150) NOT NULL COMMENT '告警id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `alert_status` varchar(50) DEFAULT NULL COMMENT '告警状态 1是实时告警，2是消警',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1993 DEFAULT CHARSET=utf8;

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:19:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_snmp_node
-- ----------------------------
DROP TABLE IF EXISTS `alert_snmp_node`;
CREATE TABLE `alert_snmp_node` (
  `node_misc_id` varchar(300) DEFAULT NULL COMMENT 'OID的值',
  `node_misc_name` varchar(300) DEFAULT NULL COMMENT 'OID对应的描述',
  `snmp_status` varchar(100) DEFAULT NULL COMMENT '预留字段'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alert_snmp_node
-- ----------------------------
INSERT INTO `alert_snmp_node` VALUES ('1.3.6.1.6.3.1.1.4.1.1', 'ALERT_ID', 'Y');
INSERT INTO `alert_snmp_node` VALUES ('1.3.6.1.6.3.1.1.4.1.2', 'NE_CODE', 'Y');
INSERT INTO `alert_snmp_node` VALUES ('1.3.6.1.6.3.1.1.4.1.3', 'BUSINESS_SYSTEM', 'Y');
INSERT INTO `alert_snmp_node` VALUES ('1.3.6.1.6.3.1.1.4.1.4', 'MONI_OBJECT', 'Y');
INSERT INTO `alert_snmp_node` VALUES ('1.3.6.1.6.3.1.1.4.1.5', 'MONI_INDEX', 'Y');
INSERT INTO `alert_snmp_node` VALUES ('1.3.6.1.6.3.1.1.4.1.6', 'ALERT_LEVEL', 'Y');
INSERT INTO `alert_snmp_node` VALUES ('1.3.6.1.6.3.1.1.4.1.7', 'MONI_TYPE', 'Y');
INSERT INTO `alert_snmp_node` VALUES ('1.3.6.1.6.3.1.1.4.1.8', 'ALERT_START_TIME', 'Y');
INSERT INTO `alert_snmp_node` VALUES ('1.3.6.1.6.3.1.1.4.1.9', 'REMOVED_DATE', 'Y');
INSERT INTO `alert_snmp_node` VALUES ('1.3.6.1.6.3.1.1.4.1.10', 'EVENT_STATUS', 'Y');
INSERT INTO `alert_snmp_node` VALUES ('1.3.6.1.6.3.1.1.4.1.11', 'TITLE', 'Y');
INSERT INTO `alert_snmp_node` VALUES ('1.3.6.1.6.3.1.1.4.1.12', 'ALERT_DESC', 'Y');
INSERT INTO `alert_snmp_node` VALUES ('1.3.6.1.6.3.1.1.4.1.13', 'DEVICE', 'Y');
INSERT INTO `alert_snmp_node` VALUES ('1.3.6.1.6.3.1.1.4.1.14', 'CUR_MONI_VALUE', 'Y');
INSERT INTO `alert_snmp_node` VALUES ('1.3.6.1.6.3.1.1.4.1.15', 'PROVINCE_NAME', 'Y');
INSERT INTO `alert_snmp_node` VALUES ('1.3.6.1.6.3.1.1.4.1.16', 'CITY_NAME', 'Y');
INSERT INTO `alert_snmp_node` VALUES ('1.3.6.1.6.3.1.1.4.1.17', 'FACTOR_NAME', 'Y');
INSERT INTO `alert_snmp_node` VALUES ('1.3.6.1.6.3.1.1.4.1.18', 'NEW1', 'Y');

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:20:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_transfer
-- ----------------------------
DROP TABLE IF EXISTS `alert_transfer`;
CREATE TABLE `alert_transfer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `alert_id` varchar(200) NOT NULL COMMENT '告警id',
  `user_id` varchar(32) DEFAULT NULL COMMENT '操作人id',
  `user_name` varchar(32) DEFAULT NULL COMMENT '操作人名称',
  `confirm_user_id` varchar(32) DEFAULT NULL COMMENT '待确认人id',
  `confirm_user_name` varchar(32) DEFAULT NULL COMMENT '待确认人name',
  `operation_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2136 DEFAULT CHARSET=utf8;

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:20:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_work_config
-- ----------------------------
DROP TABLE IF EXISTS `alert_work_config`;
CREATE TABLE `alert_work_config` (
  `uuid` varchar(255) NOT NULL COMMENT 'uuid',
  `day_start_time` varchar(255) DEFAULT NULL COMMENT '白班 开始时间',
  `day_end_time` varchar(255) DEFAULT NULL COMMENT '白班 结束时间内',
  `night_start_time` varchar(255) DEFAULT NULL COMMENT '夜班 开始时间',
  `night_end_time` varchar(255) DEFAULT NULL COMMENT '夜班 结束时间',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='值班人员配置表';

-- ----------------------------
-- Records of alert_work_config
-- ----------------------------
INSERT INTO `alert_work_config` VALUES ('05757c87-9a7b-4a37-8e85-8131824895d1', '09:00', '18:00', '18:00', '09:00');

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:23:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for auto_alert_record
-- ----------------------------
DROP TABLE IF EXISTS `auto_alert_record`;
CREATE TABLE `auto_alert_record` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ALARM_ID` varchar(64) DEFAULT NULL,
  `FLAG` varchar(32) DEFAULT NULL,
  `MSG` varchar(64) DEFAULT NULL,
  `STATUS` char(1) DEFAULT NULL COMMENT '状态为0 表示执行中，为1表示 执行完成',
  `TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ALARM_ID_INDEX` (`ALARM_ID`) USING BTREE,
  KEY `MSG_INDEX` (`MSG`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=603338 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:43:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for security_leak_scan_records
-- ----------------------------
DROP TABLE IF EXISTS `security_leak_scan_records`;
CREATE TABLE `security_leak_scan_records` (
  `id` varchar(50) NOT NULL COMMENT '主键ID',
  `biz_line` varchar(64) NOT NULL COMMENT '归属业务线',
  `report_file_name` varchar(64) NOT NULL COMMENT '报告文件名',
  `report_file_url` varchar(128) NOT NULL COMMENT '服务端-报告文件下载地址',
  `scan_date` date NOT NULL COMMENT '扫描日期',
  `file_id` varchar(50) DEFAULT NULL COMMENT '工单上传文件ID',
  `bpm_id` varchar(50) DEFAULT NULL COMMENT '工单流程ID',
  `repair_stat` tinyint(1) DEFAULT '0' COMMENT '修复状态: 0 未修复，1 已修复',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='安全漏洞扫描记录表';

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:43:48
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for security_leak_scan_report_files
-- ----------------------------
DROP TABLE IF EXISTS `security_leak_scan_report_files`;
CREATE TABLE `security_leak_scan_report_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `scan_id` varchar(50) NOT NULL COMMENT '扫描记录主键ID',
  `file_name` varchar(64) NOT NULL COMMENT '报告压缩包文件名',
  `ftp_path` varchar(120) NOT NULL COMMENT 'FTP-报告压缩包文件下载地址',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COMMENT='安全漏洞扫描记录文件解析表';

/*
Navicat MySQL Data Transfer

Source Server         : 10.12.70.40
Source Server Version : 50726
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-08-05 17:44:31
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for security_leak_scan_reports
-- ----------------------------
DROP TABLE IF EXISTS `security_leak_scan_reports`;
CREATE TABLE `security_leak_scan_reports` (
  `id` varchar(50) NOT NULL COMMENT '主键ID',
  `scan_id` varchar(50) NOT NULL COMMENT '扫描记录主键ID',
  `ip` varchar(64) NOT NULL COMMENT 'IP 地址',
  `report_path` varchar(128) DEFAULT NULL COMMENT '报告路径',
  `high_leaks` int(11) DEFAULT '0' COMMENT '高危漏洞数量',
  `medium_leaks` int(11) DEFAULT '0' COMMENT '中危漏洞数量',
  `low_leaks` int(11) DEFAULT '0' COMMENT '低微漏洞数量',
  `risk_val` float DEFAULT '0' COMMENT '风险值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='安全漏洞扫描记录表';

/*
Navicat MySQL Data Transfer

Source Server         : bpm
Source Server Version : 50730
Source Host           : 10.12.70.40:3306
Source Database       : mirror

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2020-06-29 09:15:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alert_standard
-- ----------------------------
DROP TABLE IF EXISTS `alert_standard`;
CREATE TABLE `alert_standard` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_class` varchar(20) DEFAULT NULL COMMENT '设备分类',
  `device_type` varchar(20) DEFAULT NULL COMMENT '设备类型',
  `monitor_key` varchar(40) DEFAULT NULL COMMENT '监控指标key',
  `standard_name` varchar(40) DEFAULT NULL COMMENT '标准名称',
  `alert_desc` varchar(100) DEFAULT NULL COMMENT '告警描述',
  `status` varchar(2) DEFAULT NULL COMMENT '状态 ( 1 启用、0 禁用)',
  `alert_level` varchar(20) DEFAULT NULL COMMENT '告警等级 (低、中、高、重大)',
  `insert_person` varchar(20) DEFAULT NULL COMMENT '创建人员',
  `insert_time` datetime DEFAULT NULL COMMENT '创建时间',
  `is_delete` varchar(2) DEFAULT '0' COMMENT '是否删除（0 否 | 1 是）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;



