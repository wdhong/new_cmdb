/*
Navicat MySQL Data Transfer

Source Server         : 现网-ums
Source Server Version : 50718
Source Host           : localhost:9999
Source Database       : resource

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2019-12-02 15:58:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `uuid` char(36) NOT NULL COMMENT 'id',
  `parent_id` char(36) DEFAULT NULL COMMENT '父节点id',
  `name` char(100) DEFAULT NULL COMMENT '部门名称',
  `no` varchar(20) DEFAULT NULL COMMENT '部门编号',
  `dept_type` tinyint(4) DEFAULT NULL COMMENT '部门类型',
  `descr` varchar(100) DEFAULT NULL COMMENT '部门描述',
  `level` tinyint(4) DEFAULT NULL COMMENT '部门层级',
  `namespace` char(100) DEFAULT NULL COMMENT '空间',
  PRIMARY KEY (`uuid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for department_user
-- ----------------------------
DROP TABLE IF EXISTS `department_user`;
CREATE TABLE `department_user` (
  `dept_id` char(36) COLLATE utf8_bin NOT NULL COMMENT '部门ID',
  `user_id` char(36) COLLATE utf8_bin NOT NULL COMMENT '用户ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `uuid` char(36) COLLATE utf8_bin NOT NULL COMMENT '权限UUID主键',
  `role_uuid` char(36) COLLATE utf8_bin NOT NULL COMMENT '角色UUID主键',
  `actions` longtext COLLATE utf8_bin NOT NULL COMMENT '资源操作',
  `resources` longtext COLLATE utf8_bin NOT NULL COMMENT '资源名',
  `constraints` longtext COLLATE utf8_bin NOT NULL COMMENT '资源约束',
  PRIMARY KEY (`uuid`) USING BTREE,
  KEY `index_permissions_role_uuid` (`role_uuid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for resource_schema
-- ----------------------------
DROP TABLE IF EXISTS `resource_schema`;
CREATE TABLE `resource_schema` (
  `resource` varchar(50) NOT NULL COMMENT '资源类型',
  `general` varchar(5) DEFAULT NULL COMMENT '是否全局',
  `created_at` datetime(6) NOT NULL COMMENT '创建时间',
  `name` varchar(100) DEFAULT NULL COMMENT '名称',
  `parent_resource` varchar(50) DEFAULT NULL COMMENT '父节点resource',
  PRIMARY KEY (`resource`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for resource_schema_actions
-- ----------------------------
DROP TABLE IF EXISTS `resource_schema_actions`;
CREATE TABLE `resource_schema_actions` (
  `resource` varchar(50) NOT NULL COMMENT '资源类型',
  `action` varchar(100) NOT NULL COMMENT '资源操作',
  `action_name` varchar(100) DEFAULT NULL COMMENT '操作名称',
  `action_type` tinyint(1) DEFAULT NULL COMMENT '操作类型',
  KEY `index_actions_resource` (`resource`) USING BTREE,
  CONSTRAINT `resource_schema_actions_ibfk_1` FOREIGN KEY (`resource`) REFERENCES `resource_schema` (`resource`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `uuid` char(36) COLLATE utf8_bin NOT NULL COMMENT '角色UUID主键',
  `name` char(100) COLLATE utf8_bin NOT NULL COMMENT '角色名称',
  `role_type` tinyint(1) NOT NULL COMMENT '角色类型',
  `namespace` char(100) COLLATE utf8_bin NOT NULL COMMENT '空间名',
  `admin_role` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否为管理员角色',
  `descr` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '角色描述',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`uuid`) USING BTREE,
  KEY `index_role_name` (`name`) USING BTREE,
  KEY `index_role_namespace` (`namespace`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for role_parents
-- ----------------------------
DROP TABLE IF EXISTS `role_parents`;
CREATE TABLE `role_parents` (
  `role_uuid` char(36) COLLATE utf8_bin NOT NULL COMMENT '角色UUID主键',
  `parent_uuid` char(36) COLLATE utf8_bin NOT NULL COMMENT '父角色UUID主键',
  `assigned_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '分配时间',
  KEY `index_parents_role_uuid` (`role_uuid`) USING BTREE,
  KEY `index_parents_parent_uuid` (`parent_uuid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for roles_user
-- ----------------------------
DROP TABLE IF EXISTS `roles_user`;
CREATE TABLE `roles_user` (
  `role_uuid` char(36) COLLATE utf8_bin NOT NULL COMMENT '角色UUID主键',
  `namespace` char(100) COLLATE utf8_bin NOT NULL COMMENT '空间名',
  `username` char(100) COLLATE utf8_bin NOT NULL COMMENT '用户名',
  `assigned_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '分配时间',
  KEY `index_users_role_uuid` (`role_uuid`) USING BTREE,
  KEY `index_users_username` (`username`) USING BTREE,
  KEY `index_users_namespace` (`namespace`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `uuid` char(36) NOT NULL COMMENT '主键',
  `name` char(100) DEFAULT NULL COMMENT '姓名',
  `user_type` tinyint(4) DEFAULT NULL COMMENT '用户类型',
  `dept_Id` char(36) DEFAULT NULL COMMENT '所属部门',
  `no` varchar(20) DEFAULT NULL COMMENT '人员编号',
  `sex` tinyint(4) DEFAULT NULL COMMENT '性别',
  `mail` varchar(200) DEFAULT NULL COMMENT '邮箱',
  `address` varchar(500) DEFAULT NULL COMMENT '地址',
  `phone` varchar(20) DEFAULT NULL COMMENT '电话号码',
  `mobile` varchar(20) DEFAULT NULL COMMENT '移动号码',
  `ldapId` char(36) DEFAULT NULL COMMENT '账号id',
  `namespace` char(100) DEFAULT NULL COMMENT '空间',
  `code` varchar(50) DEFAULT NULL COMMENT '人员代码',
  `fax` varchar(50) DEFAULT NULL COMMENT '传真',
  `post` varchar(50) DEFAULT NULL COMMENT '职责',
  `relation_person` char(36) DEFAULT NULL COMMENT '关联人员id',
  `descr` varchar(255) DEFAULT NULL COMMENT '描述',
  `ldap_status` tinyint(1) DEFAULT NULL COMMENT '账号状态',
  `ldap_password_updatetime` datetime DEFAULT NULL COMMENT '密码更新时间',
  `ldap_lock_times` int(11) unsigned zerofill DEFAULT NULL COMMENT '锁定次数',
  `picture` longtext,
  PRIMARY KEY (`uuid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for sys_manage
-- ----------------------------
DROP TABLE IF EXISTS `sys_manage`;
CREATE TABLE `sys_manage` (
  `id` varchar(64) NOT NULL COMMENT '主键id，存uuid',
  `name` varchar(128) NOT NULL COMMENT '系统名称',
  `manage_code` varchar(128) NOT NULL COMMENT '业务系统编码',
  `creater` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL,
  `del_status` varchar(2) DEFAULT '1' COMMENT '是否删除，1-未删除，0-已删除',
  PRIMARY KEY (`id`),
  UNIQUE KEY `i_name` (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单对应系统名称管理';

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` varchar(64) NOT NULL,
  `parent_id` varchar(64) NOT NULL DEFAULT '-1' COMMENT '父id',
  `name` varchar(128) NOT NULL COMMENT '菜单名称',
  `icon` varchar(128) DEFAULT NULL COMMENT '图标',
  `menu_type` varchar(32) NOT NULL COMMENT '菜单类型，children-父菜单，routers-菜单，vue-vue页面，page-外部打开，iframe-内部打开',
  `base` varchar(128) DEFAULT NULL COMMENT 'vue路由地址',
  `path` varchar(1024) DEFAULT NULL COMMENT 'vue页面地址',
  `component` varchar(128) DEFAULT NULL COMMENT 'vue页面文件名',
  `url` varchar(1024) DEFAULT NULL COMMENT '外部页面地址',
  `sort` int(11) NOT NULL COMMENT '菜单排序',
  `is_show` char(1) NOT NULL DEFAULT '1' COMMENT '是否在菜单中显示',
  `system_id` varchar(64) NOT NULL COMMENT '所属系统ID',
  `del_status` varchar(2) DEFAULT '1' COMMENT '是否删除，1-未删除，0-已删除',
  `creater` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL,
  `editer` varchar(64) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `i_parent_id` (`parent_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='动态菜单表';

-- ----------------------------
-- Table structure for resources_resource
-- ----------------------------
DROP TABLE IF EXISTS `resources_resource`;
CREATE TABLE `resources_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) DEFAULT NULL,
  `uuid` varchar(36) NOT NULL,
  `namespace` varchar(36) NOT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  `region_id` varchar(40) DEFAULT NULL,
  `space_uuid` varchar(40) NOT NULL,
  `project_uuid` varchar(36) NOT NULL,
  `knamespace_uuid` char(36) DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  UNIQUE KEY `resources_resource_name_3c5f024138507265_uniq` (`name`,`type`,`namespace`,`space_uuid`,`region_id`),
  KEY `resources_resource_namespace_4391c881a574ddc9_idx` (`namespace`,`uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=101662 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for code_dict
-- ----------------------------
DROP TABLE IF EXISTS `code_dict`;
CREATE TABLE `code_dict` (
  `id` int(32) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `code_type` varchar(50) NOT NULL COMMENT '编码类型',
  `key` varchar(50) NOT NULL COMMENT '编码key',
  `value` varchar(100) NOT NULL COMMENT '编码值',
  `code_desc` varchar(255) DEFAULT NULL COMMENT '描述',
  `valid_flag` varchar(1) NOT NULL COMMENT '是否有效or可见，1=是；0=否',
  `order_id` int(11) DEFAULT NULL COMMENT '排序（从1开始的正整数，NULL表示不排序，各level互不影响）',
  `parent_id` int(32) DEFAULT NULL COMMENT '上级ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='编码字典';

-- ----------------------------
-- Table structure for resource_schema_constraints
-- ----------------------------
DROP TABLE IF EXISTS `resource_schema_constraints`;
CREATE TABLE `resource_schema_constraints` (
  `resource` varchar(50) NOT NULL COMMENT '资源类型',
  `const_key` varchar(100) NOT NULL COMMENT '资源约束名',
  `const_value` varchar(100) NOT NULL COMMENT '资源约束值',
  KEY `index_constraints_resource` (`resource`) USING BTREE,
  CONSTRAINT `resource_schema_constraints_ibfk_1` FOREIGN KEY (`resource`) REFERENCES `resource_schema` (`resource`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
