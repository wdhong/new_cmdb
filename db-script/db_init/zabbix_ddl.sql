DROP TABLE IF EXISTS `alert_scan_index`;
CREATE TABLE `alert_scan_index` (
`id`  int(11) NULL DEFAULT NULL ,
`scan_index`  int(11) NULL DEFAULT NULL COMMENT '扫描索引' ,
`update_time`  timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间' 
);

-- ----------------------------
-- Table structure for cmdb_instance
-- ----------------------------
DROP TABLE IF EXISTS `cmdb_instance`;
CREATE TABLE `cmdb_instance` (
  `instance_id` varchar(64) DEFAULT NULL COMMENT '设备id',
  `ip` varchar(40) DEFAULT NULL COMMENT '设备ip',
  `idcType` varchar(40) DEFAULT NULL COMMENT '资源池',
  `idcType_name` varchar(100) DEFAULT NULL COMMENT '资源池中文',
  `bizSystem` varchar(100) DEFAULT NULL COMMENT '业务系统',
  `bizSystem_name` varchar(100) DEFAULT NULL COMMENT '业务系统中文',
  `device_class` varchar(40) DEFAULT NULL COMMENT '设备分类',
  `device_class_name` varchar(100) DEFAULT NULL COMMENT '设备分类中文',
  `roomId` varchar(40) DEFAULT NULL COMMENT '机房位置',
  `roomId_name` varchar(100) DEFAULT NULL COMMENT '机房中文',
  `device_type` varchar(40) DEFAULT NULL COMMENT '设备类型',
  `device_type_name` varchar(100) DEFAULT NULL COMMENT '设备类型中文',
  `device_mfrs` varchar(40) DEFAULT NULL COMMENT '设备厂商',
  `host_name` varchar(300) DEFAULT NULL COMMENT '主机名',
  `department1` varchar(40) DEFAULT NULL COMMENT '一级部门',
  `department1_name` varchar(100) DEFAULT NULL COMMENT '一级部门中文',
  `department2` varchar(40) DEFAULT NULL COMMENT '二级部门',
  `department2_name` varchar(100) DEFAULT NULL COMMENT '二级部门中文',
  `device_model` varchar(40) DEFAULT NULL COMMENT '设备型号',
  `pod_name` varchar(40) DEFAULT NULL COMMENT 'Pod池',
  `pod_name_name` varchar(100) DEFAULT NULL COMMENT 'pod名称中文',
  `project_name` varchar(40) DEFAULT NULL COMMENT '工程期数',
  `project_name_name` varchar(100) DEFAULT NULL COMMENT '项目名称中文',
  `insert_time` varchar(40) DEFAULT NULL COMMENT '录入时间',
  `device_class_3` varchar(40) DEFAULT NULL COMMENT '设备三级分类',
  `device_name` varchar(225) DEFAULT NULL COMMENT '设备名称',
  `node_type` varchar(40) DEFAULT NULL,
  `node_type_name` varchar(100) DEFAULT NULL COMMENT '节点类型中文',
  `suyan_uuid` varchar(255) DEFAULT NULL,
  UNIQUE KEY `idx_id_ip` (`instance_id`,`ip`) USING BTREE,
  KEY `idcTpe_ip` (`ip`,`idcType`) USING BTREE,
  KEY `suyanUuid`(`suyan_uuid`)  USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='cmdb资产信息表';