-- ----------------------------
-- Table structure for history_pipeline
-- ----------------------------
DROP TABLE IF EXISTS `cd_history_pipeline`;
CREATE TABLE `cd_history_pipeline` (
  `uuid` varchar(36) COLLATE utf8_bin NOT NULL comment'主键',
  `pipeline_uuid` varchar(36) COLLATE utf8_bin NOT NULL comment'流水线 uuid',
  `description` varchar(200) COLLATE utf8_bin DEFAULT NULL comment'描述',
  `trigger` varchar(30) COLLATE utf8_bin DEFAULT NULL comment'触发器类型: build/repository',
  `status` varchar(30) COLLATE utf8_bin NOT NULL comment'执行状态: pending --> deploying --> ongoing --> faild/completed',
  `created_by` varchar(30) COLLATE utf8_bin DEFAULT NULL comment'创建者名称',
  `created_by_token` varchar(50) COLLATE utf8_bin DEFAULT NULL comment'创建者 token',
  `started_at` timestamp NULL DEFAULT NULL comment'启动时间',
  `ended_at` timestamp NULL DEFAULT NULL comment'结束时间(status=faild/complete 时)',
  `created_at` timestamp NOT NULL DEFAULT now() comment'创建时间',
  `updated_at` timestamp NOT NULL DEFAULT now() comment'更改时间',
  `artifact_enabled` int(11) NOT NULL DEFAULT 0 comment'是否支持产出物: 0不支持; 1支持',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin comment'流水线的历史记录';

-- ----------------------------
-- Table structure for history_pipeline_data
-- ----------------------------
DROP TABLE IF EXISTS `cd_history_pipeline_data`;
CREATE TABLE `cd_history_pipeline_data` (
  `history_uuid` varchar(36) COLLATE utf8_bin NOT NULL comment'主键',
  `key` varchar(256) COLLATE utf8_bin NOT NULL comment'标示',
  `value` varchar(20000) CHARACTER SET utf8 DEFAULT NULL comment'值'
  -- PRIMARY KEY `history_uuid` (`history_uuid`)
 -- CONSTRAINT `fk_cd_history_pipeline_data_history_uuid` FOREIGN KEY (`history_uuid`) REFERENCES `cd_history_pipeline` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin comment '流水线历史记录的关联的数据';

-- ----------------------------
-- Table structure for history_pipeline_on_end
-- ----------------------------
DROP TABLE IF EXISTS `cd_history_pipeline_on_end`;
CREATE TABLE `cd_history_pipeline_on_end` (
  `uuid` varchar(36) COLLATE utf8_bin NOT NULL comment'主键',
  `on_end_uuid` varchar(36) COLLATE utf8_bin NOT NULL comment'关联到 cd_pipeline_on_end 的 uuid',
  `history_uuid` varchar(36) COLLATE utf8_bin NOT NULL comment'关联到 cd_history_pipeline 的 uuid',
  `task_order` int(11) NOT NULL DEFAULT 1 comment'任务顺序',
  `name` varchar(100) COLLATE utf8_bin NOT NULL comment'名称',
  `type` varchar(30) COLLATE utf8_bin NOT NULL comment'类型',
  `status` varchar(30) COLLATE utf8_bin NOT NULL comment'状态',
  `timeout` int(11) NOT NULL DEFAULT 0 comment'超时时长',
  `started_at` timestamp NULL DEFAULT NULL comment'开始时间',
  `ended_at` timestamp NULL DEFAULT NULL comment'结束时间',
  PRIMARY KEY (`uuid`),
  KEY `history_uuid` (`history_uuid`)
 -- CONSTRAINT `fk_cd_history_pipeline_on_end` FOREIGN KEY (`history_uuid`) REFERENCES `cd_history_pipeline` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin comment'流水线历史记录中的 on_end';

-- ----------------------------
-- Table structure for history_pipeline_on_end_data
-- ----------------------------
DROP TABLE IF EXISTS `cd_history_pipeline_on_end_data`;
CREATE TABLE `cd_history_pipeline_on_end_data` (
  `on_end_uuid` varchar(36) COLLATE utf8_bin NOT NULL comment'关联到 cd_history_pipeline_on_end 的 uuid',
  `key` varchar(256) COLLATE utf8_bin NOT NULL comment'标示',
  `value` varchar(20000) COLLATE utf8_bin DEFAULT NULL comment'值',
  KEY `on_end_uuid` (`on_end_uuid`)
 -- CONSTRAINT `fk_history_pipeline_on_end_data_on_end_uuid` FOREIGN KEY (`on_end_uuid`) REFERENCES `cd_history_pipeline_on_end` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin comment'流水线历史记录中 on_end 关联的数据';

-- ----------------------------
-- Table structure for history_stage
-- ----------------------------
DROP TABLE IF EXISTS `cd_history_stage`;
CREATE TABLE `cd_history_stage` (
  `uuid` varchar(36) COLLATE utf8_bin NOT NULL comment'主键',
  `stage_uuid` varchar(36) COLLATE utf8_bin NOT NULL comment'关联到 stage 的 uuid',
  `history_uuid` varchar(36) COLLATE utf8_bin NOT NULL comment'关联到 cd_history_pipeline 的 uuid',
  `pipeline_order` int(11) NOT NULL DEFAULT 1 comment'在 pipeline 中的顺序, 现在只有 1',
  `region_uuid` varchar(36) COLLATE utf8_bin NOT NULL comment'集群 uuid',
  `status` varchar(30) COLLATE utf8_bin NOT NULL comment'状态',
  `environment_type` varchar(30) COLLATE utf8_bin NOT NULL comment'闲置',
  `started_at` timestamp NULL DEFAULT NULL comment'开始时间',
  `ended_at` timestamp NULL DEFAULT NULL comment'结束时间',
  PRIMARY KEY (`uuid`),
  KEY `history_uuid` (`history_uuid`)
  -- CONSTRAINT `fk_cd_history_stage_history_uuid` FOREIGN KEY (`history_uuid`) REFERENCES `cd_history_pipeline` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin comment'stage历史记录';

-- ----------------------------
-- Table structure for history_stage_data
-- ----------------------------
DROP TABLE IF EXISTS `cd_history_stage_data`;
CREATE TABLE `cd_history_stage_data` (
  `stage_uuid` varchar(36) COLLATE utf8_bin NOT NULL comment'关联到 history_stage 的 uuid',
  `key` varchar(256) COLLATE utf8_bin NOT NULL comment'标示',
  `value` varchar(20000) COLLATE utf8_bin DEFAULT NULL comment'值',
  `type` int(11) NOT NULL DEFAULT 0 comment'闲置',
  `processed` tinyint(1) DEFAULT NULL comment'是否处理过, 当 stage 中数据记录到这里后, 此处设为 true',
  KEY `stage_uuid` (`stage_uuid`)
 -- CONSTRAINT `fk_cd_history_stage_data_stage_uuid` FOREIGN KEY (`stage_uuid`) REFERENCES `cd_history_stage` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin comment'stage 历史记录关联的数据';

-- ----------------------------
-- Table structure for history_task
-- ----------------------------
DROP TABLE IF EXISTS `cd_history_task`;
CREATE TABLE `cd_history_task` (
  `uuid` varchar(36) COLLATE utf8_bin NOT NULL comment'主键',
  `task_uuid` varchar(36) COLLATE utf8_bin NOT NULL comment'关联到 task 的 uuid',
  `stage_uuid` varchar(36) COLLATE utf8_bin NOT NULL comment'关联到 history_stage 的 uuid',
  `stage_order` int(11) NOT NULL DEFAULT 1 comment'任务的执行顺序',
  `status` varchar(30) COLLATE utf8_bin NOT NULL comment'状态',
  `name` varchar(100) COLLATE utf8_bin NOT NULL comment'名称',
  `type` varchar(30) COLLATE utf8_bin NOT NULL comment'类型',
  `timeout` int(11) NOT NULL DEFAULT 0 comment'超时时长',
  `started_at` timestamp NULL DEFAULT NULL comment'开始时间',
  `ended_at` timestamp NULL DEFAULT NULL comment'结束时间',
  `region_uuid` varchar(36) COLLATE utf8_bin DEFAULT NULL comment'集群 uuid',
  PRIMARY KEY (`uuid`),
  KEY `stage_uuid` (`stage_uuid`)
  -- CONSTRAINT `fk_cd_history_task_stage_uuid` FOREIGN KEY (`stage_uuid`) REFERENCES `cd_history_stage` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin comment'task 的历史记录';

-- ----------------------------
-- Table structure for history_task_data
-- ----------------------------
DROP TABLE IF EXISTS `cd_history_task_data`;
CREATE TABLE `cd_history_task_data` (
  `task_uuid` varchar(36) COLLATE utf8_bin NOT NULL comment'关联到 task 表的 uuid',
  `key` varchar(256) COLLATE utf8_bin NOT NULL comment'标示',
  `value` varchar(20000) COLLATE utf8_bin DEFAULT NULL comment'值',
  KEY `task_uuid` (`task_uuid`)
 -- CONSTRAINT `fk_history_task_data_task_uuid` FOREIGN KEY (`task_uuid`) REFERENCES `cd_history_task` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin comment'task 历史记录关联的数据';

-- ----------------------------
-- Table structure for pipeline
-- ----------------------------
DROP TABLE IF EXISTS `cd_pipeline`;
CREATE TABLE `cd_pipeline` (
  `uuid` varchar(36) COLLATE utf8_bin NOT NULL comment' 主键',
  `description` varchar(200) COLLATE utf8_bin DEFAULT NULL comment'描述',
  `created_at` timestamp NOT NULL DEFAULT now() comment'创建时间',
  `updated_at` timestamp NOT NULL DEFAULT now() comment'修改时间',
  `artifact_enabled` int(11) NOT NULL DEFAULT 0 comment'是否保存流水线中产出物',
  `schedule_config_id` varchar(36) COLLATE utf8_bin DEFAULT NULL comment'定时配置的 id',
  `schedule_config_secret_key` varchar(36) COLLATE utf8_bin DEFAULT NULL comment'定时配置的 key, 与 schedule_config_secret_key 配合使用',
  `created_by` varchar(30) COLLATE utf8_bin DEFAULT NULL comment'创建者',
  `created_by_token` varchar(50) COLLATE utf8_bin DEFAULT NULL comment'创建者 token',
  `project_name` varchar(100) COLLATE utf8_bin DEFAULT NULL comment'项目名称',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin comment'流水线信息';

-- ----------------------------
-- Table structure for pipeline_on_end
-- ----------------------------
DROP TABLE IF EXISTS `cd_pipeline_on_end`;
CREATE TABLE `cd_pipeline_on_end` (
  `uuid` varchar(36) COLLATE utf8_bin NOT NULL comment'主键',
  `pipeline_uuid` varchar(36) COLLATE utf8_bin NOT NULL comment'关联到 pipeline 的 uuid',
  `task_order` int(11) NOT NULL DEFAULT 1 comment'任务顺序',
  `name` varchar(100) COLLATE utf8_bin NOT NULL comment'名称',
  `type` varchar(30) COLLATE utf8_bin NOT NULL comment'类型',
  `timeout` int(11) NOT NULL DEFAULT 0 comment'超时时长',
  PRIMARY KEY (`uuid`),
  KEY `pipeline_uuid` (`pipeline_uuid`)
  -- CONSTRAINT `fk_cd_pipeline_on_end_pipeline_uuid` FOREIGN KEY (`pipeline_uuid`) REFERENCES `cd_pipeline` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin comment'流水线中的结束任务';

-- ----------------------------
-- Table structure for pipeline_on_end_data
-- ----------------------------
DROP TABLE IF EXISTS `cd_pipeline_on_end_data`;
CREATE TABLE `cd_pipeline_on_end_data` (
  `on_end_uuid` varchar(36) COLLATE utf8_bin NOT NULL comment'关联到 pipeline_on_end 的 uuid',
  `key` varchar(256) COLLATE utf8_bin NOT NULL comment'标识',
  `value` varchar(20000) COLLATE utf8_bin DEFAULT NULL comment'值',
  KEY `on_end_uuid` (`on_end_uuid`)
 -- CONSTRAINT `fk_cd_pipeline_on_end_data_uuid` FOREIGN KEY (`on_end_uuid`) REFERENCES `cd_pipeline_on_end` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin comment'保存 pipeline_on_end 中的数据';

-- ----------------------------
-- Table structure for pipeline_trigger_build
-- ----------------------------
DROP TABLE IF EXISTS `cd_pipeline_trigger_build`;
CREATE TABLE `cd_pipeline_trigger_build` (
  `pipeline_uuid` varchar(36) COLLATE utf8_bin NOT NULL comment'关联到 pipeline 的 uuid',
  `build_config_uuid` varchar(36) COLLATE utf8_bin DEFAULT NULL comment'项目构建 uuid',
  `build_config_name` varchar(100) COLLATE utf8_bin DEFAULT NULL comment'项目构建名称',
  `code_repo_client` varchar(100) COLLATE utf8_bin DEFAULT NULL comment'闲置',
  KEY `pipeline_uuid` (`pipeline_uuid`)
 -- CONSTRAINT `fk_cd_pipeline_trigger_build_pipeline_uuid` FOREIGN KEY (`pipeline_uuid`) REFERENCES `cd_pipeline` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin comment'触发器: 代码仓库';

-- ----------------------------
-- Table structure for pipeline_trigger_repository
-- ----------------------------
DROP TABLE IF EXISTS `cd_pipeline_trigger_repository`;
CREATE TABLE `cd_pipeline_trigger_repository` (
  `pipeline_uuid` varchar(36) COLLATE utf8_bin NOT NULL comment'关联到 pipeline 的 uuid',
  `registry` varchar(100) COLLATE utf8_bin DEFAULT NULL comment'源',
  `registry_uuid` varchar(36) COLLATE utf8_bin DEFAULT NULL comment'源 uuid',
  `repository` varchar(100) COLLATE utf8_bin DEFAULT NULL comment'仓库',
  `repository_uuid` varchar(36) COLLATE utf8_bin DEFAULT NULL comment'仓库 uuid',
  `image_tag` varchar(100) COLLATE utf8_bin DEFAULT NULL comment'镜像 tag 名称',
  KEY `pipeline_uuid` (`pipeline_uuid`)
 -- CONSTRAINT `fk_pipeline_trigger_repository_pipeline_uuid` FOREIGN KEY (`pipeline_uuid`) REFERENCES `cd_pipeline` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin comment'触发器: 镜像仓库';

-- ----------------------------
-- Table structure for provider_tasks_list
-- ----------------------------
DROP TABLE IF EXISTS `cd_provider_tasks_list`;
CREATE TABLE `cd_provider_tasks_list` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT comment'主键',
  `type` varchar(256) COLLATE utf8_bin DEFAULT NULL comment'类型',
  `key` varchar(256) COLLATE utf8_bin DEFAULT NULL comment'标示',
  `worker` varchar(100) COLLATE utf8_bin DEFAULT NULL comment'worker标示',
  `status` varchar(50) COLLATE utf8_bin DEFAULT NULL comment'状态',
  `created_at` timestamp NOT NULL DEFAULT now() comment'开始时间',
  `updated_at` timestamp NOT NULL DEFAULT now() comment'更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin comment'执行中的任务列表';

-- ----------------------------
-- Table structure for region_task_queue
-- ----------------------------
DROP TABLE IF EXISTS `cd_region_task_queue`;
CREATE TABLE `cd_region_task_queue` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT comment'主键',
  `type` varchar(100) COLLATE utf8_bin DEFAULT NULL comment'类型',
  `key` varchar(200) COLLATE utf8_bin DEFAULT NULL comment'标示',
  `stage_uuid` varchar(36) COLLATE utf8_bin DEFAULT NULL comment'关联到 stage 表的 uuid',
  `log_uuid` varchar(36) COLLATE utf8_bin DEFAULT NULL comment'日志 uuid',
  `region_uuid` varchar(36) COLLATE utf8_bin DEFAULT NULL comment'集群 uuid',
  `status` varchar(36) COLLATE utf8_bin DEFAULT NULL comment'状态',
  `created_at` timestamp NOT NULL DEFAULT now() comment'开始时间',
  `updated_at` timestamp NOT NULL DEFAULT now() comment'结束时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin comment'集群中的 task 执行序列, 一般是保存在 redis 中';

-- ----------------------------
-- Table structure for stage
-- ----------------------------
DROP TABLE IF EXISTS `cd_stage`;
CREATE TABLE `cd_stage` (
  `uuid` varchar(36) COLLATE utf8_bin NOT NULL comment'主键',
  `pipeline_uuid` varchar(36) COLLATE utf8_bin NOT NULL comment'关联到 pipeline 表的 uuid',
  `pipeline_order` int(11) NOT NULL DEFAULT 1 comment 'stage 在 pipeline 中的执行顺序, 现在只有1',
  `region_uuid` varchar(36) COLLATE utf8_bin NOT NULL comment'集群 uuid, 现在移到 task 中配置',
  `environment_type` varchar(30) COLLATE utf8_bin NOT NULL comment'闲置',
  PRIMARY KEY (`uuid`),
  KEY `pipeline_uuid` (`pipeline_uuid`)
 -- CONSTRAINT `fk_cd_stage_pipeline_uuid` FOREIGN KEY (`pipeline_uuid`) REFERENCES `cd_pipeline` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin comment'stage 信息';

-- ----------------------------
-- Table structure for stage_data
-- ----------------------------
DROP TABLE IF EXISTS `cd_stage_data`;
CREATE TABLE `cd_stage_data` (
  `stage_uuid` varchar(36) COLLATE utf8_bin NOT NULL comment'关联到 stage 表的 uuid',
  `key` varchar(256) COLLATE utf8_bin NOT NULL comment'标识',
  `value` varchar(20000) COLLATE utf8_bin DEFAULT NULL comment'值',
   KEY `stage_uuid` (`stage_uuid`)
  -- CONSTRAINT `fk_stage_data_stage_uuid` FOREIGN KEY (`stage_uuid`) REFERENCES `cd_stage` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin comment'stage 中关联的数据';

-- ----------------------------
-- Table structure for task
-- ----------------------------
DROP TABLE IF EXISTS `cd_task`;
CREATE TABLE `cd_task` (
  `uuid` varchar(36) COLLATE utf8_bin NOT NULL comment'主键',
  `stage_uuid` varchar(36) COLLATE utf8_bin NOT NULL comment'关联到 stage 表的 uuid',
  `stage_order` int(11) NOT NULL DEFAULT 1 comment'task 在 stage 中的顺序',
  `name` varchar(100) COLLATE utf8_bin NOT NULL comment'名称',
  `type` varchar(30) COLLATE utf8_bin NOT NULL comment'类型',
  `timeout` int(11) NOT NULL DEFAULT 0 comment'超时时长',
  `region_uuid` varchar(36) COLLATE utf8_bin DEFAULT NULL comment'集群 uuid',
  PRIMARY KEY (`uuid`),
  KEY `stage_uuid` (`stage_uuid`)
 -- CONSTRAINT `fk_cd_task_stage_uuid` FOREIGN KEY (`stage_uuid`) REFERENCES `cd_stage` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin comment'task 信息';

-- ----------------------------
-- Table structure for task_data
-- ----------------------------
DROP TABLE IF EXISTS `cd_task_data`;
CREATE TABLE `cd_task_data` (
  `task_uuid` varchar(36) COLLATE utf8_bin NOT NULL comment'关联到 task 表的 uuid',
  `key` varchar(256) COLLATE utf8_bin NOT NULL comment'标识',
  `value` varchar(20000) COLLATE utf8_bin DEFAULT NULL comment'值',
  KEY `task_uuid` (`task_uuid`)
 -- CONSTRAINT `fk_cd_task_data_task_uuid` FOREIGN KEY (`task_uuid`) REFERENCES `cd_task` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin comment'task 关联的数据';

-- ----------------------------
-- history_pipeline modify by jinsu for add field build_num
-- ----------------------------
DROP TABLE IF EXISTS `cd_history_pipeline`;
CREATE TABLE `cd_history_pipeline` (
  `uuid` varchar(36) COLLATE utf8_bin NOT NULL comment'主键',
  `pipeline_uuid` varchar(36) COLLATE utf8_bin NOT NULL comment'流水线 uuid',
  `description` varchar(200) COLLATE utf8_bin DEFAULT NULL comment'描述',
  `trigger` varchar(30) COLLATE utf8_bin DEFAULT NULL comment'触发器类型: build/repository',
  `status` varchar(30) COLLATE utf8_bin NOT NULL comment'执行状态: pending --> deploying --> ongoing --> faild/completed',
  `created_by` varchar(30) COLLATE utf8_bin DEFAULT NULL comment'创建者名称',
  `created_by_token` varchar(50) COLLATE utf8_bin DEFAULT NULL comment'创建者 token',
  `started_at` timestamp NULL DEFAULT NULL comment'启动时间',
  `ended_at` timestamp NULL DEFAULT NULL comment'结束时间(status=faild/complete 时)',
  `created_at` timestamp NOT NULL DEFAULT now() comment'创建时间',
  `updated_at` timestamp NOT NULL DEFAULT now() comment'更改时间',
  `artifact_enabled` int(11) NOT NULL DEFAULT 0 comment'是否支持产出物: 0不支持; 1支持',
  `build_num` int(32) COLLATE utf8_bin DEFAULT 0 comment '构建数',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin comment'流水线的历史记录';

-- ----------------------------
-- trigger_afterdelete_on_cd_history_pipeline create by jinsu 2017/09/29
-- ----------------------------

set @@sql_mode = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';

DROP TRIGGER IF EXISTS trigger_afterdelete_on_cd_history_pipeline;
DELIMITER //
CREATE TRIGGER trigger_afterdelete_on_cd_history_pipeline AFTER DELETE ON cd_history_pipeline FOR EACH ROW
BEGIN
delete from cd_history_pipeline_data where history_uuid=old.uuid;
delete from cd_history_pipeline_on_end_data where on_end_uuid in(select uuid from cd_history_pipeline_on_end where history_uuid=old.uuid);
delete from cd_history_pipeline_on_end where history_uuid=old.uuid;
delete from cd_history_stage_data where stage_uuid in (select uuid from cd_history_stage where history_uuid=old.uuid);
delete from cd_history_task_data where task_uuid in (select uuid from cd_history_task where stage_uuid in (select uuid from cd_history_stage where history_uuid=old.uuid));
delete from cd_history_task where stage_uuid in (select uuid from cd_history_stage where history_uuid=old.uuid);
delete from cd_history_stage where history_uuid=old.uuid;
END;//
DELIMITER ;



DROP PROCEDURE IF EXISTS p_alter_cd_pipeline;
delimiter //
CREATE PROCEDURE p_alter_cd_pipeline()
begin
IF NOT EXISTS(SELECT 1 FROM information_schema.COLUMNS WHERE TABLE_SCHEMA='${MYSQL_CICDDB}' AND table_name='cd_pipeline' AND COLUMN_NAME='auto_trigger_enabled') THEN
   ALTER TABLE `cd_pipeline` ADD COLUMN `auto_trigger_enabled` INT(1) NULL DEFAULT 1 AFTER `artifact_enabled`;
END IF;
end;//
delimiter ;
call p_alter_cd_pipeline();

ALTER TABLE `cd_pipeline` ADD space_name VARCHAR(100) DEFAULT NULL;
ALTER TABLE `cd_pipeline` ADD name VARCHAR(100) DEFAULT NULL;
ALTER TABLE `cd_history_pipeline` ADD auto_trigger INT(1) NULL DEFAULT 1;
-- ALTER TABLE pipeline DROP COLUMN auto_trigger_enabled;
UPDATE `cd_pipeline` set auto_trigger_enabled=0;
UPDATE `cd_history_pipeline` set auto_trigger=0 ;

alter table cd_pipeline add region_id varchar(36);
alter table cd_history_pipeline add region_id varchar(36);
alter table cd_pipeline add region_name varchar(36);
alter table cd_history_pipeline add region_name varchar(36);
