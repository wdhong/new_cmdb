DROP TABLE IF EXISTS `ci_envs_catalog`;
CREATE TABLE IF NOT EXISTS `ci_envs_catalog` (
  `id` int(32) NOT NULL AUTO_INCREMENT comment 'id',
  `language` VARCHAR(128) COLLATE utf8_bin NOT NULL comment '语言',
  `runtime` VARCHAR(128) COLLATE utf8_bin NOT NULL comment '运行时版本，比如Java7',
  `is_default` tinyint(1) NOT NULL comment '是否是默认运行时',
  `has_addons` tinyint(1) NOT NULL comment '是否有附加组件',
  `image` VARCHAR(128) COLLATE utf8_bin NOT NULL comment '镜像名',
  `tag` VARCHAR(255) COLLATE utf8_bin NOT NULL comment '镜像tag',
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_idx_ci_envs_catalog_image_tag` (`tag`,`image`),
  UNIQUE KEY `u_idx_ci_envs_catalog_language_runtime_has_addons` (`has_addons`,`runtime`,`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin comment '构建配置';

-- ----------------------------
-- Records of ci_envs_catalog
-- ----------------------------
--INSERT INTO `ci_envs_catalog` VALUES ('1', 'golang', 'go1.7', '1', '1', '1', '1');
--INSERT INTO `ci_envs_catalog` VALUES ('2', 'golang', 'go1.6', '0', '0', '2', '1');
--INSERT INTO `ci_envs_catalog` VALUES ('3', 'java', 'openjdk7', '1', '1', '1', '2');
--INSERT INTO `ci_envs_catalog` VALUES ('4', 'java', 'oraclejdk8', '0', '0', '2', '2');
-- ----------------------------

-- 原表 private_build_configs_buildconfig
-- ----------------------------
DROP TABLE IF EXISTS `ci_build_config`;
CREATE TABLE IF NOT EXISTS `ci_build_config` (
  `config_id` VARCHAR(36) COLLATE utf8_bin NOT NULL comment '配置ID',
  `image_repo_id` VARCHAR(36) COLLATE utf8_bin NOT NULL comment '镜像仓库id',
  `customize_tag` VARCHAR(32) COLLATE utf8_bin default 'latest' NOT NULL comment '自定义tag',
  `auto_tag_type` VARCHAR(16) COLLATE utf8_bin default 'NONE' NOT NULL comment '镜像tag类型',
  `code_cache_enabled` tinyint(1) NOT NULL comment '代码缓存启用',
  `image_cache_enabled` tinyint(1) NOT NULL comment '镜像缓存启用',
  `auto_build_enabled` tinyint(1) NOT NULL comment '自动持续构建启用',
  `notification_id` VARCHAR(36) COLLATE utf8_bin DEFAULT NULL comment '通知id',
  `created_at` timestamp NOT NULL DEFAULT now() comment '构建配置创建时间',
  `updated_at` timestamp NOT NULL DEFAULT now() comment '构建配置更新时间',
  `build_enabled` tinyint(1) NOT NULL comment '构建启用标志',
  `ci_enabled` tinyint(1) NOT NULL comment 'CI持续集成启用',
  `ci_envs` text COLLATE utf8_bin NOT NULL comment ' CI持续集成镜像环境变量',
  `endpoint_id` VARCHAR(64) COLLATE utf8_bin NOT NULL comment '构建节点id',
  `ci_config_file_location` VARCHAR(255) COLLATE utf8_bin NOT NULL comment 'CI配置文件位置',
  `artifact_upload_enabled` tinyint(1) NOT NULL comment '产出物上传启用标志',
  `ci_steps` text COLLATE utf8_bin NOT NULL comment ' CI执行的步骤',
  `schedule_config_id` VARCHAR(36) COLLATE utf8_bin NOT NULL comment '定时配置ID',
  `schedule_config_secret_key` VARCHAR(16) COLLATE utf8_bin NOT NULL comment '定时配置秘钥',
  `cpu` double NOT NULL comment 'cpu分配量',
  `memory` int(32) NOT NULL comment '内存分配量',
  `customize_tag_rule` VARCHAR(255) COLLATE utf8_bin NOT NULL comment '自定义镜像tag规则',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin comment '构建配置信息';

-- ----------------------------
-- 原表 private_build_configs_buildconfigcode
-- ----------------------------
DROP TABLE IF EXISTS `ci_build_code`;
CREATE TABLE IF NOT EXISTS `ci_build_code` (
  `code_id` VARCHAR(36) COLLATE utf8_bin NOT NULL comment '代码配置ID',
  `code_repo_client` VARCHAR(20) COLLATE utf8_bin NOT NULL comment '客户端',
  `code_repo_path` text COLLATE utf8_bin NOT NULL comment '代码路径',
  `code_repo_type` VARCHAR(16) COLLATE utf8_bin NOT NULL comment '代码仓库类型',
  `code_repo_type_value` VARCHAR(64) COLLATE utf8_bin NOT NULL comment '分支名或目录名',
  `dockerfile_location` VARCHAR(128) COLLATE utf8_bin NOT NULL comment ' Dockerfile 文件路径',
  `dockerfile_content` text COLLATE utf8_bin comment ' dockerfile文件内容',
  `build_context_path` VARCHAR(128) COLLATE utf8_bin NOT NULL comment '编译所在路径',
  `code_repo_webhook_id` VARCHAR(128) COLLATE utf8_bin DEFAULT NULL comment '代码仓库 webhook_id',
  `code_repo_webhook_code` VARCHAR(16) COLLATE utf8_bin DEFAULT NULL comment '代码仓库 webhook_code',
  `code_repo_key_id` int(32) DEFAULT NULL comment '代码仓库 key_id',
  `code_repo_private_key` text COLLATE utf8_bin comment '代码仓库私钥',
  `code_repo_public_key` text COLLATE utf8_bin comment '代码仓库公钥',
  `created_at` timestamp NOT NULL DEFAULT now() comment '创建时间',
  `updated_at` timestamp NOT NULL DEFAULT now() comment '更新时间',
  `build_config_id` VARCHAR(36) COLLATE utf8_bin NOT NULL comment '构建配置ID',
  `code_repo_password` VARCHAR(128) COLLATE utf8_bin NOT NULL comment '代码仓库密码',
  `code_repo_username` VARCHAR(120) COLLATE utf8_bin NOT NULL comment '代码仓库用户名',
  `repo_id` int(32) DEFAULT NULL comment '镜像仓库ID',
  `head_commit_id` VARCHAR(128) COLLATE utf8_bin DEFAULT NULL comment '代码提交ID',
  PRIMARY KEY (`code_id`),
  KEY `idx_ci_build_code_repo_webhook_code` (`code_repo_webhook_code`),
  KEY `idx_ci_build_code_config_id` (`build_config_id`)
  -- CONSTRAINT `fk_ci_build_code_id` FOREIGN KEY (`build_config_id`) REFERENCES `ci_build_config` (`config_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin comment '配置代码仓库';

-- ----------------------------
-- 原表 private_code_clients_codeclientuser
-- ----------------------------
DROP TABLE IF EXISTS `ci_build_code_client_user`;
CREATE TABLE IF NOT EXISTS `ci_build_code_client_user` (
  `id` VARCHAR(36) COLLATE utf8_bin NOT NULL comment '主键',
  `access_token` VARCHAR(128) COLLATE utf8_bin DEFAULT NULL comment '代码仓库 access_token',
  `access_token_secret` VARCHAR(64) COLLATE utf8_bin DEFAULT NULL comment '代码仓库 refresh_token',
  `code_repo_client_user_name` VARCHAR(64) COLLATE utf8_bin DEFAULT NULL comment '代码仓库用户名',
  `code_repo_client` VARCHAR(16) COLLATE utf8_bin NOT NULL comment '标识代码仓库的客户端',
  `user_name` VARCHAR(64) COLLATE utf8_bin NOT NULL comment '用户名称',
  `request_token_secret` VARCHAR(32) COLLATE utf8_bin DEFAULT NULL comment '请求token',
  `code_repo_client_user_id` VARCHAR(64) COLLATE utf8_bin DEFAULT NULL comment '代码仓库用户id',
  `refresh_token` VARCHAR(128) COLLATE utf8_bin NOT NULL comment 'refresh token',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin comment '代码仓库用户管理';

-- ----------------------------
-- 原表 private_builds_buildendpoint
-- ----------------------------
DROP TABLE IF EXISTS `ci_build_endpoint`;
CREATE TABLE IF NOT EXISTS `ci_build_endpoint` (
  `endpoint_id` VARCHAR(36) COLLATE utf8_bin NOT NULL comment 'endpoint_id',
  `namespace` VARCHAR(32) COLLATE utf8_bin NOT NULL comment '名称空间',
  `endpoint` VARCHAR(128) COLLATE utf8_bin NOT NULL comment '构建节点标识',
  `builder_image` VARCHAR(128) COLLATE utf8_bin NOT NULL comment '构建镜像',
  `es_host` VARCHAR(255) COLLATE utf8_bin NOT NULL comment 'elasticsearch host',
  `es_password` VARCHAR(128) COLLATE utf8_bin NOT NULL comment 'elasticsearch 密码',
  `es_username` VARCHAR(128) COLLATE utf8_bin NOT NULL comment 'elasticsearch 用户名',
  `proxy_config` text COLLATE utf8_bin NOT NULL comment '代理配置',
  `region_id` VARCHAR(64) COLLATE utf8_bin NOT NULL comment '集群ID',
  `is_public` tinyint(1) NOT NULL comment '是否是公共构建节点/集群',
  PRIMARY KEY (`endpoint_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin comment '构建端点';

-- ----------------------------
-- 原表 private_builds_buildimagetag
-- ----------------------------
DROP TABLE IF EXISTS `ci_build_image_tag`;
CREATE TABLE IF NOT EXISTS `ci_build_image_tag` (
  `id` int(32) NOT NULL AUTO_INCREMENT comment '',
  `image_repo_id` VARCHAR(36) COLLATE utf8_bin NOT NULL comment '镜像id',
  `tag_name` VARCHAR(128) COLLATE utf8_bin NOT NULL comment '镜像tag名称',
  `namespace` VARCHAR(30) COLLATE utf8_bin DEFAULT NULL comment '名称空间',
  `user_token` VARCHAR(100) COLLATE utf8_bin DEFAULT NULL comment '用户token',
  `build_id` VARCHAR(36) COLLATE utf8_bin DEFAULT NULL comment '构建id',
  `artifact_upload_enabled` tinyint(1) NOT NULL comment '标识产出物上传是否启用',
  `created_at` timestamp NOT NULL DEFAULT now() comment '创建时间',
  `updated_at` timestamp NOT NULL DEFAULT now() comment '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_idx_ci_build_image_tag_repo_id_tag_name` (`tag_name`,`image_repo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin comment '构建镜像版本';


-- ----------------------------
-- 原表 private_builds_build
-- ----------------------------
DROP TABLE IF EXISTS `ci_build_history`;
CREATE TABLE IF NOT EXISTS `ci_build_history` (
  `build_id` VARCHAR(36) COLLATE utf8_bin NOT NULL comment '构建id',
  `image_id` VARCHAR(128) COLLATE utf8_bin DEFAULT NULL comment '镜像中心id',
  `image_repo_id` VARCHAR(36) COLLATE utf8_bin NOT NULL comment '镜像仓库id',
  `registry_index` VARCHAR(128) COLLATE utf8_bin NOT NULL comment '镜像中心 urL',
  `docker_repo_path` VARCHAR(128) COLLATE utf8_bin NOT NULL comment '镜像仓库路径',
  `docker_repo_tag` VARCHAR(32) COLLATE utf8_bin NOT NULL comment '镜像仓库tag',
  `auto_tag_type` VARCHAR(16) COLLATE utf8_bin NOT NULL comment '镜像tag类型',
  `docker_repo_version_tag` VARCHAR(128) COLLATE utf8_bin DEFAULT NULL comment '镜像仓库 version_tag',
  `status` VARCHAR(8) COLLATE utf8_bin NOT NULL comment '构建状态',
  `step_at` int(32) NOT NULL comment '构建阶段',
  `notification_id` VARCHAR(36) COLLATE utf8_bin DEFAULT NULL comment '通知id ',
  `notification_name` VARCHAR(128) COLLATE utf8_bin DEFAULT NULL comment '通知名',
  `build_cache_enabled` tinyint(1) NOT NULL comment '构建缓存启用标志',
  `is_auto_created` tinyint(1) NOT NULL comment '是否是webhook触发构建',
  `dockerfile_content` text COLLATE utf8_bin comment ' dockerfile文件内容',
  `created_by` VARCHAR(30) COLLATE utf8_bin NOT NULL comment '构建创建者',
  `user_token` VARCHAR(100) COLLATE utf8_bin NOT NULL comment '用户 token',
  `namespace` VARCHAR(30) COLLATE utf8_bin NOT NULL comment '名称空间',
  `created_at` timestamp NOT NULL DEFAULT now() comment '创建时间',
  `started_at` timestamp NULL DEFAULT NULL comment '开始时间',
  `ended_at` timestamp NULL DEFAULT NULL comment '结束时间',
  `config_id` VARCHAR(36) COLLATE utf8_bin NOT NULL comment '配置id',
  `build_enabled` tinyint(1) NOT NULL comment '构建启用标志',
  `ci_enabled` tinyint(1) NOT NULL comment ' ci持续集成启用标志',
  `ci_envs` text COLLATE utf8_bin NOT NULL comment ' ci持续集成镜像环境变量',
  `endpoint_id` VARCHAR(64) COLLATE utf8_bin NOT NULL comment '构建节点id',
  `ci_config_file_location` VARCHAR(255) COLLATE utf8_bin NOT NULL comment 'CI配置文件位置',
  `artifact_upload_enabled` tinyint(1) NOT NULL comment '产出物上传启用标志',
  `ci_steps` text COLLATE utf8_bin NOT NULL comment ' ci执行的步骤',
  `is_scheduled` tinyint(1) NOT NULL comment '是否是定时触发构建',
  `cpu` double NOT NULL comment '构建时job分配的cpu',
  `memory` int(32) NOT NULL comment '内存分配量',
  `customize_tag_rule` VARCHAR(255) COLLATE utf8_bin NOT NULL comment '自定义镜像tag规则',
  `docker_repo_label_tag` VARCHAR(128) COLLATE utf8_bin NOT NULL comment '镜像仓库 label_ta',
  PRIMARY KEY (`build_id`),
  KEY `idx_ci_build_history_config_id` (`config_id`)
  --  CONSTRAINT `fk_ci_build_history_config_id` FOREIGN KEY (`config_id`) REFERENCES `ci_build_config` (`config_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin comment '构建历史表';


-- ----------------------------
-- 原表 private_builds_buildcode
-- ----------------------------
DROP TABLE IF EXISTS `ci_build_code_history`;
CREATE TABLE IF NOT EXISTS `ci_build_code_history` (
  `id` VARCHAR(36) NOT NULL comment '',
  `code_id` VARCHAR(36) COLLATE utf8_bin NOT NULL comment 'code_id',
  `code_repo_client` VARCHAR(20) COLLATE utf8_bin NOT NULL comment '代码仓库客户端',
  `code_repo_path` VARCHAR(255) COLLATE utf8_bin NOT NULL comment '代码仓库路径',
  `code_repo_type` VARCHAR(16) COLLATE utf8_bin NOT NULL comment '代码仓库类型',
  `code_repo_type_value` VARCHAR(64) COLLATE utf8_bin NOT NULL comment '代码仓库类型值',
  `code_head_commit` VARCHAR(128) COLLATE utf8_bin DEFAULT NULL comment '代码提交id',
  `dockerfile_location` VARCHAR(128) COLLATE utf8_bin NOT NULL comment 'dockerfile文件路径',
  `build_context_path` VARCHAR(128) COLLATE utf8_bin NOT NULL comment '构建上线文路径',
  `code_repo_key_id` int(32) DEFAULT NULL comment '代码仓库 key_id',
  `code_repo_private_key` text COLLATE utf8_bin comment '代码仓库 私钥',
  `created_at` timestamp NOT NULL DEFAULT now() comment '创建时间',
  `build_id` VARCHAR(36) COLLATE utf8_bin NOT NULL comment '镜像id',
  `code_repo_password` VARCHAR(128) COLLATE utf8_bin NOT NULL comment '代码仓库密码',
  `code_repo_username` VARCHAR(120) COLLATE utf8_bin NOT NULL comment '代码仓库用户名',
  PRIMARY KEY (`id`),
  KEY `idx_ci_build_code_history_build_id` (`build_id`)
  --  CONSTRAINT `fk_ci_build_code_history_build_id` FOREIGN KEY (`build_id`) REFERENCES `ci_build_history` (`build_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin comment '构建代码配置';


-- ----------------------------
-- 原表 private_builds_build modify by jinsu for add field build_num
-- ----------------------------
DROP TABLE IF EXISTS `ci_build_history`;
CREATE TABLE IF NOT EXISTS `ci_build_history` (
  `build_id` VARCHAR(36) COLLATE utf8_bin NOT NULL comment '构建id',
  `image_id` VARCHAR(128) COLLATE utf8_bin DEFAULT NULL comment '镜像中心id',
  `image_repo_id` VARCHAR(36) COLLATE utf8_bin NOT NULL comment '镜像仓库id',
  `registry_index` VARCHAR(128) COLLATE utf8_bin NOT NULL comment '镜像中心 urL',
  `docker_repo_path` VARCHAR(128) COLLATE utf8_bin NOT NULL comment '镜像仓库路径',
  `docker_repo_tag` VARCHAR(32) COLLATE utf8_bin NOT NULL comment '镜像仓库tag',
  `auto_tag_type` VARCHAR(16) COLLATE utf8_bin NOT NULL comment '镜像tag类型',
  `docker_repo_version_tag` VARCHAR(128) COLLATE utf8_bin DEFAULT NULL comment '镜像仓库 version_tag',
  `status` VARCHAR(8) COLLATE utf8_bin NOT NULL comment '构建状态',
  `step_at` int(32) NOT NULL comment '构建阶段',
  `notification_id` VARCHAR(36) COLLATE utf8_bin DEFAULT NULL comment '通知id ',
  `notification_name` VARCHAR(128) COLLATE utf8_bin DEFAULT NULL comment '通知名',
  `build_cache_enabled` tinyint(1) NOT NULL comment '构建缓存启用标志',
  `is_auto_created` tinyint(1) NOT NULL comment '是否是webhook触发构建',
  `dockerfile_content` text COLLATE utf8_bin comment ' dockerfile文件内容',
  `created_by` VARCHAR(30) COLLATE utf8_bin NOT NULL comment '构建创建者',
  `user_token` VARCHAR(100) COLLATE utf8_bin NOT NULL comment '用户 token',
  `namespace` VARCHAR(30) COLLATE utf8_bin NOT NULL comment '名称空间',
  `created_at` timestamp NOT NULL DEFAULT now() comment '创建时间',
  `started_at` timestamp NULL DEFAULT NULL comment '开始时间',
  `ended_at` timestamp NULL DEFAULT NULL comment '结束时间',
  `config_id` VARCHAR(36) COLLATE utf8_bin NOT NULL comment '配置id',
  `build_enabled` tinyint(1) NOT NULL comment '构建启用标志',
  `ci_enabled` tinyint(1) NOT NULL comment ' ci持续集成启用标志',
  `ci_envs` text COLLATE utf8_bin NOT NULL comment ' ci持续集成镜像环境变量',
  `endpoint_id` VARCHAR(64) COLLATE utf8_bin NOT NULL comment '构建节点id',
  `ci_config_file_location` VARCHAR(255) COLLATE utf8_bin NOT NULL comment 'CI配置文件位置',
  `artifact_upload_enabled` tinyint(1) NOT NULL comment '产出物上传启用标志',
  `ci_steps` text COLLATE utf8_bin NOT NULL comment ' ci执行的步骤',
  `is_scheduled` tinyint(1) NOT NULL comment '是否是定时触发构建',
  `cpu` double NOT NULL comment '构建时job分配的cpu',
  `memory` int(32) NOT NULL comment '内存分配量',
  `customize_tag_rule` VARCHAR(255) COLLATE utf8_bin NOT NULL comment '自定义镜像tag规则',
  `docker_repo_label_tag` VARCHAR(128) COLLATE utf8_bin NOT NULL comment '镜像仓库 label_ta',
  `build_num` int(32) COLLATE utf8_bin DEFAULT 0 comment '构建数',
  PRIMARY KEY (`build_id`),
  KEY `idx_ci_build_history_config_id` (`config_id`)
  --  CONSTRAINT `fk_ci_build_history_config_id` FOREIGN KEY (`config_id`) REFERENCES `ci_build_config` (`config_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin comment '构建历史表';

-- add ci artifact function by jinsu 2017/11/08 
DROP TABLE IF EXISTS `ci_artifact_info`;
CREATE TABLE `ci_artifact_info` (
`build_id`  varchar(36) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '构建ID' ,
`jenkins_id`  varchar(36) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'jenkins分配的ID' ,
`name`  varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '名称' ,
`path`  varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '用于拼下载地址' ,
`url`  varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'url' ,
`size`  int(11) NOT NULL COMMENT '文件大小' ,
`last_modified`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '最后修改时间' 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_bin
COMMENT='产物信息表'
ROW_FORMAT=DYNAMIC
;

alter table ci_build_config add build_image_enabled tinyint(1) DEFAULT Null;
alter table ci_build_history add build_image_enabled tinyint(1) DEFAULT Null;

--update ci_envs_catalog set runtime='openjdk8' where runtime = 'openjdk7';

-- {{jinsu add 2017/12/08 16:51
-- 构建仓库地址需要更改10.181.12.12:60080/nodejs
--delete from ci_envs_catalog;
--INSERT INTO `ci_envs_catalog` VALUES ('1', 'java', 'openjdk8', '0', '0', '${ONEBOX_IP}:60080/claas/openjdk8', 'latest');
INSERT INTO `ci_envs_catalog` VALUES ('1', 'java', 'oraclejdk8', '1', '0', '${ONEBOX_IP}:60080/claas/oraclejdk8-alpine', 'v20180626');
INSERT INTO `ci_envs_catalog` VALUES ('2', 'java', 'oraclejre8', '0', '0', '${ONEBOX_IP}:60080/claas/oraclejre8-alpine', 'v20180626');
INSERT INTO `ci_envs_catalog` VALUES ('3', 'vue', 'nodejs9', '0', '0', '${ONEBOX_IP}:60080/claas/nodejs', '9.11.3-alpine');
-- end}}

alter table ci_build_config MODIFY image_repo_id varchar(36) DEFAULT Null;
alter table ci_build_config MODIFY customize_tag varchar(32) DEFAULT Null;
alter table ci_build_config MODIFY auto_tag_type varchar(16) DEFAULT Null;

alter table ci_build_history MODIFY image_repo_id varchar(36) DEFAULT Null;
alter table ci_build_history MODIFY docker_repo_tag varchar(32) DEFAULT Null;
alter table ci_build_history MODIFY auto_tag_type varchar(16) DEFAULT Null;
alter table ci_build_history MODIFY registry_index varchar(128) DEFAULT Null;
alter table ci_build_history MODIFY docker_repo_path varchar(128) DEFAULT Null;

alter table ci_build_config MODIFY endpoint_id varchar(64) DEFAULT Null;
alter table ci_build_history MODIFY endpoint_id varchar(64) DEFAULT Null;
alter table ci_build_config add region_id varchar(36);
alter table ci_build_history add region_id varchar(36);
alter table ci_build_config add region_name varchar(36);
alter table ci_build_history add region_name varchar(36);

