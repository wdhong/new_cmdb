/*
Navicat MySQL Data Transfer

Source Server         : 10.181.12.150_148
Source Server Version : 50718
Source Host           : 10.181.12.148:3306
Source Database       : prod_112_db

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2018-05-25 16:05:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `resource_project`
-- ----------------------------
DROP TABLE IF EXISTS `resource_project`;
CREATE TABLE resource_project (
	project_uuid varchar(36) NOT NULL,
	resource_uuid varchar(36) NOT NULL,
	belongs_to_project varchar(20),
	primary key (project_uuid,resource_uuid)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for resources_resource
-- ----------------------------
DROP TABLE IF EXISTS `resources_resource`;
CREATE TABLE `resources_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) DEFAULT NULL,
  `uuid` varchar(36) NOT NULL,
  `namespace` varchar(36) NOT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  `region_id` varchar(40) DEFAULT NULL,
  `space_uuid` varchar(40) NOT NULL,
  `project_uuid` varchar(36) NOT NULL,
  `knamespace_uuid` char(36) DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  UNIQUE KEY `resources_resource_name_3c5f024138507265_uniq` (`name`,`type`,`namespace`,`space_uuid`,`region_id`),
  KEY `resources_resource_namespace_4391c881a574ddc9_idx` (`namespace`,`uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=100946 DEFAULT CHARSET=utf8;


INSERT INTO `resources_resource` (`id`, `type`, `uuid`, `namespace`, `created_by`, `created_at`, `name`, `region_id`, `space_uuid`, `project_uuid`, `knamespace_uuid`) VALUES ('100002', 'PROJECT_TEMPLATE', '2d4cc683-ca96-4ee8-a4d2-1ebfb1ed63d9', '', 'alauda', '2017-11-01 17:53:51', 'base-template', '', '', '', '');
INSERT INTO `resources_resource` (`type`, `uuid`, `namespace`, `created_by`, `created_at`, `name`, `region_id`, `space_uuid`, `project_uuid`, `knamespace_uuid`) VALUES ('ORGANIZATION', '', 'admin', 'alauda', '2017-10-13 16:56:41', 'admin', '', '', '', '');
INSERT INTO `resources_resource` (`type`, `uuid`, `namespace`, `created_by`, `created_at`, `name`, `region_id`, `space_uuid`, `project_uuid`, `knamespace_uuid`) VALUES ('organization', RAND()*100000000000, 'alauda', 'alauda', '2017-10-13 16:56:41', 'alauda', '', '', '', '');
INSERT INTO `resources_resource` (`type`, `uuid`, `namespace`, `created_by`, `created_at`, `name`, `region_id`, `space_uuid`, `project_uuid`, `knamespace_uuid`) VALUES ('ROLE', '3dcbf31b-9924-41bf-b3dd-e0e702c0c4e3', 'alauda', 'alauda', NOW(), 'administrator', '', '', '', '');

--   2018/04/27: 新环境使用migu根账号替换alauda
-- INSERT INTO `resources_resource` (`id`, `type`, `uuid`, `namespace`, `created_by`, `created_at`, `name`, `region_id`, `space_uuid`, `project_uuid`, `knamespace_uuid`) VALUES ('100002', 'PROJECT_TEMPLATE', '2d4cc683-ca96-4ee8-a4d2-1ebfb1ed63d9', '', 'migu', '2017-11-01 17:53:51', 'base-template', '', '', '', '');
-- INSERT INTO `resources_resource` (`type`, `uuid`, `namespace`, `created_by`, `created_at`, `name`, `region_id`, `space_uuid`, `project_uuid`, `knamespace_uuid`) VALUES ('ORGANIZATION', '', 'admin', 'migu', '2017-10-13 16:56:41', 'admin', '', '', '', '');
-- INSERT INTO `resources_resource` (`type`, `uuid`, `namespace`, `created_by`, `created_at`, `name`, `region_id`, `space_uuid`, `project_uuid`, `knamespace_uuid`) VALUES ('organization', RAND()*100000000000, 'migu', 'migu', '2017-10-13 16:56:41', 'migu', '', '', '', '');
-- INSERT INTO `resources_resource` (`type`, `uuid`, `namespace`, `created_by`, `created_at`, `name`, `region_id`, `space_uuid`, `project_uuid`, `knamespace_uuid`) VALUES ('ROLE', '3dcbf31b-9924-41bf-b3dd-e0e702c0c4e3', 'migu', 'migu', NOW(), 'administrator', '', '', '', '');

