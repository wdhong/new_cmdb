/*
SQLyog Ultimate v11.22 (64 bit)
MySQL - 5.7.18-log : Database - prod_xhy_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `alarm_action` */

DROP TABLE IF EXISTS `alarm_action`;

CREATE TABLE `alarm_action` (
  `severity_level` varchar(96) DEFAULT NULL,
  `resource_uuid` varchar(192) DEFAULT NULL,
  `resource_name` varchar(768) DEFAULT NULL,
  `resource_type` varchar(192) DEFAULT NULL,
  `resource_action` varchar(192) DEFAULT NULL,
  `uuid` varchar(192) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `alarm_config` */

DROP TABLE IF EXISTS `alarm_config`;

CREATE TABLE `alarm_config` (
  `uuid` varchar(192) DEFAULT NULL,
  `region_id` varchar(40) NOT NULL COMMENT '域ID',
  `name` varchar(384) DEFAULT NULL,
  `namespace` varchar(192) DEFAULT NULL,
  `description` text,
  `alarm_actions` text,
  `insufficient_actions` text,
  `ok_actions` text,
  `notify_interval` int(11) DEFAULT NULL,
  `metric_name` varchar(6144) DEFAULT NULL,
  `statistic` varchar(96) DEFAULT NULL,
  `comparison_operator` varchar(96) DEFAULT NULL,
  `Period` int(11) DEFAULT NULL,
  `Threshold` float DEFAULT NULL,
  `Tags` text,
  `exclude_tags` text,
  `created_by` varchar(384) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(384) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `alarm_observer` */

DROP TABLE IF EXISTS `alarm_observer`;

CREATE TABLE `alarm_observer` (
  `alert_key` varchar(1024) NOT NULL,
  `status` varchar(60) DEFAULT NULL,
  `bosun_ack` tinyint(1) DEFAULT '0' COMMENT '0:未确认,1:已确认',
  `ago` datetime DEFAULT NULL,
  `actions` text,
  `alarm_name` varchar(384) DEFAULT NULL,
  `tsdb_query` varchar(3072) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `message` text,
  `version` int(11) DEFAULT NULL,
  `uuid` varchar(192) DEFAULT NULL,
  `region_id` varchar(40) NOT NULL COMMENT '域ID',
  PRIMARY KEY (`alert_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `alarm_observer_action` */

DROP TABLE IF EXISTS `alarm_observer_action`;

CREATE TABLE `alarm_observer_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `severity_level` int(11) DEFAULT NULL COMMENT 'normal = 0, warning = 1, critical = 2, unknown = 3, error = 4',
  `resource_uuid` varchar(40) COLLATE utf8_bin DEFAULT NULL COMMENT '资源ID',
  `resource_name` text COLLATE utf8_bin COMMENT '资源名',
  `resource_type` varchar(36) COLLATE utf8_bin DEFAULT NULL COMMENT '资源类型',
  `resource_action` varchar(40) COLLATE utf8_bin DEFAULT NULL COMMENT '资源操作',
  `fk_alarm_id` varchar(40) COLLATE utf8_bin NOT NULL COMMENT '关联alarm_observer_alarm_config.uuid',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='报警监控操作表';

/*Table structure for table `alarm_observer_alarm` */

DROP TABLE IF EXISTS `alarm_observer_alarm`;

CREATE TABLE `alarm_observer_alarm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alert_key` text COLLATE utf8_bin COMMENT '警报',
  `status` int(11) DEFAULT NULL COMMENT 'normal = 0, warning = 1, critical = 2, unknown = 3, error = 4',
  `ago` datetime DEFAULT NULL,
  `actions` text COLLATE utf8_bin COMMENT '操作',
  `alarm_name` varchar(128) COLLATE utf8_bin DEFAULT NULL COMMENT '警报名称',
  `updated_at` bigint(20) NOT NULL COMMENT '更新时间',
  `message` text COLLATE utf8_bin COMMENT '消息内容',
  `fk_alarm_id` varchar(40) COLLATE utf8_bin NOT NULL COMMENT '关联alarm_observer_alarm_config.uuid',
  `version` int(11) DEFAULT NULL COMMENT '版本',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='报警监控表';

/*Table structure for table `alarm_observer_alarm_config` */

DROP TABLE IF EXISTS `alarm_observer_alarm_config`;

CREATE TABLE `alarm_observer_alarm_config` (
  `uuid` varchar(40) COLLATE utf8_bin NOT NULL,
  `name` varchar(128) COLLATE utf8_bin NOT NULL COMMENT '名称',
  `namespace` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '命名空间',
  `description` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '描述',
  `alarm_actions` text COLLATE utf8_bin COMMENT '报警操作,JSON格式',
  `insufficient_actions` text COLLATE utf8_bin COMMENT 'JSON格式',
  `ok_actions` text COLLATE utf8_bin COMMENT 'JSON格式',
  `notify_interval` int(11) DEFAULT NULL COMMENT '提示间隔',
  `metric_name` varchar(128) COLLATE utf8_bin DEFAULT NULL COMMENT '维度名称',
  `statistic` int(11) DEFAULT NULL COMMENT 'Average = 1, Maximum = 2, Minimum = 3, Sum = 4, SampleCount = 5',
  `comparison_operator` int(11) DEFAULT NULL COMMENT 'LessThan=1, LessThanOrEqualTo = 2, EqualTo = 3, UnequalTo = 4, GreaterThan=5, GreaterThanOrEqualTo = 6',
  `period` int(11) DEFAULT NULL COMMENT '周期',
  `threshold` float DEFAULT NULL COMMENT '阀值',
  `tags` text COLLATE utf8_bin COMMENT '标签信息',
  `exclude_tags` text COLLATE utf8_bin COMMENT '排除的标签信息',
  `created_by` varchar(128) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `created_at` bigint(20) NOT NULL COMMENT '创建时间',
  `updated_by` varchar(128) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `updated_at` bigint(20) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='报警配置表';

/*Table structure for table `alarm_observer_resource` */

DROP TABLE IF EXISTS `alarm_observer_resource`;

CREATE TABLE `alarm_observer_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(40) COLLATE utf8_bin NOT NULL,
  `name` varchar(128) COLLATE utf8_bin NOT NULL COMMENT '名称',
  `type` varchar(16) COLLATE utf8_bin NOT NULL COMMENT '类型',
  `namespace` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '命名空间',
  `space_name` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `app_name` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='报警监控资源表';

/*Table structure for table `alarm_observer_version` */

DROP TABLE IF EXISTS `alarm_observer_version`;

CREATE TABLE `alarm_observer_version` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT '名称',
  `version` int(11) DEFAULT NULL COMMENT '版本',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='报警监控版本表';

/*Table structure for table `alarm_resource` */

DROP TABLE IF EXISTS `alarm_resource`;

CREATE TABLE `alarm_resource` (
  `uuid` varchar(192) DEFAULT NULL,
  `name` varchar(384) DEFAULT NULL,
  `type` varchar(96) DEFAULT NULL,
  `namespace` varchar(192) DEFAULT NULL,
  `space_name` varchar(192) DEFAULT NULL,
  `app_name` varchar(192) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `alarm_sync_unifyoperation` */

DROP TABLE IF EXISTS `alarm_sync_unifyoperation`;

CREATE TABLE `alarm_sync_unifyoperation` (
  `alert_key` varchar(1024) NOT NULL,
  `transaction_id` varchar(128) NOT NULL COMMENT 'transactionid',
  `eventid` varchar(128) NOT NULL COMMENT 'eventid 表示单条告警数据',
  `index_num` int(4) DEFAULT NULL COMMENT '序列号',
  `moni_result` int(1) DEFAULT NULL COMMENT '1 增加一个告警,2 消除一个告警',
  `monitor_source` varchar(128) DEFAULT NULL COMMENT '第三方将空系统编码',
  `monitor_type` int(1) DEFAULT NULL COMMENT '4.系统告警',
  `device_ip` varchar(128) DEFAULT NULL COMMENT '监控对象IP地址',
  `serv_system` varchar(128) DEFAULT NULL COMMENT '被监控对象所属的业务系统',
  `monitor_room` varchar(128) DEFAULT NULL COMMENT '被监控对象所属的机房',
  `monitor_object` varchar(128) DEFAULT NULL COMMENT '被监控目标对象',
  `monitor_index` varchar(128) DEFAULT NULL COMMENT '被监控指标',
  `alter_time` datetime DEFAULT NULL COMMENT '告警时间',
  `alter_level` int(1) DEFAULT NULL COMMENT '告警级别',
  `alter_des` varchar(1024) DEFAULT NULL COMMENT '告警描述',
  `cur_monitime` datetime DEFAULT NULL COMMENT '最近告警时间',
  `cur_monivalue` varchar(128) DEFAULT NULL COMMENT '最近告警值',
  `status` int(128) DEFAULT NULL COMMENT '0表示未发送，1表示发送成功',
  `process_time` datetime DEFAULT NULL COMMENT '发送时间',
  PRIMARY KEY (`transaction_id`,`eventid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `alarm_version` */

DROP TABLE IF EXISTS `alarm_version`;

CREATE TABLE `alarm_version` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(64) DEFAULT NULL COMMENT '名称',
  `version` int(11) DEFAULT NULL COMMENT '版本',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `name_2` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Table structure for table `monitor_metricsv2_dashboard` */

DROP TABLE IF EXISTS `monitor_metricsv2_dashboard`;

CREATE TABLE `monitor_metricsv2_dashboard` (
  `uuid` varchar(40) COLLATE utf8_bin NOT NULL,
  `region_id` varchar(40) COLLATE utf8_bin NOT NULL COMMENT '域ID',
  `namespace` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '命名空间',
  `dashboard_name` varchar(80) COLLATE utf8_bin NOT NULL COMMENT '面板名称',
  `display_name` varchar(80) COLLATE utf8_bin DEFAULT NULL COMMENT '显示名称',
  `orders` varchar(2048) COLLATE utf8_bin DEFAULT NULL,
  `created_at` bigint(20) NOT NULL COMMENT '创建时间',
  `updated_at` bigint(20) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='监控-维度-面板表';

/*Table structure for table `monitor_metricsv2_dashboardchart` */

DROP TABLE IF EXISTS `monitor_metricsv2_dashboardchart`;

CREATE TABLE `monitor_metricsv2_dashboardchart` (
  `uuid` varchar(40) COLLATE utf8_bin NOT NULL,
  `region_id` varchar(40) COLLATE utf8_bin NOT NULL COMMENT '域ID',
  `display_name` varchar(80) COLLATE utf8_bin DEFAULT NULL COMMENT '显示名称',
  `created_at` bigint(20) NOT NULL COMMENT '创建时间',
  `updated_at` bigint(20) NOT NULL COMMENT '更新时间',
  `dashboard_id` varchar(40) COLLATE utf8_bin NOT NULL COMMENT '面板ID',
  `metrics` text COLLATE utf8_bin COMMENT '将页面的监控指标  统计方式 分组 图表类型组装成一个json数组保存',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='监控-维度-面板图表';

/*Table structure for table `monitor_metricsv2_metric` */

DROP TABLE IF EXISTS `monitor_metricsv2_metric`;

CREATE TABLE `monitor_metricsv2_metric` (
  `uuid` varchar(40) COLLATE utf8_bin NOT NULL,
  `region_id` varchar(40) COLLATE utf8_bin NOT NULL COMMENT '域ID',
  `namespace` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '命名空间',
  `display_name` varchar(80) COLLATE utf8_bin DEFAULT NULL COMMENT '显示名称',
  `metric_name` varchar(2048) COLLATE utf8_bin NOT NULL COMMENT '维度名称',
  `created_at` bigint(20) NOT NULL COMMENT '创建时间',
  `updated_at` bigint(20) NOT NULL COMMENT '更新时间',
  `description` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '描述',
  `metric_type` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '数据类型,可选值gauge ，count，rate ;',
  `unit` varchar(2048) COLLATE utf8_bin DEFAULT NULL COMMENT '单元',
  `last_seen` bigint(20) DEFAULT NULL COMMENT '最后查看时间',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='监控-维度-字典表';

DROP PROCEDURE IF EXISTS pro_delete_alarms;
delimiter //
CREATE PROCEDURE `pro_delete_alarms`(
  IN i_namespace VARCHAR(64),
  IN i_alarm_id VARCHAR(64)
)
BEGIN
    DECLARE t_error INTEGER DEFAULT 0;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET t_error = 1;
    START TRANSACTION;
    IF i_alarm_id IS NULL THEN
        DELETE FROM alarm_observer WHERE `uuid` IN (SELECT `uuid` FROM alarm_config WHERE namespace = i_namespace) ;
        DELETE FROM alarm_action WHERE `uuid` IN (SELECT `uuid` FROM alarm_config WHERE namespace = i_namespace) ;
        DELETE FROM alarm_config WHERE namespace = i_namespace;
    ELSE
        DELETE FROM alarm_observer WHERE `uuid` = i_alarm_id;
        DELETE FROM alarm_action WHERE `uuid` = i_alarm_id;
        DELETE FROM alarm_config WHERE `uuid` = i_alarm_id;
    END IF;
    UPDATE alarm_version SET `version` = `version` + 1 WHERE `name` = 'migu';
    IF t_error = 1 THEN
       ROLLBACK;
    ELSE
       COMMIT;
    END IF;
END ;//
delimiter ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
