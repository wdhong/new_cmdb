drop table if exists notice_subscription;

/*==============================================================*/
/* Table: subscription                                          */
/*==============================================================*/
create table notice_subscription
(
   uuid                 varchar(36) not null comment '通知接收主键',
   method               varchar(16) not null comment 'email/webhook/sms 三种',
   recipient            varchar(2048) not null comment '收件人',
   notification_id      varchar(36) not null comment '链接到notification的外键',
   remark               varchar(256) comment '备注',
   secret               varchar(256) comment '密码webhook 有时会需要',
   primary key (uuid)
);

alter table notice_subscription comment '通知接收方表';


drop table if exists notice_notification;

/*==============================================================*/
/* Table: notification                                          */
/*==============================================================*/
create table notice_notification
(
   uuid                 varchar(36) not null comment '通知表主键',
   primary key (uuid)
);

alter table notice_notification comment '通知表';

/*==============================================================*/
/* Table: notice_send_utils_emailtrace                                          */
/*==============================================================*/
DROP TABLE IF EXISTS `notice_send_utils_emailtrace`;
CREATE TABLE `notice_send_utils_emailtrace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_type` varchar(128) NOT NULL COMMENT '邮件类型，比如触发报警',
  `recipient_list` text NOT NULL COMMENT '收件人列表',
  `language_code` varchar(32) NOT NULL COMMENT '语言，cn、en 等',
  `email_param` text NOT NULL COMMENT '邮件正文',
  `email_format` varchar(32) NOT NULL COMMENT '邮件格式，txt/html',
  `state` varchar(32) NOT NULL COMMENT '状态',
  `exception` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;