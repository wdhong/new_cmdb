DROP TABLE IF EXISTS `app_template`;
CREATE TABLE `app_template` (
  `uuid` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `namespace` varchar(30) NOT NULL,
  `description` text,
  `template` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `supported_platforms` varchar(200) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `space_uuid` varchar(50) DEFAULT NULL,
  `project_uuid` varchar(50) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `knamespace_uuid` varchar(50) DEFAULT NULL,
  `space_name` varchar(36) DEFAULT NULL,
  `project_name` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `config_config`;
CREATE TABLE `config_config` (
  `id` varchar(50) NOT NULL,
  `description` text,
  `current_version` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `config_configversion`;
CREATE TABLE `config_configversion` (
  `id` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `config` varchar(50) NOT NULL,
  `commit_message` text,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `envfile_envfile`;
CREATE TABLE `envfile_envfile` (
  `uuid` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `description` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;