#------------deploy_server database tables-----------#
DROP TABLE IF EXISTS `container_setting`;
CREATE TABLE IF NOT EXISTS `container_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `setting_key` varchar(255) DEFAULT NULL,
  `setting_value` tinytext,
  `component_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `component_version`;
CREATE TABLE IF NOT EXISTS `component_version` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(50) DEFAULT NULL,
  `component_name` varchar(255) DEFAULT NULL,
  `default_version` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into component_version(version,component_name,default_version) values('20180601','pudge','1');
insert into component_version(version,component_name,default_version) values('5fdac17841ef5b1b8147c1a860fc4a8c0fb85318', 'dd_agent','1');
insert into component_version(version,component_name,default_version) values('fddbe02a55f1697c6fe6d0113ea5916ea016da3b', 'dd_server','1');
insert into component_version(version,component_name,default_version) values('2.2.0-dev','logcollect','1');
insert into component_version(version,component_name,default_version) values('c3d2ed117d35c132002318f9c85468773d962298', 'registry','1');
insert into component_version(version,component_name,default_version) values('20180518', 'doom','1');
insert into component_version(version,component_name,default_version) values('be5e1da3b618c90bf6922f60aa1e65dc3fc2745d', 'haproxy','1');
insert into component_version(version,component_name,default_version) values('v1', 'controller_haproxy','1');
insert into component_version(version,component_name,default_version) values('3d2938670adca9da3b79eb5841d5075bf85a6a35', 'keepalived','1');
insert into component_version(version,component_name,default_version) values('1204', 'kubernetes_init','1');
insert into component_version(version,component_name,default_version) values('1204', 'kubernetes_compute','1');
insert into component_version(version,component_name,default_version) values('0042543acae28dec2effdd5c5f8859e6efe75563', 'puck','1');
insert into component_version(version,component_name,default_version) values('k8s', 'etcd','1');
insert into component_version(version,component_name,default_version) values('36493e662b15e3a52abd0eff5484f8371b93f15a', 'alb_haproxy','1');
insert into component_version(version,component_name,default_version) values('v1.0.1', 'coil_client', '1');
insert into component_version(version,component_name,default_version) values('6f99e52319ba54b0cafb63fe26bf0d8693c0978b', 'xin_driver', '1');