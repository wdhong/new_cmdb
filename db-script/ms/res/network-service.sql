/*
Navicat MySQL Data Transfer

Source Server         : 172.30.150.50
Source Server Version : 50718
Source Host           : 172.30.150.50:3306
Source Database       : network

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2018-01-08 16:56:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for snapshots
-- ----------------------------
-- DROP TABLE IF EXISTS `snapshots`;
-- CREATE TABLE `snapshots` (
--  `id` varchar(64) NOT NULL,
--  `region_id` varchar(64) NOT NULL,
--  `name` varchar(256) NOT NULL,
--  `size` int(11) NOT NULL,
--  `driver_name` varchar(128) DEFAULT NULL,
--  `volume_name` varchar(256) NOT NULL,
--  `volume_id` varchar(64) NOT NULL,
--  `driver_snapshot_id` varchar(128) DEFAULT NULL,
--  `volume_type` varchar(64) DEFAULT NULL,
--  `state` varchar(64) DEFAULT NULL,
--  `created_at` datetime DEFAULT NULL,
--  PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for storage_pvc
-- ----------------------------
DROP TABLE IF EXISTS `storage_pvc`;
CREATE TABLE `storage_pvc` (
  `id` varchar(64) NOT NULL,
  `name` varchar(256) NOT NULL,
  `size` int(11) NOT NULL,
  `driver_name` varchar(128) DEFAULT NULL,
  `snapshot_id` varchar(128) DEFAULT NULL,
  `state` varchar(64) NOT NULL,
  `knamespace` varchar(128) NOT NULL,
  `region_id` varchar(64) NOT NULL,
  `errormsg` varchar(2048) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
