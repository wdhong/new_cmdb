#------------deploy_proxy database tables-----------#
DROP TABLE IF EXISTS `auth_user`;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `is_superuser` varchar(5) NOT NULL DEFAULT 'f',
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL DEFAULT '',
  `last_name` varchar(30) NOT NULL DEFAULT '',
  `email` varchar(254) NOT NULL DEFAULT '',
  `is_staff` varchar(5) NOT NULL DEFAULT 'f',
  `is_active` varchar(5) NOT NULL DEFAULT 't',
  `date_joined` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `auth_user_username_51b3b110094b8aae_like` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1199 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `authtoken_token`;
CREATE TABLE `authtoken_token` (
  `key` varchar(40) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`key`),
  KEY `authtoken_token_key_7222ec672cd32dcd_like` (`key`) USING BTREE,
  KEY `fk_authtoken_user_id` (`user_id`),
  CONSTRAINT `fk_authtoken_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `controller_spectre`;
CREATE TABLE `controller_spectre` (
  `version_uuid` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '版本id',
  `version` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '版本',
  `image` varchar(512) COLLATE utf8_bin NOT NULL COMMENT '镜像名称',
  `description` varchar(512) COLLATE utf8_bin DEFAULT NULL COMMENT '描述',
  `username` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '用户名',
  `password` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '密码',
  `is_latest` char(1) COLLATE utf8_bin NOT NULL DEFAULT 'f' COMMENT '是否为默认',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`version_uuid`),
  KEY `version_uuid` (`version_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='记录spectre版本信息';

DROP TABLE IF EXISTS `controller_environment`;
CREATE TABLE `controller_environment` (
  `env_uuid` varchar(50) COLLATE utf8_bin NOT NULL COMMENT 'uuid',
  `reg_token` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '节点注册所用token',
  `description` varchar(512) COLLATE utf8_bin DEFAULT NULL COMMENT '环境描述',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `spectre_id` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '所使用的spectre版本id',
  PRIMARY KEY (`env_uuid`),
  KEY `fk_env_spectre_id` (`spectre_id`),
  CONSTRAINT `fk_env_spectre_id` FOREIGN KEY (`spectre_id`) REFERENCES `controller_spectre` (`version_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='记录环境信息';

DROP TABLE IF EXISTS `controller_node`;
CREATE TABLE `controller_node` (
  `node_uuid` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '节点id',
  `goal_incarnation` int(11) NOT NULL DEFAULT '1' COMMENT '目标版本id',
  `ip_address` varchar(15) COLLATE utf8_bin NOT NULL COMMENT 'ip地址',
  `current_incarnation` int(11) DEFAULT NULL COMMENT '当前版本信息',
  `health_state` varchar(50) COLLATE utf8_bin NOT NULL DEFAULT 'UNKNOWN' COMMENT '健康状态',
  `status` varchar(50) COLLATE utf8_bin NOT NULL DEFAULT 'UNKNOWN' COMMENT '节点状态',
  `description` varchar(512) COLLATE utf8_bin DEFAULT NULL COMMENT '描述信息',
  `last_goalstate_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '最后在目前状态的更新时间',
  `last_heartbeat_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '最后一次心跳时间',
  `env_uuid_id` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '所属环境id',
  `node_type` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '节点类型',
  `first_unhealthy_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '首次不健康的时间',
  `failure_info` text COLLATE utf8_bin NOT NULL COMMENT '错误信息',
  PRIMARY KEY (`node_uuid`),
  KEY `fk_node_env_uuid` (`env_uuid_id`),
  CONSTRAINT `fk_node_env_uuid` FOREIGN KEY (`env_uuid_id`) REFERENCES `controller_environment` (`env_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='记录节点信息';

DROP TABLE IF EXISTS `controller_resource`;
CREATE TABLE `controller_resource` (
  `resource_uuid` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '资源id',
  `name` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '资源名',
  `type` varchar(16) COLLATE utf8_bin NOT NULL COMMENT '资源类型',
  `identifier` text COLLATE utf8_bin NOT NULL COMMENT '资源标识',
  `configuration` text COLLATE utf8_bin NOT NULL COMMENT '资源其余配置信息',
  `incarnation` int(11) NOT NULL DEFAULT '0' COMMENT '资源版本信息',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `env_uuid_id` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '所属环境id',
  PRIMARY KEY (`resource_uuid`),
  KEY `fk_resource_env_uuid` (`env_uuid_id`),
  CONSTRAINT `fk_resource_env_uuid` FOREIGN KEY (`env_uuid_id`) REFERENCES `controller_environment` (`env_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='记录资源信息';

DROP TABLE IF EXISTS `controller_noderesourcemap`;
CREATE TABLE `controller_noderesourcemap` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `node_uuid_id` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '节点id',
  `resource_uuid_id` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '资源id',
  PRIMARY KEY (`id`),
  KEY `fk_map_node_uuid` (`node_uuid_id`),
  KEY `fk_map_resource_uuid` (`resource_uuid_id`),
  CONSTRAINT `fk_map_node_uuid` FOREIGN KEY (`node_uuid_id`) REFERENCES `controller_node` (`node_uuid`),
  CONSTRAINT `fk_map_resource_uuid` FOREIGN KEY (`resource_uuid_id`) REFERENCES `controller_resource` (`resource_uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=970 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='资源和节点之间的映射关系';

INSERT INTO `auth_user` VALUES ('1', 'pbkdf2_sha256$20000$tohQ1xhMKjfa$Qcdt/Wi0Zdhcd9OKMv1Scp13aE59lEEjfUJy36F5Tf4=', null, 't', 'alauda', '', '', 'alauda.alauda.io', 't', 't', '2017-10-29 02:46:41');
INSERT INTO `authtoken_token` VALUES ('1f0759fe291d591ea399732a6135173e80795152', '2017-10-29 02:46:41', '1');
INSERT INTO `controller_spectre` VALUES ('b7c44cd6-c6c4-4707-9088-e67fe01386aq', '2.91', '${ONEBOX_IP}:60080/claas/spectre:2.91', 'Support kubernetes', '', '', 't', '2017-10-31 13:23:47', '2017-09-28 08:12:03');