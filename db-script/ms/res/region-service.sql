/*
Navicat MySQL Data Transfer

Source Server         : 172.30.150.141_res
Source Server Version : 50718
Source Host           : 172.30.150.141:3306
Source Database       : res

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2018-01-10 20:14:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for app_region
-- ----------------------------
DROP TABLE IF EXISTS `app_region`;
CREATE TABLE `app_region` (
  `id` varchar(36) NOT NULL,
  `name` varchar(36) NOT NULL,
  `display_name` varchar(36) NOT NULL,
  `namespace` varchar(36) NOT NULL,
  `state` varchar(16) NOT NULL,
  `attr` longtext NOT NULL,
  `features` longtext NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `env_uuid` varchar(36) DEFAULT NULL,
  `container_manager` varchar(16) NOT NULL,
  `zone_uuid` varchar(50) DEFAULT NULL,
  `meta_info` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_region_namespace_6cb135207aaa6111_uniq` (`namespace`,`name`),
  KEY `app_region_namespace_6cb135207aaa6111_idx` (`namespace`,`name`),
  KEY `app_region_08566bbf` (`env_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for app_zone
-- ----------------------------
DROP TABLE IF EXISTS `app_zone`;
CREATE TABLE `app_zone` (
  `zone_uuid` varchar(50) NOT NULL,
  `zone_name` varchar(36) NOT NULL,
  `endpoint` varchar(128) NOT NULL,
  `description` varchar(128) DEFAULT NULL,
  `meta_info` text,
  PRIMARY KEY (`zone_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for app_regionsetting
-- ----------------------------
DROP TABLE IF EXISTS `app_regionsetting`;
CREATE TABLE `app_regionsetting` (
  `id` varchar(36) NOT NULL,
  `master_node` longtext NOT NULL,
  `slave_node` longtext NOT NULL,
  `region_location` varchar(64) NOT NULL,
  `image_location` varchar(64) NOT NULL,
  `user_settings` longtext NOT NULL,
  `user_registries` longtext NOT NULL,
  `cluster_settings` longtext NOT NULL,
  `cluster_template_type` varchar(16) NOT NULL,
  `is_update_blocked` tinyint(1) NOT NULL,
  `region_id` varchar(36) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `app_regionsetting_region_id_3e14ca4bb3adf67b_fk_app_region_id` (`region_id`),
  CONSTRAINT `app_regionsetting_region_id_3e14ca4bb3adf67b_fk_app_region_id` FOREIGN KEY (`region_id`) REFERENCES `app_region` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for app_componentversion
-- ----------------------------
DROP TABLE IF EXISTS `app_componentversion`;
CREATE TABLE `app_componentversion` (
  `id` varchar(50) NOT NULL,
  `name` varchar(36) NOT NULL,
  `version` varchar(64) NOT NULL,
  `incarnation` int(11) NOT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `region_settings_id` varchar(36) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_componentversion_region_settings_id_4123ab8800dd2606_uniq` (`region_settings_id`,`name`),
  KEY `app_componentversion_region_settings_id_4123ab8800dd2606_idx` (`region_settings_id`,`name`),
  KEY `app_componentversion_ae406d2d` (`region_settings_id`),
  CONSTRAINT `app__region_settings_id_36fbeab70caf9efa_fk_app_regionsetting_id` FOREIGN KEY (`region_settings_id`) REFERENCES `app_regionsetting` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for app_knamespace
-- ----------------------------
DROP TABLE IF EXISTS `app_knamespace`;
CREATE TABLE `app_knamespace` (
  `uuid` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `region_id` varchar(50) NOT NULL,
  `description` varchar(4000) DEFAULT NULL,
  `status` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `quota` text NOT NULL,
  `label` text,
  `project_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`uuid`),
  KEY `region_id` (`region_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for app_node
-- ----------------------------
DROP TABLE IF EXISTS `app_node`;
CREATE TABLE `app_node` (
  `id` varchar(36) NOT NULL,
  `private_ip` varchar(32) NOT NULL,
  `instance_id` varchar(128) DEFAULT NULL,
  `state` varchar(16) NOT NULL,
  `attr` longtext,
  `region_id` varchar(36) NOT NULL,
  `resources` longtext,
  `type` varchar(16) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_node_region_id_721aa0a9787a9cb6_uniq` (`region_id`,`private_ip`),
  KEY `app_node_region_id_721aa0a9787a9cb6_idx` (`region_id`,`private_ip`),
  CONSTRAINT `app_node_region_id_6076eb03984b4a4_fk_app_region_id` FOREIGN KEY (`region_id`) REFERENCES `app_region` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for app_portpool
-- ----------------------------
DROP TABLE IF EXISTS `app_portpool`;
CREATE TABLE `app_portpool` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `port` int(11) NOT NULL,
  `used` tinyint(1) NOT NULL,
  `public_ip` varchar(32) DEFAULT NULL,
  `private_ip` varchar(32) DEFAULT NULL,
  `region_name` varchar(64) DEFAULT NULL,
  `type` varchar(64) NOT NULL,
  `namespace` varchar(36) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_portpool_public_ip_173c2b9679410494_uniq` (`public_ip`,`private_ip`,`port`),
  KEY `app_portpool_2509e1ea` (`public_ip`),
  KEY `app_portpool_599dcce2` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=312337 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for app_stats
-- ----------------------------
DROP TABLE IF EXISTS `app_stats`;
CREATE TABLE `app_stats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL,
  `data` longtext NOT NULL,
  `region_id` varchar(36) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_stats_region_id_4e6e6288b88498d3_uniq` (`region_id`,`type`),
  KEY `app_stats_region_id_4e6e6288b88498d3_idx` (`region_id`,`type`),
  CONSTRAINT `app_stats_region_id_5ac2e684fe34a888_fk_app_region_id` FOREIGN KEY (`region_id`) REFERENCES `app_region` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7001', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7002', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7003', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7004', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7005', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7006', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7007', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7008', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7009', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7010', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7011', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7012', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7013', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7014', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7015', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7016', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7017', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7018', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7019', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7020', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7021', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7022', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7023', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7024', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7025', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7026', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7027', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7028', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7029', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7030', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7031', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7032', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7033', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7034', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7035', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7036', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7037', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7038', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7039', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7040', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7041', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7042', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7043', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7044', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7045', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7046', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7047', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7048', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7049', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7050', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'SSH', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7051', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7052', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7053', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7054', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7055', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7056', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7057', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7058', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7059', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7060', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7061', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7062', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7063', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7064', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7065', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7066', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7067', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7068', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7069', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7070', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7071', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7072', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7073', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7074', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7075', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7076', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7077', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7078', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7079', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7080', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7081', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7082', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7083', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7084', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7085', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7086', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7087', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7088', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7089', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7090', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7091', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7092', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7093', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7094', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7095', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7096', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7097', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7098', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7099', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7100', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7101', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7102', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7103', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7104', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7105', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7106', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7107', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7108', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7109', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7110', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7111', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7112', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7113', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7114', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7115', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7116', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7117', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7118', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7119', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7120', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7121', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7122', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7123', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7124', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7125', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7126', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7127', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7128', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7129', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7130', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7131', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7132', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7133', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7134', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7135', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7136', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7137', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7138', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7139', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7140', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7141', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7142', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7143', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7144', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7145', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7146', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7147', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7148', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7149', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7150', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7151', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7152', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7153', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7154', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7155', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7156', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7157', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7158', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7159', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7160', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7161', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7162', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7163', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7164', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7165', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7166', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7167', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7168', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7169', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7170', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7171', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7172', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7173', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7174', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7175', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7176', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7177', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7178', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7179', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7180', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7181', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7182', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7183', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7184', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7185', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7186', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7187', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7188', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7189', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7190', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7191', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7192', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7193', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7194', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7195', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7196', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7197', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7198', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7199', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7200', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7201', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7202', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7203', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7204', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7205', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7206', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7207', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7208', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7209', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7210', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7211', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7212', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7213', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7214', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7215', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7216', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7217', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7218', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7219', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7220', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7221', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7222', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7223', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7224', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7225', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7226', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7227', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7228', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7229', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7230', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7231', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7232', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7233', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7234', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7235', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7236', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7237', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7238', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7239', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7240', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7241', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7242', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7243', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7244', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7245', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7246', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7247', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7248', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7249', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7250', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7251', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7252', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7253', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7254', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7255', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7256', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7257', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7258', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7259', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7260', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7261', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7262', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7263', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7264', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7265', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7266', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7267', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7268', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7269', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7270', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7271', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7272', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7273', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7274', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7275', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7276', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7277', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7278', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7279', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7280', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7281', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7282', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7283', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7284', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7285', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7286', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7287', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7288', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7289', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7290', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7291', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7292', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7293', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7294', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7295', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7296', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7297', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7298', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7299', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
INSERT into app_portpool(port, used, public_ip, private_ip, region_name, type, namespace) values('7300', 0, '${ONEBOX_IP}', '${ONEBOX_IP}', '', 'Mapping', '');
