DROP TABLE IF EXISTS `app_service`;
DROP TABLE IF EXISTS `app_app`;
CREATE TABLE `app_app` (
  `id` VARCHAR(50) NOT NULL,
  `unique_name` VARCHAR(50) NOT NULL,
  `region` VARCHAR(255) NOT NULL,
  `created_by` VARCHAR(255) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME DEFAULT NULL,
  `deploy` TEXT,
  `status` VARCHAR(50) NOT NULL,
  `appbody` VARCHAR(2000) DEFAULT NULL,
  `knamespace_uuid` VARCHAR(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_name` (`unique_name`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `service_volume`;

DROP TABLE IF EXISTS `service_rawcontainerendpoint`;
DROP TABLE IF EXISTS `service_mountpoint_service_version`;
DROP TABLE IF EXISTS `service_mountpoint`;
DROP TABLE IF EXISTS `service_link`;
DROP TABLE IF EXISTS `service_envvar_service_version`;
DROP TABLE IF EXISTS `service_envvar`;
DROP TABLE IF EXISTS `service_envfile_service_version`;
DROP TABLE IF EXISTS `service_envfile`;
DROP TABLE IF EXISTS `service_serviceversion`;
DROP TABLE IF EXISTS `service_service`;

CREATE TABLE `service_service` (
  `uuid` VARCHAR(50) CHARACTER SET latin1 NOT NULL,
  `region_id` VARCHAR(50) CHARACTER SET latin1 NOT NULL,
  `target_state` VARCHAR(16) CHARACTER SET latin1 NOT NULL,
  `network_mode` VARCHAR(50) CHARACTER SET latin1 NOT NULL,
  `started_at` DATETIME NOT NULL,
  `stopped_at` DATETIME NOT NULL,
  `mirana_domain_name` VARCHAR(255) CHARACTER SET latin1 DEFAULT NULL,
  `status` VARCHAR(50) CHARACTER SET latin1 NOT NULL,
  `created_by` VARCHAR(64) CHARACTER SET latin1 DEFAULT NULL,
  `creation_datetime` DATETIME NOT NULL,
  `last_update_datetime` DATETIME DEFAULT NULL,
  `custom_domain_name` VARCHAR(255) CHARACTER SET latin1 DEFAULT NULL,
  `current_version_id` VARCHAR(50) CHARACTER SET latin1 DEFAULT NULL,
  `previous_version_id` VARCHAR(50) CHARACTER SET latin1 DEFAULT NULL,
  `container_manager` VARCHAR(16) CHARACTER SET latin1 NOT NULL,
  `service_name` VARCHAR(128) CHARACTER SET latin1 NOT NULL,
  `space_uuid` VARCHAR(50) CHARACTER SET latin1 DEFAULT NULL,
  `subnet_id` VARCHAR(50) CHARACTER SET latin1 DEFAULT NULL,
  `app_name` VARCHAR(128) CHARACTER SET latin1 DEFAULT NULL,
  `space_name` VARCHAR(128) CHARACTER SET latin1 DEFAULT NULL,
  `project_id` VARCHAR(50) CHARACTER SET latin1 NOT NULL,
  `pod_controller` VARCHAR(16) CHARACTER SET latin1 DEFAULT NULL,
  `pod_labels` TEXT CHARACTER SET latin1,
  `knamespace_uuid` VARCHAR(50) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`uuid`),
  UNIQUE KEY `service_name` (`service_name`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE `service_serviceversion` (
  `id` VARCHAR(50) NOT NULL,
  `image_name` TEXT NOT NULL,
  `image_tag` VARCHAR(256) NOT NULL,
  `instance_size` VARCHAR(16) NOT NULL,
  `target_num_instances` INT(10) NOT NULL,
  `run_command` VARCHAR(1024) DEFAULT NULL,
  `entrypoint` VARCHAR(1024) DEFAULT NULL,
  `scaling_mode` VARCHAR(16) NOT NULL,
  `autoscaling_config` TEXT,
  `service_id` VARCHAR(50) NOT NULL,
  `custom_instance_size` TEXT,
  `health_checks` TEXT,
  `node_tag` VARCHAR(50) DEFAULT NULL,
  `node_alone` VARCHAR(16) DEFAULT NULL,
  `network_port_type` VARCHAR(16) DEFAULT NULL,
  `serv_status_type` VARCHAR(16) DEFAULT NULL,
  `entry_point_type` VARCHAR(16) NOT NULL,
  `healthy_num_instances` INT(10) DEFAULT NULL,
  `node_selector` TEXT,
  `pod_affinity` TEXT,
  `pod_anti_affinity` TEXT,
  `update_strategy` VARCHAR(16) DEFAULT NULL,
  `tolerations` VARCHAR(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `service_id` (`service_id`),
  CONSTRAINT `service_serviceversion_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `service_service` (`uuid`) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `epm_process`;
CREATE TABLE `epm_process` (
  `process_id` VARCHAR(40) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `parent_id` VARCHAR(40) DEFAULT NULL,
  `root_id` VARCHAR(40) NOT NULL,
  `status` VARCHAR(20) NOT NULL,
  `deal_type` VARCHAR(20) DEFAULT NULL,
  `pre_status` VARCHAR(30) DEFAULT NULL,
  `create_time` DATETIME NOT NULL,
  `last_update_time` DATETIME DEFAULT NULL,
  `owner_id` VARCHAR(40) NOT NULL,
  `define_content` TEXT,
  PRIMARY KEY (`process_id`),
  KEY `epm_process_index` (`name`,`owner_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `epm_task`;
CREATE TABLE `epm_task` (
  `task_id` VARCHAR(40) NOT NULL,
  `process_id` VARCHAR(40) DEFAULT NULL,
  `parent_id` VARCHAR(40) DEFAULT NULL,
  `root_id` VARCHAR(40) DEFAULT NULL,
  `role` VARCHAR(4000) DEFAULT NULL,
  `viewer` VARCHAR(4000) DEFAULT NULL,
  `owner_type` VARCHAR(30) NOT NULL,
  `owner_id` VARCHAR(40) NOT NULL,
  `dealer` VARCHAR(40) DEFAULT NULL,
  `choice` VARCHAR(100) DEFAULT NULL,
  `create_time` DATETIME NOT NULL,
  `deal_time` DATETIME DEFAULT NULL,
  `status` VARCHAR(30) DEFAULT NULL,
  `deal_result` CHAR(1) DEFAULT NULL,
  `file_group_id` VARCHAR(40) DEFAULT NULL,
  `remark` VARBINARY(5000) DEFAULT NULL,
  `is_deal` CHAR(1) DEFAULT '0',
  `is_finish` CHAR(1) DEFAULT '0',
  `deal_type` CHAR(2) DEFAULT NULL,
  `reviewer` VARCHAR(100) DEFAULT NULL,
  `extra1` VARCHAR(100) DEFAULT NULL,
  `extra2` VARCHAR(100) DEFAULT NULL,
  `extra3` VARCHAR(100) DEFAULT NULL,
  PRIMARY KEY (`task_id`),
  KEY `epm_task_index` (`owner_type`,`owner_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

CREATE TABLE `app_service` (
  `id` VARCHAR(50) NOT NULL,
  `unique_name` VARCHAR(255) NOT NULL,
  `name` VARCHAR(128) NOT NULL,
  `appid` VARCHAR(50) NOT NULL,
  `meta` TEXT,
  `status` VARCHAR(128) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `appid` (`appid`),
  CONSTRAINT `app_service_ibfk_1` FOREIGN KEY (`appid`) REFERENCES `app_app` (`id`) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

CREATE TABLE `service_envfile` (
  `id` VARCHAR(50) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `content` TEXT NOT NULL,
  `index` INT(11) NOT NULL,
  `service_id` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `service_id` (`service_id`),
  CONSTRAINT `service_envfile_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `service_service` (`uuid`) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

CREATE TABLE `service_envfile_service_version` (
  `id` VARCHAR(50) NOT NULL,
  `envfile_id` VARCHAR(50) NOT NULL,
  `serviceversion_id` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `serviceversion_id` (`serviceversion_id`),
  CONSTRAINT `service_envfile_service_version_ibfk_1` FOREIGN KEY (`serviceversion_id`) REFERENCES `service_serviceversion` (`id`) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

CREATE TABLE `service_envvar` (
  `id` VARCHAR(50) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `value` VARCHAR(255) NOT NULL,
  `service_id` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `service_id` (`service_id`),
  CONSTRAINT `service_envvar_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `service_service` (`uuid`) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

CREATE TABLE `service_envvar_service_version` (
  `id` VARCHAR(50) NOT NULL,
  `envvar_id` VARCHAR(50) NOT NULL,
  `serviceversion_id` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `envvar_id` (`envvar_id`),
  KEY `serviceversion_id` (`serviceversion_id`),
  CONSTRAINT `service_envvar_service_version_ibfk_1` FOREIGN KEY (`envvar_id`) REFERENCES `service_envvar` (`id`) ON DELETE CASCADE,
  CONSTRAINT `service_envvar_service_version_ibfk_2` FOREIGN KEY (`serviceversion_id`) REFERENCES `service_serviceversion` (`id`) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

CREATE TABLE `service_link` (
  `id` VARCHAR(50) NOT NULL,
  `dest_alias` VARCHAR(255) NOT NULL,
  `dest` VARCHAR(50) NOT NULL,
  `src` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dest` (`dest`),
  KEY `src` (`src`),
  CONSTRAINT `service_link_ibfk_1` FOREIGN KEY (`dest`) REFERENCES `service_service` (`uuid`) ON DELETE CASCADE,
  CONSTRAINT `service_link_ibfk_2` FOREIGN KEY (`src`) REFERENCES `service_service` (`uuid`) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

CREATE TABLE `service_mountpoint` (
  `id` VARCHAR(50) NOT NULL,
  `volume_name` VARCHAR(128) DEFAULT NULL,
  `type` VARCHAR(128) NOT NULL,
  `path` TEXT NOT NULL,
  `value` TEXT NOT NULL,
  `service_id` VARCHAR(50) NOT NULL,
  `deleted` INT(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `service_id` (`service_id`),
  CONSTRAINT `service_mountpoint_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `service_service` (`uuid`) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

CREATE TABLE `service_mountpoint_service_version` (
  `id` VARCHAR(50) NOT NULL,
  `mountpoint_id` VARCHAR(50) NOT NULL,
  `serviceversion_id` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mountpoint_id` (`mountpoint_id`),
  KEY `serviceversion_id` (`serviceversion_id`),
  CONSTRAINT `service_mountpoint_service_version_ibfk_1` FOREIGN KEY (`mountpoint_id`) REFERENCES `service_mountpoint` (`id`) ON DELETE CASCADE,
  CONSTRAINT `service_mountpoint_service_version_ibfk_2` FOREIGN KEY (`serviceversion_id`) REFERENCES `service_serviceversion` (`id`) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

CREATE TABLE `service_rawcontainerendpoint` (
  `id` VARCHAR(50) NOT NULL,
  `container_port` INT(11) NOT NULL,
  `host_port` INT(11) NOT NULL,
  `protocol` VARCHAR(50) NOT NULL,
  `service_id` VARCHAR(50) NOT NULL,
  `nodeport` INT(11) NOT NULL,
  `lb_type` VARCHAR(36) NOT NULL,
  `output_addr` VARCHAR(36) NOT NULL,
  `type` VARCHAR(20) DEFAULT NULL,
  `name` VARCHAR(100) DEFAULT NULL,
  `region_id` VARCHAR(50) DEFAULT NULL,
  `editable` TINYINT(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `service_id` (`service_id`),
  CONSTRAINT `service_rawcontainerendpoint_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `service_service` (`uuid`) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;


CREATE TABLE `service_volume` (
  `id` VARCHAR(50) NOT NULL,
  `volume_name` VARCHAR(128) NOT NULL,
  `app_volume_dir` VARCHAR(255) NOT NULL,
  `volume_id` VARCHAR(64) NOT NULL,
  `service_id` VARCHAR(50) NOT NULL,
  `region_id` VARCHAR(64) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `service_id` (`service_id`),
  CONSTRAINT `service_volume_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `service_service` (`uuid`) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1
