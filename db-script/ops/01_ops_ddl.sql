-- --------------------------------------------------------
-- 主机:                           10.12.70.40
-- 服务器版本:                        5.7.26-log - MySQL Community Server (GPL)
-- 服务器操作系统:                      Linux
-- HeidiSQL 版本:                  9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

use deploy_proxy;

-- 导出  表 deploy_proxy.ops_account 结构
CREATE TABLE IF NOT EXISTS `ops_account` (
  `account_name` varchar(30) NOT NULL COMMENT '用户名',
  `creater` varchar(30) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_auto_repair_execute_log 结构
CREATE TABLE IF NOT EXISTS `ops_auto_repair_execute_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `scheme_id` bigint(20) DEFAULT NULL COMMENT '自愈方案id',
  `scheme_name` varchar(128) DEFAULT NULL COMMENT '方案名称',
  `current_pipeline_order` int(20) DEFAULT NULL COMMENT '当前作业的顺序号',
  `current_pipeline_instance_id` bigint(20) DEFAULT NULL COMMENT '当前的运行作业实例id',
  `current_pipeline_instance_name` varchar(128) DEFAULT NULL COMMENT '当前正运行的作业实例name',
  `executed_pipe_inst_id_list` tinytext COMMENT '已执行作业实例id',
  `executed_pipe_inst_name_list` tinytext COMMENT '已执行作业实例name',
  `source_mark` varchar(10) DEFAULT NULL COMMENT '来源mark： xj: 巡检  gj: 告警',
  `ap_item_type` varchar(64) DEFAULT NULL COMMENT '自愈指标类型',
  `ap_item_type_name` varchar(128) DEFAULT NULL COMMENT '自愈指标类型name',
  `ap_item` varchar(64) DEFAULT NULL COMMENT '自愈指标',
  `ap_item_name` varchar(128) DEFAULT NULL COMMENT '自愈指标name',
  `asset_pool` varchar(64) DEFAULT NULL COMMENT 'cmdb设备资源池',
  `agent_ip` varchar(18) DEFAULT NULL,
  `extend_data` text COMMENT '扩展数据',
  `trigger_time` datetime DEFAULT NULL COMMENT '触发时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  `total_time` float DEFAULT NULL COMMENT '耗时(s)',
  `status` int(11) DEFAULT NULL COMMENT '操作结果状态',
  `exit_code` int(11) DEFAULT NULL COMMENT '执行返回码  0 正常   非0  执行存在错误',
  `response` longtext COMMENT '执行响应结果',
  `aspnode_result` tinyint(4) DEFAULT NULL COMMENT 'aspnode result： 0 成功  1 失败',
  `aspnode_msg` tinytext COMMENT 'aspnode message ',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_autorepair_currPipeInst` (`current_pipeline_instance_id`),
  KEY `uk_autorepair_result` (`scheme_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1010 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_auto_repair_item 结构
CREATE TABLE IF NOT EXISTS `ops_auto_repair_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ap_item_type_id` bigint(20) DEFAULT NULL COMMENT '自愈指标类型id',
  `ap_item` varchar(64) DEFAULT NULL COMMENT '自愈指标',
  `ap_item_name` varchar(128) DEFAULT NULL COMMENT '自愈指标name',
  `ap_item_value_type` int(11) DEFAULT NULL COMMENT '自愈监控项值类型',
  `ap_item_value_unit` varchar(32) DEFAULT NULL COMMENT '指标值单位',
  `extend_attrs` tinytext COMMENT '扩展属性',
  `first_sync_time` datetime DEFAULT NULL COMMENT '初次同步时间',
  `last_update_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_ap_item` (`ap_item_type_id`,`ap_item`)
) ENGINE=InnoDB AUTO_INCREMENT=182 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_auto_repair_item_type 结构
CREATE TABLE IF NOT EXISTS `ops_auto_repair_item_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ap_item_type` varchar(64) DEFAULT NULL COMMENT '自愈指标类型',
  `ap_item_type_name` varchar(128) DEFAULT NULL COMMENT '自愈指标类型name',
  `source_mark` varchar(10) DEFAULT NULL COMMENT '来源mark： xj: 巡检  gj: 告警',
  `first_sync_time` datetime DEFAULT NULL COMMENT '初次同步时间',
  `last_update_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_ap_item_type` (`ap_item_type`,`source_mark`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_auto_repair_scheme 结构
CREATE TABLE IF NOT EXISTS `ops_auto_repair_scheme` (
  `scheme_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `scheme_name` varchar(128) DEFAULT NULL COMMENT '自愈方案name',
  `multi_items_apply_time` int(11) DEFAULT NULL COMMENT '多个自愈监控项同时匹配时的有效时间跨度 (分钟)',
  `refer_pipeline_count` int(11) DEFAULT NULL COMMENT '关联的作业数量',
  `description` varchar(128) DEFAULT NULL COMMENT '描述',
  `creater` varchar(30) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `updater` varchar(30) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`scheme_id`),
  UNIQUE KEY `uk_ops_scheme_name` (`scheme_name`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_auto_repair_scheme_item 结构
CREATE TABLE IF NOT EXISTS `ops_auto_repair_scheme_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `scheme_id` bigint(20) DEFAULT NULL COMMENT '方案id',
  `ap_item_type_id` bigint(20) DEFAULT NULL COMMENT '自愈指标类型id',
  `ap_item` varchar(64) DEFAULT NULL COMMENT '自愈指标',
  `judge_symbol` varchar(64) DEFAULT NULL COMMENT '判断符号  <, <=, ==, >=, >, contain',
  `judge_value` varchar(30) DEFAULT NULL COMMENT '判断值',
  `ord` int(11) DEFAULT NULL COMMENT '排序号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_scheme_item` (`ap_item_type_id`,`ap_item`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_auto_repair_scheme_pipeline 结构
CREATE TABLE IF NOT EXISTS `ops_auto_repair_scheme_pipeline` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `scheme_id` bigint(20) DEFAULT NULL COMMENT '自愈方案id',
  `pipeline_id` bigint(20) DEFAULT NULL COMMENT '自愈方案关联的作业id',
  `finish_judge_type` varchar(20) DEFAULT NULL COMMENT '当前作业执行完成后的后续作业的判断类型:  exec_status|aspnode_result|aspnode_msg',
  `finish_judge_value` varchar(128) DEFAULT NULL COMMENT '作业完成后后续动作判断值',
  `finish_judge_action` int(11) DEFAULT NULL COMMENT '匹配到执行完成状态后的动作  0: 自动终止  1：继续执行  2： 人工干预',
  `ord` int(11) DEFAULT NULL COMMENT '排序号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_scheme_pipeline` (`scheme_id`,`pipeline_id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_file 结构
CREATE TABLE IF NOT EXISTS `ops_file` (
  `file_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '序列号',
  `file_name` varchar(128) NOT NULL COMMENT '文件名称',
  `file_version` varchar(128) NOT NULL COMMENT '文件版本',
  `file_type` varchar(10) NOT NULL COMMENT '文件类型 1直接使用、2拆分文件、3加密文件',
  `file_generate_type` varchar(10) NOT NULL COMMENT '生成类型1本地上传/2自动生成',
  `file_class` varchar(10) DEFAULT NULL COMMENT '文件分类 1基线文件/2账号文件/3巡检报告文件/4汇总结果保存文件/5日志文件/9其他',
  `file_path` varchar(512) DEFAULT NULL COMMENT '文件地址',
  `file_desc` varchar(512) DEFAULT NULL COMMENT '文件描述',
  `creater` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_updater` varchar(64) DEFAULT NULL COMMENT '最后更新人',
  `last_update_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  `process_rule` varchar(1000) DEFAULT NULL COMMENT '处理规则，如何拆分，如何加密，后续扩展',
  `relation_id` varchar(100) DEFAULT NULL COMMENT '关联id(巡检报告文件为巡检报告id,汇总结果/日志文件则是实例id)',
  `relation_name` varchar(512) DEFAULT NULL COMMENT '扩展一个关联内容名称描述',
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6136 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_group 结构
CREATE TABLE IF NOT EXISTS `ops_group` (
  `group_id` bigint(11) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(11) NOT NULL,
  `group_name` varchar(512) NOT NULL COMMENT '分组名称',
  `group_desc` varchar(1024) DEFAULT NULL COMMENT '分组描述',
  `creater` varchar(128) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL,
  `updater` varchar(128) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_group_relation 结构
CREATE TABLE IF NOT EXISTS `ops_group_relation` (
  `group_relation_id` bigint(11) NOT NULL AUTO_INCREMENT,
  `group_id` bigint(11) NOT NULL,
  `object_type` varchar(100) NOT NULL COMMENT '对象类型',
  `object_id` bigint(11) NOT NULL COMMENT '对象ID',
  PRIMARY KEY (`group_relation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=776 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_host_step_operate_log 结构
CREATE TABLE IF NOT EXISTS `ops_host_step_operate_log` (
  `step_instance_id` bigint(20) NOT NULL,
  `target_host` varchar(40) NOT NULL COMMENT '目标主机',
  `status` tinyint(4) DEFAULT NULL COMMENT '操作结果状态',
  `exit_code` tinyint(4) DEFAULT NULL COMMENT '执行返回码  0 正常   非0  执行存在错误',
  `ops_log` longtext COMMENT '反馈信息',
  `aspnode_result` tinyint(4) DEFAULT NULL COMMENT 'aspnode result： 0 成功  1 失败',
  `aspnode_msg` varchar(5000) DEFAULT NULL COMMENT 'aspnode message ',
  `mark_time` datetime DEFAULT NULL COMMENT '更新时间',
  `total_time` float DEFAULT NULL COMMENT '耗时(s)',
  UNIQUE KEY `idx_host_step_instance_detail` (`step_instance_id`,`target_host`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_label 结构
CREATE TABLE IF NOT EXISTS `ops_label` (
  `code` varchar(30) NOT NULL COMMENT '编码',
  `label` varchar(60) NOT NULL COMMENT '展示名',
  `description` varchar(100) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_os_distribution 结构
CREATE TABLE IF NOT EXISTS `ops_os_distribution` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `os_type` varchar(20) DEFAULT NULL COMMENT '操作系统类型  linux | windows',
  `os_vendor` varchar(64) DEFAULT NULL COMMENT '发行商',
  `version` varchar(30) DEFAULT NULL COMMENT '版本',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_os_distribution` (`os_type`,`os_vendor`,`version`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_param 结构
CREATE TABLE IF NOT EXISTS `ops_param` (
  `param_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '序列号',
  `param_code` varchar(128) NOT NULL COMMENT '参数编码',
  `param_name` varchar(512) NOT NULL COMMENT '参数名称',
  `param_desc` varchar(1000) DEFAULT NULL COMMENT '参数描述',
  `param_type` varchar(10) DEFAULT NULL COMMENT '参数类型  1常量 2密码变量 ',
  `length` int(11) DEFAULT NULL COMMENT '参数值长度',
  `param_default_value` varchar(20) DEFAULT NULL COMMENT '参数默认值',
  PRIMARY KEY (`param_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_param_value 结构
CREATE TABLE IF NOT EXISTS `ops_param_value` (
  `param_value_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '序列号',
  `param_code` varchar(128) NOT NULL COMMENT '参数编码',
  `param_value` varchar(512) NOT NULL COMMENT '参数值',
  `step_instance_id` bigint(20) DEFAULT NULL COMMENT '步骤id',
  `pipeline_instance_id` bigint(20) DEFAULT NULL COMMENT '作业实例id',
  `is_valid` varchar(10) DEFAULT NULL COMMENT '0无效/1生效',
  `agent_ip` varchar(50) DEFAULT NULL COMMENT 'proxyId:IP',
  `old_param_value` varchar(512) DEFAULT NULL COMMENT '上一次生效参数值',
  PRIMARY KEY (`param_value_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13522 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_pipeline 结构
CREATE TABLE IF NOT EXISTS `ops_pipeline` (
  `pipeline_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(512) NOT NULL,
  `creater` varchar(128) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updater` varchar(128) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `label_id` varchar(255) DEFAULT NULL COMMENT '标签id',
  `step_count` int(11) DEFAULT NULL COMMENT '作业中包含的步骤数量',
  PRIMARY KEY (`pipeline_id`),
  UNIQUE KEY `uk_assembly_name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=242 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_pipeline_instance 结构
CREATE TABLE IF NOT EXISTS `ops_pipeline_instance` (
  `pipeline_instance_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '作业实例id',
  `pipeline_id` bigint(20) NOT NULL COMMENT '作业id',
  `name` varchar(512) NOT NULL COMMENT '作业名',
  `operator` varchar(128) DEFAULT NULL COMMENT '操作发起人',
  `trigger_way` tinyint(4) DEFAULT NULL COMMENT '发起方式',
  `current_step_id` bigint(20) DEFAULT NULL COMMENT '作业当前step',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '作业当前状态',
  `start_time` datetime DEFAULT NULL COMMENT '作业开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '作业结束时间',
  `total_time` float(11,3) DEFAULT NULL COMMENT '总耗时(s)',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `instance_classify` smallint(6) DEFAULT NULL COMMENT ' 1.实时脚本作业   2.实时文件分发   9. 流程作业',
  `biz_classify` smallint(6) DEFAULT NULL COMMENT '业务分类   0 通用ops操作     1 监控项脚本采集',
  `label_id` varchar(255) DEFAULT NULL COMMENT '标签id',
  `review_applicant` varchar(512) DEFAULT NULL COMMENT '审核申请人',
  `review_apply_time` timestamp NULL DEFAULT NULL COMMENT '审核申请时间',
  `aspnode_result` tinyint(4) DEFAULT NULL COMMENT '结果状态 0 成功  1 失败',
  `output_path` varchar(512) DEFAULT NULL COMMENT '产出物地址',
  PRIMARY KEY (`pipeline_instance_id`)
) ENGINE=InnoDB AUTO_INCREMENT=344925 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_pipeline_instance_log 结构
CREATE TABLE IF NOT EXISTS `ops_pipeline_instance_log` (
  `pipeline_instance_id` bigint(20) NOT NULL COMMENT '作业实例序列号',
  `log_path` varchar(255) DEFAULT NULL COMMENT '作业历史日志路径',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态 0：未生成 1：已生成'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_pipeline_run_job 结构
CREATE TABLE IF NOT EXISTS `ops_pipeline_run_job` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(512) NOT NULL,
  `pipeline_id` bigint(20) NOT NULL COMMENT 'assembly_id',
  `cron_expression` varchar(64) NOT NULL COMMENT 'cron表达式',
  `status` tinyint(4) DEFAULT NULL COMMENT '定时任务当前状态',
  `creater` varchar(128) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updater` varchar(128) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`job_id`),
  UNIQUE KEY `uk_pipeline_run_job` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_pipeline_scenes 结构
CREATE TABLE IF NOT EXISTS `ops_pipeline_scenes` (
  `pipeline_scenes_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `pipeline_id` bigint(20) NOT NULL COMMENT '流程ID',
  `scenes_name` varchar(100) DEFAULT NULL COMMENT '场景名称',
  `scenes_desc` varchar(512) DEFAULT NULL COMMENT '场景描述',
  `scenes_picture` longtext COMMENT '场景图',
  `creater` varchar(30) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `updater` varchar(30) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`pipeline_scenes_id`),
  UNIQUE KEY `uk_scenes_name` (`scenes_name`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_script 结构
CREATE TABLE IF NOT EXISTS `ops_script` (
  `script_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `script_name` varchar(100) NOT NULL COMMENT '脚本名',
  `script_content` longtext COMMENT '脚本内容',
  `content_type` tinyint(30) DEFAULT NULL COMMENT '内容类型：1 sh 2 bat 3 python',
  `step_id` bigint(20) DEFAULT NULL COMMENT '所属作业id',
  `is_public` tinyint(4) DEFAULT NULL COMMENT '是否公共脚本',
  `label_id` varchar(30) DEFAULT NULL COMMENT 'label id',
  `creater` varchar(30) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `updater` varchar(30) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `script_use_desc` varchar(1000) DEFAULT NULL COMMENT '脚本使用说明',
  `ops_param_code` varchar(512) DEFAULT NULL COMMENT '引用的自定义参数',
  `package_password` varchar(100) DEFAULT NULL COMMENT '引用密码参数，默认系统生成加密产出，需要用户提供加密串',
  PRIMARY KEY (`script_id`),
  UNIQUE KEY `uk_script_name` (`script_name`)
) ENGINE=InnoDB AUTO_INCREMENT=627 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_sensitive_config 结构
CREATE TABLE IF NOT EXISTS `ops_sensitive_config` (
  `sensitive_config_id` bigint(11) NOT NULL AUTO_INCREMENT,
  `command` varchar(128) NOT NULL COMMENT '敏感指令',
  `params` varchar(128) DEFAULT NULL COMMENT '参数',
  `label` varchar(50) DEFAULT NULL COMMENT '标签',
  `os_type` varchar(20) DEFAULT NULL COMMENT '操作系统类型',
  `ops_user` varchar(256) DEFAULT NULL COMMENT '匹配的操作用户',
  `ops_target_hosts` longtext COMMENT '匹配的主机',
  `creater` varchar(128) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL,
  `updater` varchar(128) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `sensitive_range` longtext COMMENT '敏感指令范围JSON',
  `content_type` tinyint(30) DEFAULT NULL COMMENT '内容类型：1 sh 2 bat 3 python',
  `deal_type` tinyint(2) DEFAULT NULL COMMENT '响应方式  1：审核2：阻断',
  `match_type` tinyint(2) DEFAULT NULL COMMENT '匹配方式 1：完全匹配 2：正则匹配',
  `path` varchar(512) DEFAULT NULL COMMENT '命令路径',
  PRIMARY KEY (`sensitive_config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_sensitive_review 结构
CREATE TABLE IF NOT EXISTS `ops_sensitive_review` (
  `review_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pipeline_instance_id` bigint(20) DEFAULT NULL COMMENT '作业id',
  `sensitive_env_hash` varchar(64) DEFAULT NULL COMMENT '敏感环境参数hash(目标操作用户、目标主机)',
  `sensitive_command_hash` varchar(64) DEFAULT NULL COMMENT '敏感指令hash',
  `review_status` tinyint(2) DEFAULT NULL COMMENT '审核状态 0待确认 1 待审核  2 审核通过  3 审核不通过9 阻断',
  `applicant` varchar(128) DEFAULT NULL COMMENT '申请人',
  `apply_time` datetime DEFAULT NULL COMMENT '申请时间',
  `reviewer` varchar(128) DEFAULT NULL COMMENT '审核人',
  `review_time` datetime DEFAULT NULL COMMENT '审核时间',
  `review_content` longtext COMMENT '审批内容',
  `sensitive_config_id` bigint(20) DEFAULT NULL COMMENT '敏感指令ID',
  `sensitive_rule_id` bigint(20) DEFAULT NULL COMMENT '敏感指令规则ID',
  `rule_name` varchar(128) DEFAULT NULL COMMENT '敏感指令规则名称',
  `command` varchar(128) DEFAULT NULL COMMENT '敏感指令内容',
  PRIMARY KEY (`review_id`)
) ENGINE=InnoDB AUTO_INCREMENT=509487 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_sensitive_review_role 结构
CREATE TABLE IF NOT EXISTS `ops_sensitive_review_role` (
  `review_id` bigint(20) DEFAULT NULL,
  `role_id` varchar(64) DEFAULT NULL COMMENT '审核角色ID',
  `role_name` varchar(512) DEFAULT NULL COMMENT '角色名称'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_sensitive_rule 结构
CREATE TABLE IF NOT EXISTS `ops_sensitive_rule` (
  `sensitive_rule_id` bigint(11) NOT NULL AUTO_INCREMENT,
  `sensitive_config_id` bigint(11) NOT NULL,
  `rule_name` varchar(50) DEFAULT NULL COMMENT '规则名称',
  `rule_range` longtext COMMENT '规则范围JSON',
  `status` tinyint(2) DEFAULT NULL COMMENT '规则状态',
  PRIMARY KEY (`sensitive_rule_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_sensitive_rule_white 结构
CREATE TABLE IF NOT EXISTS `ops_sensitive_rule_white` (
  `sensitive_rule_white_id` bigint(11) NOT NULL AUTO_INCREMENT,
  `sensitive_rule_id` bigint(11) NOT NULL,
  `object_type` varchar(50) DEFAULT NULL COMMENT '对象类型',
  `object_id` bigint(11) NOT NULL,
  `object_status` tinyint(2) DEFAULT NULL COMMENT '对象状态',
  `untie_source` tinyint(2) DEFAULT NULL COMMENT '解绑来源，1表示修改自动解绑',
  PRIMARY KEY (`sensitive_rule_white_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_spectre_host 结构
CREATE TABLE IF NOT EXISTS `ops_spectre_host` (
  `proxy_id` bigint(20) NOT NULL COMMENT '所属的proxy的标识',
  `agent_ip` varchar(18) NOT NULL COMMENT 'agent主机ip',
  `agent_env_id` varchar(50) DEFAULT NULL COMMENT '群组id',
  `host_os_type` varchar(10) DEFAULT NULL COMMENT '操作系统类型：   linux  |  windows',
  `base_user` varchar(25) DEFAULT NULL COMMENT 'agent进程的运行用户',
  `embed_ssh_user` varchar(20) DEFAULT NULL COMMENT '内置sshd登录用户名',
  `embed_ssh_pass` varchar(64) DEFAULT NULL COMMENT '内置sshd服务登录密码',
  `embed_ssh_port` smallint(4) DEFAULT NULL COMMENT '内置sshd服务端口',
  `status` varchar(12) DEFAULT NULL COMMENT 'agent连接状态；NORMAL:正常  UNLINK:未连接',
  `last_sync_time` datetime DEFAULT NULL COMMENT '最后同步时间',
  PRIMARY KEY (`proxy_id`,`agent_ip`),
  UNIQUE KEY `uk_agent_host` (`proxy_id`,`agent_ip`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_spectre_proxy 结构
CREATE TABLE IF NOT EXISTS `ops_spectre_proxy` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `pool` varchar(50) NOT NULL COMMENT '资源池',
  `proxy_identity` varchar(50) NOT NULL COMMENT 'proxy唯一标识',
  `description` varchar(200) DEFAULT NULL COMMENT '描述信息',
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_proxy` (`proxy_identity`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_status_enum 结构
CREATE TABLE IF NOT EXISTS `ops_status_enum` (
  `status_code` int(11) NOT NULL,
  `status_name` varchar(128) NOT NULL,
  `description` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`status_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_step 结构
CREATE TABLE IF NOT EXISTS `ops_step` (
  `step_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `pipeline_id` bigint(20) NOT NULL COMMENT 'assembly_id',
  `name` varchar(512) NOT NULL,
  `ops_type` tinyint(4) NOT NULL COMMENT '操作类型：0 脚本执行  1 文件下发 2 结果文件存储',
  `ord` int(11) NOT NULL COMMENT '顺序',
  `block_ord` int(11) DEFAULT NULL COMMENT '执行块顺序号',
  `block_name` varchar(512) DEFAULT NULL COMMENT '执行块名称',
  `target_ops_user` varchar(128) DEFAULT NULL COMMENT '执行用户',
  `target_hosts` longtext COMMENT '执行目标主机',
  `script_id` bigint(20) DEFAULT NULL COMMENT '脚本id',
  `script_param` longtext COMMENT '脚本执行参数',
  `script_sudo` tinyint(4) DEFAULT NULL COMMENT '是否需要使用sudo执行脚本  0 不使用  1  使用',
  `ops_timeout` int(11) DEFAULT NULL COMMENT '执行超时时长',
  `file_source` longtext COMMENT '文件分发源',
  `file_target_path` varchar(256) DEFAULT NULL COMMENT '文件分发目标主机目录',
  `file_store_source` longtext COMMENT 'agent文件下载源(内容为json数组)',
  `tip_text` varchar(256) DEFAULT NULL COMMENT 'step操作提示文本',
  `creater` varchar(128) DEFAULT NULL COMMENT '创建人',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updater` varchar(128) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `pause_flag` tinyint(11) DEFAULT NULL COMMENT '暂停标记位  0 自然执行 1 暂停',
  `param_sensive_flag` tinyint(1) DEFAULT '0' COMMENT '参数敏感标记位  0 不敏感  1 敏感',
  `replace_attrs` longtext COMMENT '模板可替换属性定义',
  `file_type` tinyint(1) DEFAULT NULL COMMENT '文件类型 0：普通 1:4A',
  `file_store_converge_type` tinyint(4) DEFAULT NULL COMMENT '文件结果汇聚类型  0：不汇聚  1：追加汇聚（同文件名内容追加汇总）2：分类汇聚（按指定规则汇聚，暂按特定规则处理基线shadow文件）',
  `file_store_safety` tinyint(4) DEFAULT NULL COMMENT '0 不安全  1安全',
  PRIMARY KEY (`step_id`),
  UNIQUE KEY `uk_step_name` (`name`),
  KEY `idx_assembly_id` (`pipeline_id`),
  KEY `idx_script_id` (`script_id`)
) ENGINE=InnoDB AUTO_INCREMENT=343 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_step_instance 结构
CREATE TABLE IF NOT EXISTS `ops_step_instance` (
  `step_instance_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `step_id` bigint(20) NOT NULL COMMENT 'step_id',
  `pipeline_instance_id` bigint(20) NOT NULL COMMENT 'assembly_instance_id',
  `name` varchar(512) NOT NULL COMMENT 'name',
  `ops_type` tinyint(4) NOT NULL COMMENT '操作类型：0 脚本执行  1 文件下发 2 结果文件存储',
  `ord` int(11) NOT NULL COMMENT '作业流程中的顺序号',
  `block_ord` int(11) DEFAULT NULL COMMENT '作业块的顺序号',
  `block_name` varchar(512) DEFAULT NULL COMMENT '作业块名称',
  `target_ops_user` varchar(256) DEFAULT NULL COMMENT '目标作业用户',
  `target_hosts` longtext COMMENT '目标主机',
  `bad_hosts` longtext COMMENT '失败主机列表',
  `script_content` longtext COMMENT '脚本内容',
  `script_content_type` tinyint(4) DEFAULT NULL COMMENT '脚本内容类型  1 sh  2 bat  3 perl  4 python  5 powershell',
  `script_param` longtext COMMENT '脚本参数',
  `script_sudo` tinyint(4) DEFAULT NULL COMMENT '是否需要使用sudo执行脚本  0 不使用  1  使用',
  `ops_timeout` int(11) DEFAULT NULL COMMENT '操作超时时间(s)',
  `file_source` longtext COMMENT '文件分发源',
  `file_target_path` varchar(256) DEFAULT NULL COMMENT '文件分发目标路径',
  `file_store_source` longtext COMMENT 'agent文件下载源(内容为json数组)',
  `tip_text` varchar(256) DEFAULT NULL COMMENT '提示文本',
  `operator` varchar(128) DEFAULT NULL COMMENT '操作发起人',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '当前状态',
  `start_time` datetime DEFAULT NULL COMMENT '操作开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '操作结束时间',
  `total_time` float(11,3) DEFAULT NULL COMMENT '总耗时',
  `total_hosts_num` int(11) DEFAULT NULL COMMENT '总目标主机数',
  `bad_hosts_num` int(11) DEFAULT NULL COMMENT '运行失败主机数',
  `run_hosts_num` int(11) DEFAULT NULL COMMENT '正在运行主机数',
  `success_hosts_num` int(11) DEFAULT NULL COMMENT '成功主机数',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `pause_flag` int(11) DEFAULT NULL COMMENT '暂停标记位  0 自然执行 1 暂停',
  `param_sensive_flag` tinyint(1) DEFAULT '0' COMMENT '参数敏感标记位  0 不敏感  1 敏感',
  `file_type` tinyint(1) DEFAULT NULL COMMENT '文件类型 0：普通 1:4A',
  `aspnode_result` tinyint(4) DEFAULT NULL COMMENT '结果状态 0 成功  1 失败',
  `ops_param_code` varchar(512) DEFAULT NULL COMMENT '引用的自定义参数',
  `package_password` varchar(100) DEFAULT NULL COMMENT '引用密码参数，默认系统生成加密产出，需要用户提供加密串',
  PRIMARY KEY (`step_instance_id`),
  KEY `idx_assembly_instance_id` (`pipeline_instance_id`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=368706 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_vulnerability 结构
CREATE TABLE IF NOT EXISTS `ops_vulnerability` (
  `id` varchar(64) NOT NULL COMMENT 'id',
  `port` varchar(256) DEFAULT NULL COMMENT '端口',
  `protocol` varchar(20) DEFAULT NULL COMMENT '协议',
  `service` varchar(50) DEFAULT NULL COMMENT '服务',
  `name` varchar(300) DEFAULT NULL COMMENT '漏洞名',
  `risk_level` varchar(20) DEFAULT NULL COMMENT '危险等级',
  `cve_number` varchar(64) DEFAULT NULL,
  `cncve_number` varchar(64) DEFAULT NULL,
  `cnvd_number` varchar(64) DEFAULT NULL,
  `cnnvd_number` varchar(64) DEFAULT NULL,
  `vul_describe` text,
  `repair_method` text COMMENT '解决办法',
  `pipeline_id_list` tinytext COMMENT '关联的漏洞修复作业id,多个使用英文逗号分隔',
  `soft_dependencies` text COMMENT '软件依赖',
  `need_reboot` varchar(1) DEFAULT 'N' COMMENT '是否需要重启',
  `can_fixed` varchar(1) DEFAULT 'Y' COMMENT '是否可修复',
  `remark` varchar(128) DEFAULT NULL COMMENT '备注',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_vulnerability_group 结构
CREATE TABLE IF NOT EXISTS `ops_vulnerability_group` (
  `vulnerability_group_id` bigint(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(511) NOT NULL COMMENT '漏洞分组名称',
  `vulnerability_group_desc` varchar(1000) DEFAULT NULL COMMENT '漏洞分组描述',
  `vulnerability_group_rule` longtext COMMENT '漏洞规则JSON',
  `is_valid` varchar(10) DEFAULT NULL COMMENT '是否生效0否，1是',
  `creater` varchar(128) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL,
  `updater` varchar(128) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `order_num` int(11) DEFAULT NULL COMMENT '排序号',
  PRIMARY KEY (`vulnerability_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_vulnerability_group_relation 结构
CREATE TABLE IF NOT EXISTS `ops_vulnerability_group_relation` (
  `vulnerability_group_id` bigint(11) DEFAULT NULL,
  `vulnerability_id` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_vulnerability_instance 结构
CREATE TABLE IF NOT EXISTS `ops_vulnerability_instance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `scan_cycle` varchar(10) DEFAULT NULL COMMENT '扫描周期: yyyyMM',
  `pool_name` varchar(36) DEFAULT NULL COMMENT '资源池',
  `host_ip` varchar(16) DEFAULT NULL,
  `vulnerability_id` varchar(64) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL COMMENT '状态：WAIT_REPAIR, BEYOND_REPAIR, PROCESSED 等待修复, 不能修复, 已处理',
  `report_time` varchar(20) DEFAULT NULL COMMENT '报告时间',
  `last_operator` varchar(36) DEFAULT NULL COMMENT '最后操作人',
  `last_update_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  `history_flag` varchar(1) DEFAULT 'N' COMMENT '是否历史数据',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_vul_instance` (`scan_cycle`,`pool_name`,`host_ip`,`vulnerability_id`)
) ENGINE=InnoDB AUTO_INCREMENT=102415 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_vulnerability_repair_log 结构
CREATE TABLE IF NOT EXISTS `ops_vulnerability_repair_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `vulnerability_instance_id` bigint(20) NOT NULL COMMENT '漏洞实例id',
  `pipeline_instance_id` bigint(20) NOT NULL COMMENT '漏洞作业实例id',
  `operator` varchar(36) DEFAULT NULL COMMENT '操作人',
  `execute_time` datetime DEFAULT NULL COMMENT '执行时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_vulnerability_scan_cycle 结构
CREATE TABLE IF NOT EXISTS `ops_vulnerability_scan_cycle` (
  `scan_cycle` varchar(6) DEFAULT NULL COMMENT '漏洞处理周期:  yyyyMM',
  `status` varchar(1) DEFAULT NULL COMMENT 'Y: 已完成   N：未完成',
  `process_time` datetime DEFAULT NULL COMMENT '处理时间',
  UNIQUE KEY `uk_vul_scan_cycle` (`scan_cycle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_vul_pool_code_name 结构
CREATE TABLE IF NOT EXISTS `ops_vul_pool_code_name` (
  `pool_code` varchar(200) NOT NULL COMMENT '漏洞扫描分析结果中的资源池code',
  `pool_name` varchar(200) DEFAULT NULL COMMENT '资源池name',
  PRIMARY KEY (`pool_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_yum_file 结构
CREATE TABLE IF NOT EXISTS `ops_yum_file` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `file_type` tinyint(4) NOT NULL COMMENT '文件类型   0：yum源文件   1：yum配置文件',
  `name` varchar(100) DEFAULT NULL COMMENT '名称',
  `version` varchar(50) DEFAULT NULL COMMENT '版本',
  `os_distribution_id` int(11) DEFAULT NULL COMMENT '操作系统发行版本',
  `upload_file_path` varchar(200) DEFAULT NULL COMMENT '文件服务器存储目录',
  `yumfile_group_id` bigint(20) DEFAULT NULL COMMENT '分组id',
  `description` varchar(250) DEFAULT NULL,
  `creater` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_updater` varchar(64) DEFAULT NULL COMMENT '最后更新人',
  `last_update_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_yum_file` (`name`,`version`,`file_type`) USING BTREE,
  KEY `idx_yum_file` (`file_type`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 deploy_proxy.ops_yum_file_group 结构
CREATE TABLE IF NOT EXISTS `ops_yum_file_group` (
  `group_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(50) NOT NULL COMMENT '分组名',
  `parent_group_id` bigint(20) DEFAULT NULL COMMENT '父节点id',
  `level` smallint(6) DEFAULT NULL COMMENT '层级',
  `description` varchar(250) DEFAULT NULL,
  `creater` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_updater` varchar(64) DEFAULT NULL COMMENT '最后修改人',
  `last_update_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`group_id`),
  UNIQUE KEY `uk_yum_file_group` (`group_name`,`parent_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  视图 deploy_proxy.v_base_info 结构
-- 创建临时表以解决视图依赖性错误
CREATE TABLE `v_base_info` (
	`object_type` VARCHAR(9) NOT NULL COLLATE 'utf8mb4_general_ci',
	`object_id` BIGINT(20) NOT NULL,
	`object_name` VARCHAR(512) NULL COLLATE 'utf8_general_ci',
	`creater` VARCHAR(128) NULL COLLATE 'utf8_general_ci',
	`create_time` DATETIME NULL,
	`updater` VARCHAR(128) NULL COLLATE 'utf8_general_ci',
	`update_time` DATETIME NULL
) ENGINE=MyISAM;

-- 导出  视图 deploy_proxy.v_base_info 结构
-- 移除临时表并创建最终视图结构
DROP TABLE IF EXISTS `v_base_info`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_base_info` AS select 'script' AS `object_type`,`ops_script`.`script_id` AS `object_id`,`ops_script`.`script_name` AS `object_name`,`ops_script`.`creater` AS `creater`,`ops_script`.`create_time` AS `create_time`,`ops_script`.`updater` AS `updater`,`ops_script`.`update_time` AS `update_time` from `ops_script` union select 'pipeline' AS `object_type`,`ops_pipeline`.`pipeline_id` AS `object_id`,`ops_pipeline`.`name` AS `object_name`,`ops_pipeline`.`creater` AS `creater`,`ops_pipeline`.`create_time` AS `create_time`,`ops_pipeline`.`updater` AS `updater`,`ops_pipeline`.`update_time` AS `update_time` from `ops_pipeline` union select 'yum' AS `object_type`,`ops_yum_file`.`id` AS `object_id`,`ops_yum_file`.`name` AS `object_name`,`ops_yum_file`.`creater` AS `creater`,`ops_yum_file`.`create_time` AS `create_time`,`ops_yum_file`.`last_updater` AS `updater`,`ops_yum_file`.`last_update_time` AS `update_time` from `ops_yum_file` union select 'scenes' AS `object_type`,`ops_pipeline_scenes`.`pipeline_scenes_id` AS `object_id`,`ops_pipeline_scenes`.`scenes_name` AS `object_name`,`ops_pipeline_scenes`.`creater` AS `creater`,`ops_pipeline_scenes`.`create_time` AS `create_time`,`ops_pipeline_scenes`.`updater` AS `updater`,`ops_pipeline_scenes`.`update_time` AS `update_time` from `ops_pipeline_scenes` union select 'ap_scheme' AS `object_type`,`ops_auto_repair_scheme`.`scheme_id` AS `object_id`,`ops_auto_repair_scheme`.`scheme_name` AS `object_name`,`ops_auto_repair_scheme`.`creater` AS `creater`,`ops_auto_repair_scheme`.`create_time` AS `create_time`,`ops_auto_repair_scheme`.`updater` AS `updater`,`ops_auto_repair_scheme`.`update_time` AS `update_time` from `ops_auto_repair_scheme` union select 'file' AS `object_type`,`ops_file`.`file_id` AS `object_id`,`ops_file`.`file_name` AS `object_name`,`ops_file`.`creater` AS `creater`,`ops_file`.`create_time` AS `create_time`,`ops_file`.`last_updater` AS `updater`,`ops_file`.`last_update_time` AS `update_time` from `ops_file`;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;


