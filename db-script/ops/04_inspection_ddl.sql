-- --------------------------------------------------------
-- 主机:                           10.12.70.40
-- 服务器版本:                        5.7.26-log - MySQL Community Server (GPL)
-- 服务器操作系统:                      Linux
-- HeidiSQL 版本:                  9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

use mirror;

-- 导出  表 mirror.inspection_day_bizsystem 结构
DROP TABLE IF EXISTS `inspection_day_bizsystem`;
CREATE TABLE IF NOT EXISTS `inspection_day_bizsystem` (
  `bizsystem_count` varchar(255) DEFAULT NULL COMMENT '业务数量',
  `normal_count` varchar(255) DEFAULT NULL COMMENT '正常数量',
  `normal_login_count` varchar(255) DEFAULT NULL COMMENT '正常登陆数量',
  `normal_visit_count` varchar(255) DEFAULT NULL COMMENT '正常访问占比',
  `normal_rate` varchar(255) DEFAULT NULL COMMENT '正常占比',
  `exception_count` varchar(255) DEFAULT NULL COMMENT '异常数量',
  `exception_login_count` varchar(255) DEFAULT NULL COMMENT '异常登陆数量',
  `exception_visit_count` varchar(255) DEFAULT NULL COMMENT '异常访问数量',
  `exception_rate` varchar(255) DEFAULT NULL COMMENT '异常数量占比',
  `mon` varchar(255) DEFAULT NULL COMMENT '月份'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='每日业务巡检记录表';

-- 数据导出被取消选择。
-- 导出  表 mirror.inspection_day_device 结构
DROP TABLE IF EXISTS `inspection_day_device`;
CREATE TABLE IF NOT EXISTS `inspection_day_device` (
  `device_sum` varchar(255) DEFAULT NULL COMMENT '当日设备总量',
  `normal_sum` varchar(255) DEFAULT NULL COMMENT '当日设备正常数量',
  `normal_rate` varchar(255) DEFAULT NULL COMMENT '当日设备正常占比',
  `vm_sum` varchar(255) DEFAULT NULL COMMENT '当日虚拟机异常数量',
  `vm_rate` varchar(255) DEFAULT NULL COMMENT '当日虚拟机异常占比',
  `phy_sum` varchar(255) DEFAULT NULL COMMENT '当日物理机异常数量',
  `phy_rate` varchar(255) DEFAULT NULL COMMENT '当日物理机异常占比',
  `host_sum` varchar(255) DEFAULT NULL COMMENT '当日宿主机异常数量',
  `host_rate` varchar(255) DEFAULT NULL COMMENT '当日宿主机异常占比',
  `net_sum` varchar(255) DEFAULT NULL COMMENT '当日网络设备异常数量',
  `net_rate` varchar(255) DEFAULT NULL COMMENT '当日网络设备异常占比',
  `exception_rate` varchar(255) DEFAULT NULL COMMENT '异常占比',
  `mon` varchar(255) DEFAULT NULL COMMENT '月份'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='每日设备巡检报表';

-- 数据导出被取消选择。
-- 导出  表 mirror.inspection_report 结构
DROP TABLE IF EXISTS `inspection_report`;
CREATE TABLE IF NOT EXISTS `inspection_report` (
  `report_id` varchar(50) NOT NULL COMMENT '报告ID',
  `task_id` varchar(50) NOT NULL COMMENT '任务ID',
  `name` varchar(200) NOT NULL COMMENT '报告名称',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `status` varchar(20) NOT NULL COMMENT 'RUNNING-运行中\r\nFINNISHED运行完成',
  `biz_status` varchar(20) DEFAULT NULL COMMENT '业务状态  成功0，失败1，运行中2',
  `finish_time` datetime DEFAULT NULL COMMENT '结束时间',
  `report_file_path` varchar(500) DEFAULT NULL COMMENT '报告文件路径',
  `total_time` float(10,3) DEFAULT NULL COMMENT '总耗时（s）',
  PRIMARY KEY (`report_id`),
  KEY `task_id` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 mirror.inspection_report_item 结构
DROP TABLE IF EXISTS `inspection_report_item`;
CREATE TABLE IF NOT EXISTS `inspection_report_item` (
  `report_item_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '序列号',
  `report_id` varchar(50) NOT NULL COMMENT '报告ID',
  `server_type` varchar(35) DEFAULT NULL,
  `key` varchar(100) DEFAULT NULL,
  `item_id` varchar(50) NOT NULL COMMENT '监控项ID',
  `clock` int(10) DEFAULT NULL COMMENT '时间戳（秒）',
  `value` varchar(4000) DEFAULT NULL COMMENT '巡检采集值',
  `pre_value` varchar(4000) DEFAULT NULL COMMENT '上一次巡检采集值',
  `ns` int(10) DEFAULT NULL COMMENT '时间戳（小于一秒的时间）',
  `status` varchar(20) DEFAULT NULL COMMENT '状态：\\r\\n0-正常\\r\\n1-异常\\r\\n2-无结果',
  `log` text COMMENT '日志',
  `ri_id` varchar(50) DEFAULT NULL COMMENT '预留字段',
  `object_type` varchar(25) DEFAULT NULL,
  `object_id` varchar(50) DEFAULT NULL,
  `exec_status` varchar(20) DEFAULT NULL COMMENT '脚本执行状态0是失败1是成功2是触发器计算完成',
  PRIMARY KEY (`report_item_id`),
  KEY `report_id` (`report_id`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `inspection_report_item_ibfk_1` FOREIGN KEY (`report_id`) REFERENCES `inspection_report` (`report_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6751 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 mirror.inspection_report_item_ops_ext 结构
DROP TABLE IF EXISTS `inspection_report_item_ops_ext`;
CREATE TABLE IF NOT EXISTS `inspection_report_item_ops_ext` (
  `report_item_ext_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '序列号',
  `report_item_id` bigint(20) NOT NULL COMMENT '报告元素序列号',
  `device_class` varchar(255) DEFAULT NULL COMMENT '设备大类',
  `device_type` varchar(255) DEFAULT NULL COMMENT '设备子类',
  `log` text COMMENT '日志',
  `agent_ip` varchar(50) DEFAULT NULL COMMENT 'agent设备IP',
  `exec_status` varchar(20) DEFAULT NULL COMMENT '脚本执行状态0是失败1是成功2是触发器计算完成',
  `device_name` varchar(512) DEFAULT NULL COMMENT '主机名',
  `idc_type` varchar(512) DEFAULT NULL COMMENT '资源池',
  `biz_system` varchar(512) DEFAULT NULL COMMENT '业务系统',
  PRIMARY KEY (`report_item_ext_id`),
  KEY `inspection_report_item_ext_ibfk_1` (`report_item_id`),
  CONSTRAINT `inspection_report_item_ext_ibfk_1` FOREIGN KEY (`report_item_id`) REFERENCES `inspection_report_item` (`report_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9705 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 mirror.inspection_report_item_value 结构
DROP TABLE IF EXISTS `inspection_report_item_value`;
CREATE TABLE IF NOT EXISTS `inspection_report_item_value` (
  `report_item_value_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '序列号',
  `report_item_id` bigint(20) NOT NULL COMMENT '报告元素序列号',
  `result_name` varchar(1000) DEFAULT NULL COMMENT '子结果名称描述',
  `result_value` varchar(4000) DEFAULT NULL COMMENT '巡检采集值',
  `result_status` varchar(20) DEFAULT NULL COMMENT '状态：0-正常1-异常2-无结果',
  PRIMARY KEY (`report_item_value_id`)
) ENGINE=InnoDB AUTO_INCREMENT=341 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 mirror.inspection_task 结构
DROP TABLE IF EXISTS `inspection_task`;
CREATE TABLE IF NOT EXISTS `inspection_task` (
  `task_id` varchar(50) NOT NULL COMMENT '任务ID',
  `name` varchar(200) NOT NULL COMMENT '任务名',
  `type` varchar(20) NOT NULL COMMENT '任务类型\r\n1-手动\r\n2-自动',
  `cycle` varchar(20) NOT NULL COMMENT '时间周期，只有任务类型为自动，才能选择\r\nMIN-分钟\r\nMON-月\r\nWEEK-周\r\nDAY-日\r\nDEFINE-自定义',
  `exec_time` varchar(100) NOT NULL,
  `status` varchar(20) NOT NULL COMMENT '状态：\r\nON-启动\r\nOFF-禁用',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 mirror.inspection_task_device 结构
DROP TABLE IF EXISTS `inspection_task_device`;
CREATE TABLE IF NOT EXISTS `inspection_task_device` (
  `task_device_id` varchar(50) NOT NULL COMMENT '任务设备关系ID',
  `task_id` varchar(50) DEFAULT NULL,
  `device_id` varchar(50) NOT NULL COMMENT '设备ID',
  `template_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`task_device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 mirror.inspection_task_object 结构
DROP TABLE IF EXISTS `inspection_task_object`;
CREATE TABLE IF NOT EXISTS `inspection_task_object` (
  `task_object_id` varchar(50) NOT NULL COMMENT '任务设备关系ID',
  `task_id` varchar(50) NOT NULL COMMENT '任务ID',
  `object_type` varchar(50) DEFAULT NULL COMMENT '关联对象类型\r\n1-设备ID\r\n2-业务系统',
  `object_id` varchar(50) NOT NULL COMMENT '设备ID',
  `template_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`task_object_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 mirror.inspection_task_receiver 结构
DROP TABLE IF EXISTS `inspection_task_receiver`;
CREATE TABLE IF NOT EXISTS `inspection_task_receiver` (
  `receiver_id` varchar(50) NOT NULL COMMENT '序列号',
  `task_id` varchar(50) NOT NULL COMMENT '任务ID',
  `user_id` varchar(50) NOT NULL COMMENT '用户ID',
  PRIMARY KEY (`receiver_id`),
  KEY `task_id` (`task_id`),
  CONSTRAINT `inspection_task_receiver_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `inspection_task` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 mirror.monitor_actions 结构
DROP TABLE IF EXISTS `monitor_actions`;
CREATE TABLE IF NOT EXISTS `monitor_actions` (
  `action_id` varchar(50) NOT NULL COMMENT '事件ID',
  `name` varchar(500) NOT NULL COMMENT '事件名',
  `event_source` varchar(20) NOT NULL COMMENT '事件来源\r\n0-指来源为触发器trigger\r\n1-指来源为自动发现descover\r\n2-指来源为自动登记auto_register\r\n3-为网络发现产生的事件源\r\n',
  `eval_type` varchar(20) DEFAULT NULL COMMENT '表示执行action的前提条件的逻辑关系\r\n0表示and/or\r\n1表示and\r\n2表示or\r\n',
  `status` varchar(20) NOT NULL COMMENT '状态\r\nON-启动\r\nOFF-禁用',
  `type` varchar(20) NOT NULL COMMENT '类型\r\n1-回调url\r\n2-函数',
  `dealer` varchar(500) NOT NULL COMMENT '处理程序\r\n如果type为1，则为url\r\n如果type为2，则为类名.方法名\r\n入参包含事件、指标、触发器信息，需定义',
  `trigger_id` varchar(50) NOT NULL COMMENT '触发器ID',
  `event_type` int(11) NOT NULL COMMENT '事件类型：\r\n1-异常事件\r\n2-正常事件\r\n3-通用事件',
  PRIMARY KEY (`action_id`),
  KEY `trigger_id` (`trigger_id`),
  CONSTRAINT `monitor_actions_ibfk_1` FOREIGN KEY (`trigger_id`) REFERENCES `monitor_triggers` (`trigger_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 mirror.monitor_api_server_config 结构
DROP TABLE IF EXISTS `monitor_api_server_config`;
CREATE TABLE IF NOT EXISTS `monitor_api_server_config` (
  `api_server_id` varchar(50) NOT NULL COMMENT 'api配置ID',
  `url` varchar(1000) NOT NULL COMMENT '服务URL地址',
  `username` varchar(200) NOT NULL COMMENT '用户名',
  `password` varchar(200) NOT NULL COMMENT '密码',
  `room` varchar(200) NOT NULL COMMENT '所属机房域',
  `server_type` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`api_server_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 mirror.monitor_biz_theme 结构
DROP TABLE IF EXISTS `monitor_biz_theme`;
CREATE TABLE IF NOT EXISTS `monitor_biz_theme` (
  `theme_id` varchar(50) NOT NULL COMMENT '主题ID',
  `theme_code` varchar(255) NOT NULL COMMENT '主题编码',
  `object_type` varchar(50) NOT NULL COMMENT '关联对象类型\r\n1-设备ID\r\n2-业务系统',
  `object_id` varchar(50) DEFAULT NULL COMMENT 'object_id',
  `index_name` varchar(255) NOT NULL COMMENT 'es索引名',
  `value_type` varchar(10) DEFAULT NULL,
  `up_type` varchar(10) NOT NULL COMMENT '上报类型0：接口接入1：日志接入',
  `up_status` varchar(10) DEFAULT NULL COMMENT '上报状态0：成功1：失败',
  `last_up_value` varchar(2000) DEFAULT NULL COMMENT '最近上报值',
  `last_up_time` datetime DEFAULT NULL COMMENT '最近上报时间',
  `up_switch` varchar(10) DEFAULT NULL COMMENT '上报开关0：开启1：关闭',
  `status` varchar(10) DEFAULT NULL COMMENT '状态0：正式1：临时',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_fail_time` datetime DEFAULT NULL COMMENT '最近失败上报时间',
  `interval` int(255) DEFAULT NULL,
  `dim_ids` varchar(255) DEFAULT NULL COMMENT '维度id集合',
  `theme_name` varchar(255) DEFAULT NULL,
  `log_reg` varchar(2000) DEFAULT NULL COMMENT '日志正则表达式',
  `log_content` varchar(2000) DEFAULT NULL COMMENT '日志样例内容',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `delete_flag` varchar(20) DEFAULT NULL COMMENT '删除标识1-删除0-未删除',
  PRIMARY KEY (`theme_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 mirror.monitor_biz_theme_dim 结构
DROP TABLE IF EXISTS `monitor_biz_theme_dim`;
CREATE TABLE IF NOT EXISTS `monitor_biz_theme_dim` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `dim_name` varchar(255) DEFAULT NULL COMMENT '维度名称',
  `dim_type` varchar(10) DEFAULT NULL COMMENT '维度类型',
  `theme_id` varchar(50) DEFAULT NULL COMMENT '主题ID',
  `dim_order` int(11) DEFAULT NULL COMMENT '序号',
  `dim_reg` varchar(1000) DEFAULT NULL COMMENT '主题正则表达式',
  `dim_code` varchar(100) DEFAULT NULL COMMENT '维度编码',
  `match_flag` varchar(20) DEFAULT NULL COMMENT '匹配类型1-字段2-非字段',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4523 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 mirror.monitor_events 结构
DROP TABLE IF EXISTS `monitor_events`;
CREATE TABLE IF NOT EXISTS `monitor_events` (
  `event_id` varchar(50) NOT NULL COMMENT '序列号',
  `source` varchar(20) DEFAULT NULL COMMENT '事件来源：\r\nTRIGGERS-触发器\r\nDISCOVERY-新发现\r\nREGISTRATION-自动注册（agent/proxy）\r\n',
  `object` varchar(20) NOT NULL COMMENT 'TRIGGER-触发器 ',
  `object_id` varchar(50) NOT NULL COMMENT 'object类型对应的object的ID（触发器ID）',
  `value` varchar(20) NOT NULL COMMENT '事件类型\r\n1-异常\r\n0-正常',
  `acknowledged` varchar(20) NOT NULL COMMENT '确认标识：\r\n0-未确认\r\n1-已确认',
  `clock` int(10) NOT NULL COMMENT '事件产生时间',
  `ns` int(10) NOT NULL COMMENT '纳秒\r\n小于秒的部分',
  `device_id` varchar(50) NOT NULL COMMENT '事件产生对象',
  `biz_id` varchar(50) DEFAULT NULL,
  `biz_object` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 mirror.monitor_filter 结构
DROP TABLE IF EXISTS `monitor_filter`;
CREATE TABLE IF NOT EXISTS `monitor_filter` (
  `filter_id` varchar(50) NOT NULL COMMENT '过滤器ID',
  `name` varchar(500) NOT NULL COMMENT '过滤器名称',
  `status` varchar(20) NOT NULL COMMENT '状态\r\nON-启动\r\nOFF-禁用',
  `type` varchar(20) NOT NULL COMMENT '类型\r\n1-回调url\r\n2-函数',
  `dealer` varchar(500) NOT NULL COMMENT '处理程序\r\n如果type为1，则为url\r\n如果type为2，则为类名.方法名\r\n入参包含事件、指标、触发器信息，需定义',
  `condition` varchar(4000) NOT NULL COMMENT '过滤条件\r\n可以按主机、业务系统等维度设计',
  PRIMARY KEY (`filter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 mirror.monitor_functions 结构
DROP TABLE IF EXISTS `monitor_functions`;
CREATE TABLE IF NOT EXISTS `monitor_functions` (
  `function_id` varchar(50) NOT NULL COMMENT '函数ID',
  `item_id` varchar(50) NOT NULL COMMENT '监控项ID',
  `trigger_id` varchar(50) NOT NULL COMMENT '触发器ID',
  `function` varchar(100) NOT NULL COMMENT '函数',
  `parameter` varchar(500) NOT NULL COMMENT '参数',
  PRIMARY KEY (`function_id`),
  KEY `item_id` (`item_id`),
  KEY `trigger_id` (`trigger_id`),
  CONSTRAINT `monitor_functions_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `monitor_items` (`item_id`),
  CONSTRAINT `monitor_functions_ibfk_2` FOREIGN KEY (`trigger_id`) REFERENCES `monitor_triggers` (`trigger_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 mirror.monitor_http_config 结构
DROP TABLE IF EXISTS `monitor_http_config`;
CREATE TABLE IF NOT EXISTS `monitor_http_config` (
  `id` int(38) unsigned NOT NULL AUTO_INCREMENT,
  `monitor_name` varchar(255) NOT NULL,
  `biz_system_id` varchar(128) DEFAULT NULL COMMENT '业务系统id',
  `alert_level` tinyint(2) NOT NULL,
  `test_period` int(255) NOT NULL COMMENT '检测周期',
  `idcType` varchar(128) NOT NULL COMMENT '归属资源池',
  `isIntranet` tinyint(1) DEFAULT NULL COMMENT '是否内网（1内网0外网）',
  `Intranet_idcType` varchar(128) DEFAULT NULL COMMENT '检测资源池',
  `extranet` varchar(128) DEFAULT NULL COMMENT '检测外网',
  `create_staff` varchar(128) DEFAULT NULL COMMENT '创建人',
  `update_staff` varchar(128) DEFAULT NULL COMMENT '修改人',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `request_http_addr` varchar(255) DEFAULT NULL COMMENT '监控地址',
  `request_method` varchar(128) DEFAULT NULL COMMENT '请求方法',
  `response_type` varchar(255) DEFAULT NULL COMMENT '请求返回类型(json/html)',
  `regular_check` varchar(255) DEFAULT NULL COMMENT '正则表达式',
  `response_code` tinyint(1) DEFAULT NULL COMMENT '返回码（1:200,0：非200）',
  `request_parm` varchar(4000) DEFAULT NULL COMMENT '请求参数',
  `json_attribute` varchar(255) DEFAULT NULL COMMENT 'JSON属性',
  `json_value` varchar(255) DEFAULT NULL COMMENT 'JSON正常参考值',
  `json_operator` varchar(255) DEFAULT NULL COMMENT 'Json属性运算符（>,<,=...）',
  `time_out` int(11) DEFAULT NULL COMMENT '超时时间',
  `html_format` varchar(4000) DEFAULT NULL COMMENT '返回的Html格式',
  `http_content_type` varchar(255) DEFAULT NULL COMMENT '请求媒体类型',
  `status` tinyint(2) DEFAULT NULL COMMENT '状态（:0：暂停1：运行）',
  `idcTypeUrl` varchar(2000) DEFAULT NULL COMMENT '资源池访问的代理记账地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COMMENT='http监控配置表';

-- 数据导出被取消选择。
-- 导出  表 mirror.monitor_http_his 结构
DROP TABLE IF EXISTS `monitor_http_his`;
CREATE TABLE IF NOT EXISTS `monitor_http_his` (
  `id` int(38) NOT NULL AUTO_INCREMENT,
  `http_config_id` int(38) DEFAULT NULL COMMENT '监控配置表ID',
  `request_method` varchar(255) DEFAULT NULL COMMENT '请求方法(post/get)',
  `http_content_type` varchar(255) DEFAULT NULL COMMENT '请求媒体类型',
  `request_parm` varchar(4000) DEFAULT NULL COMMENT '请求参数',
  `request_http_addr` varchar(255) DEFAULT NULL COMMENT '监控地址',
  `time_out` int(11) DEFAULT NULL COMMENT '超时时间',
  `start_time` varchar(128) DEFAULT NULL COMMENT '请求时间',
  `end_time` varchar(128) DEFAULT NULL COMMENT '响应时间',
  `time_con` varchar(128) DEFAULT NULL COMMENT '响应耗时（毫秒）',
  `response_type` varchar(255) DEFAULT NULL COMMENT '请求返回类型(json/html)',
  `response_code` tinyint(1) DEFAULT NULL COMMENT '返回码（1:200,0：非200）',
  `json_attribute` varchar(255) DEFAULT NULL COMMENT 'JSON属性',
  `json_value` varchar(255) DEFAULT NULL COMMENT 'JSON正常参考值',
  `json_operator` varchar(255) DEFAULT NULL COMMENT 'Json属性运算符（>,<,=...）',
  `regular_check` varchar(255) DEFAULT NULL COMMENT '正则表达式',
  `html_format` varchar(4000) DEFAULT NULL COMMENT '返回的Html格式',
  `request_result` text COMMENT '返回结果字符串',
  `conclusion` varchar(255) DEFAULT NULL COMMENT '返回结果判定',
  `normal` tinyint(1) DEFAULT NULL COMMENT '返回结果(1正常0响应超时2主机连接异常3服务器连接错误)',
  `head_response` varchar(4000) DEFAULT NULL COMMENT '返回的head信息',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `result` tinyint(1) DEFAULT NULL COMMENT '结果：1正常0异常',
  `response_status_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59029 DEFAULT CHARSET=utf8 COMMENT='http监控历史表';

-- 数据导出被取消选择。
-- 导出  表 mirror.monitor_http_idctype 结构
DROP TABLE IF EXISTS `monitor_http_idctype`;
CREATE TABLE IF NOT EXISTS `monitor_http_idctype` (
  `idcType` varchar(255) NOT NULL,
  `url` varchar(2000) NOT NULL,
  PRIMARY KEY (`idcType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 mirror.monitor_items 结构
DROP TABLE IF EXISTS `monitor_items`;
CREATE TABLE IF NOT EXISTS `monitor_items` (
  `item_id` varchar(50) NOT NULL COMMENT '监控项ID',
  `name` varchar(100) NOT NULL COMMENT '监控项名称',
  `type` varchar(20) NOT NULL COMMENT '监控项类型\r\nSCRIPT-脚本（现有）\r\nINDEX-监控指标（现有）',
  `template_id` varchar(50) NOT NULL COMMENT '模版ID',
  `key` varchar(100) NOT NULL COMMENT '监控键值',
  `delay` int(8) DEFAULT NULL COMMENT '监控周期，单位分钟',
  `history` int(8) DEFAULT NULL COMMENT '保留时间，单位天',
  `status` varchar(20) NOT NULL COMMENT '状态：\r\nON-启用\r\nOFF-禁用\r\nNONSUPPORT-不支持（启用状态的监控项采集不到数据）（暂时不用）',
  `value_type` varchar(20) NOT NULL COMMENT '监控项数据格式：\r\nFLOAT-浮点数\r\nSTR-字符串\r\nLOG-日志\r\nUINT64-整数\r\nTEXT-文本',
  `units` varchar(20) NOT NULL COMMENT '单位',
  `error` varchar(4000) DEFAULT NULL COMMENT '错误信息',
  `data_type` varchar(20) DEFAULT NULL COMMENT '监控项数据类型\r\nDECIMAL-十进制\r\nOCTAL-八进制\r\nHEXADECIMAL-十六进制\r\nBOOLEAN-布尔值\r\n',
  `sys_type` varchar(50) NOT NULL COMMENT '接入监控系统类型（目前为zabbix）\r\nMIRROR\r\nZABBIX\r\nNONE',
  `calc_type` varchar(20) DEFAULT NULL COMMENT '采集周期计算类型',
  `biz_index` varchar(200) DEFAULT NULL,
  `biz_calc_obj` varchar(4000) DEFAULT NULL,
  `biz_calc_exp` varchar(200) DEFAULT NULL,
  `biz_theme_id` varchar(50) DEFAULT NULL,
  `biz_is_zero` varchar(50) DEFAULT NULL,
  `cron` varchar(20) DEFAULT NULL,
  `biz_group` varchar(500) DEFAULT NULL COMMENT '计算项分组[{code:value,name:value},{code:value,name:value}]',
  `biz_theme_exp` varchar(2000) DEFAULT NULL COMMENT '指标计算公式[{code:value,name:value,exp:value},{code:value,name:value,exp:value}]',
  `group_flag` varchar(20) DEFAULT NULL COMMENT '是否根据模板关联对象进行分组0：否1：是',
  `creater` varchar(255) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`item_id`),
  KEY `template_id` (`template_id`),
  CONSTRAINT `monitor_items_ibfk_1` FOREIGN KEY (`template_id`) REFERENCES `monitor_template` (`template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 mirror.monitor_log_theme_flush_time 结构
DROP TABLE IF EXISTS `monitor_log_theme_flush_time`;
CREATE TABLE IF NOT EXISTS `monitor_log_theme_flush_time` (
  `flush_time` varchar(50) DEFAULT NULL COMMENT '刷新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='日志主题处理刷新时间';

-- 数据导出被取消选择。
-- 导出  表 mirror.monitor_template 结构
DROP TABLE IF EXISTS `monitor_template`;
CREATE TABLE IF NOT EXISTS `monitor_template` (
  `template_id` varchar(50) NOT NULL COMMENT '模版ID',
  `name` varchar(100) NOT NULL COMMENT '模版名称',
  `description` varchar(4000) NOT NULL COMMENT '描述',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `type` varchar(20) NOT NULL COMMENT '模版类型：\r\n1-硬件\r\n2-网络\r\n3-主机操作系统\r\n4-应用\r\n',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `fun_type` varchar(20) NOT NULL COMMENT '功能类型：\r\n1-监控\r\n2-巡检',
  `mon_type` varchar(20) DEFAULT NULL COMMENT '监控类型:1系统2业务',
  `status` varchar(20) DEFAULT NULL COMMENT '模版状态',
  `sys_type` varchar(20) DEFAULT NULL COMMENT '系统类型 ZABBIX PROMETHEUS THEME MIRROR',
  PRIMARY KEY (`template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 mirror.monitor_template_data_sync 结构
DROP TABLE IF EXISTS `monitor_template_data_sync`;
CREATE TABLE IF NOT EXISTS `monitor_template_data_sync` (
  `sync_id` int(32) NOT NULL AUTO_INCREMENT COMMENT '同步',
  `sync_data_type` varchar(50) NOT NULL COMMENT '同步数据类型',
  `data_id` varchar(50) NOT NULL COMMENT '同步数据ID',
  `operate_type` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`sync_id`)
) ENGINE=InnoDB AUTO_INCREMENT=806 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 mirror.monitor_template_device 结构
DROP TABLE IF EXISTS `monitor_template_device`;
CREATE TABLE IF NOT EXISTS `monitor_template_device` (
  `template_device_id` varchar(50) NOT NULL COMMENT '模版设备关系ID',
  `template_id` varchar(50) NOT NULL COMMENT '模版ID',
  `device_id` varchar(50) NOT NULL COMMENT '设备ID',
  PRIMARY KEY (`template_device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 mirror.monitor_template_object 结构
DROP TABLE IF EXISTS `monitor_template_object`;
CREATE TABLE IF NOT EXISTS `monitor_template_object` (
  `template_object_id` varchar(50) NOT NULL COMMENT '模版设备关系ID',
  `template_id` varchar(50) NOT NULL COMMENT '模版ID',
  `object_type` varchar(50) NOT NULL COMMENT '关联对象类型\r\n1-设备ID\r\n2-业务系统',
  `object_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`template_object_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 mirror.monitor_theme_key 结构
DROP TABLE IF EXISTS `monitor_theme_key`;
CREATE TABLE IF NOT EXISTS `monitor_theme_key` (
  `theme_id` varchar(50) DEFAULT NULL COMMENT '主题ID',
  `dim_code` varchar(100) DEFAULT NULL COMMENT '维度编码'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 mirror.monitor_triggers 结构
DROP TABLE IF EXISTS `monitor_triggers`;
CREATE TABLE IF NOT EXISTS `monitor_triggers` (
  `trigger_id` varchar(50) NOT NULL COMMENT '触发器ID',
  `name` varchar(4000) NOT NULL COMMENT '触发器名称',
  `expression` varchar(4000) NOT NULL COMMENT '表达式',
  `url` varchar(2000) DEFAULT NULL COMMENT 'URL',
  `status` varchar(20) NOT NULL COMMENT '状态：\r\nON-启用\r\nOFF-禁用',
  `value` varchar(20) DEFAULT NULL COMMENT '值类型\r\nOK\r\nPROBLEM\r\nUNKNOWN',
  `priority` varchar(20) NOT NULL COMMENT '优先级\r\n0-Not classified\r\n1 -Information\r\n2-Warning\r\n3-Average\r\n4-High\r\n5-Disaster\r\n',
  `item_id` varchar(50) NOT NULL COMMENT '监控项ID',
  `param` varchar(2000) NOT NULL COMMENT '脚本类监控触发器的参数值',
  PRIMARY KEY (`trigger_id`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `monitor_triggers_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `monitor_items` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;