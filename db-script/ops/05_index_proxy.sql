-- --------------------------------------------------------
-- 主机:                           10.12.70.40
-- 服务器版本:                        5.7.26-log - MySQL Community Server (GPL)
-- 服务器操作系统:                      Linux
-- HeidiSQL 版本:                  9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

use index_proxy;

-- 导出  表 index_proxy.cache_map 结构
DROP TABLE IF EXISTS `cache_map`;
CREATE TABLE IF NOT EXISTS `cache_map` (
  `cache_key` varchar(260) NOT NULL COMMENT '缓存键',
  `cache_value` text NOT NULL COMMENT '缓存值',
  PRIMARY KEY (`cache_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 index_proxy.data_sync_mark 结构
DROP TABLE IF EXISTS `data_sync_mark`;
CREATE TABLE IF NOT EXISTS `data_sync_mark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_identity` varchar(30) NOT NULL,
  `sync_seq` int(11) DEFAULT '0',
  `last_sync_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 index_proxy.monitor_events 结构
DROP TABLE IF EXISTS `monitor_events`;
CREATE TABLE IF NOT EXISTS `monitor_events` (
  `event_id` varchar(50) NOT NULL COMMENT '序列号',
  `source_type` varchar(20) NOT NULL COMMENT '事件来源：\r\nTRIGGERS-触发器\r\nDISCOVERY-新发现\r\nREGISTRATION-自动注册（agent/proxy）',
  `source_id` varchar(50) NOT NULL COMMENT 'source_type对应的id',
  `source` varchar(4000) DEFAULT NULL COMMENT '保留字段',
  `object_type` varchar(20) NOT NULL COMMENT '1-设备\r\n2-业务系统',
  `object_id` varchar(50) NOT NULL COMMENT 'object_type对应的object的ID',
  `object` text COMMENT 'object_id相关的扩展存储属性\r\n相关业务使用',
  `value` varchar(20) NOT NULL COMMENT '事件类型\r\n1-异常\r\n0-正常',
  `acknowledged` varchar(20) NOT NULL COMMENT '确认标识：\r\n0-未确认\r\n1-已确认',
  `clock` int(10) NOT NULL COMMENT '事件产生时间',
  `ns` int(10) NOT NULL COMMENT '纳秒\r\n小于秒的部分',
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 index_proxy.sync_monitor_actions 结构
DROP TABLE IF EXISTS `sync_monitor_actions`;
CREATE TABLE IF NOT EXISTS `sync_monitor_actions` (
  `action_id` varchar(50) NOT NULL COMMENT '事件ID',
  `name` varchar(500) NOT NULL COMMENT '事件名',
  `event_source` varchar(20) NOT NULL COMMENT '事件来源\r\n0-指来源为触发器trigger\r\n1-指来源为自动发现descover\r\n2-指来源为自动登记auto_register\r\n3-为网络发现产生的事件源\r\n',
  `eval_type` varchar(20) DEFAULT NULL COMMENT '表示执行action的前提条件的逻辑关系\r\n0表示and/or\r\n1表示and\r\n2表示or\r\n',
  `status` varchar(20) NOT NULL COMMENT '状态\r\nON-启动\r\nOFF-禁用',
  `type` varchar(20) NOT NULL COMMENT '类型\r\n1-回调url\r\n2-函数',
  `dealer` varchar(500) NOT NULL COMMENT '处理程序\r\n如果type为1，则为url\r\n如果type为2，则为类名.方法名\r\n入参包含事件、指标、触发器信息，需定义',
  `trigger_id` varchar(50) NOT NULL COMMENT '触发器ID',
  `event_type` int(11) NOT NULL COMMENT '事件类型：\r\n1-异常事件\r\n2-正常事件\r\n3-通用事件',
  PRIMARY KEY (`action_id`),
  KEY `trigger_id` (`trigger_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 index_proxy.sync_monitor_items 结构
DROP TABLE IF EXISTS `sync_monitor_items`;
CREATE TABLE IF NOT EXISTS `sync_monitor_items` (
  `item_id` varchar(50) NOT NULL COMMENT '监控项ID',
  `name` varchar(100) DEFAULT NULL COMMENT '监控项名称',
  `type` varchar(20) DEFAULT NULL COMMENT '监控项类型\r\nSCRIPT-脚本（现有）\r\nINDEX-监控指标（现有）',
  `template_id` varchar(50) DEFAULT NULL COMMENT '模版ID',
  `key` varchar(100) DEFAULT NULL COMMENT '监控键值',
  `delay` int(8) DEFAULT NULL COMMENT '监控周期，单位分钟',
  `history` int(8) DEFAULT NULL COMMENT '保留时间，单位天',
  `status` varchar(20) DEFAULT NULL COMMENT '状态：\r\nON-启用\r\nOFF-禁用\r\nNONSUPPORT-不支持（启用状态的监控项采集不到数据）（暂时不用）',
  `value_type` varchar(20) DEFAULT NULL COMMENT '监控项数据格式：\r\nFLOAT-浮点数\r\nSTR-字符串\r\nLOG-日志\r\nUINT64-整数\r\nTEXT-文本',
  `units` varchar(20) DEFAULT NULL COMMENT '单位',
  `error` varchar(4000) DEFAULT NULL COMMENT '错误信息',
  `data_type` varchar(20) DEFAULT NULL COMMENT '监控项数据类型\r\nDECIMAL-十进制\r\nOCTAL-八进制\r\nHEXADECIMAL-十六进制\r\nBOOLEAN-布尔值\r\n',
  `sys_type` varchar(50) DEFAULT NULL COMMENT '接入监控系统类型（目前为zabbix）\r\nMIRROR\r\nZABBIX\r\nNONE',
  PRIMARY KEY (`item_id`),
  KEY `template_id` (`template_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 index_proxy.sync_monitor_template 结构
DROP TABLE IF EXISTS `sync_monitor_template`;
CREATE TABLE IF NOT EXISTS `sync_monitor_template` (
  `template_id` varchar(50) NOT NULL COMMENT '模版ID',
  `name` varchar(100) DEFAULT NULL COMMENT '模版名称',
  `description` varchar(4000) DEFAULT NULL COMMENT '描述',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `type` varchar(20) DEFAULT NULL COMMENT '模版类型：\\r\\n1-硬件\\r\\n2-网络\\r\\n3-主机操作系统\\r\\n4-应用\\r\\n',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `fun_type` varchar(20) DEFAULT NULL COMMENT '功能类型：\\r\\n1-监控\\r\\n2-巡检',
  `sys_type` varchar(20) DEFAULT NULL COMMENT '系统类型 ZABBIX PROMETHEUS THEME MIRROR SCRIPT',
  PRIMARY KEY (`template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 index_proxy.sync_monitor_triggers 结构
DROP TABLE IF EXISTS `sync_monitor_triggers`;
CREATE TABLE IF NOT EXISTS `sync_monitor_triggers` (
  `trigger_id` varchar(50) NOT NULL COMMENT '触发器ID',
  `name` varchar(4000) DEFAULT NULL COMMENT '触发器名称',
  `expression` varchar(4000) DEFAULT NULL COMMENT '表达式',
  `url` varchar(2000) DEFAULT NULL COMMENT 'URL',
  `status` varchar(20) DEFAULT NULL COMMENT '状态：\\r\\nON-启用\\r\\nOFF-禁用',
  `value` varchar(20) DEFAULT NULL COMMENT '值类型\r\nOK\r\nPROBLEM\r\nUNKNOWN',
  `priority` varchar(20) DEFAULT NULL COMMENT '优先级\\r\\n0-Not classified\\r\\n1 -Information\\r\\n2-Warning\\r\\n3-Average\\r\\n4-High\\r\\n5-Disaster\\r\\n',
  `item_id` varchar(50) DEFAULT NULL COMMENT '监控项ID',
  `param` varchar(2000) DEFAULT NULL COMMENT '脚本类监控触发器的参数值',
  PRIMARY KEY (`trigger_id`),
  KEY `item_id` (`item_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `data_sync_mark` (`item_identity`, `sync_seq`, `last_sync_time`) VALUES ('monitor_actions', 0, '2019-07-12 18:11:23');
INSERT INTO `data_sync_mark` (`item_identity`, `sync_seq`, `last_sync_time`) VALUES ('monitor_items', 0, '2020-04-03 10:15:11');
INSERT INTO `data_sync_mark` (`item_identity`, `sync_seq`, `last_sync_time`) VALUES ('monitor_triggers', 0, '2020-04-03 10:15:12');
INSERT INTO `data_sync_mark` (`item_identity`, `sync_seq`, `last_sync_time`) VALUES ('monitor_template', 0, '2020-04-03 10:15:14');

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
