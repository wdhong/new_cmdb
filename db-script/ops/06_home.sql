ALTER TABLE `inspection_report`
ADD COLUMN `biz_status`  varchar(20) NULL COMMENT '业务状态  成功0，失败1，运行中2';

ALTER TABLE `inspection_report`
ADD COLUMN `total_time`  float(10,3) NULL COMMENT '总耗时（s）';


CREATE TABLE `inspection_report_item_ops_ext` (
	`report_item_ext_id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '序列号',
	`report_item_id` BIGINT(20) NOT NULL COMMENT '报告元素序列号',
	`device_class` VARCHAR(255) NULL DEFAULT NULL COMMENT '设备大类',
	`device_type` VARCHAR(255) NULL DEFAULT NULL COMMENT '设备子类',
	`log` TEXT NULL COMMENT '日志',
	`agent_ip` VARCHAR(50) NULL DEFAULT NULL COMMENT 'agent设备IP',
	`exec_status` VARCHAR(20) NULL DEFAULT NULL COMMENT '脚本执行状态0是失败1是成功2是触发器计算完成',
	PRIMARY KEY (`report_item_ext_id`),
	INDEX `inspection_report_item_ext_ibfk_1` (`report_item_id`),
	CONSTRAINT `inspection_report_item_ext_ibfk_1` FOREIGN KEY (`report_item_id`) REFERENCES `inspection_report_item` (`report_item_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

ALTER TABLE  `ops_auto_repair_execute_log` ADD UNIQUE `uk_autorepair_currPipeInst`(`current_pipeline_instance_id`);
ALTER TABLE `ops_spectre_host` ADD COLUMN `status` varchar(12) DEFAULT NULL COMMENT 'agent连接状态；NORMAL:正常  UNLINK:未连接';
