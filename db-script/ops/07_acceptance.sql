use deploy_proxy;
ALTER TABLE  `ops_auto_repair_execute_log` ADD UNIQUE `uk_autorepair_currPipeInst`(`current_pipeline_instance_id`);
ALTER TABLE `ops_spectre_host` ADD COLUMN `status` varchar(12) DEFAULT NULL COMMENT 'agent连接状态；NORMAL:正常  UNLINK:未连接';

ALTER TABLE `ops_script`
ADD COLUMN `script_use_desc`  varchar(1000) NULL COMMENT '脚本使用说明';

ALTER TABLE `ops_step`
ADD COLUMN `file_type`  TINYINT(1) NULL COMMENT '文件类型 0：普通 1:4A';

ALTER TABLE `ops_step_instance`
ADD COLUMN `file_type`  TINYINT(1) NULL COMMENT '文件类型 0：普通 1:4A';

ALTER TABLE `ops_step_instance`
ADD COLUMN `script_sudo`  TINYINT(4) NULL COMMENT '是否需要使用sudo执行脚本  0 不使用  1  使用';


ALTER TABLE `ops_step`
MODIFY COLUMN `ops_type`  tinyint(4) NOT NULL COMMENT '操作类型：0 脚本执行  1 文件下发 2 结果文件存储' AFTER `name`,
ADD COLUMN `file_store_source`  longtext NULL COMMENT 'agent文件下载源(内容为json数组)' AFTER `file_target_path`;

ALTER TABLE `ops_step_instance`
MODIFY COLUMN `ops_type`  tinyint(4) NOT NULL COMMENT '操作类型：0 脚本执行  1 文件下发 2 结果文件存储' AFTER `name`,
ADD COLUMN `file_store_source`  longtext NULL COMMENT 'agent文件下载源(内容为json数组)' AFTER `file_target_path`;


ALTER TABLE `ops_step`
ADD COLUMN `script_sudo`  tinyint NULL COMMENT '是否需要使用sudo执行脚本  0 不使用  1  使用' AFTER `script_param`;

ALTER TABLE `ops_step_instance`
ADD COLUMN `script_sudo`  tinyint NULL COMMENT '是否需要使用sudo执行脚本  0 不使用  1  使用' AFTER `script_param`;


ALTER TABLE `ops_sensitive_config`
ADD COLUMN `path`  varchar(512) NULL COMMENT '命令路径';

ALTER TABLE `ops_step_instance`
ADD COLUMN `aspnode_result`  TINYINT(4) NULL COMMENT '结果状态 0 成功  1 失败';

ALTER TABLE `ops_pipeline_instance`
ADD COLUMN `aspnode_result`  TINYINT(4) NULL COMMENT '结果状态 0 成功  1 失败';

ALTER TABLE `ops_pipeline_instance`
ADD COLUMN `output_path`  VARCHAR(512) NULL COMMENT '产出物地址';

CREATE TABLE `ops_pipeline_instance_log` (
	`pipeline_instance_id` BIGINT(20) NOT NULL COMMENT '作业实例序列号',
	`log_path` VARCHAR(255) NULL DEFAULT NULL COMMENT '作业历史日志路径',
	`status` TINYINT(4) NULL DEFAULT NULL COMMENT '状态 0：未生成 1：已生成'
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

use mirror;
ALTER TABLE `inspection_report`
ADD COLUMN `biz_status`  varchar(20) NULL COMMENT '业务状态  成功0，失败1，运行中2';

ALTER TABLE `inspection_report`
ADD COLUMN `total_time`  float(10,3) NULL COMMENT '总耗时（s）';


CREATE TABLE `inspection_report_item_ops_ext` (
	`report_item_ext_id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '序列号',
	`report_item_id` BIGINT(20) NOT NULL COMMENT '报告元素序列号',
	`device_class` VARCHAR(255) NULL DEFAULT NULL COMMENT '设备大类',
	`device_type` VARCHAR(255) NULL DEFAULT NULL COMMENT '设备子类',
	`log` TEXT NULL COMMENT '日志',
	`agent_ip` VARCHAR(50) NULL DEFAULT NULL COMMENT 'agent设备IP',
	`exec_status` VARCHAR(20) NULL DEFAULT NULL COMMENT '脚本执行状态0是失败1是成功2是触发器计算完成',
	PRIMARY KEY (`report_item_ext_id`),
	INDEX `inspection_report_item_ext_ibfk_1` (`report_item_id`),
	CONSTRAINT `inspection_report_item_ext_ibfk_1` FOREIGN KEY (`report_item_id`) REFERENCES `inspection_report_item` (`report_item_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

ALTER TABLE `inspection_report_item_ops_ext`
ADD COLUMN `device_name`  varchar(512) NULL COMMENT '主机名';

ALTER TABLE `inspection_report_item_ops_ext`
ADD COLUMN `idc_type`  varchar(512) NULL COMMENT '资源池';

ALTER TABLE `inspection_report_item_ops_ext`
ADD COLUMN `biz_system`  varchar(512) NULL COMMENT '业务系统';


use mirror;
CREATE TABLE `inspection_report_item_value` (
	`report_item_value_id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '序列号',
	`report_item_id` BIGINT(20) NOT NULL COMMENT '报告元素序列号',
	`result_name` VARCHAR(1000) NULL DEFAULT NULL COMMENT '子结果名称描述',
	`result_value` VARCHAR(4000) NULL DEFAULT NULL COMMENT '巡检采集值',
	`result_status` VARCHAR(20) NULL DEFAULT NULL COMMENT '状态：0-正常1-异常2-无结果',
	PRIMARY KEY (`report_item_value_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=49
;

ALTER TABLE `monitor_items`
ADD COLUMN `creater` varchar(255) NULL COMMENT '创建人';

ALTER TABLE `monitor_items`
ADD COLUMN `create_time` DATETIME NULL COMMENT '创建时间';


ALTER TABLE `inspection_report_item_value`
ADD COLUMN `result_desc`  varchar(2000) NULL COMMENT '值描述';

ALTER TABLE `monitor_items`
ADD COLUMN `item_group`  varchar(10) NULL COMMENT '指标项分组';

ALTER TABLE `inspection_report_item_ops_ext`
ADD COLUMN `item_group`  varchar(10) NULL COMMENT '指标项分组';

ALTER TABLE `inspection_report_item_ops_ext`
ADD COLUMN `expression`  varchar(255) NULL COMMENT '触发表达式';

ALTER TABLE `inspection_report_item_ops_ext`
ADD COLUMN `item_name`  varchar(512) NULL COMMENT '指标名称';

INSERT INTO `code_dict` (`id`, `code_type`, `key`, `value`, `code_desc`, `valid_flag`, `order_id`, `parent_id`) VALUES (27, 'item_group', '1', '低', '脚本巡检指标分组', '1', 1, NULL);
INSERT INTO `code_dict` (`id`, `code_type`, `key`, `value`, `code_desc`, `valid_flag`, `order_id`, `parent_id`) VALUES (28, 'item_group', '2', '中', '脚本巡检指标分组', '1', 2, NULL);
INSERT INTO `code_dict` (`id`, `code_type`, `key`, `value`, `code_desc`, `valid_flag`, `order_id`, `parent_id`) VALUES (29, 'item_group', '3', '高', '脚本巡检指标分组', '1', 3, NULL);


INSERT INTO `code_dict` (`code_type`, `key`, `value`, `code_desc`, `valid_flag`, `order_id`, `parent_id`) VALUES ('vulnerability_rule_item', 'service_name', '服务名', '漏洞规则元素', '1', 1, NULL);
INSERT INTO `code_dict` (`code_type`, `key`, `value`, `code_desc`, `valid_flag`, `order_id`, `parent_id`) VALUES ('vulnerability_rule_item', 'port', '端口', '漏洞规则元素', '1', 2, NULL);
INSERT INTO `code_dict` (`code_type`, `key`, `value`, `code_desc`, `valid_flag`, `order_id`, `parent_id`) VALUES ('vulnerability_rule_item', 'name', '漏洞名称', '漏洞规则元素', '1', 3, NULL);
INSERT INTO `code_dict` (`code_type`, `key`, `value`, `code_desc`, `valid_flag`, `order_id`, `parent_id`) VALUES ('vulnerability_rule_item', 'id', '漏洞编号', '漏洞规则元素', '1', 4, NULL);

ALTER TABLE `inspection_report`
ADD COLUMN `result`  varchar(1000) NULL COMMENT '报告结果';

use index_proxy;
ALTER TABLE `sync_monitor_items`
ADD COLUMN `sys_type`  varchar(50) NULL COMMENT '接入监控系统类型（目前为zabbix）\r\nMIRROR\r\nZABBIX\r\nNONE';

use deploy_proxy;
CREATE TABLE `ops_param` (
	`param_id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '序列号',
	`param_code` VARCHAR(128) NOT NULL COMMENT '参数编码',
	`param_name` VARCHAR(512) NOT NULL COMMENT '参数名称',
	`param_desc` VARCHAR(1000) NULL DEFAULT NULL COMMENT '参数描述',
	`param_type` VARCHAR(10) NULL DEFAULT NULL COMMENT '参数类型  1常量 2密码变量 ',
	`length` int(11) NULL DEFAULT NULL COMMENT '参数值长度',
	`param_default_value` VARCHAR(20) NULL DEFAULT NULL COMMENT '参数默认值',
	PRIMARY KEY (`param_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `ops_file` (
	`file_id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '序列号',
	`file_name` VARCHAR(128) NOT NULL COMMENT '文件名称',
	`file_version` VARCHAR(128) NOT NULL COMMENT '文件版本',
	`file_type` VARCHAR(10) NOT NULL COMMENT '文件类型 1直接使用、2拆分文件、3加密文件',
	`file_generate_type` VARCHAR(10) NOT NULL COMMENT '生成类型1本地上传/2自动生成',
	`file_class` VARCHAR(10) NULL DEFAULT NULL COMMENT '文件分类 1基线文件/2账号文件/3巡检报告文件/4汇总结果保存文件/5日志文件/9其他',
	`file_path` VARCHAR(512) NULL DEFAULT NULL COMMENT '文件地址',
	`file_desc` VARCHAR(512) NULL DEFAULT NULL COMMENT '文件描述',
	`creater` VARCHAR(64) NULL DEFAULT NULL COMMENT '创建人',
	`create_time` DATETIME NULL DEFAULT NULL COMMENT '创建时间',
	`last_updater` VARCHAR(64) NULL DEFAULT NULL COMMENT '最后更新人',
	`last_update_time` DATETIME NULL DEFAULT NULL COMMENT '最后更新时间',
	`process_rule` VARCHAR(1000) NULL DEFAULT NULL COMMENT '处理规则，如何拆分，如何加密，后续扩展',
	`relation_id` VARCHAR(100) NULL DEFAULT NULL COMMENT '关联id(巡检报告文件为巡检报告id,汇总结果/日志文件则是实例id)',
	`relation_name` VARCHAR(512) NULL DEFAULT NULL COMMENT '扩展一个关联内容名称描述',
	PRIMARY KEY (`file_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

ALTER TABLE `ops_script`
ADD COLUMN `ops_param_code`  varchar(512) NULL COMMENT '引用的自定义参数';

ALTER TABLE `ops_script`
ADD COLUMN `package_password`  varchar(100) NULL COMMENT '引用密码参数，默认系统生成加密产出，需要用户提供加密串';


ALTER TABLE `ops_step_instance`
ADD COLUMN `ops_param_code`  varchar(512) NULL COMMENT '引用的自定义参数';

ALTER TABLE `ops_step_instance`
ADD COLUMN `package_password`  varchar(100) NULL COMMENT '引用密码参数，默认系统生成加密产出，需要用户提供加密串';

ALTER TABLE `ops_host_step_operate_log`
modify COLUMN `aspnode_msg`  varchar(5000) NULL COMMENT 'aspnode message ';

CREATE TABLE `ops_param_value` (
	`param_value_id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '序列号',
	`param_code` VARCHAR(128) NOT NULL COMMENT '参数编码',
	`param_value` VARCHAR(512) NOT NULL COMMENT '参数值',
	`step_instance_id` BIGINT(20) NULL DEFAULT NULL COMMENT '步骤id',
	`pipeline_instance_id` BIGINT(20) NULL DEFAULT NULL COMMENT '作业实例id',
	`is_valid` VARCHAR(10) NULL DEFAULT NULL COMMENT '0无效/1生效',
	`agent_ip` VARCHAR(50) NULL DEFAULT NULL COMMENT 'proxyId:IP',
	`old_param_value` VARCHAR(512) NULL DEFAULT NULL COMMENT '上一次生效参数值',
	PRIMARY KEY (`param_value_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;

DROP VIEW IF EXISTS `v_base_info`;
create view v_base_info as
SELECT 'script' AS `object_type`,`ops_script`.`script_id` AS `object_id`,`ops_script`.`script_name` AS `object_name`,`ops_script`.`creater` AS `creater`,`ops_script`.`create_time` AS `create_time`,`ops_script`.`updater` AS `updater`,`ops_script`.`update_time` AS `update_time`
FROM `ops_script` UNION
SELECT 'pipeline' AS `object_type`,`ops_pipeline`.`pipeline_id` AS `object_id`,`ops_pipeline`.`name` AS `object_name`,`ops_pipeline`.`creater` AS `creater`,`ops_pipeline`.`create_time` AS `create_time`,`ops_pipeline`.`updater` AS `updater`,`ops_pipeline`.`update_time` AS `update_time`
FROM `ops_pipeline` UNION
SELECT 'yum' AS `object_type`,`ops_yum_file`.`id` AS `object_id`,`ops_yum_file`.`name` AS `object_name`,`ops_yum_file`.`creater` AS `creater`,`ops_yum_file`.`create_time` AS `create_time`,`ops_yum_file`.`last_updater` AS `updater`,`ops_yum_file`.`last_update_time` AS `update_time`
FROM `ops_yum_file` UNION
SELECT 'scenes' AS `object_type`,`ops_pipeline_scenes`.`pipeline_scenes_id` AS `object_id`,`ops_pipeline_scenes`.`scenes_name` AS `object_name`,`ops_pipeline_scenes`.`creater` AS `creater`,`ops_pipeline_scenes`.`create_time` AS `create_time`,`ops_pipeline_scenes`.`updater` AS `updater`,`ops_pipeline_scenes`.`update_time` AS `update_time`
FROM `ops_pipeline_scenes` UNION
SELECT 'ap_scheme' AS `object_type`,`ops_auto_repair_scheme`.`scheme_id` AS `object_id`,`ops_auto_repair_scheme`.`scheme_name` AS `object_name`,`ops_auto_repair_scheme`.`creater` AS `creater`,`ops_auto_repair_scheme`.`create_time` AS `create_time`,`ops_auto_repair_scheme`.`updater` AS `updater`,`ops_auto_repair_scheme`.`update_time` AS `update_time`
FROM `ops_auto_repair_scheme`UNION
SELECT 'file' AS `object_type`,`ops_file`.`file_id` AS `object_id`,`ops_file`.`file_name` AS `object_name`,`ops_file`.`creater` AS `creater`,`ops_file`.`create_time` AS `create_time`,`ops_file`.`last_updater` AS `updater`,`ops_file`.`last_update_time` AS `update_time`
FROM `ops_file`;

ALTER TABLE `ops_step`
ADD COLUMN `file_store_converge_type`  tinyint NULL COMMENT '文件结果汇聚类型  0：不汇聚  1：追加汇聚（同文件名内容追加汇总）2：分类汇聚（按指定规则汇聚，暂按特定规则处理基线shadow文件）';

ALTER TABLE `ops_step`
ADD COLUMN `file_store_safety`  tinyint NULL COMMENT '0 不安全  1安全';

INSERT INTO `deploy_proxy`.`ops_label` (`code`, `label`, `description`) VALUES ('vulnerability', 'vulnerability', 'vulnerability');

INSERT INTO `ops_vul_pool_code_name` (`pool_code`, `pool_name`) VALUES ('xxg', '信息港资源池');
INSERT INTO `ops_vul_pool_code_name` (`pool_code`, `pool_name`) VALUES ('hachi', '哈尔滨资源池');
INSERT INTO `ops_vul_pool_code_name` (`pool_code`, `pool_name`) VALUES ('huchi', '呼和浩特资源池');
INSERT INTO `ops_vul_pool_code_name` (`pool_code`, `pool_name`) VALUES ('feichi', '业支域非池化');



INSERT INTO `sys_menu` (`id`, `parent_id`, `name`, `icon`, `menu_type`, `base`, `path`, `component`, `url`, `sort`, `is_show`, `system_id`, `del_status`, `creater`, `create_time`, `editer`, `update_time`) VALUES ('047839c5-6b3b-43ea-9cfe-026a7375fea3', 'ec691b52-d3dc-4787-8008-545a24b7739c', '文件管理', '', 'routers', '/', '', '', '', 4582, '1', 'fc57247e-bcc2-4773-8033-d0e01e7a3a74', '1', 'alauda', '2020-06-15 16:36:41', 'alauda', '2020-06-15 16:55:29');
INSERT INTO `sys_menu` (`id`, `parent_id`, `name`, `icon`, `menu_type`, `base`, `path`, `component`, `url`, `sort`, `is_show`, `system_id`, `del_status`, `creater`, `create_time`, `editer`, `update_time`) VALUES ('64fe2b7e-a11a-4863-8a6b-fcf767d04357', '047839c5-6b3b-43ea-9cfe-026a7375fea3', '文件管理', '', 'vue', '', 'file_manage', 'index.vue', '', 4582, '1', 'fc57247e-bcc2-4773-8033-d0e01e7a3a74', '1', 'alauda', '2020-06-15 16:37:17', 'alauda', '2020-06-15 16:56:26');

INSERT INTO `resource_schema` (`resource`, `general`, `created_at`, `name`, `parent_resource`) VALUES ('047839c5-6b3b-43ea-9cfe-026a7375fea3', 'f', '2020-06-15 16:36:41.421000', '文件管理', 'ec691b52-d3dc-4787-8008-545a24b7739c');

INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('047839c5-6b3b-43ea-9cfe-026a7375fea3', 'opsFile:view', 'ops文件查看', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('047839c5-6b3b-43ea-9cfe-026a7375fea3', 'opsFile:create', 'ops文件新增', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('047839c5-6b3b-43ea-9cfe-026a7375fea3', 'opsFile:delete', 'ops文件删除', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('047839c5-6b3b-43ea-9cfe-026a7375fea3', 'opsFile:download', 'ops文件下载', 1);
INSERT INTO `resource_schema_actions` (`resource`, `action`, `action_name`, `action_type`) VALUES ('047839c5-6b3b-43ea-9cfe-026a7375fea3', 'opsFile:upload', 'ops文件上传', 1);

