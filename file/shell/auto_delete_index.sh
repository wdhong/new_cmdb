#/bin/bash

host='http://127.0.0.1:9200/'
echo -e "执行ES清除索引开始，es请求地址：${host}"

#日志、配置文件副本保留7天
#指定日期(7天前)
DATA=`date -d "7 day ago" +%Y%m%d`
curl -XPUT ${host}log*${DATA}*/_settings -H 'Content-Type: application/json' -d '{"number_of_replicas": 0}'
curl -XPUT ${host}*config-network*${DATA}*/_settings -H 'Content-Type: application/json' -d '{"number_of_replicas": 0}'
echo -e "更新log*${DATA}* 副本数为0"
echo -e "更新config*${DATA}* 副本数为0"

#指定日期(15天前)
DATA=`date -d "15 day ago" +%Y%m%d`
#关闭15天前的日志、配置文件
curl -XPOST ${host}log*${DATA}*/_close
curl -XPOST ${host}*config-network*${DATA}*/_close
echo -e "关闭log*${DATA}*索引"
echo -e "关闭config*${DATA}*索引"

#history指标数据副本保留1个月
DATA=`date -d "30 day ago" +%Y%m%d`
curl -XPUT ${host}history*${DATA}*/_settings -H 'Content-Type: application/json' -d '{"number_of_replicas": 0}'
echo -e "更新history*${DATA}* 副本数为0"
curl -XPUT ${host}kpi*${DATA}*/_settings -H 'Content-Type: application/json' -d '{"number_of_replicas": 0}'
echo -e "更新kpi*${DATA}* 副本数为0"

#关闭2个月前的监控数据
DATA=`date -d "60 day ago" +%Y%m%d`
curl -XPOST ${host}history*${DATA}*/_close
echo -e "关闭history*${DATA}*索引"
curl -XPOST ${host}kpi*${DATA}*/_close
echo -e "关闭kpi*${DATA}*索引"

#删除13个月前的监控、日志、配置文件数据
DATA=`date -d "395 day ago" +%Y%m%d`
curl -XPOST ${host}*${DATA}*/_open
echo -e "打开*${DATA}*索引"

curl -XDELETE ${host}*${DATA}*
echo -e "删除*${DATA}*索引"

