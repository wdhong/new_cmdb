#/bin/bash

#迁移脚本执行之前需确认准备迁移的索引全部处于open状态
#迁移目标地址
host='http://10.12.70.40:9201'
#迁移源地址
sourceHost='http://10.12.70.40:9200'
echo -e "迁移开始，es目标地址：${host}，es源地址：${sourceHost}"

if [ -n "$1" ]; then
        curl -XPOST ${sourceHost}/$1/_open
        echo -e "执行 curl -XPOST ${sourceHost}/$1/_open 打开索引"
        RESULT=$(curl -XGET ${sourceHost}/_cat/indices/$1)
        echo -e $RESULT
        i=0
        for var in ${RESULT[@]}
        do
           if [ "$var" = "open" ]; then
              i=1
           else
              if [ $i -eq 1 ]; then
                echo -e ""
                echo -e $var
				#逐条迁移每个索引的数据
				resp=`curl -XPOST ${host}/_reindex -H 'Content-Type: application/json' -d '{"source": {"remote": {"host": "'$sourceHost'","socket_timeout": "30s","connect_timeout": "30s"},"index": "'$var'","size": 5000},"dest": {"index": "'$var'"}}'`
				echo $resp
              fi
              i=0
           fi
        done
else
        echo -e "没有可执行的索引参数，脚本执行结束。"
fi
