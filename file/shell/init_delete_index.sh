#/bin/bash

host='http://127.0.0.1:9200/'
echo -e "执行ES清除索引开始，es请求地址：${host}"
# 更新副本数为0
for ((i=1; i<=395; i ++))
do
	DATA=`date -d "$i day ago" +%Y%m%d`
#	if [ $i -gt 7 ]; then 
#		#更新7天前的索引副本数为0
#		curl -XPUT ${host}log*${DATA}*/_settings -H 'Content-Type: application/json' -d '{"number_of_replicas": 0}'
#		curl -XPUT ${host}config*${DATA}*/_settings -H 'Content-Type: application/json' -d '{"number_of_replicas": 0}'
#		echo -e "更新log*${DATA}* 副本数为0"
#		echo -e "更新config*${DATA}* 副本数为0"
#	fi
#	if [ $i -gt 30 ]; then 
#		curl -XPUT ${host}history*${DATA}*/_settings -H 'Content-Type: application/json' -d '{"number_of_replicas": 0}'
#		echo -e "更新history*${DATA}* 副本数为0"
#		curl -XPUT ${host}kpi*${DATA}*/_settings -H 'Content-Type: application/json' -d '{"number_of_replicas": 0}'
#		echo -e "更新kpi*${DATA}* 副本数为0"
#	fi
done

# 关闭索引
for ((i=1; i<=395; i ++))
do
	DATA=`date -d "$i day ago" +%Y%m%d`
	if [ $i -gt 15 ]; then 
		#关闭15天前的日志、配置文件
		curl -XPOST ${host}log*${DATA}*/_close
		curl -XPOST ${host}config*${DATA}*/_close
		echo -e "关闭log*${DATA}*索引"
		echo -e "关闭config*${DATA}*索引"
	fi
	if [ $i -gt 60 ]; then 
		curl -XPOST ${host}history*${DATA}*/_close
		echo -e "关闭history*${DATA}*索引"
		curl -XPOST ${host}kpi*${DATA}*/_close
		echo -e "关闭kpi*${DATA}*索引"
	fi
done

#删除超过13个月的历史数据
for ((i=395; i<=600; i ++))
do
	DATA=`date -d "$i day ago" +%Y%m%d`
	curl -XPOST ${host}*${DATA}*/_open
	echo -e "打开*${DATA}*索引"

	curl -XDELETE ${host}*${DATA}*
	echo -e "删除*${DATA}*索引"
done
