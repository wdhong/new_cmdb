#!/usr/bin/env bash

config_jar=config-server-0.0.1-SNAPSHOT.jar
eureka_war=eureka.war
theme_war=theme.war
template_war=template.war
proxy_war=proxy.war
common_war=common.war
composite_war=composite.war
alert_war=alert.war

root_path="/Users/liu.mac/liu.soft/gitcom/aspire/MIGUMS_PROJECT/mirror-master"
confg_path=${config_jar}/mirror-public/config-server/target
eureka_path=${eureka_war}/mirror-public/config-server/target
theme_path=${theme_war}/mirror-public/config-server/target
template_path=${template_war}/mirror-public/config-server/target
proxy_path=${proxy_war}/mirror-public/config-server/target
common_path=${common_war}/mirror-public/config-server/target
composite_path=${composite_war}/mirror-public/config-server/target
alert_path=${alert_war}/mirror-public/config-server/target

cd ${root_path}
#mvn clean install

# config
scp ${confg_path}/${config_jar} webbasbm@10.12.3.123:~/apache-tomcat-8.5.42/webapps
