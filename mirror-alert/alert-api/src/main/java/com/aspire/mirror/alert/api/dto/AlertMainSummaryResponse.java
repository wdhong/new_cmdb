package com.aspire.mirror.alert.api.dto;

import com.aspire.mirror.alert.api.dto.model.AlertStatisticSummaryDTO;
import lombok.Data;

@Data
public class AlertMainSummaryResponse {

    private String deviceClass;

    private AlertStatisticSummaryDTO alertStatisticSummaryDTO;
}
