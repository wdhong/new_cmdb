package com.aspire.mirror.alert.api.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class AlertRecord {
	private String idcType;

	private String alertLevel;
	
	//Map<String,Integer> deviceTypeMap = new HashMap<>();

	private Integer physicalMachineCount;

	private Integer firewallCount;

	private Integer routerCount;

	private Integer switchDeviceCount;

	private Integer cloudStorageCount;

	private Integer sdnControllerCount;

	private Integer diskArrayCount;

	private Integer tapeLibraryCount;
	
	private String month;
}
