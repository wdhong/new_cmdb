package com.aspire.mirror.alert.api.dto;

import lombok.Data;

import java.util.Date;

@Data
public class AlertWorkConfig {

    private String uuid;
    private String dayStartTme;
    private String dayEndTme;
    private String nightStartTme;
    private String nightEndTme;
}
