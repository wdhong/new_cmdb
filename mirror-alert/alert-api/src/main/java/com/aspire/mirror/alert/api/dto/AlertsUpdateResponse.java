package com.aspire.mirror.alert.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 告警修改返回
 * <p>
 * 项目名称:  mirror平台
 * 包:        com.aspire.mirror.alert.api.dto
 * 类名称:    AlertsUpdateResponse.java
 * 类描述:    告警修改返回
 * 创建人:    JinSu
 * 创建时间:  2018/9/19 11:09
 * 版本:      v1.0
 */
@NoArgsConstructor
@Data
public class AlertsUpdateResponse {
    @JsonProperty("alert_id")
    private String alertId;

    /** 告警结束时间 */
    @JsonProperty("alert_end_time")
    private Date alertEndTime;


    /** 1-未派单
     2-处理中
     3-已完成
     */
    @JsonProperty("order_status")
    private String orderStatus;
}
