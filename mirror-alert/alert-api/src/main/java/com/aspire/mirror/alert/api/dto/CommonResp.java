package com.aspire.mirror.alert.api.dto;

import lombok.Data;

@Data
public class CommonResp {
    private String code = "0000";
    private String message = "success";
}
