package com.aspire.mirror.alert.api.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@NoArgsConstructor
public class CycReportResFileRequest {
    private String fileName;
    private int fileType;
    private String filePath;
    private Date startTime;
    private String sessionId;
    private Integer requestId;
    private int readStatus;
}
