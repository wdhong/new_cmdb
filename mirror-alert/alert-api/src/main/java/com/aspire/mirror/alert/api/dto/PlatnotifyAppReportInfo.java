package com.aspire.mirror.alert.api.dto;

import lombok.Data;
@Data
public class PlatnotifyAppReportInfo {
	PlatnotifyAppReport device_info;
}
