package com.aspire.mirror.alert.api.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class ProcessLoadResponse {
    private String runId;
    private String actInstId;
    private boolean result;
    private String message;
    private String status;
}
