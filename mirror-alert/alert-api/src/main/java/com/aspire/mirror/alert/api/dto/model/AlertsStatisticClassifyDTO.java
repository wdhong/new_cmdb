package com.aspire.mirror.alert.api.dto.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AlertsStatisticClassifyDTO {
    private String deviceType;

    private Integer alertNum;
}
