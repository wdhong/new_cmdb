package com.aspire.mirror.alert.api.dto.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AlertsStatisticSourceClassifyDTO {

    private String idcType;

    private String sourceRoom;

    private Integer alertNum;
}
