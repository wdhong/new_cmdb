package com.aspire.mirror.alert.api.dto.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AlertsTop10DTOResponse {

    private String colName;

    private double rate;//内容占比

    private Integer count;
}
