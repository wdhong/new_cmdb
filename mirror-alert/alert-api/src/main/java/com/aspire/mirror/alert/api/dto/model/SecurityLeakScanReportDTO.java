package com.aspire.mirror.alert.api.dto.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SecurityLeakScanReportDTO {
    private String id;
    private String scanId;
    private String ip;
    private String reportPath;
    private Integer highLeaks;
    private Integer mediumLeaks;
    private Integer lowLeaks;
    private Double riskVal;
}
