package com.aspire.mirror.alert.api.service;

import java.util.Map;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 自动告警服务
 * <p>
 * 项目名称:  mirror平台
 * 包:        com.aspire.mirror.alert.api.metrics
 * 类名称:    AutoAlertsService.java
 * 类描述:    自动告警服务
 * 创建人:    JinSu
 * 创建时间:  2018/9/19 11:09
 * 版本:      v1.0
 */
public interface AutoAlertsService {
	
	
    /**
     * 自动派单
     */
    @PostMapping(value = "/v1/auotoalerts/autoSendInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "自动派单", notes = "自动派单", tags = {"AutoAlerts API"})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "返回", response = Map.class),
            @ApiResponse(code = 500, message = "Unexpected error", response = Map.class)})
    @ResponseBody
    Map<String,Object> autoSendInfo();
    
    
    /**
     * 前端页面告警配置组合信息入库
     */
    @PostMapping(value = "/v1/auotoalerts/insertAutoAlarmConfig", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "前端页面告警配置组合信息入库", notes = "前端页面告警配置组合信息入库", tags = {"AutoAlerts API"})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "返回", response = Object[].class),
            @ApiResponse(code = 500, message = "Unexpected error", response = Object[].class)})
    @ResponseBody
    Object[] insertAutoAlarmConfig(@RequestBody Map<String, String> params);
    
}