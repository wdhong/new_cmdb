package com.aspire.mirror.alert.api.service;

import com.aspire.mirror.alert.api.dto.CycReportResFileRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
@Api(value = "资源上报数据接口")
@RequestMapping("/v1/alerts/cycReport/")
public interface CycReportResService {
    /**
     * 保存数据文件名元数据到数据库
     *
     * @return AlertsSummaryResponse 告警概览
     */
    @PostMapping(value = "/saveReportFtpFiles", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "保存数据文件名到数据库", notes = "保存数据文件名到数据库", tags = {"CycReportResService API"})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "返回"),
            @ApiResponse(code = 500, message = "Unexpected error")})
    void saveReportFtpFiles(@RequestBody List<CycReportResFileRequest> filelist);
}
