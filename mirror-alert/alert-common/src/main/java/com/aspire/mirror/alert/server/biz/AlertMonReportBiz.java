package com.aspire.mirror.alert.server.biz;

import java.util.List;
import java.util.Map;

public interface AlertMonReportBiz {

    Map<String,Object> viewByIdcType(Map<String, String> map);

    List<Map<String,Object>> viewByIp(Map<String, String> map);

    List<Map<String,Object>> viewByKeyComment(Map<String, String> map);
}
