package com.aspire.mirror.alert.server.biz;


import com.aspire.mirror.alert.server.domain.AlertScanComparisionDetailDTO;
import com.aspire.mirror.alert.server.domain.AlertScanComparisionReqDTO;
import com.aspire.mirror.common.entity.PageResponse;

import java.util.List;
import java.util.Map;

public interface AlertScanComparisionBiz {

    PageResponse<AlertScanComparisionDetailDTO> getScanComparisionList(AlertScanComparisionReqDTO request);

    void deleteScanComparisionById(List<String> request);

    void synScanComparision(List<Map<String,String>> request);

    List<Map<String, Object>> exportScanComparision(AlertScanComparisionReqDTO request);

    void alertScanComparisonSchedule();

    void compareCmdb();
}
