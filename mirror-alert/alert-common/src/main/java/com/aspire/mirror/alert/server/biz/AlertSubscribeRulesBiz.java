package com.aspire.mirror.alert.server.biz;

import com.aspire.mirror.alert.server.dao.po.*;
import com.aspire.mirror.common.entity.PageResponse;

import java.util.List;
import java.util.Map;

public interface AlertSubscribeRulesBiz {
    /**
     * 订阅告警管理的查询
     *
     * @return
     */
    PageResponse<AlertSubscribeRules> query(AlertSubscribeRules alertSubscribeRules);

    PageResponse<AlertSubscribeRules> queryRules(AlertSubscribeRules alertSubscribeRules);

    void deteleRules(List<String> idlist);


    void deleteSubscribeRulesManagement(List<String> idlist);

    void updateRules(List<String> idlist, String isOpen);

    List<ExportAlertSubscribeRulesManagement> export(List<String> idlist);

    List<AlertSubscribeRulesManagement> querySubscribeRules();

    void CreateSubscribeRules(CreateAlertSubscribeRules createAlertSubscribeRules);

    void UpdateSubscribeRules(UpdateAlertSubscribeRules updateAlertSubscribeRules);

    AlertSubscribeRulesDetailShow GetSubscribeRulesById(String id);

    List<AlertSubscribeRules> queryAlertSubscribeRules();
}
