package com.aspire.mirror.alert.server.biz;

import com.aspire.mirror.alert.server.dao.po.AlertVoiceNotifyDetail;
import com.aspire.mirror.alert.server.dao.po.AlertVoiceNotifyReqDTO;
import com.aspire.mirror.alert.server.domain.AlertVoiceNotifyContentReqDTO;
import org.springframework.http.ResponseEntity;

public interface AlertVoiceNotifyBiz {

    String createdAlertVoiceNotify(AlertVoiceNotifyReqDTO request);

    AlertVoiceNotifyDetail getAlertVoiceNotify(String creator);

    ResponseEntity<String> getVoiceContent(AlertVoiceNotifyContentReqDTO request);

}
