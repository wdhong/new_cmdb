package com.aspire.mirror.alert.server.biz;

import com.aspire.mirror.alert.server.biz.model.AlertBpmStartCallBack;
import com.aspire.mirror.alert.server.biz.model.AlertClearCountModel;
import com.aspire.mirror.alert.server.dao.po.Alerts;
import com.aspire.mirror.alert.server.dao.po.AlertsDetail;
import com.aspire.mirror.alert.server.dao.po.AlertsNotify;
import com.aspire.mirror.alert.server.dao.po.AlertsRecord;
import com.aspire.mirror.alert.server.domain.AlertsDTO;
import com.aspire.mirror.alert.server.domain.AlertsOperationRequestDTO;
import com.aspire.mirror.alert.server.domain.NotifyPageDTO;
import com.aspire.mirror.common.entity.PageRequest;
import com.aspire.mirror.common.entity.PageResponse;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 告警业务层接口
 * <p>
 * 项目名称:  mirror平台
 * 包:        com.aspire.mirror.alert.server.biz.impl
 * 类名称:    AlertsBiz.java
 * 类描述:    告警业务层接口
 * 创建人:    JinSu
 * 创建时间:  2018/9/14 15:55
 * 版本:      v1.0
 */
public interface AlertsBiz {
    /**
     * 创建告警
     *
     * @param alertsDTO 告警对象
     * @return String 告警ID
     */
    String insert(AlertsDTO alertsDTO);

    /**
     * 修改感觉数据
     *
     * @param alertsDTO 告警修改对象
     * @return 影响数据条数
     */
    int updateByPrimaryKey(AlertsDTO alertsDTO);

    /**
     * 根据主键查询alert
     *
     * @param alertId 告警ID
     * @return AlertsDTO 告警对象
     */
    AlertsDTO selectAlertByPrimaryKey(String alertId);

    /**
     * 根据条件查询
     * @param alertQuery
     * @return
     */
    List<AlertsDTO> select(Alerts alertQuery);
    
   
    /**
     * 告警上报分页
     *
     * @param alertId 告警Id
     * @return
     */
    PageResponse<AlertsDetail> alertGenerateListByPage(String alertId, String pageNo, String pageSize);


	/**
     * 告警操作分页
     *
     * @param alertId 告警Id
     * @return
     */
	PageResponse<AlertsRecord> alertRecordListByPage(String alertId, String pageNo, String pageSize);


	/**
     * 告警通知分页
     *
     * @param alertId 告警Id
     * @return
     */
	NotifyPageDTO<AlertsNotify> alertNotifyListByPage(String alertId, String pageNo, String pageSize, String reportType);


    /**
     * 告警 相关告警
     *
     * @param alertId 告警ID集合
     * @return List<AlertsDTO> 告警列表
     */
    List<AlertsDTO> selectAlertGenerateList(String alertId);


    /**
     * 根据主键查询alertRecord
     *
     * @param alertId 告警ID
     * @return alertRecord 告警对象
     */
    List<AlertsRecord> selectAlertRecordByPrimaryKey(String alertId);

    /**
     * 根据主键查询alertNotify
     *
     * @param alertId 告警ID
     * @return alertNotify 告警对象
     */
    List<AlertsNotify> selectalertNotifyByPrimaryKey(String alertId);



    /**
     * 告警列表
     *
     * @param page 告警查询page对象
     * @return PageResponse<AlertsDTO> 告警分页返回对象
     */
    PageResponse<AlertsDTO> pageList(PageRequest page);

    /**
     * 根据告警ID集合删除告警记录
     *
     * @param alertIdArrays 告警ID集合
     * @return int 影响数据条数
     */
    int deleteByPrimaryKeyArrays(String[] alertIdArrays);

    /**
     * 批量新增
     *
     * @param alertsDTOList 新增告警列表
     * @return 返回告警列表
     */
    List<AlertsDTO> insertByBatch(List<AlertsDTO> alertsDTOList);

    /**
     * 告警列表查询
     *
     * @param alertIdArrays 告警ID集合
     * @return List<AlertsDTO> 告警列表
     */
    List<AlertsDTO> selectByPrimaryKeyArrays(String[] alertIdArrays);


    /**
     * 告警修改备注. <br/>
     * <p>
     * 作者： pengguihua
     *
     * @return void 告警修改备注
     */
    void  updateNote(String alertId, String note);


    /**
     * 告警转派. <br/>
     * <p>
     * 作者： pengguihua
     *
     * @return void 告警转派
     */
    void  alertTransfer(String namespace, String alertIds, String userIds);


    /**
     * 告警确认. <br/>
     * <p>
     * 作者： pengguihua
     *
     * @return void 告警确认
     */
    void  alertConfirm(AlertsOperationRequestDTO request);




    /**
     * 查询所有手工清除告警记录对象. <br/>
     * <p>
     * 作者： pengguihua
     *
     * @return List<AlertClearCountModel> 清除告警结果
     */
    List<AlertClearCountModel> listAllAlertsManualClearModels();

    /**
     * 手动派单
     *
     * @param alertIds 告警ID集合
     * @param orderType
     */
    String genOrder(String namespace, String alertIds, Integer orderType);

    /**
     * 派单分流
     * @param namespace
     * @param alertIds
     * @param orderType
     * @return
     */
    AlertBpmStartCallBack switchOrder(String namespace, String alertIds, Integer orderType);
	 /**
     * 触发发送邮件
     */
    void recordEmailNotify(String namespace, String alertIds, String[] destin, String message, String status);


    /**
     * 触发发送短信
     */
    void recordSMSNotify(String namespace, String alertIds, String[] destin, String message, String status);



    /**
     * 根据工单ID修改工单状态
     *
     * @param orderId     工单ID
     * @param orderStatus 工单状态
     */
    void modOrderStatusByOrderId(String orderId, String orderStatus);

    /**
     * 查询. <br/>
     * <p>
     * 作者： pengguihua
     *
     * @param alertClearModel alertClearModel
     * @return AlertClearCountModel
     */
    AlertClearCountModel getAlertClearCount(AlertClearCountModel alertClearModel);

    /**
     * 新增. <br/>
     * <p>
     * 作者： pengguihua
     *
     * @param alertClearModel alertClearModel
     */
    void insertAlertClearCount(AlertClearCountModel alertClearModel);

    /**
     * 递增手工清除count. <br/>
     * <p>
     * 作者： pengguihua
     *
     * @param alertClearModel alertClearModel
     */
    void updateAlertClearCount(AlertClearCountModel alertClearModel);

    List<Map<String,Object>> getAlertConfig();

	List<Map<String, String>> getFirstBusiness(Map<String, Object> param);

    /**
     * 根据告警id删除上报记录
     * @param alertId
     */
	void deleteAlertsDetail(String alertId);

    /**
     * 新增告警上报记录
     * @param alertDetail
     */
	void insertAlertsDetail(AlertsDetail alertDetail);

    /**
     * 提供bpm侧接口，同步告警事件
     * @param oldOrderId
     * @param orderId
     * @param type
     * @param orderStatus
     * @param userName
     * @return
     */
    String upgrade(String oldOrderId, String orderId, String type,String orderStatus, String userName);

    /**
     * 根据id更新告警监控时间
     * @param alertId
     * @param curMoniTime
     */
    void updateCurMoniTime(String alertId, Date curMoniTime);

    int selectAlert(PageRequest pageRequest);

    int getAlertValue(Map<String,List<String>> ipMap, List<String> alertLevel, List<String> itemIdList);
}
