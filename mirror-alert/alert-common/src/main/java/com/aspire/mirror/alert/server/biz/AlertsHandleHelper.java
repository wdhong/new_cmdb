package com.aspire.mirror.alert.server.biz;

import com.aspire.mirror.alert.server.constant.Constants;
import com.aspire.mirror.alert.server.dao.*;
import com.aspire.mirror.alert.server.dao.po.*;
import com.aspire.mirror.alert.server.domain.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * 告警处理帮助类, 告警、自动消警、手工消警  都封装在此类中    <br/>
 * Project Name:alert-metrics
 * File Name:AlertsHandleHelper.java
 * Package Name:com.aspire.mirror.alert.server.biz
 * ClassName: AlertsHandleHelper <br/>
 * date: 2018年10月12日 上午10:40:07 <br/>
 *
 * @author pengguihua
 * @since JDK 1.6
 */
@Slf4j
@Service
public class AlertsHandleHelper {

    @Autowired
    private AlertsRecordDao alertsRecordDao;

    @Autowired
    protected AlertsBiz alertsBiz;
    @Autowired
    protected AlertsHisBiz alertsHisBiz;
    @Autowired
    private AutoConfirmClearDao autoConfirmClearDao;

    @Autowired
    private IBpmTaskService iBpmTaskService;

    /**
     * 手工清除指定告警id的告警. <br/>
     * <p>
     * 作者： pengguihua
     */
    @Transactional
    public void manualClear(AlertsOperationRequestDTO request) {
        String[] alertIdArrays = request.getAlertIds().split(",");
        if (alertIdArrays.length > 0) {
            List<AlertsDTO> alertList = alertsBiz.selectByPrimaryKeyArrays(alertIdArrays);
            this.manualClear(alertList, request.getNamespace(), request.getContent());
            for (AlertsDTO alertsDTO : alertList) {
                AlertsRecord alertsRecord = new AlertsRecord();
                alertsRecord.setAlertId(alertsDTO.getAlertId());
                alertsRecord.setUserName(request.getNamespace());
                alertsRecord.setOperationType("3");
                alertsRecord.setContent(request.getContent());
                alertsRecord.setOperationStatus("1");
                alertsRecordDao.insert(alertsRecord);
                if (request.getAutoType() != -1) {
                    AutoConfirmClearDTO autoConfirmClearId = autoConfirmClearDao.getAutoConfirmClearId(
                            alertsDTO.getDeviceIp(),
                            alertsDTO.getIdcType(),
                            alertsDTO.getBizSys(),
                            alertsDTO.getAlertLevel(),
                            alertsDTO.getSource(),
                            alertsDTO.getItemId(),
                            request.getAutoType(),
                            null);
                    if (null == autoConfirmClearId) {
                        AutoConfirmClearDTO autoConfirmClearDTO = new AutoConfirmClearDTO();
                        autoConfirmClearDTO.setUuid(UUID.randomUUID().toString());
                        autoConfirmClearDTO.setDeviceIp(alertsDTO.getDeviceIp());
                        autoConfirmClearDTO.setIdcType(alertsDTO.getIdcType());
                        autoConfirmClearDTO.setBizSys(alertsDTO.getBizSys());
                        autoConfirmClearDTO.setAlertLevel(alertsDTO.getAlertLevel());
                        autoConfirmClearDTO.setSource(alertsDTO.getSource());
                        autoConfirmClearDTO.setItemId(alertsDTO.getItemId());
                        autoConfirmClearDTO.setAutoType(request.getAutoType());
                        autoConfirmClearDTO.setContent(request.getContent());
                        autoConfirmClearDTO.setStartTime(request.getStartTime());
                        autoConfirmClearDTO.setEndTime(request.getEndTime());
                        autoConfirmClearDTO.setOperator(request.getNamespace());
                        autoConfirmClearDao.insert(autoConfirmClearDTO);
                    }
                }
            }
        }

    }

    /**
     * 自动消警
     */
    public void autoClearAlert(final List<AlertsDTO> alertList, String namespace, String content) {
        this.manualClear(alertList, namespace, content);
    }

    /**
     * 手工清除告警.  <br/>
     * <p>
     * 作者： pengguihua
     *
     * @param alertList
     */
    private void manualClear(final List<AlertsDTO> alertList, String namespace, String content) {

        Date nowTime = new Date();
        List<String> alertIdList = new ArrayList<>();
        String[] ids = new String[alertList.size()];
        for (int i = 0; i < alertList.size(); i++) {
            AlertsDTO alert = alertList.get(i);
            AlertsHisDTO hisDto = new AlertsHisDTO();
            BeanUtils.copyProperties(alert, hisDto);
//            hisDto.setClearTime(nowTime);// 手工消警时, 设置此字段
            hisDto.setAlertEndTime(nowTime);
            hisDto.setClearUser(namespace);
            hisDto.setClearContent(content);
            if (StringUtils.isNotEmpty(hisDto.getOrderId()) && Constants.ORDER_DEALING.equals(hisDto.getOrderStatus())) {
                String message = iBpmTaskService.closeBpmInstance(hisDto.getOrderId(), content);
                if ("ERROR".equals(message)) {
                    hisDto.setOrderStatus(Constants.ORDER_ERROR);
                } else {
                    hisDto.setOrderStatus(Constants.ORDER_END);
                }
            }
            alertsHisBiz.insert(hisDto);
            ids[i] = alert.getAlertId();
        }
        alertsBiz.deleteByPrimaryKeyArrays(ids);
    }

}
