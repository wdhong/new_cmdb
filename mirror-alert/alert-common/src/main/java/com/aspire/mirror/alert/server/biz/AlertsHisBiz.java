package com.aspire.mirror.alert.server.biz;

import com.aspire.mirror.alert.server.dao.po.AlertsDetail;
import com.aspire.mirror.alert.server.dao.po.AlertsHis;
import com.aspire.mirror.alert.server.domain.AlertsHisDTO;
import com.aspire.mirror.common.entity.PageRequest;
import com.aspire.mirror.common.entity.PageResponse;

import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestParam;

/**
 * 告警业务层接口
 * <p>
 * 项目名称:  mirror平台
 * 包:        com.aspire.mirror.alert.server.biz.impl
 * 类名称:    AlertsHisBiz.java
 * 类描述:    告警业务层接口
 * 创建人:    JinSu
 * 创建时间:  2018/9/14 15:55
 * 版本:      v1.0
 */
public interface AlertsHisBiz {
    /**
     * 创建告警
     *
     * @param alertsDTO 告警对象
     * @return String 告警ID
     */
    String insert(AlertsHisDTO alertsDTO);

    /**
     * 根据主键查询
     *
     * @param alertId 告警ID
     * @return AlertsHisDTO 告警对象
     */
    AlertsHisDTO selectByPrimaryKey(String alertId);
 
   
    
    /**
     * 告警上报分页
     *
     * @param alertId 告警Id
     * @return
     */
    public PageResponse<AlertsDetail> alertGenerateListByPage(String alertId, String pageNo, String pageSize);



    /**
     * 历史告警相关告警
     *
     * @param alertId 告警ID集合
     * @return List<AlertsDTO> 告警列表
     */
    List<AlertsHisDTO> selectAlertGenerateList(String alertId);



    /**
     * 告警列表
     *
     * @param page 告警查询page对象
     * @return
     */
    PageResponse<AlertsHisDTO> pageList(PageRequest page);

    PageResponse<AlertsHisDTO> getAlertHisList(PageRequest page);

    /**
     * 批量创建历史告警
     *
     * @param alertsDTOList 需新增历史告警列表数据
     * @return
     */
    List<AlertsHisDTO> insertByBatch(List<AlertsHisDTO> alertsDTOList);


    /**
     * 告警修改备注. <br/>
     * <p>
     * 作者： pengguihua
     *
     * @return void 告警修改备注
     */
    void  updateNote(String alertId, String note);

    /**
     * 根据条件查询
     * @param alertHisQuery
     * @return
     */
    List<AlertsHisDTO> select(AlertsHis alertHisQuery);
    /**
     * @param alertsHisDTO 告警修改对象
     * @return
     */
    int updateByPrimaryKey(AlertsHisDTO alertsHisDTO);

    PageResponse<Map<String, Object>> alertRelateData(int alertType,String alertId,Integer pageSize,Integer pageNo);
}
