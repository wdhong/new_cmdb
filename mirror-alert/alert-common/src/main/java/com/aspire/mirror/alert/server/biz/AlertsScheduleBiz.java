package com.aspire.mirror.alert.server.biz;

import com.aspire.mirror.alert.server.v2.dao.po.AlertFieldRequestDTO;

import java.util.List;
import java.util.Map;

public interface AlertsScheduleBiz {

    void alert(String startTime,String endTime,String month);

    void device(String startTime,String endTime,String month);

    void alertIndex(String startTime,String endTime,String month);

    void alertSum(String startTime,String endTime,String month);

    void synchronize();

    void insertCmdb (Map<String, Object> ciMap, List<Map<String, Object>> list, List<AlertFieldRequestDTO> modelFromRedis);

    void deviceInspectionByDay();

    void bizSystemInspectionByDay();
}
