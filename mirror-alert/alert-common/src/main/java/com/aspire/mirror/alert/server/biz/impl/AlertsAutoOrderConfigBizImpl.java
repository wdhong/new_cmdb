package com.aspire.mirror.alert.server.biz.impl;

import com.aspire.mirror.alert.server.biz.AlertsAutoOrderConfigBiz;
import com.aspire.mirror.alert.server.biz.model.AlertBpmStartCallBack;
import com.aspire.mirror.alert.server.constant.Constants;
import com.aspire.mirror.alert.server.dao.AlertsAutoOrderConfigDao;
import com.aspire.mirror.alert.server.domain.AlertAutoOrderConfigDetailDTO;
import com.aspire.mirror.alert.server.domain.AlertAutoOrderConfigReqDTO;
import com.aspire.mirror.alert.server.domain.AlertAutoOrderLogDTO;
import com.aspire.mirror.alert.server.domain.AlertAutoOrderLogReqDTO;
import com.aspire.mirror.alert.server.util.CommonUtil;
import com.aspire.mirror.alert.server.util.PayloadParseUtil;
import com.aspire.mirror.alert.server.util.StringUtils;
import com.aspire.mirror.alert.server.v2.biz.AlertsBizV2;
import com.aspire.mirror.alert.server.v2.constant.AlertConfigConstants;
import com.aspire.mirror.common.constant.Constant;
import com.aspire.mirror.common.entity.PageResponse;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;


@Slf4j
@Service
public class AlertsAutoOrderConfigBizImpl implements AlertsAutoOrderConfigBiz {

    @Autowired
    private AlertsAutoOrderConfigDao dao;
    @Autowired
    private AlertsBizV2 alertsBizV2;
//    @Autowired
//    private RedisTemplate<String, Object> redisTemplate;

    @Override
    public PageResponse<AlertAutoOrderConfigDetailDTO> getAlertAutoOrderConfigList(String configName,
                                                                                   String isOpen,
                                                                                   String startTime,
                                                                                   String endTime,
                                                                                   String orderType,
                                                                                   String orderTimeInterval,
                                                                                   Integer pageNum,
                                                                                   Integer pageSize) {
        PageResponse<AlertAutoOrderConfigDetailDTO> response = new PageResponse<AlertAutoOrderConfigDetailDTO>();
        response.setCount(dao.getAlertAutoOrderConfigCount(configName, isOpen, startTime, endTime, orderType, orderTimeInterval,pageNum, pageSize));
        List<AlertAutoOrderConfigDetailDTO> alertAutoOrderConfigList =
                dao.getAlertAutoOrderConfigList(configName, isOpen, startTime, endTime, orderType, orderTimeInterval, pageNum, pageSize);
        alertAutoOrderConfigList.forEach(item -> {
            switch (item.getOrderType()) {
                case Constants.ORDER_ALERT:
                    item.setOrderType(Constant.ALERT_ORDER);
                    break;
                case Constants.ORDER_HITCH:
                    item.setOrderType(Constant.HITCH_ORDER);
                    break;
                case Constants.ORDER_MAINTENANCE:
                    item.setOrderType(Constant.MAINTENANCE_ORDER);
                    break;
                case Constants.ORDER_TUNING:
                    item.setOrderType(Constant.TUNING_ORDER);
                    break;
                default: // 其它工单类型
                    break;
            }

        });
        response.setResult(alertAutoOrderConfigList);
        return response;
    }

    @Override
    public void createAlertAutoOrderConfig(AlertAutoOrderConfigReqDTO request) {
        request.setUuid(UUID.randomUUID().toString());
        dao.createAlertAutoOrderConfig(request);
    }

    @Override
    public String checkName(String configName) {
        AlertAutoOrderConfigDetailDTO alertAutoOrderConfigDetailDTO = dao.checkName(configName);
        return null == alertAutoOrderConfigDetailDTO ? null : alertAutoOrderConfigDetailDTO.getUuid();
    }

    @Override
    public void updateAlertAutoOrderConfig(AlertAutoOrderConfigReqDTO request) {
        dao.updateAlertAutoOrderConfig(request);
    }

    @Override
    public AlertAutoOrderConfigDetailDTO getAlertAutoOrderConfigDetail(String uuid) {
        return dao.getAlertAutoOrderConfigDetail(uuid);
    }

    @Override
    public void deleteAlertAutoOrderConfig(List<String> uuidList) {
        dao.deleteAlertAutoOrderConfig(uuidList);
    }

    @Override
    public void updateAlertAutoOrderConfigStatus(List<String> uuidList,String configStatus) {
        Map<String, Object> map = Maps.newHashMap();
        map.put("uuidList",uuidList);
        map.put("configStatus",configStatus);
        dao.updateAlertAutoOrderConfigStatus(map);
    }

    @Override
    public void copyAlertAutoOrderConfig(String uuid,String userName) {
        AlertAutoOrderConfigDetailDTO alertAutoOrderConfigDetail = dao.getAlertAutoOrderConfigDetail(uuid);
        AlertAutoOrderConfigReqDTO alertAutoOrderConfigReqDTO =
                PayloadParseUtil.fastjsonBaseParse(AlertAutoOrderConfigReqDTO.class, alertAutoOrderConfigDetail);
        alertAutoOrderConfigReqDTO.setUuid(UUID.randomUUID().toString());
        String configName = alertAutoOrderConfigDetail.getConfigName();
        String[] split = configName.split("_");
        SimpleDateFormat SDF2 = new SimpleDateFormat("yyyyMMddHHmmss");
        alertAutoOrderConfigReqDTO.setConfigName(split[0]+ "_" + SDF2.format(new Date()));
        alertAutoOrderConfigReqDTO.setCreator(userName);
        alertAutoOrderConfigReqDTO.setIsOpen("2");
        dao.createAlertAutoOrderConfig(alertAutoOrderConfigReqDTO);
    }

    @Override
    public PageResponse<AlertAutoOrderLogDTO> getAlertAutoOrderLogList(AlertAutoOrderLogReqDTO request) {
        PageResponse<AlertAutoOrderLogDTO> response = new PageResponse<AlertAutoOrderLogDTO>();
        response.setCount(dao.getAlertAutoOrderLogCount(request));
        response.setResult(dao.getAlertAutoOrderLogList(request));
        return response;
    }

    @Override
    public List<Map<String, Object>> exportAlertAutoOrderLogList(AlertAutoOrderLogReqDTO request) {
        return dao.exportAlertAutoOrderLogList(request);
    }

    private final ConcurrentHashMap<String, Date> localMap = new ConcurrentHashMap<>(1);

    @Override
    public ResponseEntity<String> alertAutoOrderSchedule() {
//        if (redisTemplate.hasKey(AlertConfigConstants.REDIS_ALERT_ORDER_LOCK)) {
//            log.info("Last alert order task not finished!");
//            return new ResponseEntity<String>("", HttpStatus.OK);
//        }
//        redisTemplate.opsForValue().set(AlertConfigConstants.REDIS_ALERT_ORDER_LOCK, AlertCommonConstant.NUM.ONE);
//        redisTemplate.expire(AlertConfigConstants.REDIS_ALERT_ORDER_LOCK, 30, TimeUnit.MINUTES);
        Date curTime = new Date();
        Date date = localMap.get(AlertConfigConstants.REDIS_ALERT_ORDER_LOCK);
        if (date != null && date.getTime() >= (curTime.getTime() - 1800 * 1000) ) {
            log.info("Last alert order task not finished!");
            return new ResponseEntity<String>("", HttpStatus.OK);
        }
        localMap.put(AlertConfigConstants.REDIS_ALERT_ORDER_LOCK, curTime);
        String res = "success";
        try {

            List<String> keys = Lists.newArrayList();
            Map<String,AlertAutoOrderLogDTO> alertLists = Maps.newHashMap();

            // 获取处于启用状态的规则
            List<AlertAutoOrderConfigDetailDTO> alertAutoOrderConfigList =
                    dao.getAlertAutoOrderConfigList(null, "1", null, null,null,null, null, null);

            log.info("告警派单生效规则：{}", alertAutoOrderConfigList);
            SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            for (AlertAutoOrderConfigDetailDTO detail : alertAutoOrderConfigList) {
                // 判断规则的生效类型
                if (detail.getConfigTimeType().equals("2")) {

                    Date startTime = SDF.parse(detail.getStartTime());
                    Date endTime = SDF.parse(detail.getEndTime());
                    if (startTime.after(curTime) || endTime.before(curTime)) {
                        continue;
                    }
                }
                // 判断是否处于派单时段
                if (StringUtils.isNotEmpty(detail.getOrderTimeInterval())) {
                    String[] split = detail.getOrderTimeInterval().split("-");
                    String curHour = new SimpleDateFormat("HH:mm").format(curTime);
                    if (split[0].compareTo(curHour) > 0 || split[1].compareTo(curHour) < 0) {
                        continue;
                    }
                }
                // 获取告警数据
                String optionCondition = CommonUtil.getCondition(detail.getAlertFilter());
                Map<String,Object> queryAlertParam = Maps.newHashMap();
                queryAlertParam.put("optionCondition", optionCondition);
//                queryAlertParam.put("startTime", DateUtils.getTime(detail.getOrderTimeSpace()));
//                queryAlertParam.put("endTime", SDF.format(curTime));
                List<Map<String, Object>> alertDataByFilter = dao.getAlertDataByFilter(queryAlertParam);
                log.info("派单规则 ： {} ,查到的符合规则条数是： {}", detail.getConfigName(), alertDataByFilter.size());
                List<AlertAutoOrderLogDTO> alerts = PayloadParseUtil.jacksonBaseParse(AlertAutoOrderLogDTO.class, alertDataByFilter);
                long orderTimeSpace = System.currentTimeMillis() - detail.getOrderTimeSpace() * 60 * 1000;
                // 数据去重，根据派单类型优先级
                for (AlertAutoOrderLogDTO alert : alerts) {
//                    String curMoniTime = alert.getCurMoniTime();
//                    String time = DateUtils.getTime(detail.getOrderTimeSpace());
//                    if (curMoniTime.compareTo(time) > 0) {
//                        continue;
//                    }
                    Date createTime = alert.getCreateTime();
                    // 判断时间
                    if (createTime != null && createTime.getTime() > orderTimeSpace) {
                        continue;
                    }
                    String key = alert.getAlertId();
                    if (keys.contains(key)) {
                        AlertAutoOrderLogDTO alertData = alertLists.get(key);
                        int orderType = Integer.parseInt(alertData.getOrderType());
                        if (orderType <= Integer.parseInt(detail.getOrderType())) {
                            alertData.setOrderType(detail.getOrderType());
                            alertData.setConfigId(detail.getUuid());
                            alertLists.put(key, alertData);
                        }
                    } else {
                        keys.add(key);
                        alert.setOrderType(detail.getOrderType());
                        alert.setConfigId(detail.getUuid());
                        alertLists.put(key, alert);
                    }
                }
            }
            log.info("需要派单的条数是:{}, 告警id是:{}", keys.size(), keys);
            // 派单
            for (String key : keys) {
                try {
                    AlertAutoOrderLogDTO alert = alertLists.get(key);
                    AlertBpmStartCallBack orderMessage = alertsBizV2.switchOrder("alauda", alert.getAlertId(), Integer.parseInt(alert.getOrderType()));
                    int count = dao.getOrderLogCountByAlertIdAndOrderType(alert.getAlertId(), alert.getOrderType());
                    if (StringUtils.isEmpty(orderMessage.getOrderIdList()) && count > 0) {
                        dao.updateOrderTime(alert.getAlertId(), alert.getOrderType());
                    } else {
                        alert.setOrderId(orderMessage.getOrderIdList());
                        alert.setStatus(StringUtils.isNotEmpty(orderMessage.getOrderIdList()) ? "1" : "0");
                        dao.insertAlertOrderConfigLog(alert);
                    }
                } catch (Exception e) {
                    log.error("[AlertAutoOrderSchedule] Error is {}", e);
                }
            }
        } catch (Exception e) {
            log.error("[AlertAutoOrderSchedule] Error is {}", e);
            res = e.toString();
        } finally {
//            redisTemplate.delete(AlertConfigConstants.REDIS_ALERT_ORDER_LOCK);
            localMap.remove(AlertConfigConstants.REDIS_ALERT_ORDER_LOCK);
        }
        return new ResponseEntity<String>(res, HttpStatus.OK);
    }
}
