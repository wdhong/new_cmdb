package com.aspire.mirror.alert.server.biz.impl;

import com.aspire.mirror.alert.server.biz.AlertsBiz;
import com.aspire.mirror.alert.server.biz.IBpmTaskService;
import com.aspire.mirror.alert.server.biz.model.AlertBpmCallBack;
import com.aspire.mirror.alert.server.biz.model.AlertBpmStartCallBack;
import com.aspire.mirror.alert.server.biz.model.AlertClearCountModel;
import com.aspire.mirror.alert.server.constant.AlertCommonConstant;
import com.aspire.mirror.alert.server.constant.Constants;
import com.aspire.mirror.alert.server.dao.*;
import com.aspire.mirror.alert.server.dao.po.*;
import com.aspire.mirror.alert.server.domain.AlertsDTO;
import com.aspire.mirror.alert.server.domain.AlertsOperationRequestDTO;
import com.aspire.mirror.alert.server.domain.AutoConfirmClearDTO;
import com.aspire.mirror.alert.server.domain.NotifyPageDTO;
import com.aspire.mirror.alert.server.util.TransformUtils;
import com.aspire.mirror.common.constant.Constant;
import com.aspire.mirror.common.entity.Page;
import com.aspire.mirror.common.entity.PageRequest;
import com.aspire.mirror.common.entity.PageResponse;
import com.aspire.mirror.common.util.PageUtil;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 告警实现类
 * <p>
 * 项目名称:  mirror平台
 * 包:        com.aspire.mirror.alert.server.biz.impl
 * 类名称:    AlertsBizImpl.java
 * 类描述:    告警实现类
 * 创建人:    JinSu
 * 创建时间:  2018/9/18 16:16
 * 版本:      v1.0
 */
@Slf4j
@Service
@Transactional
public class AlertsBizImpl implements AlertsBiz {

    @Autowired
    private AlertsDao alertsDao;

    @Autowired
    private AlertsRecordDao alertsRecordDao;

    @Autowired
    private AlertsTransferDao alertsTransferDao;

    @Autowired
    private AlertsNotifyDao alertsNotifyDao;

    @Autowired
    private AlertsHisDao alertHisDao;

    @Autowired(required = false)
    private IBpmTaskService iBpmTaskService;

    @Autowired
    private AlertsDetailDao alertsDetailDao;

    @Autowired
    private AlertsStatisticDao alertsStatisticDao;

    @Autowired
    private AutoConfirmClearDao autoConfirmClearDao;

    @Autowired
    private AlertBpmTuningRecordDao alertBpmTuningRecordDao;

    /**
     * 告警转派
     *
     * @param alertIds 派单的告警ID列表
     */
    @Override
    @Transactional
    public void alertTransfer(String namespace, String alertIds, String userIds) {


        String[] alertIdArrays = alertIds.split(",");
        String[] userIdArrays = userIds.split(",");

        if (alertIdArrays.length > 0 && userIdArrays.length > 0) {
            for (int i = 0; i < alertIdArrays.length; i++) {
                String alertId = alertIdArrays[i];

                for (int j = 0; j < userIdArrays.length; j++) {

                    String userId = userIdArrays[j];
                    AlertsTransfer alertsTransfer = new AlertsTransfer();
                    alertsTransfer.setAlertId(alertId);
                    alertsTransfer.setUserName(namespace);
                    alertsTransfer.setConfirmUserName(userId);
                    alertsTransferDao.insert(alertsTransfer);

                }

                AlertsRecord alertsRecord = new AlertsRecord();
                alertsRecord.setAlertId(alertId);
                alertsRecord.setUserName(namespace);
                alertsRecord.setOperationType("0");
                String content = "转派给" + userIds;
                alertsRecord.setContent(content);

                Alerts alerts = alertsDao.selectByPrimaryKey(alertId);
                if (alerts.getOperateStatus() == 0) {

                    alertsRecord.setOperationStatus("1");

                } else {

                    alerts.setOperateStatus(0);
                    int index = alertsDao.updateByPrimaryKey(alerts);

                    if (index == 1) {
                        alertsRecord.setOperationStatus("1");
                    } else {
                        alertsRecord.setOperationStatus("0");
                    }

                }

                //设置为待确认状态

                alertsRecordDao.insert(alertsRecord);

            }

        }


    }

    /**
     * 告警确认
     */
    @Override
    @Transactional
    public void alertConfirm(AlertsOperationRequestDTO request) {
        String[] alertIdArrays = request.getAlertIds().split(",");
        if (alertIdArrays.length > 0) {
            for (String alertId : alertIdArrays) {
                Alerts alerts = alertsDao.selectByPrimaryKey(alertId);
                //设置为已确认状态
                alerts.setOperateStatus(1);
                int index = alertsDao.updateByPrimaryKey(alerts);
                AlertsRecord alertsRecord = new AlertsRecord();
                alertsRecord.setAlertId(alertId);
                alertsRecord.setUserName(request.getNamespace());
                alertsRecord.setOperationType("1");
                alertsRecord.setContent(request.getContent());
                if (index == 1) {
                    alertsRecord.setOperationStatus("1");
                } else {
                    alertsRecord.setOperationStatus("0");
                }
                alertsRecordDao.insert(alertsRecord);
                if (request.getAutoType() != -1) {
                    AutoConfirmClearDTO autoConfirmClearId = autoConfirmClearDao.getAutoConfirmClearId(
                            alerts.getDeviceIp(),
                            alerts.getIdcType(),
                            alerts.getBizSys(),
                            alerts.getAlertLevel(),
                            alerts.getSource(),
                            alerts.getItemId(),
                            request.getAutoType(),
                            null);
                    if (null == autoConfirmClearId) {
                        AutoConfirmClearDTO autoConfirmClearDTO = new AutoConfirmClearDTO();
                        autoConfirmClearDTO.setUuid(UUID.randomUUID().toString());
                        autoConfirmClearDTO.setDeviceIp(alerts.getDeviceIp());
                        autoConfirmClearDTO.setIdcType(alerts.getIdcType());
                        autoConfirmClearDTO.setBizSys(alerts.getBizSys());
                        autoConfirmClearDTO.setAlertLevel(alerts.getAlertLevel());
                        autoConfirmClearDTO.setSource(alerts.getSource());
                        autoConfirmClearDTO.setItemId(alerts.getItemId());
                        autoConfirmClearDTO.setAutoType(request.getAutoType());
                        autoConfirmClearDTO.setContent(request.getContent());
                        autoConfirmClearDTO.setStartTime(request.getStartTime());
                        autoConfirmClearDTO.setEndTime(request.getEndTime());
                        autoConfirmClearDTO.setOperator(request.getNamespace());
                        autoConfirmClearDao.insert(autoConfirmClearDTO);
                    }
                }
            }
        }

    }

    /**
     * 手动派单-调优工单
     * @param namespace
     * @param alertIds
     * @param orderType
     * @return
     */
    private AlertBpmStartCallBack genTuningOder(String namespace, String alertIds, String orderType) {
        AlertBpmStartCallBack callBack = new AlertBpmStartCallBack();
        int successNum = 0;
        String[] alertIdArrays = alertIds.split(",");
        for (String alertId : alertIdArrays) {
            Map param = Maps.newHashMap();
            param.put("alertIdArrays", new String[]{alertId});
            List<Map<String, Object>> list = alertsDao.selectOrderParam1(param);
            if (CollectionUtils.isEmpty(list)) {
                continue;
            }
            AlertTuningRecord recordInDB = alertBpmTuningRecordDao.select(alertId);
            if (recordInDB != null) {
                log.info("此告警已发起过调优工单，跳过!");
                successNum++;
                continue;
            }
            Map<String, Object> alertMap = list.get(0);
            if ("1".equals(alertMap.get("object_type"))) { // 设备类型告警
                AlertBpmCallBack tuningCallBack = iBpmTaskService.callBpmFlowStart(namespace, alertMap, orderType);
                if ("1".equals(tuningCallBack.getStatus())) { // 发起调优工单成功
                    successNum++;
                    String runId = tuningCallBack.getRunId(); // 工单实例ID
                    // 录入调优工单记录表
                    AlertTuningRecord record = new AlertTuningRecord();
                    record.setAlertId(alertId);
                    record.setOrderId(runId); // 工单实例ID
                    record.setOrderType(orderType); // 工单类型
                    record.setOrderStatus(Constant.ORDER_DEALING); // 处理中
                    alertBpmTuningRecordDao.insert(record);
                    // 录入告警操作记录表
                    AlertsRecord alertsRecord = new AlertsRecord();
                    alertsRecord.setAlertId(alertId);
                    alertsRecord.setUserName(namespace);
                    alertsRecord.setOperationType("2"); // 派发工单
                    alertsRecord.setContent(Constant.TUNING_ORDER);
                    alertsRecord.setOperationStatus("1");
                    alertsRecord.setOperationTime(new Date());
                    alertsRecordDao.insert(alertsRecord);
                } else {
                    log.error("告警调优工单发起失败, alertId: {}, {}! ", alertId, tuningCallBack.getMessage());
                    continue;
                }
            } else {
                log.info("非设备类型告警，不能发起告警调优工单，跳过！");
                continue;
            }
        }
        callBack.setTotal(alertIdArrays.length);
        callBack.setSuccess(successNum);
        if (successNum == 0) {
            callBack.setStatus(false);
            callBack.setMessage("工单生成失败");
        }
        return callBack;
    }

    /**
     * 手动派单
     *
     * @param alertIds 派单的告警ID列表
     * @param orderType
     */
    @Override
    public String genOrder(String namespace, String alertIds, Integer orderType) {

        String[] alertIdArrays = alertIds.split(",");

        Map paramMap = Maps.newHashMap();
        // 工单状态：未派单
//        paramMap.put("orderStatus", Constant.ORDER_BEFOR);
        // 告警ID列表
        paramMap.put("alertIdArrays", alertIdArrays);
        List<Map<String, Object>> list = alertsDao.selectOrderParam1(paramMap);
        List<Map<String, Object>> unSend = list.stream().filter(p->p.get("order_status").equals(Constant.ORDER_BEFOR)
                                            ||p.get("order_status").equals("4")).collect(Collectors.toList());
        //根据orderType过滤需要派单的告警 如果orderType=1，则只给未派单的告警派单；
        // 如果orderType=2，则只给未派单的告警或派单类型为告警工单的告警事件派单；
        // 如果orderType=3，则只给未派单的告警或派单类型不是维保工单的告警事件派单。
        List<Map<String, Object>> newList = new ArrayList<>();
        newList.addAll(unSend);
        log.info("#=====> orderType: {}" , orderType);
        if (orderType.toString().equals(Constants.ORDER_HITCH)){
            List<Map<String, Object>> list1 = list.stream().filter(p->p.containsKey("order_type"))
                    .filter(p->p.get("order_type").equals(Constants.ORDER_ALERT)).collect(Collectors.toList());
            newList.addAll(list1);
        }else if (orderType.toString().equals(Constants.ORDER_MAINTENANCE)){
            List<Map<String, Object>> list2 = list.stream().filter(p->p.containsKey("order_type"))
                    .filter(p->!p.get("order_type").equals(Constants.ORDER_MAINTENANCE)).collect(Collectors.toList());
            newList.addAll(list2);
        }
        String message = iBpmTaskService.alertHandleBpmResult(newList, AlertCommonConstant.NUM.ONE, namespace,orderType);
//        List<Map<String, Object>> list = alertsDao.selectOrderParam(paramMap);
//        alertGenOrder.syncOrder(list);

//        for (Map<String, Object> map : list) {
//
//
//        	 AlertsRecord  alertsRecord=new  AlertsRecord();
//
//        	 String contentRecord="告警工单";
//			 String alertId = map.get("alert_id") == null ? map.get("ALERT_ID").toString(): map.get("alert_id").toString();
//        	 alertsRecord.setAlertId( alertId);
//        	 alertsRecord.setUserName(namespace);
//        	 alertsRecord.setOperationType("2");
//        	 alertsRecord.setContent(contentRecord);
//        	 alertsRecord.setOperationStatus("1");
//             alertsRecord.setOperationTime(new Date());
//        	 alertsRecordDao.insert(alertsRecord);
//
//		}
        return message;
    }

    /**
     * 派单分流
     *
     * @param namespace
     * @param alertIds
     * @param orderType
     * @return
     */
    @Override
    public AlertBpmStartCallBack switchOrder(String namespace, String alertIds, Integer orderType) {
        AlertBpmStartCallBack message = null;
        String orderTpye = orderType.toString();
        switch (orderTpye) {
            case Constants.ORDER_TUNING: // 告警调优工单
                message = genTuningOder(namespace, alertIds, orderTpye);
                break;
            default: // 其它工单类型
                String genMessage = genOrder(namespace, alertIds, orderType);
                message = new AlertBpmStartCallBack();
                if (genMessage.length()>8 && genMessage.substring(0,8).equals("success:")) {
//                    String successNum = genMessage.substring(8);
//                    if (StringUtils.isNotEmpty(successNum)) {
//                        message.setSuccess(Integer.valueOf(successNum));
//                    }
                    String successNum = genMessage.substring(8,9);
                    if (StringUtils.isNotEmpty(successNum)) {
                        message.setSuccess(Integer.valueOf(successNum));
                    }
                    if (genMessage.contains("_")) {
                        message.setOrderIdList(genMessage.split("_")[1]);
                    }
                } else { // 失败
                    message.setStatus(false);
                    message.setMessage(genMessage);
                }
                break;
        }
        return message;
    }

    //发送邮件记录
    @Override
    public void recordEmailNotify(String namespace, String alertIds, String[] destination, String message, String status) {

        String[] alertIdArrays = alertIds.split(",");

        if (alertIdArrays.length > 0 && destination.length > 0) {

            for (int i = 0; i < alertIdArrays.length; i++) {

                String alertId = alertIdArrays[i];

                for (int j = 0; j < destination.length; j++) {
                    String dest = destination[j];

                    AlertsNotify alertsNotify = new AlertsNotify();
                    alertsNotify.setAlertId(alertId);
                    alertsNotify.setUserName(namespace);
                    alertsNotify.setReportType("1");     //邮件
                    alertsNotify.setDestination(dest);
                    alertsNotify.setMessage(message);
                    alertsNotify.setStatus(status);

                    alertsNotifyDao.insert(alertsNotify);

                }

                AlertsRecord alertsRecord = new AlertsRecord();

                String contentRecord = "告警邮件";

                alertsRecord.setAlertId(alertId);
                alertsRecord.setUserName(namespace);
                alertsRecord.setOperationType("4");
                alertsRecord.setContent(contentRecord);
                alertsRecord.setOperationStatus(status);
                alertsRecordDao.insert(alertsRecord);

            }

        }

    }

    //发送短信记录
    @Override
    public void recordSMSNotify(String namespace, String alertIds, String[] destination, String message, String status) {

        String[] alertIdArrays = alertIds.split(",");

        if (alertIdArrays.length > 0 && destination.length > 0) {

            for (int i = 0; i < alertIdArrays.length; i++) {

                String alertId = alertIdArrays[i];


                for (int j = 0; j < destination.length; j++) {

                    String dest = destination[j];

                    AlertsNotify alertsNotify = new AlertsNotify();
                    alertsNotify.setAlertId(alertId);
                    alertsNotify.setUserName(namespace);
                    alertsNotify.setReportType("0");    //短信
                    alertsNotify.setDestination(dest);
                    alertsNotify.setMessage(message);
                    alertsNotify.setStatus(status);

                    alertsNotifyDao.insert(alertsNotify);

                }

                AlertsRecord alertsRecord = new AlertsRecord();
                String contentRecord = "告警短信";
                alertsRecord.setAlertId(alertId);
                alertsRecord.setUserName(namespace);
                alertsRecord.setOperationType("4");
                alertsRecord.setContent(contentRecord);
                alertsRecord.setOperationStatus(status);
                alertsRecordDao.insert(alertsRecord);

            }

        }
    }


    /**
     * 告警详情
     *
     * @param alertId 告警ID
     * @return
     */
    @Override
    public AlertsDTO selectAlertByPrimaryKey(String alertId) {
        if (StringUtils.isEmpty(alertId)) {
            LOGGER.warn("method[selectByPrimaryKey] param[alertId] is null");
            return null;
        }
        Alerts alerts = alertsDao.selectByPrimaryKey(alertId);

        if (alerts == null) {
            return null;
        }
        AlertsDTO alertsDTO = TransformUtils.transform(AlertsDTO.class, alerts);
        return alertsDTO;
    }

    /**
     * 根据条件查询
     *
     * @param alertQuery
     * @return
     */
    public List<AlertsDTO> select(Alerts alertQuery) {
        List<Alerts> list = alertsDao.select(alertQuery);
        return TransformUtils.transform(AlertsDTO.class, list);
    }


    //分页查询告警上报记录
    @Override
    public PageResponse<AlertsDetail> alertGenerateListByPage(String alertId, String pageNo, String pageSize) {

        PageResponse<AlertsDetail> pageResponse = new PageResponse<AlertsDetail>();

        Map<String, Object> hashMap = new HashMap<String, Object>();
        int pageSize1 = Integer.valueOf(pageSize);
        int pageNo1 = Integer.valueOf(pageNo);
        hashMap.put("pageNo", (pageNo1 - 1) * pageSize1);
        hashMap.put("pageSize", pageSize1);
        hashMap.put("alertId", alertId);

        int count = alertsDetailDao.countByAlertId(hashMap);
        List<AlertsDetail> alertDetailList = alertsDetailDao.selectByAlertId(hashMap);

//  		List<AlertGenResp> alertGenRespList=new ArrayList<AlertGenResp>();
//        for ( AlertsDetail alertsDetail : alertDetailList ){
//
//        	AlertGenResp alertGenResp=new AlertGenResp();
//
//        	BeanUtils.copyProperties(alertsDetail, alertGenResp);
//
//        	alertGenRespList.add(alertGenResp);
//  		}
        pageResponse.setCount(count);
        pageResponse.setResult(alertDetailList);
        return pageResponse;
    }


    //分页查询告警操作记录
    @Override
    public PageResponse<AlertsRecord> alertRecordListByPage(String alertId, String pageNo, String pageSize) {

        PageResponse<AlertsRecord> pageResponse = new PageResponse<AlertsRecord>();

        Map<String, Object> hashMap = new HashMap<String, Object>();

        int pageSize1 = Integer.valueOf(pageSize);
        int pageNo1 = Integer.valueOf(pageNo);

        hashMap.put("pageNo", (pageNo1 - 1) * pageSize1);
        hashMap.put("pageSize", pageSize1);

        hashMap.put("alertId", alertId);

        int count = alertsRecordDao.getAlertRecordCount(hashMap);

        List<AlertsRecord> alertsRecordList = alertsRecordDao.getAlertRecordByPage(hashMap);

//  		List<AlertRecordResp> alertRecordRespList=new ArrayList<AlertRecordResp>();
//
//        for ( AlertsRecord alertsRecord : alertsRecordList ){
//
//        	AlertRecordResp alertRecordResp=new AlertRecordResp();
//
//        	BeanUtils.copyProperties(alertsRecord, alertRecordResp);
//
//        	alertRecordRespList.add(alertRecordResp);
//  		}
//
        pageResponse.setCount(count);

        pageResponse.setResult(alertsRecordList);

        return pageResponse;


    }


    //分页查询告警通知记录
    @Override
    public NotifyPageDTO<AlertsNotify> alertNotifyListByPage(String alertId, String pageNo, String pageSize,
                                                             String reportType) {

        try {
            NotifyPageDTO<AlertsNotify> pageResponse = new NotifyPageDTO<AlertsNotify>();
            Map<String, Object> hashMap = new HashMap<String, Object>();
            int pageSize1 = Integer.valueOf(pageSize);
            int pageNo1 = Integer.valueOf(pageNo);
            hashMap.put("pageNo", (pageNo1 - 1) * pageSize1);
            hashMap.put("pageSize", pageSize1);
            hashMap.put("alertId", alertId);
            hashMap.put("reportType", reportType);

            if (StringUtils.isEmpty(reportType)) {
                List<String> all = Arrays.asList("1", "2", "3");
                hashMap.put("notifyTypeList", all);
            } else {
                List<String> mobile = Arrays.asList("1");
                List<String> email = Arrays.asList("2", "3");
                hashMap.put("notifyType", "0".equals(reportType) ? mobile : email);
            }
            int sumCount = 0;
            List<Integer> count = alertsNotifyDao.getAlertNotifyCount(hashMap);
            for (Integer integer : count) {
                sumCount += integer;
            }
            List<Integer> success = alertsNotifyDao.getSuccessAlertNotifyCount(hashMap);
            int successCount = 0;
            for (Integer integer : success) {
                successCount += integer;
            }
            List<AlertsNotify> alertsNotifyList = alertsNotifyDao.getAlertNotifyByPage(hashMap);
//            List<AlertNotifyResp> alertNotifyRespList=new ArrayList<AlertNotifyResp>();
//            for ( AlertsNotify alertsNotify : alertsNotifyList ){
//                AlertNotifyResp alertNotifyResp=new AlertNotifyResp();
//                BeanUtils.copyProperties(alertsNotify, alertNotifyResp);
//                alertNotifyRespList.add(alertNotifyResp);
//            }
            pageResponse.setCount(sumCount);
            pageResponse.setSuccessCount(successCount);
            pageResponse.setFallCount(sumCount - successCount);
            pageResponse.setResult(alertsNotifyList);
            return pageResponse;
        } catch (Exception e) {
            log.error("alertNotifyListByPage is error {}", e);
        }
        return null;

    }


    //告警上报记录Excel
    @Override
    public List<AlertsDTO> selectAlertGenerateList(String alertId) {


        Alerts alerts = alertsDao.selectByPrimaryKey(alertId);

        String deviceIp = alerts.getDeviceIp();
        String moniObject = alerts.getMoniObject();
        String alertLevel = alerts.getAlertLevel();


        List<Alerts> alertList = alertsDao.selectAllAlertGenerateList(deviceIp, moniObject, alertLevel);

        if (alertList == null) {
            return null;
        }

        List<AlertsDTO> alertDtolist = new ArrayList<AlertsDTO>();

        for (Alerts alerts1 : alertList) {
            AlertsDTO alertsDTO = TransformUtils.transform(AlertsDTO.class, alerts1);
            alertDtolist.add(alertsDTO);

        }

        return alertDtolist;
    }

    //告警操作记录Excel
    @Override
    public List<AlertsRecord> selectAlertRecordByPrimaryKey(String alertId) {

        if (StringUtils.isEmpty(alertId)) {
            LOGGER.warn("method[selectByPrimaryKey] param[alertId] is null");
            return null;
        }

        List<AlertsRecord> alertsRecords = alertsRecordDao.selectRecordByPrimaryKey(alertId);


        return alertsRecords;

    }

    //告警通知记录Excel
    @Override
    public List<AlertsNotify> selectalertNotifyByPrimaryKey(String alertId) {

        if (StringUtils.isEmpty(alertId)) {
            LOGGER.warn("method[selectByPrimaryKey] param[alertId] is null");
            return null;
        }

        List<AlertsNotify> alertsNotifys = alertsNotifyDao.selectNotifyByPrimaryKey(alertId);

        return alertsNotifys;
    }


    //根据主键修改备注
    @Override
    public void updateNote(String alertId, String note) {

        Alerts alerts = alertsDao.selectByPrimaryKey(alertId);

        alerts.setRemark(note);

        int index = alertsDao.updateByPrimaryKey(alerts);

    }


    /**
     * 根据工单ID修改工单状态
     *
     * @param orderId     工单ID
     * @param orderStatus 工单状态
     */
    @Override
    public void modOrderStatusByOrderId(String orderId, String orderStatus) {
        alertsDao.modOrderStatusByOrderId(orderId, orderStatus);
    }

    /**
     * 告警创建
     *
     * @param alertsDTO 告警对象
     * @return String 告警ID
     */
    @Override
    public String insert(AlertsDTO alertsDTO) {
        if (null == alertsDTO) {
            LOGGER.error("method[insert] param[alertsDTO] is null");
            throw new RuntimeException("insert alert param[alertsDTO] is null");
        }
        Alerts alerts = TransformUtils.transform(Alerts.class, alertsDTO);
        String uuid = "";
        synchronized (uuid) {
            uuid = UUID.randomUUID().toString();
        }
        String alertID = alerts.getAlertId() == null ? uuid : alerts.getAlertId();
        alerts.setAlertId(alertID);
        alertsDao.insert(alerts);
        return alertID;
    }

    /**
     * @param alertsDTO 告警修改对象
     * @return
     */
    @Override
    public int updateByPrimaryKey(AlertsDTO alertsDTO) {
        if (null == alertsDTO) {
            LOGGER.error("method[updateByPrimaryKey] param[alertsDTO] is null");
            throw new RuntimeException("param[alertsDTO] is null");
        }
        if (StringUtils.isEmpty(alertsDTO.getAlertId())) {
            LOGGER.warn("method[updateByPrimaryKey] param[alertId] is null");
            throw new RuntimeException("param[alertId] is null");
        }
        Alerts alerts = TransformUtils.transform(Alerts.class, alertsDTO);
        int result = alertsDao.updateByPrimaryKey(alerts);
        return result;
    }


    /**
     * @param pageRequest 告警查询page对象
     * @return
     */
    @Override
    public PageResponse<AlertsDTO> pageList(PageRequest pageRequest) {
        Page page = PageUtil.convert(pageRequest);
        int count = alertsDao.pageListCount(page);
        PageResponse<AlertsDTO> pageResponse = new PageResponse<AlertsDTO>();
        pageResponse.setCount(count);
        if (count > 0) {
            List<Alerts> listAlerts = alertsDao.pageList(page);
            List<AlertsDTO> listDTO = TransformUtils.transform(AlertsDTO.class, listAlerts);
            pageResponse.setResult(listDTO);
        }
        return pageResponse;
    }

    /**
     * @param alertIdArrays 告警ID集合
     * @return
     */
    @Override
    public int deleteByPrimaryKeyArrays(String[] alertIdArrays) {
        if (ArrayUtils.isEmpty(alertIdArrays)) {
            LOGGER.error("method[deleteByPrimaryKeyArrays] param[alertIdArrays] is null");
            throw new RuntimeException("param[alertIdArrays] is null");
        }
        return alertsDao.deleteByPrimaryKeyArrays(alertIdArrays);
    }

    /**
     * @param alertsDTOList 新增告警列表
     * @return
     */
    @Override
    public List<AlertsDTO> insertByBatch(List<AlertsDTO> alertsDTOList) {
        if (CollectionUtils.isEmpty(alertsDTOList)) {
            LOGGER.error("method[insertByBatch] param[alertsDTOList] is null");
            throw new RuntimeException("param[alertsDTOList] is null");
        }
        for (AlertsDTO alertsDTO : alertsDTOList) {
            String uuid = UUID.randomUUID().toString();
            alertsDTO.setAlertId(uuid);
        }
        List<Alerts> listAlert = TransformUtils.transform(Alerts.class, alertsDTOList);
        alertsDao.insertByBatch(listAlert);
        //插入同步数据
        return alertsDTOList;
    }

    @Override
    public List<AlertsDTO> selectByPrimaryKeyArrays(String[] alertIdArrays) {
        if (ArrayUtils.isEmpty(alertIdArrays)) {
            LOGGER.warn("method[selectByPrimaryKeyArrays] param[alertIdArrays] is null");
            return Collections.emptyList();
        }
        List<Alerts> alertsList = alertsDao.selectByPrimaryKeyArrays(alertIdArrays);
        return TransformUtils.transform(AlertsDTO.class, alertsList);
    }

    @Override
    public List<AlertClearCountModel> listAllAlertsManualClearModels() {
        return this.alertsDao.listAllAlertsClearModels();
    }

    /**
     * 查询. <br/>
     * <p>
     * 作者： pengguihua
     *
     * @param alertClearModel
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public AlertClearCountModel getAlertClearCount(AlertClearCountModel alertClearModel) {
        return this.alertsDao.getAlertClearCount(alertClearModel);
    }

    /**
     * 新增. <br/>
     * <p>
     * 作者： pengguihua
     *
     * @param alertClearModel
     */
    @Override
    public void insertAlertClearCount(AlertClearCountModel alertClearModel) {
        this.alertsDao.insertAlertClearCount(alertClearModel);
    }

    /**
     * 递增手工清除count. <br/>
     * <p>
     * 作者： pengguihua
     *
     * @param alertClearModel
     */
    @Override
    public void updateAlertClearCount(AlertClearCountModel alertClearModel) {
        this.alertsDao.updateAlertClearCount(alertClearModel);
    }

    private Logger LOGGER = LoggerFactory.getLogger(AlertsBizImpl.class);

    @Override
    public List<Map<String, Object>> getAlertConfig() {
        return alertsDao.getAlertConfig();
    }

    @Override
    public List<Map<String, String>> getFirstBusiness(Map<String, Object> param) {
        return alertsDao.getFirstBusiness(param);
    }

    /**
     * 根据告警id删除上报记录
     *
     * @param alertId
     */
    public void deleteAlertsDetail(String alertId) {
        alertsDetailDao.deleteByAlertId(alertId);
    }

    /**
     * 新增告警上报记录
     *
     * @param alertDetail
     */
    public void insertAlertsDetail(AlertsDetail alertDetail) {
        alertsDetailDao.insert(alertDetail);
    }

    @Override
    public String upgrade(String oldOrderId, String orderId, String type,String orderStatus, String userName) {
        Alerts alertQuery = new Alerts();
        alertQuery.setOrderId(oldOrderId);
        List<Alerts> alertList = alertsDao.select(alertQuery);
        for (Alerts alert : alertList) {
            alert.setOrderId(orderId);
            alert.setOrderType(type);
            alert.setOrderStatus(orderStatus);
            alertsDao.updateByPrimaryKey(alert);
            //如果是工单升级，同时往告警操作记录表插入一条数据
            if (orderStatus.equals(Constants.ORDER_DEALING)){
                String content = "";
                if (type.equals("2")){
                    content ="bpm侧升级到故障工单";
                }else if (type.equals("3")){
                    content ="bpm侧升级到维保工单";
                }
                AlertsRecord alertsRecord = new AlertsRecord();
                alertsRecord.setAlertId(alert.getAlertId());
                alertsRecord.setUserName(userName);
                alertsRecord.setOperationType("100");
                alertsRecord.setOperationTime(new Date());
                alertsRecord.setOperationStatus("1");
                alertsRecord.setContent(content);
                alertsRecordDao.insert(alertsRecord);
            }
        }

        AlertsHis alertsHisQuery = new AlertsHis();
        alertsHisQuery.setOrderId(oldOrderId);
        List<AlertsHis> alertsHisList = alertHisDao.select(alertsHisQuery);
        for (AlertsHis alertsHis : alertsHisList) {
            alertsHis.setOrderId(orderId);
            alertsHis.setOrderType(type);
            alertsHis.setOrderStatus(orderStatus);
            alertHisDao.updateByPrimaryKey(alertsHis);
        }
        return "OK";
    }

    /**
     * 根据id更新告警监控时间
     * @param alertId
     * @param curMoniTime
     */
    public void updateCurMoniTime(String alertId, Date curMoniTime) {
        Map<String, Object> map = Maps.newHashMap();
        map.put("alertId", alertId);
        map.put("curMoniTime", curMoniTime);
        alertsDao.updateCurMoniTime(map);
    }

    @Override
    public int selectAlert(PageRequest pageRequest) {
        Page page = PageUtil.convert(pageRequest);
        return alertsStatisticDao.selectAlert(page);
    }

    @Override
    public int getAlertValue(Map<String, List<String>> ipMap, List<String> alertLevel, List<String> itemIdList) {
        return alertsDao.getAlertValue(ipMap, alertLevel, itemIdList);
    }
}
