package com.aspire.mirror.alert.server.biz.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;

/**
 * @ClassName CommonServiceImpl
 * @Description: TODO
 * @Author baiwenping
 * @Date 2019/9/19
 * @Version V1.0
 **/
public class CommonServiceImpl {

    /**
     * @MethodName: getOptionStr
     * @Description: TODO
     * @Param: [optionCondition]
     * @Return: java.lang.String
     * @Author: baiwenping
     * @Date: 2019/9/19
    **/
    protected String getOptionStr (String optionCondition) {
        StringBuffer sb = new StringBuffer();
        if (StringUtils.isEmpty(optionCondition)) {
            return sb.toString();
        }
        JSONObject jsonObject = JSONObject.parseObject(optionCondition);
        JSONArray objects = jsonObject.getJSONArray("data");
        //条件为空的忽略
        if (objects == null || objects.isEmpty()) {
            return sb.toString();
        }
        sb.append("(");
        //逐条解析条件
        for (int i = 0; i < objects.size(); i++) {
            JSONObject andJson = objects.getJSONObject(i);
            JSONArray andlist = andJson.getJSONArray("data");
            if (andlist == null || andlist.isEmpty()) {
                continue;
            }
            if (sb.length() > 2 && i > 0) {
                sb.append(",或者");
            }
            // 判断条件,单条规则所有条件都符合才能屏蔽
            boolean DeriveFlag = true;
            boolean updateFlag = false;
            for (int j = 0; j < andlist.size(); j++) {
                JSONObject val = andlist.getJSONObject(j);
                String filterItemName = val.getString("filterItemName");
                String operate = val.getString("operate").toLowerCase();
                String value = val.getString("value");
                sb.append(filterItemName);
                switch (operate) {
                    case "like":
                        sb.append("模糊匹配");
                        break;
                    case "=":
                        sb.append("等于");
                        break;
                    case "!=":
                        sb.append("不等于");
                        break;
                    case ">":
                        sb.append("大于");
                        break;
                    case ">=":
                        sb.append("大于等于");
                        break;
                    case "<":
                        sb.append("小于");
                        break;
                    case "<=":
                        sb.append("小于等于");
                        break;
                    case "in":
                        sb.append("包含");
                        break;
                    case "not in":
                        sb.append("不包含");
                        break;
                    default:
                        sb.append("未知");
                        break;
                }
                sb.append(value).append("、");
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
