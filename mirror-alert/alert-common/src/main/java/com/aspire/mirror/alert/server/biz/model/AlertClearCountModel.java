package com.aspire.mirror.alert.server.biz.model;

import lombok.Data;

/**
 * 告警清除次数model  <br/>
 * Project Name:alert-service
 * File Name:AlertClearCountModel.java
 * Package Name:com.aspire.mirror.alert.server.biz.model
 * ClassName: AlertClearCountModel <br/>
 * date: 2018年10月3日 下午4:48:30 <br/>
 *
 * @author pengguihua
 * @since JDK 1.6
 */
@Data
public class AlertClearCountModel {
    public static final String JOIN = "_";
    private String roomId;
    private String sourceAlertId;
    private Integer autoCount;
    private Integer manualCount;

    public static AlertClearCountModel build(String roomId, String sourceAlertId) {
        AlertClearCountModel model = new AlertClearCountModel();
        model.setRoomId(roomId);
        model.setSourceAlertId(sourceAlertId);
        return model;
    }

    public String getJoinedClearCount() {
        StringBuilder sb = new StringBuilder();
        sb.append(autoCount == null ? 0 : autoCount);
        sb.append(JOIN);
        sb.append(manualCount == null ? 0 : manualCount);
        return sb.toString();
    }
}
