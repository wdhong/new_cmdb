package com.aspire.mirror.alert.server.biz.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
public class LeakScanModel {
    // 漏洞扫描对象
    private Map<String, Object> ldsmdx;
}
