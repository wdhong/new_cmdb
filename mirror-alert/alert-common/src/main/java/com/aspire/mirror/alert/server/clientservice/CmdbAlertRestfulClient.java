package com.aspire.mirror.alert.server.clientservice;

import com.aspire.ums.cmdb.restful.alert.IAlertRestfulAPI;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(value = "CMDB")
public interface CmdbAlertRestfulClient extends IAlertRestfulAPI {
}
