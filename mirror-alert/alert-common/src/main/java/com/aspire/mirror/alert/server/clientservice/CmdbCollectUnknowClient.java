package com.aspire.mirror.alert.server.clientservice;

import com.aspire.ums.cmdb.collectUnknown.ICollectUnkownAPI;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(value = "CMDB")
public interface CmdbCollectUnknowClient extends ICollectUnkownAPI {
}
