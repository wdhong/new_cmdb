package com.aspire.mirror.alert.server.clientservice;

import com.aspire.ums.cmdb.dict.ICmdbDictAPI;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(value = "CMDB")
public interface CmdbDictClient extends ICmdbDictAPI {
}
