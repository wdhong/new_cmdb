package com.aspire.mirror.alert.server.clientservice;

import com.aspire.ums.cmdb.restful.common.ICommonRestfulAPI;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * @author baiwp
 * @title: CmdbInstanceClient
 * @projectName mirror-alert
 * @description: TODO
 * @date 2019/7/239:58
 */
@FeignClient(value = "CMDB")
public interface CmdbResfulClient extends ICommonRestfulAPI {
}
