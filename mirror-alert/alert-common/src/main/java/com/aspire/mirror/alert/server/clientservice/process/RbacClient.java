package com.aspire.mirror.alert.server.clientservice.process;

import com.aspire.mirror.alert.server.clientservice.payload.AlertUserVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("rbac")
public interface RbacClient {

    @ApiOperation("获取根账号中的成员信息列表")
    @GetMapping(value = "/v1/user/findByLdapId", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    AlertUserVO findByLdapId(@RequestParam("ldap_id") String ldapId);
}
