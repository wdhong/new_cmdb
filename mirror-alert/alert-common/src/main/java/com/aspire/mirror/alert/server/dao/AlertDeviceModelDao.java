package com.aspire.mirror.alert.server.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface AlertDeviceModelDao {

    void insert (@Param("fieldCodes") List<String> keys,
                 @Param("fieldValues") List<Map<String, Object>> values);

    void insert2 (@Param("sql") String sql);

    void delete();
}
