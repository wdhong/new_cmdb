package com.aspire.mirror.alert.server.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.aspire.mirror.alert.server.dao.po.AlertStandardizationDTO;
import com.aspire.mirror.alert.server.dao.po.AlertStandardizationLogDTO;
import com.aspire.mirror.alert.server.dao.po.AuthFieldConfig;
import com.aspire.mirror.alert.server.dao.po.KpiBookDTO;
import com.aspire.mirror.alert.server.dao.po.KpiListData;

@Mapper
public interface AlertRestfulDao {
   
    void insertBookAlerts(AlertStandardizationDTO stand);
    
    List<AlertStandardizationDTO> getBookAlerts(@Param("source")String source);
    
    int insertKpiList(List<KpiListData> list);
    
    void insertKpiBook(KpiBookDTO kpi);
    
    List<KpiBookDTO> getKpiBook(@Param("source")String source);
    
    AlertStandardizationLogDTO getLastStandardizationLog(@Param("type")int type,@Param("configId")int configId);
    //插入标准化日志
    int insertStandardizationLog(AlertStandardizationLogDTO log);
    
    List<AuthFieldConfig> selectAuthField(@Param("type") Integer type);
    
    //资源池的设备性能分布
    List<Map<String,Object>> getIdcTypePerformanceData(@Param("idcType") String idcType,@Param("startTime") String startTime
    		,@Param("endTime") String endTime,@Param("item") String item,@Param("deviceType") String deviceType);
    
    
    int batchInsertIdcTypePerformance(List<Map<String,Object>> data);
    
    //资源池的设备性能分布:查所有资源池数据
    List<Map<String,Object>> getAllIdcTypePerformanceData(@Param("day") String day);
    
    int insertDeviceAlerts(List<Map<String,Object>> list);
}
