package com.aspire.mirror.alert.server.dao;

import com.aspire.mirror.alert.server.domain.AlertScanComparisionDetailDTO;
import com.aspire.mirror.alert.server.domain.AlertScanComparisionReqDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface AlertScanComparisionDao {

    int getScanComparisionCount(AlertScanComparisionReqDTO request);

    List<AlertScanComparisionDetailDTO> getScanComparisionList(AlertScanComparisionReqDTO request);

    void deleteScanComparisionById(List<String> request);

    List<Map<String, Object>> exportScanComparision(AlertScanComparisionReqDTO request);

    
    void updateSynStatus(List<Map<String, Object>> request);

    List<Map<String, String>> getAlerts();

    AlertScanComparisionDetailDTO getAlertScanComparisionDetailByIpAndPool(@Param("deviceIp") String deviceIp,
                                                                           @Param("idcType") String idcType);
    

    void insertScanComparision(List<AlertScanComparisionDetailDTO> request);

    void updateById(@Param("id") String id,
                    @Param("curMoniTime") String curMoniTime);
}
