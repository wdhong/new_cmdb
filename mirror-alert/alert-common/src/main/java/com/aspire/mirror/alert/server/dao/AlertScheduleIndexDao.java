package com.aspire.mirror.alert.server.dao;

import com.aspire.mirror.alert.server.domain.AlertScheduleIndexDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AlertScheduleIndexDao {

    List<AlertScheduleIndexDTO> getALLScheduleIndex();

    List<AlertScheduleIndexDTO> getAlertScheduleIndexDetail(@Param("indexType") String indexType);

    void insertAlertScheduleIndex(AlertScheduleIndexDTO request);

    void updateAlertScheduleIndex(AlertScheduleIndexDTO request);

    void deleteById (@Param("id") String id);
}
