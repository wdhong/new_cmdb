package com.aspire.mirror.alert.server.dao;

import com.aspire.mirror.alert.server.dao.po.AlertWorkConfigDTO;
import com.aspire.mirror.alert.server.dao.po.AlertWorkLogInfoDTO;
import com.aspire.mirror.alert.server.dao.po.AlertWorkLogInfoRespDTO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface AlertWorkLogDao {

    void insertAlertWorkConfig(AlertWorkConfigDTO request);

    void updateAlertWorkConfig(AlertWorkConfigDTO request);

    AlertWorkConfigDTO getAlertWorkConfig();

    List<AlertWorkLogInfoRespDTO> getAlertListByAlerts(AlertWorkLogInfoDTO alertWorkLogTaskRequest);

    List<AlertWorkLogInfoRespDTO> getAlertListByAlertsHis(AlertWorkLogInfoDTO alertWorkLogTaskRequest);

    List<Map<String,Object>> getAlertsFromAlert(AlertWorkLogInfoDTO alertWorkLogTaskRequest);

}
