package com.aspire.mirror.alert.server.dao;

import com.aspire.mirror.alert.server.domain.AlertAutoOrderConfigDetailDTO;
import com.aspire.mirror.alert.server.domain.AlertAutoOrderConfigReqDTO;
import com.aspire.mirror.alert.server.domain.AlertAutoOrderLogDTO;
import com.aspire.mirror.alert.server.domain.AlertAutoOrderLogReqDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface AlertsAutoOrderConfigDao {
    /**
     * 获取告警自动派单配置列表
     */
    List<AlertAutoOrderConfigDetailDTO> getAlertAutoOrderConfigList(@Param("configName") String configName,
                                                                    @Param("isOpen") String isOpen,
                                                                    @Param("startTime") String startTime,
                                                                    @Param("endTime") String endTime,
                                                                    @Param("orderType") String orderType,
                                                                    @Param("orderTimeInterval") String orderTimeInterval,
                                                                    @Param("pageNum") Integer pageNum,
                                                                    @Param("pageSize") Integer pageSize);
    int getAlertAutoOrderConfigCount(@Param("configName") String configName,
                                    @Param("isOpen") String isOpen,
                                    @Param("startTime") String startTime,
                                    @Param("endTime") String endTime,
                                    @Param("orderType") String orderType,
                                    @Param("orderTimeInterval") String orderTimeInterval,
                                    @Param("pageNum") Integer pageNum,
                                    @Param("pageSize") Integer pageSize);
    /**
     * 创建告警自动派单配置
     */
    void createAlertAutoOrderConfig(AlertAutoOrderConfigReqDTO request);
    /**
     * 校验配置名称
     */
    AlertAutoOrderConfigDetailDTO checkName(@Param("configName") String configName);
    /**
     * 修改告警自动派单配置
     */
    void updateAlertAutoOrderConfig(AlertAutoOrderConfigReqDTO request);
    /**
     * 获取告警自动派单配置详情
     */
    AlertAutoOrderConfigDetailDTO getAlertAutoOrderConfigDetail(@Param("uuid") String uuid);
    /**
     * 删除告警自动派单配置详情
     */
    void deleteAlertAutoOrderConfig(List<String> uuidList);
    /**
     * 更改告警自动派单配置状态
     */
    void updateAlertAutoOrderConfigStatus(Map<String, Object> map);
    /**
     * 获取告警自动派单配置日志列表
     */
    List<AlertAutoOrderLogDTO> getAlertAutoOrderLogList(AlertAutoOrderLogReqDTO request);
    int getAlertAutoOrderLogCount(AlertAutoOrderLogReqDTO request);
    /**
     * 导出告警自动派单配置日志
     */
    List<Map<String, Object>> exportAlertAutoOrderLogList(AlertAutoOrderLogReqDTO request);
    /**
     * 根据过滤内容获取告警
     */
    List<Map<String, Object>> getAlertDataByFilter(Map<String, Object> map);

    /**
     * 添加派单日志
     */
    void insertAlertOrderConfigLog(AlertAutoOrderLogDTO request);
    int getOrderLogCountByAlertIdAndOrderType(@Param("alertId") String alertId,
                                              @Param("orderType") String orderType);
    void updateOrderTime(@Param("alertId") String alertId,
                                              @Param("orderType") String orderType);
}
