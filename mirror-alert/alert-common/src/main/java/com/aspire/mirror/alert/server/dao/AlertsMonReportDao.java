package com.aspire.mirror.alert.server.dao;

import com.aspire.mirror.alert.server.domain.AlertsMonReportAlertDTO;
import com.aspire.mirror.alert.server.domain.AlertsMonReportIdcTypeDTO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface AlertsMonReportDao {

    List<AlertsMonReportAlertDTO> getDeviceData(Map<String,Object> param);

    void insertMonReportAlertDevice(AlertsMonReportAlertDTO param);

    List<AlertsMonReportAlertDTO> getAlertIndexData(Map<String,Object> param);

    void insertMonReportAlertIndex(AlertsMonReportAlertDTO param);

    AlertsMonReportIdcTypeDTO getAlertIdcData(Map<String,Object> param);

    List<Map<String,Object>> getAlertByDeviceType(Map<String,Object> param);

    void insertAlertMonReportIdc(AlertsMonReportIdcTypeDTO param);

    List<Map<String,Object>> getIdcTypeAlertCount(Map<String,Object> param);

    void insertAlertMonReportSum(Map<String,Object> param);

    int getAlertIndexSum(Map<String,Object> param);

    List<Map<String,Object>> viewByIdcType(Map<String, String> map);

    List<Map<String,Object>> viewByIp(Map<String, String> map);

    List<Map<String,Object>> viewByKeyComment(Map<String, String> map);

}
