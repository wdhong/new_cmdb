package com.aspire.mirror.alert.server.dao;

import com.aspire.mirror.alert.server.domain.AutoConfirmClearDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface AutoConfirmClearDao {

    void insert(AutoConfirmClearDTO request);

    AutoConfirmClearDTO getAutoConfirmClearId(@Param("deviceIp") String deviceIp,
                                 @Param("idcType") String idcType,
                                 @Param("bizSys") String bizSys,
                                 @Param("alertLevel") String alertLevel,
                                 @Param("source") String source,
                                 @Param("itemId") String itemId,
                                 @Param("autoType") Integer autoType,
                                 @Param("curTime") String curTime);

    List<AutoConfirmClearDTO> getAutoConfirmClearDTOs(Map<String,Object> map);


    void deleteRule(@Param("curTime") String curTime);

}
