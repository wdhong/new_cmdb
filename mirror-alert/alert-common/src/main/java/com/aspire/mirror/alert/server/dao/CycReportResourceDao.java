package com.aspire.mirror.alert.server.dao;

import com.aspire.mirror.alert.server.dao.po.CycReportResFile;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CycReportResourceDao {
    void insertFtpFileList(@Param(value = "fileList") List<CycReportResFile> fileList);
    void updateFtpFileReadStatus(@Param(value = "filePath") String filePath);
}
