package com.aspire.mirror.alert.server.dao.po;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 告警配置详情类
 * <p>
 * 项目名称:  mirror平台
 * 包:        com.aspire.mirror.alert.server.dao.po
 * 类名称:    AlertConfigDetail.java
 * 类描述:    告警配置详情类po类
 * 创建人:    qianch
 * 创建时间:  2019/1/24 16:58
 * 版本:      v1.0
 */
@Data
@NoArgsConstructor
public class AlertConfigDetail {
	
	/**
	 * ID
	 */
	private String id;
    /**
     * ALERT_CONFIG_ID
     */
    private String alertConfigId;
    
    /**
     * 配置类型
     */
    private String configType;
    
    /**
     * TARGET_ID
     */
    private String targetId;

    /**
     * monitType
     */
    private String monitType;

    /**
     * 告警等级
     */
    private String alertLevel;

}
