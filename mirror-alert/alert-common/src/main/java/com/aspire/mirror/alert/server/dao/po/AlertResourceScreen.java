package com.aspire.mirror.alert.server.dao.po;


import java.io.Serializable;

import lombok.Data;

@Data
public class AlertResourceScreen implements Serializable {

	private static final long serialVersionUID = 1L;


	private String department1;
	private String department2;
	private String bizSystem;
	private String deviceUseCount;
	private String avgValue;

	private String maxValue;


}