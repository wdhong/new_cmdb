package com.aspire.mirror.alert.server.dao.po;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 告警转化类
 * <p>
 * 项目名称:  mirror平台
 * 包:        com.aspire.mirror.alert.server.dao.po
 * 类名称:    Alerts.java
 * 类描述:    告警po类
 * 创建人:    JinSu
 * 创建时间:  2018/9/18 16:58
 * 版本:      v1.0
 */
@Data
@NoArgsConstructor
public class AlertRestfulStandardization {

    /**
     * 告警ID
     */
    private String alert_type;
    
    /**
     * clock
     */
    private String alert_id;
    
    /**
     * businessSystem
     */
    private String device_id;

    /**
     * 关联告警ID
     */
    private String source;

    /**
     * 时间ID
     */
    private String device_ip;

    private String source_room;

    /**
     * 设备ID
     */
    private String idc_type;


    /**
     * 业务系统
     */
    private String device_class;

    /**
     * 监控指标/内容，关联触发器name
     */
    private String device_type;

    /**
     * 监控对象
     */
    private String device_mfrs;

    /**
     * 当前监控值
     */
    private String device_model;

    /**
     * 当前监控时间
     */
    private String host_name;

    /**
     * 告警开始时间
     */
    private String pod_name;

    /**
     * 告警级别
     * 1-提示
     * 2-低
     * 3-中
     * 4-高
     * 5-严重
     */
    private String project_name;

    private String biz_sys;

    /**  */
    private String moni_object;

    /**
     * 备注
     */
    private String moni_index;
    
    /**
     * 信息
     */
    private String alert_level;

    /**
     * 1-未派单
     * 2-处理中
     * 3-已完成
     */
    private String cur_moni_time;

    /**
     * 告警来源
     * MIRROR
     * ZABBIX
     */
    private String cur_moni_value;

    /**
     * 机房/资源池
     */
    private String item_id;

    private String alert_start_time;
    /**
     * 1-系统
     * 2-业务
     */
    private String alert_end_time;

    /**
     * 所属域/资源池
     */
    private String remark;

    /**
     * ip
     */
    private String object_type;
   
}
