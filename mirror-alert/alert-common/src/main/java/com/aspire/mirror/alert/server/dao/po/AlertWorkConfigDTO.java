package com.aspire.mirror.alert.server.dao.po;

import lombok.Data;

@Data
public class AlertWorkConfigDTO {

    private String uuid;
    private String dayStartTme;
    private String dayEndTme;
    private String nightStartTme;
    private String nightEndTme;
}
