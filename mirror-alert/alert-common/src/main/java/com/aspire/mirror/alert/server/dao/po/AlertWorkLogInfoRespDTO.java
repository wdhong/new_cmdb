package com.aspire.mirror.alert.server.dao.po;

import lombok.Data;

@Data
public class AlertWorkLogInfoRespDTO {

    // 等级
    private String alertLevel;
    // 操作类型
    private long operationType;
}
