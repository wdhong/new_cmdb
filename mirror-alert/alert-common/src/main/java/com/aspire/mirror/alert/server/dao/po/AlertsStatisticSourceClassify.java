package com.aspire.mirror.alert.server.dao.po;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AlertsStatisticSourceClassify {

    private String idcType;

    private String sourceRoom;

    private Integer alertNum;
}
