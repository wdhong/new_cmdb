package com.aspire.mirror.alert.server.dao.po;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AlertsTop10DTO {

    private String colName;

    private double rate;//内容占比

    private Integer count;
}
