package com.aspire.mirror.alert.server.dao.po;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@NoArgsConstructor
@Data
public class CycReportResFile {
    private String fileName;
    private int fileType;
    private String filePath;
    private Date startTime;
    private String sessionId;
    private Integer requestId;
    private int readStatus;
}
