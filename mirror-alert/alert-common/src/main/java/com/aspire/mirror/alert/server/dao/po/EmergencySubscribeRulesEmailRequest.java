package com.aspire.mirror.alert.server.dao.po;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EmergencySubscribeRulesEmailRequest {
    /**
     * 邮箱内容
     */
    private String emails;
    /**
     * 邮箱主题
     */
    private String subject;

    /**
     *告警id
     */
    private String alertIds;
}
