package com.aspire.mirror.alert.server.dao.po;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class EmergencySubscribeRulesRequest {
    //当前用户name
    @JsonProperty("username")
    private String username;

    //告警id
    @JsonProperty("alertIds")
    private String alertIds;

    //邮箱模板
    @JsonProperty("emails")
    private String emails;

    //邮件主题
    @JsonProperty("subject")
    private String subject;
}
