package com.aspire.mirror.alert.server.domain;

import lombok.Data;

@Data
public class AlertScheduleIndexDTO {

    private String id;

    private String indexName;

    private String indexValue;

    private String indexType;

    private String remark;
}
