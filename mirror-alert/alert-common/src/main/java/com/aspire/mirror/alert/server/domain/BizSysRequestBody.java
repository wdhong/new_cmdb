package com.aspire.mirror.alert.server.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BizSysRequestBody {
    private String ip;
    private String bizSystem;
}
