package com.aspire.mirror.alert.server.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * @author menglinjie
 */
@Data
public class SmsRecordExportVoDTO implements Serializable {

    private String startTime;

    private String endTime;

    private String receiver;

    private String content;

    private Integer status;

    private static final long serialVersionUID = 1L;

}