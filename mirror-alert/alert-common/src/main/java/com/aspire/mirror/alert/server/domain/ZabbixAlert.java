package com.aspire.mirror.alert.server.domain;

import com.aspire.mirror.alert.server.biz.helper.CmdbHelper;
import com.aspire.mirror.alert.server.util.StringUtils;
import com.aspire.ums.cmdb.instance.payload.CmdbInstance;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/** 
 *
 * 项目名称: ums-web-client 
 * <p/>
 * 
 * 类名: ZabbixAlert
 * <p/>
 *
 * 类功能描述: 告警   消息体
 * <p/>
 *
 * @author	pengguihua
 *
 * @date	2017年2月28日  
 *
 * @version	V1.0 
 * <br/>
 *
 * <b>Copyright(c)</b> 2017 卓望公司-版权所有 
 *
 */
@Data
@JsonInclude(Include.NON_NULL)
public class ZabbixAlert {
	public static final String	SOURCE_ZABBIX				= "ZABBIX";
	public static final String	BIZ_MONITOR_ITEM_KEY_PREFIX	= "YWZB_";
	
	private Integer	indexNum;		// 当前批次中的序号
	private String	moniResult;		// 指标监测结果 1: 新增告警 2: 解除告警

	@JsonProperty("alert_id")
	private String alertId;

	@JsonProperty("z_alert_Id")
	private String	zbxAlertId;		// zabbix中告警id
	
	@JsonProperty("eventId")
	private String	umsAlertId;		// ums告警id
	
	@JsonProperty("z_event_Id")
	private String	zbxEventId;		// zabbix中eventId
	
//	private String	monitorSource;	// 第三方监控系统编码, 在第三方的告警中，当作proxyName
	private String	deviceIP;		// 所属设备
	private String	servSystem;		// 所属业务系统
	private String	monitorRoom;	// 监控机房
	private String	monitorObject;	// 监控对象
	private String	monitorIndex;	// 监控指标
	private String	moniIndexValue;
	private String	alertLevel;		// 告警级别
	private String	alertDesc;		// 告警描述
	private String	curMoniTime;	// 当前监测时间 yyyy-MM-dd HH:mm:ss
	private String	curMoniValue;	// 当前监测值
	private String	businessSystem;	// 在Zabbix系统告警中，当作proxyName
//	private String	itemCategory;
//	private String	hostGroupId;
//	private String	hostId;
	private String	hostName;
	@JsonProperty("z_itemId")
	private String	zbxItemId;
	private String oldEventId;
	private String actionId;
	private String objectType;
	/**
	 * 告警开始时间
	 */
	private String alertStartTime;
	/**
	 * 告警前缀，用于区分不同zabbix
	 */
	private String prefix;
	private String source;
	/**
	 * 监控对象
	 */
	private String	itemKey;
	
	public AlertsDTO parse(final CmdbHelper cmdbHelper) throws Exception {
		AlertsDTO dto = new AlertsDTO();
//		dto.setAlertId(this.getAlertId());
		dto.setRAlertId(this.getUmsAlertId());
		dto.setAlertLevel(this.getAlertLevel());
		
		DateFormat format = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
		dto.setCurMoniTime(format.parse(this.getCurMoniTime()));
		dto.setCurMoniValue(this.getCurMoniValue());
		if (StringUtils.isNotEmpty(this.getAlertStartTime())) {
			dto.setAlertStartTime(format.parse(this.getAlertStartTime()));
		} else {
			dto.setAlertStartTime(format.parse(this.getCurMoniTime()));
		}
		dto.setDeviceIp(this.getDeviceIP());
		dto.setBizSys(this.getServSystem());
		dto.setEventId(this.getZbxEventId());
		dto.setAlertType(this.getMoniResult());
		dto.setItemId(this.getZbxItemId()); 		// 后续均为保存微服务中定义的itemid
		dto.setMoniIndex(this.getMonitorIndex());
		dto.setMoniObject(this.getMonitorObject());
		dto.setSource(this.getSource());
		dto.setItemKey(this.getItemKey());
		dto.setRemark(this.getAlertDesc());
//		dto.setSourceRoom(this.getMonitorRoom());
		dto.setActionId(this.getActionId());
		dto.setOldEventId(this.getOldEventId());
		dto.setPrefix(this.getPrefix());
		String idcType = cmdbHelper.getIdc(this.getBusinessSystem());
		dto.setIdcType(idcType);
		if (StringUtils.isNotEmpty(this.getObjectType())) {
			dto.setObjectType(this.getObjectType());
		} else {
			dto.setObjectType(AlertsDTO.OBJECT_TYPE_DEVICE);
		}
		if (dto.getObjectType().equals(AlertsDTO.OBJECT_TYPE_BIZ) && StringUtils.isEmpty(dto.getMoniObject())) {
			dto.setMoniObject("Application");
			dto.setObjectId(dto.getBizSys());
		} else if (StringUtils.isNotEmpty(this.getDeviceIP())){
			// 根据  机房 + IP, 查找设备
			cmdbHelper.queryDeviceForAlert(dto);
		}
		return dto;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!ZabbixAlert.class.isInstance(obj)) {
			return false;
		}
		ZabbixAlert other = ZabbixAlert.class.cast(obj);
		if (this.getMonitorRoom() == null) {
			return false;
		}
		if (this.umsAlertId == null) {
			return false;
		}
		return getMonitorRoom().equals(other.getMonitorRoom()) && getUmsAlertId().equals(other.getUmsAlertId());
	}
	
	@Override
	public int hashCode() {
		int hash = 17;
		hash = hash * 13 + (getMonitorRoom() == null ? "" : getMonitorRoom()).hashCode();
		hash = hash * 13 + (getUmsAlertId() == null ? "" : getUmsAlertId()).hashCode();
		return hash;
	}
}
