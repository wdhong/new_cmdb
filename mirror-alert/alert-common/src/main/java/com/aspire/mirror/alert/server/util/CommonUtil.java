package com.aspire.mirror.alert.server.util;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class CommonUtil {

    // 获取场景配置内容
    public static String getCondition(String optionCondition) {
        JSONObject filterOption = JSON.parseObject(optionCondition);
        StringBuffer orSb = new StringBuffer();
        JSONArray orlist = filterOption.getJSONArray("data");
        for (int i = 0; i < orlist.size(); i++) {
            StringBuffer andSb = new StringBuffer();
            JSONObject andJson = orlist.getJSONObject(i);
            JSONArray andlist = andJson.getJSONArray("data");
            for (int j = 0; j < andlist.size(); j++) {
                JSONObject val = andlist.getJSONObject(j);
                String filterItemName = val.getString("filterItemName");
                String operate = val.getString("operate");
                String value = val.getString("value");
                String jdbcType = val.getString("jdbcType");
                if (andSb.length() == 0) {
                    andSb.append("(").append(filterItemName).append(" ").append(operate).append(" ");
                } else {
                    andSb.append(" and ").append(filterItemName).append(" ").append(operate).append(" ");
                }
                if(operate.equalsIgnoreCase("in") || operate.indexOf("not")==0) {
                    JSONArray valJson = JSON.parseArray(value);
                    for (int k = 0; k < valJson.size(); k++) {
                        String valStr = valJson.getString(k);
                        if (!jdbcType.equalsIgnoreCase("number")) {
                            valStr = "'" + valStr + "'";
                        }
                        if(k==0) {
                            andSb.append("(").append(valStr);
                        }else {
                            andSb.append(",").append(valStr);
                        }
                    }
                    andSb.append(")");

                }else {
                    if (!jdbcType.equalsIgnoreCase("number")) {
                        if(operate.equalsIgnoreCase("like")) {
                            value = "'%" + value + "%'";
                        }else {
                            value = "'" + value + "'";
                        }
                    }
                    andSb.append(value);
                }
            }
            andSb.append(")");
            if (orSb.length() == 0) {
                orSb.append("( ");
                orSb.append(andSb);
            } else {
                orSb.append(" or ").append(andSb);
            }
        }
        if(orSb.length()>0) {
            orSb.append(")");
        }
        return orSb.toString();
    }



}
