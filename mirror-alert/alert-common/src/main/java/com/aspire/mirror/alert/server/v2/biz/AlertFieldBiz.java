package com.aspire.mirror.alert.server.v2.biz;

import com.aspire.mirror.alert.server.v2.dao.po.AlertFieldDetailDTO;
import com.aspire.mirror.alert.server.v2.dao.po.AlertFieldRequestDTO;
import com.aspire.mirror.common.entity.PageResponse;

import java.util.List;
import java.util.Map;

public interface AlertFieldBiz {

    /** 
    * 
    * @auther baiwenping
    * @Description 
    * @Date 15:47 2020/2/21
    * @Param [tableName]
    * @return java.util.List<com.aspire.mirror.alert.server.v2.dao.po.AlertFieldRequestDTO>
    **/
    
    List<AlertFieldRequestDTO> getModelFromRedis (String tableName, String sort);

    /**
     *  添加告警模型字段
     */
    void insertAlertModel(AlertFieldRequestDTO requestDTO);
    /**
     *  根据ID获取告警模型字段详情
     */
    AlertFieldDetailDTO getAlertFieldDetailById(String id);
    /**
     *  根据ID删除告警模型字段详情
     */
    void deleteAlertFieldDetailById(String id, String modelId, String userName);
    /**
     *  修改告警模型字段
     */
    void updateAlertField(AlertFieldRequestDTO requestDTO);
    /**
     * 获取告警模型字段列表
     */
    PageResponse<AlertFieldDetailDTO> getAlertFieldListByModelId(Map<String,Object> request);
    /**
     * 修改锁定状态
     */
    void updateLockStatus(String id, String modelId, String isLock, String userName);
    /**
     * 同步告警模型字段
     */
    void synchronizeField(String modelId, String userName);

    /**
     * 告警模型字段缓存
     * @param tableName
     * @return
     */
    List<AlertFieldRequestDTO> getModelField (String tableName);
}
