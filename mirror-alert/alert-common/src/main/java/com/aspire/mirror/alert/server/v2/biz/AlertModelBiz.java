package com.aspire.mirror.alert.server.v2.biz;

import com.aspire.mirror.alert.server.v2.dao.po.AlertModelDetailDTO;
import com.aspire.mirror.alert.server.v2.dao.po.AlertModelRequestDTO;

import java.util.List;
import java.util.Map;

public interface AlertModelBiz {

    /**
     *  添加告警模型
     */
    void insertAlertModel(AlertModelRequestDTO request);
    /**
     *  获取告警模型列表
     */
    List<AlertModelDetailDTO> getAlertModelList(String modelName, String tableName);
    /**
     *  获取告警模型树
     */
    Object getAlertModelTreeData();
    /**
     *  删除告警模型
     */
    void deleteAlertModel(String userName,List<String> request);
    /**
     *  获取告警模型详情
     */
    AlertModelDetailDTO getAlertModelDetail(String id);
    /**
     *  编辑告警模型数据
     */
    void updateAlertModel(AlertModelRequestDTO request);
}
