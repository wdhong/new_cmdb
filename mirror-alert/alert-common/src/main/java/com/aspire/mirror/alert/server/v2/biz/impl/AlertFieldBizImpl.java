package com.aspire.mirror.alert.server.v2.biz.impl;

import com.aspire.mirror.alert.server.dao.AlertOperateLogMapper;
import com.aspire.mirror.alert.server.dao.po.AlertOperateLog;
import com.aspire.mirror.alert.server.util.StringUtils;
import com.aspire.mirror.alert.server.v2.biz.AlertFieldBiz;
import com.aspire.mirror.alert.server.v2.dao.AlertFieldDao;
import com.aspire.mirror.alert.server.v2.dao.AlertModelDao;
import com.aspire.mirror.alert.server.v2.dao.po.AlertFieldDetailDTO;
import com.aspire.mirror.alert.server.v2.dao.po.AlertFieldRequestDTO;
import com.aspire.mirror.alert.server.v2.dao.po.AlertModelDetailDTO;
import com.aspire.mirror.common.entity.PageResponse;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;


@Service
@Slf4j
public class AlertFieldBizImpl implements AlertFieldBiz {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    @Autowired
    private AlertFieldDao alertFieldDao;
    @Autowired
    private AlertModelDao alertModelDao;
    @Autowired
    private AlertOperateLogMapper alertOperateLogMapper;

    private final ConcurrentHashMap<String, List<AlertFieldRequestDTO>> alertModelCache = new ConcurrentHashMap<>();

    /**
    *
    * @auther baiwenping
    * @Description
    * @Date 15:47 2020/2/21
    * @Param [tableName]
    * @return java.util.List<com.aspire.mirror.alert.server.v2.dao.po.AlertFieldRequestDTO>
    **/
    public List<AlertFieldRequestDTO> getModelFromRedis (String tableName, String sort) {
        //判断sort格式，防止sql恶意注入
        if (!StringUtils.isEmpty(sort) && !sort.matches("[a-zA-Z0-9\\.\\_\\ ,]+")) {
            sort = null;
        }
        // 经测试，由于数据量小，直接请求效率高于redis
//        List<AlertFieldRequestDTO> list = Lists.newArrayList();
//        if (redisTemplate.hasKey(tableName)) {
//            List<Object> values = redisTemplate.opsForHash().values(tableName);
//            list.addAll(PayloadParseUtil.jacksonBaseParse(AlertFieldRequestDTO.class, values));
//        } else {
            // 如果redis没有数据，则从数据库查询数据并更新redis
            List<AlertFieldRequestDTO> alertFieldList = alertFieldDao.getAlertFieldListByTableName(tableName, sort);
//            for (AlertFieldRequestDTO alertFieldDetailDTO:alertFieldList) {
//                redisTemplate.opsForHash().put(tableName, alertFieldDetailDTO.getId(), alertFieldDetailDTO);
//                redisTemplate.expire(tableName, 24, TimeUnit.HOURS);
//            }
//            list.addAll(alertFieldList);
//        }

        return  alertFieldList;
    }

    @Override
    @Transactional(rollbackFor= Exception.class)
    public void insertAlertModel(AlertFieldRequestDTO requestDTO) {

        // 设置id
        requestDTO.setId( UUID.randomUUID().toString());
        requestDTO.setCreateTime(new Date());
        try {
            // 写入数据库
            alertFieldDao.insertAlertModel(requestDTO);
            AlertModelDetailDTO alertModelDetail = alertModelDao.getAlertModelDetail(requestDTO.getModelId());
            Map<String, Object> map = Maps.newHashMap();
            map.put("tableName", alertModelDetail.getTableName());
            map.put("fieldCode", requestDTO.getFieldCode());
            map.put("jdbcType", requestDTO.getDataType());
            map.put("jdbcLength", StringUtils.isEmpty(requestDTO.getDataLength()) ? 0 : Integer.valueOf(requestDTO.getDataLength()));
            map.put("jdbcTip", requestDTO.getDataTip());
            alertFieldDao.addFieldColumn(map);
            // 更新redis
//            AlertModelRequestDTO alertModelDetail = alertModelDao.getAlertModelDetail(requestDTO.getModelId());
//            redisTemplate.opsForHash().put(alertModelDetail.getTableName(), requestDTO.getId(), requestDTO);
            // 添加操作日志
            AlertOperateLog alertOperateLog = new AlertOperateLog();
            alertOperateLog.setOperateContent("新增模型字段");
            alertOperateLog.setOperateModel("alert_field");
            alertOperateLog.setOperateModelDesc("告警模型字段");
            alertOperateLog.setOperater(requestDTO.getCreator());
            alertOperateLog.setOperateTime(new Date());
            alertOperateLog.setOperateType("insert");
            alertOperateLog.setOperateTypeDesc("新增模型字段");
            alertOperateLog.setRelationId(requestDTO.getId());
            alertOperateLogMapper.insert(alertOperateLog);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new RuntimeException(e);
        }

    }

    @Override
    public AlertFieldDetailDTO getAlertFieldDetailById(String id) {
        return alertFieldDao.getAlertFieldDetailById(id);
    }

    @Override
    public void deleteAlertFieldDetailById(String id, String modelId, String userName) {

        AlertModelDetailDTO alertModelDetail = alertModelDao.getAlertModelDetail(modelId);
        AlertFieldDetailDTO alertFieldDetailById = alertFieldDao.getAlertFieldDetailById(id);
        // 删除数据库
        alertFieldDao.deleteAlertFieldDetailById(id,modelId);
        alertFieldDao.deleteFieldColumn(alertModelDetail.getTableName(),alertFieldDetailById.getFieldCode());
        // 添加操作日志
        AlertOperateLog alertOperateLog = new AlertOperateLog();
        alertOperateLog.setOperateContent("删除模型字段");
        alertOperateLog.setOperateModel("alert_field");
        alertOperateLog.setOperateModelDesc("告警模型字段");
        alertOperateLog.setOperater(userName);
        alertOperateLog.setOperateTime(new Date());
        alertOperateLog.setOperateType("delete");
        alertOperateLog.setOperateTypeDesc("删除模型字段");
        alertOperateLog.setRelationId(id);
        alertOperateLogMapper.insert(alertOperateLog);
        // 删除redis
//        AlertModelRequestDTO alertModelDetail = alertModelDao.getAlertModelDetail(modelId);
//        redisTemplate.opsForHash().delete(alertModelDetail.getTableName(), id);
    }

    @Override
    @Transactional(rollbackFor= Exception.class)
    public void updateAlertField(AlertFieldRequestDTO requestDTO) {
        requestDTO.setUpdateTime(new Date());
        AlertModelDetailDTO alertModelDetail = alertModelDao.getAlertModelDetail(requestDTO.getModelId());
        AlertFieldDetailDTO alertFieldDetailById = alertFieldDao.getAlertFieldDetailById(requestDTO.getId());
        try {
            // 修改数据库
            alertFieldDao.updateAlertField(requestDTO);
            if (!requestDTO.getDataLength().equals(alertFieldDetailById.getDataLength()) ||
                    !requestDTO.getDataType().equals(alertFieldDetailById.getDataType()) ||
                    !requestDTO.getDataTip().equals(alertFieldDetailById.getDataTip())) {
                alertFieldDao.deleteFieldColumn(alertModelDetail.getTableName(),requestDTO.getFieldCode());
                Map<String, Object> map = Maps.newHashMap();
                map.put("tableName", alertModelDetail.getTableName());
                map.put("fieldCode", requestDTO.getFieldCode());
                map.put("jdbcType", requestDTO.getDataType());
                map.put("jdbcLength", StringUtils.isEmpty(requestDTO.getDataLength()) ? 0 : Integer.valueOf(requestDTO.getDataLength()));
                map.put("jdbcTip", requestDTO.getDataTip());
                alertFieldDao.addFieldColumn(map);
            }
            // 添加操作日志
            AlertOperateLog alertOperateLog = new AlertOperateLog();
            alertOperateLog.setOperateContent("修改模型字段");
            alertOperateLog.setOperateModel("alert_field");
            alertOperateLog.setOperateModelDesc("告警模型字段");
            alertOperateLog.setOperater(requestDTO.getUpdater());
            alertOperateLog.setOperateTime(new Date());
            alertOperateLog.setOperateType("update");
            alertOperateLog.setOperateTypeDesc("修改模型字段");
            alertOperateLog.setRelationId(requestDTO.getId());
            alertOperateLogMapper.insert(alertOperateLog);
            // 修改redis
//            AlertModelRequestDTO alertModelDetail = alertModelDao.getAlertModelDetail(requestDTO.getModelId());
//            redisTemplate.opsForHash().put(alertModelDetail.getTableName(), requestDTO.getId(), requestDTO);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new RuntimeException(e);
        }
    }

    @Override
    public PageResponse<AlertFieldDetailDTO> getAlertFieldListByModelId(Map<String,Object> request) {
//        Map<String, Object> param = Maps.newHashMap();
//        param.put("modelId", modelId);
//        param.put("searchText", searchText);
//        param.put("pageNum", pageNo);
//        param.put("pageSize", pageSize);
        PageResponse<AlertFieldDetailDTO> response = new PageResponse<AlertFieldDetailDTO>();
        response.setCount(alertFieldDao.getAlertFieldListCountByModelId(request));
        response.setResult(alertFieldDao.getAlertFieldListByModelId(request));
        return response;
    }

    @Override
    @Transactional(rollbackFor= Exception.class)
    public void updateLockStatus(String id, String modelId,String isLock, String userName) {
        try {
            // 更新数据库
            alertFieldDao.updateLockStatus(id,isLock);
            // 添加操作日志
            AlertOperateLog alertOperateLog = new AlertOperateLog();
            alertOperateLog.setOperateContent("更新模型字段锁定状态");
            alertOperateLog.setOperateModel("alert_field");
            alertOperateLog.setOperateModelDesc("告警模型字段");
            alertOperateLog.setOperater(userName);
            alertOperateLog.setOperateTime(new Date());
            alertOperateLog.setOperateType("update");
            alertOperateLog.setOperateTypeDesc("更新模型字段锁定状态");
            alertOperateLog.setRelationId(id);
            alertOperateLogMapper.insert(alertOperateLog);
            // 更新redis
//            AlertModelRequestDTO alertModelDetail = alertModelDao.getAlertModelDetail(modelId);
            // 更新锁定状态可以不更新redis
            // redisTemplate.opsForHash().put(alertModelDetail.getTableName(), id, );

        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new RuntimeException(e);
        }
    }

    @Override
    @Transactional(rollbackFor= Exception.class)
    public void synchronizeField(String modelId, String userName) {
        try {
            // 获取 redis 数据
            AlertModelDetailDTO alertModelDetail = alertModelDao.getAlertModelDetail(modelId);
//            List<Object> values = redisTemplate.opsForHash().values(alertModelDetail.getTableName());
//            List<AlertFieldRequestDTO> alertFieldRequestDTOS = PayloadParseUtil.jacksonBaseParse(AlertFieldRequestDTO.class, values);
            List<AlertFieldRequestDTO> alertFieldRequestDTOS = getModelFromRedis(alertModelDetail.getTableName(), null);
            for (AlertFieldRequestDTO alertFieldRequestDTO : alertFieldRequestDTOS) {
                if (alertFieldRequestDTO.getFieldType().equals("2")) {
                    alertFieldDao.deleteAlertFieldDetailById(alertFieldRequestDTO.getId(),alertFieldRequestDTO.getModelId());
                    alertFieldDao.deleteFieldColumn(alertModelDetail.getTableName(),alertFieldRequestDTO.getFieldCode());
//                    redisTemplate.opsForHash().delete(alertModelDetail.getTableName(),alertFieldRequestDTO.getId());
                }
            }
            List<AlertFieldRequestDTO> alert_alerts = getModelFromRedis("alert_alerts", null);
            for (AlertFieldRequestDTO alertFieldRequestDTO : alert_alerts) {
                if (alertFieldRequestDTO.getFieldType().equals("1")) continue;
                alertFieldRequestDTO.setId(UUID.randomUUID().toString());
                alertFieldRequestDTO.setModelId(modelId);
                // 写入数据库
                alertFieldDao.insertAlertModel(alertFieldRequestDTO);
                Map<String, Object> map = Maps.newHashMap();
                map.put("tableName", alertModelDetail.getTableName());
                map.put("fieldCode", alertFieldRequestDTO.getFieldCode());
                map.put("jdbcType", alertFieldRequestDTO.getDataType());
                map.put("jdbcLength", StringUtils.isEmpty(alertFieldRequestDTO.getDataLength()) ? 0 : Integer.valueOf(alertFieldRequestDTO.getDataLength()));
                map.put("jdbcTip", alertFieldRequestDTO.getDataTip());
                alertFieldDao.addFieldColumn(map);
                // 更新redis
//                redisTemplate.opsForHash().put(alertModelDetail.getTableName(), alertFieldRequestDTO.getId(), alertFieldRequestDTO);
            }
            // 添加操作日志
            AlertOperateLog alertOperateLog = new AlertOperateLog();
            alertOperateLog.setOperateContent("同步模型字段");
            alertOperateLog.setOperateModel("alert_field");
            alertOperateLog.setOperateModelDesc("告警模型字段");
            alertOperateLog.setOperater(userName);
            alertOperateLog.setOperateTime(new Date());
            alertOperateLog.setOperateType("insert");
            alertOperateLog.setOperateTypeDesc("同步模型字段");
            alertOperateLog.setRelationId(modelId);
            alertOperateLogMapper.insert(alertOperateLog);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new RuntimeException(e);
        }

    }

    /**
     * 告警模型字段缓存
     * @param tableName
     * @return
     */
    public List<AlertFieldRequestDTO> getModelField (String tableName) {
        List<AlertFieldRequestDTO> alertFieldList = alertModelCache.get(tableName);
        if (CollectionUtils.isEmpty(alertFieldList)) {
            alertFieldList = getModelFromRedis(tableName, null);
            alertModelCache.put(tableName, alertFieldList);
        }
        return alertFieldList;
    }

    @Scheduled(cron = "0 0 */1 * * ?")
    public void flushAlertModelCache () {
        alertModelCache.clear();
    }
}
