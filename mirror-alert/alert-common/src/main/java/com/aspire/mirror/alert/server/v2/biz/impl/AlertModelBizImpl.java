package com.aspire.mirror.alert.server.v2.biz.impl;

import com.aspire.mirror.alert.server.dao.AlertOperateLogMapper;
import com.aspire.mirror.alert.server.dao.po.AlertOperateLog;
import com.aspire.mirror.alert.server.v2.biz.AlertModelBiz;
import com.aspire.mirror.alert.server.v2.dao.AlertModelDao;
import com.aspire.mirror.alert.server.v2.dao.po.AlertModelDetailDTO;
import com.aspire.mirror.alert.server.v2.dao.po.AlertModelRequestDTO;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
public class AlertModelBizImpl implements AlertModelBiz {

    @Autowired
    private AlertModelDao alertModelDao;
    @Autowired
    private AlertOperateLogMapper alertOperateLogMapper;

    @Override
    public void insertAlertModel(AlertModelRequestDTO request) {
        // 设置id
        request.setId( UUID.randomUUID().toString());
        // 写入数据库
        alertModelDao.insertAlertModel(request);
        // 添加操作日志
        AlertOperateLog alertOperateLog = new AlertOperateLog();
        alertOperateLog.setOperateContent("新增模型");
        alertOperateLog.setOperateModel("alert_model");
        alertOperateLog.setOperateModelDesc("告警模型");
        alertOperateLog.setOperater(request.getCreator());
        alertOperateLog.setOperateTime(new Date());
        alertOperateLog.setOperateType("insert");
        alertOperateLog.setOperateTypeDesc("新增");
        alertOperateLogMapper.insert(alertOperateLog);
    }

    @Override
    public List<AlertModelDetailDTO> getAlertModelList(String modelName, String tableName) {
        return alertModelDao.getAlertModelList(modelName, tableName);
    }

    @Override
    public Object getAlertModelTreeData() {
        List<AlertModelDetailDTO> res = Lists.newArrayList();
        List<AlertModelDetailDTO> alertModelTreeData = alertModelDao.getAlertModelTreeData();
        res.add(getTree(alertModelTreeData,null));
        return res;
    }

    private AlertModelDetailDTO getTree(List<AlertModelDetailDTO> data,
                                        AlertModelDetailDTO alertModelDetailDTO) {
        if (null == alertModelDetailDTO) {
            alertModelDetailDTO = new AlertModelDetailDTO();
            alertModelDetailDTO.setId("all");
            alertModelDetailDTO.setParentId("-1");
            alertModelDetailDTO.setModelName("模型");
            alertModelDetailDTO.setDescription("根节点");
            alertModelDetailDTO.setSort(0);
            alertModelDetailDTO.setChildList(new ArrayList<AlertModelDetailDTO>());
            getTree(data,alertModelDetailDTO);
        } else {
            List<AlertModelDetailDTO> childList = alertModelDetailDTO.getChildList();
            for (AlertModelDetailDTO datum : data) {
                if (datum.getParentId().equals(alertModelDetailDTO.getId())) {
                    childList.add(datum);
                    getTree(data,datum);
                }
            }
            List<AlertModelDetailDTO> sorted =
                    childList.stream().sorted(Comparator.comparing(AlertModelDetailDTO::getSort)).collect(Collectors.toList());
            alertModelDetailDTO.setChildList(sorted);
        }
        return alertModelDetailDTO;
    }

    @Override
    public void deleteAlertModel(String userName,List<String> request) {
        alertModelDao.deleteAlertModel(request);
        // 操作日志
        AlertOperateLog alertOperateLog = new AlertOperateLog();
        alertOperateLog.setOperateContent("删除模型");
        alertOperateLog.setOperateModel("alert_model");
        alertOperateLog.setOperateModelDesc("告警模型");
        alertOperateLog.setOperater(userName);
        alertOperateLog.setOperateTime(new Date());
        alertOperateLog.setOperateType("delete");
        alertOperateLog.setOperateTypeDesc("删除");
        alertOperateLogMapper.insert(alertOperateLog);
    }

    @Override
    public AlertModelDetailDTO getAlertModelDetail(String id) {
        return alertModelDao.getAlertModelDetail(id);
    }

    @Override
    public void updateAlertModel(AlertModelRequestDTO request) {
        alertModelDao.updateAlertModel(request);
        // 操作日志
        AlertOperateLog alertOperateLog = new AlertOperateLog();
        alertOperateLog.setOperateContent("编辑模型");
        alertOperateLog.setOperateModel("alert_model");
        alertOperateLog.setOperateModelDesc("告警模型");
        alertOperateLog.setOperater(request.getUpdater());
        alertOperateLog.setOperateTime(new Date());
        alertOperateLog.setOperateType("update");
        alertOperateLog.setOperateTypeDesc("编辑");
        alertOperateLogMapper.insert(alertOperateLog);
    }
}
