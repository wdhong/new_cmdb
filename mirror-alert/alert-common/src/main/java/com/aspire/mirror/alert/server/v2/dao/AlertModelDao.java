package com.aspire.mirror.alert.server.v2.dao;

import com.aspire.mirror.alert.server.v2.dao.po.AlertModelDetailDTO;
import com.aspire.mirror.alert.server.v2.dao.po.AlertModelRequestDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AlertModelDao {

    /**
     *  添加告警模型
     */
    void insertAlertModel(AlertModelRequestDTO request);
    /**
     *  获取告警模型列表
     */
    List<AlertModelDetailDTO> getAlertModelList(@Param("modelName") String modelName,
                                                 @Param("tableName") String tableName);

    List<AlertModelDetailDTO> getAlertModelTreeData();
    /**
     *  删除告警模型
     */
    void deleteAlertModel(@Param("list") List<String> request);

    /**
     *  根据id获取告警模型详情
     */
    AlertModelDetailDTO getAlertModelDetail(@Param("id") String id);
    /**
     *  编辑告警模型
     */
    void updateAlertModel(AlertModelRequestDTO request);
}
