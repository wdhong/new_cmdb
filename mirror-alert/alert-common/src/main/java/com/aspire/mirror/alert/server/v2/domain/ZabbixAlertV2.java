package com.aspire.mirror.alert.server.v2.domain;

import com.aspire.mirror.alert.server.biz.helper.CmdbHelper;
import com.aspire.mirror.alert.server.domain.AlertsDTO;
import com.aspire.mirror.alert.server.util.StringUtils;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Map;

/**
 * @BelongsProject: mirror-alert
 * @BelongsPackage: com.aspire.mirror.alert.server.v2.domain
 * @Author: baiwenping
 * @CreateTime: 2020-02-21 16:33
 * @Description: ${Description}
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ZabbixAlertV2 {
    public static final String	SOURCE_ZABBIX				= "ZABBIX";
    public static final String	BIZ_MONITOR_ITEM_KEY_PREFIX	= "YWZB_";

    private String	moniResult;		// 指标监测结果 1: 新增告警 2: 解除告警

    @JsonProperty("alert_id")
    private String alertId;

    @JsonProperty("z_alert_Id")
    private String	zbxAlertId;		// zabbix中告警id

    //	private String	monitorSource;	// 第三方监控系统编码, 在第三方的告警中，当作proxyName
    private String	deviceIP;		// 所属设备
    private String	servSystem;		// 所属业务系统
    private String	monitorObject;	// 监控对象
    private String	monitorIndex;	// 监控指标
    private String	moniIndexValue;
    private String	alertLevel;		// 告警级别
    private String	alertDesc;		// 告警描述
    private String	curMoniTime;	// 当前监测时间 yyyy-MM-dd HH:mm:ss
    private String	curMoniValue;	// 当前监测值
    private String	businessSystem;	// 在Zabbix系统告警中，当作proxyName

    @JsonProperty("z_itemId")
    private String	zbxItemId;
    private String objectType;
    /**
     * 告警开始时间
     */
    private String alertStartTime;
    /**
     * 告警前缀，用于区分不同zabbix
     */
    private String source;
    /**
     * 监控对象
     */
    private String	itemKey;

    /**
     * 监控对象描述/告警标题
     */
    private String	keyComment;

    private Map<String, Object> ext;

    public AlertsDTOV2 parse(final CmdbHelper cmdbHelper) throws Exception {
        AlertsDTOV2 dto = new AlertsDTOV2();
//		dto.setAlertId(this.getAlertId());
        dto.setRAlertId(this.getAlertId());
        dto.setAlertLevel(this.getAlertLevel());

        DateFormat format = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        dto.setCurMoniTime(format.parse(this.getCurMoniTime()));
        dto.setCurMoniValue(this.getCurMoniValue());
        if (StringUtils.isNotEmpty(this.getAlertStartTime())) {
            dto.setAlertStartTime(format.parse(this.getAlertStartTime()));
        } else {
            dto.setAlertStartTime(format.parse(this.getCurMoniTime()));
        }
        dto.setDeviceIp(this.getDeviceIP());
        dto.setBizSys(this.getServSystem());
        dto.setAlertType(this.getMoniResult());
        dto.setItemId(this.getZbxItemId()); 		// 后续均为保存微服务中定义的itemid
        dto.setMoniIndex(this.getMonitorIndex());
        dto.setMoniObject(this.getMonitorObject());
        dto.setSource(this.getSource());
        dto.setItemKey(this.getItemKey());
        dto.setKeyComment(this.getKeyComment());
        dto.setRemark(this.getAlertDesc());
        String idcType = cmdbHelper.getIdc(this.getBusinessSystem());
        dto.setIdcType(idcType);
        dto.setAlertCount(1);
        dto.setExt(this.getExt());
        if (StringUtils.isNotEmpty(this.getObjectType())) {
            dto.setObjectType(this.getObjectType());
        } else {
            dto.setObjectType(AlertsDTO.OBJECT_TYPE_DEVICE);
        }
        return dto;
    }

//    @Override
//    public boolean equals(Object obj) {
//        if (!ZabbixAlert.class.isInstance(obj)) {
//            return false;
//        }
//        ZabbixAlert other = ZabbixAlert.class.cast(obj);
//        if (this.getMonitorRoom() == null) {
//            return false;
//        }
//        if (this.umsAlertId == null) {
//            return false;
//        }
//        return getMonitorRoom().equals(other.getMonitorRoom()) && getUmsAlertId().equals(other.getUmsAlertId());
//    }
//
//    @Override
//    public int hashCode() {
//        int hash = 17;
//        hash = hash * 13 + (getMonitorRoom() == null ? "" : getMonitorRoom()).hashCode();
//        hash = hash * 13 + (getUmsAlertId() == null ? "" : getUmsAlertId()).hashCode();
//        return hash;
//    }
}
