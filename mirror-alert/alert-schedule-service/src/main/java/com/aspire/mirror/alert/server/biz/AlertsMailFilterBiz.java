package com.aspire.mirror.alert.server.biz;

import com.aspire.mirror.alert.server.dao.po.AlertMailFilter;
import com.aspire.mirror.common.entity.PageRequest;
import com.aspire.mirror.common.entity.PageResponse;

import java.util.List;

public interface AlertsMailFilterBiz {
    AlertMailFilter selectById(String id);
    void insertAlertsMailFilter(AlertMailFilter mailFilter);
    void updateAlertsMailFilter(AlertMailFilter mailFilter);
    void removeAlertsMailFilter(List<String> idList);
    PageResponse<AlertMailFilter> select(PageRequest pageRequest);
}
