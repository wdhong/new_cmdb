package com.aspire.mirror.alert.server.biz;

public interface AutoConfirmClearScheduleBiz {

    void autoConfirm();

    void autoClear();

    void delete();
}
