package com.aspire.mirror.alert.server.biz;

import com.aspire.mirror.alert.api.dto.CycReportResFileRequest;

import java.util.List;

public interface CycReportResourceBiz {

    void insertFtpFiles(List<CycReportResFileRequest> files);
    void updateFtpFileReadStat(String filePath);
}
