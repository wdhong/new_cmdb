package com.aspire.mirror.alert.server.biz;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aspire.mirror.alert.api.dto.ItemMonitorEventCallBackRequest;

import lombok.extern.slf4j.Slf4j;

/**
* 监控事件处理chain   <br/>
* Project Name:alert-service
* File Name:ItemEventCallBackHandleChain.java
* Package Name:com.aspire.mirror.template.server.biz
* ClassName: ItemEventCallBackHandleChain <br/>
* date: 2018年10月11日 上午10:47:39 <br/>
* @author pengguihua
* @version 
* @since JDK 1.6
*/ 
@Slf4j
@Component
public final class ItemEventCallBackHandleChain {
	@Autowired(required = false)
	private List<ItemMonitorEventHandler> itemDataHandlerList;
	
	public void handle(ItemMonitorEventCallBackRequest monitorEvent) {
		if (CollectionUtils.isEmpty(itemDataHandlerList)) {
			log.error("There is no ItemMonitorEventHandler implemention defined in the system.");
			return;
		}
		for (ItemMonitorEventHandler handler : itemDataHandlerList) {
			if (handler.isAware(monitorEvent)) {
				handler.handle(monitorEvent);
				return;
			}
		}
		log.warn("There is no ItemMonitorEventHandler implemention to handle the item monitor Event {}", monitorEvent);
	}
	
	/**
	* 监控项值回调处理接口    <br/>
	* Project Name:alert-service
	* File Name:ItemMonitorEventHandler.java
	* Package Name:com.aspire.mirror.template.server.biz
	* ClassName: ItemMonitorEventHandler <br/>
	* date: 2018年10月11日 上午10:49:01 <br/>
	* @author pengguihua
	* @version 
	* @since JDK 1.6
	*/ 
	public interface ItemMonitorEventHandler {
		/**
		* 是否需要处理该监控项回调. <br/>
		*
		* 作者： pengguihua
		* @param itemData
		* @return
		*/  
		public boolean isAware(ItemMonitorEventCallBackRequest itemData);
		
		/**
		* 处理监控项值回调. <br/>
		*
		* 作者： pengguihua
		* @param itemData
		*/  
		public void handle(ItemMonitorEventCallBackRequest itemData);
	}
}
