package com.aspire.mirror.alert.server.biz.impl;

import com.aspire.mirror.alert.server.clientservice.process.ProcessService;
import com.aspire.mirror.alert.server.clientservice.process.ProcessServiceImplService;
import com.aspire.mirror.alert.server.dao.AlertsDao;
import com.aspire.mirror.alert.server.dao.po.Alerts;
import com.aspire.mirror.common.constant.Constant;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * 告警生成工单
 * <p>
 * 项目名称:  mirror平台
 * 包:        com.aspire.mirror.alert.server.biz.impl
 * 类名称:    AlertGenOrder.java
 * 类描述:    告警生成工单
 * 创建人:    JinSu
 * 创建时间:  2018/9/28 10:51
 * 版本:      v1.0
 */
@Service
public class AlertGenOrder {
    private static final Logger LOGGER = LoggerFactory.getLogger(AlertGenOrder.class);

//    @Value("${ORDER_AUTOGEN_LEVEL}")
    private String levels;

//    @Value("${FULLNAME}")
    private String fullName;

//    @Value("${FLOW_KEY}")
    private String flowKey;

//    @Value("${WSDL_URL}")
    private String wsdlUrl;

    /**
     * 自动派单
     */
//    @Scheduled(cron = "${orderAutoGen.cron}")
    private void orderAutoGenJob() {
        String[] levelArr = levels.split(",");
        for (String lev : levelArr) {
            Map paramMap = Maps.newHashMap();
            paramMap.put("alertLevel", lev); //告警级别
            paramMap.put("orderStatus", Constant.ORDER_BEFOR); //工单状态：未派单
            List<Map<String, Object>> list = alertsDao.selectOrderParam(paramMap);
            syncOrder(list);
        }
    }

    /**
     * 同步工单
     *
     * @param list 工单信息列表
     */
    public void syncOrder(List<Map<String, Object>> list) {
        if (list != null && list.size() > 0) {
            LOGGER.info("需要进行工单生成的告警数量为" + list.size());
            for (Map<String, Object> map : list) {
                // 告警工单自动生成
                Map<String, Object> mapNew = new HashMap<String, Object>();
                mapNew.put("fullname", fullName);
                //转换流程流转变量LEV-lev
                Set<Map.Entry<String, Object>> entries = map.entrySet();

                for (Map.Entry<String, Object> entry : entries) {
                    //System.out.println(entry.getKey()+":"+entry.getValue());
                    mapNew.put(entry.getKey().toLowerCase(), entry.getValue());
                }
                // TODO 走咪咕自动审批工单处理人
                String[] userSplit = fullName.split("/");
                String account = userSplit.length > 1 ? userSplit[1] : userSplit[0];
                //组装请求参数
                String xml = buildReqest(flowKey,
                        mapNew.get("title").toString(),
                        account,
                        mapNew);
                LOGGER.info("UMS调用BPM发起流程接口，发起请求：" + xml);
                //System.out.println("xml:"+xml);

                //调用webservice接口启动流程

                try {
                    ProcessServiceImplService.setBpmWsdlUrl(wsdlUrl);
                    ProcessServiceImplService s = new ProcessServiceImplService();
                    ProcessService port = s.getProcessServiceImplPort();
                    String result = port.start(xml);
                    //System.out.println("result:"+result);
                    LOGGER.info("UMS调用BPM发起流程接口，返回结果：" + result);

                    //解析返回结果
                    Document resultDom = DocumentHelper.parseText(result);
                    Element e = resultDom.getRootElement();
                    if (e.attribute("result") != null && e.attribute("result").getText().equals("false")) {
                        LOGGER.error("告警工单BPM接口调用异常，响应信息：" + e.attribute("message").getText());
                    } else {
//                                String actDefId = e.attribute("actDefId").getText();
//                                String businessKey = e.attribute("businessKey").getText();
                        String runId = e.attribute("runId").getText();

                        //根据返回的结果，修改告警时间
                        Alerts updateE = new Alerts();
                        updateE.setAlertId((String) map.get("alertid"));
                        updateE.setOrderStatus(Constant.ORDER_DEALING);
                        updateE.setOrderId(runId);
//                        updateE.setObjectType(Constant.ORDER_WARN);
                        alertsDao.updateByPrimaryKey(updateE);
                    }
                } catch (Exception e) {
                    LOGGER.error("告警工单BPM接口调用异常，错误信息：" + e.getMessage());
                }

            }
        }
        LOGGER.info("-----------------离开工单自动生成接口AlertOrderAutoGenJob------------------");
    }

    private String buildReqest(String flowKey, String subject, String account, Map<String, Object> map) {
        StringBuffer sb = new StringBuffer();
        String head =
                "<req flowKey=\"{flowKey}\" subject=\"{subject}\" account=\"{account}\" businessKey=\"\" runId=\"\">"
                        + "<data>";
        sb.append(head.replace("{flowKey}", flowKey)
                .replace("{subject}", subject)
                .replace("{account}", account));

        Map<String, Object> jmap = new HashMap<String, Object>();
        Map<String, Object> main = new HashMap<String, Object>();
        List sub = new ArrayList();
        List opinion = new ArrayList();
        jmap.put("main", main);
        main.put("fields", map);
        jmap.put("sub", sub);
        jmap.put("opinion", opinion);

        GsonBuilder gb = new GsonBuilder();
        Gson g = gb.create();
        //System.out.println(g.toJson(jmap));

        sb.append(g.toJson(jmap));
        sb.append("</data></req>");


        return sb.toString();
    }

    @Autowired
    private AlertsDao alertsDao;
}
