package com.aspire.mirror.alert.server.biz.impl;

import com.aspire.mirror.alert.server.biz.AlertMonthReportBiz;
import com.aspire.mirror.alert.server.biz.helper.CmdbHelper;
import com.aspire.mirror.alert.server.clientservice.CmdbDictClient;
import com.aspire.mirror.alert.server.dao.AlertsMonReportDao;
import com.aspire.mirror.alert.server.domain.AlertsMonReportAlertDTO;
import com.aspire.mirror.alert.server.domain.AlertsMonReportIdcTypeDTO;
import com.aspire.mirror.alert.server.util.StringUtils;
import com.aspire.ums.cmdb.dict.payload.ConfigDict;
import com.aspire.ums.cmdb.instance.payload.CmdbInstance;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
public class AlertMonthReportBizImpl implements AlertMonthReportBiz {

//    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//    private SimpleDateFormat sdfMonth = new SimpleDateFormat("yyyy-MM");

//    @Autowired
//    private CmdbDictClient cmdbDictClient;
//    @Autowired
//    private AlertsMonReportDao alertsMonReportDao;
//    @Autowired
//    private CmdbHelper cmdbHelper;

//    private Map<String,Object> getTimeRange() {
//        Map<String,Object> map = Maps.newHashMap();
//        //获取前一个月第一天
//        Calendar calendar1 = Calendar.getInstance();
//        calendar1.add(Calendar.MONTH, -1);
//        calendar1.set(Calendar.DAY_OF_MONTH,1);
//        String firstDay = sdf.format(calendar1.getTime());
//        //获取当前月第一天
//        Calendar calendar2 = Calendar.getInstance();
//        calendar2.add(Calendar.MONTH, 0);
//        calendar2.set(Calendar.DAY_OF_MONTH, 1);
//        String nowFirstDay = sdf.format(calendar2.getTime());
//
//        Calendar calendar3 = Calendar.getInstance();
//        calendar3.add(Calendar.MONTH, -1);
//        String month = sdfMonth.format(calendar3.getTime());
//
//        map.put("startTime",firstDay);
//        map.put("endTIme",nowFirstDay);
//        map.put("month",month);
//        return map;
//    }



    @Override
    public void alert() {
//        try {
//            //获取时间范围-前一个月范围
//            Map<String, Object> alertParam = getTimeRange();
//            //获取资源池
//            List<ConfigDict> idcType = cmdbDictClient.getDictsByType("idcType",null,null,null);
//            List<AlertsMonReportIdcTypeDTO> res = Lists.newArrayList();
//            for (ConfigDict configDict : idcType) {
//                alertParam.put("idcType",configDict.getValue());
//                //严重
//                AlertsMonReportIdcTypeDTO alertIdcS = getAlertIdc( alertParam, "5" );
//                res.add(alertIdcS);
//                //高
//                AlertsMonReportIdcTypeDTO alertIdcH = getAlertIdc( alertParam, "4" );
//                res.add(alertIdcH);
//                //中
//                AlertsMonReportIdcTypeDTO alertIdcM = getAlertIdc( alertParam, "3" );
//                res.add(alertIdcM);
//                //低
//                AlertsMonReportIdcTypeDTO alertIdcL = getAlertIdc( alertParam, "2" );
//                res.add(alertIdcL);
//            }
//            //写入数据库
//            for (AlertsMonReportIdcTypeDTO alertsMonReportIdcTypeDTO : res) {
//                alertsMonReportDao.insertAlertMonReportIdc(alertsMonReportIdcTypeDTO);
//            }
//        } catch (Exception e) {
//            log.error("[运营月报告警分布定时任务] error is {}",e);
//        }

    }

//    private AlertsMonReportIdcTypeDTO getAlertIdc(Map<String, Object> alertParam,String alertLevel) {
//        alertParam.put("alertLevel",alertLevel);
//        AlertsMonReportIdcTypeDTO alertIdcData = alertsMonReportDao.getAlertIdcData(alertParam);
//        if (null == alertIdcData) {
//            AlertsMonReportIdcTypeDTO result = new AlertsMonReportIdcTypeDTO();
//            result.setIdcType(String.valueOf(alertParam.get("idcType")));
//            result.setAlertLevel(alertLevel);
//            result.setMon(String.valueOf(alertParam.get("month")));
//            return result;
//        } else {
//        List<Map<String, Object>> alertByDeviceType = alertsMonReportDao.getAlertByDeviceType(alertParam);
//        int diskArrayCount = alertIdcData.getDiskArrayCount();
//        int tapeLibraryCount = alertIdcData.getTapeLibraryCount();
//        if (CollectionUtils.isNotEmpty(alertByDeviceType)) {
//            for (Map<String, Object> map : alertByDeviceType) {
//                if (null == map.get("ip")) continue;
//                String idcType = null == map.get("idcType") ? null : String.valueOf(map.get("idcType"));
//                CmdbInstance cmdbInstance = cmdbHelper.queryDeviceByRoomIdAndIP(idcType,String.valueOf(map.get("ip")));
//                if (cmdbInstance.getDeviceClass3().equals("磁盘阵列")) {
//                    diskArrayCount += (int)map.get("count");
//                } else if (cmdbInstance.getDeviceClass3().equals("磁带库")) {
//                    tapeLibraryCount += (int)map.get("count");
//                }
//            }
//        }
//        alertIdcData.setDiskArrayCount(diskArrayCount);
//        alertIdcData.setTapeLibraryCount(tapeLibraryCount);
//            alertIdcData.setMon(String.valueOf(alertParam.get("month")));
//        }
//        return alertIdcData;
//    }

    @Override
    public void device() {
//        try {
//            //获取时间范围-前一个月范围
//            Map<String, Object> param = getTimeRange();
//            // 获取资源池
//            List<ConfigDict> idcType = cmdbDictClient.getDictsByType("idcType",null,null,null);
//            List<AlertsMonReportAlertDTO> list = Lists.newArrayList();
//            for (ConfigDict configDict : idcType) {
//                param.put("idcType",configDict.getValue());
//                // 严重
//                List<AlertsMonReportAlertDTO> deviceSData = getDeviceData( param, "5" );
//                list.addAll(deviceSData);
//                //高
//                List<AlertsMonReportAlertDTO> deviceHData = getDeviceData( param, "4" );
//                list.addAll(deviceHData);
//                //中
//                List<AlertsMonReportAlertDTO> deviceMData = getDeviceData( param, "3" );
//                list.addAll(deviceMData);
//                //低
//                List<AlertsMonReportAlertDTO> deviceLData = getDeviceData( param, "2" );
//                list.addAll(deviceLData);
//            }
//            //写入数据库
//            if (CollectionUtils.isNotEmpty(list)) {
//                for (AlertsMonReportAlertDTO alertsMonReportAlertDTO : list) {
//                    alertsMonReportDao.insertMonReportAlertDevice(alertsMonReportAlertDTO);
//                }
//            }
//        } catch (Exception e) {
//            log.error("[运营月报告警设备定时任务] error is {}",e);
//        }

    }

//    private List<AlertsMonReportAlertDTO> getDeviceData (Map<String, Object> param, String alertLevel) {
//        param.put("alertLevel",alertLevel);
//        List<AlertsMonReportAlertDTO> deviceData = alertsMonReportDao.getDeviceData(param);
//        if (CollectionUtils.isNotEmpty(deviceData)) {
//            int i=1;
//            for (AlertsMonReportAlertDTO alertsMonReportAlertDTO : deviceData) {
//                alertsMonReportAlertDTO.setRank(i++);
//                alertsMonReportAlertDTO.setMonth(String.valueOf(param.get("month")));
//                StringBuffer mrfsModelOption = new StringBuffer();
//                if (StringUtils.isNotEmpty(alertsMonReportAlertDTO.getDeviceMrfs()))
//                    mrfsModelOption.append(alertsMonReportAlertDTO.getDeviceMrfs());
//                if (StringUtils.isNotEmpty(alertsMonReportAlertDTO.getDeviceModel()))
//                    mrfsModelOption.append(alertsMonReportAlertDTO.getDeviceModel());
//                mrfsModelOption.append(alertsMonReportAlertDTO.getIdcType());
//                alertsMonReportAlertDTO.setMrfsModelOption(mrfsModelOption.toString());
//                StringBuffer idcPod = new StringBuffer();
//                idcPod.append(alertsMonReportAlertDTO.getIdcType());
//                if (StringUtils.isNotEmpty(alertsMonReportAlertDTO.getPod()))
//                    idcPod.append(alertsMonReportAlertDTO.getPod());
//                alertsMonReportAlertDTO.setIdcPod(idcPod.toString());
//            }
//        }
//
//        return deviceData;
//    }

    @Override
    public void alertIndex() {
//        //获取时间范围-前一个月范围
//        Map<String, Object> indexParam = getTimeRange();
//        // 获取资源池
//        List<ConfigDict> idcType = cmdbDictClient.getDictsByType("idcType",null,null,null);
//        // 上月告警数量总和
//        float alertIndexSum = alertsMonReportDao.getAlertIndexSum(indexParam);
//        List<AlertsMonReportAlertDTO> list = Lists.newArrayList();
//        for (ConfigDict configDict : idcType) {
//            indexParam.put("idcType",configDict.getValue());
//            // 严重
//            List<AlertsMonReportAlertDTO> deviceSData = getAlertIndexData( indexParam, "5", alertIndexSum );
//            list.addAll(deviceSData);
//            //高
//            List<AlertsMonReportAlertDTO> deviceHData = getAlertIndexData( indexParam, "4", alertIndexSum );
//            list.addAll(deviceHData);
//            //中
//            List<AlertsMonReportAlertDTO> deviceMData = getAlertIndexData( indexParam, "3", alertIndexSum );
//            list.addAll(deviceMData);
//            //低
//            List<AlertsMonReportAlertDTO> deviceLData = getAlertIndexData( indexParam, "2", alertIndexSum );
//            list.addAll(deviceLData);
//        }
//        //写入数据库
//        if (CollectionUtils.isNotEmpty(list)) {
//            for (AlertsMonReportAlertDTO alertsMonReportAlertDTO : list) {
//                alertsMonReportDao.insertMonReportAlertIndex(alertsMonReportAlertDTO);
//            }
//        }
    }

//    private List<AlertsMonReportAlertDTO> getAlertIndexData (Map<String, Object> param, String alertLevel, float alertIndexSum) {
//        param.put("alertLevel",alertLevel);
//        List<AlertsMonReportAlertDTO> getAlertIndexData = alertsMonReportDao.getAlertIndexData(param);
//
//        if (CollectionUtils.isNotEmpty(getAlertIndexData)) {
//            int n = 1;
//            for (AlertsMonReportAlertDTO alertsMonReportAlertDTO : getAlertIndexData) {
//                alertsMonReportAlertDTO.setRank(n++);
//                alertsMonReportAlertDTO.setMonth(String.valueOf(param.get("month")));
//                if (alertIndexSum > 0) {
//                    float v = alertsMonReportAlertDTO.getSCount() / alertIndexSum;
//                    BigDecimal bd = new BigDecimal(v);
//                    double  retValue = bd.setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue() * 100;
//                    int i = (new Double(retValue)).intValue();
//                    alertsMonReportAlertDTO.setRate(String.valueOf(i));
//                }
//            }
//        }
//
//        return getAlertIndexData;
//    }

    @Override
    public void alertSum() {
//        //获取时间范围-前一个月范围
//        Map<String, Object> indexParam = getTimeRange();
//        // 查询数据库
//        List<Map<String,Object>> alertIdcData = alertsMonReportDao.getIdcTypeAlertCount(indexParam);
//        for (Map<String,Object> map : alertIdcData) {
//            map.put("mon", indexParam.get("month"));
//            alertsMonReportDao.insertAlertMonReportSum(map);
//        }

    }


}
