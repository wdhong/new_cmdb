package com.aspire.mirror.alert.server.biz.impl;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aspire.mirror.alert.server.biz.AlertCabinetColumnCommonBiz;
import com.aspire.mirror.alert.server.biz.AlertPoorEfficiencyDeviceBiz;
import com.aspire.mirror.alert.server.clientservice.MonthDayReportNewServiceClient;
import com.aspire.mirror.alert.server.dao.AlertCabinetColumnDao;
import com.aspire.mirror.alert.server.dao.AlertsPoorEfficiencyDeviceDao;
import com.aspire.mirror.alert.server.dao.po.AlertCabinetColumnConfig;
import com.aspire.mirror.alert.server.dao.po.AlertCabinetColumnConfigData;
import com.aspire.mirror.alert.server.util.DateUtils;
import com.aspire.mirror.elasticsearch.api.dto.MonthReportRequest;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AlertPoorEfficiencyDeviceImpl implements AlertPoorEfficiencyDeviceBiz {
	@Autowired
	private AlertsPoorEfficiencyDeviceDao alertsPoorEfficiencyDeviceDao;

	@Autowired
	private MonthDayReportNewServiceClient MonthDayReportNewServiceClient;
	
	@Autowired
    private AlertCabinetColumnDao alertCabinetColumnDao;
	
	
	@Autowired
	private AlertCabinetColumnCommonBiz alertCabinetColumnCommonBiz;
	
	private DecimalFormat   df   =new   java.text.DecimalFormat("#.00");  

	@Override
	public void getPoorEfficiencyDeviceMonthData(String item ,int count) throws Exception {
		try {
			Map<String,Object> param  = Maps.newHashMap();
			//param.put("date", value);
			param.put("item", item);
			param.put("type", 2);
			Map<String,Object> map = alertsPoorEfficiencyDeviceDao.getDeviceDataLatestDate(param);//数据库最近的时间
			Date lastMonth = DateUtils.getTimesLastMonthmorning(null);//上月日期
			if(null!=map) {
				Date hisDate = DateUtils.stringToDate(DateUtils.DEFAULT_DATETIME_FMT, map.get("date").toString());
				Date curDate = DateUtils.getTimesNextMonthmorning(hisDate);//数据库最近时间的 下月时间
				while(curDate.getTime()<=lastMonth.getTime()) {//循环查询数据
					queryMonthData(curDate,item);
					curDate = DateUtils.getTimesNextMonthmorning(curDate);
				}
				
			}else {
				queryMonthData(lastMonth,item);//第一次跑任务
			}
		}catch(Exception e) {
			if(count>0) {
				throw e;
			}else {
				count++;
			}
			log.error("低设备性能按月跑数据第一次报错",e);
			getPoorEfficiencyDeviceMonthData(item,0);
		}
		
	}
	//查询月数据
	@Override
	public void queryMonthData(Date curDate,String item) throws Exception {
		Map<String,Object> logInfo = getLogInfo(item,curDate,2);
		try {
			MonthReportRequest request = new MonthReportRequest();
			String startTime = DateUtils.datetimeToString(DateUtils.DEFAULT_DATETIME_FMT, curDate);
			request.setStartTime(startTime);
			logInfo.put("date", startTime);
			request.setItem(item);
			request.setType(2);
			List<Map<String, Object>> dataList = MonthDayReportNewServiceClient.getPoorEfficiencyDeviceMonthData(request);
			if(null!=dataList && dataList.size()>0) {
				alertsPoorEfficiencyDeviceDao.insertPoorEfficiencyDeviceData(dataList);
				dataList.clear();
				dataList = null;
			}
			
		}catch(Exception e) {
			logInfo.put("status", "error");
			StringBuffer sb = new StringBuffer();
			String msg = sb.append("月任务：").append(item).append(curDate).append(e.getClass().getName())
					.append(":").append(e.getMessage()).toString();
			logInfo.put("content",msg);
			throw e;
			
		}finally {
			logInfo.put("create_time", new Date());
			alertsPoorEfficiencyDeviceDao.insertDeviceLogData(logInfo);//记录日志
		}
		
	}
	
	

	private Map<String, Object> getLogInfo(String item,Date curDate,int type) {
		Map<String,Object> logInfo = Maps.newHashMap();
		logInfo.put("type", type);
		logInfo.put("item", item);
		logInfo.put("exec_time", new Date());
		logInfo.put("status", "success");
		return logInfo;
	}

	@Override
	public void getPoorEfficiencyDeviceWeekData(String item,int count) throws Exception {
		try {
		Map<String,Object> param  = Maps.newHashMap();
		//param.put("date", value);
		param.put("item", item);
		param.put("type", 2);
		
		//判断上个月的日志是否存在
		Date lastMonth = DateUtils.getTimesLastMonthmorning(null);//上月日期
		String lastMonthStr = DateUtils.datetimeToString(DateUtils.DEFAULT_DATETIME_FMT, lastMonth);
		param.put("date", lastMonthStr);
		//List<Map<String,Object>> monthList = alertsPoorEfficiencyDeviceDao.getDeviceDataByDate(param);
		Map<String,Object> lastLog = alertsPoorEfficiencyDeviceDao.getDeviceDataLatestDate(param);
		if(null==lastLog) {
			this.getPoorEfficiencyDeviceMonthData(item,0);
		
		}
		param.put("type", 1);
		param.remove("date");
		Map<String,Object> latestLog = alertsPoorEfficiencyDeviceDao.getDeviceDataLatestDate(param);//数据库最近的时间
		Date lastWeek = DateUtils.getLaskWeekMonday(null);//上周一
		if(null!=latestLog) {
			Date hisDate = DateUtils.stringToDate(DateUtils.DEFAULT_DATETIME_FMT, latestLog.get("date").toString());
			Date curDate = DateUtils.getNextWeekMonday(hisDate);//数据库最近时间的 下周时间
			while(curDate.getTime()<=lastWeek.getTime()) {//循环查询数据
				queryWeekData(curDate,item);
				curDate = DateUtils.getNextWeekMonday(curDate);
			}
			
		}else {
			queryWeekData(lastWeek,item);
		}
		}catch(Exception e) {
			log.error("低设备性能按周跑数据第一次报错",e);
			if(count>0) {
				throw e;
			}else {
				count++;
			}
			getPoorEfficiencyDeviceWeekData(item,count);
			
		}
		
	}
	//查询周数据
	@Override
	public void queryWeekData(Date curDate,String item) throws Exception {
		Map<String,Object> logInfo = getLogInfo(item,curDate,1);
		try {
			MonthReportRequest request = new MonthReportRequest();
			String startTime = DateUtils.datetimeToString(DateUtils.DEFAULT_DATETIME_FMT, curDate);
			request.setStartTime(startTime);
			logInfo.put("date", startTime);
			long dateLong = 6*24*60*60*1000l;//获取周日日期0点
			String endTime =  DateUtils.datetimeToString(DateUtils.DEFAULT_DATETIME_FMT, new Date(curDate.getTime()+dateLong));
			request.setEndTime(endTime);
			request.setItem(item);
			request.setType(2);
			List<Map<String, Object>> dataList = MonthDayReportNewServiceClient.getPoorEfficiencyDeviceWeekData(request);
			if(null!=dataList && dataList.size()>0) {
				getCompareData( curDate,item,dataList);//获取与上月差值
				alertsPoorEfficiencyDeviceDao.insertPoorEfficiencyDeviceData(dataList);
				dataList.clear();
				dataList = null;
			}
			
		}catch(Exception e) {
			logInfo.put("status", "error");
			StringBuffer sb = new StringBuffer();
			String msg = sb.append("周任务：").append(item).append(curDate).append(e.getClass().getName())
					.append(":").append(e.getMessage()).toString();
			logInfo.put("content",msg);
			throw e;
			
		}finally {
			logInfo.put("create_time", new Date());
			alertsPoorEfficiencyDeviceDao.insertDeviceLogData(logInfo);//记录日志
		}
		
	}
	private void getCompareData(Date curDate, String item, List<Map<String, Object>> dataList) {
		Date lastMonth = DateUtils.getTimesLastMonthmorning(curDate);//上月日期
		String lastMonthStr = DateUtils.datetimeToString(DateUtils.DEFAULT_DATETIME_FMT, lastMonth);
		Map<String,Object> param = Maps.newHashMap();
		param.put("item", item);
		param.put("type", 2);
		param.put("date", lastMonthStr);
		List<Map<String,Object>> monthList = alertsPoorEfficiencyDeviceDao.getDeviceDataByDate(param);
		Map<String,Map<String,Object>> monthMap = Maps.newHashMap();
		for(Map<String,Object> m:monthList) {
			monthMap.put(m.get("resourceId").toString(), m);
		}
		
		for(Map<String, Object> d:dataList) {
			if(null!=d.get("avg_value_week")) {
				String resourceId = d.get("resourceId").toString();
				float avgValue = Float.parseFloat(d.get("avg_value_week").toString());
				float avgMonthValue = 0;
				if(monthMap.containsKey(resourceId)) {
					Map<String,Object> m = monthMap.get(resourceId);
					avgMonthValue =  m.get("avg_value_month")==null?0:Float.parseFloat(m.get("avg_value_month").toString());
				}
				d.put("avg_value_month", avgMonthValue);
				d.put("diff_value", df.format(avgValue-avgMonthValue));
			}
		}
		//释放内存
		monthMap.clear();
		monthMap = null;
		monthList.clear();
		monthList = null;
		
	}
	@Override
	public void CabinetColumnTask() throws Exception{
		List<AlertCabinetColumnConfig> list = alertCabinetColumnDao.getConfigList();
		Map<String,AlertCabinetColumnConfig> roomMap = Maps.newHashMap();
		Map<String,AlertCabinetColumnConfig> idcTypeMap = Maps.newHashMap();
		AlertCabinetColumnConfig commonConfig = null;
		for(AlertCabinetColumnConfig c:list) {
			int configType = c.getConfigType();
			String roomId = c.getRoomId();
			String idcType = c.getIdcType();
			if(configType==3) {
				String key = idcType+"_"+roomId;
				roomMap.put(key, c);
			}
			if(configType==2) {
				String key = idcType;
				idcTypeMap.put(key, c);
			}else {
				commonConfig = c;
			}
			
		}
		AlertCabinetColumnConfig config = new AlertCabinetColumnConfig();
		config.setConfigType(1);
		//删除不存在的列头柜
		alertCabinetColumnCommonBiz.delConfigData(config);
		
		int  num = alertCabinetColumnDao.updateConfigDataAll();
		log.info("***update num:{}****",num);
		
		//获取可能原来没有配置的列头柜,再配置保存		
		List<AlertCabinetColumnConfigData> temp = alertCabinetColumnCommonBiz.getConfigData(config);//获取可能原来没有配置的列头柜,再配置保存
		if(null!=temp && temp.size()>0) {
			for(AlertCabinetColumnConfigData a:temp) {
				String roomId = a.getRoomId();
				String idcType = a.getIdcType();
				String roomKey = idcType+"_"+roomId;
				AlertCabinetColumnConfig entity ;
				if(roomMap.containsKey(roomKey)) {
					entity = roomMap.get(roomKey);
					a.setAlertPercentage(entity.getAlertPercentage());
					a.setTimeRange(entity.getTimeRange());
				}else if(idcTypeMap.containsKey(roomKey)) {
					entity = idcTypeMap.get(roomKey);
					a.setAlertPercentage(entity.getAlertPercentage());
					a.setTimeRange(entity.getTimeRange());
				}else {
					a.setAlertPercentage(commonConfig.getAlertPercentage());
					a.setTimeRange(commonConfig.getTimeRange());
				}
			}
			
			alertCabinetColumnDao.batchInsertCabinetColumnData(temp);
		}
		
	}
}
