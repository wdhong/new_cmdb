package com.aspire.mirror.alert.server.biz.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aspire.mirror.alert.server.biz.AlertRepPanelBiz;
import com.aspire.mirror.alert.server.biz.FtpService;
import com.aspire.mirror.alert.server.dao.AlertMonthReportSyncDao;
import com.aspire.mirror.alert.server.dao.AlertRepPanelDao;
import com.aspire.mirror.alert.server.dao.AlertRepPanelItemDao;
import com.aspire.mirror.alert.server.dao.AlertRepPanelMoniterItemDao;
import com.aspire.mirror.alert.server.dao.po.AlertRepPanel;
import com.aspire.mirror.alert.server.dao.po.AlertRepPanelItem;
import com.aspire.mirror.alert.server.dao.po.AlertRepPanelMoniterItem;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class AlertRepPanelBizImpl implements AlertRepPanelBiz{
	private static final Logger LOGGER = LoggerFactory.getLogger(AlertRepPanelBizImpl.class);
	@Autowired
    private AlertRepPanelDao alertRepPanelDao;
	
	@Autowired
    private AlertRepPanelItemDao alertRepPanelItemDao;
	
	@Autowired
    private AlertRepPanelMoniterItemDao alertRepPanelMoniterItemDao;
	
	private static final String OPERATE_EDIT = "edit";
	
	private static final String OPERATE_COPY = "copy";
	
	@Autowired
	private AlertMonthReportSyncDao alertMonthReportSyncDao;

	
	@Value("${AlertMonthReportNewDayTask.ftpFilePath:monthly_operation_report}")
	private  String ftpFilePath ;
	
	@Value("${AlertMonthReportNewDayTask.ftpFilePath:download}")
	private  String downFilePath ;
	
	@Autowired
	private FtpService ftpService;
	
	@Override
	@Transactional(rollbackFor=Exception.class)  
	public AlertRepPanel insert(AlertRepPanel panel) throws Exception{
		try {
			panel.setId(null);
			alertRepPanelDao.insert(panel);
			if (null!=panel.getOperateFlag() && panel.getOperateFlag().equals(OPERATE_COPY)) {
				List<AlertRepPanelItem> items = panel.getItems();
				if (null != items && items.size() > 0) {
					for (AlertRepPanelItem i : items) {
						i.setPanel_id(panel.getId());
						i.setId(null);
					}
					alertRepPanelItemDao.batchInsert(items);

					for (AlertRepPanelItem i : items) {
						List<AlertRepPanelMoniterItem> moniters = i.getMoniterItems();
						for (AlertRepPanelMoniterItem m : moniters) {
							m.setItem_id(i.getId());
							m.setId(null);
						}
						alertRepPanelMoniterItemDao.batchInsert(moniters);
					}
				}
			}
			return panel;
			
		}catch(Exception e) {
			throw new Exception("新增大屏报错",e);
		}
	}
	@Override
	public void update(AlertRepPanel panel) throws Exception{
		try {
			alertRepPanelDao.update(panel);
			if (null ==panel.getOperateFlag() || !panel.getOperateFlag().equals(OPERATE_EDIT)) {
				List<AlertRepPanelItem> items = panel.getItems();
				//先删除 再 新增
				alertRepPanelMoniterItemDao.deleteByPanelId(panel.getId());
				alertRepPanelItemDao.deleteByPanelId(panel.getId());
				
				if(items.size()>0) {
					for(AlertRepPanelItem i:items) {
						i.setPanel_id(panel.getId());
					}
					alertRepPanelItemDao.batchInsert(items);
					
					for(AlertRepPanelItem i:items) {
						List<AlertRepPanelMoniterItem> moniters = i.getMoniterItems();
						for(AlertRepPanelMoniterItem m:moniters) {
							m.setItem_id(i.getId());
						}
						alertRepPanelMoniterItemDao.batchInsert(moniters);
					}
				}
			}
		}catch(Exception e) {
			throw new Exception("修改大屏报错", e);
		}
	}
	@Override
	public void delete(String id) throws Exception{
		try {
			alertRepPanelDao.deleteByPrimaryKey(id);
			alertRepPanelMoniterItemDao.deleteByPanelId(id);
			alertRepPanelItemDao.deleteByPanelId(id);
		}catch(Exception e) {
			throw new Exception("删除大屏报错",e);
		}
	}
	@Override
	public AlertRepPanel getByName(String name) {
		List<AlertRepPanel> ps = alertRepPanelDao.getByName(name);
		if (ps.size()>0) {
			return ps.get(0);
		}else {
			return null;
		}
	}
	@Override
	public List<AlertRepPanel> getAllPanel() {
		return alertRepPanelDao.selectAll();
	}
	@Override
	public AlertRepPanel selectByPrimaryKey(String id) {
		return alertRepPanelDao.selectByPrimaryKey(id);
	}
	@Override
	public void exportBizSystemMonthExcel(String month, String idcType,int hisFlag) throws Exception {

		
		LOGGER.info("***********exportReport--begin**************");
		
		List<String> hList = Lists.newArrayList();
		List<String> kList = Lists.newArrayList();
		hList.add("所属资源池名称");
		hList.add("归属部门（二级）");
		hList.add("所属租户（一级）");
		hList.add("业务系统名称");
		hList.add("联系人");
		hList.add("联系电话");
		kList.add("idcType");
		kList.add("department2");
		kList.add("department1");
		kList.add("bizSystem");
		kList.add("contact");
		kList.add("phone");
		
		/*
		 * Calendar c = Calendar.getInstance();
		 * 
		 * int year = c.get(Calendar.YEAR); int month = c.get(Calendar.MONTH)+1; int
		 * date = c.get(Calendar.DATE); if(date==1) { c.add(Calendar.MONTH, -1); year =
		 * c.get(c.YEAR); month = c.get(c.MONTH) + 1; } String monthStr = month+"";
		 * if(month<10) { monthStr = "0"+month; }
		 */
		List<String> days = alertMonthReportSyncDao.queryDays(month,idcType,hisFlag);
		List<Map<String, Object>> dataList = fromDate(month,idcType,hisFlag);
				
				
		if(days.size()==0) {
			throw new Exception("没有数据");
		}
		Date startTime = DateUtils.parseDate(days.get(0), new String[] { "yyyy-MM-dd" });
		Date endTime = DateUtils.parseDate(days.get(days.size()-1), new String[] { "yyyy-MM-dd" });
		List<String> dateList = Lists.newArrayList();
		
		initList1(hList, kList, startTime, endTime, dateList);
		String[] headerList = hList.toArray(new String[] {});
		String[] keyList = kList.toArray(new String[] {});

		XSSFWorkbook wb = new XSSFWorkbook();
		XSSFSheet sheet = null;
		sheet = wb.createSheet("sheet1");
		String title = month + "月份租户利用率数据表";
		
		if(StringUtils.isNotBlank(idcType)) {
			title = idcType+month + "月份租户利用率数据表";
		}
		String fileName = title + ".xlsx";
		// 计算该报表的列数
		int number = hList.size();
		int num = dateList.size() + 6;

		for (int i = 0; i < number; i++) {
			sheet.setColumnWidth(i, 3000);
		}

		// 创建单元格样式
		XSSFCellStyle cellStyle = wb.createCellStyle();

		// 指定单元格居中对齐
		cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);

		// 指定单元格垂直居中对齐
		cellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

		// 指定当单元格内容显示不下时自动换行
		cellStyle.setWrapText(true);

		//cellStyle.setBorderLeft(CellStyle.BORDER_MEDIUM);
		//cellStyle.setBorderBottom(CellStyle.BORDER_MEDIUM);
		//cellStyle.setBorderRight(CellStyle.BORDER_MEDIUM);
		//cellStyle.setBorderTop(CellStyle.BORDER_MEDIUM);

		// 设置单元格字体
		XSSFFont font = wb.createFont();
		// font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		font.setFontName("仿宋_GB2312");
		font.setFontHeight((short) 240);
		cellStyle.setFont(font);

		XSSFRow row1 = sheet.createRow(0);
		// 设置行高
		row1.setHeight((short) 1500);

		XSSFCell rowCell1 = null;
		// 创建不同的LIST的列标题
		for (int i = 0; i < num; i++) {
			
			if (i > 5) {
				String titleDay = dateList.get(i-6);
				int m = (i - 6) * 8 + 6;
				rowCell1 = row1.createCell(m);
				rowCell1.setCellStyle(cellStyle);
				String name =getDate(titleDay);//month + "月" + (i - 5) + "日";
				rowCell1.setCellValue(name);
				for (int k = 0; k < 7; k++) {

					rowCell1 = row1.createCell(m + 1 + k);
					rowCell1.setCellValue("");
				}
				// i += 7;
				if (i == num - 1) {
					m = (i - 6) * 8 + 6 + 8;
					rowCell1 = row1.createCell(m);
					rowCell1.setCellStyle(cellStyle);
					rowCell1.setCellValue("平均值");
					for (int k = 0; k < 7; k++) {
						rowCell1 = row1.createCell(m + 1 + k);
						rowCell1.setCellValue("");
					}
				}
			} else {
				rowCell1 = row1.createCell(i);
				rowCell1.setCellStyle(cellStyle);
				rowCell1.setCellValue(new XSSFRichTextString(hList.get(i).toString()));
			}

		}

		int dayNum = num - 6;
		// 合并单元格
		// sheet.addMergedRegion(new CellRangeAddress(0, 0, 5, 7));
		for (int i = 0; i < dayNum; i++) {
			int k = i * 8;
			sheet.addMergedRegion(new CellRangeAddress(0, 0, k + 6, k + 13));
			if (i == dayNum - 1) {
				k = i * 8 + 8;
				sheet.addMergedRegion(new CellRangeAddress(0, 0, k + 6, k + 13));
			}
		}

		XSSFCell row2Cell = null;
		XSSFRow row2 = sheet.createRow(1);
		row2.setHeight((short) 800);
		for (int i = 0; i < number; i++) {
			row2Cell = row2.createCell(i);
			row2Cell.setCellStyle(cellStyle);
			if (i > 5) {
				String name = "裸金属";
				row2Cell.setCellValue(name);
				for (int k = 0; k < 3; k++) {
					row2Cell = row2.createCell(i + 1 + k);
					row2Cell.setCellValue("");
				}
				String name1 = "云主机";
				row2Cell = row2.createCell(i + 4);
				row2Cell.setCellStyle(cellStyle);
				row2Cell.setCellValue(name1);
				for (int k = 0; k < 3; k++) {
					row2Cell = row2.createCell(i + 5 + k);
					row2Cell.setCellValue("");
				}
				i += 7;

			} else {
				row2Cell.setCellValue(new XSSFRichTextString(""));
			}

		}
		// 合并单元格
		for (int i = 0; i < dayNum; i++) {
			int k = i * 8;
			sheet.addMergedRegion(new CellRangeAddress(1, 1, k + 6, k + 9));
			sheet.addMergedRegion(new CellRangeAddress(1, 1, k + 10, k + 13));
			if (i == dayNum - 1) {
				k = i * 8 + 8;
				sheet.addMergedRegion(new CellRangeAddress(1, 1, k + 6, k + 9));
				sheet.addMergedRegion(new CellRangeAddress(1, 1, k + 10, k + 13));
			}
		}

		XSSFCell row3Cell = null;
		XSSFRow row3 = sheet.createRow(2);
		row3.setHeight((short) 800);
		for (int i = 0; i < number; i++) {
			row3Cell = row3.createCell(i);
			row3Cell.setCellStyle(cellStyle);
			if (i > 5) {
				String name1 = "cpu日均均值";
				String name2 = "cpu日均峰值";
				String name3 = "内存日均均值";
				String name4 = "内存日均峰值";
				String name5 = "cpu日均均值";
				String name6 = "cpu日均峰值";
				String name7 = "内存日均均值";
				String name8 = "内存日均峰值";

				if (i == number - 8) {
					name1 = "cpu均值";
					name2 = "cpu峰值";
					name3 = "内存均值";
					name4 = "内存峰值";
					name5 = "cpu均值";
					name6 = "cpu峰值";
					name7 = "内存均值";
					name8 = "内存峰值";
				}
				row3Cell.setCellValue(name1);
				row3Cell = row3.createCell(i + 1);
				row3Cell.setCellValue(name2);
				row3Cell = row3.createCell(i + 2);
				row3Cell.setCellValue(name3);
				row3Cell = row3.createCell(i + 3);
				row3Cell.setCellValue(name4);
				row3Cell = row3.createCell(i + 4);
				row3Cell.setCellValue(name5);
				row3Cell = row3.createCell(i + 5);
				row3Cell.setCellValue(name6);
				row3Cell = row3.createCell(i + 6);
				row3Cell.setCellValue(name7);
				row3Cell = row3.createCell(i + 7);
				row3Cell.setCellValue(name8);
				i += 7;

			} else {
				row3Cell.setCellValue(new XSSFRichTextString(""));
			}

		}
		// 合并单元格
		for (int i = 0; i < 6; i++) {
			sheet.addMergedRegion(new CellRangeAddress(0, 2, i, i));

		}

		for (int i = 0; i < dataList.size(); i++) {
			int k = i + 3;
			Map<String, Object> map = dataList.get(i);
			XSSFRow row = sheet.createRow(k);
			row3.setHeight((short) 800);
			XSSFCell rowCell = null;
			String department1 = "";
			String department2 = "";
			String bizSystem = "";

			if (map.get("bizSystem") != null && map.get("bizSystem").toString() != "") {
				bizSystem = map.get("bizSystem").toString();

				/*
				 * Map<String, String> biz =
				 * cmdbAlertRestfulClient.getDepartmentInfoByBizSystem(bizSystem); if (null !=
				 * biz) { department1 = StringUtils.isEmpty(biz.get("department1")) ? "" :
				 * biz.get("department1"); department2 =
				 * StringUtils.isEmpty(biz.get("department2")) ? "" : biz.get("department2"); }
				 */

			}

			for (int j = 0; j < 6; j++) {
				rowCell = row.createCell(j);
				if (j == 0) {
					rowCell.setCellValue(new XSSFRichTextString(map.get("idcType").toString()));
				} else if (j == 1) {
					rowCell.setCellValue(new XSSFRichTextString(department2));
				} else if (j == 2) {
					rowCell.setCellValue(new XSSFRichTextString(department1));
				} else if (j == 3) {
					rowCell.setCellValue(new XSSFRichTextString(bizSystem));
				} else if (j == 4) {
					rowCell.setCellValue(new XSSFRichTextString(""));
				} else if (j == 5) {
					rowCell.setCellValue(new XSSFRichTextString(""));
				}
			}
			for (int j = 0; j < dateList.size(); j++) {
				int m = 8 * j;
				String timeStr = dateList.get(j);
				rowCell = row.createCell(m + 6);
				StringBuffer sb = new StringBuffer();
				sb.append(timeStr).append("_").append("X86服务器").append("_").append("cpu").append("_avg");
				String key = sb.toString();
				rowCell.setCellValue(new XSSFRichTextString(map.get(key) == null ? "" : map.get(key).toString()));

				rowCell = row.createCell(m + 7);
				sb.setLength(0);
				sb.append(timeStr).append("_").append("X86服务器").append("_").append("cpu").append("_max");
				key = sb.toString();
				rowCell.setCellValue(new XSSFRichTextString(map.get(key) == null ? "" : map.get(key).toString()));

				rowCell = row.createCell(m + 8);
				sb.setLength(0);
				sb.append(timeStr).append("_").append("X86服务器").append("_").append("memory").append("_avg");
				key = sb.toString();
				rowCell.setCellValue(new XSSFRichTextString(map.get(key) == null ? "" : map.get(key).toString()));

				rowCell = row.createCell(m + 9);
				sb.setLength(0);
				sb.append(timeStr).append("_").append("X86服务器").append("_").append("memory").append("_max");
				key = sb.toString();
				rowCell.setCellValue(new XSSFRichTextString(map.get(key) == null ? "" : map.get(key).toString()));

				rowCell = row.createCell(m + 10);
				sb.setLength(0);
				sb.append(timeStr).append("_").append("云主机").append("_").append("cpu").append("_avg");
				key = sb.toString();
				rowCell.setCellValue(new XSSFRichTextString(map.get(key) == null ? "" : map.get(key).toString()));

				rowCell = row.createCell(m + 11);
				sb.setLength(0);
				sb.append(timeStr).append("_").append("云主机").append("_").append("cpu").append("_max");
				key = sb.toString();
				rowCell.setCellValue(new XSSFRichTextString(map.get(key) == null ? "" : map.get(key).toString()));

				rowCell = row.createCell(m + 12);
				sb.setLength(0);
				sb.append(timeStr).append("_").append("云主机").append("_").append("memory").append("_avg");
				key = sb.toString();
				rowCell.setCellValue(new XSSFRichTextString(map.get(key) == null ? "" : map.get(key).toString()));

				rowCell = row.createCell(m + 13);
				sb.setLength(0);
				sb.append(timeStr).append("_").append("云主机").append("_").append("memory").append("_max");
				key = sb.toString();
				rowCell.setCellValue(new XSSFRichTextString(map.get(key) == null ? "" : map.get(key).toString()));

				if (j == dateList.size() - 1) {
					m = m + 8;
					rowCell = row.createCell(m + 6);
					sb.setLength(0);
					timeStr = "日均均值";
					sb.append(timeStr).append("_").append("X86服务器").append("_").append("cpu").append("_avg");
					key = sb.toString();
					rowCell.setCellValue(new XSSFRichTextString(map.get(key) == null ? "" : map.get(key).toString()));

					rowCell = row.createCell(m + 7);
					sb.setLength(0);
					sb.append(timeStr).append("_").append("X86服务器").append("_").append("cpu").append("_max");
					key = sb.toString();
					rowCell.setCellValue(new XSSFRichTextString(map.get(key) == null ? "" : map.get(key).toString()));

					rowCell = row.createCell(m + 8);
					sb.setLength(0);
					sb.append(timeStr).append("_").append("X86服务器").append("_").append("memory").append("_avg");
					key = sb.toString();
					rowCell.setCellValue(new XSSFRichTextString(map.get(key) == null ? "" : map.get(key).toString()));

					rowCell = row.createCell(m + 9);
					sb.setLength(0);
					sb.append(timeStr).append("_").append("X86服务器").append("_").append("memory").append("_max");
					key = sb.toString();
					rowCell.setCellValue(new XSSFRichTextString(map.get(key) == null ? "" : map.get(key).toString()));

					rowCell = row.createCell(m + 10);
					sb.setLength(0);
					sb.append(timeStr).append("_").append("云主机").append("_").append("cpu").append("_avg");
					key = sb.toString();
					rowCell.setCellValue(new XSSFRichTextString(map.get(key) == null ? "" : map.get(key).toString()));

					rowCell = row.createCell(m + 11);
					sb.setLength(0);
					sb.append(timeStr).append("_").append("云主机").append("_").append("cpu").append("_max");
					key = sb.toString();
					rowCell.setCellValue(new XSSFRichTextString(map.get(key) == null ? "" : map.get(key).toString()));

					rowCell = row.createCell(m + 12);
					sb.setLength(0);
					sb.append(timeStr).append("_").append("云主机").append("_").append("memory").append("_avg");
					key = sb.toString();
					rowCell.setCellValue(new XSSFRichTextString(map.get(key) == null ? "" : map.get(key).toString()));

					rowCell = row.createCell(m + 13);
					sb.setLength(0);
					sb.append(timeStr).append("_").append("云主机").append("_").append("memory").append("_max");
					key = sb.toString();
					rowCell.setCellValue(new XSSFRichTextString(map.get(key) == null ? "" : map.get(key).toString()));
				}

			}
		}

		
		/*
		 * ExportExcelUtil eeu = new ExportExcelUtil(); Workbook book = new
		 * SXSSFWorkbook(128); eeu.exportExcel(book, 0, fileName, headerList, dataList,
		 * keyList);
		 */
		ByteArrayOutputStream ops = null;
		ByteArrayInputStream in = null;
		try {
			ops = new ByteArrayOutputStream();
			wb.write(ops);
			byte[] b = ops.toByteArray();
			in = new ByteArrayInputStream(b);
			ftpService.uploadtoFTPNew(fileName, in,ftpFilePath);
			ops.flush();
		} catch (Exception e) {
			LOGGER.error("导出excel失败，失败原因：", e);
		} finally {
			//IOUtils.closeQuietly(book);
			IOUtils.closeQuietly(ops);
			IOUtils.closeQuietly(in);
		}
		LOGGER.info("***********exportReport--end**************");
	
		
	}
	
	private String getDate(String titleDay) {
		String[] days = titleDay.split("-");
		int month = Integer.parseInt(days[1]);
		int day = Integer.parseInt(days[2]);
		return month+"月"+day+"日";
	}

	private List<Map<String, Object>> fromDate(String month,String idcType,int hisFlag) {
		List<Map<String, Object>> daysData = alertMonthReportSyncDao.queryDayBizSystem(month,idcType,hisFlag);
		List<Map<String, Object>> monthData = alertMonthReportSyncDao.queryDayBizSystemMonth(month,idcType,hisFlag);
		Map<String, Map<String, Object>> dataMap = Maps.newLinkedHashMap();
		formMapVal(daysData,dataMap,month,false);
		formMapVal(monthData,dataMap,month,true);
		return new ArrayList(dataMap.values());
	}
	

	private void formMapVal(List<Map<String, Object>> daysData
			, Map<String, Map<String, Object>> dataMap,String month,boolean flag) {
		for(Map<String, Object> day:daysData) {
			if(null==day.get("bizSystem")) {
				continue;
			}
			String biz = day.get("bizSystem").toString();
			String idcTypeVal = day.get("idcType").toString();
			String deviceType = day.get("deviceType").toString();
			String cpu_avg = day.get("cpu_avg") == null ? "" : day.get("cpu_avg").toString();
			String cpu_max = day.get("cpu_max") == null ? "" : day.get("cpu_max").toString();
			String memory_avg = day.get("memory_avg") == null ? "" : day.get("memory_avg").toString();
			String memory_max = day.get("memory_max") == null ? "" : day.get("memory_max").toString();
			String dayVal = day.get("day") == null ? "" : day.get("day").toString();
			Map<String, Object> val = new HashMap<String, Object>();
			String key = biz + "_"+idcTypeVal;
			if(dataMap.containsKey(key)) {
				val = dataMap.get(key);
			}else {
				val.put("idcType", idcTypeVal);
				val.put("bizSystem", biz);
				dataMap.put(key, val);
				val.put("month", month);
			}
			val.put("deviceType", deviceType);
			StringBuffer sb = new StringBuffer();
			if(flag) {
				sb.append("日均均值").append("_").append(deviceType).append("_");
			}else {
				sb.append(dayVal).append("_").append(deviceType).append("_");
			}
			val.put(sb.toString()+"cpu_avg", cpu_avg);
			val.put(sb.toString()+"cpu_max", cpu_max);
			val.put(sb.toString()+"memory_avg", memory_avg);
			val.put(sb.toString()+"memory_max", memory_max);
		}
		
	}

	private void initList1(List<String> hList, List<String> kList, Date startTime, Date endTime,
			List<String> dateList) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startTime);

		DateFormat returndf = new SimpleDateFormat("yyyy-MM-dd");

		while (calendar.getTime().getTime() <= endTime.getTime()) {
			String timeStr = returndf.format(calendar.getTime());
			dateList.add(timeStr);
			StringBuffer sb = new StringBuffer();
			sb.append(timeStr).append("_").append("X86服务器").append("_").append("cpu");
			hList.add(sb.toString() + "_日均均值");
			hList.add(sb.toString() + "_日均峰值");
			kList.add(sb.toString() + "_avg");
			kList.add(sb.toString() + "_max");

			sb.setLength(0);
			sb.append(timeStr).append("_").append("X86服务器").append("_").append("内存");
			hList.add(sb.toString() + "_日均均值");
			hList.add(sb.toString() + "_日均峰值");
			kList.add(sb.toString() + "_avg");
			kList.add(sb.toString() + "_max");

			sb.setLength(0);
			sb.append(timeStr).append("_").append("云主机").append("_").append("cpu");
			hList.add(sb.toString() + "_日均均值");
			hList.add(sb.toString() + "_日均峰值");
			kList.add(sb.toString() + "_avg");
			kList.add(sb.toString() + "_max");

			sb.setLength(0);
			sb.append(timeStr).append("_").append("云主机").append("_").append("内存");
			hList.add(sb.toString() + "_日均均值");
			hList.add(sb.toString() + "_日均峰值");
			kList.add(sb.toString() + "_avg");
			kList.add(sb.toString() + "_max");

			calendar.add(Calendar.DATE, +1);

		}
		StringBuffer sb = new StringBuffer();
		sb.append("日均均值").append("_").append("X86服务器").append("_").append("cpu");
		hList.add(sb.toString() + "_月均值");
		hList.add(sb.toString() + "_月峰值");
		kList.add(sb.toString() + "_avg");
		kList.add(sb.toString() + "_max");

		sb.setLength(0);
		sb.append("日均均值").append("_").append("X86服务器").append("_").append("内存");
		hList.add(sb.toString() + "_月均值");
		hList.add(sb.toString() + "_月峰值");
		kList.add(sb.toString() + "_avg");
		kList.add(sb.toString() + "_max");

		sb.setLength(0);
		sb.append("日均均值").append("_").append("云主机").append("_").append("cpu");
		hList.add(sb.toString() + "_月均值");
		hList.add(sb.toString() + "_月峰值");
		kList.add(sb.toString() + "_avg");
		kList.add(sb.toString() + "_max");

		sb.setLength(0);
		sb.append("日均均值").append("_").append("云主机").append("_").append("内存");
		hList.add(sb.toString() + "_月均值");
		hList.add(sb.toString() + "_月峰值");
		kList.add(sb.toString() + "_avg");
		kList.add(sb.toString() + "_max");
	}
	

}
