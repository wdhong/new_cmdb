package com.aspire.mirror.alert.server.biz.kafka.monitor;

import com.alibaba.fastjson.JSON;
import com.aspire.mirror.alert.server.clientservice.elasticsearch.MonitorKpiServiceClient;
import com.aspire.mirror.alert.server.dao.KpiCommonMapper;
import com.aspire.mirror.alert.server.domain.MonitorKpiDto;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * @BelongsProject: mirror-alert
 * @BelongsPackage: com.aspire.mirror.alert.server.biz.kafka.monitor
 * @Author: baiwenping
 * @CreateTime: 2020-04-22 17:21
 * @Description: ${Description}
 */
@Slf4j
@Component
@ConditionalOnExpression("${middleware.configuration.switch.kafka:true}")
public class MonitorKpiConsumer {

    private static final String DB = "db";
    private static final String ES = "es";

    @Autowired
    private MonitorKpiServiceClient monitorKpiServiceClient;
    @Autowired
    private KpiCommonMapper kpiCommonMapper;

    @KafkaListener(topics = "${kafka.topic.topic_monitor_kpi:TOPIC_MONITOR_KPI}")
    public void listen(ConsumerRecord<?, String> cr) throws Exception {
        Long time1 = System.currentTimeMillis();
        String value = cr.value();
        MonitorKpiDto monitorKpiDto = JSON.parseObject(value, MonitorKpiDto.class);
        String dataType = monitorKpiDto.getDataType();
        String dataTable = monitorKpiDto.getDataTable();
        if (DB.equalsIgnoreCase(dataType)) {
            kpiCommonMapper.insert(dataTable, monitorKpiDto.getFieldList(), monitorKpiDto.getDataList());
        } else if (ES.equalsIgnoreCase(dataType)) {
            monitorKpiServiceClient.insertBatch(dataTable, "doc", monitorKpiDto.getDataList());
        }

        log.info("consumer monitor kpi size are : {}; use {} ms", monitorKpiDto.getDataList().size(), (System.currentTimeMillis() - time1));
    }
}
