package com.aspire.mirror.alert.server.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.time.DateUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.aspire.mirror.alert.server.biz.AlertsBiz;
import com.aspire.mirror.alert.server.biz.AlertsHisBiz;
import com.aspire.mirror.alert.server.biz.FtpService;
import com.aspire.mirror.alert.server.clientservice.CmdbInstanceClient;
import com.aspire.mirror.alert.server.clientservice.CmdbResfulClient;
import com.aspire.mirror.alert.server.clientservice.EsCloudSysClient;
import com.aspire.mirror.alert.server.clientservice.ZabbixRatioServiceClient;
import com.aspire.mirror.alert.server.dao.AlertRestfulDao;
import com.aspire.mirror.alert.server.dao.Alerts3050Dao;
import com.aspire.mirror.alert.server.dao.po.AlertStandardizationDTO;
import com.aspire.mirror.alert.server.dao.po.Alerts;
import com.aspire.mirror.alert.server.dao.po.AlertsHis;
import com.aspire.mirror.alert.server.domain.AlertsDTO;
import com.aspire.mirror.alert.server.domain.AlertsHisDTO;
import com.aspire.mirror.alert.server.task.CloudSysSyncKafkaTask;
import com.aspire.mirror.alert.server.util.ExportExcelUtil;
import com.aspire.mirror.alert.server.util.StringUtils;
import com.aspire.mirror.elasticsearch.api.dto.AlertEsHistoryReq;
import com.aspire.mirror.elasticsearch.api.dto.NetPerformanceAnalysis;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

@RestController
public class AlertController {

    @Autowired
    private AlertsBiz alertsBiz;
    @Autowired
    private AlertsHisBiz alertsHisBiz;

    private static final Logger LOGGER = LoggerFactory.getLogger(AlertController.class);
    


    @PutMapping(value = "/v1/alert/refresh")
    public void refreshAllAlerts(@RequestParam(value = "device_ip", required = false) String deviceIp,
                                 @RequestParam(value = "idc_type", required = false) String idcType,
                                 @RequestParam(value = "device_class", required = false) String deviceClass) {
        Alerts alerts = new Alerts();
        if (!StringUtils.isEmpty(deviceIp)) {
            alerts.setDeviceIp(deviceIp);
        }
        if (!StringUtils.isEmpty(idcType)) {
            alerts.setIdcType(idcType);
        }
        if (!StringUtils.isEmpty(deviceClass)) {
            alerts.setDeviceClass(deviceClass);
        }
        List<AlertsDTO> alertsDTOList = alertsBiz.select(alerts);
        alertsDTOList.stream().forEach((AlertsDTO alertsDTO) -> {
            if (AlertsDTO.OBJECT_TYPE_BIZ.equals(alertsDTO.getObjectType())) {
                return;
            }
//            CmdbInstance cmdbInstance = cmdbHelper.queryDeviceByRoomIdAndIP(alertsDTO.getIdcType(), alertsDTO.getDeviceIp());
//            if (cmdbInstance != null) {
//                boolean flag = false;
//                if (StringUtils.isNotEmpty(cmdbInstance.getId()) &&
//                        !cmdbInstance.getId().equals(alertsDTO.getDeviceId())) {
//                    flag = true;
//                    alertsDTO.setDeviceId(cmdbInstance.getId());
//                    alertsDTO.setObjectId(cmdbInstance.getId());
//                }
//                if (StringUtils.isNotEmpty(cmdbInstance.getRoomId()) &&
//                        !cmdbInstance.getRoomId().equals(alertsDTO.getSourceRoom())) {
//                    flag = true;
//                    alertsDTO.setSourceRoom(cmdbInstance.getRoomId());
//                }
//                if (StringUtils.isNotEmpty(cmdbInstance.getBizSystem()) &&
//                        !cmdbInstance.getBizSystem().equals(alertsDTO.getBizSys())) {
//                    flag = true;
//                    alertsDTO.setBizSys(cmdbInstance.getBizSystem());
//                    alertsDTO.setBizSysName(cmdbInstance.getBizSystem());
//                }
//                if (StringUtils.isNotEmpty(cmdbInstance.getIdcType()) &&
//                        !cmdbInstance.getIdcType().equals(alertsDTO.getIdcType())) {
//                    flag = true;
//                    alertsDTO.setIdcType(cmdbInstance.getIdcType());
//                }
//                if (StringUtils.isNotEmpty(cmdbInstance.getDeviceClass()) &&
//                        !cmdbInstance.getDeviceClass().equals(alertsDTO.getDeviceClass())) {
//                    flag = true;
//                    alertsDTO.setDeviceClass(cmdbInstance.getDeviceClass());
//                }
//                if (StringUtils.isNotEmpty(cmdbInstance.getDeviceType()) &&
//                        !cmdbInstance.getDeviceType().equals(alertsDTO.getDeviceType())) {
//                    flag = true;
//                    alertsDTO.setDeviceType(cmdbInstance.getDeviceType());
//                }
//                if (StringUtils.isNotEmpty(cmdbInstance.getDeviceMfrs()) &&
//                        !cmdbInstance.getDeviceMfrs().equals(alertsDTO.getDeviceMfrs())) {
//                    flag = true;
//                    alertsDTO.setDeviceMfrs(cmdbInstance.getDeviceMfrs());
//                }
//                if (StringUtils.isNotEmpty(cmdbInstance.getDeviceModel()) &&
//                        !cmdbInstance.getDeviceModel().equals(alertsDTO.getDeviceModel())) {
//                    flag = true;
//                    alertsDTO.setDeviceModel(cmdbInstance.getDeviceModel());
//                }
//                if (StringUtils.isNotEmpty(cmdbInstance.getPodName()) &&
//                        !cmdbInstance.getPodName().equals(alertsDTO.getPodName())) {
//                    flag = true;
//                    alertsDTO.setPodName(cmdbInstance.getPodName());
//                }
//                if (StringUtils.isNotEmpty(cmdbInstance.getHostName()) &&
//                        !cmdbInstance.getHostName().equals(alertsDTO.getHostName())) {
//                    flag = true;
//                    alertsDTO.setHostName(cmdbInstance.getHostName());
//                }
//                if (StringUtils.isNotEmpty(cmdbInstance.getProjectName()) &&
//                        !cmdbInstance.getProjectName().equals(alertsDTO.getProjectName())) {
//                    flag = true;
//                    alertsDTO.setProjectName(cmdbInstance.getProjectName());
//                }
//                if (flag) {
//                    alertsBiz.updateByPrimaryKey(alertsDTO);
//                }
//
//            }
        });

    }

    @PutMapping(value = "/v1/alertHis/refresh")
    public void refreshAllAlertsHis (@RequestParam(value = "device_ip", required = false) String deviceIp,
                                     @RequestParam(value = "idc_type", required = false) String idcType,
                                     @RequestParam(value = "device_class", required = false) String deviceClass) {
        AlertsHis alertsHisQuery = new AlertsHis();
        if (!StringUtils.isEmpty(deviceIp)) {
            alertsHisQuery.setDeviceIp(deviceIp);
        }
        if (!StringUtils.isEmpty(idcType)) {
            alertsHisQuery.setIdcType(idcType);
        }
        if (!StringUtils.isEmpty(deviceClass)) {
            alertsHisQuery.setDeviceClass(deviceClass);
        }
        List<AlertsHisDTO> alertsHisDTOList = alertsHisBiz.select(alertsHisQuery);
        alertsHisDTOList.stream().forEach((AlertsHisDTO alertsHisDTO) -> {
            if (AlertsDTO.OBJECT_TYPE_BIZ.equals(alertsHisDTO.getObjectType())) {
                return;
            }
//            CmdbInstance cmdbInstance = cmdbHelper.queryDeviceByRoomIdAndIP(alertsHisDTO.getIdcType(), alertsHisDTO.getDeviceIp());
//            if (cmdbInstance != null) {
//                boolean flag = false;
//                if (StringUtils.isNotEmpty(cmdbInstance.getId()) &&
//                        !cmdbInstance.getId().equals(alertsHisDTO.getDeviceId())) {
//                    flag = true;
//                    alertsHisDTO.setDeviceId(cmdbInstance.getId());
//                    alertsHisDTO.setObjectId(cmdbInstance.getId());
//                }
//                if (StringUtils.isNotEmpty(cmdbInstance.getRoomId()) &&
//                        !cmdbInstance.getRoomId().equals(alertsHisDTO.getSourceRoom())) {
//                    flag = true;
//                    alertsHisDTO.setSourceRoom(cmdbInstance.getRoomId());
//                }
//                if (StringUtils.isNotEmpty(cmdbInstance.getBizSystem()) &&
//                        !cmdbInstance.getBizSystem().equals(alertsHisDTO.getBizSys())) {
//                    flag = true;
//                    alertsHisDTO.setBizSys(cmdbInstance.getBizSystem());
//                    alertsHisDTO.setBizSysName(cmdbInstance.getBizSystem());
//                }
//                if (StringUtils.isNotEmpty(cmdbInstance.getIdcType()) &&
//                        !cmdbInstance.getIdcType().equals(alertsHisDTO.getIdcType())) {
//                    flag = true;
//                    alertsHisDTO.setIdcType(cmdbInstance.getIdcType());
//                }
//                if (StringUtils.isNotEmpty(cmdbInstance.getDeviceClass()) &&
//                        !cmdbInstance.getDeviceClass().equals(alertsHisDTO.getDeviceClass())) {
//                    flag = true;
//                    alertsHisDTO.setDeviceClass(cmdbInstance.getDeviceClass());
//                }
//                if (StringUtils.isNotEmpty(cmdbInstance.getDeviceType()) &&
//                        !cmdbInstance.getDeviceType().equals(alertsHisDTO.getDeviceType())) {
//                    flag = true;
//                    alertsHisDTO.setDeviceType(cmdbInstance.getDeviceType());
//                }
//                if (StringUtils.isNotEmpty(cmdbInstance.getPodName()) &&
//                        !cmdbInstance.getPodName().equals(alertsHisDTO.getPodName())) {
//                    flag = true;
//                    alertsHisDTO.setPodName(cmdbInstance.getPodName());
//                }
//                if (StringUtils.isNotEmpty(cmdbInstance.getDeviceMfrs()) &&
//                        !cmdbInstance.getDeviceMfrs().equals(alertsHisDTO.getDeviceMfrs())) {
//                    flag = true;
//                    alertsHisDTO.setDeviceMfrs(cmdbInstance.getDeviceMfrs());
//                }
//                if (StringUtils.isNotEmpty(cmdbInstance.getDeviceModel()) &&
//                        !cmdbInstance.getDeviceModel().equals(alertsHisDTO.getDeviceModel())) {
//                    flag = true;
//                    alertsHisDTO.setDeviceModel(cmdbInstance.getDeviceModel());
//                }
//                if (StringUtils.isNotEmpty(cmdbInstance.getHostName()) &&
//                        !cmdbInstance.getHostName().equals(alertsHisDTO.getHostName())) {
//                    flag = true;
//                    alertsHisDTO.setHostName(cmdbInstance.getHostName());
//                }
//                if (StringUtils.isNotEmpty(cmdbInstance.getProjectName()) &&
//                        !cmdbInstance.getProjectName().equals(alertsHisDTO.getProjectName())) {
//                    flag = true;
//                    alertsHisDTO.setProjectName(cmdbInstance.getProjectName());
//                }
//                if (flag) {
//                    alertsHisBiz.updateByPrimaryKey(alertsHisDTO);
//                }
//
//            }
        });

    }
    
    @Autowired
	private CmdbInstanceClient instanceClient;

	@Autowired
	private ZabbixRatioServiceClient zabbixRatioServiceClient;

	@Autowired
	private FtpService ftpService;
	
    @Value("${MoniterServerRatioExportTask.ftpPath}")
	private String ftpPath;

	/**
	 * 告警自动化派单定时任务
	 * 
	 * @throws Exception
	 */
	 @GetMapping(value = "/v1/alert/syncAlertData")
	public void syncAlertData() throws Exception {
		 LOGGER.info("synchronization syncAlertData begin*****************************");
		List<Map<String, Object>> result = Lists.newArrayList();
		Map<String, Object> queryInstance = Maps.newHashMap();
		String deviceType = "云主机";
		String idcType = "信息港资源池";
		int pageNo = 1;
		int pageSize = 100;
		queryInstance.put("device_type", deviceType);
		queryInstance.put("idcType", idcType);
		queryInstance.put("currentPage", pageNo);
		queryInstance.put("currentPage", pageNo);
		queryInstance.put("pageSize", pageSize);
		//ueryInstance.put("pageSize", pageSize);
		//queryInstance.setQueryType("normal");
		// Result<Map<String, Object>> rs =

		getDataList(queryInstance, result);
		
		queryInstance.put("device_type", "X86服务器");
		getDataList(queryInstance, result);
		queryInstance.put("device_type", "云主机");
		queryInstance.put("idcType", "南方基地");
		getDataList(queryInstance, result);
		queryInstance.put("device_type", "X86服务器");
		getDataList(queryInstance, result);

//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		String fileName = "dailyReportOfServerUtilization"+date.getTime() + ".xlsx";
		String[] header = { "UUID", "管理IP", "业务IP", "类型", "CPU峰值（单位%）", "CPU均值（单位%）", "内存峰值（单位%）", "内存均值（单位%）", "资源池",
				"POD", "一级部门名称", "二级部门名称", "业务系统名称", "加电状态" };
		String[] keys = { "id", "ip", "ip", "device_type", "cpu_max", "cpu_avg", "mem_max", "mem_avg", "idcType",
				"pod_name", "department1", "department2", "bizSystem", "device_status" };

		ExportExcelUtil eeu = new ExportExcelUtil();
		Workbook book = new SXSSFWorkbook(128);
		eeu.exportExcel(book, 0, fileName, header, result, keys);
		ByteArrayOutputStream ops = null;
		ByteArrayInputStream in = null;
		try {
			ops = new ByteArrayOutputStream();
			book.write(ops);
			byte[] b = ops.toByteArray();
			in = new ByteArrayInputStream(b);
			LOGGER.info("syncAlertData导出excel开始*******************************");
			ftpService.uploadtoFTPNew(fileName, in,null);
			LOGGER.info("syncAlertData导出excel结束*******************************");
			ops.flush();
		} catch (Exception e) {
//			logger.error("导出excel失败，失败原因：", e);
            throw new RuntimeException("导出excel失败，失败原因：", e);
		} finally {
			IOUtils.closeQuietly(book);
			IOUtils.closeQuietly(ops);
			IOUtils.closeQuietly(in);
		}

		LOGGER.info("synchronization syncAlertData end*****************************");

	}

	public void getDataList(Map<String, Object> queryInstance, List<Map<String, Object>> result) {
		int total = instanceClient.getInstanceListV3(queryInstance,null).getTotalSize();
		int pageSize = Integer.parseInt(queryInstance.get("pageSize").toString());
		int gageAll = total / pageSize;
		int mode = total % pageSize;
		if (mode > 0) {
			gageAll++;
		}
		if (total > 0) {
			for (int i = 1; i <= gageAll;i++) {
				//queryInstance.setPageNumber(i);
				queryInstance.put("currentPage", i);
				List<Map<String, Object>> rsList = instanceClient.getInstanceListV3(queryInstance,null).getData();
				List<String> ipList = Lists.newArrayList();
				for (Map<String, Object> map : rsList) {
					if (null != map.get("device_status")) {
						if (map.get("device_status").equals("运行正常")) {
							map.put("device_status", "是");
						} else {
							map.put("device_status", "否");
						}
					} else {
						map.put("device_status", "否");
					}
					if (null != map.get("ip")) {
						ipList.add(map.get("ip").toString());
					}
				}
				String deviceType = queryInstance.get("device_type").toString();
				String idcType = queryInstance.get("idcType").toString();
						
				Map<String, NetPerformanceAnalysis> dataMap = zabbixRatioServiceClient
						.getServerRatioData(deviceType,idcType, ipList);
				for (Map<String, Object> map : rsList) {
					String ip = map.get("ip").toString();
					if (null != ip) {
						NetPerformanceAnalysis val = dataMap.get(ip);
						if (null != val) {
							map.put("cpu_max", val.getCpuMax());
							map.put("cpu_avg", val.getCpuAvg());
							map.put("mem_max", val.getMemMax());
							map.put("mem_avg", val.getMemAvg());
						}
					}
				}
				result.addAll(rsList);
			}
		}
	}

	
	
	
	
	
	
	@Autowired
	private AlertRestfulDao alertRestfulDao;
	@Autowired
	private Alerts3050Dao alerts3050Dao;
	
	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;

	
	
	@Autowired
	private EsCloudSysClient esCloudSysClient;
	
	@PostMapping(value = "/v1/alert/getPhyData")
	public void getPhyData() throws Exception {
		AlertEsHistoryReq es = new AlertEsHistoryReq();
		es.setSuyanResid("331e300e-a39d-492a-b883-a200d39dd3ab");
		es.setItemListStr("{\"col_time\":\"2020-04-10 14:30:41\",\"cpu_avg_util_percent_percent_avg\":22.24,"
				+ "\"create_time\":\"2020-04-10 14:30:41\",\"mem_mem_total\":503.39,\"mem_mem_used\":495.87,"
				+ "\"mem_percent_usedwobufferscaches\":97.07,\"res_id\":\"0fa7ab1cf418490f84fee42004353b5b\","
				+ "\"res_type\":\"服务器\"}");
		esCloudSysClient.insertCloudPhy(es);
		//formEsData(es,"");
		
		/*
		 * Date now = new Date(); Calendar calendar = Calendar.getInstance();
		 * calendar.add(Calendar.MINUTE, -10); Date startTime = calendar.getTime();
		 * SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss"); String
		 * startStr = sdf.format(startTime); String endStr = sdf.format(now); String
		 * phyUrl = this.PHY_URL; //phyUrl =
		 * "http://localhost:28177/v1/alerts/deriveAlert/v1/alert/getPhyData"; String
		 * url = phyUrl + "?" + "begintime=" + startStr + "&endtime=" + endStr;
		 * 
		 * HttpGet get = new HttpGet(url);
		 * 
		 * CloseableHttpClient client = HttpClients.createDefault();
		 * CloseableHttpResponse response = null; StringBuilder res = new
		 * StringBuilder(); try { response = client.execute(get); RequestConfig config =
		 * RequestConfig.custom().setConnectTimeout(5000).build();
		 * get.setConfig(config); HttpEntity entity = response.getEntity(); InputStream
		 * is = null; if (entity != null) { is = entity.getContent(); // 转换为字节输入流
		 * BufferedReader br = new BufferedReader(new InputStreamReader(is,
		 * Consts.UTF_8)); String body = null; while ((body = br.readLine()) != null) {
		 * res.append(body); } } JSONObject js = JSON.parseObject(res.toString()); if
		 * (js.getString("returncode").equals("000")) { JSONArray arr =
		 * js.getJSONArray("rcd"); if (null != arr && arr.size() > 0) { long size =
		 * arr.size(); int k = 0; while (k < size) { JSONArray tem = new JSONArray();
		 * for (int i = 0; i < this.num; i++) { if ((k + i) >= size) { break; }
		 * tem.add(arr.get(i + k)); } kafkaTemplate.send(this.phy_kafkaTopic,
		 * tem.toJSONString()); k += this.num; tem.clear(); } arr.clear(); } } else {
		 * LOGGER.error("全网监控中心系统-云资源数据调用服务器接口失败"); throw new
		 * Exception("全网监控中心系统-云资源数据调用服务器接口失败"); } } catch (Exception e) {
		 * e.printStackTrace(); throw new Exception("全网监控中心系统-云资源数据调用服务器接口失败",e); }
		 */

	}
	@Autowired
	private CloudSysSyncKafkaTask cloudSysSyncKafkaTask;
	@Autowired
	private ThreadPoolTaskExecutor taskExecutor;
	
	@PostMapping(value = "/v1/alert/syncSuyanData")
	public void syncSuyanData(@RequestParam(value = "startTimeConfig", required = false) String startTimeConfig,
			@RequestParam(value = "endTimeConfig", required = false) String endTimeConfig,
			@RequestParam(value = "deviceType", required = false) String deviceType) throws Exception {
	
			Runnable runnable = new Runnable() {
				public void run() {
					 try {
						 getSuyanData(startTimeConfig,endTimeConfig,deviceType);
				        } catch (Exception e) {
				            e.printStackTrace();
				        }
				}
			};
			taskExecutor.execute(runnable);

	}
	public void getSuyanData(String startTimeConfig,String endTimeConfig,String deviceType) throws Exception {
		LOGGER.info("**getSuyanData-begin*********");
		Date start = DateUtils.parseDate(startTimeConfig, new String[] { "yyyyMMddHHmmss" });
		Date end = DateUtils.parseDate(endTimeConfig, new String[] { "yyyyMMddHHmmss" });
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		 Calendar calendar = Calendar.getInstance();
		 long period = 10*60*1000;
		  for(long i=start.getTime();i<end.getTime();i+=period) {
	        	Date start1 = new Date(i);
	        	String timeStr = sdf.format(start1);
	        	
	        	calendar.setTime(start1);
	        	calendar.add(Calendar.MINUTE, 10);
	        	Date end1 = calendar.getTime();
	        	cloudSysSyncKafkaTask.getData(timeStr, sdf.format(end1), deviceType, "re2"+deviceType, new Date());
	        }
		LOGGER.info("**getSuyanData-end*********");
	}
	
	
    
}
