package com.aspire.mirror.alert.server.dao.po.transform;

import lombok.Data;

@Data
public class AlertNotifyConfigLogDetail {

    private String alertNotifyConfigId;
    private String voiceAlertId;
    private String operator;
    private String operationTime;

}
