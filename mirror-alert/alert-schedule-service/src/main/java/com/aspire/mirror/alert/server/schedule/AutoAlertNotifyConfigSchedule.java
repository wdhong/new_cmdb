package com.aspire.mirror.alert.server.schedule;

import com.aspire.mirror.alert.server.biz.AlertNotifyConfigBiz;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;

//@Component
//@EnableScheduling
@Slf4j
public class AutoAlertNotifyConfigSchedule implements SchedulingConfigurer {

    @Autowired
    private AlertNotifyConfigBiz alertNotifyConfigBiz;

    private ScheduledTaskRegistrar taskRegistrar;

    private Map<String, ScheduledFuture<?>> taskFutures = new ConcurrentHashMap<String, ScheduledFuture<?>>();

    @Override
    public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
        initSchedule(scheduledTaskRegistrar);
    }

    /**
     *初始化定时任务
     */
    private void initSchedule(ScheduledTaskRegistrar scheduledTaskRegistrar) {
//        ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();
//        threadPoolTaskScheduler.setPoolSize(10);
//        threadPoolTaskScheduler.initialize();
//        scheduledTaskRegistrar.setTaskScheduler(threadPoolTaskScheduler);
//        TaskScheduler scheduler = scheduledTaskRegistrar.getScheduler();
//        Map<String,String> alertNotifyConfigRule = alertNotifyConfigBiz.getAlertNotifyConfigRule();
//        Set<String> strings = alertNotifyConfigRule.keySet();
//        for (String str: strings) {
//            String cron = alertNotifyConfigRule.get(str);
//            taskFutures.put(str, scheduler.schedule(() -> {
//                if ("send".equals(str)) {
//                    log.info("------告警通知发送定时任务开启------");
//                    log.info("StartTime Of Sending Alert Notify Config is {}", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(  )));
//                    alertNotifyConfigBiz.sendAlertNotifyConfig();
//                    log.info("------告警通知发送定时任务关闭------");
//                    log.info("EndTime Of Sending Alert Notify Config is {}", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(  )));
//                } else if ("resend".equals(str)) {
//                    log.info("------重发告警通知发送定时任务开启------");
//                    log.info("StartTime Of Resending Alert Notify Config is {}", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
//                    alertNotifyConfigBiz.reSendAlertNotifyConfig();
//                    log.info("------重发告警通知发送定时任务关闭------");
//                    log.info("StartTime Of Resending Alert Notify Config is {}", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
//                }
//            }, new CronTrigger(cron)));
//            this.taskRegistrar = scheduledTaskRegistrar;
//        }
    }

    /**
     * 添加定时任务
     */
    public void addTriggerTask(Map<String,String> ruleMap) {
//        Set<String> tasks = ruleMap.keySet();
//        for (String task : tasks) {
//            TaskScheduler scheduler = taskRegistrar.getScheduler();
//            String cron = ruleMap.get(task);
//            TriggerTask triggerTask = new TriggerTask(() -> {
//                if ("send".equals(task)) {
//                    log.info("------告警通知发送定时任务开启------");
//                    log.info("StartTime Of Sending Alert Notify Config is {}", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(  )));
//                    alertNotifyConfigBiz.sendAlertNotifyConfig();
//                    log.info("------告警通知发送定时任务关闭------");
//                    log.info("EndTime Of Sending Alert Notify Config is {}", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(  )));
//                } else if ("resend".equals(task)) {
//                    log.info("------重发告警通知发送定时任务开启------");
//                    log.info("StartTime Of Resending Alert Notify Config is {}", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
//                    alertNotifyConfigBiz.reSendAlertNotifyConfig();
//                    log.info("------重发告警通知发送定时任务关闭------");
//                    log.info("EndTime Of Resending Alert Notify Config is {}", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
//                }
//            }, new CronTrigger(cron));
//            ScheduledFuture<?> future = scheduler.schedule(triggerTask.getRunnable(), triggerTask.getTrigger());
//            taskFutures.put(task, future);
//        }
    }

    /**
     * 取消任务
     */
    public void cancelTriggerTask(Map<String,String> ruleMap) {
        Set<String> tasks = ruleMap.keySet();
        for (String task : tasks) {
            ScheduledFuture<?> future = taskFutures.get(task);
            if (future != null) {
                future.cancel(true);
            }
            taskFutures.remove(task);
        }
    }

    /**
     * 重置任务
     */
    public void resetTriggerTask(Map<String,String> ruleMap) {
        cancelTriggerTask(ruleMap);
        addTriggerTask(ruleMap);
    }

}
