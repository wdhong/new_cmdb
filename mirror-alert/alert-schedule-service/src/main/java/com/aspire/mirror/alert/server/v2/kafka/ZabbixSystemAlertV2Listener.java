package com.aspire.mirror.alert.server.v2.kafka;

import com.alibaba.fastjson.JSONObject;
import com.aspire.mirror.alert.server.biz.helper.CmdbHelper;
import com.aspire.mirror.alert.server.dao.DeviceItemInfoDao;
import com.aspire.mirror.alert.server.util.JsonUtil;
import com.aspire.mirror.alert.server.util.StringUtils;
import com.aspire.mirror.alert.server.v2.biz.AlertFieldBiz;
import com.aspire.mirror.alert.server.v2.biz.AlertsHandleV2Helper;
import com.aspire.mirror.alert.server.v2.constant.AlertConfigConstants;
import com.aspire.mirror.alert.server.v2.dao.po.AlertFieldRequestDTO;
import com.aspire.mirror.alert.server.v2.domain.AlertsDTOV2;
import com.aspire.mirror.alert.server.v2.domain.ZabbixAlertV2;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;

/**
 * @BelongsProject: mirror-alert
 * @BelongsPackage: com.aspire.mirror.alert.server.v2.kafka
 * @Author: baiwenping
 * @CreateTime: 2020-02-20 16:51
 * @Description: ${Description}
 */
@Slf4j
@Component
@ConditionalOnExpression("${middleware.configuration.switch.kafka:true}")
public class ZabbixSystemAlertV2Listener {
    @Autowired
    private AlertsHandleV2Helper alertHandleHelper;
    @Autowired
    private CmdbHelper cmdbHelper;
    @Autowired
    private AlertFieldBiz alertFieldBiz;
    @Autowired
    private DeviceItemInfoDao deviceItemInfoDao;

    /**
    * 监听并处理系统告警. <br/>
    * @auther baiwenping
    * @Description
    * @Date 16:51 2020/2/20
    * @Param [cr]
    * @return void
    **/
    @KafkaListener(topics = "${kafka.topic.topic_system_alerts:TOPIC_SYSTEM_ALERTS}")
    public void listen(ConsumerRecord<?, String> cr) throws Exception {
        Long time1 = System.currentTimeMillis();
//        List<AlertFieldRequestDTO> alertFieldList = alertHandleHelper.getModelField(AlertConfigConstants.REDIS_MODEL_ALERT);
        ZabbixAlertV2 zbxAlert = JsonUtil.jacksonConvert(cr.value(), ZabbixAlertV2.class);
//		log.info("Received system alert message from kafka topic, alert id is {}, topic partition is {},offset is {}.", zbxAlert.getAlertId(), cr.partition(),cr.offset());
        AlertsDTOV2 alert = zbxAlert.parse(cmdbHelper);

        String alertType = "recover";
        //插入监控项和监控标准化描述
        if (AlertsDTOV2.ALERT_ACTIVE.equals(alert.getAlertType())) {
//            if (org.apache.commons.lang.StringUtils.isNotEmpty(alert.getItemKey()) && StringUtils.isEmpty(alert.getKeyComment())) {
//                List<DeviceItemInfo> monitorList = deviceItemInfoDao.getMonitorByKey(alert.getItemKey());
//                if (CollectionUtils.isNotEmpty(monitorList)) {
//                    alert.setKeyComment(monitorList.get(0).getComment());
//                }
//            }
            alertType = "create";
        }

        if (alert.getObjectType().equals(AlertsDTOV2.OBJECT_TYPE_BIZ) && StringUtils.isEmpty(alert.getMoniObject())) {
            alert.setMoniObject("Application");
//            alert.setObjectId(alert.getBizSys());
        }
//        ObjectMapper objectMapper = new ObjectMapper();
//        String jsonString = "{}";
//        try {
//            jsonString = objectMapper.writeValueAsString(alert);
//        } catch (JsonProcessingException e) {
//        }
//        JSONObject alertJson = JSONObject.parseObject(jsonString);
//        Map<String, Object> ext = zbxAlert.getExt();
//        cmdbHelper.doExt (alertJson, alertFieldList, ext);
//        //处理cmdb数据
//        if (StringUtils.isNotEmpty(alert.getDeviceIp()) && CollectionUtils.isEmpty(ext)){
//            // 根据  机房 + IP, 查找设备
//            cmdbHelper.queryDeviceForAlertV2(alertJson, alertFieldList,alertHandleHelper.getModelField(AlertConfigConstants.REDIS_MODEL_DEVICE_INSTANCE));
//        }

        alertHandleHelper.handleAlert(alert);
        log.info("------------------consumer {} alert use : {} ms", alertType, (System.currentTimeMillis()-time1));
    }
}
