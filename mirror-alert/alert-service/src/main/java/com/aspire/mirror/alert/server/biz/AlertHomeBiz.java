package com.aspire.mirror.alert.server.biz;

import com.aspire.mirror.alert.api.dto.AlertsQueryRequest;
import com.aspire.mirror.alert.api.dto.model.AlertStatisticSummaryDTO;
import com.aspire.mirror.alert.server.dao.po.Alerts;
import com.aspire.mirror.alert.server.dao.po.AlertsHis;
import com.aspire.mirror.alert.server.domain.AlertsDTO;
import com.aspire.mirror.alert.server.domain.AlertsHisDTO;
import com.aspire.mirror.common.entity.PageRequest;
import com.aspire.mirror.common.entity.PageResponse;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface AlertHomeBiz {

    PageResponse<AlertsDTO> select(PageRequest pageRequest, String alertType);

    List<String> activeAlertSourceList();
    
    AlertStatisticSummaryDTO getOverview(PageRequest pageRequest,String alertType);

    PageResponse<AlertsHisDTO> selectHis(PageRequest pageRequest);

    List<Alerts> export(PageRequest pageRequest, String alertType);

    List<AlertsHis> exportHis(PageRequest pageRequest);

    ResponseEntity<String> getHomeAlertVoiceContent(boolean isUandS, AlertsQueryRequest queryRequest);

    AlertStatisticSummaryDTO hisOverview(PageRequest pageRequest);

}
