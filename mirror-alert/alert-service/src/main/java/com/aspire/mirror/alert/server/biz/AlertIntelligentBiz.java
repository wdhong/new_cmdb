package com.aspire.mirror.alert.server.biz;

import com.aspire.mirror.alert.api.dto.model.AlertStatisticSummaryDTO;
import com.aspire.mirror.alert.server.domain.AlertsDTO;
import com.aspire.mirror.common.entity.PageRequest;
import com.aspire.mirror.common.entity.PageResponse;

import java.util.List;

public interface AlertIntelligentBiz {

    PageResponse<AlertsDTO> queryAlertIntelligent(PageRequest pageRequest, String alertId);

    AlertStatisticSummaryDTO alertIntelligentOverview(PageRequest pageRequest);

    List<AlertsDTO> exportAlertIntelligentData(PageRequest pageRequest);

}
