package com.aspire.mirror.alert.server.biz;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import com.aspire.mirror.alert.api.dto.AlertEsDataRequest;
import com.aspire.mirror.alert.server.dao.po.AlertStandardizationDTO;
import com.aspire.mirror.alert.server.dao.po.KpiBookDTO;

/**
 * 告警业务层接口
 * <p>
 * 项目名称:  mirror平台
 * 包:        com.aspire.mirror.alert.server.biz.impl
 * 类名称:    AlertsHisBiz.java
 * 类描述:    告警业务层接口
 * 创建人:    JinSu
 * 创建时间:  2018/9/14 15:55
 * 版本:      v1.0
 */
public interface AlertRestfulBiz {
  
	void insertBookAlerts(AlertStandardizationDTO stand);

	List<AlertStandardizationDTO> getBookAlerts(String source);
	
	void insertBookKpiList(KpiBookDTO kpi);
	

	Map<String, Object> getIdcTypeUserRate(AlertEsDataRequest request) throws ParseException;
	
	List<Map<String, Object>> getIdcTypeTrends(AlertEsDataRequest request);
}
