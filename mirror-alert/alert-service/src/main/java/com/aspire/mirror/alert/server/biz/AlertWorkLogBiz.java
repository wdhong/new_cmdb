package com.aspire.mirror.alert.server.biz;

import com.aspire.mirror.alert.server.dao.po.AlertWorkConfigDTO;

public interface AlertWorkLogBiz {

    String createdAlerts(AlertWorkConfigDTO alertWorkConfigDTO);

    AlertWorkConfigDTO getAlertWorkConfig();

    Object getWorkLogInfo(String workName,String workDate,String workTime,String work);

    Object getWorkLogList(String workName, String workMonth);

}
