package com.aspire.mirror.alert.server.biz;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.aspire.mirror.alert.api.dto.model.AlertStatisticSummaryDTO;
import com.aspire.mirror.alert.api.dto.model.AlertsStatisticClassifyDTO;
import com.aspire.mirror.alert.api.dto.model.AlertsTop10DTOResponse;
import com.aspire.mirror.alert.server.dao.po.Alerts;
import com.aspire.mirror.alert.server.domain.AlertsDTO;
import com.aspire.mirror.common.entity.PageResponse;

public interface AlertsIndexPageBiz {

    AlertStatisticSummaryDTO getSummaryByOperateType(Integer code,String idcType,String startTime,String endTime);
    AlertStatisticSummaryDTO getHisSummaryByOperateType(String idcType,String startTime,String endTime);
    AlertStatisticSummaryDTO getSummaryByDateRange(Date startDate, Date endDate, String idcType);
    Map<String, Object> getTrendSeries(String interval, String idcType, String deviceType, String alertLevel, String source);
    List<AlertsStatisticClassifyDTO> getClassifySeries(Date startDate, Date endDate, String idcType,Map<String, List<String>> resFilterMap);
	List<AlertsTop10DTOResponse> getAlertTop10(String idcType, String deviceType,
			String alertLevel, String colName);
	PageResponse<AlertsTop10DTOResponse> getAlertMoniIndexTop10(String startDate, String endDate, String idcType,
			String deviceType, String alertLevel);
	List<AlertsDTO> getLatest(Integer limit, String idcType,Integer operateStatus,
			String startDate, String endDate);
	List<Alerts> getLatestOrder(Integer limit, String idcType, Integer operateStatus,
								String startDate, String endDate);
	List<Map<String, Object>> selectIdcTypeAlertsByOperateStatus(Integer operateStatus,
			String startDate, String endDate);
	List<Map<String, Object>> selectDeviceTypeAlertsByOperateStatus(Integer operateStatus, String startDate,
			String endDate);
	List<Map<String, Object>> selectIdcTypeAlertsByOperateStatusOrder(Integer operateStatus,
																 String startDate, String endDate);
	List<Map<String, Object>> selectDeviceTypeAlertsByOperateStatusOrder(Integer operateStatus, String startDate,
																	String endDate);
	List<Map<String,Object>> alertIdcDoHours (String alertLevel);
}
