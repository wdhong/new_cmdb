package com.aspire.mirror.alert.server.biz;

import com.aspire.mirror.alert.api.dto.AlertMainSummaryResponse;
import com.aspire.mirror.alert.api.dto.model.AlertStatisticSummaryDTO;
import com.aspire.mirror.alert.api.dto.model.AlertsStatisticClassifyDTO;
import com.aspire.mirror.alert.api.dto.model.AlertsStatisticSourceClassifyDTO;
import com.aspire.mirror.alert.server.dao.po.AlertsExport;
import com.aspire.mirror.alert.server.domain.AlertTargetCountDTO;
import com.aspire.mirror.alert.server.domain.AlertsDTO;
import com.aspire.mirror.alert.server.domain.AlertsHisDTO;
import com.aspire.mirror.common.entity.PageRequest;
import com.aspire.mirror.common.entity.PageResponse;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface AlertsStatisticBiz {

    AlertStatisticSummaryDTO getSummaryByOperateType(Map<String, Object> param, Map<String, List<String>> resFilterMap);
    AlertStatisticSummaryDTO getSummaryByOperateTypeOrder(Map<String, Object> param, Map<String, List<String>> resFilterMap);
    AlertStatisticSummaryDTO getHisSummaryByOperateType(Map<String, Object> param, Map<String, List<String>> resFilterMap);
    AlertStatisticSummaryDTO getHisSummaryByOperateTypeOrder(Map<String, Object> param, Map<String, List<String>> resFilterMap);
    AlertStatisticSummaryDTO getSummaryByDateRange(Date startDate, Date endDate);
    AlertStatisticSummaryDTO getOverview(String codes);
    List<String> getAlertsMonitObjectList();
    List<String> getHisAlertsMonitObjectList();
    Map<String, Object> getTrendSeries(String interval);
    List<AlertsStatisticClassifyDTO> getClassifySeries(Date startDate, Date endDate);
    Map<String, List<AlertsStatisticSourceClassifyDTO>> getSourceClassifySeries(Date startDate, Date endDate);
    List<AlertsDTO> getLatest(Integer limit,Map<String, List<String>> param);
    PageResponse<AlertsDTO> select(PageRequest pageRequest);
    PageResponse<AlertsHisDTO> selectHis(PageRequest pageRequest);
    List<AlertsExport> export(PageRequest pageRequest);
    List<Map<String, Object>> exportHis(PageRequest pageRequest);
    List<AlertMainSummaryResponse> getAlertSummaryByDeviceClass(String code);
    List<AlertTargetCountDTO> getAlertTargetCount(String alertLevel, String type);
    AlertStatisticSummaryDTO getSummaryByAuthContent(Map<String, Object> authContent);
    List<String> getSummaryByAuthContentTest(Map<String, Object> authContent, Map<String, List<String>> resFilterMap);
    int selectAlert(PageRequest pageRequest);
    List<Map<String, Object>> getAlertCountByIpAndIdc(List<Map<String,Object>> request);

}
