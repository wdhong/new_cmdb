package com.aspire.mirror.alert.server.biz;

import java.util.List;

import com.aspire.mirror.alert.api.dto.AlertDataListSyncRequest;
import com.aspire.mirror.alert.api.dto.AlertSyncResponseItem;

/**
 * 告警数据同步处理业务接口    <br/>
 * Project Name:alert-service
 * File Name:IAlertDataListSyncBiz.java
 * Package Name:com.aspire.mirror.alert.server.biz
 * ClassName: IAlertDataListSyncBiz <br/>
 * date: 2018年9月21日 下午4:34:03 <br/>
 *
 * @author pengguihua
 * @since JDK 1.6
 */
public interface IAlertDataListSyncBiz {
    public List<AlertSyncResponseItem> handleAlertDataListSync(AlertDataListSyncRequest request);
}
