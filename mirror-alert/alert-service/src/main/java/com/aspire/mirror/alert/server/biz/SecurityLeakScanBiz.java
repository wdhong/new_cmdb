package com.aspire.mirror.alert.server.biz;

import com.aspire.mirror.alert.api.dto.model.LeakScanSummaryDTO;
import com.aspire.mirror.alert.api.dto.model.SecurityLeakScanRecordDTO;
import com.aspire.mirror.alert.api.dto.model.SecurityLeakScanReportDTO;
import com.aspire.mirror.alert.server.dao.po.SecurityLeakScanReportFile;
import com.aspire.mirror.common.entity.PageRequest;
import com.aspire.mirror.common.entity.PageResponse;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

public interface SecurityLeakScanBiz {

    String persistScanRecords(File localFile, String dateStr, String ftpFilePath, String fileName) throws IOException, ParseException;
    void fillScanRecordBpmId(String scanId, String bpmId);
    void fillScanRecordBpmFileId(String scanId, String bpmFileId);
    void fillScanReportHtmlPath(String scanId, String dateStr, String bizLine);
    void setBpmReapirStat(String bpmId, int stat);
    List<SecurityLeakScanReportFile> getFileByFtpPath(String ftpFilePath);
    SecurityLeakScanRecordDTO getSecurityLeakScanRecordById(String id);
    List<SecurityLeakScanRecordDTO> getSecurityLeakScanRecordByDateAndFileName(String dateStr, String fileName) throws ParseException;
    PageResponse<LeakScanSummaryDTO> summaryList(PageRequest pageRequest);
    List<LeakScanSummaryDTO> exportList(PageRequest pageRequest);
    PageResponse<SecurityLeakScanReportDTO> reportList(PageRequest pageRequest);
    List<SecurityLeakScanReportDTO> getReportListByScanId(String scanId);
    void clearPastScanRecords(String bizLine, String dateStr) throws ParseException;
    
	PageResponse<LeakScanSummaryDTO> getLeakScanDetailByDate(PageRequest pageRequest);
	Map<String, Object> leaksRankDistribute(String beginDate, String endDate);
	PageResponse<Map<String, Object>> leakStatByBiz(String beginDate, String endDate, Integer begin,Integer pageSize);
	PageResponse<Map<String, Object>> leakStatListByBiz(String beginDate, String endDate, String rankType, Integer begin,Integer pageSize);
	PageResponse<Map<String, Object>> leakTrend(String beginDate, String endDate,Integer begin,Integer pageSize);
	Map<String, Object> leakSummary(String beginDate, String endDate);
    List<Map<String, Object>> getLeakByBizSystem();
    List<Map<String, Object>> getLeakByIp();
    List<Map<String,Object>> getLeakByIdcType();
}
