package com.aspire.mirror.alert.server.biz;
import com.aspire.mirror.alert.api.dto.model.TodayWarningMessageDTO;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

public interface TodayWaringMessageService {

    List<TodayWarningMessageDTO> getSummaryByIdctype(@Param(value = "param") Map<String, Object> param);

    List<TodayWarningMessageDTO> getSummaryByDeviceMfrs(@Param(value = "param") Map<String, Object> param);


    List<TodayWarningMessageDTO> getSummaryByDeviceTypeAndSoyrceRoomAndDeviceClass(@Param(value = "param") Map<String, Object> param);


}
