package com.aspire.mirror.alert.server.biz.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.aspire.mirror.alert.api.dto.AlertDataListSyncRequest;
import com.aspire.mirror.alert.api.dto.AlertSyncResponseItem;
import com.aspire.mirror.alert.server.biz.AlertsBiz;
import com.aspire.mirror.alert.server.biz.AlertsHisBiz;
import com.aspire.mirror.alert.server.util.JsonUtil;
import com.fasterxml.jackson.core.type.TypeReference;

/**
 * 不同来源的告警数据同步处理抽象父类    <br/>
 * Project Name:alert-service
 * File Name:AbstractAlertDataListSyncHandler.java
 * Package Name:com.aspire.mirror.alert.server.biz.impl
 * ClassName: AbstractAlertDataListSyncHandler <br/>
 * date: 2018年9月21日 下午4:41:42 <br/>
 *
 * @author pengguihua
 * @since JDK 1.6
 */
abstract class AbstractAlertDataListSyncHandler {
    @Autowired
    private AlertsBiz alertsBiz;
    @Autowired
    private AlertsHisBiz alertsHisBiz;

    /**
     * 当前handler是否对request负责. <br/>
     * <p>
     * 作者： pengguihua
     *
     * @param request
     * @return
     */
    public abstract boolean isAware(AlertDataListSyncRequest request);

    /**
     * 处理告警同步. <br/>
     * <p>
     * 作者： pengguihua
     *
     * @param request
     * @return
     */
    public abstract List<AlertSyncResponseItem> handleAlertDataListSync(AlertDataListSyncRequest request);

    /**
     * 把告警数据对象转化成目标对象. <br/>
     * <p>
     * 作者： pengguihua
     *
     * @param alertDataList 告警数据
     * @param targetType    目标类型
     * @return
     */
    protected final <T> List<T> parseAlertDataList(List<Object> alertDataList, TypeReference<List<T>> targetType) {
        if (alertDataList == null) {
            return new ArrayList<T>();
        }
        return JsonUtil.jacksonConvert(alertDataList, targetType);
    }
}
