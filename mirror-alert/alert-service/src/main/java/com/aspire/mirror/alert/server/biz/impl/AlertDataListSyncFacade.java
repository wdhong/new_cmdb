package com.aspire.mirror.alert.server.biz.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import com.aspire.mirror.alert.api.dto.AlertDataListSyncRequest;
import com.aspire.mirror.alert.api.dto.AlertSyncResponseItem;
import com.aspire.mirror.alert.server.biz.IAlertDataListSyncBiz;

import lombok.extern.slf4j.Slf4j;

/**
 * 告警数据同步facade <br/>
 * Project Name:alert-service
 * File Name:AlertDataListSyncFacade.java
 * Package Name:com.aspire.mirror.alert.server.biz.impl
 * ClassName: AlertDataListSyncFacade <br/>
 * date: 2018年9月21日 下午5:55:22 <br/>
 *
 * @author pengguihua
 * @since JDK 1.6
 */
@Slf4j
@Primary
@Component
public class AlertDataListSyncFacade implements IAlertDataListSyncBiz {
    @Autowired
    private List<AbstractAlertDataListSyncHandler> handlerChain;

    @Override
    public List<AlertSyncResponseItem> handleAlertDataListSync(AlertDataListSyncRequest request) {
        for (AbstractAlertDataListSyncHandler handler : handlerChain) {
            if (handler.isAware(request)) {
                return handler.handleAlertDataListSync(request);
            }
        }
        log.error("No handler to handle the alertDataListSync request with the source {}.", request.getSource());
        return new ArrayList<AlertSyncResponseItem>();
    }
}
