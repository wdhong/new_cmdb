package com.aspire.mirror.alert.server.biz.impl;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aspire.mirror.alert.api.dto.AlertEsDataRequest;
import com.aspire.mirror.alert.server.biz.AlertRestfulBiz;
import com.aspire.mirror.alert.server.dao.AlertMonthReportSyncDao;
import com.aspire.mirror.alert.server.dao.AlertRestfulDao;
import com.aspire.mirror.alert.server.dao.po.AlertMonthReportIdcType;
import com.aspire.mirror.alert.server.dao.po.AlertStandardizationDTO;
import com.aspire.mirror.alert.server.dao.po.KpiBookDTO;
import com.aspire.mirror.alert.server.dao.po.KpiListData;
import com.aspire.mirror.alert.server.util.DateUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class AlertRestfulBizImpl implements AlertRestfulBiz {
	@Autowired
	private AlertRestfulDao alertRestfulDao;

	@Autowired
	private AlertMonthReportSyncDao alertMonthReportSyncDao;

	@Override
	public void insertBookAlerts(AlertStandardizationDTO stand) {
		String condtions = stand.getConditions();
		if (StringUtils.isNotBlank(condtions)) {
			StringBuffer sb = new StringBuffer();
			String[] arr = condtions.split(";");
			for (String a : arr) {
				String[] cArr = a.split(":");
				if (cArr[0].equals("idcType")) {
					sb.append("and idc_type='" + cArr[1] + "'");
				}
				if (cArr[0].equals("pod")) {
					sb.append("and pod_name='" + cArr[1] + "'");
				}
				if (cArr[0].equals("roomId")) {
					sb.append("and source_room='" + cArr[1] + "'");
				}
				if (cArr[0].equals("source")) {
					sb.append("and source='" + cArr[1] + "'");
				}
			}
			stand.setConditions(sb.toString());
		}

		String extraCols = stand.getExtraCols();
		if (StringUtils.isNotBlank(extraCols)) {
			StringBuffer sb = new StringBuffer();
			String[] arr = extraCols.split(";");
			for (String a : arr) {

				String[] cArr = a.split(":");
				sb.append(",'").append(cArr[0]).append("'").append(" ").append(cArr[1]);

			}
			stand.setExtraCols(sb.toString().substring(1));
		}

		String displayCols = stand.getDisplayCols();

		StringBuffer sbCols = new StringBuffer();
		String[] arr = displayCols.split(";");
		for (String a : arr) {
			if (a.equals("idcType")) {
				sbCols.append(",idc_type");
			}
			if (a.equals("pod")) {
				sbCols.append(",pod_name");
			}
			if (a.equals("roomId")) {
				sbCols.append(",source_room");
			}
			if (a.equals("alertId")) {
				sbCols.append(",alert_id");
			}
			if (a.equals("deviceId")) {
				sbCols.append(",device_id");
			}
			if (a.equals("deviceIp")) {
				sbCols.append(",device_ip");
			}
			if (a.equals("deviceClass")) {
				sbCols.append(",device_class");
			}
			if (a.equals("deviceType")) {
				sbCols.append(",device_type");
			}
			if (a.equals("deviceMfrs")) {
				sbCols.append(",device_mfrs");
			}
			if (a.equals("deviceModel")) {
				sbCols.append(",device_model");
			}
			if (a.equals("hostName")) {
				sbCols.append(",host_name");
			}
			if (a.equals("projectName")) {
				sbCols.append(",project_name");
			}
			if (a.equals("bizSys")) {
				sbCols.append(",biz_sys");
			}
			if (a.equals("moniObject")) {
				sbCols.append(",moni_object");
			}
			if (a.equals("moniIndex")) {
				sbCols.append(",moni_index");
			}
			if (a.equals("alertLevel")) {
				sbCols.append(",alert_level");
			}
			if (a.equals("curMoni_time")) {
				sbCols.append(",cur_moni_time");
			}
			if (a.equals("curMoni_value")) {
				sbCols.append(",cur_moni_value");
			}
			if (a.equals("itemId")) {
				sbCols.append(",item_id");
			}
			if (a.equals("alertStartTime")) {
				sbCols.append(",alert_start_time");
			}
			if (a.equals("alertEndTime")) {
				sbCols.append(",alert_end_time");
			}
			if (a.equals("remark")) {
				sbCols.append(",remark");
			}
			if (a.equals("objectType")) {
				sbCols.append(",object_type");
			}

		}
		stand.setDisplayCols(sbCols.toString().substring(1));

		alertRestfulDao.insertBookAlerts(stand);
	}

	@Override
	public List<AlertStandardizationDTO> getBookAlerts(String source) {
		return alertRestfulDao.getBookAlerts(source);
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void insertBookKpiList(KpiBookDTO kpi) {

		alertRestfulDao.insertKpiBook(kpi);
		List<KpiListData> list = kpi.getKpiList();
		for (KpiListData d : list) {
			d.setKpi_id(kpi.getId());
		}
		alertRestfulDao.insertKpiList(list);
	}

	

	@Override
	public Map<String, Object> getIdcTypeUserRate(AlertEsDataRequest request) throws ParseException {
		String month = request.getMonth();
		if (StringUtils.isBlank(month)) {
			log.warn("运营月报资源池利用率,月份不能为空！");
			return null;
		}
		Map<String, Object> monthData = alertMonthReportSyncDao.queryMonthIdcTypeUseRate(request.getMonth(),
				request.getIdcType(), request.getDeviceType());
		if(null==monthData) {
			return null;
		}
		
		
		String lastMonth = DateUtils.getLastMonth(month);
		
		Map<String, Object> lastMonthData = alertMonthReportSyncDao.queryMonthIdcTypeUseRate(lastMonth,
				request.getIdcType(), request.getDeviceType());
		Float cpuAvg = monthData.get("cpu_avg")==null?null:Float.parseFloat(monthData.get("cpu_avg").toString());
		Float memAvg = monthData.get("memory_avg")==null?null:Float.parseFloat(monthData.get("memory_avg").toString());
		Float cpuMax = monthData.get("cpu_max")==null?null:Float.parseFloat(monthData.get("cpu_max").toString());
		Float memMax = monthData.get("memory_max")==null?null:Float.parseFloat(monthData.get("memory_max").toString());
		monthData.put("last_null_flag", false);
		if(null==lastMonthData) {
			monthData.put("last_null_flag", true);
			return monthData;
		}
		if(null!=cpuAvg) {
			Float lastCpuAvg = lastMonthData.get("cpu_avg")==null?null:Float.parseFloat(lastMonthData.get("cpu_avg").toString());
			if(null!=lastCpuAvg) {
				float compareVal = getCompareVal(cpuAvg,lastCpuAvg);
				monthData.put("compare_cpu_avg", compareVal);
			}
		}
		if(null!=memAvg) {
			Float lastMemAvg = lastMonthData.get("memory_avg")==null?null:Float.parseFloat(lastMonthData.get("memory_avg").toString());
			if(null!=lastMemAvg) {
				float compareVal = getCompareVal(memAvg,lastMemAvg);
				monthData.put("compare_memory_avg", compareVal);
			}
		}
		if(null!=cpuMax) {
			Float lastCpuMax = lastMonthData.get("cpu_max")==null?null:Float.parseFloat(lastMonthData.get("cpu_max").toString());
			if(null!=lastCpuMax) {
				float compareVal = getCompareVal(cpuMax,lastCpuMax);
				monthData.put("compare_cpu_max", compareVal);
			}
		}
		if(null!=memMax) {
			Float lastMemMax =  lastMonthData.get("memory_max")==null?null:Float.parseFloat(lastMonthData.get("memory_max").toString());
			if(null!=lastMemMax) {
				float compareVal = getCompareVal(memMax,lastMemMax);
				monthData.put("compare_memory_max", compareVal);
			}
		}
		return monthData;
	}
	
	public float getCompareVal(float cpuAvg,float lastCpuAvg){
		float compareVal = cpuAvg-lastCpuAvg;
		/*if(lastCpuAvg>0) {
			compareVal = (float)compareVal*100/(float)lastCpuAvg;
		}*/
		compareVal = (float)Math.round(compareVal*100)/100;
		return compareVal;
	}

	@Override
	public List<Map<String, Object>> getIdcTypeTrends(AlertEsDataRequest request) {
		return alertMonthReportSyncDao.queryMonthIdcTypeTrends(request.getMonth(), request.getIdcType(),
				request.getDeviceType());
	}

}
