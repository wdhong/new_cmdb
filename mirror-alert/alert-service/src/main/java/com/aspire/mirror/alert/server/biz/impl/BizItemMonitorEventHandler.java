package com.aspire.mirror.alert.server.biz.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.aspire.mirror.alert.server.clientservice.CmdbResfulClient;
import com.aspire.mirror.alert.server.domain.AlertsDTO;
import com.aspire.mirror.alert.server.v2.biz.AlertsHandleV2Helper;
import com.aspire.mirror.alert.server.v2.domain.AlertsDTOV2;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aspire.mirror.alert.api.dto.ItemMonitorEventCallBackRequest;
import com.aspire.mirror.alert.server.biz.ItemEventCallBackHandleChain.ItemMonitorEventHandler;
import com.aspire.mirror.alert.server.biz.helper.CmdbHelper;

import lombok.extern.slf4j.Slf4j;

/**
* 业务监控告警事件回调处理器    <br/>
* Project Name:alert-service
* File Name:BizItemMonitorEventHandler.java
* Package Name:com.aspire.mirror.alert.server.biz.impl
* ClassName: BizItemMonitorEventHandler <br/>
* date: 2018年10月11日 下午5:29:51 <br/>
* @author pengguihua
* @version 
* @since JDK 1.6
*/ 
@Slf4j
@Component
public class BizItemMonitorEventHandler implements ItemMonitorEventHandler {
	public static final String	TIME_FORMAT					= "yyyy-MM-dd HH:mm:ss";
	private static final String	BIZ_MONITOR_ALERT_ID_PREFIX	= "B_R";				// 业务item生成告警id的前缀

	@Autowired
	private AlertsHandleV2Helper alertHandleHelper;

	@Autowired
	private CmdbResfulClient cmdbResfulClient;
	@Autowired
	private CmdbHelper			cmdbHelper;

	@Override
	public boolean isAware(ItemMonitorEventCallBackRequest event) {
		return event != null && ItemMonitorEventCallBackRequest.OBJECT_TYPE_BIZ.equals(event.getObjectType());
	}

	@Override
	public void handle(ItemMonitorEventCallBackRequest event) {
		try {
			AlertsDTOV2 alert = generateAlert(event);
			alertHandleHelper.handleAlert(alert);
		} catch (Exception e) {
			log.error("Exception when try to handle biz monitor alert event.", e);
		}
	}
	
	/**
	* 构造告警. <br/>
	*
	* 作者： pengguihua
	* @param itemData
	* @return
	*/  
	private AlertsDTOV2 generateAlert(ItemMonitorEventCallBackRequest itemData) {
		AlertsDTOV2 alert = new AlertsDTOV2();
		String ralertId = generateBizMonitorRAlertId(itemData);
//		alert.setSourceRoom(itemData.getRoomId());
//		alert.setAlertId(ralertId);		// 不设置alertId, 由alertHandleHelper生成
		alert.setRAlertId(ralertId);
		alert.setAlertLevel(itemData.getPriority());
		if (ItemMonitorEventCallBackRequest.STATUS_NORMAL.equals(itemData.getStatus())) {
			alert.setAlertType(AlertsDTO.ALERT_REVOKE);
		} else {
			alert.setAlertType(AlertsDTO.ALERT_ACTIVE);
		}
		alert.setBizSys(itemData.getObjectId());
		alert.setCurMoniTime(new Date(itemData.getClock() * 1000L));
		alert.setCurMoniValue(itemData.getItemValue());
		alert.setItemId(itemData.getItemId());
		alert.setObjectType(itemData.getObjectType());
//		alert.setEventId(itemData.getTriggerId());
//		alert.setObjectId(itemData.getObjectId());
		//设置操作状态为待确认
		alert.setOperateStatus(0);
		List<Map<String, Object>> groupDescList = itemData.getGroupDesc();
		if (CollectionUtils.isEmpty(groupDescList)) {
			alert.setMoniObject(itemData.getItemName());
		} else {
			List<String> descList = groupDescList.stream().map(item -> {
				String name = String.valueOf(item.getOrDefault("name", ""));
				String value = String.valueOf(item.getOrDefault("value", ""));
				return name + ":" + value;
			}).collect(Collectors.toList());
			alert.setMoniObject(StringUtils.join(descList, ",") +" "+ itemData.getItemName());
		}
		alert.setMoniIndex(itemData.getTriggerName());
//		alert.setMoniObject(itemData.getItemName());
		Map<String, Object> params = Maps.newHashMap();
		params.put("condicationCode", "business_detail");
		params.put("token", "5245ed1b-6345-11e");
		params.put("module_id", "9212e88a698d43cbbf9ec35b83773e2d");
		params.put("id", itemData.getObjectId());
		Map<String, Object> resultInstance = cmdbResfulClient.getInstanceDetail(params);
		if (resultInstance != null && resultInstance.get("bizSystem") != null) {
			String bizName = (String) resultInstance.get("bizSystem");
			alert.setBizSys(bizName);
		}
//			alert.setBizSys(itemData.getObjectId());
//		alert.setBizSysName(cmdbHelper.getBizSysName(itemData.getObjectId()));
//		alert.setSource(SOURCE_MIRROR);
		if (StringUtils.isNotBlank(itemData.getRemark())) {
			alert.setRemark(itemData.getRemark());
		} 
		else {
			DateFormat dataFormat = new SimpleDateFormat(TIME_FORMAT);
			alert.setRemark("Business alert at timemark: " + dataFormat.format(new Date()));
		}
		return alert;
	}
	
	/**
	* 生成业务监控项的R告警id. <br/>
	*
	* 生成逻辑：  B_R机房id_itemId_bizCode_L告警级别 _G告警分组     样例： B_R1-2-xxxx-xxxx_L4_G_xxxx  <br/>
	*
	* 作者： pengguihua
	* @param itemData
	* @return
	*/  
	private String generateBizMonitorRAlertId(ItemMonitorEventCallBackRequest itemData) {
		String alertId = BIZ_MONITOR_ALERT_ID_PREFIX + itemData.getRoomId() + "-" + itemData.getObjectType() + "-" 
				+ itemData.getObjectId() + "-" + itemData.getItemId()  + "_L" + itemData.getPriority();
		String groupKey = itemData.getGroupKey();
		if (StringUtils.isBlank(groupKey)) {
			return alertId;
		}
		int iTemp = groupKey.hashCode();
		if (Integer.MIN_VALUE != iTemp) {
			alertId = alertId + "_G" + Math.abs(iTemp);
		} else {
			alertId = alertId + "_G" + iTemp;
		}
		return alertId;
	}
}
