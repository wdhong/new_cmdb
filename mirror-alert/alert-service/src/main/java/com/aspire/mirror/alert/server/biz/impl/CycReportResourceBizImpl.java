package com.aspire.mirror.alert.server.biz.impl;

import com.aspire.mirror.alert.api.dto.CycReportResFileRequest;
import com.aspire.mirror.alert.server.biz.CycReportResourceBiz;
import com.aspire.mirror.alert.server.dao.CycReportResourceDao;
import com.aspire.mirror.alert.server.dao.po.CycReportResFile;
import com.google.common.collect.Lists;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CycReportResourceBizImpl implements CycReportResourceBiz {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(CycReportResourceBizImpl.class);

    @Autowired
    private CycReportResourceDao cycReportResourceDao;

    @Override
    public void insertFtpFiles(List<CycReportResFileRequest> files) {
        List<CycReportResFile> fileList = Lists.newArrayList();
        for (CycReportResFileRequest resFileRequest : files) {
            CycReportResFile po = new CycReportResFile();
            BeanUtils.copyProperties(resFileRequest, po);
            fileList.add(po);
        }
        cycReportResourceDao.insertFtpFileList(fileList);
    }

    @Override
    public void updateFtpFileReadStat(String filePath) {
        cycReportResourceDao.updateFtpFileReadStatus(filePath);
    }
}
