package com.aspire.mirror.alert.server.biz.impl;

import com.aspire.mirror.alert.api.dto.ItemMonitorEventCallBackRequest;
import com.aspire.mirror.alert.server.biz.ItemEventCallBackHandleChain.ItemMonitorEventHandler;
import com.aspire.mirror.alert.server.biz.helper.CmdbHelper;
import com.aspire.mirror.alert.server.domain.AlertsDTO;
import com.aspire.mirror.alert.server.v2.biz.AlertsHandleV2Helper;
import com.aspire.mirror.alert.server.v2.domain.AlertsDTOV2;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
* 业务监控告警事件回调处理器    <br/>
* Project Name:alert-service
* File Name:BizItemMonitorEventHandler.java
* Package Name:com.aspire.mirror.alert.server.biz.impl
* ClassName: BizItemMonitorEventHandler <br/>
* date: 2018年10月11日 下午5:29:51 <br/>
* @author pengguihua
* @version 
* @since JDK 1.6
*/ 
@Slf4j
@Component
public class SysItemMonitorEventHandler implements ItemMonitorEventHandler {
	public static final String	TIME_FORMAT					= "yyyy-MM-dd HH:mm:ss";
	private static final String	SYS_MONITOR_ALERT_ID_PREFIX	= "S_R";				// 业务item生成告警id的前缀

	@Autowired
	private AlertsHandleV2Helper alertHandleHelper;

	@Autowired
	private CmdbHelper			cmdbHelper;

	@Override
	public boolean isAware(ItemMonitorEventCallBackRequest event) {
		return event != null && ItemMonitorEventCallBackRequest.OBJECT_TYPE_DEVICE.equals(event.getObjectType());
	}

	@Override
	public void handle(ItemMonitorEventCallBackRequest event) {
		try {
			AlertsDTOV2 alert = generateAlert(event);
			alertHandleHelper.handleAlert(alert);
		} catch (Exception e) {
			log.error("Exception when try to handle biz monitor alert event.", e);
		}
	}
	
	/**
	* 构造告警. <br/>
	*
	* 作者： pengguihua
	* @param itemData
	* @return
	*/  
	private AlertsDTOV2 generateAlert(ItemMonitorEventCallBackRequest itemData) {
		AlertsDTOV2 alert = new AlertsDTOV2();
		String ralertId = generateBizMonitorRAlertId(itemData);
//		alert.setSourceRoom(itemData.getRoomId());
//		alert.setAlertId(alertId);	// 由alertsHandleHelper生成
		alert.setRAlertId(ralertId);
		alert.setAlertLevel(itemData.getPriority());
		if (ItemMonitorEventCallBackRequest.STATUS_NORMAL.equals(itemData.getStatus())) {
			alert.setAlertType(AlertsDTO.ALERT_REVOKE);
		} else {
			alert.setAlertType(AlertsDTO.ALERT_ACTIVE);
		}
		alert.setDeviceId(itemData.getObjectId());
		String deviceIp = cmdbHelper.getDeviceIp(itemData.getObjectId());
		if (deviceIp != null) {
			alert.setDeviceIp(deviceIp);
		}
		alert.setCurMoniTime(new Date(itemData.getClock() * 1000L));
		alert.setCurMoniValue(itemData.getItemValue());
		alert.setItemId(itemData.getItemId());
		alert.setObjectType(itemData.getObjectType());
//		alert.setEventId(itemData.getTriggerId());
//		alert.setObjectId(itemData.getObjectId());
		alert.setMoniIndex(itemData.getTriggerName());
		alert.setMoniObject(itemData.getItemName());
//		alert.setBizSysName(cmdbHelper.getBizSysName(itemData.getObjectId()));
//		alert.setSource(SOURCE_MIRROR);
		DateFormat dataFormat = new SimpleDateFormat(TIME_FORMAT);
		alert.setRemark("Business alert at timemark: " + dataFormat.format(new Date()));
		return alert;
	}
	
	/**
	* 生成业务监控项的告警id. <br/>
	*
	* 生成逻辑：  B_R机房id_itemId_bizCode_L告警级别      样例： B_R1-2-xxxx-xxxx_L4  <br/>
	*
	* 作者： pengguihua
	* @param itemData
	* @return
	*/  
	private String generateBizMonitorRAlertId(ItemMonitorEventCallBackRequest itemData) {
		return SYS_MONITOR_ALERT_ID_PREFIX + itemData.getRoomId() + "-" + itemData.getObjectType() + "-"
				+ itemData.getObjectId() + "-" + itemData.getItemId()  + "_L" + itemData.getPriority();
	}
}
