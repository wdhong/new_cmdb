package com.aspire.mirror.alert.server.biz.impl;

import com.aspire.mirror.alert.api.dto.model.TodayWarningMessageDTO;
import com.aspire.mirror.alert.server.biz.TodayWaringMessageService;
import com.aspire.mirror.alert.server.controller.third.WaringMessageutils;
import com.aspire.mirror.alert.server.dao.AlertsTodayWaringMessageDao;
import com.aspire.mirror.alert.server.dao.TodayWaringMessageDao;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
@Service
@Slf4j
public class TodayWaringMessageServiceImpl implements TodayWaringMessageService {
    @Autowired
    private TodayWaringMessageDao todayWaringMessageDao;
    @Autowired
    private AlertsTodayWaringMessageDao alertsTodayWaringMessageDao;

    @Override
    public List<TodayWarningMessageDTO> getSummaryByIdctype(Map<String, Object> param) {
        //根据时间去查询出当前待确认和确认的告警信息
        List todayWarningMessageDTOList = new ArrayList();
        List<Map<String, Object>> summaryByIdctype = todayWaringMessageDao.getSummaryByIdctype(param);
        if (!CollectionUtils.isEmpty(summaryByIdctype)) {
            for (int i = 0; i < summaryByIdctype.size(); i++) {
                if ("toBeConfirmed".equals(summaryByIdctype.get(i).get("operateStatus")) ||
                        "confirmed".equals(summaryByIdctype.get(i).get("operateStatus"))) {
                    TodayWarningMessageDTO todayWarningMessageDTO = new TodayWarningMessageDTO();
                    if ("toBeConfirmed".equals(summaryByIdctype.get(i).get("operateStatus"))) {
                        todayWarningMessageDTO.setName("待确认");
                    } else if ("confirmed".equals(summaryByIdctype.get(i).get("operateStatus"))) {
                        todayWarningMessageDTO.setName("已确认");
                    }
                    todayWarningMessageDTO.setSum(summaryByIdctype.get(i).get("codeCount") == null ? 0 : Integer.valueOf(String.valueOf(summaryByIdctype.get(i).get("codeCount"))));
                    todayWarningMessageDTO.setElementary(summaryByIdctype.get(i).get("lCount") == null ? 0 : Integer.valueOf(String.valueOf(summaryByIdctype.get(i).get("lCount"))));
                    todayWarningMessageDTO.setMediumGrade(summaryByIdctype.get(i).get("mCount") == null ? 0 : Integer.valueOf(String.valueOf(summaryByIdctype.get(i).get("mCount"))));
                    todayWarningMessageDTO.setSenior(summaryByIdctype.get(i).get("hCount") == null ? 0 : Integer.valueOf(String.valueOf(summaryByIdctype.get(i).get("hCount"))));
                    todayWarningMessageDTO.setGreat(summaryByIdctype.get(i).get("sCount") == null ? 0 : Integer.valueOf(String.valueOf(summaryByIdctype.get(i).get("sCount"))));
                    todayWarningMessageDTOList.add(todayWarningMessageDTO);
                }
                continue;
            }
        }
        List<Map<String, Object>> hisSummaryByIdctyp = null;
        int i = 0;int a = 0;int b = 0;int c = 0;int d = 0;
        TodayWarningMessageDTO todayWarningMessageDTO = new TodayWarningMessageDTO();
        try {
            hisSummaryByIdctyp = alertsTodayWaringMessageDao.getHisSummaryByIdctyp(param);
            if (!CollectionUtils.isEmpty(hisSummaryByIdctyp)) {
                for (int j = 0; j < hisSummaryByIdctyp.size(); j++) {

                    //清除人
                    if (!"".equals(hisSummaryByIdctyp.get(j).get("clear_user")) && null != hisSummaryByIdctyp.get(j).get("clear_user")) {
                        todayWarningMessageDTO.setName("已清除");
                        //获取到清除告警信息的总数
                        i += Integer.parseInt(hisSummaryByIdctyp.get(j).get("codeCount").toString());
                        a += Integer.parseInt(hisSummaryByIdctyp.get(j).get("lCount").toString());
                        b += Integer.parseInt(hisSummaryByIdctyp.get(j).get("mCount").toString());
                        c += Integer.parseInt(hisSummaryByIdctyp.get(j).get("hCount").toString());
                        d += Integer.parseInt(hisSummaryByIdctyp.get(j).get("sCount").toString());
                    } else {
                        TodayWarningMessageDTO todayWarningMessageDTO1 = new TodayWarningMessageDTO();
                        todayWarningMessageDTO1.setName("已解除");
                        //获取到告警信息的总数
                        todayWarningMessageDTO1.setSum(hisSummaryByIdctyp.get(j).get("codeCount") == null ? 0 : Integer.valueOf(String.valueOf(hisSummaryByIdctyp.get(j).get("codeCount"))));
                        todayWarningMessageDTO1.setElementary(hisSummaryByIdctyp.get(j).get("lCount") == null ? 0 : Integer.valueOf(String.valueOf(hisSummaryByIdctyp.get(j).get("lCount"))));
                        todayWarningMessageDTO1.setMediumGrade(hisSummaryByIdctyp.get(j).get("mCount") == null ? 0 : Integer.valueOf(String.valueOf(hisSummaryByIdctyp.get(j).get("mCount"))));
                        todayWarningMessageDTO1.setSenior(hisSummaryByIdctyp.get(j).get("hCount") == null ? 0 : Integer.valueOf(String.valueOf(hisSummaryByIdctyp.get(j).get("hCount"))));
                        todayWarningMessageDTO1.setGreat(hisSummaryByIdctyp.get(j).get("sCount") == null ? 0 : Integer.valueOf(String.valueOf(hisSummaryByIdctyp.get(j).get("sCount"))));
                        todayWarningMessageDTOList.add(todayWarningMessageDTO1);
                    }
                }
            }
            if(!CollectionUtils.isEmpty(hisSummaryByIdctyp)){
            todayWarningMessageDTO.setSum(i);
            todayWarningMessageDTO.setElementary(a);
            todayWarningMessageDTO.setMediumGrade(b);
            todayWarningMessageDTO.setSenior(c);
            todayWarningMessageDTO.setGreat(d);
            todayWarningMessageDTOList.add(todayWarningMessageDTO);
            }

        } catch (Exception e) {
            log.info("eror" + e);
        }
        return todayWarningMessageDTOList;
    }


    @Override
    public List<TodayWarningMessageDTO> getSummaryByDeviceMfrs(Map<String, Object> param) {
        List<Map<String, Object>> summaryByyDeviceMfrs = todayWaringMessageDao.getSummaryByDeviceMfrs(param);
        return WaringMessageutils.util(summaryByyDeviceMfrs);
    }

    @Override
    public List<TodayWarningMessageDTO> getSummaryByDeviceTypeAndSoyrceRoomAndDeviceClass(Map<String, Object> param) {
        List summaryByIdctypeList = new ArrayList();
        List<Map<String, Object>> summaryByDeviceTypeAndSoyrceRoomAndDeviceClass = todayWaringMessageDao.getSummaryByDeviceTypeAndSoyrceRoomAndDeviceClass(param);
        if (!CollectionUtils.isEmpty(summaryByDeviceTypeAndSoyrceRoomAndDeviceClass)) {
            for (int i = 0; i < summaryByDeviceTypeAndSoyrceRoomAndDeviceClass.size(); i++) {
                TodayWarningMessageDTO todayWarningMessageDTO = new TodayWarningMessageDTO();
                if ("X86服务器".equals(summaryByDeviceTypeAndSoyrceRoomAndDeviceClass.get(i).get("device_class"))) {
                    todayWarningMessageDTO.setName("裸金属");
                    todayWarningMessageDTO.setSum(summaryByDeviceTypeAndSoyrceRoomAndDeviceClass.get(i).get("codeCount") == null ? 0 : Integer.valueOf(String.valueOf(summaryByDeviceTypeAndSoyrceRoomAndDeviceClass.get(i).get("codeCount"))));
                    todayWarningMessageDTO.setElementary(summaryByDeviceTypeAndSoyrceRoomAndDeviceClass.get(i).get("lCount") == null ? 0 : Integer.valueOf(String.valueOf(summaryByDeviceTypeAndSoyrceRoomAndDeviceClass.get(i).get("lCount"))));
                    todayWarningMessageDTO.setMediumGrade(summaryByDeviceTypeAndSoyrceRoomAndDeviceClass.get(i).get("mCount") == null ? 0 : Integer.valueOf(String.valueOf(summaryByDeviceTypeAndSoyrceRoomAndDeviceClass.get(i).get("mCount"))));
                    todayWarningMessageDTO.setSenior(summaryByDeviceTypeAndSoyrceRoomAndDeviceClass.get(i).get("hCount") == null ? 0 : Integer.valueOf(String.valueOf(summaryByDeviceTypeAndSoyrceRoomAndDeviceClass.get(i).get("hCount"))));
                    todayWarningMessageDTO.setGreat(summaryByDeviceTypeAndSoyrceRoomAndDeviceClass.get(i).get("sCount") == null ? 0 : Integer.valueOf(String.valueOf(summaryByDeviceTypeAndSoyrceRoomAndDeviceClass.get(i).get("sCount"))));
                    summaryByIdctypeList.add(todayWarningMessageDTO);
                } else {
                    todayWarningMessageDTO.setName((String) summaryByDeviceTypeAndSoyrceRoomAndDeviceClass.get(i).get("device_class") == null ? null : (String) summaryByDeviceTypeAndSoyrceRoomAndDeviceClass.get(i).get("device_class"));
                    todayWarningMessageDTO.setSum(summaryByDeviceTypeAndSoyrceRoomAndDeviceClass.get(i).get("codeCount") == null ? 0 : Integer.valueOf(String.valueOf(summaryByDeviceTypeAndSoyrceRoomAndDeviceClass.get(i).get("codeCount"))));
                    todayWarningMessageDTO.setElementary(summaryByDeviceTypeAndSoyrceRoomAndDeviceClass.get(i).get("lCount") == null ? 0 : Integer.valueOf(String.valueOf(summaryByDeviceTypeAndSoyrceRoomAndDeviceClass.get(i).get("lCount"))));
                    todayWarningMessageDTO.setMediumGrade(summaryByDeviceTypeAndSoyrceRoomAndDeviceClass.get(i).get("mCount") == null ? 0 : Integer.valueOf(String.valueOf(summaryByDeviceTypeAndSoyrceRoomAndDeviceClass.get(i).get("mCount"))));
                    todayWarningMessageDTO.setSenior(summaryByDeviceTypeAndSoyrceRoomAndDeviceClass.get(i).get("hCount") == null ? 0 : Integer.valueOf(String.valueOf(summaryByDeviceTypeAndSoyrceRoomAndDeviceClass.get(i).get("hCount"))));
                    todayWarningMessageDTO.setGreat(summaryByDeviceTypeAndSoyrceRoomAndDeviceClass.get(i).get("sCount") == null ? 0 : Integer.valueOf(String.valueOf(summaryByDeviceTypeAndSoyrceRoomAndDeviceClass.get(i).get("sCount"))));
                    summaryByIdctypeList.add(todayWarningMessageDTO);
                }
            }
        }
        return summaryByIdctypeList;
    }
}
