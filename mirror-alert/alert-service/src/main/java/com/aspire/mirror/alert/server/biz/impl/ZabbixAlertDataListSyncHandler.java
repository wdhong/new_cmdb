package com.aspire.mirror.alert.server.biz.impl;
import static com.aspire.mirror.alert.server.domain.ZabbixAlert.SOURCE_ZABBIX;

import java.util.ArrayList;
import java.util.List;

import com.aspire.mirror.alert.server.domain.AlertsDTO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aspire.mirror.alert.api.dto.AlertDataListSyncRequest;
import com.aspire.mirror.alert.api.dto.AlertSyncResponseItem;
import com.aspire.mirror.alert.server.biz.AlertsHandleHelper;
import com.aspire.mirror.alert.server.biz.helper.CmdbHelper;
import com.aspire.mirror.alert.server.domain.ZabbixAlert;
import com.fasterxml.jackson.core.type.TypeReference;

/**
* ZABBIX告警数据同步处理器    <br/>
* Project Name:alert-service
* File Name:ZabbixAlertDataListSyncHandler.java
* Package Name:com.aspire.mirror.alert.server.biz.impl
* ClassName: ZabbixAlertDataListSyncHandler <br/>
* date: 2018年9月21日 下午4:45:25 <br/>
* @author pengguihua
* @version 
* @since JDK 1.6
*/
@Component
class ZabbixAlertDataListSyncHandler extends AbstractAlertDataListSyncHandler {
	@Autowired
	private CmdbHelper			cmdbHelper;
	@Autowired
	private AlertsHandleHelper	alertHandleHelper;
	
	@Override
	public boolean isAware(AlertDataListSyncRequest request) {
		return (request != null) && (SOURCE_ZABBIX.equalsIgnoreCase(request.getSource()));
	}

	@Override
	public List<AlertSyncResponseItem> handleAlertDataListSync(AlertDataListSyncRequest request) {
		TypeReference<List<ZabbixAlert>> typeRef = new TypeReference<List<ZabbixAlert>>() {};
		List<ZabbixAlert> zbxAlertList = super.parseAlertDataList(request.getAlertDataList(), typeRef);
		List<ZabbixAlert> failList = new ArrayList<ZabbixAlert>();
		List<AlertSyncResponseItem> respItemList = new ArrayList<AlertSyncResponseItem>();
		
		for (ZabbixAlert zbxAlert : zbxAlertList) {
			try {
				if (failList.contains(zbxAlert)) {
					String failTip = "As the previous alert with same alertId failed, "
							       + "this alert record is marked as failure directly.";
					respItemList.add(buildResponseItem(zbxAlert, AlertSyncResponseItem.RESP_CODE_ERROR, failTip));
					continue;
				}
				
				if (StringUtils.isBlank(zbxAlert.getMoniResult())
						|| (!AlertsDTO.ALERT_ACTIVE.equals(zbxAlert.getMoniResult())
								&& !AlertsDTO.ALERT_REVOKE.equals(zbxAlert.getMoniResult()))) {
					failList.add(zbxAlert);
					String failTip = "The value of the monitResult is not valid.";
					respItemList.add(buildResponseItem(zbxAlert, AlertSyncResponseItem.RESP_CODE_ERROR, failTip));
					continue;
				}
				
				AlertsDTO dto = zbxAlert.parse(cmdbHelper);
//				alertHandleHelper.handleAlert(dto);
				respItemList.add(buildResponseItem(zbxAlert, AlertSyncResponseItem.RESP_CODE_OK, null));
			} catch (Exception e) {
				failList.add(zbxAlert);
				String failTip = "Exception during the process, detail is: " + e.getMessage();
				respItemList.add(buildResponseItem(zbxAlert, AlertSyncResponseItem.RESP_CODE_ERROR, failTip));
			}
		}
		return respItemList;
	}
	
	private AlertSyncResponseItem buildResponseItem(ZabbixAlert alert, String respCode, String errorTip) {
		AlertSyncResponseItem respItem = new AlertSyncResponseItem();
		respItem.setEventId(alert.getUmsAlertId());
		respItem.setIndexNum(alert.getIndexNum());
		respItem.setRespCode(respCode);
		respItem.setRespDesc(errorTip);
		return respItem;
	}
}
