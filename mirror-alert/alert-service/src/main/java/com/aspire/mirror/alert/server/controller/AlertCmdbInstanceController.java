package com.aspire.mirror.alert.server.controller;

import com.aspire.mirror.alert.api.service.IAlertCmdbInstanceService;
import com.aspire.mirror.alert.server.v2.biz.AlertFieldBiz;
import com.aspire.mirror.alert.server.v2.constant.AlertConfigConstants;
import com.aspire.mirror.alert.server.v2.dao.CmdbInstanceMapper;
import com.aspire.mirror.alert.server.v2.dao.po.AlertFieldRequestDTO;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Locale;
import java.util.Map;

@RestController
@Slf4j
public class AlertCmdbInstanceController implements IAlertCmdbInstanceService {

    @Autowired
    private CmdbInstanceMapper cmdbInstanceMapper;
    @Autowired
    private AlertFieldBiz alertFieldBiz;
    @Override
    public Map<String, Object> detailById(@PathVariable("id") String id) {
        List<Map<String, Object>> list = cmdbInstanceMapper.detailsById(id);
        if (list.isEmpty()) {
            return Maps.newHashMap();
        }
        Map<String, Object> map = list.get(0);
        List<AlertFieldRequestDTO> cmdbModelFieldList = alertFieldBiz.getModelField(AlertConfigConstants.REDIS_MODEL_DEVICE_INSTANCE);

        Map<String, Object> resultMap = Maps.newHashMap();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            String key = entry.getKey();
            if (!key.toLowerCase(Locale.getDefault()).endsWith("_name")) {
                if (!resultMap.containsKey(key)) {
                    resultMap.put(key, entry.getValue());
                }
                continue;
            }
            String preKey = key.substring(0, key.length() - 5);
            if (!map.containsKey(preKey)) {
                if (!resultMap.containsKey(key)) {
                    resultMap.put(key, entry.getValue());
                }
                continue;
            }
            if (CollectionUtils.isEmpty(cmdbModelFieldList)) {
                continue;
            }

            for (AlertFieldRequestDTO alertFieldRequestDTO: cmdbModelFieldList) {
                String fieldCode = alertFieldRequestDTO.getFieldCode();
                String ciCode = alertFieldRequestDTO.getCiCode();
                if (!key.equalsIgnoreCase(fieldCode)) {
                    continue;
                }

                if (ciCode.toLowerCase(Locale.getDefault()).endsWith("_name")) {
                    resultMap.put(preKey, entry.getValue());
                }
            }

        }

        return resultMap;
    }
}
