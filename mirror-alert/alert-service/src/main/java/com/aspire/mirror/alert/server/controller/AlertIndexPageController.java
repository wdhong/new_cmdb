package com.aspire.mirror.alert.server.controller;

import com.aspire.mirror.alert.api.dto.AlertEsDataRequest;
import com.aspire.mirror.alert.api.dto.AlertStatisticSummaryResponse;
import com.aspire.mirror.alert.api.dto.AlertsDetailResponse;
import com.aspire.mirror.alert.api.dto.model.AlertStatisticSummaryDTO;
import com.aspire.mirror.alert.api.dto.model.AlertsStatisticClassifyDTO;
import com.aspire.mirror.alert.api.dto.model.AlertsTop10DTOResponse;
import com.aspire.mirror.alert.api.service.AlertIndexPageService;
import com.aspire.mirror.alert.server.aspect.RequestAuthContext;
import com.aspire.mirror.alert.server.biz.AlertsIndexPageBiz;
import com.aspire.mirror.alert.server.clientservice.CmdbResfulClient;
import com.aspire.mirror.alert.server.constant.AlertCommonConstant;
import com.aspire.mirror.alert.server.dao.AlertMonthReportSyncDao;
import com.aspire.mirror.alert.server.dao.AlertStorageUseRateDao;
import com.aspire.mirror.alert.server.dao.po.Alerts;
import com.aspire.mirror.alert.server.domain.AlertsDTO;
import com.aspire.mirror.alert.server.helper.AuthHelper;
import com.aspire.mirror.alert.server.util.DateUtils;
import com.aspire.mirror.alert.server.util.TransformUtils;
import com.aspire.mirror.alert.server.v2.constant.AlertConfigConstants;
import com.aspire.mirror.common.entity.PageResponse;
import com.aspire.mirror.common.util.DateUtil;
import com.aspire.ums.cmdb.restful.payload.StatisticRequestEntity;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
public class AlertIndexPageController implements AlertIndexPageService {
	@Autowired
	private AlertsIndexPageBiz alertsIndexPageBiz;
	
	@Autowired
	private AlertStorageUseRateDao alertStorageUseRateDao;
	
	@Autowired
	private AlertMonthReportSyncDao alertMonthReportSyncDao;
	@Autowired
	private CmdbResfulClient  cmdbResfulClient;
	@Value("${cmdbQueryType:bpm_department_index_bizSystem_instance_stats}")
	private String   cmdbQueryName;

	@Value("${alert.branch:}")
	private String branch;
	@Autowired
	private AuthHelper authHelper;

	@Override
	public AlertStatisticSummaryResponse summary(@RequestParam(value = "idcType", required = false) String idcType) {
		AlertStatisticSummaryResponse response = new AlertStatisticSummaryResponse();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String startTime = simpleDateFormat.format(date);
		String endTime = DateUtils.getSpecifiedDayAfter(startTime,1);
		AlertStatisticSummaryDTO toBeConfirmed = alertsIndexPageBiz.getSummaryByOperateType(0, idcType,startTime,endTime);// 待确认
		response.setToBeConfirmed(toBeConfirmed);
		AlertStatisticSummaryDTO confirmed = alertsIndexPageBiz.getSummaryByOperateType(1, idcType,startTime,endTime);
		response.setConfirmed(confirmed);// 已确认

		AlertStatisticSummaryDTO toBeResolved = new AlertStatisticSummaryDTO();
		toBeResolved.setSummary(toBeConfirmed.getSummary() + confirmed.getSummary());
		toBeResolved.setLow(toBeConfirmed.getLow() + confirmed.getLow());
		toBeResolved.setMedium(toBeConfirmed.getMedium() + confirmed.getMedium());
		toBeResolved.setHigh(toBeConfirmed.getHigh() + confirmed.getHigh());
		toBeResolved.setSerious(toBeConfirmed.getSerious() + confirmed.getSerious());
		response.setToBeResolved(toBeResolved);// 待解决 = 待确认 + 已确认

		response.setResolved(alertsIndexPageBiz.getHisSummaryByOperateType(idcType,startTime,endTime));// 已处理
		return response;
	}

	@Override
	public AlertStatisticSummaryDTO alertLevelSummayByTimeSpan(@RequestParam(value = "span") String span,
			@RequestParam(value = "idcType", required = false) String idcType) {
		if (StringUtils.isEmpty(span)) {
			return null;
		}
		Date startDate = DateUtils.getDateBySpan(span.toLowerCase());
		Date endDate = DateUtils.getTimesmorning();
		return alertsIndexPageBiz.getSummaryByDateRange(startDate, endDate, idcType);
	}

	@Override
	public Map trend(@RequestParam(value = "span") String inteval,
			@RequestParam(value = "idcType", required = false) String idcType,
			@RequestParam(value = "deviceType", required = false) String deviceType,
			@RequestParam(value = "alertLevel", required = false) String alertLevel,
			@RequestParam(value = "source", required = false) String source) {
		if(StringUtils.isNotBlank(source)) {
			if(source.equals("日志")) {
				source = "syslog";
			}
		}
		return alertsIndexPageBiz.getTrendSeries(inteval, idcType, deviceType, alertLevel, source);
	}

	@Override
	public List<AlertsStatisticClassifyDTO> classifyByTimeSpan(@RequestParam(value = "span") String span,
			@RequestParam(value = "idcType", required = false) String idcType) {
		if (StringUtils.isEmpty(span)) {
			return null;
		}
		Date startDate = DateUtils.getDateBySpan(span.toLowerCase());
		Date endDate = DateUtils.getTimesmorning();
		// 获取数据权限
		Map<String, List<String>> resFilterMap = RequestAuthContext.getRequestHeadUser().getResFilterConfig();
		return alertsIndexPageBiz.getClassifySeries(startDate, endDate, idcType,resFilterMap);
	}

	@Override
	public List<AlertsTop10DTOResponse> alertTop10(@RequestParam(value = "idcType", required = false) String idcType,
			@RequestParam(value = "deviceType", required = false) String deviceType,
			@RequestParam(value = "alertLevel", required = false) String alertLevel,
			@RequestParam(value = "colName") String colName) {
		if (StringUtils.isEmpty(colName)) {
			return null;
		}
		return alertsIndexPageBiz.getAlertTop10(idcType, deviceType, alertLevel, colName);
	}

	@Override
	public PageResponse<AlertsTop10DTOResponse> alertMoniIndexTop10(
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate,
			@RequestParam(value = "idcType", required = false) String idcType,
			@RequestParam(value = "deviceType", required = false) String deviceType,
			@RequestParam(value = "alertLevel", required = false) String alertLevel) {
		return alertsIndexPageBiz.getAlertMoniIndexTop10(startDate, endDate, idcType, deviceType, alertLevel);
	}

	@Override
	public List<AlertsDetailResponse> latest(@RequestParam(value = "limit", required = false) Integer limit,
			@RequestParam(value = "idcType", required = false) String idcType
			,@RequestParam(value = "operateStatus", required = false) Integer operateStatus,
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate) {
			if(StringUtils.isBlank(startDate)) {
				Date now = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				endDate = sdf1.format( now);
				startDate = sdf.format(now)+" 00:00:00";
			}
			if (AlertConfigConstants.BRANCH_IT_YUN.equalsIgnoreCase(branch)) {
				return TransformUtils.transform(AlertsDetailResponse.class,
						alertsIndexPageBiz.getLatestOrder(limit,idcType,operateStatus,null,null));
			} else {
				return TransformUtils.transform(AlertsDetailResponse.class,
						alertsIndexPageBiz.getLatest(limit,idcType,operateStatus,startDate,endDate));
			}
	}

	/**
     * 导出警报列表数据
     */
    @Override
    public List<Map<String, Object>> exportLatest(@RequestParam(value = "limit", required = false) Integer limit,
			@RequestParam(value = "idcType", required = false) String idcType
			,@RequestParam(value = "operateStatus", required = false) Integer operateStatus,
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate) throws Exception {
    	if(StringUtils.isBlank(startDate)) {
			Date now = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			endDate = sdf1.format( now);
			startDate = sdf.format(now)+" 00:00:00";
		} 
        List<Map<String, Object>> dataLists = Lists.newArrayList();
		if (AlertConfigConstants.BRANCH_IT_YUN.equalsIgnoreCase(branch)) {
			List<Alerts> alertsList = alertsIndexPageBiz.getLatestOrder(limit, idcType, operateStatus, null, null);
			for (Alerts alerts: alertsList) {
				alerts.setAlertLevel(AlertCommonConstant.alertLevelMap.get(alerts.getAlertLevel()));
				Map<String, Object> val = objectToMap(alerts);
				String pattern = "yyyy-MM-dd HH:mm:ss";
				for (Map.Entry<String, Object> entry: val.entrySet()) {
					Object value = entry.getValue();
					if (value == null) {
						continue;
					}
					if (value instanceof Date) {
						Date date = (Date) value;
						val.put(entry.getKey(), DateUtil.format(date, pattern));
					}
				}
				dataLists.add(val);
			}

		} else {
			List<AlertsDTO> pageResult =  alertsIndexPageBiz.getLatest(limit,idcType,operateStatus,startDate,endDate);
			for (AlertsDTO alertsDTO : pageResult) {
				alertsDTO.setAlertLevel(AlertCommonConstant.alertLevelMap.get(alertsDTO.getAlertLevel()));
				Map<String, Object> val = objectToMap(alertsDTO);
				String pattern = "yyyy-MM-dd HH:mm:ss";
				if(null!=val.get("alertStartTime")) {
					Date start = (Date)val.get("alertStartTime");
					val.put("alertStartTime", DateUtil.format(start, pattern));
				}
				if(null!=val.get("curMoniTime")) {
					Date start = (Date)val.get("curMoniTime");
					val.put("curMoniTime", DateUtil.format(start, pattern));
				}

				dataLists.add(val);
			}
		}

        return dataLists;
    }
    
    private Map<String, Object> objectToMap(Object obj) throws Exception {
        if(obj == null){
            return null;
        }
        Map<String, Object> map = new HashMap<String, Object>();
        Field[] declaredFields = obj.getClass().getDeclaredFields();
        for (Field field : declaredFields) {
            field.setAccessible(true);
            map.put(field.getName(), field.get(obj));
        }
        return map;
    }

	/*****************************************************
	 * 监控首页
	 ************************************/

	
	@Override
	public Map<String, Object> StorageUseRate() {
		return alertStorageUseRateDao.getStorageUserate();
	}
	
	
	@Override
	public List<Map<String,Object>> selectAlertsByOperateStatus(@RequestParam(value = "operateStatus", required = false) Integer operateStatus,
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate,
			@RequestParam(value = "colType", required = false) String colType) {
		if(StringUtils.isBlank(startDate)) {
			Date now = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			endDate = sdf1.format( now);
			startDate = sdf.format(now)+" 00:00:00";
			//startDate = "2020-05-01";
		}
		if (AlertConfigConstants.BRANCH_IT_YUN.equalsIgnoreCase(branch)) {
			if(colType.equals("idcType")) {
				return alertsIndexPageBiz.selectIdcTypeAlertsByOperateStatusOrder(operateStatus,null,null);
			}else {
				return alertsIndexPageBiz.selectDeviceTypeAlertsByOperateStatusOrder(operateStatus,null,null);
			}
		} else {
			if(colType.equals("idcType")) {
				return alertsIndexPageBiz.selectIdcTypeAlertsByOperateStatus(operateStatus,startDate,endDate);
			}else {
				return alertsIndexPageBiz.selectDeviceTypeAlertsByOperateStatus(operateStatus,startDate,endDate);
			}
		}

		
	}
	@Override
	public Map<String, Object> getUserRateForZH(@RequestBody AlertEsDataRequest request) throws ParseException {
		Map<String, List<String>> resFilterMap = RequestAuthContext.getRequestHeadUser().getResFilterConfig();
		Map<String,String> map = authHelper.packSqlConditionByType(resFilterMap, 3);
		
		
		String month = request.getMonth();
		if (StringUtils.isBlank(month)) {
			log.warn("运营月报资源池利用率,月份不能为空！");
			return null;
		}
		List<Map<String,Object>> monthData = alertMonthReportSyncDao.queryMonthUseRateForZH(request.getMonth(),
				map, request.getDeviceType());
		if(null==monthData || monthData.size()==0) {
			return null;
		}
		
		Map<String, Object> monthMap = getMonthData(monthData,request.getDeviceType());
		
		String lastMonth = DateUtils.getLastMonth(month);
		
		List<Map<String,Object>> lastMonthData = alertMonthReportSyncDao.queryMonthUseRateForZH(lastMonth,
				map, request.getDeviceType());
		Map<String, Object> lastMonthMap = getMonthData(lastMonthData,request.getDeviceType());
		
		Float cpuAvg = monthMap.get("cpu_avg")==null?null:Float.parseFloat(monthMap.get("cpu_avg").toString());
		Float memAvg = monthMap.get("memory_avg")==null?null:Float.parseFloat(monthMap.get("memory_avg").toString());
		Float cpuMax = monthMap.get("cpu_max")==null?null:Float.parseFloat(monthMap.get("cpu_max").toString());
		Float memMax = monthMap.get("memory_max")==null?null:Float.parseFloat(monthMap.get("memory_max").toString());
		monthMap.put("last_null_flag", false);
		if(null==lastMonthData || lastMonthData.size()==0) {
			monthMap.put("last_null_flag", true);
			return monthMap;
		}
		if(null!=cpuAvg) {
			Float lastCpuAvg = lastMonthMap.get("cpu_avg")==null?null:Float.parseFloat(lastMonthMap.get("cpu_avg").toString());
			if(null!=lastCpuAvg) {
				float compareVal = getCompareVal(cpuAvg,lastCpuAvg);
				monthMap.put("cpu_avg", (float)Math.round(cpuAvg*100)/100);
				monthMap.put("compare_cpu_avg", compareVal);
			}
		}
		if(null!=memAvg) {
			Float lastMemAvg = lastMonthMap.get("memory_avg")==null?null:Float.parseFloat(lastMonthMap.get("memory_avg").toString());
			if(null!=lastMemAvg) {
				float compareVal = getCompareVal(memAvg,lastMemAvg);
				monthMap.put("memory_avg", (float)Math.round(memAvg*100)/100);
				monthMap.put("compare_memory_avg", compareVal);
			}
		}
		if(null!=cpuMax) {
			Float lastCpuMax = lastMonthMap.get("cpu_max")==null?null:Float.parseFloat(lastMonthMap.get("cpu_max").toString());
			if(null!=lastCpuMax) {
				float compareVal = getCompareVal(cpuMax,lastCpuMax);
				monthMap.put("cpu_max", (float)Math.round(cpuMax*100)/100);
				monthMap.put("compare_cpu_max", compareVal);
			}
		}
		if(null!=memMax) {
			Float lastMemMax =  lastMonthMap.get("memory_max")==null?null:Float.parseFloat(lastMonthMap.get("memory_max").toString());
			if(null!=lastMemMax) {
				float compareVal = getCompareVal(memMax,lastMemMax);
				monthMap.put("memory_max", (float)Math.round(memMax*100)/100);
				monthMap.put("compare_memory_max", compareVal);
			}
		}
		return monthMap;
	}
	
	public float getCompareVal(float cpuAvg,float lastCpuAvg){
		float compareVal = cpuAvg-lastCpuAvg;
		/*if(lastCpuAvg>0) {
			compareVal = (float)compareVal*100/(float)lastCpuAvg;
		}*/
		compareVal = (float)Math.round(compareVal*100)/100;
		return compareVal;
	}

	private Map<String, Object> getMonthData(List<Map<String, Object>> monthData,String deviceType) {
		List<String> bizList = Lists.newArrayList();
		Map<String, Map<String, Object>> bizMap = Maps.newHashMap();
		for(Map<String, Object> map:monthData) {
			String bizSystem = map.get("bizSystem")==null?null:map.get("bizSystem").toString();
			 if(StringUtils.isBlank(bizSystem)) {
				 continue;
			 } 
			 bizList.add(bizSystem);
			 bizMap.put(bizSystem, map);
		}
		if(bizList.size()==0) {
			return null;
		}
		StatisticRequestEntity entity = new StatisticRequestEntity();
		entity.setName(this.cmdbQueryName);
		 Map<String, Object> params = Maps.newHashMap();
		 params.put("device_type", deviceType);
		 params.put("bizSystem", bizList);
		entity.setParams(params);
		entity.setResponseType("list");
	  Object	value = cmdbResfulClient.getInstanceStatistics(entity);
	  
		if(null!=value) {
			Map<String,Object> map = Maps.newHashMap();
			int sum = 0;
			Float cpuAvgSum = 0f;
			Float cpuMaxSum = 0f;
			Float memoryAvgSum = 0f;
			Float memoryMaxSum = 0f;
			List<Map<String,Object>> dataList = (List<Map<String,Object>>)value;
			/*
			 * Map<String,Object> a = Maps.newHashMap(); a.put("bizSystem", "监控智能运营工具");
			 * a.put("instance_count", 3490); dataList.add(a); Map<String,Object> b =
			 * Maps.newHashMap(); b.put("bizSystem", "批量部署工具"); b.put("instance_count", 35);
			 * dataList.add(b);
			 */
			
			for(Map<String,Object> m:dataList) {
				String biz =m.get("bizSystem")==null?null:m.get("bizSystem").toString();
				 if(StringUtils.isBlank(biz)) {
					 continue;
				 }
				 Map<String, Object> monthMap = bizMap.get(biz);
				 if(null==monthMap) {
					 continue;
				 }
				 String cpuAvgValue = monthMap.get("cpu_avg")==null?null:monthMap.get("cpu_avg").toString();
				 if(StringUtils.isEmpty(cpuAvgValue)) {
					 continue;
				 }
				 Float cpuAvg = Float.parseFloat(cpuAvgValue);
					Float memAvg = monthMap.get("memory_avg")==null?null:Float.parseFloat(monthMap.get("memory_avg").toString());
					Float cpuMax = monthMap.get("cpu_max")==null?null:Float.parseFloat(monthMap.get("cpu_max").toString());
					Float memMax = monthMap.get("memory_max")==null?null:Float.parseFloat(monthMap.get("memory_max").toString());
				int count = m.get("instance_count")==null?null:Integer.parseInt(m.get("instance_count").toString());
				sum +=count;
				cpuAvgSum += cpuAvg*count;
				cpuMaxSum += cpuMax*count;
				memoryAvgSum += memAvg*count;
				memoryMaxSum += memMax*count;
				
			}
			map.put("cpu_avg", cpuAvgSum/sum);
			map.put("memory_avg", memoryAvgSum/sum);
			map.put("cpu_max", cpuMaxSum/sum);
			map.put("memory_max", memoryMaxSum/sum);
			return map;
		}
		return null;
	}

	public List<Map<String, Object>> alertIdcDoHours(@RequestParam(value = "alertLevel") String alertLevel) {
		return alertsIndexPageBiz.alertIdcDoHours(alertLevel);
	}

}
