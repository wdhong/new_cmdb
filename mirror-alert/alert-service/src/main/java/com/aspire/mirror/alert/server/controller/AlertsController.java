package com.aspire.mirror.alert.server.controller;

import com.aspire.mirror.alert.api.dto.*;
import com.aspire.mirror.alert.api.service.AlertsService;
import com.aspire.mirror.alert.server.biz.AlertsBiz;
import com.aspire.mirror.alert.server.biz.AlertsHandleHelper;
import com.aspire.mirror.alert.server.biz.IAlertDataListSyncBiz;
import com.aspire.mirror.alert.server.biz.ItemEventCallBackHandleChain;
import com.aspire.mirror.alert.server.biz.helper.CmdbHelper;
import com.aspire.mirror.alert.server.biz.model.AlertBpmStartCallBack;
import com.aspire.mirror.alert.server.dao.po.AlertsDetail;
import com.aspire.mirror.alert.server.dao.po.AlertsNotify;
import com.aspire.mirror.alert.server.dao.po.AlertsRecord;
import com.aspire.mirror.alert.server.domain.AlertsDTO;
import com.aspire.mirror.alert.server.domain.AlertsOperationRequestDTO;
import com.aspire.mirror.alert.server.domain.NotifyPageDTO;
import com.aspire.mirror.alert.server.util.TransformUtils;
import com.aspire.mirror.common.entity.PageRequest;
import com.aspire.mirror.common.entity.PageResponse;
import com.aspire.mirror.common.util.FieldUtil;
import com.aspire.ums.cmdb.instance.payload.CmdbInstance;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.aspire.mirror.alert.server.util.PayloadParseUtil.jacksonBaseParse;

/**
 * 告警控制层
 * <p>
 * 项目名称:  mirror平台
 * 包:        com.aspire.mirror.alert.server.controller
 * 类名称:    AlertsController.java
 * 类描述:    告警控制层
 * 创建人:    JinSu
 * 创建时间:  2018/9/14 17:51
 * 版本:      v1.0
 */
@RestController
public class AlertsController implements AlertsService {


    private static final Logger LOGGER = LoggerFactory.getLogger(AlertsController.class);

    @Autowired
    private AlertsBiz alertsBiz;
    @Autowired
    private AlertsHandleHelper alertHandleHelper;
    @Autowired
    private ItemEventCallBackHandleChain callBackHandlerChain;
    @Autowired
    private IAlertDataListSyncBiz alertDataListSyncBiz;
    @Autowired
    private CmdbHelper cmdbHelper;

    /**
     * 创建告警
     *
     * @param createRequest 创建对象
     * @return AlertsCreateResponse 告警创建返回
     */
    @Override
    public AlertsCreateResponse createdAlerts(@RequestBody AlertsCreateRequest createRequest) {
        if (createRequest == null) {
            LOGGER.error("created param createRequest is null");
            throw new RuntimeException("createRequest is null");
        }
        AlertsDTO alertsDTO = new AlertsDTO();
        BeanUtils.copyProperties(createRequest, alertsDTO);
        String alertId = alertsBiz.insert(alertsDTO);
        AlertsCreateResponse createResponse = new AlertsCreateResponse();
        createResponse.setAlertId(alertId);
        return createResponse;
    }

    @Override
    public String upgrade(@RequestParam("old_order_id") String oldOrderId,
                          @RequestParam("order_id") String orderId,
                          @RequestParam("type") String type,
                          @RequestParam("order_status") String orderStatus,
                          @RequestParam("user_name") String userName) {
        LOGGER.info("call upgrade oldOrderId is {}, orderId is {}, orderStatus is {}",oldOrderId,orderId,orderStatus);
        if (!org.apache.commons.lang3.StringUtils.isNumeric(type)) {
            return "{\"status\":\"error\",\"message\":\"type is not number\"}";
        }
        String status = alertsBiz.upgrade(oldOrderId, orderId, type,orderStatus,userName);
        return "{\"status\":\"" + status + "\"}";
    }

    /**
     * 告警列表
     *
     * @param pageRequset 查询page对象
     * @return PageResponse 列表返回对象
     */
    @Override
    public PageResponse<AlertsDetailResponse> pageList(@RequestBody AlertsPageRequset pageRequset) {
        if (pageRequset == null) {
            LOGGER.warn("pageList param pageRequset is null");
            return null;
        }
        PageRequest page = new PageRequest();
        BeanUtils.copyProperties(pageRequset, page);
        Map<String, Object> map = FieldUtil.getFiledMap(pageRequset);
        for (String key : map.keySet()) {
            page.addFields(key, map.get(key));
        }
        PageResponse<AlertsDTO> pageResult = alertsBiz.pageList(page);
        List<AlertsDetailResponse> listAlert = Lists.newArrayList();
        if (!CollectionUtils.isEmpty(pageResult.getResult())) {
            for (AlertsDTO alertsDTO : pageResult.getResult()) {
                AlertsDetailResponse alertsDetailResponse = new AlertsDetailResponse();
                BeanUtils.copyProperties(alertsDTO, alertsDetailResponse);
                listAlert.add(alertsDetailResponse);
            }
        }
        PageResponse<AlertsDetailResponse> result = new PageResponse<AlertsDetailResponse>();
        result.setCount(pageResult.getCount());
        result.setResult(listAlert);
        return result;
    }

//    /**
//     * 删除告警
//     * @param alertIds 告警id集合，逗号分隔
//     * @return ResponseEntity 返回
//     */
//    @Override
//    public ResponseEntity<String> deleteByPrimayKeyArrays(@PathVariable("alert_ids") String alertIds) {
//        try {
//            if (StringUtils.isEmpty(alertIds)) {
//                throw new RuntimeException("alertIds is null");
//            }
//            String[] alertIdArrays = alertIds.split(",");
//            alertsBiz.deleteByPrimaryKeyArrays(alertIdArrays);
//            return new ResponseEntity<String>(HttpStatus.NO_CONTENT);
//        } catch (Exception e) {
//            LOGGER.error("deleteByPrimaryKeyArrays error:" + e.getMessage(), e);
//            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }


    /**
     * 告警批量创建请求
     *
     * @param batchCreateRequest 批量创建请求
     * @return
     */
    @Override
    public List<AlertsCreateResponse> createBatch(@RequestBody AlertsBatchCreateRequest batchCreateRequest) {
        if (null == batchCreateRequest) {
            LOGGER.error("created param batchCreateRequest is null");
            throw new RuntimeException("batchCreateRequest is null");
        }
        if (CollectionUtils.isEmpty(batchCreateRequest.getAlertList())) {
            LOGGER.error("created param object batchCreateItems is empty");
            throw new RuntimeException("batchCreateItems param is empty");
        }
        List<AlertsDTO> alertsDTOList = Lists.newArrayList();

        for (AlertsCreateRequest createRequest : batchCreateRequest.getAlertList()) {
            AlertsDTO alertsDTO = new AlertsDTO();
            BeanUtils.copyProperties(createRequest, alertsDTO);
            alertsDTOList.add(alertsDTO);
        }
        List<AlertsDTO> respListDTO = alertsBiz.insertByBatch(alertsDTOList);
        List<AlertsCreateResponse> resp = Lists.newArrayList();
        for (AlertsDTO alertsDTO : respListDTO) {
            AlertsCreateResponse createResponse = new AlertsCreateResponse();
            BeanUtils.copyProperties(alertsDTO, createResponse);
            resp.add(createResponse);
        }
        return resp;
    }

    /**
     * 告警转派
     *
     * @param alertsOperationRequest 人员id集合，逗号分隔
     * @return ResponseEntity 返回
     */
    @Override
    public ResponseEntity<String> alertTransfer(@RequestBody AlertsTransferRequest alertsOperationRequest) {

        if (StringUtils.isEmpty(alertsOperationRequest.getAlertIds())) {
            LOGGER.error("listByPrimaryKeyArrays param alertIds is null");
            return null;
        }

        if (StringUtils.isEmpty(alertsOperationRequest.getUserNames())) {
            LOGGER.error("listByPrimaryKeyArrays param userNames is null");
            return null;
        }

        String namespace = alertsOperationRequest.getNamespace();
        String alertIds = alertsOperationRequest.getAlertIds();
        String userNames = alertsOperationRequest.getUserNames();

        alertsBiz.alertTransfer(namespace, alertIds, userNames);

        return new ResponseEntity<String>("success", HttpStatus.OK);

    }

    /**
     * 告警确认
     *
     * @param alertsOperationRequest 告警id集合，逗号分隔
     * @return ResponseEntity 返回
     */
    @Override
    public ResponseEntity<String> alertConfirm(@RequestBody AlertsConfirmRequest alertsOperationRequest) {

        if (StringUtils.isEmpty(alertsOperationRequest.getAlertIds())) {
            LOGGER.error("listByPrimaryKeyArrays param alertIds is null");
            return null;
        }
//        String namespace = alertsOperationRequest.getNamespace();
//        String alertIds = alertsOperationRequest.getAlertIds();
//        String content = alertsOperationRequest.getContent();

        alertsBiz.alertConfirm(jacksonBaseParse(AlertsOperationRequestDTO.class, alertsOperationRequest));

        return new ResponseEntity<String>("success", HttpStatus.OK);
    }


    /**
     * 告警详情
     *
     * @param alertId 告警Id
     * @return
     */
    @Override
    public AlertSecondDetailResp findAlertByPrimaryKey(@PathVariable("alert_id") String alertId) {
        if (StringUtils.isEmpty(alertId)) {
            LOGGER.warn("findByPrimaryKey param alertId is null");
            return null;
        }

        LOGGER.info("alertId is {} ", alertId);

        String[] alertIdarray = alertId.split(",");

        int length = alertIdarray.length;
        String alertIda = alertIdarray[length - 1];

        AlertsDTO alertsDTO = alertsBiz.selectAlertByPrimaryKey(alertIda);

        AlertSecondDetailResp alertSecondDetailResp = new AlertSecondDetailResp();

        if (null == alertsDTO) {

            return null;
        }

        BeanUtils.copyProperties(alertsDTO, alertSecondDetailResp);

//        if ("1".equals(alertSecondDetailResp.getObjectType())) {
//            Map<String, Object> deviceInfo = cmdbHelper.queryDeviceByRoomIdAndIP(alertSecondDetailResp.getIdcType(), alertSecondDetailResp.getDeviceIp());
//            if (deviceInfo != null) {
//                alertSecondDetailResp.setDeviceDescription(deviceInfo.getDeviceDescription());
//                alertSecondDetailResp.setDeviceId(deviceInfo.getId());
//                alertSecondDetailResp.setIdcCabinet(deviceInfo.getIdcCabinet());
//            }
//        }
        return alertSecondDetailResp;
    }

    /**
     * 告警上报分页
     *
     * @param alertId 告警Id
     * @return
     */
    @Override
    public PageResponse<AlertGenResp> alertGenerateList(@RequestParam("alert_id") String alertId,
                                                        @RequestParam("page_no") String pageNo,
                                                        @RequestParam("page_size") String pageSize) {

        LOGGER.info("alertId is {} ", alertId);
        LOGGER.info("alertId is {} ", pageNo);
        LOGGER.info("alertId is {} ", pageSize);

        PageResponse<AlertsDetail> alertGenPage = alertsBiz.alertGenerateListByPage(alertId, pageNo, pageSize);

        List<AlertGenResp> alertGenRespList = new ArrayList<AlertGenResp>();
        for (AlertsDetail alertsDetail : alertGenPage.getResult()) {

            AlertGenResp alertGenResp = new AlertGenResp();

            BeanUtils.copyProperties(alertsDetail, alertGenResp);

            alertGenRespList.add(alertGenResp);
        }
        PageResponse<AlertGenResp> alertGenRespPageResponse = new PageResponse<>();
        alertGenRespPageResponse.setCount(alertGenPage.getCount());
        alertGenRespPageResponse.setResult(alertGenRespList);
        return alertGenRespPageResponse;
    }

    /**
     * 告警操作分页
     *
     * @param alertId 告警Id
     * @return
     */
    @Override
    public PageResponse<AlertRecordResp> alertRecordList(@RequestParam("alert_id") String alertId,
                                                         @RequestParam("page_no") String pageNo,
                                                         @RequestParam("page_size") String pageSize) {
        LOGGER.info("alertId is {} ", alertId);
        LOGGER.info("alertId is {} ", pageNo);
        LOGGER.info("alertId is {} ", pageSize);

        PageResponse<AlertsRecord> alertRecordPage = alertsBiz.alertRecordListByPage(alertId, pageNo, pageSize);

        List<AlertRecordResp> alertRecordRespList = new ArrayList<AlertRecordResp>();

        for (AlertsRecord alertsRecord : alertRecordPage.getResult()) {

            AlertRecordResp alertRecordResp = new AlertRecordResp();

            BeanUtils.copyProperties(alertsRecord, alertRecordResp);

            alertRecordRespList.add(alertRecordResp);
        }
        PageResponse<AlertRecordResp> alertRecordRespPageResponse = new PageResponse<>();
        alertRecordRespPageResponse.setCount(alertRecordPage.getCount());
        alertRecordRespPageResponse.setResult(alertRecordRespList);
        return alertRecordRespPageResponse;


    }

    /**
     * 告警通知分页
     *
     * @param alertId 告警Id
     * @return
     */
    @Override
    public NotifyPageResponse<AlertNotifyResp> alertNotifyList(@RequestParam("alert_id") String alertId,
                                                               @RequestParam("page_no") String pageNo,
                                                               @RequestParam("page_size") String pageSize,
                                                               @RequestParam("report_type") String reportType) {
        LOGGER.info("alertId is {} ", alertId);
        LOGGER.info("alertId is {} ", pageNo);
        LOGGER.info("alertId is {} ", pageSize);
        LOGGER.info("reportType is {} ", reportType);


        NotifyPageDTO<AlertsNotify> alertsNotifyNotifyPageDTO = alertsBiz.alertNotifyListByPage(alertId, pageNo, pageSize, reportType);

        List<AlertNotifyResp> alertNotifyRespList = new ArrayList<AlertNotifyResp>();
        for (AlertsNotify alertsNotify : alertsNotifyNotifyPageDTO.getResult()) {
            AlertNotifyResp alertNotifyResp = new AlertNotifyResp();
            BeanUtils.copyProperties(alertsNotify, alertNotifyResp);
            alertNotifyRespList.add(alertNotifyResp);
        }
        NotifyPageResponse<AlertNotifyResp> notifyRespNotifyPageResponse = new NotifyPageResponse<>();
        notifyRespNotifyPageResponse.setCount(alertsNotifyNotifyPageDTO.getCount());
        notifyRespNotifyPageResponse.setFallCount(alertsNotifyNotifyPageDTO.getFallCount());
        notifyRespNotifyPageResponse.setSuccessCount(alertsNotifyNotifyPageDTO.getSuccessCount());
        notifyRespNotifyPageResponse.setResult(alertNotifyRespList);
        return notifyRespNotifyPageResponse;


    }


    //告警上报记录excel 下载
    @Override
    public List<AlertGenResp> alertGenerateDownload(@RequestParam("alert_id") String alertId) {

        LOGGER.info("alertId is {} ", alertId);

        List<AlertsDTO> alertsList = alertsBiz.selectAlertGenerateList(alertId);

        List<AlertGenResp> alertGenRespList = new ArrayList<AlertGenResp>();
        for (AlertsDTO alertsDTO : alertsList) {
            AlertGenResp alertGenResp = new AlertGenResp();
            BeanUtils.copyProperties(alertsDTO, alertGenResp);

            alertGenRespList.add(alertGenResp);
        }

        return alertGenRespList;
    }


    //告警操作记录excel 下载
    @Override
    public List<AlertRecordResp> alertRecordDownload(@RequestParam("alert_id") String alertId) {

        LOGGER.info("alertId is {} ", alertId);

        List<AlertsRecord> alertsRecordList = alertsBiz.selectAlertRecordByPrimaryKey(alertId);
        List<AlertRecordResp> alertRecordRespList = new ArrayList<AlertRecordResp>();
        for (AlertsRecord alertsRecord : alertsRecordList) {
            AlertRecordResp alertRecordResp = new AlertRecordResp();
            BeanUtils.copyProperties(alertsRecord, alertRecordResp);

            alertRecordRespList.add(alertRecordResp);
        }

        return alertRecordRespList;
    }


    //告警通知记录excel 下载
    @Override
    public List<AlertNotifyResp> alertNotifyDownload(@RequestParam("alert_id") String alertId) {

        LOGGER.info("alertId is {} ", alertId);

        List<AlertsNotify> AlertsNotifyList = alertsBiz.selectalertNotifyByPrimaryKey(alertId);
        List<AlertNotifyResp> alertNotifyRespList = new ArrayList<AlertNotifyResp>();
        for (AlertsNotify alertsNotify : AlertsNotifyList) {

            AlertNotifyResp alertNotifyResp = new AlertNotifyResp();
            BeanUtils.copyProperties(alertsNotify, alertNotifyResp);
            alertNotifyRespList.add(alertNotifyResp);
        }
        return alertNotifyRespList;
    }


    //修改告警备注
    @Override
    public ResponseEntity<String> updateNote(@PathVariable("alert_id") String alertId, @RequestParam("note") String note) {

        if (StringUtils.isEmpty(alertId)) {
            LOGGER.warn("updateNote param alertId is null");
            return null;
        }
        if (StringUtils.isEmpty(note)) {
            LOGGER.warn("updateNote param note is null");
            return null;
        }

        alertsBiz.updateNote(alertId, note);

        return new ResponseEntity<String>("success", HttpStatus.OK);
    }


    /**
     * 根据告警ID集合查询结果
     *
     * @param alertIds 模板主键(多个以逗号分隔)
     * @return
     */
    @Override
    public List<AlertsDetailResponse> listByPrimaryKeyArrays(@PathVariable("alert_ids") String alertIds) {
        if (StringUtils.isEmpty(alertIds)) {
            LOGGER.error("listByPrimaryKeyArrays param alertIds is null");
            return Lists.newArrayList();
        }
        String[] alertIdArrays = alertIds.split(",");
        return TransformUtils.transform(AlertsDetailResponse.class, alertsBiz.selectByPrimaryKeyArrays(alertIdArrays));
    }

    @Override
    public List<AlertSyncResponseItem> syncAlertDataList(@RequestBody AlertDataListSyncRequest request) {
        return alertDataListSyncBiz.handleAlertDataListSync(request);
    }


    //告警清除
    @Override
    public ResponseEntity<String> alertRemove(@RequestBody AlertsClearRequest alertsOperationRequest) {

        if (StringUtils.isEmpty(alertsOperationRequest.getAlertIds())) {
            LOGGER.error("alertRemove param alert_ids is null");
            return null;
        }
        alertHandleHelper.manualClear(jacksonBaseParse(AlertsOperationRequestDTO.class, alertsOperationRequest));
        return new ResponseEntity<String>("success", HttpStatus.OK);

    }


    /**
     * 告警工单
     *
     * @param alertsOperationRequest 告警ID集合
     * @return
     */
    @Override
    public ResponseEntity<String> alertOrder(@RequestBody AlertsOrderRequest alertsOperationRequest) {
        String successNum = "0";
        try {
            if (StringUtils.isEmpty(alertsOperationRequest.getAlertIds())) {
                throw new RuntimeException("alertIds is null");
            }

            String namespace = alertsOperationRequest.getNamespace();
            String alertIds = alertsOperationRequest.getAlertIds();
            LOGGER.info("111111method[alertOrder] param is {}",alertsOperationRequest);
            Integer orderType = alertsOperationRequest.getOrderType();
            AlertBpmStartCallBack callBack = alertsBiz.switchOrder(namespace, alertIds,orderType);
            if (callBack.isStatus()){
                successNum = callBack.getSuccess() + "";
            }else {
                return new ResponseEntity<String>(callBack.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
            }
//            if (org.apache.commons.lang.StringUtils.isNotEmpty(message)) {
//                return new ResponseEntity<String>(message, HttpStatus.INTERNAL_SERVER_ERROR);
//            }

        } catch (Exception e) {
            LOGGER.error("alertOrder error:" + e.getMessage(), e);

            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        System.out.println("==============================="+new ResponseEntity<String>("success:"+successNum, HttpStatus.OK));
        return new ResponseEntity<String>("success:"+successNum, HttpStatus.OK);
    }

    /**
     * 告警发送邮件
     */
    @Override
    public ResponseEntity<String> alertemailNotify(@RequestBody AlertsOperationRequest alertsOperationRequest) {

        if (StringUtils.isEmpty(alertsOperationRequest.getAlertIds())) {
            throw new RuntimeException("alertIds is null");
        }

        String namespace = alertsOperationRequest.getNamespace();
        String alertIds = alertsOperationRequest.getAlertIds();

        String[] destination = alertsOperationRequest.getDestination();

        String message = alertsOperationRequest.getMessage();

        String status = alertsOperationRequest.getStatus();

        alertsBiz.recordEmailNotify(namespace, alertIds, destination, message, status);

        return new ResponseEntity<String>("success", HttpStatus.OK);

    }


    /**
     * 手动发送短信
     */
    @Override
    public ResponseEntity<String> recordSMSNotify(@RequestBody AlertsOperationRequest alertsOperationRequest) {
        if (StringUtils.isEmpty(alertsOperationRequest.getAlertIds())) {
            throw new RuntimeException("alertIds is null");
        }
        String namespace = alertsOperationRequest.getNamespace();
        String alertIds = alertsOperationRequest.getAlertIds();
        String[] destination = alertsOperationRequest.getDestination();

        String message = alertsOperationRequest.getMessage();
        String status = alertsOperationRequest.getStatus();

        alertsBiz.recordSMSNotify(namespace, alertIds, destination, message, status);
        return new ResponseEntity<String>("success", HttpStatus.OK);

    }


    @Override
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void onItemMonitorEventCallBack(@RequestBody ItemMonitorEventCallBackRequest monitorEvent) {
        callBackHandlerChain.handle(monitorEvent);
    }

    /**
     * 根据工单ID修改状态
     *
     * @param orderId     工单ID
     * @param orderStatus 工单状态
     */
    @Override
    public void modOrderStatusByOrderId(@PathVariable("order_id") String orderId,
                                        @PathVariable("status") String orderStatus) {
        LOGGER.info("call modOrderStatusByOrderId order_id is {}, status is {}!",orderId, orderStatus);
        if (StringUtils.isEmpty(orderId) || StringUtils.isEmpty(orderStatus)) {
            LOGGER.warn("AlertsController[modOrderStatusByOrderId] param validation is failed");
        } else {
            alertsBiz.modOrderStatusByOrderId(orderId, orderStatus);
        }
    }

    @Override
    public int getAlertValue(@RequestBody @Validated AlertValueSearchRequest alertValueSearchRequest) {
        int result = alertsBiz.getAlertValue(alertValueSearchRequest.getIpMap(), alertValueSearchRequest
                .getAlertLevel(), alertValueSearchRequest.getItemIdList());
        return result;
    }

}
