package com.aspire.mirror.alert.server.controller;

import com.aspire.mirror.alert.api.service.AlertsMonReportService;
import com.aspire.mirror.alert.server.biz.AlertMonReportBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class AlertsMonReportController implements AlertsMonReportService {

    @Autowired
    private AlertMonReportBiz alertMonReportBiz;

    @Override
    public Map<String,Object> viewByIdcType(@RequestBody Map<String,String> map) {
        return alertMonReportBiz.viewByIdcType(map);
    }

    @Override
    public List<Map<String, Object>> viewByIp(@RequestBody Map<String,String> map) {
        return alertMonReportBiz.viewByIp(map);
    }

    @Override
    public List<Map<String, Object>> viewByKeyComment(@RequestBody Map<String,String> map) {
        return alertMonReportBiz.viewByKeyComment(map);
    }
}
