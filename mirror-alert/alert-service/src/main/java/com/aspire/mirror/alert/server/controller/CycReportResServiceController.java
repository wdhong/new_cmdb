package com.aspire.mirror.alert.server.controller;

import com.alibaba.fastjson.JSONObject;
import com.aspire.mirror.alert.api.dto.CycReportResFileRequest;
import com.aspire.mirror.alert.api.service.CycReportResService;
import com.aspire.mirror.alert.server.biz.CycReportResourceBiz;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@RestController
public class CycReportResServiceController implements CycReportResService {
    private static org.slf4j.Logger logger = LoggerFactory.getLogger(CycReportResServiceController.class);
    @Autowired
    private CycReportResourceBiz cycReportResourceBiz;
    /**
     * 保存数据文件名元数据到数据库
     *
     * @param filelist
     * @return AlertsSummaryResponse 告警概览
     */
    @Override
    public void saveReportFtpFiles(@RequestBody List<CycReportResFileRequest> filelist) {
        logger.info("#=====> fileList: {}", JSONObject.toJSONString(filelist));
        cycReportResourceBiz.insertFtpFiles(filelist);
    }
}
