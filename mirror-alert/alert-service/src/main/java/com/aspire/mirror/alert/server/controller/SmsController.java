package com.aspire.mirror.alert.server.controller;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.aspire.mirror.alert.api.dto.SmsListParam;
import com.aspire.mirror.alert.api.dto.SmsRecordExportVo;
import com.aspire.mirror.alert.api.dto.SmsRecordReq;
import com.aspire.mirror.alert.api.dto.SmsTemplateDetailVo;
import com.aspire.mirror.alert.api.dto.SmsTemplateVo;
import com.aspire.mirror.alert.api.service.SmsService;
import com.aspire.mirror.alert.server.aspect.RequestAuthContext;
import com.aspire.mirror.alert.server.biz.SmsBiz;
import com.aspire.mirror.alert.server.dao.po.SmsRecord;
import com.aspire.mirror.alert.server.dao.po.SmsTemplate;
import com.aspire.mirror.alert.server.dao.po.SmsTemplateDetail;
import com.aspire.mirror.alert.server.domain.SmsRecordExportVoDTO;
import com.aspire.mirror.alert.server.domain.SmsRecordReqDTO;
import com.aspire.mirror.alert.server.domain.SmsTemplateVoDTO;
import com.aspire.mirror.alert.server.util.PayloadParseUtil;
import com.aspire.mirror.common.entity.PageResponse;

/**
 * @author menglinjie
 */
@RestController
public class SmsController implements SmsService {

    @Resource
    private SmsBiz smsBiz;


    @Override
    public Map<String, Object> sendSms(@RequestBody SmsRecordReq req) {
    	RequestAuthContext authCtx = RequestAuthContext.currentRequestAuthContext();
	    String account = authCtx.getUser().getUsername();
    	req.setSenderUuid(account);
        return smsBiz.sendSms(PayloadParseUtil.jacksonBaseParse(SmsRecordReqDTO.class,req));
    }

    @Override
    public PageResponse<SmsRecordReq> findSmsList(@RequestBody SmsListParam param) throws ParseException {
        PageResponse<SmsRecord>  smsRecordPage = smsBiz.findSmsList(param.getStartTime(),param.getEndTime(),param.getReceiver(),
        		param.getContent(),param.getStatus(),param.getPageNo(),param.getPageSize());
        List<SmsRecord> smsRecordList = smsRecordPage.getResult();
        List<SmsRecordReq> recordReqList = PayloadParseUtil.jacksonBaseParse(SmsRecordReq.class,smsRecordList);
        PageResponse<SmsRecordReq>  smsRecordReqPage = new PageResponse<>();
        smsRecordReqPage.setCount(smsRecordPage.getCount());
        smsRecordReqPage.setResult(recordReqList);
        return smsRecordReqPage;
    }

    @Override
    public Map<String, Object> addSmsTemplate(@RequestBody List<SmsTemplateVo> voList) {
    	Map<String, Object> map = new HashMap<>();
    	voList.forEach(it -> smsBiz.addSmsTemplate(PayloadParseUtil.jacksonBaseParse(SmsTemplateVoDTO.class,it)));
    	map.put("state","success");
        return map;
    }

    @Override
    public PageResponse<SmsTemplateVo> findSmsTemplateList(Integer pageNo, Integer pageSize) throws ParseException {
        PageResponse<SmsTemplate>  smsTemplatePage = smsBiz.findSmsTemplateList(pageNo,pageSize);
        List<SmsTemplate> smsTemplateList = smsTemplatePage.getResult();
        List<SmsTemplateVo> templateReqList = PayloadParseUtil.jacksonBaseParse(SmsTemplateVo.class,smsTemplateList);
        PageResponse<SmsTemplateVo>  smsTemplateVoPage = new PageResponse<>();
        smsTemplateVoPage.setCount(smsTemplatePage.getCount());
        smsTemplateVoPage.setResult(templateReqList);
        return smsTemplateVoPage;
    }

    @Override
    public Map<String, Object> addContentToTemplate(@RequestBody SmsTemplateVo vo) {
        return smsBiz.addContentToTemplate(PayloadParseUtil.jacksonBaseParse(SmsTemplateVoDTO.class,vo));
    }
    
    @Override
	public Map<String, Object> editTemplate(@RequestBody SmsTemplateVo vo) {
    	return smsBiz.editTemplate(PayloadParseUtil.jacksonBaseParse(SmsTemplateVoDTO.class,vo));
	}
    
    @Override
	public Map<String, Object> editTemplateContent(@RequestBody SmsTemplateVo vo) {
    	return smsBiz.editTemplateContent(PayloadParseUtil.jacksonBaseParse(SmsTemplateVoDTO.class,vo));
	}

	@Override
	public Map<String, Object> deleteTemplateContent(@RequestBody SmsTemplateVo vo) {
		return smsBiz.deleteTemplateContent(PayloadParseUtil.jacksonBaseParse(SmsTemplateVoDTO.class,vo));
	}

    @Override
    public Map<String, Object> deleteTemplate(@RequestBody SmsTemplateVo vo) {
        return smsBiz.deleteTemplate(PayloadParseUtil.jacksonBaseParse(SmsTemplateVoDTO.class,vo));
    }

    @Override
    public PageResponse<SmsTemplateDetailVo> findDetailListByCondition(String templateId,String content, Integer pageNo, Integer pageSize) throws ParseException {
        PageResponse<SmsTemplateDetail>  smsTemplateDetailPage = smsBiz.findDetailListByCondition(templateId,content,pageNo,pageSize);
        List<SmsTemplateDetail> smsTemplateDetailList = smsTemplateDetailPage.getResult();
        List<SmsTemplateDetailVo> templateDetailReqList = PayloadParseUtil.jacksonBaseParse(SmsTemplateDetailVo.class,smsTemplateDetailList);
        PageResponse<SmsTemplateDetailVo>  smsTemplateDetailVoPage = new PageResponse<>();
        smsTemplateDetailVoPage.setCount(smsTemplateDetailPage.getCount());
        smsTemplateDetailVoPage.setResult(templateDetailReqList);
        return smsTemplateDetailVoPage;
    }

    @Override
    public Map<String, Object> deleteSmsRecord(@RequestBody SmsRecordReq req) {
        return smsBiz.deleteSmsRecord(PayloadParseUtil.jacksonBaseParse(SmsRecordReqDTO.class,req));
    }

    @Override
    public Map<String, Object> exportSmsRecord(@RequestBody SmsRecordExportVo vo) throws ParseException {
        return smsBiz.exportSmsRecord(PayloadParseUtil.jacksonBaseParse(SmsRecordExportVoDTO.class, vo));
    }
}
