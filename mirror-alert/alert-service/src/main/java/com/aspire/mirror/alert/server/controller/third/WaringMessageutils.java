package com.aspire.mirror.alert.server.controller.third;

import com.aspire.mirror.alert.api.dto.model.TodayWarningMessageDTO;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class WaringMessageutils {

    public static List util(List<Map<String, Object>> summaryByyDeviceMfrs) {
        List summaryByIdctypeList = new ArrayList();
        if (!CollectionUtils.isEmpty(summaryByyDeviceMfrs)) {
            for (int i = 0; i < summaryByyDeviceMfrs.size(); i++) {
                TodayWarningMessageDTO todayWarningMessageDTO = new TodayWarningMessageDTO();
                todayWarningMessageDTO.setName((String) summaryByyDeviceMfrs.get(i).get("device_mfrs"));
                todayWarningMessageDTO.setSum(summaryByyDeviceMfrs.get(i).get("codeCount") == null ? 0 : Integer.valueOf(String.valueOf(summaryByyDeviceMfrs.get(i).get("codeCount"))));
                todayWarningMessageDTO.setElementary(summaryByyDeviceMfrs.get(i).get("lCount") == null ? 0 : Integer.valueOf(String.valueOf(summaryByyDeviceMfrs.get(i).get("lCount"))));
                todayWarningMessageDTO.setMediumGrade(summaryByyDeviceMfrs.get(i).get("mCount") == null ? 0 : Integer.valueOf(String.valueOf(summaryByyDeviceMfrs.get(i).get("mCount"))));
                todayWarningMessageDTO.setSenior(summaryByyDeviceMfrs.get(i).get("hCount") == null ? 0 : Integer.valueOf(String.valueOf(summaryByyDeviceMfrs.get(i).get("hCount"))));
                todayWarningMessageDTO.setGreat(summaryByyDeviceMfrs.get(i).get("sCount") == null ? 0 : Integer.valueOf(String.valueOf(summaryByyDeviceMfrs.get(i).get("sCount"))));
                summaryByIdctypeList.add(todayWarningMessageDTO);
            }
        }
        return summaryByIdctypeList;
    }
}
