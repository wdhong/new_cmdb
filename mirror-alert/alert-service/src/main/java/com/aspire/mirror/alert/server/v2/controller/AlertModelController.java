package com.aspire.mirror.alert.server.v2.controller;

import com.aspire.mirror.alert.api.v2.dto.AlertModelDetail;
import com.aspire.mirror.alert.api.v2.dto.AlertModelRequest;
import com.aspire.mirror.alert.api.v2.service.AlertsModelService;
import com.aspire.mirror.alert.server.util.PayloadParseUtil;
import com.aspire.mirror.alert.server.v2.biz.AlertModelBiz;
import com.aspire.mirror.alert.server.v2.dao.po.AlertModelRequestDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

import static com.aspire.mirror.alert.server.util.PayloadParseUtil.jacksonBaseParse;

@RestController
@Slf4j
public class AlertModelController implements AlertsModelService {

    @Autowired
    private AlertModelBiz alertModelBiz;

    @Override
    public void insertAlertModel(@RequestBody AlertModelRequest request) {

        if (null == request) {
            log.error("[AlertModel] >>> Insert Alert Model Request is Null");
            throw new RuntimeException("[AlertModel] >>> Insert Alert Model Request is null");
        }
        log.info("[AlertModel] >>> Insert Alert Model Request is {}", request);
        alertModelBiz.insertAlertModel(jacksonBaseParse( AlertModelRequestDTO.class, request));

    }

    @Override
    public List<AlertModelDetail> getAlertModelList(@RequestParam(value = "modelName", required = false) String modelName,
                                                     @RequestParam(value = "tableName", required = false) String tableName) {
        return PayloadParseUtil.jacksonBaseParse(AlertModelDetail.class, alertModelBiz.getAlertModelList(modelName, tableName));
    }

    @Override
    public Object getAlertModelTreeData() {
        return alertModelBiz.getAlertModelTreeData();
    }

    @Override
    public void deleteAlertModel(@RequestParam(value = "userName") String userName,@RequestBody List<String> request) {
        alertModelBiz.deleteAlertModel(userName, request);
    }

    @Override
    public AlertModelDetail getAlertModelDetail(@RequestParam(value = "id") String id) {
        return PayloadParseUtil.jacksonBaseParse(AlertModelDetail.class,alertModelBiz.getAlertModelDetail(id));
    }

    @Override
    public void updateAlertModel(@RequestBody AlertModelRequest request) {
        alertModelBiz.updateAlertModel(jacksonBaseParse(AlertModelRequestDTO.class, request));
    }
}
