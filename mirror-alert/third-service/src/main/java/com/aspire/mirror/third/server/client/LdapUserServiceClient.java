package com.aspire.mirror.third.server.client;

import com.migu.tsg.microservice.atomicservice.ldap.service.LdapUserService;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * @ClassName LdapUserServiceClient
 * @Description: TODO
 * @Author baiwenping
 * @Date 2019/12/19
 * @Version V1.0
 **/
@FeignClient("ldap")
public interface LdapUserServiceClient extends LdapUserService{
}
