package com.aspire.ums.bills.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Copyright (C), 2015-2020, 卓望数码有限公司
 * FileName: CmdbFeignClient
 * Author:   zhu.juwang
 * Date:     2020/7/22 14:46
 * Description: CMDB服务的 Feign Client 类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */

@FeignClient(value = "CMDB")
public interface CmdbFeignClient {
    /**
     *  CI业务系统配额模型数据查询
     * @return
     */
    @RequestMapping(value = "/v1/cmdb/module/businessQuota/list", method = RequestMethod.GET)
    List<Map<String,Object>> getAllBusinessQuotaInfo();

    /**
     *  CI需要计费业务系统配额模型数据查询
     * @return
     */
    @RequestMapping(value = "/v1/cmdb/module/businessQuota/listNeedCharge", method = RequestMethod.GET)
    List<Map<String, Object>> getNeedChargeBusinessQuotaInfo();
    /**
     * 获取字典值列表
     * @param type 字典类型
     * @param pid 父字典ID
     * @param pValue 父字典值
     * @param pType 父字典类型
     * @return 字典值列表
     */
    @GetMapping("/cmdb/configDict/getDictsByType")
    List<Map<String,Object>> getDictsByType(@RequestParam("type") String type,
                        @RequestParam(value="pid",required = false) String pid,
                        @RequestParam(value="pValue",required = false) String pValue,
                        @RequestParam(value="pType",required = false) String pType);

    @RequestMapping(value = "/cmdb/code/getSimpleCode", method = RequestMethod.POST)
    Map<String,String> getSimpleCode(@RequestBody Map<String,String> codeEntity);

    /**
     * 获取CI详情
     */
    @RequestMapping(value = "/cmdb/instance/detail", method = RequestMethod.GET)
    Map<String, Object> getInstanceDetail(@RequestParam("module_id") String moduleId,
                                          @RequestParam(value = "instance_id") String instanceId);

    /**
     * 获取配置
     * @param
     * @return
     */
    @GetMapping("/v3/cmdb/module/config/getConfigByCode")
    Map<String, Object> getConfigByCode(@RequestParam("configCode") String configCode);

    /**
     * 获取配置
     * @param
     * @return
     */
    @GetMapping("/cmdb/module/getModuleColumns")
    Map<String, Object> getModuleColumns(@RequestParam("moduleId") String moduleId);
}
