package com.aspire.cmdb.agent.schedule;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.aspire.cmdb.agent.util.StringUtils;
import com.aspire.ums.cmdb.code.payload.CmdbSimpleCode;
import com.aspire.ums.cmdb.collectApproval.payload.CmdbCollectApproval;
import com.aspire.ums.cmdb.common.Constants;
import com.aspire.ums.cmdb.helper.JDBCHelper;
import com.aspire.ums.cmdb.sqlManage.CmdbSqlManage;
import com.aspire.ums.cmdb.v2.code.service.ICmdbCodeService;
import com.aspire.ums.cmdb.v2.instance.service.ICmdbInstanceService;
import com.aspire.ums.cmdb.v2.module.service.ModuleService;
import com.aspire.ums.cmdb.v3.config.payload.CmdbConfig;
import com.aspire.ums.cmdb.v3.config.service.ICmdbConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Copyright (C), 2015-2019, 卓望数码有限公司
 * FileName: SyncModuleData
 * Author:   hangfang
 * Date:     2020/12/10
 * Description: 一个模型同步另一个模型N字段
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
@Slf4j
@EnableScheduling
@Component
public class SyncModuleData {

    @Autowired
    private ModuleService moduleService;
    @Autowired
    private ICmdbConfigService configService;
    @Autowired
    private ICmdbInstanceService instanceService;
    @Autowired
    private ICmdbCodeService codeService;

    @Autowired
    private JDBCHelper jdbcHelper;

    @Scheduled(cron = "0 0 0 1/1 * ?")
    public void startToSync() {
        //获取模型之间字段配置[{"toModule":{"id":"","relationKey":"owner_biz_system"},"syncFields":["department1","department2"],"fromModule": {"id":"","relationKey":"id"}}]
         CmdbConfig sourceConfig = configService.getConfigByCode("sync_from_other_module");
         if (sourceConfig == null) {
             log.error("未配置");
             return;
         }
        List<Map> configList = JSONArray.parseArray(sourceConfig.getConfigValue(), Map.class);
         for (Map config : configList) {
             // 从哪个模型中获取数据
             Map<String, Object> fromModuleInfo = JSONObject.parseObject(JSON.toJSONString(config.get("fromModule")), Map.class);
             String fromModuleId = fromModuleInfo.get("id").toString();
             String fromModuleRelationKey = fromModuleInfo.get("relationKey").toString();
             // 需要同步的字段
             List<Map> syncFields = JSONArray.parseArray(JSON.toJSONString(config.get("syncFields")), Map.class);
             // 目标模型信息
             Map<String, Object> toModuleInfo = JSONObject.parseObject(JSON.toJSONString(config.get("toModule")), Map.class);
             String toModuleId = toModuleInfo.get("id").toString();
             String toModuleRelationKey = toModuleInfo.get("relationKey").toString();
            if (syncFields == null || syncFields.size() == 0) {
                continue;
            }
             // 从目标模型查所有源模型主键
            String toBaseSql = moduleService.getModuleQuerySQL(toModuleId);
            String toSql = "select id, " + toModuleRelationKey + " from ( " + toBaseSql + ") target";
             CmdbSqlManage sqlManage = new CmdbSqlManage(toSql, null , Constants.UN_NEED_AUTH);
            List<Map<String, Object>> toKeyList = jdbcHelper.getQueryList(sqlManage, null, null, null, null);
            // 第个业务系统对应多少业务配额
            Map<String, List<String>> toKeyMap = new HashMap<>();
            if (null == toKeyList || toKeyList.size() == 0) {
                return;
            }
            // 例：一条业务系统下有多条业务配额，根据业务系统id进行整理
             for (Map<String,Object> keyMap: toKeyList) {
                 String targetKey = keyMap.get(toModuleRelationKey).toString();
                 if (!toKeyMap.containsKey(targetKey)) {
                     toKeyMap.put(targetKey, new ArrayList<>());
                 }
                 toKeyMap.get(targetKey).add(keyMap.get("id").toString());
             }
             // 源数据查询
             String fromBaseSql = moduleService.getModuleQuerySQL(fromModuleId);
             StringBuilder whereBuiler = new StringBuilder( "and " + fromModuleRelationKey + " in (");
             for (String tKey : toKeyMap.keySet()) {
                 whereBuiler.append("\"").append(tKey).append("\",");
             }
             String whereSql = whereBuiler.substring(0, whereBuiler.length() -1) + ")";
             sqlManage = new CmdbSqlManage(fromBaseSql, null , Constants.UN_NEED_AUTH);
             List<Map<String, Object>> sourceDataList = jdbcHelper.getQueryList(sqlManage, whereSql, null, null, null);

             for(Map<String, Object> sData : sourceDataList) {
                 List<CmdbCollectApproval> approvalList = new ArrayList<>();
                 //业务系统key
                String fKey = sData.get("id").toString();
                // 需要更新的实例列表
                List<String> tKeyList = toKeyMap.get(fKey);
                for (String tKey : tKeyList) {
                    Map<String, Object> params = new HashMap<>();
                    Map<String, Object> oldInstanceData = instanceService.getInstanceDetail(toModuleId, tKey);
                    params.put("module_id", toModuleId);
                    for (Map<String, String> fieldMap : syncFields) {
                        String toKey = fieldMap.get("toKey");
                        String curValue = StringUtils.isNotEmpty(fieldMap.get("fromKey")) ? sData.get(fieldMap.get("fromKey")).toString() : "";
                        String oldValue = StringUtils.isNotEmpty(oldInstanceData.get(toKey)) ? oldInstanceData.get(toKey).toString() : "";
                        if (curValue.equals(oldValue)) {
                            continue;
                        }
                        CmdbSimpleCode code = codeService.getSimpleCodeByCodeAndModuleId(toModuleId, toKey);
                        params.put(fieldMap.get("toKey"), sData.get(fieldMap.get("fromKey")));
                        CmdbCollectApproval approval = new CmdbCollectApproval();
                        approval.setModuleId(toModuleId);
                        approval.setOldValue(oldValue);
                        approval.setCurrValue(curValue);
                        approval.setCodeId(code.getCodeId());
                        approval.setResourceData(JSON.toJSONString(params));
                        approval.setOwnerModuleId(code.getOwnerModuleId());
                        approval.setApprovalType("update");
                        approval.setOperaterType("业务配额同步");
                        approval.setOperator("系统管理员");
                        approval.setOperatorTime(new Date());
                        approvalList.add(approval);
                    }
                    instanceService.update("系统管理员", approvalList);
                }
            }
         }


    }
}
