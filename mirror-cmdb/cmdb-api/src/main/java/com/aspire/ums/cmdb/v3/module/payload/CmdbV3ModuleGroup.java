package com.aspire.ums.cmdb.v3.module.payload;
import java.util.Date;
import java.util.List;

import com.aspire.ums.cmdb.code.payload.CmdbCode;
import com.aspire.ums.cmdb.module.payload.Module;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* 描述：
* @author
* @date 2020-01-09 14:33:21
*/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CmdbV3ModuleGroup {

    /**
     * ID标识
     */
    private String id;
    /**
     * 模型分类编码
     */
    private String groupCode;
    /**
     * 
     */
    private String groupName;
    /**
     * 是否删除, 0 否 1 是
     */
    private Integer isDelete;
}