package com.aspire.ums.cmdb.sync.client;

import com.migu.tsg.microservice.atomicservice.rbac.dto.UserBatchCreateRequest;
import com.migu.tsg.microservice.atomicservice.rbac.dto.UserCreateResponse;
import com.migu.tsg.microservice.atomicservice.rbac.dto.vo.UserVO;
import com.migu.tsg.microservice.atomicservice.rbac.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@FeignClient("rbac")
public interface RbacServiceClient extends UserService {

    /**
     * 创建信息
     *
     * @param userBatchCreateRequest 用户批量创建请求对象
     * @return UserCreateResponse 用户创建响应对象
     */
    @PostMapping(value = "/v1/user/batchInsert", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "创建", notes = "创建", response = UserCreateResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "返回", response = UserCreateResponse.class),
            @ApiResponse(code = 500, message = "Unexpected error", response = UserCreateResponse.class)})
    int batchCreatedUser(@RequestBody UserBatchCreateRequest userBatchCreateRequest);

//    @PutMapping(value = "/v1/user/batchModify", produces = MediaType.APPLICATION_JSON_VALUE)
//    @ApiOperation(value = "修改", notes = "修改", response = UserUpdateResponse.class)
//    @ApiResponses(value = {
//            @ApiResponse(code = 200, message = "返回", response = UserUpdateResponse.class),
//            @ApiResponse(code = 500, message = "Unexpected error", response = UserUpdateResponse.class)})
//    int modifyArrayByCode(@RequestBody
//            List<UserUpdateRequest> userUpdateRequest);
    /**
     * 根据ldapID查询用户详情
     *
     * @param userId
     *            动作主键
     * @return UserVO 动作详情响应对象
     */
    @GetMapping(value = "/v1/user/findByLdapId")
    @ApiOperation(value = "详情", notes = "根据userId获取动作详情")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "成功"), @ApiResponse(code = 500, message = "内部错误") })
    UserVO findByLdapId(@RequestParam("ldap_id") String ldapId);

    /**
     * 根据主键删除单作信息
     *
     * @param actionId
     *            主键
     * @@return Result 返回结果
     */
    @DeleteMapping(value = "/v1/user/{user_id}")
    @ApiOperation(value = "删除单条信息", notes = "删除单条信息")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "成功") })
    ResponseEntity<String> deleteByPrimaryKey(@PathVariable("user_id") String userId);


}


