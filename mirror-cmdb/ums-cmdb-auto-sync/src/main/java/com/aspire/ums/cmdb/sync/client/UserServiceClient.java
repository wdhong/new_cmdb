package com.aspire.ums.cmdb.sync.client;

import com.migu.tsg.microservice.atomicservice.rbac.service.UserService;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * <p>
 * 部门Feign接口
 * </p>
 * 
 * @title DepartmentServiceClient.java
 * @package com.migu.tsg.microservice.atomicservice.composite.clientservice.rabc
 * @author 曾祥华
 * @version 0.1 2019年3月5日
 */
@FeignClient(value = "rbac")
public interface UserServiceClient extends UserService {

}
