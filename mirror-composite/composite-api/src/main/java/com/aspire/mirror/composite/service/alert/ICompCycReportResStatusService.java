package com.aspire.mirror.composite.service.alert;

import com.aspire.mirror.composite.service.alert.payload.CycReportResStatusReq;
import com.aspire.mirror.composite.service.alert.payload.CycReportResStatusResp;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.text.ParseException;

@Api(value = "资源上报数据接口")
@RequestMapping("/${version}/alerts/cycReport/")
public interface ICompCycReportResStatusService {

    /**
     * 资源上报数据接口
     *
     * @return CycReportResStatusResp 资源上报数据接口
     */
    @PostMapping(value = "/reportFtpFiles", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "上报数据文件名", notes = "资源上报", response = CycReportResStatusResp.class, tags = {"CycReportResStatus API"})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "返回", response = CycReportResStatusResp.class),
            @ApiResponse(code = 500, message = "Unexpected error", response = CycReportResStatusResp.class)})
    CycReportResStatusResp reportFtpFiles(@RequestBody CycReportResStatusReq reportResStatusReq) throws ParseException;
}
