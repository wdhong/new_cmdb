package com.aspire.mirror.composite.service.alert.payload;

import lombok.Data;

/**
 * TODO
 * <p>
 * 项目名称:  mirror平台
 * 包:        com.aspire.mirror.composite.service.alert.payload
 * 类名称:    ComAlertDeviceIpPageRequst.java
 * 类描述:    TODO
 * 创建人:    JinSu
 * 创建时间:  2019/9/5 18:43
 * 版本:      v1.0
 */
@Data
public class ComAlertDeviceIpPageRequst extends ComAlertDeviceIpRequst{
    private String moniObject;

    private Integer pageNum;

    private Integer pageSize;

    private String name;
}
