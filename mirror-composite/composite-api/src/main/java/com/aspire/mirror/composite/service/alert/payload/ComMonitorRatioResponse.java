package com.aspire.mirror.composite.service.alert.payload;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ComMonitorRatioResponse {

    private String tenant;

    private String cpu;

    private String men;

    private String storage;

}
