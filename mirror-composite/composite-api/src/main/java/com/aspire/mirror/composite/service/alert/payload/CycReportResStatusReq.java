package com.aspire.mirror.composite.service.alert.payload;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
public class CycReportResStatusReq {

    /**
     * 0：配置数据主动上报
     * 1：性能数据主动上报
     * 2：流量监控数据主动上报

     */
    @NotNull
    @JsonProperty(value = "file_type")
    private Integer fileType;
    /**
     * 数据文件的全路径名称列表
     */
    @JsonProperty(value = "file_list")
    private List<String> fileList;
    /**
     * 不同请求消息的流水号
     */
    @JsonProperty(value = "request_id")
    private Integer requestId;
}
