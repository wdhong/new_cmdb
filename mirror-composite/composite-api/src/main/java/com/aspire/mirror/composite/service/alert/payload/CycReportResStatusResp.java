package com.aspire.mirror.composite.service.alert.payload;

import lombok.Data;

@Data
public class CycReportResStatusResp {

    private String faultString;

    private Integer responseID;
}
