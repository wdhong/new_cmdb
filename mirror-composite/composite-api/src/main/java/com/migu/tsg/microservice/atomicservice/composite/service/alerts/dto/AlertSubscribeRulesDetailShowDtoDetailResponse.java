package com.migu.tsg.microservice.atomicservice.composite.service.alerts.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class AlertSubscribeRulesDetailShowDtoDetailResponse {
    private AlertSubscribeRulesDetailShowDtoDetail alertSubscribeRulesDetailShowDtoDetail;
    private List<Reciver> reciverList;
   private List<AlertSubscribeRulesManagementRespone> AlertSubscribeRulesManagementResponeList;

}
