package com.migu.tsg.microservice.atomicservice.composite.clientservice.alert;

import com.aspire.ums.cmdb.instance.IInstanceAPI;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(value = "CMDB")
public interface CmdbInstanceClient extends IInstanceAPI {
}
