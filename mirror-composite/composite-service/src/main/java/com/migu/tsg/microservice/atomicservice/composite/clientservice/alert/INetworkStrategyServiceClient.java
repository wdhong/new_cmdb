package com.migu.tsg.microservice.atomicservice.composite.clientservice.alert;

import com.aspire.mirror.elasticsearch.api.service.zabbix.INetworkStrategyService;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * @author baiwp
 * @title: INetworkStrategyServiceClient
 * @projectName msp-composite
 * @description: TODO
 * @date 2019/7/2910:07
 */
@FeignClient("ELASTICSEARCH-SERVICE")
public interface INetworkStrategyServiceClient extends INetworkStrategyService {
}
