package com.migu.tsg.microservice.atomicservice.composite.clientservice.alert;

import org.springframework.cloud.netflix.feign.FeignClient;

import com.aspire.mirror.elasticsearch.api.service.zabbix.IStpInfoService;

/**
 * @author longfeng
 * @title: IStpInfoServiceClient
 * @projectName msp-composite
 * @description: TODO
 * @date 2019/7/2910:07
 */
@FeignClient("ELASTICSEARCH-SERVICE")
public interface IStpInfoServiceClient extends IStpInfoService {
}
