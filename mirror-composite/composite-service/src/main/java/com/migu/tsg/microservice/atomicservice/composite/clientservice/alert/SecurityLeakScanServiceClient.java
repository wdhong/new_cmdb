package com.migu.tsg.microservice.atomicservice.composite.clientservice.alert;

import com.aspire.mirror.alert.api.service.LeakScanService;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("ALERT-SERVICE")
public interface SecurityLeakScanServiceClient extends LeakScanService {
}
