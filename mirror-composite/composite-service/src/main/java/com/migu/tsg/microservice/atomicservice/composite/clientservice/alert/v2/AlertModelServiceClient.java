package com.migu.tsg.microservice.atomicservice.composite.clientservice.alert.v2;

import com.aspire.mirror.alert.api.v2.service.AlertsModelService;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(value = "${mirror.feign.alert.value}", path = "${mirror.feign.alert.path:}")
public interface AlertModelServiceClient extends AlertsModelService {
}
