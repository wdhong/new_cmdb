package com.migu.tsg.microservice.atomicservice.composite.clientservice.alert.v2.alert;

import com.aspire.mirror.alert.api.v2.service.alert.AlertBpmService;
import org.springframework.cloud.netflix.feign.FeignClient;


@FeignClient(value = "${mirror.feign.alert.value}", path = "${mirror.feign.alert.path:}")
public interface AlertBpmServiceClient extends AlertBpmService {

}
