package com.migu.tsg.microservice.atomicservice.composite.clientservice.bills.calculate;

import com.aspire.ums.bills.calculate.payload.CmdbBillsMonthBill;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * @projectName: CmdbBillsRecordClient
 * @description: 类
 * @author: luowenbo
 * @create: 2020-08-05 17:23
 **/
@FeignClient("BILLS")
public interface CmdbBillsRecordClient {

    @RequestMapping(value = "/v1/cmdb/bill/monthRecord/list",method = RequestMethod.GET)
    List<Map<String,Object>> listBillMonthRecord(@RequestParam("chargeTime") String chargeTime);

    @RequestMapping(value = "/v1/cmdb/bill/monthRecord/update",method = RequestMethod.PUT)
    Map<String,Object> updateBillMonth(@RequestBody CmdbBillsMonthBill monthBill);

    @RequestMapping(value = "/v1/cmdb/bill/monthRecord/manuallyCreate",method = RequestMethod.PUT)
    Map<String,Object> manuallyGeneratedMonthBills(@RequestParam("chargeTime") String chargeTime);
}
