package com.migu.tsg.microservice.atomicservice.composite.clientservice.cmdb;

import com.aspire.ums.cmdb.restful.alert.IAlertRestfulAPI;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(value = "CMDB")
public interface CmdbAlertRestfulClient extends IAlertRestfulAPI {
}
