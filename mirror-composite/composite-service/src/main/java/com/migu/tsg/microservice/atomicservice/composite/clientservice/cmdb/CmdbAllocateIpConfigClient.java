package com.migu.tsg.microservice.atomicservice.composite.clientservice.cmdb;

import com.aspire.ums.cmdb.allocate.IAllocateIpConfigAPI;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(value = "cmdb")
public interface CmdbAllocateIpConfigClient extends IAllocateIpConfigAPI {
}
