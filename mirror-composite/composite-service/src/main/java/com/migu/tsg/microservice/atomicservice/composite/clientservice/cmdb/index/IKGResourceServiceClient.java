package com.migu.tsg.microservice.atomicservice.composite.clientservice.cmdb.index;

import com.alibaba.fastjson.JSONObject;
import com.aspire.ums.cmdb.index.KGResourceIndexAPI;
import com.aspire.ums.cmdb.index.payload.ItCloudScreenRequest;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Map;

@FeignClient(value = "CMDB")
public interface IKGResourceServiceClient extends KGResourceIndexAPI {
}
