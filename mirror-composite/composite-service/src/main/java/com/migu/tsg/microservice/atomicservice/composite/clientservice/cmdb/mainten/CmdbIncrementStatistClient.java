package com.migu.tsg.microservice.atomicservice.composite.clientservice.cmdb.mainten;

import com.aspire.ums.cmdb.maintenance.payload.CmdbIncrementStatistRequest;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

/**
 * @ClassName CmdbIncrementStatistClient
 * @Description TODO
 * @Author luowenbo
 * @Date 2020/2/19 17:11
 * @Version 1.0
 */
@FeignClient(value = "CMDB")
public interface CmdbIncrementStatistClient {
    /*
     *  按照时间维度，来统计每个月的设备增量
     * */
    @PostMapping(value = "/cmdb/maintenance/statist/time" )
    List<Map<String,Object>> statistIncrementByTime(@RequestBody CmdbIncrementStatistRequest req);
}
