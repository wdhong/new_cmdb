package com.migu.tsg.microservice.atomicservice.composite.clientservice.cmdb.restful.bpm;

import com.aspire.ums.cmdb.common.Result;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@FeignClient(value = "CMDB")
public interface IBPMRestfulClient {
    /**
     * 对接BPM资源申请流程
     * @param resourceInfo 申请资源信息
     *  {
     *    "bizSystem": "业务系统",
     *    "idcType": "资源池名称",
     *    "data": [{
     *          "type": "资源类型",
     *          "num": "申请数量",
     *          "cpu": "云主机CPU数量",
     *          "memory": "云主机内存数量"
     *       }]
     *  }
     */
    @RequestMapping(value = "/cmdb/restful/bpm/resource/request", method = RequestMethod.POST)
    Map<String, Object> resourceRequestProcess(@RequestBody Map<String, Object> resourceInfo);

    @RequestMapping(value = "/cmdb/restful/bpm/resource/syncOrgSystem", method = RequestMethod.POST)
    Map<String, Object> syncOrgSystem(@RequestBody Map<String, Object> orgManagerData);

    @RequestMapping(value = "/cmdb/restful/bpm/resource/listOrderReportData", method = RequestMethod.GET)
    List<Map<String, Object>> listOrderReportData(@RequestParam("time") String submitMonth);

    /**
     * 根据系统账号获取业务系统列表
     * 1. 如果账号有绑定业务系统, 则显示绑定的业务系统列表
     * 2. 如果账号没有绑定业务系统, 则显示账号归属的部门及该部门下所有子部门的业务系统列表
     * @param account 系统账号
     * @return
     */
    @RequestMapping(value = "/cmdb/restful/bpm/resource/getBizSystemListByAccount", method = RequestMethod.GET)
    Result<Map<String, Object>> getBizSystemListByAccount(@RequestParam("account") String account,
                                                          @RequestParam(value = "bizSystem", required = false) String bizSystem,
                                                          @RequestParam("currentPage") int currentPage,
                                                          @RequestParam("pageSize") int pageSize);
}
