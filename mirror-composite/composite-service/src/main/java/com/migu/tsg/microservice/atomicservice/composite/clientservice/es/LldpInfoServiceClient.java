package com.migu.tsg.microservice.atomicservice.composite.clientservice.es;

import com.aspire.mirror.elasticsearch.api.service.zabbix.ILLdpInfoService;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * TODO
 * <p>
 * 项目名称:  mirror平台
 * 包:        com.migu.tsg.microservice.atomicservice.composite.clientservice.es
 * 类名称:    LldpInfoServiceClient.java
 * 类描述:    TODO
 * 创建人:    JinSu
 * 创建时间:  2019/9/20 17:12
 * 版本:      v1.0
 */
@FeignClient("ELASTICSEARCH-SERVICE")
public interface LldpInfoServiceClient extends ILLdpInfoService {
}
