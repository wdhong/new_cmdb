package com.migu.tsg.microservice.atomicservice.composite.clientservice.es;

import org.springframework.cloud.netflix.feign.FeignClient;

import com.aspire.mirror.elasticsearch.api.service.zabbix.INetPerformanceAnalysisService;

/**
 * @author baiwp
 * @title: ZabbixItemServiceClient
 * @projectName msp-composite
 * @description: TODO
 * @date 2019/6/2410:44
 */
@FeignClient("ELASTICSEARCH-SERVICE")
public interface NetPerformanceAnalysisClient extends INetPerformanceAnalysisService {
}
