package com.migu.tsg.microservice.atomicservice.composite.clientservice.log;

import com.aspire.mirror.log.api.service.IBusinessLogService;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("logService")
public interface IBusinessLogServiceClient extends IBusinessLogService {

}
