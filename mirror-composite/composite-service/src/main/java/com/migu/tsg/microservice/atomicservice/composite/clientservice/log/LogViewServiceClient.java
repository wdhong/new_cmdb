package com.migu.tsg.microservice.atomicservice.composite.clientservice.log;

import com.aspire.mirror.log.api.service.ILogService;
import org.springframework.cloud.netflix.feign.FeignClient;


@FeignClient("logService")
public interface LogViewServiceClient extends ILogService {
	
}