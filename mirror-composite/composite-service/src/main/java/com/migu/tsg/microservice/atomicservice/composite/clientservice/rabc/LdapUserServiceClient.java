package com.migu.tsg.microservice.atomicservice.composite.clientservice.rabc;

import com.migu.tsg.microservice.atomicservice.ldap.service.LdapUserService;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * @ClassName LdapUserServiceClient
 * @Description: TODO
 * @Author baiwenping
 * @Date 2019/12/26
 * @Version V1.0
 **/
@FeignClient("ldap")
public interface LdapUserServiceClient extends LdapUserService {
}
