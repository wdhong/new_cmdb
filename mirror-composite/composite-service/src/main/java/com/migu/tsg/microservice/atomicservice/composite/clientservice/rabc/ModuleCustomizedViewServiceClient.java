package com.migu.tsg.microservice.atomicservice.composite.clientservice.rabc;


import com.migu.tsg.microservice.atomicservice.rbac.service.ModuleCustomizedViewService;


import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(value="rbac",url="http://localhost:28101")
public interface ModuleCustomizedViewServiceClient extends ModuleCustomizedViewService {

	



}
