package com.migu.tsg.microservice.atomicservice.composite.clientservice.rabc;

import com.migu.tsg.microservice.atomicservice.rbac.service.SysManageService;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * @ClassName SysManageServiceClient
 * @Description: TODO
 * @Author baiwenping
 * @Date 2019/11/26
 * @Version V1.0
 **/
@FeignClient("rbac")
public interface SysManageServiceClient extends SysManageService {
}
