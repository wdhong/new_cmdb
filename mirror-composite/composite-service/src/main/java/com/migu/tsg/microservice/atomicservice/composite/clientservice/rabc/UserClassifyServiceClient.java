package com.migu.tsg.microservice.atomicservice.composite.clientservice.rabc;

import com.migu.tsg.microservice.atomicservice.rbac.service.UserClassifyService;
import org.springframework.cloud.netflix.feign.FeignClient;


/**
 * @author menglinjie
 */
@FeignClient(value = "rbac")
public interface UserClassifyServiceClient extends UserClassifyService {
}
