package com.migu.tsg.microservice.atomicservice.composite.config;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.migu.tsg.microservice.atomicservice.composite.clientservice.FeignExceptionDecoder;
import com.migu.tsg.microservice.atomicservice.composite.clientservice.rabc.DepartmentServiceClient;
import com.migu.tsg.microservice.atomicservice.composite.clientservice.rabc.UserServiceClient;
import com.migu.tsg.microservice.atomicservice.composite.controller.authcontext.RequestAuthContext;
import com.migu.tsg.microservice.atomicservice.composite.controller.authcontext.RequestAuthContext.RequestHeadUser;
import com.migu.tsg.microservice.atomicservice.composite.controller.util.JsonUtil;
import com.migu.tsg.microservice.atomicservice.composite.controller.util.ResourceAuthHelper;
import feign.Logger;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import feign.codec.ErrorDecoder;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.Base64Utils;
import rx.exceptions.CompositeException;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Feign全局配置
 * Project Name:composite-service
 * File Name:FeignConfiguration.java
 * Package Name:com.migu.tsg.microservice.atomicservice.composite.config
 * ClassName: FeignConfiguration <br/>
 * date: 2017年10月23日 下午7:53:52 <br/>
 * Feign全局配置
 *
 * @author pengguihua
 * @since JDK 1.6
 */
@Configuration
public class FeignConfiguration {
    public static final String RES_FILTER_HEADER = "head_resFilter";

    @Bean
    public RequestInterceptor requestInterceptor(ResourceAuthHelper resAuthHelper, UserServiceClient userClient, DepartmentServiceClient departmentService) {
        return new AuthInfoInterceptor(resAuthHelper, userClient, departmentService);
    }

    private static class AuthInfoInterceptor implements RequestInterceptor {
        private ResourceAuthHelper resAuthHelper;
        private UserServiceClient userClient;
        private DepartmentServiceClient departmentService;

        public AuthInfoInterceptor(ResourceAuthHelper resAuthHelper, UserServiceClient userClient, DepartmentServiceClient departmentService) {
            this.resAuthHelper = resAuthHelper;
            this.userClient = userClient;
            this.departmentService = departmentService;
        }

        @Override
        public void apply(RequestTemplate template) {
            RequestAuthContext context = RequestAuthContext.currentRequestAuthContext();
            RequestHeadUser user = context.getUser();
            template.header("head_orgAccount", user.getNamespace());
            template.header("head_userName", user.getUsername());
            template.header("head_isAdmin", String.valueOf(user.isAdmin()));
            template.header("head_isSuperUser", String.valueOf(user.isSuperUser()));

            // 如果为管理员, 或者配置为不需要解析资源权限配置, 直接返回
            if (user.isSuperUser() || user.isAdmin() || !Boolean.TRUE.equals(context.getLoadResFilter())) {
                return;
            }

            // 加载资源权限过滤配置, 放入HTTP请求头
            String cacheResFilterConfig = context.getCtxCacheResFilterConfig();
            if (StringUtils.isNotBlank(cacheResFilterConfig)) {
                template.header(RES_FILTER_HEADER, cacheResFilterConfig);
                return;
            }

            Map<String, Set<String>> resFilterConfig = resAuthHelper.verifyGetResource(user);
            // 获取用户所属部门及子部门所有用户
//            List<String> ldapIdIdList = userClient.findSameDeptUserIdByLdapId(user.getUsername());
//
//            if (!CollectionUtils.isEmpty(ldapIdIdList)) {
//                if (resFilterConfig == null)  resFilterConfig = Maps.newHashMap();
//                resFilterConfig.put("authAccounts", Sets.newHashSet(ldapIdIdList));
//            }


            if (MapUtils.isEmpty(resFilterConfig)) {
                return;
            }
            resFilterConfig = hanldeRoomConfig(resFilterConfig);

//            template.header("head_authAccounts", Joiner.on(",").join(ldapAccountList));
            try {
                byte[] jsonByteArr = JsonUtil.toJacksonJson(resFilterConfig).getBytes("UTF-8");
                String safeBase64 = Base64Utils.encodeToUrlSafeString(jsonByteArr);
                context.setCtxCacheResFilterConfig(safeBase64);
                template.header(RES_FILTER_HEADER, safeBase64);
            } catch (UnsupportedEncodingException e) {
                throw new CompositeException(e);
            }
        }

//        private void getSubDept(List<String> deptIdList, List<DepartmentDTO> deptList) {
//            if (!CollectionUtils.isEmpty(deptList)) {
//                deptIdList.addAll(deptList.stream().map(DepartmentDTO::getUuid).collect(Collectors.toList()));
////                    List<DepartmentDTO> departmentDTOList = rbacUser.getDeptList().stream().filter(item -> item.getDeptType() == 1).collect(Collectors.toList());
//                deptList.stream().forEach(item -> {
//                    List<DepartmentDTO> deptSubList = departmentService.queryByDeptId(item.getUuid());
//                    getSubDept(deptIdList, deptSubList);
//                });
//            } else {
//                return;
//            }
//        }

        private Map<String, Set<String>> hanldeRoomConfig(final Map<String, Set<String>> resFilterConfig) {
            Set<String> areaSet = resFilterConfig.get(ResourceAuthHelper.AREA);
            if (CollectionUtils.isEmpty(areaSet)) {
                return resFilterConfig;
            }
            List<String> concatList = areaSet.stream().filter(area -> StringUtils.isNotBlank(area) && area.indexOf("_") > 0).collect(Collectors.toList());
            Set<String> roomList = concatList.stream().map(area -> {
                areaSet.remove(area);
                int idx = area.indexOf("_");
                String idcType = area.substring(0, idx);
                areaSet.add(idcType);
                return area.substring(idx + 1);
            }).collect(Collectors.toSet());
            resFilterConfig.put(ResourceAuthHelper.ROOM, roomList);
            return resFilterConfig;
        }
    }


    @Bean
    public ErrorDecoder customErrorDecoder() {
        return new FeignExceptionDecoder();
    }

    @Bean
    public Logger.Level level() {
        return Logger.Level.FULL;
    }
}
