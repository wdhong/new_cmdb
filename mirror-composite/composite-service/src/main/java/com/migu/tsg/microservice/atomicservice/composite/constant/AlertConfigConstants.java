package com.migu.tsg.microservice.atomicservice.composite.constant;

/**
 * @BelongsProject: mirror-alert
 * @BelongsPackage: com.aspire.mirror.alert.server.v2.constant
 * @Author: baiwenping
 * @CreateTime: 2020-02-21 16:30
 * @Description: ${Description}
 */
public class AlertConfigConstants {
    public static final String YES = "1";

    public static final String NO = "0";

    public static final String MESSAGE_TEMPLATE_ALERT_TEMPLATE = "alert_template";


    public static final String IDC_TRANSFER = "idcTransfer";
}
