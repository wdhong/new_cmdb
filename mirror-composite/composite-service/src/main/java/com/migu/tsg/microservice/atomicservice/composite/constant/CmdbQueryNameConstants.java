package com.migu.tsg.microservice.atomicservice.composite.constant;

/**
 * @BelongsProject: mirror-alert
 * @BelongsPackage: com.aspire.mirror.alert.server.v2.constant
 * @Author: baiwenping
 * @CreateTime: 2020-02-21 16:30
 * @Description: ${Description}
 */
public class CmdbQueryNameConstants {
   
    //根据bizSyste 集合查联系人电话
    public static final String QUERY_BIZSYSTEM_INFO_BY_NAME = "query_bizSystem_info_by_name";

}
