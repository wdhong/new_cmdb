package com.migu.tsg.microservice.atomicservice.composite.controller.alert;

import com.alibaba.fastjson.JSONObject;
import com.aspire.mirror.alert.api.dto.AlertVoiceNotifyContentReq;
import com.aspire.mirror.alert.api.dto.AlertVoiceNotifyReq;
import com.aspire.mirror.composite.service.alert.IComAlertVoiceNotifyService;
import com.aspire.mirror.composite.service.alert.payload.CompAlertVoiceNotifyContentReq;
import com.aspire.mirror.composite.service.alert.payload.CompAlertVoiceNotifyReq;
import com.migu.tsg.microservice.atomicservice.composite.clientservice.alert.AlertVoiceNotifyServiceClient;
import com.migu.tsg.microservice.atomicservice.composite.controller.authcontext.RequestAuthContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static com.migu.tsg.microservice.atomicservice.composite.controller.util.PayloadParseUtil.jacksonBaseParse;

@RestController
@Slf4j
public class CompAlertVoiceNotifyController implements IComAlertVoiceNotifyService {

    @Autowired
    private AlertVoiceNotifyServiceClient client;


    @Override
    public String createdAlertVoiceNotify(@RequestBody CompAlertVoiceNotifyReq request) {
        RequestAuthContext authCtx = RequestAuthContext.currentRequestAuthContext();
        request.setCreator(authCtx.getUser().getUsername());
        AlertVoiceNotifyReq alertVoiceNotifyContentReq = jacksonBaseParse(AlertVoiceNotifyReq.class, request);
        log.info("#=====> create alert voice notify entity: {}", JSONObject.toJSONString(alertVoiceNotifyContentReq));
        return client.createdAlertVoiceNotify(alertVoiceNotifyContentReq);
    }

    @Override
    public Object getAlertVoiceNotify() {
        RequestAuthContext authCtx = RequestAuthContext.currentRequestAuthContext();
        return client.getAlertVoiceNotify(authCtx.getUser().getUsername());
    }

    @Override
    public ResponseEntity<String> getAlertVoiceNotifyContent(@RequestBody CompAlertVoiceNotifyContentReq request) {
        AlertVoiceNotifyContentReq alertVoiceNotifyContentReq = jacksonBaseParse(AlertVoiceNotifyContentReq.class, request);
        return client.getAlertVoiceNotifyContent(alertVoiceNotifyContentReq);
    }
}
