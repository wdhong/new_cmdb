package com.migu.tsg.microservice.atomicservice.composite.controller.alert;

import static com.migu.tsg.microservice.atomicservice.composite.controller.util.PayloadParseUtil.jacksonBaseParse;

import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.assertj.core.util.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.aspire.mirror.alert.api.dto.AlertEsDataRequest;
import com.aspire.mirror.alert.api.dto.AlertsDetailResponse;
import com.aspire.mirror.alert.api.dto.model.AlertsStatisticClassifyDTO;
import com.aspire.mirror.alert.api.dto.model.AlertsTop10DTOResponse;
import com.aspire.mirror.common.entity.PageResponse;
import com.aspire.mirror.composite.service.alert.IComAlertIndexPageService;
import com.aspire.mirror.composite.service.alert.payload.ComAlertsTop10DTOResponse;
import com.aspire.mirror.composite.service.alert.payload.ComIdcTypePhysicalReq;
import com.aspire.mirror.composite.service.alert.payload.CompAlertStatisticLevelResp;
import com.aspire.mirror.composite.service.alert.payload.CompAlertStatisticSummaryResp;
import com.aspire.mirror.composite.service.alert.payload.CompAlertsDetailResp;
import com.aspire.mirror.composite.service.alert.payload.CompAlertsStatisticClassifyResp;
import com.aspire.mirror.elasticsearch.api.dto.DevicePusedTopN;
import com.aspire.mirror.elasticsearch.api.dto.IdcTypePhysicalReq;
import com.google.common.collect.Maps;
import com.migu.tsg.microservice.atomicservice.composite.clientservice.alert.AlertCmdbInstanceServiceClient;
import com.migu.tsg.microservice.atomicservice.composite.clientservice.alert.AlertRestfulClient;
import com.migu.tsg.microservice.atomicservice.composite.clientservice.alert.AlertsIndexPageServiceClient;
import com.migu.tsg.microservice.atomicservice.composite.clientservice.alert.EsIndexPageServiceClient;
import com.migu.tsg.microservice.atomicservice.composite.constant.AlertConfigConstants;
import com.migu.tsg.microservice.atomicservice.composite.controller.authcontext.RequestAuthContext;
import com.migu.tsg.microservice.atomicservice.composite.controller.authcontext.ResAction;
import com.migu.tsg.microservice.atomicservice.composite.controller.util.ExportExcelUtil;
import com.migu.tsg.microservice.atomicservice.composite.controller.util.ResourceAuthHelper;
import com.migu.tsg.microservice.atomicservice.composite.controller.util.es.util.DateUtils;
import com.migu.tsg.microservice.atomicservice.composite.helper.CmdbHelper;
import com.migu.tsg.microservice.atomicservice.composite.service.rbac.payload.RbacResource;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class CompAlertsIndexPageController implements IComAlertIndexPageService {
	private Logger logger = LoggerFactory.getLogger(CompAlertsIndexPageController.class);

	@Autowired
	private AlertsIndexPageServiceClient indexPageServiceClient;
	@Autowired
	private EsIndexPageServiceClient esIndexPageServiceClient;

	/*
	 * @Autowired private IAlertRestfulClient iAlertRestfulClient;
	 */
	
	@Autowired
	private AlertRestfulClient alertRestfulClient;

	
	@Autowired
	private CmdbHelper cmdbHelper;
	
	 @Autowired
    protected ResourceAuthHelper resAuthHelper;

	@Autowired
	private AlertCmdbInstanceServiceClient alertCmdbInstanceServiceClient;
	
	//private static String query_department_by_bizSystem = "alert_query_department_by_bizSystem_id";
	

	@Override
	public CompAlertStatisticSummaryResp summary(@RequestParam(value = "idcType", required = false) String idcType) {
		return jacksonBaseParse(CompAlertStatisticSummaryResp.class, indexPageServiceClient.summary(idcType));
	}

	@Override
	public CompAlertStatisticLevelResp alertLevelSummayByTimeSpan(@RequestParam(value = "span") String span,
			@RequestParam(value = "idcType", required = false) String idcType) {
		return jacksonBaseParse(CompAlertStatisticLevelResp.class,
				indexPageServiceClient.alertLevelSummayByTimeSpan(span, idcType));
	}

	@Override
	public Map trend(@RequestParam(value = "span") String inteval,
			@RequestParam(value = "idcType", required = false) String idcType,
			@RequestParam(value = "deviceType", required = false) String deviceType,
			@RequestParam(value = "alertLevel", required = false) String alertLevel,
			@RequestParam(value = "source", required = false) String source) {
		return indexPageServiceClient.trend(inteval, idcType, deviceType, alertLevel, source);
	}

	@Override
	@ResAction(resType="mainIndexPage", action="classifyByTimeSpan", loadResFilter=true)
	public List<CompAlertsStatisticClassifyResp> classifyByTimeSpan(@RequestParam(value = "span") String span,
			@RequestParam(value = "idcType", required = false) String idcType) {
		RequestAuthContext authCtx = RequestAuthContext.currentRequestAuthContext();
		resAuthHelper.resourceActionVerify(authCtx.getUser(), new RbacResource(), authCtx.getResAction(), authCtx.getFlattenConstraints());
		List<AlertsStatisticClassifyDTO> classifyList = indexPageServiceClient.classifyByTimeSpan(span, idcType);
		List<CompAlertsStatisticClassifyResp> respList = Lists.newArrayList();
		for (AlertsStatisticClassifyDTO classifyDTO : classifyList) {
			CompAlertsStatisticClassifyResp resp = new CompAlertsStatisticClassifyResp();
			BeanUtils.copyProperties(classifyDTO, resp);
			respList.add(resp);
		}
		return respList;
	}

	@Override
	public List<ComAlertsTop10DTOResponse> alertDeviceTop10(
			@RequestParam(value = "idcType", required = false) String idcType,
			@RequestParam(value = "deviceType", required = false) String deviceType,
			@RequestParam(value = "alertLevel", required = false) String alertLevel,
			@RequestParam(value = "colName") String colName) {
		return jacksonBaseParse(ComAlertsTop10DTOResponse.class,
				indexPageServiceClient.alertTop10(idcType, deviceType, alertLevel, colName));
	}

	@Override
	public PageResponse<ComAlertsTop10DTOResponse> alertContentTop10(
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate,
			@RequestParam(value = "idcType", required = false) String idcType,
			@RequestParam(value = "deviceType", required = false) String deviceType,
			@RequestParam(value = "alertLevel", required = false) String alertLevel) {
		PageResponse<AlertsTop10DTOResponse> result = indexPageServiceClient
				.alertMoniIndexTop10(startDate + " 00:00:00", endDate + " 23:59:59", idcType, deviceType, alertLevel);
		PageResponse<ComAlertsTop10DTOResponse> response = new PageResponse<>();
		response.setCount(result.getCount());
		List<AlertsTop10DTOResponse> returnList = result.getResult();
		if (null != returnList && returnList.size() > 0) {
			response.setResult(jacksonBaseParse(ComAlertsTop10DTOResponse.class, returnList));
		}
		return response;
	}

	/**
	 * 最新警报列表
	 *
	 * @param operateStatus
	 * @return
	 */
	@Override
	public List<CompAlertsDetailResp> latest(@RequestParam(value = "operateStatus", required = false) Integer operateStatus,
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate) {
	
		List<AlertsDetailResponse> detailResponseList = indexPageServiceClient.latest(null, null,operateStatus,
				startDate,endDate);
		List<CompAlertsDetailResp> respList = Lists.newArrayList();
		for (AlertsDetailResponse detailResponse : detailResponseList) {
			CompAlertsDetailResp resp = new CompAlertsDetailResp();
			String codeName = cmdbHelper.getCodeName(AlertConfigConstants.IDC_TRANSFER, detailResponse.getIdcType());
			if (StringUtils.isNotEmpty(codeName)) {
				detailResponse.setIdcType(codeName);
			}
			BeanUtils.copyProperties(detailResponse, resp);
			respList.add(resp);
		}
		return respList;
	}

	/**
	 * 导出最新热点告警
	 *
	 * @param operateStatus
	 * @param response
	 */
	@Override
	public void exportLatest(@RequestParam(value = "operateStatus", required = false) Integer operateStatus,
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate, HttpServletResponse response)
			throws Exception {

		List<String> headerLi = Lists.newArrayList();
		headerLi.add("级别");
		headerLi.add("设备IP");
		headerLi.add("资源池");
		headerLi.add("业务线");
		headerLi.add("设备分类");
		headerLi.add("告警内容");
		headerLi.add("告警时间");
		List<String> keyLi = Lists.newArrayList();
		keyLi.add("alertLevel");
		keyLi.add("deviceIp");
		keyLi.add("idcType");
		keyLi.add("bizSys");
		keyLi.add("deviceClass");
		keyLi.add("moniIndex");
		keyLi.add("alertStartTime");
		if (1 == operateStatus) {
			headerLi.add("处理人");
			headerLi.add("处理时间");
			keyLi.add("execUser");
			keyLi.add("execTime");
		}
		String[] headerList = headerLi.toArray(new String[]{});
		String[] keyList = keyLi.toArray(new String[]{});

		SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		String title = "最新热点告警导出列表_" + sDateFormat.format(new Date());
		String fileName = title + ".xlsx";
		List<Map<String, Object>> dataList = indexPageServiceClient.exportLatest(null, null,operateStatus,
				startDate,endDate);
//       
		OutputStream os = response.getOutputStream();// 取得输出流
		response.setHeader("Content-Disposition",
				"attachment;filename=".concat(String.valueOf(URLEncoder.encode(fileName, "UTF-8"))));
		response.setHeader("Connection", "close");
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		// excel constuct
		ExportExcelUtil eeu = new ExportExcelUtil();
		Workbook book = new SXSSFWorkbook(128);
		eeu.exportExcel(book, 0, title, headerList, dataList, keyList);
		book.write(os);
	}
	
	@Override
	public List<Map<String,Object>> selectAlertsByOperateStatus(@RequestParam(value = "operateStatus", required = false) Integer operateStatus,
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate,
			@RequestParam(value = "colType", required = false) String colType) {
		List<Map<String, Object>> mapList = indexPageServiceClient.selectAlertsByOperateStatus(operateStatus, startDate, endDate, colType);
		if ("idcType".equalsIgnoreCase(colType)) {
			for (Map<String, Object> map: mapList) {
				String col = MapUtils.getString(map, "col");
				String name = cmdbHelper.getCodeName(AlertConfigConstants.IDC_TRANSFER, col);
				if (StringUtils.isNotEmpty(name)) {
					map.put("col", name);
				}
			}
		}
		return mapList;
		
	}

	/*****************************************************
	 * 监控首页
	 * 
	 * @throws Exception
	 ******************************************************/
	@Override
	public Map<String, Map<String, Long>> idcTypeDeviceUsedRate(
			@RequestBody ComIdcTypePhysicalReq comIdcTypePhysicalReq) throws Exception {

		Map<String, Object> paramMap = new HashMap<>();
		if(StringUtils.isNotBlank(comIdcTypePhysicalReq.getIdcType())) {
			paramMap.put("idcType", comIdcTypePhysicalReq.getIdcType());
		}
		if(StringUtils.isNotBlank(comIdcTypePhysicalReq.getDeviceType())) {
			paramMap.put("deviceType", comIdcTypePhysicalReq.getDeviceType());
		}
		if(StringUtils.isNotBlank(comIdcTypePhysicalReq.getBizSystem())) {
			paramMap.put("bizSystem", comIdcTypePhysicalReq.getBizSystem());
		}
		paramMap.put("deviceClass", "服务器");
		
		//List<Map<String, String>> deviceList = iAlertRestfulClient.statisticsDeviceByIdcType(paramMap);
		Object value = cmdbHelper.getCmdbData(paramMap
				,"Alert_API_statisticsDeviceByIdcType", "list");
		Map<String, Map<String, Long>> esData = esIndexPageServiceClient
				.idcTypeDeviceUsedRate(jacksonBaseParse(IdcTypePhysicalReq.class, comIdcTypePhysicalReq));
		if(null!=value) {
			List<Map<String, Object>> deviceList = (List<Map<String, Object>>)value;
			if (null != deviceList && deviceList.size() > 0) {
				for (Map<String, Object> c : deviceList) {
					long count = 0;
					if (null != c.get("deviceCount")) {
						count = Long.parseLong(c.get("deviceCount").toString());
					}

					String idcType =  c.get("idcType")==null?null:c.get("idcType").toString();
					if (esData.containsKey(idcType)) {
						Map<String, Long> dataMap = esData.get(idcType);
						dataMap.put("deviceCount", count);
						/*
						 * if (dataMap.containsKey("15-40")) { count = count -
						 * Long.parseLong(dataMap.get("15-40").toString()); } if
						 * (dataMap.containsKey("40-80")) { count = count -
						 * Long.parseLong(dataMap.get("40-80").toString()); } if
						 * (dataMap.containsKey("80")) { count = count -
						 * Long.parseLong(dataMap.get("40-80").toString()); } dataMap.put("15", count);
						 */
					} /*
						 * else { Map<String, Long> dataMap = new HashMap<>();
						 * 
						 * dataMap.put("15", count); dataMap.put("15-40", 0l); dataMap.put("40-80", 0l);
						 * dataMap.put("80", 0l);
						 * 
						 * esData.put(idcType, dataMap); dataMap.put("deviceCount", count); }
						 */
				}

			}
		}
		
		return esData;
	}

	@Override
	public Map<String, Map<String, Long>> bizSystemDeviceUsedRate(
			@RequestBody ComIdcTypePhysicalReq comIdcTypePhysicalReq) throws Exception {

		Map<String, Object> paramMap = new HashMap<>();
		if(StringUtils.isNotBlank(comIdcTypePhysicalReq.getIdcType())) {
			paramMap.put("idcType", comIdcTypePhysicalReq.getIdcType());
		}
		if(StringUtils.isNotBlank(comIdcTypePhysicalReq.getDeviceType())) {
			paramMap.put("deviceType", comIdcTypePhysicalReq.getDeviceType());
		}
		if(StringUtils.isNotBlank(comIdcTypePhysicalReq.getBizSystem())) {
			paramMap.put("bizSystem", comIdcTypePhysicalReq.getBizSystem().split(","));
		}
		paramMap.put("deviceClass", "服务器");
		Object value = cmdbHelper.getCmdbData(paramMap
				,"Alert_API_statisticsDeviceByDepartment1", "list");
		//List<Map<String, String>> deviceList = iAlertRestfulClient.statisticsDeviceByDepartment1(paramMap);

		Map<String, Map<String, Long>> esData = esIndexPageServiceClient
				.bizSystemDeviceUsedRate(jacksonBaseParse(IdcTypePhysicalReq.class, comIdcTypePhysicalReq));
		cmdbHelper.setBizDepartmentData(esData,"department");
		if(null!=value) {
			List<Map<String, Object>> deviceList = (List<Map<String, Object>>)value;
			if (null != deviceList && deviceList.size() > 0) {
				for (Map<String, Object> c : deviceList) {
					long count = 0;
					if (null != c.get("deviceCount")) {
						count = Long.parseLong(c.get("deviceCount").toString());
					}
					String idcType = c.get("department1")==null?null:c.get("department1").toString();
					if (esData.containsKey(idcType)) {
						Map<String, Long> dataMap = esData.get(idcType);
						dataMap.put("deviceCount", count);
					}
				}

			}
		}
		
		return esData;
	}
	
	@Override
	@ResAction(resType="indexpage", action="reportBizsysDeviceUsedrateTopN", loadResFilter=true)
	public Map<String, Map<String, Long>> bizSystemDeviceUsedRateTopN(
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate,
			@RequestParam(value = "size", required = false) Integer size, @RequestParam(value = "order") String order)
			throws Exception {
		
		RequestAuthContext authCtx = RequestAuthContext.currentRequestAuthContext();
		resAuthHelper.resourceActionVerify(authCtx.getUser(), new RbacResource(), authCtx.getResAction(), authCtx.getFlattenConstraints());

		if (StringUtils.isBlank(startDate) || StringUtils.isBlank(endDate)) {
			Date startDate1 = new Date();
			endDate = DateUtils.getDateStr(startDate1, "yyyy-MM-dd");
			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			c.add(Calendar.DATE, -1);
			Date d = c.getTime();
			startDate = DateUtils.getDateStr(d, "yyyy-MM-dd");
		}

		Map<String, Map<String, Long>> esData = esIndexPageServiceClient.bizSystemDeviceUsedRateTopN(startDate, endDate,
				size, "bizSystem", order);

		List<String> strList = Lists.newArrayList();
		for (String key : esData.keySet()) {
			strList.add(key);
		}
		/*
		 * Map<String, Object> paramMap = new HashMap<>(); paramMap.put("deviceClass",
		 * "服务器"); paramMap.put("bizSystem", strList);
		 */
		Map<String, Object> params = Maps.newHashMap();
		params.put("bizSystem", strList);
		//params.put("deviceClass", "服务器");
		Object value = cmdbHelper.getCmdbData(params
				,"Alert_API_statisticsDeviceByBizSystem", "list");
		cmdbHelper.setBizDepartmentData(esData,"bizSystem");
		if(null!=value) {
			List<Map<String, Object>> deviceList = (List<Map<String, Object>>)value;

			if (null != deviceList && deviceList.size() > 0) {
				for (Map<String, Object> c : deviceList) {
					long count = 0;
					if (null != c.get("deviceCount")) {
						count = Long.parseLong(c.get("deviceCount").toString());
					}
					String bizSystem = c.get("bizSystem")==null?"":c.get("bizSystem").toString();
					if (esData.containsKey(bizSystem)) {
						Map<String, Long> dataMap = esData.get(bizSystem);
						dataMap.put("deviceCount", count);
					}
				}

			}
		}
		
		return esData;
	}

	@Override
	public Map<String, Map<String, Long>> department1DeviceUsedRateTopN(
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate,
			@RequestParam(value = "size", required = false) Integer size, @RequestParam(value = "order") String order)
			throws Exception {
		if (StringUtils.isBlank(startDate) || StringUtils.isBlank(endDate)) {
			Date startDate1 = new Date();
			endDate = DateUtils.getDateStr(startDate1, "yyyy-MM-dd");
			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			c.add(Calendar.DATE, -1);
			Date d = c.getTime();
			startDate = DateUtils.getDateStr(d, "yyyy-MM-dd");
		}
		Map<String, Map<String, Long>> esData = esIndexPageServiceClient.bizSystemDeviceUsedRateTopN(startDate, endDate,
				size, "department1", order);

		List<String> strList = new ArrayList<>(esData.keySet());
		/*
		 * for (String key : esData.keySet()) { if (strList == "") { strList += key; }
		 * else { strList += "," + key; } }
		 */
		/*
		 * Map<String, Object> paramMap = new HashMap<>(); paramMap.put("deviceClass",
		 * "服务器"); paramMap.put("department1", strList);
		 */
		//List<Map<String, String>> deviceList = iAlertRestfulClient.statisticsDeviceByDepartment1(paramMap);
		Map<String, Object> params = Maps.newHashMap();
		params.put("department1", strList);
		params.put("deviceClass", "服务器");
		Object value = cmdbHelper.getCmdbData(params
				,"Alert_API_statisticsDeviceByDepartment1", "list");
		cmdbHelper.setBizDepartmentData(esData,"department");
		if(null!=value) {
			List<Map<String, Object>> deviceList = (List<Map<String, Object>>)value;
			if (null != deviceList && deviceList.size() > 0) {
				for (Map<String, Object> c : deviceList) {
					long count = 0;
					if (null != c.get("deviceCount")) {
						count =  Long.parseLong(c.get("deviceCount").toString());
					}
					String department1 = c.get("department1")==null?null:c.get("department1").toString();
					if (esData.containsKey(department1)) {
						Map<String, Long> dataMap = esData.get(department1);
						dataMap.put("deviceCount", count);
					}
				}

			}
		}
		
		return esData;
	}

	@Override
	@ResAction(resType="monitorIndexpage", action="view", loadResFilter=true)
	public Map<String, Object> deviceUsedRateTrends(@RequestBody ComIdcTypePhysicalReq comIdcTypePhysicalReq)
			throws Exception {
		RequestAuthContext authCtx = RequestAuthContext.currentRequestAuthContext();
		resAuthHelper.resourceActionVerify(authCtx.getUser(), new RbacResource(), authCtx.getResAction(), authCtx.getFlattenConstraints());
		String startDate = comIdcTypePhysicalReq.getStartDate();
		String endDate = comIdcTypePhysicalReq.getEndDate();
		int range= 7;
		if(comIdcTypePhysicalReq.getIndexType()==1) {
			range = 14;
		}		
		if (StringUtils.isBlank(startDate) || StringUtils.isBlank(endDate)) {
			Date startDate1 = new Date();
			endDate = DateUtils.getDateStr(startDate1, "yyyy-MM-dd");
			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			c.add(Calendar.DATE, -range);
			Date d = c.getTime();
			startDate = DateUtils.getDateStr(d, "yyyy-MM-dd");
			comIdcTypePhysicalReq.setStartDate(startDate);
			comIdcTypePhysicalReq.setEndDate(endDate);
		}
		Map<String,List<String>> authMap = alertRestfulClient.getAuthField(2);
		IdcTypePhysicalReq req = jacksonBaseParse(IdcTypePhysicalReq.class, comIdcTypePhysicalReq);
		req.setAuthMap(authMap);
		return esIndexPageServiceClient
				.deviceUsedRateTrends(req);
	}

	@Override
	public List<Map<String, Object>> bizSystemDeviceUsedRateLow(
			@RequestBody ComIdcTypePhysicalReq comIdcTypePhysicalReq) throws Exception {
		List<Map<String, Object>> list = esIndexPageServiceClient
				.bizSystemDeviceUsedRateLow(jacksonBaseParse(IdcTypePhysicalReq.class, comIdcTypePhysicalReq));
		if (null != list && list.size() > 0) {
			List<String> bizList = list.stream().map(item -> MapUtils.getString(item, "bizSystem"))
					.collect(Collectors.toList());
			cmdbHelper.formListBizData(list,bizList);
			
			/*Map<String, Object> params = Maps.newHashMap();
			params.put("bizSystem", bizList);
			Object value = cmdbHelper.getCmdbData(params
					, this.query_department_by_bizSystem, "list");
			if (null != value) {
				List<Map<String, Object>> departList = (List<Map<String, Object>>) value;
				if(departList.size()>0) {
					try {
						Map<String, Map<String, Object>> map = Maps.newHashMap();
						for(Map<String, Object> m:departList) {
							map.put(m.get("bizSystem").toString(), m);
						}
						for(Map<String, Object> m:list) {
							String name = m.get("bizSystem").toString();
							if(map.containsKey(name)) {
								Map<String, Object> biz = map.get(name);
								m.put("department1", biz.get("department1_name"));
								m.put("department2", biz.get("department2_name"));
							}
							
						}
					}catch(Exception e) {
						log.error("查询业务系统数据报错，查询code：{}",this.query_department_by_bizSystem);
					}
			}
			
		}}*/}
		return list;
	}

	@Override
	@ResAction(resType="monitorIndexpage", action="view", loadResFilter=true)
	public Map<String, Object> getUserRateForZH(@RequestParam(value = "deviceType",required=false) String deviceType) throws ParseException {
		RequestAuthContext authCtx = RequestAuthContext.currentRequestAuthContext();
		resAuthHelper.resourceActionVerify(authCtx.getUser(), new RbacResource(), authCtx.getResAction(), authCtx.getFlattenConstraints());
		AlertEsDataRequest request = new AlertEsDataRequest();
		   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
	      //  Date sourceDate = sdf.parse(month);  
	        Calendar cal = Calendar.getInstance();  
	        cal.add(Calendar.MONTH, -1);  
	     request.setMonth(sdf.format(cal.getTime()));
		request.setDeviceType(deviceType);
		return indexPageServiceClient.getUserRateForZH(request);
	}

	public List<Map<String, Object>> alertIdcDoHours(@RequestParam(value = "alertLevel") String alertLevel) {
		List<Map<String, Object>> mapList = indexPageServiceClient.alertIdcDoHours(alertLevel);
		for (Map<String, Object> map: mapList) {
			String col = MapUtils.getString(map, "idc_type");
			String name = cmdbHelper.getCodeName(AlertConfigConstants.IDC_TRANSFER, col);
			if (StringUtils.isNotEmpty(name)) {
				map.put("idc_type", name);
			}
		}
		return mapList;
	}

	public List<Map<String, Object>> devicePusedTopN (@PathVariable("kpi") String kpi,
											   @RequestParam(name = "idcType", required = false) String idcType,
											   @RequestParam(name = "size", defaultValue = "10") int size) {
		DevicePusedTopN devicePusedTopN = new DevicePusedTopN();
		devicePusedTopN.setIdcType(idcType);
		devicePusedTopN.setSize(size);
		List<Map<String, Object>> list = esIndexPageServiceClient.devicePusedTopN(kpi, devicePusedTopN);
		if (CollectionUtils.isNotEmpty(list)) {
			for (Map<String, Object> map: list) {
				String resourceId = MapUtils.getString(map, "resourceId");
				if (StringUtils.isEmpty(resourceId)) {
					continue;
				}
				Map<String, Object> ciMap = alertCmdbInstanceServiceClient.detailById(resourceId);
				if (ciMap == null || ciMap.isEmpty()) {
					continue;
				}
				map.put("ip", ciMap.get("ip"));
				map.put("bizSystem", ciMap.get("bizSystem"));
			}
		}

		return list;
	}
}
