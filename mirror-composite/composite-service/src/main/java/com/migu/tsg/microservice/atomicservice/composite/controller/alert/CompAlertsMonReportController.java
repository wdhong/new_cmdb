package com.migu.tsg.microservice.atomicservice.composite.controller.alert;

import com.aspire.mirror.composite.service.alert.ICompAlertsMonReportService;
import com.migu.tsg.microservice.atomicservice.composite.clientservice.alert.AlertMonReportServiceClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class CompAlertsMonReportController implements ICompAlertsMonReportService {

    @Autowired
    private AlertMonReportServiceClient client;

    @Override
    public Map<String,Object> viewByIdcType(@RequestBody Map<String,String> map) {
        return client.viewByIdcType(map);
    }

    @Override
    public List<Map<String, Object>> viewByIp(@RequestBody Map<String,String> map) {
        return client.viewByIp(map);
    }

    @Override
    public List<Map<String, Object>> viewByKeyComment(@RequestBody Map<String,String> map) {
        return client.viewByKeyComment(map);
    }
}
