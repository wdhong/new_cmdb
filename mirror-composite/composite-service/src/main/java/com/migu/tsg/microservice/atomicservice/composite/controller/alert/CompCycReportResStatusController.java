package com.migu.tsg.microservice.atomicservice.composite.controller.alert;

import com.aspire.mirror.alert.api.dto.CycReportResFileRequest;
import com.aspire.mirror.composite.service.alert.ICompCycReportResStatusService;
import com.aspire.mirror.composite.service.alert.payload.CycReportResStatusReq;
import com.aspire.mirror.composite.service.alert.payload.CycReportResStatusResp;
import com.migu.tsg.microservice.atomicservice.composite.clientservice.alert.CycReportResStatusServiceClient;
import com.migu.tsg.microservice.atomicservice.composite.controller.util.es.util.DateUtils;
import org.apache.commons.lang.StringUtils;
import org.assertj.core.util.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
public class CompCycReportResStatusController implements ICompCycReportResStatusService {

    private static final Logger logger = LoggerFactory.getLogger(CompCycReportResStatusController.class);
    @Autowired
    private CycReportResStatusServiceClient cycReportResStatusServiceClient;
    private static final String fileNameParseEx = "[a-zA-Z_]+(\\d{4}-\\d{2}-\\d{2}[_|-]\\d{2}_\\d{2}_\\d{2})_(\\d+)\\.\\w+";
    private static final Pattern fileNamePattern = Pattern.compile(fileNameParseEx, Pattern.CASE_INSENSITIVE);
    /**
     * 资源上报数据接口
     *
     * @param reportResStatusReq
     * @return CycReportResStatusResp 资源上报数据接口
     */
    @Override
    public CycReportResStatusResp reportFtpFiles(@RequestBody CycReportResStatusReq reportResStatusReq) throws ParseException {
        CycReportResStatusResp resp = new CycReportResStatusResp();
        resp.setResponseID(reportResStatusReq.getRequestId());
        if (CollectionUtils.isEmpty(reportResStatusReq.getFileList())) {
            resp.setFaultString("上报文件列表为空!");
        }
        List<CycReportResFileRequest> filelist = Lists.newArrayList();
        for (String file : reportResStatusReq.getFileList()) {
            String filePath = file.substring(0, file.lastIndexOf("/"));
            String fileName = file.substring(file.lastIndexOf("/") + 1);
            Matcher matcher = fileNamePattern.matcher(fileName);
            Date startTime = null;
            String sessionId = "";
            if (matcher.matches()) {
                String timeStr = matcher.group(1);
                String timeText = timeStr.substring(0, 10) + " "
                        + timeStr.substring(11).replace("_",":");
                if (StringUtils.isNotEmpty(timeText)) {
                    SimpleDateFormat sDateFormat = new SimpleDateFormat(DateUtils.DATA_PATTERN_FULL_SYMBOL);
                    startTime = sDateFormat.parse(timeText);
                }
                sessionId = matcher.group(2);
            }
            CycReportResFileRequest resFileRequest = new CycReportResFileRequest();
            resFileRequest.setFileName(fileName);
            resFileRequest.setFilePath(filePath);
            resFileRequest.setFileType(reportResStatusReq.getFileType());
            resFileRequest.setRequestId(reportResStatusReq.getRequestId());
            resFileRequest.setSessionId(sessionId);
            resFileRequest.setStartTime(startTime);
            filelist.add(resFileRequest);
        }
        cycReportResStatusServiceClient.saveReportFtpFiles(filelist);
        return resp;
    }

    // private static String filePath = "ftp://192.168.1.234:8021/data/uploadFile/photo/CMREPORT_2012-06-04_01_30_01_18217.xml";
}
