package com.migu.tsg.microservice.atomicservice.composite.controller.alert.v2;

import com.aspire.mirror.alert.api.dto.AlertsOrderRequest;
import com.aspire.mirror.alert.api.v2.dto.QueryParams;
import com.aspire.mirror.common.entity.PageResponse;
import com.aspire.mirror.composite.service.alert.payload.CompAlertsOrderRequest;
import com.aspire.mirror.composite.service.alert.v2.payload.CompQueryParams;
import com.aspire.mirror.composite.service.alert.v2.ICompAlertIsolateAlertsV2Service;
import com.migu.tsg.microservice.atomicservice.composite.clientservice.alert.v2.AlertIsolateAlertsV2ServiceClient;
import com.migu.tsg.microservice.atomicservice.composite.controller.authcontext.RequestAuthContext;
import com.migu.tsg.microservice.atomicservice.composite.controller.util.PayloadParseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @BelongsProject: msp-composite
 * @BelongsPackage: com.migu.tsg.microservice.atomicservice.composite.controller.alert.v2
 * @Author: baiwenping
 * @CreateTime: 2020-03-03 15:13
 * @Description: ${Description}
 */
@RestController
public class CompAlertIsolateAlertsV2Controller implements ICompAlertIsolateAlertsV2Service {
    @Autowired
    private AlertIsolateAlertsV2ServiceClient alertIsolateAlertsV2ServiceClient;

    /**
     * 查询屏蔽记录列表
     *
     * @param compQueryParams
     * @return
     */
    @Override
    public PageResponse<Map<String, Object>> list(@RequestBody CompQueryParams compQueryParams) {
        return alertIsolateAlertsV2ServiceClient.list(PayloadParseUtil.jacksonBaseParse(QueryParams.class, compQueryParams));
    }

    /**
     * 导出屏蔽记录列表
     *
     * @param compQueryParams
     * @return
     */
    @Override
    public Map<String, Object> export(@RequestBody CompQueryParams compQueryParams) {
        return alertIsolateAlertsV2ServiceClient.export(PayloadParseUtil.jacksonBaseParse(QueryParams.class, compQueryParams));
    }

    /**
     * 告警工单
     *
     * @param alertsOperationRequest 告警ID集合
     * @return 处理结果
     */
    public Map<String, Object> alertOrder(@RequestBody CompAlertsOrderRequest alertsOperationRequest) {
        RequestAuthContext authCtx = RequestAuthContext.currentRequestAuthContext();
        alertsOperationRequest.setUsername(authCtx.getUser().getUsername());
        alertsOperationRequest.setNamespace(authCtx.getUser().getNamespace());
        return alertIsolateAlertsV2ServiceClient.alertOrder(PayloadParseUtil.jacksonBaseParse(AlertsOrderRequest.class, alertsOperationRequest));
    }

    /**
     * 恢复告警
     *
     * @param params 告警ID集合
     * @return 处理结果
     */
    public Map<String, Object> alertRecovery(@RequestBody Map<String, Object> params) {
        return alertIsolateAlertsV2ServiceClient.alertRecovery(params);
    }
}
