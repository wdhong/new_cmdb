package com.aspire.mirror.jobtracker.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.github.ltsopensource.spring.boot.annotation.EnableJobTracker;

/**
 * 流水线服务启动类
 *
 * 项目名称:  mirror平台
 * 包:      com.aspire.mirror.template.server
 * 类名称:    TemplateServiceApplication.java
 * 类描述：   模板服务启动类
 * 创建人：  ZhangSheng
 * 创建时间:  2018-8-28 16:02:34
 * 版本:      v1.0
 */
@SpringBootApplication
@EnableJobTracker
@ComponentScan("com.aspire.mirror.jobtracker")
public class JobTrackerServiceApplication {
    /**
     * 服务启动入口
     *
     * @param args args
     */
    public static void main(String[] args) {
        SpringApplication.run(JobTrackerServiceApplication.class, args);
    }
}
