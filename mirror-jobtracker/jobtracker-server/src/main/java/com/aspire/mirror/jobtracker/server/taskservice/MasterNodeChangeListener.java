package com.aspire.mirror.jobtracker.server.taskservice;
import com.github.ltsopensource.core.cluster.Node;
import com.github.ltsopensource.core.commons.utils.StringUtils;
import com.github.ltsopensource.core.listener.MasterChangeListener;
import com.github.ltsopensource.core.logger.Logger;
import com.github.ltsopensource.core.logger.LoggerFactory;
import com.github.ltsopensource.spring.boot.annotation.MasterNodeListener;
@MasterNodeListener
public class MasterNodeChangeListener implements MasterChangeListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(MasterNodeChangeListener.class);

    /**
     * @param master master节点
     * @param isMaster 表示当前节点是不是master节点
     */
    @Override
    public void change(Node master, boolean isMaster) {
        if (isMaster) {
            LOGGER.info("My master");
        } else {
            LOGGER.info(StringUtils.format("master change", master));
        }
    }
}
