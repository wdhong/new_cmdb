/**
 *
 * 项目名： ops-service 
 * <p/> 
 *
 * 文件名:  OpsPipelineInstance.java 
 * <p/>
 *
 * 功能描述: TODO 
 * <p/>
 *
 * @author	pengguihua
 *
 * @date	2019年10月22日 
 *
 * @version	V1.0
 * <p/>
 *
 *<b>Copyright(c)</b> 2019 卓望公司-版权所有<br/>
 *   
 */
package com.aspire.mirror.ops.domain;

import java.util.Date;

import com.aspire.mirror.ops.api.domain.AspNodeResultEnum;
import com.aspire.mirror.ops.api.domain.OpsFileTransferActionModel;
import com.aspire.mirror.ops.api.domain.OpsIndexValueCollectRequest;
import com.aspire.mirror.ops.api.domain.OpsPipelineInstanceDTO;
import com.aspire.mirror.ops.api.domain.OpsScriptExecuteActionModel;
import com.aspire.mirror.ops.api.domain.OpsStatusEnum;
import com.aspire.mirror.ops.controller.authcontext.RequestAuthContext;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

/** 
 *
 * 项目名称: ops-service 
 * <p/>
 * 
 * 类名: OpsPipelineInstance
 * <p/>
 *
 * 类功能描述: 作业流水实例
 * <p/>
 *
 * @author	pengguihua
 *
 * @date	2019年10月22日  
 *
 * @version	V1.0 
 * <br/>
 *
 * <b>Copyright(c)</b> 2019 卓望公司-版权所有 
 *
 */
@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class OpsPipelineInstance extends OpsPipelineInstanceDTO {
	
	public Float calcTotalSeconds() {
		if (endTime == null || startTime == null) {
			return null;
		}
		return (endTime.getTime() - startTime.getTime()) / 1000f;
	}
	
	public static OpsPipelineInstance buildScriptExecutePipelineInstance(OpsScriptExecuteActionModel scriptExecuteData) {
		OpsPipelineInstance pipelineInstance = new OpsPipelineInstance();
		pipelineInstance.setPipelineId(NO_PIPELINE_ID);
		pipelineInstance.setPipelineInstanceName(scriptExecuteData.getRunName());
		pipelineInstance.setInstanceClassify(CLASSIFY_REALTIME_SCRIPT);
		pipelineInstance.setBizClassify(BIZ_CLASSIFY_OPS);
		pipelineInstance.setStatus(OpsStatusEnum.STATUS_5.getStatusCode());
		pipelineInstance.setCreateTime(new Date());
		pipelineInstance.setStartTime(new Date());
		pipelineInstance.setOperator(RequestAuthContext.getRequestHeadUserName());
		pipelineInstance.setAspNodeResult(AspNodeResultEnum.STATUS_2.getStatusCode());
		return pipelineInstance;
	}
	
	public static OpsPipelineInstance buildScriptExecutePipelineInstance(OpsIndexValueCollectRequest indexCollectData) {
		OpsPipelineInstance pipelineInstance = new OpsPipelineInstance();
		pipelineInstance.setPipelineId(NO_PIPELINE_ID);
		pipelineInstance.setPipelineInstanceName(indexCollectData.getScriptData().getScriptName());
		pipelineInstance.setInstanceClassify(CLASSIFY_REALTIME_SCRIPT);
		pipelineInstance.setBizClassify(BIZ_CLASSIFY_INDEX_COLLECT);
		pipelineInstance.setStatus(OpsStatusEnum.STATUS_5.getStatusCode());
		pipelineInstance.setCreateTime(new Date());
		pipelineInstance.setStartTime(new Date());
		pipelineInstance.setOperator(RequestAuthContext.getRequestHeadUserName());
		pipelineInstance.setAspNodeResult(AspNodeResultEnum.STATUS_2.getStatusCode());
		return pipelineInstance;
	}
	
	public static OpsPipelineInstance buildFileTransferPipelineInstance(OpsFileTransferActionModel fileTransferData) {
		OpsPipelineInstance pipelineInstance = new OpsPipelineInstance();
		pipelineInstance.setPipelineId(NO_PIPELINE_ID);
		pipelineInstance.setPipelineInstanceName(fileTransferData.getRunName());
		pipelineInstance.setInstanceClassify(CLASSIFY_FILE_TRANSFER);
		pipelineInstance.setStatus(OpsStatusEnum.STATUS_5.getStatusCode());
		pipelineInstance.setCreateTime(new Date());
		pipelineInstance.setStartTime(new Date());
		pipelineInstance.setOperator(RequestAuthContext.getRequestHeadUserName());
		pipelineInstance.setAspNodeResult(AspNodeResultEnum.STATUS_2.getStatusCode());
		return pipelineInstance;
	}
}
