package com.aspire.mirror.common.util;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.model.enums.EncryptionMethod;
import org.apache.commons.lang.StringUtils;
import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipOutputStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * TODO
 * <p>
 * 项目名称:  mirror平台
 * 包:        com.aspire.mirror.ops.util
 * 类名称:    ZipUtil.java
 * 类描述:    TODO
 * 创建人:    JinSu
 * 创建时间:  2020/6/16 13:43
 * 版本:      v1.0
 */
@Slf4j
public class ZipUtil {
    public static void toZip(String srcDir, String outPathFile, boolean isDelSrcFile) throws Exception {
        long start = System.currentTimeMillis();
        FileOutputStream out = null;
        ZipOutputStream zos = null;
        try {
            out = new FileOutputStream(new File(outPathFile));
            zos = new ZipOutputStream(out);
            File sourceFile = new File(srcDir);
            if (!sourceFile.exists()) {
                throw new Exception("待压缩文件不存在");
            }
            compress(sourceFile, zos, sourceFile.getName());
            if (isDelSrcFile) {
                delDir(srcDir);
            }
            log.info("源文件{}，压缩到{}完成，是否删除源文件{}，耗时{}ms", srcDir, outPathFile, isDelSrcFile, System.currentTimeMillis() - start);
        } catch (Exception e) {
            log.error("ZipUtil[toZip] is error, {}", e);
        } finally {
            if (zos != null) zos.close();
            if (out != null) out.close();
        }
    }

    public static void zipFileAndEncrypt(String filePath, String zipFileName, String password) {
        ZipParameters zipParameters = new ZipParameters();
        zipParameters.setEncryptFiles(true);
        zipParameters.setEncryptionMethod(EncryptionMethod.ZIP_STANDARD);
        List<File> filesToAdd = Arrays.asList(
                new File(filePath)
        );
        ZipFile zipFile = new ZipFile(zipFileName, password.toCharArray());
        try {
            zipFile.addFiles(filesToAdd, zipParameters);
        } catch (ZipException e) {
            log.error("ZipUtil[zipFileAndEncrypt] is error!", e);
        }
    }
    public static void zipFileAndEncrypt(List<File> filePathList, String zipFileName, String password) {
        ZipParameters zipParameters = new ZipParameters();
        ZipFile zipFile = null;
        if (StringUtils.isNotEmpty(password)) {
            zipParameters.setEncryptFiles(true);
            zipParameters.setEncryptionMethod(EncryptionMethod.ZIP_STANDARD);
            zipFile = new ZipFile(zipFileName, password.toCharArray());
        } else {
            zipFile = new ZipFile(zipFileName);
        }
//        List<File> filesToAdd = Lists.newArrayList();
//        for (String filePath : filePathList) {
//            filesToAdd.add(new File(filePath));
//        }
//        ZipFile zipFile = new ZipFile(zipFileName, password.toCharArray());
        try {
            zipFile.addFiles(filePathList, zipParameters);
        } catch (ZipException e) {
            log.error("ZipUtil[zipFileAndEncrypt] is error!", e);
        }
    }
    private static void delDir(String dirPath) {
        long start = System.currentTimeMillis();
        try {
            File dirFile = new File(dirPath);
            if (!dirFile.exists()) {
                return;
            }
            if (dirFile.isFile()) {
                dirFile.delete();
                return;
            }
            File[] files = dirFile.listFiles();
            if (files == null) {
                return;
            }
            for (int i = 0; i < files.length; i++) {
                delDir(files[i].toString());
            }
            dirFile.delete();
            log.info("删除文件{}，耗时：{}", dirPath, System.currentTimeMillis() - start);
        } catch (Exception e) {
            log.error("删除文件{}异常，耗时：{}, 异常原因{}", dirPath, System.currentTimeMillis() - start, e);
        }
    }

    private static void compress(File file, ZipOutputStream out, String dir) throws IOException {
        // 当前的是文件夹，则进行一步处理
        if (file.isDirectory()) {
            //得到文件列表信息
            File[] files = file.listFiles();

            //将文件夹添加到下一级打包目录
            out.putNextEntry(new ZipEntry(dir + "/"));

            dir = dir.length() == 0 ? "" : dir + "/";
            //循环将文件夹中的文件打包
            for (int i = 0; i < files.length; i++) {
                compress(files[i], out, dir + files[i].getName());
            }
        } else { // 当前是文件
            // 输入流
            FileInputStream inputStream = new FileInputStream(file);
            // 标记要打包的条目
            out.putNextEntry(new ZipEntry(dir));
            // 进行写操作
            int len = 0;
            byte[] bytes = new byte[1024];
            while ((len = inputStream.read(bytes)) > 0) {
                out.write(bytes, 0, len);
            }
            // 关闭输入流
            inputStream.close();
        }
    }
}
