package com.aspire.mirror.tasktracker.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

import com.github.ltsopensource.spring.boot.annotation.EnableTaskTracker;

/**
 * 流水线服务启动类
 *
 * 项目名称:  mirror平台
 * 包:      com.aspire.mirror.template.server
 * 类名称:    TemplateServiceApplication.java
 * 类描述：   模板服务启动类
 * 创建人：   JinSu
 * 创建时间:  2018-7-26 16:02:34
 * 版本:      v1.0
 */
@SpringBootApplication
@EnableTaskTracker
@EnableDiscoveryClient
@EnableFeignClients
public class TaskTrackerServiceApplication {
    /**
     * 服务启动入口
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(TaskTrackerServiceApplication.class, args);
    }
}
