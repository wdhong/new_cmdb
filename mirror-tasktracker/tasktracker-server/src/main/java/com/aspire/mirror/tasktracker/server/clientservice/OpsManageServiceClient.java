/**
 *
 * 项目名： tasktracker-server 
 * <p/> 
 *
 * 文件名:  OpsManageServiceClient.java 
 * <p/>
 *
 * 功能描述: TODO 
 * <p/>
 *
 * @author	pengguihua
 *
 * @date	2019年12月13日 
 *
 * @version	V1.0
 * <p/>
 *
 *<b>Copyright(c)</b> 2019 卓望公司-版权所有<br/>
 *   
 */
package com.aspire.mirror.tasktracker.server.clientservice;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

import com.aspire.mirror.ops.api.domain.GeneralResponse;

/** 
 *
 * 项目名称: tasktracker-server 
 * <p/>
 * 
 * 类名: OpsManageServiceClient
 * <p/>
 *
 * 类功能描述: TODO
 * <p/>
 *
 * @author	pengguihua
 *
 * @date	2019年12月13日  
 *
 * @version	V1.0 
 * <br/>
 *
 * <b>Copyright(c)</b> 2019 卓望公司-版权所有 
 *
 */
@FeignClient("ops-service")
public interface OpsManageServiceClient {
	
	@PutMapping(value = "/v1/ops-service/opsManage/executePipelineCronJob/{jobId}", produces = MediaType.APPLICATION_JSON_VALUE)
    GeneralResponse executePipelineCronJob(@PathVariable("jobId") Long jobId);
}
