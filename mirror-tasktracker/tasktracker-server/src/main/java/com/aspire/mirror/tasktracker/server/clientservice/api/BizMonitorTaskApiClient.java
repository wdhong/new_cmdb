package com.aspire.mirror.tasktracker.server.clientservice.api;

import feign.Param;
import feign.RequestLine;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

/**
 * TODO
 * <p>
 * 项目名称:  mirror平台
 * 包:        com.aspire.mirror.tasktracker.server.clientservice.api
 * 类名称:    BizMonitorTaskApiClient.java
 * 类描述:    TODO
 * 创建人:    JinSu
 * 创建时间:  2018/11/14 17:24
 * 版本:      v1.0
 */
@FeignClient("inspection-service")
public interface BizMonitorTaskApiClient {
    @PutMapping("/v1/items/bizMonitorTaskExcu/{item_id}")
    public void executeBizMonitorTask(@PathVariable("item_id") String itemId);
}
