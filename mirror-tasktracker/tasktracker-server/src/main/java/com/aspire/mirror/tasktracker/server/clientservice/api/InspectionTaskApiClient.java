package com.aspire.mirror.tasktracker.server.clientservice.api;

import feign.Param;
import feign.RequestLine;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

/**
* osa系统CMDB服务接口    <br/>
* Project Name:inspection-service
* File Name:InnerCmdbApiClient.java
* Package Name:com.aspire.mirror.inspection.server.clientservice
* ClassName: InnerCmdbApiClient <br/>
* date: 2018年8月27日 下午5:57:42 <br/>
* @author pengguihua
* @version 
* @since JDK 1.6
*/
@FeignClient("inspection-service")
public interface InspectionTaskApiClient {
	
	/**
	* 巡检任务执行服务接口. <br/>
	*
	* 作者： pengguihua
	* @param taskId
	*/  
	@PutMapping("/v1/taskExecute/inspection/{taskId}")
	public void executeInspectionTask(@PathVariable("taskId") String taskId);
}
