package com.aspire.mirror.tasktracker.server.taskservice;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.aspire.mirror.tasktracker.server.taskservice.taskExecute.TaskExecuteFacade;
import com.github.ltsopensource.core.domain.Action;
import com.github.ltsopensource.core.logger.Logger;
import com.github.ltsopensource.core.logger.LoggerFactory;
import com.github.ltsopensource.spring.boot.annotation.JobRunner4TaskTracker;
import com.github.ltsopensource.tasktracker.Result;
import com.github.ltsopensource.tasktracker.runner.JobContext;
import com.github.ltsopensource.tasktracker.runner.JobRunner;

/** 
* @author ZhangSheng 
* @version 2018年8月28日 下午11:30:49 
* @describe  自动巡检任务执行实现类
*/
@JobRunner4TaskTracker
public class TaskRunnerImpl implements JobRunner{
	private static final Logger LOGGER = LoggerFactory.getLogger(TaskRunnerImpl.class);
	
	@Autowired
	private TaskExecuteFacade taskExecuteFacade;
	
	@Override
	public Result run(JobContext jobContext) throws Throwable {
		SimpleDateFormat simpleDateFormat =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date =simpleDateFormat.format(new Date());
		LOGGER.info("========================= 开始任务 ("+date+")========================");
		try {
			LOGGER.info("=========================任务ID:("+jobContext.getJob().getTaskId()+")============");
			Result result =taskExecuteFacade.execute(jobContext);
			LOGGER.info("=========================任务执行成功===============================");
			return result;//new Result(Action.EXECUTE_SUCCESS,"任务执行成功!");
		} catch (Exception e) {
			LOGGER.info("=========================任务执行失败===============================");
			LOGGER.info("任务执行失败",e);
			return new Result(Action.EXECUTE_FAILED,"任务执行失败!");
		}
	}

}
