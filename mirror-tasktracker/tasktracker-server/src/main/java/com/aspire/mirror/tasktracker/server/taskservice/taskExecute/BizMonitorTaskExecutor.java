package com.aspire.mirror.tasktracker.server.taskservice.taskExecute;

import com.aspire.mirror.tasktracker.server.clientservice.ClientServiceBuilder;
import com.aspire.mirror.tasktracker.server.clientservice.api.BizMonitorTaskApiClient;
import com.github.ltsopensource.core.domain.Action;
import com.github.ltsopensource.tasktracker.Result;
import com.github.ltsopensource.tasktracker.runner.JobContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 业务监控任务
 * <p>
 * 项目名称:  mirror平台
 * 包:        com.aspire.mirror.tasktracker.server.taskservice.taskExecute
 * 类名称:    BizMonitorTaskExecutor.java
 * 类描述:    业务监控任务
 * 创建人:    JinSu
 * 创建时间:  2018/11/14 17:14
 * 版本:      v1.0
 */
@Component
public class BizMonitorTaskExecutor implements ITaskExecutor {
    private static final String BIZ_MONITOR_TASK_NAME = "biz_monitor_task";

//    @Value("${taskExecutor.bizMonitorTask.url:http://127.0.0.1:8127}")
//    private String bizMonitorEndpoint;

    @Autowired
    private BizMonitorTaskApiClient client;

    @Override
    public boolean isAware(String taskIdentity) {
        return taskIdentity.equals(BIZ_MONITOR_TASK_NAME);
    }

    @Override
    public Result execute(JobContext jobContext) {
//        BizMonitorTaskApiClient client
//                = ClientServiceBuilder.buildClientService(BizMonitorTaskApiClient.class, bizMonitorEndpoint);
        String itemId = jobContext.getJob().getTaskId();
        client.executeBizMonitorTask(itemId);
        return new Result(Action.EXECUTE_SUCCESS, "Task execute success!");
    }
}
