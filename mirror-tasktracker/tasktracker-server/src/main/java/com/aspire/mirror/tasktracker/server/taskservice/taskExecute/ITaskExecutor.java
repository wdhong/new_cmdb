package com.aspire.mirror.tasktracker.server.taskservice.taskExecute;

import com.github.ltsopensource.tasktracker.Result;
import com.github.ltsopensource.tasktracker.runner.JobContext;

/**
* 任务执行接口    <br/>
* Project Name:tasktracker-server
* File Name:ITaskExecutor.java
* Package Name:com.aspire.mirror.tasktracker.server.taskservice.taskExecute
* ClassName: ITaskExecutor <br/>
* date: 2018年9月13日 下午5:47:37 <br/>
* @author pengguihua
* @version 
* @since JDK 1.6
*/ 
interface ITaskExecutor {
	
	/**
	* 是否为当前ITaskExecute实现感兴趣的task. <br/>
	*
	* 作者： pengguihua
	* @return
	*/  
	public boolean isAware(String taskIdentity);
	
	/**
	* 执行任务. <br/>
	*
	* 作者： pengguihua
	* @param jobContext
	* @return
	*/  
	public Result execute(JobContext jobContext);
}
