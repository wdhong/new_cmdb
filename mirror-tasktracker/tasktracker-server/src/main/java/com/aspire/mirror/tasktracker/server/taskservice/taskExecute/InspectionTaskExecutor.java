package com.aspire.mirror.tasktracker.server.taskservice.taskExecute;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.aspire.mirror.tasktracker.server.clientservice.ClientServiceBuilder;
import com.aspire.mirror.tasktracker.server.clientservice.api.InspectionTaskApiClient;
import com.github.ltsopensource.core.domain.Action;
import com.github.ltsopensource.tasktracker.Result;
import com.github.ltsopensource.tasktracker.runner.JobContext;

/**
* 巡检执行实现    <br/>
* Project Name:tasktracker-server
* File Name:InspectionTaskExecutor.java
* Package Name:com.aspire.mirror.tasktracker.server.taskservice.taskExecute
* ClassName: InspectionTaskExecutor <br/>
* date: 2018年9月13日 下午6:13:19 <br/>
* @author pengguihua
* @version 
* @since JDK 1.6
*/ 
@Component
class InspectionTaskExecutor implements ITaskExecutor {
	private static final String INSPECTION_TASK_NAME = "inspection_task";
	
//	@Value("${taskExecutor.inspectionTask.url:http://127.0.0.1:8128}")
//	private String inspectionEndpoint;
	@Autowired
	private  InspectionTaskApiClient client;
	@Override
	public boolean isAware(String taskIdentity) {
		return INSPECTION_TASK_NAME.equals(taskIdentity);
	}

	@Override
	public Result execute(JobContext jobContext) {
//		InspectionTaskApiClient client
//			= ClientServiceBuilder.buildClientService(InspectionTaskApiClient.class, inspectionEndpoint);
		String taskId = jobContext.getJob().getTaskId();
		client.executeInspectionTask(taskId);
		return new Result(Action.EXECUTE_SUCCESS, "Task execute success!");
	}
}
