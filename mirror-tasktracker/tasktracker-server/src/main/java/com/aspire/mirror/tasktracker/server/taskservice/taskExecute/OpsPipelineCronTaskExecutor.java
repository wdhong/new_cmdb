/**
 *
 * 项目名： tasktracker-server 
 * <p/> 
 *
 * 文件名:  OpsPipelineCronTaskExecutor.java 
 * <p/>
 *
 * 功能描述: TODO 
 * <p/>
 *
 * @author	pengguihua
 *
 * @date	2019年12月13日 
 *
 * @version	V1.0
 * <p/>
 *
 *<b>Copyright(c)</b> 2019 卓望公司-版权所有<br/>
 *   
 */
package com.aspire.mirror.tasktracker.server.taskservice.taskExecute;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aspire.mirror.tasktracker.server.clientservice.OpsManageServiceClient;
import com.github.ltsopensource.core.domain.Action;
import com.github.ltsopensource.tasktracker.Result;
import com.github.ltsopensource.tasktracker.runner.JobContext;

import lombok.extern.slf4j.Slf4j;

/** 
 *
 * 项目名称: tasktracker-server 
 * <p/>
 * 
 * 类名: OpsPipelineCronTaskExecutor
 * <p/>
 *
 * 类功能描述: TODO
 * <p/>
 *
 * @author	pengguihua
 *
 * @date	2019年12月13日  
 *
 * @version	V1.0 
 * <br/>
 *
 * <b>Copyright(c)</b> 2019 卓望公司-版权所有 
 *
 */
@Slf4j
@Component
class OpsPipelineCronTaskExecutor implements ITaskExecutor {
	private static final String	OPS_PIPELINE_JOB_IDETIFY	= "ops_pipeline_cron_job";
	@Autowired
	private OpsManageServiceClient opsManageClient;
	
	@Override
	public boolean isAware(String taskIdentity) {
		return OPS_PIPELINE_JOB_IDETIFY.equals(taskIdentity);
	}
	
	@Override
	public Result execute(JobContext ctx) {
		String jobId = ctx.getJob().getParam("jobId");
		if (StringUtils.isBlank(jobId)) {
			String tip = "The ops pipeline run job with id '" + ctx.getJob().getTaskId() + "' has no param of jobId.";
			log.warn(tip);
			return new Result(Action.EXECUTE_FAILED, tip);
		}
		opsManageClient.executePipelineCronJob(Long.valueOf(jobId));
		return new Result(Action.EXECUTE_SUCCESS, "Pipeline run job with jobId '" + jobId + "' execute success!");
	}
}
