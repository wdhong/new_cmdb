package com.aspire.mirror.tasktracker.server.taskservice.taskExecute;

import static java.lang.String.format;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.ltsopensource.core.domain.Action;
import com.github.ltsopensource.tasktracker.Result;
import com.github.ltsopensource.tasktracker.runner.JobContext;

import lombok.extern.slf4j.Slf4j;

/**
* 任务执行facade   <br/>
* Project Name:tasktracker-server
* File Name:TaskExecuteFacade.java
* Package Name:com.aspire.mirror.tasktracker.server.taskservice.taskExecute
* ClassName: TaskExecuteFacade <br/>
* date: 2018年9月13日 下午5:51:43 <br/>
* @author pengguihua
* @version 
* @since JDK 1.6
*/ 
@Slf4j
@Component
public class TaskExecuteFacade {
	private static final String PARAM_TASK_IDENTITY = "task_identity";
	@Autowired
	private List<ITaskExecutor> taskExecutorList;
	
	/**
	* 由具体实现执行任务. <br/>
	*
	* 作者： pengguihua
	* @param ctx
	* @return
	* @throws Throwable
	*/  
	public Result execute(JobContext ctx) throws Throwable {
		String taskIdentity = ctx.getJob().getParam(PARAM_TASK_IDENTITY);
		if (StringUtils.isBlank(taskIdentity)) {
			String tip = format("The job %s must have the parameter of %s", ctx.getJob(), PARAM_TASK_IDENTITY);
			log.error(tip);
			new Result(Action.EXECUTE_FAILED, tip);
		}
		
		for (ITaskExecutor executor : taskExecutorList) {
			if (executor.isAware(taskIdentity)) {
				return executor.execute(ctx);
			}
		}
		
		String tip = format("There is no taskExcecute implementation for the task with taskIdentity %s", taskIdentity);
		log.error(tip);
		return new Result(Action.EXECUTE_FAILED, tip);
	}

}
