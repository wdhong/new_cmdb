'use strict'
const testEnvData = {
     NODE_ENV: '"production"',
    PORT: 8080,
    // API_SERVER_URL: 'http://10.153.1.36:30002', // 现网
    API_SERVER_URL: 'http://10.1.203.100:5566',
    BIZ_LOG_URL: 'http://10.181.12.121:30056',
    REALM: 'demo_realm',
    // CMDB_SERVER_URL: 'http://10.153.1.36:30004', // 现网
    CMDB_SERVER_URL: 'http://10.1.203.100:2222',
    FTP_SERVER_URL: 'http://10.12.12.139:59090',
    AUTH_SERVER_URL: 'http://10.1.203.99:8180/auth',
    // AUTH_SERVER_URL: 'http://10.153.1.54:8180/auth', // 现网
    BPMX_SERVER_URL: 'http://10.12.9.238:8081/front/home',
    GRAFANA_SERVER_URL: 'http://10.12.70.41:3000',
    DEVELOP_HELP_SERVER_URL: 'http://10.12.9.232:4044',
    RESOURCE_SERVER_URL: 'http://10.12.70.39:2223',
    SSL_REQUIRED: 'external',
    RESOURCE: 'prod_vue',
    PUBLIC_CLIENT: true,
    OPERATE_SERVER_URL: 'http://117.132.183.206:8075'
}
const prodEnvData = {
     NODE_ENV: '"production"',
    PORT: 8080,
    // API_SERVER_URL: 'http://10.153.1.36:30002', // 现网
    API_SERVER_URL: 'http://10.1.203.100:5566',
    BIZ_LOG_URL: 'http://10.181.12.121:30056',
    REALM: 'demo_realm',
    // CMDB_SERVER_URL: 'http://10.153.1.36:30004', // 现网
    CMDB_SERVER_URL: 'http://10.1.203.100:2222',
    FTP_SERVER_URL: 'http://10.12.12.139:59090',
    AUTH_SERVER_URL: 'http://10.1.203.99:8180/auth',
    // AUTH_SERVER_URL: 'http://10.153.1.54:8180/auth', // 现网
    BPMX_SERVER_URL: 'http://10.12.9.238:8081/front/home',
    GRAFANA_SERVER_URL: 'http://10.12.70.41:3000',
    DEVELOP_HELP_SERVER_URL: 'http://10.12.9.232:4044',
    RESOURCE_SERVER_URL: 'http://10.12.70.39:2223',
    SSL_REQUIRED: 'external',
    RESOURCE: 'prod_vue',
    PUBLIC_CLIENT: true,
    OPERATE_SERVER_URL: 'http://117.132.183.206:8075'
}
// 默认读取 prodEnvData
const envData = process.argv[2] === 'test' ? testEnvData : prodEnvData
module.exports = envData