export const taskState = ['已完成', '运行中']
export const taskStatus = [{
    label: '正常',
    value: '0'
}, {
    label: '异常',
    value: '1'
}, {
    label: '无结果',
    value: '2'
}]
export const reportStatus = [ {
    label: '已完成',
    value: 'FINNISHED'
}, {
    label: '运行中',
    value: 'RUNNING'
}]
export const taskExecStatus = [{
    label: '正常',
    value: '1,2'
}, {
    label: '异常',
    value: '0'
}]